
var sel = window.getSelection();
var module;
var ButtonPosition;
var scheduledTime;
var isBulk;
var recordIds;
var isAuthorized;

var userFields;
var moduleFields;
var historyFields;
var selectedUser;
var currentRecords=[];

var credential = null;

var dotCircleLoader = `<div id="floatingCirclesG">
                    <div class="f_circleG" id="frotateG_01"></div>
                    <div class="f_circleG" id="frotateG_02"></div>
                    <div class="f_circleG" id="frotateG_03"></div>
                    <div class="f_circleG" id="frotateG_04"></div>
                    <div class="f_circleG" id="frotateG_05"></div>
                    <div class="f_circleG" id="frotateG_06"></div>
                    <div class="f_circleG" id="frotateG_07"></div>
                    <div class="f_circleG" id="frotateG_08"></div>
                </div>`;

var sendingLoader = `<div class="cssload-contain">
                <div class="cssload-dot"></div>
                <div class="cssload-dot"></div>
                <div class="cssload-dot"></div>
                <div class="cssload-dot"></div>
                <div class="cssload-dot"></div>
            </div>`;

var verifiedTickSVG = `<svg class="sendingMsgSuccessSpan" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000"><g><rect fill="none" height="24" width="24"/></g><g><path d="M23,12l-2.44-2.79l0.34-3.69l-3.61-0.82L15.4,1.5L12,2.96L8.6,1.5L6.71,4.69L3.1,5.5L3.44,9.2L1,12l2.44,2.79l-0.34,3.7 l3.61,0.82L8.6,22.5l3.4-1.47l3.4,1.46l1.89-3.19l3.61-0.82l-0.34-3.69L23,12z M10.09,16.72l-3.8-3.81l1.48-1.48l2.32,2.33 l5.85-5.87l1.48,1.48L10.09,16.72z"/></g></svg>`;

var loader = `<div class="wcMsgLoadingInner" title="loading…"><svg class="wcMsgLoadingSVG" width="17" height="17" viewBox="0 0 46 46" role="status"><circle class="wcMsgLoadingSvgCircle" cx="23" cy="23" r="20" fill="none" stroke-width="6" style="stroke: rgb(57 82 234);"></circle></svg></div>`;


// Extension Details
var phoneRecord;
var SMScredential;
var msgTextMaxLength;

var extensionName = 'WhatsApp';
var extensionAPI = 'whatsappforzohocrm__';
var extensionFunction = 'sendsmsapi';
var extensionInvokeAPI = extensionAPI + extensionFunction;

var extensionConnectorName = 'ringcentral';
var extensionConnector = extensionAPI.split('__')[0]+"."+extensionConnectorName;

var extensionFieldName = "Name";
var extensionFieldMessage = extensionAPI + "WhatsApp_Message";
var extensionFieldRecipientNumber = extensionAPI + "Recipient_Number";
var extensionFieldRecipientId = extensionAPI + "Recipient_Id";
var extensionFieldSenderPhone = extensionAPI + "Sender_Phone";
var extensionFieldModule = extensionAPI + "Module";
var extensionFieldModuleForTemp = extensionAPI + "Module_Name";
var extensionFieldDeal = extensionAPI + "Deal";
var extensionFieldContact = extensionAPI + "Contact";
var extensionFieldLead = extensionAPI + "Lead";
var extensionFieldAccount = extensionAPI + "Account";
var extensionFieldSchedule = extensionAPI + "Scheduled_Time";
var extensionFieldStatus = extensionAPI + "Status";
//var extensionFieldDirection = extensionAPI + "Direction";

var extensionCredential = extensionAPI + "credentials";
var extensionTemplate = extensionAPI + "WhatsApp_Templates";
var extensionHistory = extensionAPI + "WhatsApp_History";



// var emailContentText = "Hi ,\n\nHope you're well. I'm writing to know how our business can help you.\n\nPlease choose a event to schedule an appointment:\n\n${eventsUrls} See you soon!\n\n %ownerName% \n\n %companyName%";



var isAllowed = false;
var module_name = "CustomModule";
var orgId;
var userEmail = null;
var loginUserId=null;
var pluginurl = null;
var zsckey = null;
var userId = null;
var apiKey = null;
var givenUserId=null;



var peySts;
var phoneFieldsSetting={};
var fromPhoneNumber="";
var editorExtensionId = "nkihbgbbbakefbgjnokdicilnhcdeckp";

var waMsgCrdleft = 0;
var showNoCreditErrorOnWhatsAppSend = false;

	

	

		var webOrDesktop ="web";

		var extensionVersion = null;
		var attachmentId = new Date().getTime();
		var isAttachmentAdded = false;
		var attPur = false;

		
		var convertedToNewPlan = false;
		var convertedToNewPlanFetched = false;

		

		var fileUploadComplete = true;

		


document.addEventListener("DOMContentLoaded", function(event) {    


	 	
        	
	ZOHO.embeddedApp.on("PageLoad", async function(record) {        		

       	recordIds = record.EntityId;
       	module = record.Entity;
       	ButtonPosition = record.ButtonPosition;               	

		if(module != extensionTemplate && !module.includes('CustomModule')) {


				popupSizeSet(800, 575);
				if(ButtonPosition == "DetailView" || ButtonPosition == "ListViewEachRecord") {
					isBulk = false;
				}			
				else {
					isBulk = true;
				}

				if(window.location.ancestorOrigins && window.location.ancestorOrigins[0]) {

					let origin = window.location.ancestorOrigins[0];
					if(origin.indexOf("crmplus") != -1) {
						origin=origin.substring("https://crmplus.zoho".length);
					}
					else {
						origin=origin.substring("https://crm.zoho".length);
					}

					let getmap = {"nameSpace":"<portal_name.extension_namespace>"};
					await ZOHO.CRM.CONNECTOR.invokeAPI("crm.zapikey",getmap).then(function(data){
                   		zsckey = JSON.parse(data).response;
                   		pluginurl="https://platform.zoho"+origin+"/crm/v2/functions/whatsappforzohocrm__sendwhatsapp/actions/execute?auth_type=apikey&zapikey="+zsckey;
    				});

				}	

				await ZOHO.CRM.CONFIG.getOrgInfo().then(function(data){
               		orgId = data.org[0].zgid;
				});

				await ZOHO.CRM.CONFIG.getCurrentUser().then(function(data){
               		userEmail = data.users[0].email;
					loginUserId = data.users[0].id;
				});
			 
				await ZOHO.CRM.META.getModules().then(function(modulesData) {
               		modulesData.modules.forEach(function(moduled) {
               			if(moduled.api_name == extensionHistory){
               				module_name = moduled.module_name;
               			}
               		});
               	});	

               	await executeFirebaseThings();	
                   	
                    
               	ZOHO.CRM.API.getOrgVariable("whatsappforzohocrm__mobileNumberSettings").then(function(phoneFieldsData){
					if(phoneFieldsData && phoneFieldsData.Success && phoneFieldsData.Success.Content){
						phoneFieldsSetting =JSON.parse(phoneFieldsData.Success.Content);
						if(phoneFieldsSetting["countryCode"]){
							if(isBulk) {
								document.getElementById("bulkCountryCode").value = phoneFieldsSetting["countryCode"];
							}
							else {
								document.getElementById("countryCode").value = phoneFieldsSetting["countryCode"];
							}
        				}
					}
				});
               			
                
				Promise.all([ZOHO.CRM.API.getOrgVariable("whatsappforzohocrm__userId"),ZOHO.CRM.API.getOrgVariable("whatsappforzohocrm__apikey")]).then(function(apiKeyData){
                    userId = apiKeyData[0];
                    apiKey = apiKeyData[1];
                    if(apiKey && apiKey.Success && apiKey.Success.Content && userId && userId.Success && userId.Success.Content){
                    	userId = userId.Success.Content;
                    	apiKey = apiKey.Success.Content;
                    	givenUserId = userId;
                    }
                    else {
                    	document.getElementById("loader").style.display= "none";
						wcConfirm(`Please enter your ${extensionName} credential in extension settings page.`,'','Okay',true,true);
                    }
                });

				await credentialDetailsGet();


			if(window.location.href.indexOf("https://ulgebra-cdn-cicd-crm-msgbrd.gitlab.io") == -1 && window.location.href.indexOf("http://localhost") == -1){
			     showNewUpdateLink();
			     return;
			}
			
			if(!!window.chrome){
				showForcedAlert();
			}

		    //showAnnounceMent('install_chrome_extension'); 


		    let templateModules = '<li class="templateItem senderItem"  onclick="selectModuleCreate('+"'"+module+"'"+')">'+module+'</li>';
		    $('#templateListCreate').append(templateModules);
		    $("#fileUploaderCreate").show();
				
			await selectModuleCreate(module);


		}
		else {

			popupSizeSet(720, 520);
			messageContentBoxSet();

			$('.createTemplateOuterDiv').show();
			$('#editNumberui').hide();
			$('#smsTypeSetting').hide();
			$('.templateFieldDiv').hide();			
			$('#templateNotSelect').hide();
			$('.editable').attr('contenteditable', 'true');
			$('.footer').html(`<div class="send" id="send" onClick="saveTemplate()">Save Template</div>`);
			$('.developedByExtension').css({'top': '-25px'});

			getFields('Users').then(function(fields) {
				fields.forEach(function(field){
					addListItem("dropdown-menu-user",field.field_label,"dropdown-item","Users."+field.field_label);
				});
		    });

		    getFields(extensionHistory).then(function(fields) {
				let templateModules ="";
				fields.forEach(function(field){
					if(field.data_type == "lookup" && field.field_label != 'Account'){
						templateModules = templateModules +`<li class="templateItem"  onclick="selectModuleTemp('${field.lookup.module.api_name}')">${field.lookup.module.api_name}</li>`;
					}
				});
				$('#createTemplateList').append(templateModules);
				$('#createTemplateList li').first().click();
		    });

			if(recordIds && recordIds.length){
               	if(typeof(recordIds) == 'string')
				tempRecordId = recordIds;
				else
               	tempRecordId = recordIds[0];
               	if(ButtonPosition == "DetailView" || ButtonPosition == "ListViewEachRecord" || ButtonPosition == "EditView"){
               		getRecord(extensionTemplate, tempRecordId).then(function(data){
               			data = data.data[0];
					  	document.getElementById("templateName").value = data[extensionFieldName];
						document.getElementById("emailContentEmail").innerText = data[extensionFieldMessage];
						selectModuleTemp(data[extensionFieldModule]);
						document.getElementById("loader").style.display= "none";

					});
				}
				else{
					setTimeout(function() { document.getElementById("loader").style.display= "none"; }, 1000);
				}
			}	
			else{
				setTimeout(function() { document.getElementById("loader").style.display= "none"; }, 1000);
			}	
		}

		
    });

    ZOHO.embeddedApp.init().then();

});


function messageContentBoxSet() {
	
	let selectedDiv = 'emailContentEmail';

	msgTextMaxLength = 2000;

	editDivPasteToTextFunc(selectedDiv, msgTextMaxLength);
	ontypingToSizeCheck(selectedDiv, msgTextMaxLength);
	editDivPasteToNumberFunc('editNumber');

	editDivPasteToTextFunc("emailContentEmailCreate", msgTextMaxLength);
	ontypingToSizeCheck("emailContentEmailCreate", msgTextMaxLength);

	  
}

function ontypingToSizeCheck(selected, max) {
	$('#'+selected).on('keyup, keydown', (e) => {
		check_charcount(selected, max, e);
	});
}

function editDivPasteToTextFunc(selected, max) {

	$('#'+selected).on('paste', (e) => {

		if(e.originalEvent.clipboardData) {

			let data = e.originalEvent.clipboardData.getData('text/html') || e.originalEvent.clipboardData.getData('text/plain');

			let regex = /<(?!(\/\s*)?()[>,\s])([^>])*>/g;
			data = data.replace(regex, '');

			let size = Number($('#'+selected).text().length) + Number(data.length);

			if(size > max) {
				wcConfirm(`Message should be within ${max} characters.`,'','Okay',true,false);
				e.preventDefault();
			}
			else {
				document.execCommand('insertHTML', false, data);
				e.preventDefault();
			}
			
		}	  

	});

	$('#'+selected).on('drop', (e) => {    

        if(e.originalEvent.dataTransfer) {

            let data = e.originalEvent.dataTransfer.getData('text/html') || e.originalEvent.dataTransfer.getData('text/plain');

            let regex = /<(?!(\/\s*)?()[>,\s])([^>])*>/g;
            data = data.replace(regex, '');

            let size = Number($('#'+selected).text().length) + Number(data.length);

			if(size > max) {
				wcConfirm(`Message should be within ${max} characters.`,'','Okay',true,false);
				e.preventDefault();
			}
			else {
				$('#'+selected).text($('#'+selected).text()+data);
				e.preventDefault();
			}
            
        } 

    });

}

function editDivPasteToNumberFunc(selected) {

	$('#'+selected).on('paste', (e) => {

		if(e.originalEvent.clipboardData) {

			let data = e.originalEvent.clipboardData.getData('text/html') || e.originalEvent.clipboardData.getData('text/plain');

			let regex = /<(?!(\/\s*)?()[>,\s])([^>])*>/g;
			data = data.replace(regex, '').replace(/\D/g,'');

			let size = Number($('#'+selected).val().length) + Number(data.length);

			if(size > 15) {
				wcConfirm('Invalid Number.','','Okay',true,false);
				e.preventDefault();
			}
			else {
				document.execCommand('insertHTML', false, data);
				e.preventDefault();
			}
			
		}	  

	});


	$('#'+selected).on('drop', (e) => {    

        if(e.originalEvent.dataTransfer) {

            let data = e.originalEvent.dataTransfer.getData('text/html') || e.originalEvent.dataTransfer.getData('text/plain');

            let regex = /<(?!(\/\s*)?()[>,\s])([^>])*>/g;
            data = data.replace(regex, '').replace(/\D/g,'');

            let size = Number($('#'+selected).val().length) + Number(data.length);

			if(size > 15) {
				wcConfirm(`Invalid Number.`,'','Okay',true,false);
				e.preventDefault();
			}
			else {
				$('#'+selected).val($('#'+selected).val()+data);
				e.preventDefault();
			}
            
        } 

    });


}

function check_charcount(content_id, max, e) {   
	if(e.which != 46 && e.which != 8 && $('#'+content_id).text().length > max) {
		wcConfirm(`Message should be within ${max} characters.`,'','Okay',true,false);
		e.preventDefault();
	}
}

function onKeypressNumberField(selected, e) {
    if (isNaN(String.fromCharCode(e.keyCode))) {
        if(!Number(String(e.key)))
        e.preventDefault();
    }
    else if (e.keyCode == 13) {
        e.preventDefault();
        return false;
    }
    else if (e.keyCode == 32) {
        e.preventDefault();
        return false;
    }
    else if (e.keyCode == 45) {
        e.preventDefault();
        return false;
    }
    else if (e.keyCode == 46) {
        e.preventDefault();
        return false;
    }
    else if($(selected).val().length > 14) {
        e.preventDefault();
        return false;
    }
    else if(true){

        return true;

    }

    if($(selected).val().length==15) {
        e.preventDefault();
        return false;
    }

}

function onKeyupNumberField(e) {

    if (e.which == 8 || e.which == 46) {
        $('.countryCodeHintShowBulk').text('* with Country Code');
    }

}



function popupSizeSet(width, height) {
	ZOHO.CRM.UI.Resize({height:height,width:width}).then(function(data){
		// console.log(data);
	});
}

async function getFields(entity) {
	return await ZOHO.CRM.META.getFields({"Entity":entity}).then(function(fields){
		return fields.fields;
	});
}

async function getRecord(entity, recordIds) {

	return await ZOHO.CRM.API.getRecord({Entity:entity,RecordID:recordIds}).then(function(data){
		return data;
	});	

}

async function createRecord(entity, req_data) {

	return await ZOHO.CRM.API.insertRecord({Entity:entity,APIData:req_data,Trigger:["workflow"]}).then(function(response) {
		let responseInfo	= response.data[0];
		let resCode			= responseInfo.code;
		if(resCode == 'SUCCESS'){
			return responseInfo.details.id;
		}
		else{
			return false;
		}
	});

}

async function updateRecord(entity, req_data) {

	return await ZOHO.CRM.API.updateRecord({Entity:entity,APIData:req_data,Trigger:["workflow"]}).then(function(response) {
		let responseInfo	= response.data[0];
		let resCode			= responseInfo.code;
		if(resCode == 'SUCCESS'){
			return responseInfo.details.id;
		}
		else{
			return false;
		}
	});

}


function credentialValuesSet(credential, smsRoute) {
	if(credential != {} && credential.apikey && credential.senderId) {

		let SMScredential_url = {url:`https://api.msg91.com/api/balance.php?authkey=${credential.apikey}&type=${smsRoute}`};
		zohoHttpRequest('get', SMScredential_url).then(function(SMScredts) {

			if(SMScredts && !Number.isNaN(Number(SMScredts))) {

				SMScredential = Number(SMScredts);
				$('.creditsOfextension').text(SMScredential);
				$('.creditsOfextensionDiv').show();
					
			}
			else {

				$('.creditsOfextensionDiv').remove();
				wcConfirm(`<div style="width:100%;"><div>${extensionName} API Key is invalid. Kindly update in extension settings.</div><div class="wcConfirmButt"><a style=" text-decoration: auto; position: relative; left: 23px; top: 18px; " class="wcConfirmYes" href="https://apps.ulgebra.com/contact" target="_blank">Ask help</a></div></div>`,'','Okay',true,true);

			}

			$('#loader').hide();
							
		}); 

		let SMS_user_url = {url:`https://api.msg91.com/api/profile.php?authkey=${credential.apikey}`};
		zohoHttpRequest('get', SMS_user_url).then(function(SMSuser){

			if(SMSuser && SMSuser.user_fname) {
				$('.userOfextensionSpan').text(SMSuser.user_fname+' '+SMSuser.user_lname);
				$('.sms_userDetaildivision').show();						
			}
			else
			$('.sms_userDetaildivision').remove();			
							
		}); 

	}
	else {
		$('.extensionCredentialMain').remove();
	}
}

async function invokeConnector(ApiName, body) {

    return await ZOHO.CRM.CONNECTOR.invokeAPI(extensionConnector+"."+ApiName, body).then(function(data) {

        if(data && data.response)
        return JSON.parse(data.response);
        else
        return false;

    });

}

async function isAuthorizedToConnector() {
    
    return await ZOHO.CRM.CONNECTOR.isConnectorAuthorized(extensionConnector).then(function(data) {
        
    	if(data && data != "true")
        return false;
        else
        return true;

    });

}

async function authorizedToConnector() {

    return await ZOHO.CRM.CONNECTOR.authorize(extensionConnector).then(function(data) {
        
        if(data && data != "true")
        return false;
        else
        return true;
        
    });

}

async function newAuthorizedToConnector() {

	await authorizedToConnector().then(function(Authorized) {

		isAuthorized = Authorized;
		if(!Authorized)
		window.location.reload();
		else {
			$('#templateNotSelect').hide();
			smsChannelChoose();
			singleBulkMsgSelect(recordIds);
		}

	});

}

async function credentialDetailsGet() {

	


	// ZOHO.CRM.API.getOrgVariable(extensionCredential).then(function(apiKeyData){
    		
	// 	if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content && apiKeyData.Success.Content != "0" && apiKeyData.Success.Content != "null"){
	// 		credential = apiKeyData.Success.Content;
	// 		credential = JSON.parse(credential);
				
 //        	if(credential == {} || !credential.apikey || !credential.senderId){
 //        		document.getElementById("loader").style.display= "none";
	// 			wcConfirm(`Please enter your ${extensionName} credential in extension settings page.`,'','Okay',true,true);
	// 		}
	// 		else {
				messageContentBoxSet();
				// credentialValuesSet(credential, smsRoute);
				// smsChannelChoose(credential);
				scheduleTimeSet();
				getAllUsers();
				historyFieldsGet();
               	thisModuleTemplateSet();
               	singleBulkMsgSelect(recordIds);
	// 		}
	// 	}
	// 	else{
	// 		document.getElementById("loader").style.display= "none";		
 //        	wcConfirm(`Please enter your ${extensionName} credential in extension settings page.`,'','Okay',true,true);
	// 	}
	// });


}

async function smsChannelChoose() {

	if(isAuthorized) {

		let APIName = 'phonenumbers';
		let data = { path : 'account/~/phone-number?usageType=MainCompanyNumber&usageType=AdditionalCompanyNumber&usageType=CompanyNumber&usageType=DirectNumber&usageType=CompanyFaxNumber&usageType=ForwardedNumber&usageType=ForwardedCompanyNumber&usageType=ContactCenterNumber&usageType=ConferencingNumber&usageType=MeetingsNumber&usageType=BusinessMobileNumber&usageType=IntegrationNumber&status=Normal' };

		await invokeConnector(APIName, data).then(function(resp) {

			if(resp && resp.records && resp.records.length) {

				let phones = resp.records;
				let phoneList = '';
				phones.forEach(function(phoneObj) {

					phoneList = phoneList + `<li class="templateItem senderItem" onclick="selectedSenderId(this)" from="${phoneObj.phoneNumber.replace(/\D/g,'')}">${phoneObj.phoneNumber}</li>`;
					
				});
				$('#senderList').append(phoneList);

			}
			else
			$('#senderList').append('<li style="padding: 4px 15px;box-sizing: border-box;cursor:default;">No Numbers</li>');

		});

		$('#senderList li').first().click();

	}
	
}

function selectedSenderIdGlob(selected) {

	extensionAllowedAPI = false;
	flowId = '';

	$('.editable').attr('contenteditable', 'true').attr('placeholder', 'Type here..');
	$('.templateFieldDiv').show();
	$('.userFieldDiv').show();
	$('.moduleFieldDiv').show();
	$('.messageMethodOut').attr('onclick', 'chooseMsgTypeFunc(event)');
	$('.voiceButtMsg').css({'cursor': 'pointer', 'background-color': '#ffffff', 'color': '#a0a0a0'});

	$('.editable').html('');
	document.getElementById("selectedTemplateList").innerText = $(selected).text();

	$('details').removeAttr("open");

}

function selectedSenderId(selected){
			
	document.getElementById("selectedSender").innerText = $(selected).text();
	smsFrom = $(selected).attr('from');

	$('details').removeAttr("open");

}

async function currentUserGet() {
	return await ZOHO.CRM.CONFIG.getCurrentUser().then(function(data){
		return data.users[0].id;
	});

}

async function historyFieldsGet() {
	historyFields = await getFields(extensionHistory);
}

async function getAllUsers() {

	ZOHO.CRM.API.getAllUsers({Type:"AllUsers"}).then(async function(data){

	    let users = data.users;

	    await currentUserGet().then(async function(currentLoginUser) {

	    	if(users.length) {
		    	$('#dropdown-menu-user').append(`<li class="userListHead">Users</li>`);
		    	let userCount = 0;
		    	users.forEach(async function(user) {

		    		if(user.status == "active")
			    	$('#dropdown-menu-user').append(`<li class="selectedUserList${user.role.name == 'CEO' ? ' selectedOwnerUser': ''}" userId="${user.id}" onclick="userSelectFunction(this);"><span class="selectedUserTick" style="display:none;">&#10003;</span>${user.role.name == 'CEO' ? currentLoginUser == user.id ? 'Owner/Current User' : 'Owner' : currentLoginUser == user.id ? 'Current User' : user.full_name}</li>`);
	
			    	if(userCount++ == users.length-1) {
			    		await getFields('Users').then(function(fields) {
			    			userFields = fields;
					    	$('.selectedOwnerUser').click();
					    	$('#dropdown-menu-user').append(`<li class="userListHead">User Fields</li>`);
							userFields.forEach(function(field){
								addListItem("dropdown-menu-user",field.field_label,"dropdown-item","Users."+field.field_label);
								addListItemCreate("dropdown-menu-userCreate",field.field_label,"dropdown-item","Users."+field.field_label);
							});
							$('#dropdown-menu-user').find('.dropdown_Butt').css({'padding-left': '20px'});
					    });
			    	}

			    });
		    }
		    else {
		    	$('.userFieldDiv').remove();
		    }	

	    });	

	});
}

function userSelectFunction(selected) {
	$('.selectedUserList').find('.selectedUserTick').hide();
	$('.selectedUserList').removeClass('userSelectedHover');
	$(selected).find('.selectedUserTick').show();
	$(selected).addClass('userSelectedHover');
	getRecord('users', $(selected).attr('userId')).then(function(data) {
		selectedUser = data.users[0];
	});
}

function addListItem(id,text,className,value){

	let linode = '<li class="'+className+'"><button class="dropdown_Butt" onclick="insert(this)">'+text+'<input type="hidden" value="'+value+'"></button></li>';
	$('#'+id).append(linode);

}

async function searchRecord(entity, query) {

	return await ZOHO.CRM.API.searchRecord({Entity:entity,Type:"criteria",Query:query,delay:"false"}).then(function(response) {
		return response.data;
	});

}

function thisModuleTemplateSet() {
	
	searchRecord(extensionTemplate, `(${extensionFieldModuleForTemp}:equals:${module})`).then(function(resp) {			

		let templateList="";
		if(resp) {

			resp.forEach(function(searchField) {
	      		
	      		smsTemplates[searchField.id] = searchField;
	            templateList = templateList + `<li class="templateItem" onclick="showsms(this)" recordId="${getSafeString(searchField.id)}"><span class="templateItemSpanName">${getSafeString(searchField.Name)}</span></li>`;

	        });
			
			if(templateList == "")
			$('#templateList').append('<li style="padding: 3px 15px;cursor: default;box-sizing: border-box;color: #777777;">No Templates</li>');
			else
			$('#templateList').append(templateList);
		
		}
		else{
			$('#templateList').append('<li style="padding: 3px 15px;cursor: default;box-sizing: border-box;color: #777777;">No Templates</li>');
		}
	});

}


function singleBulkMsgSelect(recordIds) {
	getRecord(module, recordIds).then(function(record){	
		currentRecords = record.data;
		if(!isBulk){
			document.getElementById("loader").style.display= "none";
			selectModule(currentRecords[0]);
		}
		else{
			$('#editNumberui').hide();
			$('#bulksettings').show();
			document.getElementById("loader").style.display= "none";
			selectModuleforBulk(currentRecords);
		}	
	});
}

function templateModuleChangeCheck(templateModule, smsContent) {

	let othermodule=false;
	if(templateModule == "Leads" && (smsContent.indexOf("${Contacts.") != -1 || smsContent.indexOf("${Accounts.") != -1 || smsContent.indexOf("${Deals.") != -1 )){
		othermodule=true;
	}
	else if(templateModule == "Contacts" && (smsContent.indexOf("${Leads.") != -1 || smsContent.indexOf("${Accounts.") != -1 || smsContent.indexOf("${Deals.") != -1 )){
		othermodule=true;
	}
	else if(templateModule == "Accounts" && (smsContent.indexOf("${Contacts.") != -1 || smsContent.indexOf("${Leads.") != -1 || smsContent.indexOf("${Deals.") != -1 )){
		othermodule=true;
	}
	else if(templateModule == "Deals" && (smsContent.indexOf("${Contacts.") != -1 || smsContent.indexOf("${Accounts.") != -1 || smsContent.indexOf("${Leads.") != -1 )){
		othermodule=true;
	}

	return othermodule;

}

function selectModuleTemp(selectModule) {

	document.getElementById("selectedmodule").innerText = selectModule;
	document.getElementById("thisModuleName").innerText = "Insert "+selectModule+" Fields";

	let smsContent = document.getElementById("emailContentEmail").innerText;
	
	if(templateModuleChangeCheck(selectModule, smsContent))
	document.getElementById("emailContentEmail").innerHTML ="";

	document.getElementById("dropdown-menu-email").innerHTML="";
	getFields(selectModule).then(function(fields){
		fields.forEach(function(field){
			addListItem("dropdown-menu-email",field.field_label,"dropdown-item",selectModule+"."+field.field_label);
		})
	});	

	$('details').removeAttr("open"); 

}

function saveTemplate() {

	let name = document.getElementById("templateName").value;
	let templateModule = document.getElementById("selectedmodule").innerText;
	let smsContent = document.getElementById("emailContentEmail").innerText.trim();
	
	if(name == ""){
		wcConfirm('Template Name cannot be empty.','','Okay',true,false);
		return ;
	}
	else if(templateModule == ""){
		wcConfirm('Please Choose Module.','','Okay',true,false);
		return ;
	}
	else if( smsContent == ""){
		wcConfirm('Message cannot be empty.','','Okay',true,false);
		return;
	}
	else if(templateModuleChangeCheck(templateModule, smsContent)){
		wcConfirm('Message Contains Other Modules Merge Fields.Please change it.','','Okay',true,false);
		return ;
	}
	else{

    	wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingLoaderInner">${loader}</div><div class="sendingLoaderHint">SMS Template is saving...</div></div>`,'','Okay',true,true);
    	
    	if(ButtonPosition != 'CreateOrCloneView' && ButtonPosition != 'ListView' && ButtonPosition != 'ListViewWithoutRecord'){

			let req_data = {"id":tempRecordId};
			req_data[extensionFieldName] = name;
			req_data[extensionFieldMessage] = smsContent;
			req_data[extensionFieldModuleForTemp] = templateModule;
			updateRecord(extensionTemplate, req_data).then(function(resp){

				let tempStatus = "Your SMS Template is updated successfully.";
				if(resp) {
					wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingMsgSuccess">${verifiedTickSVG}</div><div class="sendingMsgSuccessHint">${tempStatus}</div></div>`,'','Okay',true,true);
					setTimeout(function() {	
						ZOHO.CRM.UI.Record.open({Entity:extensionTemplate,RecordID:resp}).then(function(data){
							popupCloseFunc();
						});	
					}, 1000);
				}
				else
				setTimeout(function() {	wcConfirm('Opps! Something went wrong from server side. Please try after sometimes!!!','','Okay',true,false); }, 1500); 

			});	

    	}
    	else{

    		let req_data = {};
			req_data[extensionFieldName] = name;
			req_data[extensionFieldMessage] = smsContent;
			req_data[extensionFieldModuleForTemp] = templateModule;
			createRecord(extensionTemplate, req_data).then(function(resp){

				let tempStatus = "Your SMS Template is saved successfully.";
				if(resp) {

					wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingMsgSuccess">${verifiedTickSVG}</div><div class="sendingMsgSuccessHint">${tempStatus}</div></div>`,'','Okay',true,true);
					setTimeout(function() {	
						ZOHO.CRM.UI.Record.open({Entity:extensionTemplate,RecordID:resp}).then(function(data){
							popupCloseFunc();
						});	
					}, 1000);
				}
				else
				setTimeout(function() {	wcConfirm('Opps! Something went wrong from server side. Please try after sometimes!!!','','Okay',true,false); }, 1500); 

			});	

		}	
	}		
}



function selectModuleforBulk(){
	
	document.getElementById("thisModuleName").innerText = "Insert "+module+" Fields";
	document.getElementById("dropdown-menu-email").innerHTML="";
	document.getElementById("bulkNumberList").innerHTML="";
	getFields(module).then(function(fields){			
		
		moduleFields = fields;
		
		let NumberList= '';
        moduleFields.forEach(function(field) {
            if(field.data_type == "phone"){
                NumberList = NumberList+ `<li class="templateItem" onclick="savePhoneFields(this)" entity="${module}" api_name="${field.api_name}">${field.field_label}</li>`;
            }
            addListItem("dropdown-menu-email",field.field_label,"dropdown-item",module+"."+field.field_label);
        });
        if(module == "Deals"){
            let contactphoneList = ["Mobile","Phone","Home Phone","Other Phone","Ass Phone"];
            contactphoneList.forEach(function(field){
                NumberList = NumberList+ `<li class="templateItem" onclick="savePhoneFields(this)" entity="${'Contacts'}" api_name="${field.replace(" ","_")}">Contact - ${field}</li>`;
            })
            NumberList = NumberList+ `<li class="templateItem" onclick="savePhoneFields(this)" entity="${'Accounts'}" api_name="Phone">Account - Phone</li>`;
        }
        $('#bulkNumberList').append(NumberList); 
        $('#bulkNumberList li').first().click();

	});	

}

async function selectModule(record){

	document.getElementById("thisModuleName").innerText = "Insert "+module+" Fields";
	document.getElementById("dropdown-menu-email").innerHTML="";
	document.getElementById("NumberList").innerHTML="";

	await getFields(module).then(function(fields){
		
		moduleFields = fields;
		
		let NumberList = "";
		let lookupModules = [];
		let errText = "";
		moduleFields.forEach(function(field){
			
			if(field.data_type == "phone") {
				errText = errText + field.field_label + '/';
				if(record[field.api_name] != null)
				NumberList = NumberList+ `<li class="templateItem" onclick="setNumber(this)" entity="${module}" api_name="${field.api_name}" num="${record[field.api_name]}">${field.field_label}</li>`;
			}
			else if(field.data_type == "lookup"){
				if(record[field.api_name] != null)
				lookupModules.push(field);
			}	

			addListItem("dropdown-menu-email",field.field_label,"dropdown-item",module+"."+field.field_label);
			
		});	

		if(lookupModules.length == 0) {
			if(NumberList == ""){
				$('#NumberList').append('<li class="noNumberListStyle">No Numbers</li>');
				wcConfirm(`<div><span style="font-weight: 600;font-size: 14px;">${errText.slice(0, -1)}</span> fields is empty.</div>`,'','Okay',true,false);
			}
			else{
				$('#NumberList').append(NumberList);
				$('#NumberList li').first().click();
			}
			
			document.getElementById("loader").style.display= "none";
		}	
		else{

			for (let i = 0; i < lookupModules.length; i++) {
				let lookupModule = lookupModules[i].lookup.module.api_name;
				let lookupId = record[lookupModules[i].api_name].id;
				getFields(lookupModule).then(function(respFields) {
					getRecord(lookupModule, lookupId).then(function(datarecord) {
						
						datarecord = datarecord.data[0];
						respFields.forEach(function(field){

							if(field.data_type == "phone") {
								errText = errText + lookupModule + ' - ' +field.field_label + '/';
								if(datarecord[field.api_name] != null)
								NumberList = NumberList + `<li class="templateItem" onclick="setNumber(this)" entity="${lookupModule}" api_name="${field.api_name}" num="${datarecord[field.api_name]}">${lookupModules[i].lookup.module.api_name} - ${field.field_label}</li>`;
							}		

						});

						if(i == lookupModules.length-1) {
							if(NumberList == "") {
								$('#NumberList').append('<li class="noNumberListStyle">No Numbers</li>');
								wcConfirm(`<div><span style="font-weight: 600;font-size: 14px;">${errText.slice(0, -1)}</span> fields is empty.</div>`,'','Okay',true,false);
							}
							else {
								$('#NumberList').append(NumberList);
								$('#NumberList li').first().click();
							}
							
							document.getElementById("loader").style.display= "none";
						}

					});

				});
			}

		}	

	});	

}

function setNumber(selected){

	document.getElementById("editNumber").value = "";
	$('#numberLoader').html(dotCircleLoader).show();

    phoneRecord = { entity: $(selected).attr('entity'), api_name: $(selected).attr('api_name') };

	let selectedNumber = $(selected).attr('num').replace(/\D/g,'');
	if(selectedNumber) {
		checkPhoneNumber(selectedNumber).then(function(resp) {

			if(resp && resp.countryPrefix != null) {
	    		let phoneNumber = resp.phoneNumber.toString();
	    		let countryCode = resp.countryPrefix.toString();
	    		no = phoneNumber.substring(phoneNumber.indexOf(countryCode) + countryCode.length);

	    		document.getElementById("countryCode").value = countryCode;
	    		$('.selectedCountryCodeShow').text(countryCode);

	    		document.getElementById("editNumber").value = no;
				$('#numberLoader').html("").hide();
	    	}
	    	else {
	    		document.getElementById("editNumber").value = selectedNumber;
				$('#numberLoader').html("").hide();
	    	}	

		});
	}
			
	document.getElementById("selectedNumber").innerText =  $(selected).text();			
	$('details').removeAttr("open");

}

function selectCountryCode(selected) {
	if(!isBulk)
	$('.selectedCountryCodeShow').text($(selected).val());
	saveCountryCode($(selected).val());
}

function checkPhoneNumber(no){

	let request ={
		url : "https://rest.messagebird.com/lookup/" + no.replace(/\D/g,''),
		headers:{
			Authorization:"AccessKey 7NMPor0R8DofSHH61SpViNNqQ",
		}
	}
	return zohoHttpRequest('get', request).then(function(resp) {

		if(resp.error)
		return null;
		else
		return resp;
		
	}); 

}

function savePhoneFields(selected) {
    phoneRecord = { 'entity': $(selected).attr('entity'), api_name: $(selected).attr('api_name') };
    document.getElementById("bulkSelectedNumber").innerText = $(selected).text();
    $('details').removeAttr("open");  
}
        
// function showsms(selected) {
	
// 	document.getElementById("selectedTemplate").innerText = $(selected).find('.templateItemSpanName').text();		
// 	document.getElementById("emailContentEmail").innerText = $(selected).find('.templateItemSpan').text();
// 	$('#templateNotSelect').hide();
// 	$('details').removeAttr("open");

// }	

var smsTemplates = {};
function showsms(selected) {

	let templateId = $(selected).attr('recordId');
	document.getElementById("selectedTemplate").innerText = smsTemplates[templateId].Name;	
	document.getElementById("emailContentEmail").innerText = smsTemplates[templateId].whatsappforzohocrm__WhatsApp_Message;
	if(smsTemplates[templateId].whatsappforzohocrm__Media){
		let mediaObj = JSON.parse(smsTemplates[templateId].whatsappforzohocrm__Media);
		attachedfile = {"mediaUrl":mediaObj.url,"fileMeta":mediaObj};
		$('#attachedfile').html(`${mediaObj.name}`);
	}
	else{
		attachedfile = {};
		$('#attachedfile').html(`Attach file`);
	}
	$('#templateNotSelect').hide();
	$('details').removeAttr("open");
}

var sendBulkWhatsAppList={};
var sendBtnClickedForProceed = false;
async function sendSMS(){


	if(!chosenWASenderId){
		sendBtnClickedForProceed = true;
		$('.fromNumChooserOuter').fadeIn();
		return;
	}
	if(chosenWASenderId!=="whatsapp-web"){
		if(!fileUploadComplete){
			alert("Attached file is uploading, Please wait...");
			return;
		}
		//sendWhatcetra();
		//return;
	}
	if(showNoCreditErrorOnWhatsAppSend){
		document.write(`You have sent all messages which was allowed in this plan. Kindly request more credits to continue. <a href="https://apps.ulgebra.com/contact" target="_blank">Contact Now</a> <br><br> <a href="https://apps.ulgebra.com/zoho/crm/whatsapp-web/pricing?src=crmwabulkpop#h.xi7gibhrktbj" target="_blank">View Pricing Plans</a> `);
	}
	if (isAllowed && extensionVersion && parseFloat(extensionVersion.version) > 11) {
    	var state = await checkState(sendSMS);
        if(!state){
            return;
        }
    } 
	
	
	let country_code = $('#countryCode').find(":selected").val();
	let bulkCountry_code = $('#bulkCountryCode').find(":selected").val();
	let MobileNumber = document.getElementById("editNumber").value;
	let message = document.getElementById("emailContentEmail").innerText.trim();	

	if(message == "") {
		wcConfirm('Message cannot be empty.','','Okay',true,false);
		return false;
	}  
	else if(country_code == "" && !isBulk) {
		wcConfirm('Select countryCode.','','Okay',true,false);
		return false;
	}    	
	else if(bulkCountry_code == "" && isBulk) {
		wcConfirm('Select countryCode.','','Okay',true,false);
		return false;
	}
	else if(message.length > msgTextMaxLength){
		wcConfirm(`Message should be within ${msgTextMaxLength} characters.`,'','Okay',true,false);
		return false;
	}
	else if(scheduledTime && new Date(scheduledTime).getTime() < new Date().getTime()){
		wcConfirm('Schedule time should be in future.','','Okay',true,false);
		return false;
	}
	else if(!isBulk && (MobileNumber == null || MobileNumber == "")){
        wcConfirm('Mobile field is empty.','','Okay',true,false);
        return false;
	}
	// else if(smsFrom == "" || smsFrom == null || smsFrom == undefined) {
	// 	wcConfirm('Select From Number.','','Okay',true,false);
 //        return false;
	// }
	else{

		let countryCode;
		if(isBulk) {
			countryCode = bulkCountry_code;
			wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingLoaderInner">${sendingLoader}</div><div class="sendingLoaderHint">Sending...<br>Messages will be sent only for valid numbers.</div></div>`,'','Okay',true,true);
		}
		else {
			countryCode = country_code;
			wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingLoaderInner">${sendingLoader}</div><div class="sendingLoaderHint">Sending...</div></div>`,'','Okay',true,true);
		}
		

	    for(let i=0;i<currentRecords.length;i++) {

	  //   	if(SMScredential || SMScredential == 0) {
			// 	if(Number.isNaN(Number(SMScredential))) {
			// 		document.getElementById("Error").style.display= "none";
			// 		wcConfirm(`<div style="width:100%;"><div>${extensionName} API Key is invalid. Kindly update in extension settings.</div><div class="wcConfirmButt"><a style=" text-decoration: auto; position: relative; left: 23px; top: 18px; " class="wcConfirmYes" href="https://apps.ulgebra.com/contact" target="_blank">Ask help</a></div></div>`,'','Okay',true,true);
			// 		return false;
			// 	}
			// 	else if(Number(SMScredential) <= 0) {
			// 		wcConfirm(`<div style="width:100%;"><div>You do not have ${$('#sms_creditsSelect').find('option:selected').text()}.</div><div class="wcConfirmButt"><a style=" text-decoration: auto; position: relative; left: 23px; top: 18px; " class="wcConfirmYes" href="https://control.msg91.com/payment/pay-step1.php" target="_blank">Buy Credits</a></div></div>`,'','Okay',true,true);
			// 		return false;
			// 	}
			// }

	    	let currentRecord = currentRecords[i];

	    	await getMobileNumber(currentRecord);

	    	if(!isBulk) {
	    		
	    		if(MobileNumber != phoneRecord.Mobile.replace(/\D/g,'') && countryCode+MobileNumber != phoneRecord.Mobile.replace(/\D/g,'')) {
	    			phoneRecord['Mobile'] = MobileNumber;
					phoneRecord['recipientName'] = MobileNumber;
					phoneRecord['id'] = '';
	    		}

	    	}

	    	if(phoneRecord['Mobile'] && phoneRecord['Mobile'].replace(/\D/g,'')) {

	    		let filledMessage = await getMessageWithFields(message, currentRecord);

				if(filledMessage.length > msgTextMaxLength && !isBulk)
				{
					wcConfirm('Message is Too Large.','','Okay',true,false);
					return;
				}
				else if(filledMessage.length < 1)
				{
					if(!isBulk) {
						wcConfirm('Merge Fields value is empty.','','Okay',true,false);
						return;
					}
					else {
						filledMessage = " ";
					}
				}
		        		
				let to = await checkMobileNumber(phoneRecord.Mobile, countryCode);

				if(to != phoneRecord.Mobile.replace(/\D/g,'')) {
					if(phoneRecord['recipientName'] == phoneRecord['Mobile'])				
					phoneRecord['recipientName'] = to;
	    			phoneRecord['Mobile'] = to;
	    		}
	    		
				let req_data = {};
				req_data[extensionFieldName] = `WhatsApp to ${phoneRecord.recipientName}`;
				req_data[extensionFieldMessage] = filledMessage;
				req_data[extensionFieldRecipientNumber] = phoneRecord.Mobile;
				req_data[extensionFieldSenderPhone] = fromPhoneNumber;
				req_data[extensionFieldRecipientId] = currentRecord.id;
				req_data[extensionFieldModule] = module;
				//req_data[extensionFieldDirection] = "Outbound";
				
				historyFields.forEach(function(field){
					if(field.data_type == "lookup" && phoneRecord.entity && phoneRecord.entity.includes(field.lookup.module.api_name)){
						req_data[field.api_name] = phoneRecord.id;
					}
					if(field.data_type == "lookup" && field.lookup.module.api_name == module)
					req_data[field.api_name] = currentRecord.id;
				});


				if(scheduledTime)
				{
					let time = scheduledTime.substring(0,19) + "+00:00";
					req_data[extensionFieldSchedule]=time.toString();
					req_data[extensionFieldStatus]="Scheduled";

					if(attPur && attachedfile && attachedfile.mediaUrl) {
						req_data["whatsappforzohocrm__Media_URL"]=attachedfile.mediaUrl;
					}

					let requestMap = {status:"Your WhatsApp SMS has been scheduled successfully.", count: i};
					await smsResponseToHistory(true, requestMap, req_data);

				}
		        else {

					let requestMap = { to: to, msg: filledMessage, count: i };

					await sendSMS_Request(requestMap, req_data).then(async function(response) {
						if(!response.resp && !response.resp.resp) {
							wcConfirm('Message sent is failed.','','Okay',true,false);
							return false;
						}					
						else
						await sendSMS_Response(response.resp.status, response.requestMap, response.req_data);
					}); 

				}

	    	}
	    	else {

	    		if(!isBulk) {
		        	wcConfirm('Mobile field is empty or invalid for all choosen.','','Okay',true,false);
		        	return false;
		        }
		        else {

		   //      	let filledMessage = await getMessageWithFields(message, currentRecord);

		   //      	let req_data = {};
					// req_data[extensionFieldName] = `SMS to No Number`;
					// req_data[extensionFieldMessage] = filledMessage;
					// req_data[extensionFieldContactNumber] = '';
					// req_data[extensionFieldModule] = module;
				 //    req_data[extensionFieldMsgType] = msgType;

					// historyFields.forEach(function(field){
					// 	if(field.data_type == "lookup" && phoneRecord.entity && phoneRecord.entity.includes(field.lookup.module.api_name)){
					// 		req_data[field.api_name] = phoneRecord.id;
					// 	}
					// 	if(field.data_type == "lookup" && field.lookup.module.api_name == module)
					// 	req_data[field.api_name] = currentRecord.id;
					// });

					// req_data[extensionFieldStatus]="Mobile field is empty.";
		         	let requestMap = {status:"WhatsApp SMS to No Number", count: i};
					// await smsResponseToHistory(false, requestMap, req_data);

					popupSccuessExit(false, requestMap);

					if(currentRecords.length == i+1)
					wcConfirm('Mobile field is empty or invalid for all choosen.','','Okay',true,false);

		        }

	    	}


	    }  
	    		
	}	
}

async function smsResponseToHistory(Success, requestMap, req_data) {

	await createHistory(requestMap, req_data).then(function(response) {
		if(response.resp)
		popupSccuessExit(Success, response.requestMap);
		else
		setTimeout(function() {	wcConfirm('Opps! Something went wrong from server side. Please try after sometimes!!!','','Okay',true,false); }, 1500); 
	});

}

async function zohoHttpRequest(method, request) {


	if(method == 'get')
	return await ZOHO.CRM.HTTP.get(request).then(function(resp) {
		if(resp.includes('{') && resp.includes('}'))
     	resp = JSON.parse(resp);
     	return resp;
     	
    },function(err) {
		console.log(err);
		return null;
	});
	
	if(method == 'post')
	return await ZOHO.CRM.HTTP.post(request).then(function(resp) {
		if(resp.includes('{') && resp.includes('}'))
     	resp = JSON.parse(resp);
     	return resp;
     	
    },function(err) {
		console.log(err);
		return null;
	});
	
}

async function sendSMS_Request(requestMap, req_data) {


 
if(extensionVersion){
	if(!isAllowed){
	    showAnnounceMent('purchase_license');
	}else{
		showAnnounceMent('maintenence');
	}
}
else{
	showAnnounceMent('install_chrome_extension');
}

if(true) {

	if(chosenWASenderId!=="whatsapp-web") {

		if (isAllowed && extensionVersion) {
			

			let mediaObj = null;
	        let user = whatcetraApp.auth().currentUser;  
	        let agent = {
	            name: user.displayName,
	            email: user.email,
	            id:user.uid,
	            photoUrl:user.photoURL
	        }
	        if(attPur && attachedfile && attachedfile.mediaUrl){
	            mediaObj = {
	                "url": attachedfile.mediaUrl,
	                "name": attachedfile.fileMeta.name,
	                "type": attachedfile.fileMeta.type,
	                "size": attachedfile.fileMeta.size
	            }    
	            //attachedfile = null;
	        }   
	        
	        sendBulkWhatsAppList[new Date().getTime()]={"contactNumber":req_data.whatsappforzohocrm__Recipient_Number,"text":req_data.whatsappforzohocrm__WhatsApp_Message,"createdTime":new Date().getTime(),"agent":agent,"media":mediaObj};
	      
	        let status = await addToWhatcetraQueue(sendBulkWhatsAppList);
	        return {resp: {resp: true, status: status}, requestMap: requestMap, req_data: req_data}; 
	          

			}
			else{

				if(webOrDesktop == "web"){
				    var url = 'https://web.whatsapp.com/send?phone=' + req_data.whatsappforzohocrm__Recipient_Number + '&text=' + encodeURIComponent(req_data.whatsappforzohocrm__WhatsApp_Message);
				}
				else{
				    var url = 'https://wa.me/' + req_data.whatsappforzohocrm__Recipient_Number + '?text=' + encodeURIComponent(req_data.whatsappforzohocrm__WhatsApp_Message);
				}

				urlList = `<div class="whatsappLink">
				        <div class="contactName">${req_data.Name.substring(12)} </div>
				        <div class="sendButton" style="cursor:pointer;" onclick="sendWhatsapp(${requestMap.count})" id="${requestMap.count}_send"> Send Now</div>
				        <div class="sendButton sent" id="${requestMap.count}_sent"> Sent </div>
				        <div class="sendButton sending" id="${requestMap.count}_sending"> Sending</div>
				    </div>`

				$("#whatsAppUrlList").append(urlList);
				document.getElementById("container").style.display= "none";
				document.getElementById("whatsAppUrlList").style.display= "block";
				return {resp: {resp: true, status:`Your Message has been sent successfully in the background <br> via Chrome Extension.`}, requestMap: requestMap, req_data: req_data}; 

			}

	}	
	else {


		if(selectors && pluginurl){
	    	chrome.runtime.sendMessage(editorExtensionId,{"message":"setSelectors","s":selectors,"url":pluginurl},function (response){
				console.log(response);
				if (response.status != "success"){
				  // handleError(url);
				   console.log(response);
				}
			});
	    }

	    

		let details = Date.now();
		if(module_name && orgId){
			var url = "https://crm.zoho.com/crm/org"+orgId+"/tab/"+module_name+"/"+details;
		}
		sendBulkWhatsAppList[details+""]={"n":req_data.whatsappforzohocrm__Recipient_Number,"m":getSanitizedContent(req_data.whatsappforzohocrm__WhatsApp_Message),"u":url,"dt":new Date().getTime()};
		if(attachmentId && isAttachmentAdded) {
			sendBulkWhatsAppList[details+""]['t'] = attachmentId;
		}
		var requestJson = sendBulkWhatsAppList;
		//sendBulkWhatsAppList["queueIsStopped"]=false;
		if(attachmentId && isAttachmentAdded){
			requestJson = {"message": "getAttachments","t":attachmentId,"sendBulkWhatsAppList":sendBulkWhatsAppList};
		}
		logSUBActivityLog(1);
		$.ajax({url: "https://us-central1-wawebbulk.cloudfunctions.net/updateLICCA?src=crm-wa-bulk&email="+userEmail+"&mcc="+1, success: function(result){
			console.log(result);
		}});  
        
        chrome.runtime.sendMessage(editorExtensionId,requestJson,function (response){
            console.log(response);
            if (response.status != "success"){
              // handleError(url);
               console.log(response);
            }
        });
        
        return {resp: {resp: true, status:`Your Message has been sent successfully in the background <br> via Chrome Extension.`}, requestMap: requestMap, req_data: req_data}; 
        

	}

	
}
else {


 	var licEnabled = isAllowed;
	isAllowed = waMsgCrdleft > 0;
	if(isAllowed  && (extensionVersion || isAutomation)) {
		var details = Date.now();
		if(module_name && orgId){
			var url = "https://crm.zoho.com/crm/org"+orgId+"/tab/"+module_name+"/"+details;
		}
		sendBulkWhatsAppList[details+""]={"n":req_data.whatsappforzohocrm__Recipient_Number,"m":getSanitizedContent(req_data.whatsappforzohocrm__WhatsApp_Message),"u":url,"dt":new Date().getTime()};
		logSUBActivityLog(1);
		$.ajax({url: "https://us-central1-wawebbulk.cloudfunctions.net/updateLICCA?src=crm-wa-single&email="+userEmail+"&mcc=1", success: function(result){
			console.log(result);
		}});
		//sendBulkWhatsAppList["queueIsStopped"]=false;
		if(!isAutomation){
			chrome.runtime.sendMessage(editorExtensionId, sendBulkWhatsAppList,function(response) {
				if (!response.success){
				  //handleError(url);
				}
			});
		}
		else{
			addToQueue(sendBulkWhatsAppList);
			return {resp: {resp: true, status:``}, requestMap: requestMap, req_data: req_data}; 
		}				
		//wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingMsgSuccess">${verifiedTickSVG}</div><div class="sendingMsgSuccessHint"></div></div>`,'','Okay',true,true);
		//setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload(); }, 2500);
		return {resp: {resp: true, status:`Your Message has been sent successfully in the background <br> via Chrome Extension ${licEnabled ? '': '(7 days free trial enabled)'}.`}, requestMap: requestMap, req_data: req_data}; 

	}
	else{
		if(!reply){
			loadDebugInfo(":no-chrome-ext::uasr:"+navigator.userAgent);
		}

		let Mobile = requestMap.to.replace(/\D/g,'');
		if(document.getElementById("web").checked){
			var url = "https://web.whatsapp.com/send?phone=" + Mobile + "&text=" + encodeURIComponent(requestMap.msg);
		}
		else{
			var url = "https://wa.me/" + Mobile + "?text=" + encodeURIComponent(requestMap.msg);
		}
		window.open(url,"_blank");
		//setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload(); }, 1000);
		return {resp: {resp: true, status:`Your Message has been sent successfully in the background <br> via Chrome Extension ${licEnabled ? '': '(10 days free trial enabled)'}.`}, requestMap: requestMap, req_data: req_data}; 
	}

}



}	

function popupCloseFunc() {
	ZOHO.CRM.UI.Popup.closeReload();
}

function popupSccuessExit(Success, requestMap) {

	requestMap['single_status'] = "Your SMS has been sent successfully.";
	requestMap['bulk_status'] = "Your Bulk SMS has been sent successfully.";
	if(!requestMap['status'])
	requestMap['status'] = 'Message sent is failed.';

	if(!isBulk) {
		if(!Success)
		setTimeout(function() {	wcConfirm(requestMap.status,'','Okay',true,false); }, 1500); 
		else {
			wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingMsgSuccess">${verifiedTickSVG}</div><div class="sendingMsgSuccessHint">${requestMap.status}</div></div>`,'','Okay',true,true);
			setTimeout(function(){ popupCloseFunc(); }, 1500);
		}
	}
	else {

		if(currentRecords.length-1 == requestMap.count) {
			wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingMsgSuccess">${verifiedTickSVG}</div><div class="sendingMsgSuccessHint">${requestMap.bulk_status}</div></div>`,'','Okay',true,true);
			setTimeout(function() {	popupCloseFunc(); }, 1500);	
		}
		// else {
		// 	if(!Success)
		// 	wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingLoaderInner">${sendingLoader}</div><div class="sendingLoaderHint">${'Message sent for this '+phoneRecord.recipientName}</div></div>`,'','Okay',true,true); 
		// 	else
		// 	wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingLoaderInner">${sendingLoader}</div><div class="sendingLoaderHint">${'Message sent for this '+module+'.'}</div></div>`,'','Okay',true,true);
		// }
		
	}

}

function sendSMS_Response(resp, requestMap, req_data) {
	
	sendSMS_Response_smsChannel(resp, requestMap, req_data);

}

function sendSMS_Response_smsChannel(resp, requestMap, req_data) {

	if(resp) {
		

		req_data[extensionFieldStatus] = 'Sent';
		requestMap['status'] = resp;
		smsResponseToHistory(true, requestMap, req_data).then(function() {

		});
				
	
	}
	else {

		req_data[extensionFieldStatus]="failed";
		if(!isBulk)
		popupSccuessExit(false, requestMap);
		else
		smsResponseToHistory(false, requestMap, req_data);

	}

}

async function createHistory(requestMap, req_data) {

	return await createRecord(extensionHistory, req_data).then(function(resp) {
		return { resp: resp, requestMap: requestMap };
	});

}

function getMessageWithFields(message, currentRecord){
	
	moduleFields.forEach(function(field) {
		let replace = "${"+module+"."+field.field_label+"}";
		if(currentRecord[field.api_name] != null)
		{
			let value = currentRecord[field.api_name];
			if(field.data_type == "datetime")
			{
				value = new Date(value);
				toTimeSet = value.toLocaleTimeString().split(':')[0]+':'+value.toLocaleTimeString().split(':')[1]+' '+value.toLocaleTimeString().split(':')[2].slice(3);
				value = singleDigitChange(value.getDate())+'/'+singleDigitChange(value.getMonth()+1)+'/'+value.getFullYear()+' '+toTimeSet;
			}
			if(value.name)
			{
				value = value.name;
			}
			
			message = message.replaceAll(replace,value);
		}
		else
		{
			message = message.replaceAll(replace,"");
		}
	});	
		
	if(selectedUser) {
		userFields.forEach(function(field){
			let replace = "${Users."+field.field_label+"}";
			if(selectedUser[field.api_name] != null)
			{
				let value = selectedUser[field.api_name];
				if(field.data_type == "datetime")
				{
					value = new Date(value);
					toTimeSet = value.toLocaleTimeString().split(':')[0]+':'+value.toLocaleTimeString().split(':')[1]+' '+value.toLocaleTimeString().split(':')[2].slice(3);
					value = singleDigitChange(value.getDate())+'/'+singleDigitChange(value.getMonth()+1)+'/'+value.getFullYear()+' '+toTimeSet;
				}
				if(value.name)
				{
					value = value.name;
				}
				
				message = message.replaceAll(replace,value);
			}
			else
			{
				message = message.replaceAll(replace,"");
			}
		});
	}

	return message.replace(/ /g, ' ').trim();
	
}

async function checkMobileNumber(no, countryCode) {

	return await checkPhoneNumber(no).then(function(resp) {

			if(resp && resp.countryPrefix != null) {
	    		
	    		countryCode = resp.countryPrefix.toString();

	    		if(countryCode === "254" && no.indexOf("2540") === 0){
	                return "254"+no.substring(4);
	            }
	            else if(countryCode === "54" && no.indexOf("549") !== 0){
	                return "549"+no.substring(2);
	            }
	            else if(countryCode === "52" && no.indexOf("521") !== 0){
	                return "521"+no.substring(2);
	            }
	            else {
	            	let phoneNumber = resp.phoneNumber.toString();
		    		no = phoneNumber.substring(phoneNumber.indexOf(countryCode) + countryCode.length);
					return countryCode+no;
	            }
	    	}
	    	else {
	    		return countryCode+no.replace(/\D/g,'');
	    	}

	    		

	});
		
}

function setphoneRecordUpdate(updatePhoneRecord) {

	let recipientName = updatePhoneRecord[phoneRecord.api_name];

	if(phoneRecord.entity == "Contacts" || phoneRecord.entity == "Leads" ) {
		recipientName = updatePhoneRecord.Full_Name;
	}
	else if(phoneRecord.entity == "Accounts") {
		recipientName = updatePhoneRecord.Account_Name;
	}	
	phoneRecord['Mobile'] = updatePhoneRecord[phoneRecord.api_name];
	phoneRecord['recipientName'] = recipientName;
	phoneRecord['id'] = updatePhoneRecord.id;
	return;

}

async function getMobileNumber(currentRecord) {

	if(!phoneRecord || !phoneRecord.entity) {

		phoneRecord = {'Mobile': '', 'recipientName': '', 'id': ''};

	}
	else if(module == phoneRecord.entity) {
		setphoneRecordUpdate(currentRecord);
		return;
	}
	else {

		let moduleEntity = phoneRecord.entity;
		if(module == "Deals") {
			moduleEntity = phoneRecord.entity.slice(0, -1)+'_Name';
		}

		if(currentRecord[moduleEntity])
		await getRecord(phoneRecord.entity, currentRecord[moduleEntity].id).then(function(contactData) {

			setphoneRecordUpdate(contactData.data[0]);
			return;

		});
		else {

			phoneRecord["Mobile"] = '';
			phoneRecord["recipientName"] = '';
			phoneRecord["id"] = '';

		}

	}
	
}    
		
		
function insert(bookingLink){

	if (sel && sel.rangeCount && isDescendant(sel.focusNode)){
        let range = sel.getRangeAt(0);
        range.collapse(true);
	    let span = document.createElement("span");
	    span.appendChild( document.createTextNode('${'+bookingLink.children[0].value+'}') );
		range.insertNode(span);
		range.setStartAfter(span);
        range.collapse(true);
        sel.removeAllRanges();
        sel.addRange(range);
    }   

    $('details').removeAttr("open"); 
    
}

function isDescendant(child) {
	let parent = document.getElementById("emailContentEmail");
	let node = child.parentNode;
	while (node != null) {
	 if (node == parent || child == parent) {
	     return true;
	 }
	 node = node.parentNode;
	}
	return false;
}

function singleDigitChange(num) {
	return (num+'').length == 1 ? '0'+num : num;
}

function scheduleTimeSet(date, time) {

	let element = document.getElementById("scheduleCheck");

	if(!date || !time) {
		date = singleDigitChange(new Date().getMonth()+1)+'/'+singleDigitChange(new Date().getDate())+'/'+new Date().getFullYear();
		if(new Date().getHours()-12 == 0)
		time = '12'+':'+singleDigitChange(new Date().getMinutes())+' '+'PM';
		else if(new Date().getHours()-12 == -12)
		time = '12'+':'+singleDigitChange(new Date().getMinutes())+' '+'AM';
		else if(new Date().getHours()-12 > 0) {
			time = singleDigitChange(new Date().getHours()-12)+':'+singleDigitChange(new Date().getMinutes())+' '+'PM';
		}
		else
		time = singleDigitChange(new Date().getHours())+':'+singleDigitChange(new Date().getMinutes())+' '+'AM';
	}

	if(element.checked == true){
		document.getElementById("send").innerText="Schedule";
		document.getElementById("scheduledDateTime").innerText = new Date(date).toDateString()+" at "+time +" ("+Intl.DateTimeFormat().resolvedOptions().timeZone+")";
		scheduledTime = new Date(date+" "+time).toISOString();
		$('.schedule').css({'font-weight': '500'});
	}
	else{
		document.getElementById("scheduledDateTime").innerText = new Date(date).toDateString()+" at "+time +" ("+Intl.DateTimeFormat().resolvedOptions().timeZone+")";
		document.getElementById("send").innerText="Send";
		$('.schedule').css({'font-weight': '490'});
		scheduledTime = undefined;
	}
}	

function enableSchedule(date, time) {

	if(new Date(date+" "+time).getTime() < new Date().getTime()){
		wcConfirm('Schedule time should be greater than current time.','','Okay',true,false);
		scheduleTimeSet();
	}
	else{
		document.getElementById("scheduleCheck").checked = true;
		scheduleTimeSet(date, time);
	}	
}

function msgBoxInsertFieldButtFunc(selected) {
	$(selected).parent().click();
}

function mainBodyClickFunction(e) {

    if($(e.target).hasClass('templateItem') || $(e.target).hasClass('dropdown-menu') || $(e.target).hasClass('dropListButt') || $(e.target).hasClass('choose') || $(e.target).hasClass('arrowIcon')){

    }
    else{
        $('.dropUlOut').removeAttr("open");
    }

    if($(e.target).hasClass('dropdown-item') || $(e.target).hasClass('dropdown-menu') || $(e.target).hasClass('dropdown_Butt') || $(e.target).hasClass('userFieldDivButt') || $(e.target).hasClass('msgBoxInsertFieldButt') || $(e.target).hasClass('dp_arrowIcon') || $(e.target).hasClass('selectedUserTick') || $(e.target).hasClass('selectedUserList') || $(e.target).hasClass('userListHead')){

    }
    else{
        $('.userFieldDiv').removeAttr("open");
    }

    if($(e.target).hasClass('dropdown-item') || $(e.target).hasClass('dropdown-menu') || $(e.target).hasClass('dropdown_Butt') || $(e.target).hasClass('userFieldsCreateSub') || $(e.target).hasClass('msgBoxInsertFieldButt') || $(e.target).hasClass('dp_arrowIcon')){

    }
    else{
        $('.userFieldsCreateDiv').removeAttr("open");
    }

    if($(e.target).hasClass('dropdown-item') || $(e.target).hasClass('dropdown-menu') || $(e.target).hasClass('dropdown_Butt') || $(e.target).hasClass('moduleFieldDivButt') || $(e.target).hasClass('msgBoxInsertFieldButt') || $(e.target).hasClass('dp_arrowIcon') || $(e.target).hasClass('thisModuleName')){

    }
    else{
        $('.moduleFieldDiv').removeAttr("open");
    }

    if($(e.target).hasClass('dropdown-item') || $(e.target).hasClass('dropdown-menu') || $(e.target).hasClass('dropdown_Butt') || $(e.target).hasClass('moduleFieldsCreateSub') || $(e.target).hasClass('msgBoxInsertFieldButt') || $(e.target).hasClass('dp_arrowIcon') || $(e.target).hasClass('moduleFieldsCreate')){

    }
    else{
        $('.moduleFieldsCreateDiv').removeAttr("open");
    }

    if($(e.target).hasClass('templateItem') || $(e.target).hasClass('dropdown-menu') || $(e.target).hasClass('tempChoose') || $(e.target).hasClass('templateFieldDivButt') || $(e.target).hasClass('msgBoxInsertFieldButt') || $(e.target).hasClass('dp_arrowIcon')){

    }
    else{
        $('.templateFieldDiv').removeAttr("open");
    }

    if($(e.target).hasClass('templateItem') || $(e.target).hasClass('dropdown-menu') || $(e.target).hasClass('senderChoose') || $(e.target).hasClass('senderFieldDivButt') || $(e.target).hasClass('arrowIcon')){

    }
    else{
        $('.sender_FieldDiv').removeAttr("open");
    }

    if($(e.target).hasClass('templateItem') || $(e.target).hasClass('dropdown-menu') || $(e.target).hasClass('templateChoose') || $(e.target).hasClass('templateFieldDivButt') || $(e.target).hasClass('arrowIcon')){

    }
    else{
        $('.template_FieldDiv').removeAttr("open");
    }
    if($(e.target).hasClass('editable') || $(e.target).hasClass('send') || $(e.target).hasClass('choose') || $(e.target).hasClass('senderChoose') || $(e.target).hasClass('senderFieldDivButt') || $(e.target).hasClass('dropListButt')){
    	if($('#dateTime').is(':visible'))
    	$('#dateTime').attr('view', 'closed').hide();
    }
    else{
        
    }

}

function wcConfirm(htmlText, runFunc, confirmTitle, alart, buttonHide) {

	$('.wcConfirmOuter').remove();
    $('body').append(`<div class="wcConfirmOuter">
        <div class="wcConfirmInner divPopUpShow">
            <div class="wcConfirmBody">
                ${htmlText}
            </div>
            <div class="wcConfirmButt" style="display:${buttonHide?'none':'flex'};">
                <div class="wcConfirmCancel" style="display:${alart?'none':'block'};" onclick="wcConfirmHide()">Cancel</div>
                <div class="wcConfirmYes" onclick="wcConfirmHide();${runFunc}">${confirmTitle}</div>
            </div>
        </div>
    </div>`);

}

function wcConfirmHide() {

    $('.wcConfirmInner').removeClass('divPopUpHide').removeClass('divPopUpShow').addClass('divPopUpHide').hide();
    $('.wcConfirmOuter').fadeOut(0);
    setTimeout(function() { $('.wcConfirmInner').removeClass('divPopUpHide').removeClass('divPopUpShow').html('').hide().fadeOut(); $('.wcConfirmOuter').remove(); }, 300);

}

$(document).keypress(function(e) {

    if($('.wcConfirmYes').is(':visible'))
    if(e.which == 13) {
        $('.wcConfirmYes').click();
    }

});

function getSafeString(rawStr) {
    if (!rawStr || rawStr+"".trim() === "") {
        return "";
    }

    return $('<textarea/>').text(rawStr).html();
}

function chooseMsgTypeFunc(e) {
	if($(e.target).hasClass('voiceButtMsg') || $(e.target).hasClass('voiceButtMsgSpan') || $(e.target).hasClass('voiceButtMsgSpanOut') || $(e.target).hasClass('voiceButtMsgtxt')) {

		$('.voiceButtMsg').css({'background-color': 'rgb(15 90 193)', 'color': 'white', 'cursor': 'default'});
		$('.TextButtMsg').css({'background-color': 'white', 'color': 'rgb(160 160 160)', 'cursor': 'pointer'});
		msgType = 'voice';

    }
    else if($(e.target).hasClass('TextButtMsg') || $(e.target).hasClass('TextButtMsgSpan') || $(e.target).hasClass('TextButtMsgSpanOut') || $(e.target).hasClass('TextButtMsgtxt')) {

    	$('.voiceButtMsg').css({'background-color': 'white', 'color': 'rgb(160 160 160)', 'cursor': 'pointer'});
		$('.TextButtMsg').css({'background-color': 'rgb(15 90 193)', 'color': 'white', 'cursor': 'default'});
		msgType = 'text';

    }
}

async function sms_creditsSelectFunc(selected) {

	$('#loader').show().css({'opacity': '0.5'});
	smsRoute = $(selected).val();
	await credentialValuesSet(credential, smsRoute);

}

function extensionSignUpRedirectFuc() {
	window.open('https://www.ringcentral.com/signup');
}

function ulgContact() {
	window.open('https://apps.ulgebra.com/contact');
}

function textCopyInCommand(selected) {
    let copyFrom = $('<textarea/>');
    copyFrom.text($(selected).attr('data-copyText'));
    $('body').append(copyFrom);
    copyFrom.select();
    document.execCommand('copy');
    copyFrom.remove();

    $('.copyTextDiv').fadeIn(500);
    $('.copyTextDiv').fadeOut(1500);
}

function announcetipExitDiv() {
	$('#announcetip').hide();
	if($('.mainSearchBarHolder').is(':visible'))
	popupSizeSet(800, 625);
	else
	popupSizeSet(800, 575);
}

		function showAnnounceMent(type){
		 	if(type === "install_chrome_extension"){
                $('body').append(`<div id="announcetip" class="bottomTip" style="height: 40px; top: -8px;" >Automatically Send Single &amp; Bulk WhatsApp messages in background <div class="bottomTipActions" style="position: relative; top: -2px;"><a href="https://chrome.google.com/webstore/detail/wa-web-for-zoho-crm-bulk/haphhpfcpfeagcmannpebjjjpdlbhflh" target="_blank">Install Google Chrome Plugin </a></div>
                    <span onclick="announcetipExitDiv();" class="tt-close">X</span>
                </div>`);
            }
             if(type === "purchase_license"){
                $('body').append(`<div id="announcetip" class="bottomTip" style="height: 40px; top: -8px;" >Thank you for installing chrome plugin. <br> <br> Automatically Send Single &amp; Bulk WhatsApp messages in background <div class="bottomTipActions" style="position: relative; top: -2px;"><a href="https://apps.ulgebra.com/zoho/crm/whatsapp-web/pricing?src=crmwabulkpop#h.xi7gibhrktbj" target="_blank">Upgrade Plan</a></div>
                    <span onclick="announcetipExitDiv();" class="tt-close">X</span>
                </div>`);
            }
			/*$('body').append(`<div id="announcetip" class="bottomTip" ><div class="bottomTipActions">Temporarily stopped chrome extension. Will be enabled soon.<a href="https://forms.gle/kXX6wJnyvYLEd2nbA" target="_blank">Submit Feedback</a> </div> </div>`);*/
        }

		function setSeenAlert(alertId){
			$('.extensionVersionCheck').hide();
			try {
              localStorage.setItem("alertseen", alertId);
            }
            catch(err) {
                loadDebugInfo(":unable-to-set-alertseen:");
            }
		}


		function showForcedAlert(){

			if($('.mainSearchBarHolder').is(':visible'))
			popupSizeSet(800, 700);
			else
			popupSizeSet(800, 600);
			$('body').append(`<div id="announcetip" class="bottomTip" style="height: 40px; top: -8px;" ><div class="bottomTipActions" style="position: relative; top: -2px;"><a href="https://apps.ulgebra.com/zoho/crm/whatsapp-web/pricing?src=crmwabulkpop#h.xi7gibhrktbj" target="_blank"> Upgrade Plan </a>  <a style="margin:0px 5px" href="https://wa.me/917397415648?text=Hello%20Ulgebra,%20I%20have%20a%20query" target="_blank">Contact Us</a> </div>
                    <span onclick="announcetipExitDiv();" class="tt-close" style="top: 9px;">X</span>
                </div>`);

// 		    var seenalert = "";
// 		    try {
//                 seenalert = localStorage.getItem("alertseen");
//             }
//             catch(err) {
//                 loadDebugInfo(":unable-to-get-alertseen:");
//             }
// 			if(seenalert!=="1.0"){
// 				$("body").append(`
// 					<div class="extensionVersionCheck">
// 						<div class="alerttl">New Chrome Extension Version</div>
// 							Kindly ensure you have the latest version of our chrome extension.  <br><br>
// 							If you have installed our chrome extension, old chrome extension will not work. Kindly remove and install new chrome extension.
// 						<br><br>
// 						<span style="color:green">Latest Version : 9.0</span>
// 						<br><br>
// 						<a href="https://chrome.google.com/webstore/detail/wa-web-for-zoho-crm-bulk/nkihbgbbbakefbgjnokdicilnhcdeckp" target="_blank">New Version Extension Link</a> <br><br><div class="closealertdia" onclick="$('.extensionVersionCheck').hide()">Close alert</div><div class="closealertdia" onclick="setSeenAlert('1.0')">Done &amp; Never Show Again</div>
						
// 						</div>
// 				`);
// 			}
		}

		
       


		function showNewUpdateLink(){
            document.getElementById("loader").style.display= "none";
            //document.getElementById("ErrorText").insertAdjacentHTML("beforeend",`Please update the extension.<br><a href="https://writer.zohopublic.com/writer/published/m9ho5929b5193196a4c6cbafeee6667ac6d60" target="_blank"><button style="text-align:center;cursor:pointer;background-color: white;color: black;border-radius: 5px;border: none;margin-top: 15px;box-shadow: 0px 2px 5px rgba(0,0,0,0.4);cursor: pointer;box-sizing: border-box;padding: 8px 18px;border-radius: 2px;" >See How to Update</button></a><br><br> Go To Settings -->Marketplace-->All-->Updates-->WhatsApp Web for Zoho CRM-->Click Update`);
            wcConfirm(`<div class="update_the_extensionDiv">Please update the extension.<br><a href="https://writer.zohopublic.com/writer/published/m9ho5929b5193196a4c6cbafeee6667ac6d60" target="_blank"><button style="text-align:center;cursor:pointer;background-color: #5d6daf;color: #e5e6fa;border-radius: 5px;border: none;margin-top: 15px;box-shadow: 0px 2px 5px rgba(0,0,0,0.4);cursor: pointer;box-sizing: border-box;padding: 8px 18px;border-radius: 2px;" >See How to Update</button></a><br><br><div class="detailsShowExtensionDiv" style="font-size: 12px;font-weight: 300;line-height: 1.8;"> Go To Settings -->Marketplace-->All-->Updates-->WhatsApp Web for Zoho CRM-->Click Update</div>`,'','Okay',true,true);
        }


// function selectCountryCode(countryPrefix){
// 			countryCode = countryPrefix;
// 			saveCountryCode(countryCode);
// 		}
		function saveCountryCode(countryCode){
	         phoneFieldsSetting["countryCode"]=countryCode;
	         updateOrgVariables("whatsappforzohocrm__mobileNumberSettings",phoneFieldsSetting);
	    }
		
        function updateOrgVariables(apiname,value,key){
    		if(apiname == "whatsappforzohocrm__openwithwebordesktop"){
    			document.getElementById("web").checked = (value=="web");
    			document.getElementById("desktop").checked =(value=="desktop");
    		}
    		ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": apiname,"value": value});
        }
        

		function getSanitizedContent(msg){
			if(extensionVersion){
				$("body").append(`<img style="display:none" src="https://us-central1-slackappp-39d3d.cloudfunctions.net/zdeskww?debug=wawebext-single::cx-mail:${userEmail}:::-cexv:${extensionVersion.version}::" />`);
			}
			if(extensionVersion && parseFloat(extensionVersion.version) > 8) {
				console.log('extension version is > 8');
				return encodeURIComponent(msg);
			}
			else{
				console.log('extension version is not > 8');
				return msg;
			}
		}

		function loadDebugInfo(txt){
			
			$("body").append(`<img style="display:none" src="https://us-central1-slackappp-39d3d.cloudfunctions.net/zdeskww?debug=wawebext-bulk::cx-mail:${userEmail}:::-cexv:${encodeURIComponent(txt)}::" />`)
		}

		
		var firebaseConfig = {
			apiKey: "AIzaSyB_NTkxQH12sAiB-F0mVIm89uA2VirabC4",
			projectId: "wawebbulk",
			storageBucket: "wawebbulk.appspot.com",
			messagingSenderId: "218291591421",
			appId: "1:218291591421:web:3ca4c3a172add3e7d0c694",
			measurementId: "G-MMCYRRQPPG"
		};

		function executeFirebaseThings(){
			try{

				firebase.initializeApp(firebaseConfig);
				//firebase.analytics();
				db = firebase.firestore();

				db.collection('LicenseUsers').doc(userEmail).get().then((data)=>{
					if(!data.exists){
						fetchLICDetails(userEmail, showNewPlanDetails);
						checkVersion();
						return;
					}
					peySts = data.data().type;
					if(!peySts){
						fetchLICDetails(userEmail, showNewPlanDetails);
						checkVersion();
						return;
					}
					$('.bottomTipActions').prepend(' <b class="oldlicban"> '+peySts.substring(0, peySts.indexOf("-")).toUpperCase()+' </b> plan ');
					isAllowed = data.data().type.indexOf("basic") == 0 || data.data().type.indexOf("classic") == 0;
					if(data.data().type.indexOf("business")==0 || data.data().type.indexOf("premium")==0 || data.data().type.indexOf("classic")==0){
						$.ajax({url: "https://us-central1-wawebbulk.cloudfunctions.net/checkUALIC?src=crm-wa-bulk&email="+userEmail, success: function(result){
							if(result){
								if(result.mcn > 0){
									$('.bottomTipActions').prepend('<span class="oldlicban">credit remaining : <b>'+(result.mcn)+" </b> &nbsp;&nbsp;</span>");
								}
							}
							if(result && result.mcn <=0) {
								showNoCreditErrorOnWhatsAppSend = true;
								//document.write(`You have sent all messages which was allowed in this plan. Kindly request more credits to continue. <a href="https://apps.ulgebra.com/contact" target="_blank">Contact Now</a> <br><br> <a href="https://apps.ulgebra.com/zoho/crm/whatsapp-web/pricing" target="_blank">View Pricing Plans</a> `);
							}
							 //  executeFirebaseThings();
    					    if(result && result.status == "valid"){
    					    	isAllowed = true;
    					    }
    					}});
					}
					if(data.data().type.indexOf("business")==0 || data.data().type.indexOf("premium")==0){
						attPur = true;
						isAllowed = true;
						
					}
					if(data.data().type.indexOf("automation")==0){
						attPur = true;
						isAllowed = true;
						isAutomation = true;
					} 
					else{
						checkVersion();
					}
					if(!isAllowed){
						//showNoHighPlanError();
						fetchLICDetails(userEmail, showNewPlanDetails);
					}
				}).catch((err)=>{
					//showNoHighPlanError();
					fetchLICDetails(userEmail, showNewPlanDetails);
					$('.bottomTipActions').prepend(' <span class="oldlicban"><b> Basic </b> plan </span>');
				});

				db.collection('appActivityLog').doc(userEmail).collection('history').add({
					't' : Date.now(),
					'a': 'WWBULK-BULK',
					'cev' : extensionVersion && extensionVersion.version ? extensionVersion.version : '-'
				});
			}
			catch(err){
				console.log(err);
				$('.bottomTipActions').prepend(' <span class="oldlicban"><b> Basic </b> plan</span> ');
				loadDebugInfo(":unable-to-fetch-firebase:"+userEmail);
			}

		}


		
		
		function checkVersion(){
			if(!!window.chrome){
				chrome.runtime.sendMessage(editorExtensionId, { "message": "version" },function (reply){
					console.log(reply);
					extensionVersion = reply;
					if(extensionVersion){
						doOnValidChromeExtension();
					}
					else{
						checkVersionLatest();
					}
				});	
				}else{
				showNoChromeExtError();
			}		
		}
		function doOnValidChromeExtension(){							
			setTimeout(function(){
							checkState();
						},2000);
						// if(parseFloat(extensionVersion.version) > 9){
						// 	document.getElementsByClassName("attach-opt-shownOut")[0].insertAdjacentHTML('afterbegin', `
						//     <div class="sender-tools-waweb" id="attachDiv">
						//         <input onchange="getAttachments()" type="checkbox" id="wwebattach" name="wwebattach" style="
						//     height: 12px;
						//     width: 12px;
						// 	cursor: pointer;
						//     vertical-align: middle;
						//     margin-top: -1px;
						// "> <label for="wwebattach" style="
						//     margin-left: 5px;
						//     cursor: pointer;
						// ">Include attachments with this message</label>
						//     </div>`);
						// }
		}

		function getAttachments(){
			if(attPur){
				if(!isAttachmentAdded){
					isAttachmentAdded = true;
					$('#wa-attach-tip').remove();
		$("#attachDiv").append(`<span id="wa-attach-tip" style="display: block;border: 1px dotted green;padding: 3px 10px 2px 10px;background-color: rgba(0,200,0,0.1);word-break: unset;border-radius: 3px;font-size: 14px;color: green;text-overflow: ellipsis;overflow: hidden;white-space: nowrap;position: absolute;top: 5px;height: 17px;right: -438px;font-size: 11px;">You will be redirected to WhatsApp tab for sending attachments after you click Send. </span>`);
				}
				else{
					$("#wa-attach-tip").remove();
					isAttachmentAdded = false;
				}
			}
			else{
				if($("#wa-attach-tip").length==0){
					
					$(".sender-tools-whatcetra").append(`<span id="wa-attach-tip" style="display: flex;border: 1px dotted maroon;padding: 3px 10px 2px 10px;background-color: rgb(200 0 0 / 10%);word-break: unset;border-radius: 3px;font-size: 14px;color: maroon;text-overflow: ellipsis;overflow: hidden;white-space: nowrap;position: absolute;top: 5px;height: 17px;right: -276px;font-size: 11px;">You need to purchase license to access this feature. <div class="" style="
							margin-top: 5px;
						">
						</div> </span>`);
				}
				else{
					$("#wa-attach-tip").remove();
					isAttachmentAdded = false;
				}
			}
		}
		function checkVersionLatest(){
			if(!!window.chrome){
				chrome.runtime.sendMessage("haphhpfcpfeagcmannpebjjjpdlbhflh", { "message": "version" },function (reply){
					console.log(reply);
					extensionVersion = reply;
					if(extensionVersion){
						editorExtensionId = "haphhpfcpfeagcmannpebjjjpdlbhflh";
						doOnValidChromeExtension();
					}
					else{
						showNoChromeExtError();
					}
				});	
				}else{
				showNoChromeExtError();
			}		
		}

		
	
		var secondaryApp;
		var database;
		var atno="";
		var isAutomation= false;

        
        
        function addToQueue(data){
        	var user = secondaryApp.auth().currentUser;
        	if(user){
	        	var ref = database.ref("users/"+user.uid+"/numbers/"+atno+"/outgoing");
				return ref.update(data, function(error) {
				    if (error) {
				    	console.log(error);
				      // The write failed...
				    } else {
				    	console.log("successfully");
				      // Data saved successfully!
				    }
				    //document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your Message has been sent successfully.</div>';
					wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingMsgSuccess">${verifiedTickSVG}</div><div class="sendingMsgSuccessHint">${'Your Message has been sent successfully.'}</div></div>`,'','Okay',true,true);
					//setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload(); }, 1500);
					return true;
				});
			}
			else
			return false;		
        }

        async function getSettings(){
        	var user = secondaryApp.auth().currentUser;
        	var numbers = await database.ref("users/"+user.uid+"/settings/number").once('value');
	        console.log(numbers.val());
	        if(numbers.val()){
				atno=numbers.val()+"";
				fromPhoneNumber = atno;
				saveNumberToOrg(atno);
			}
			else{
				saveSettings();
			}		
        }
        function saveNumberToOrg(no){
        	if(phoneFieldsSetting){
        		phoneFieldsSetting["whatsappNumbers"]=[no];
	        	ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "whatsappforzohocrm__mobileNumberSettings","value": phoneFieldsSetting}).then(function(res){
	    			//document.getElementById("ErrorText").innerText = "Saved";
	    			wcConfirm(`Saved`,'','Okay',true,true);
					setTimeout(function(){wcConfirmHide(); }, 500);
	    		});
	        }
	         
        }
        function saveSettings(){
			var user = secondaryApp.auth().currentUser;
        	if(user){
	        	var ref = database.ref("users/"+user.uid+"/private");
	        	var incomingUrl =pluginurl.replace("sendwhatsapp","incominghandler");
				ref.set({"pluginUrl":incomingUrl,"orgId":orgId,"userEmail":userEmail}, function(error) {
				    if (error) {
				    	console.log(error);
				      // The write failed...
				    } else {
				    	console.log("successfully");
				      // Data saved successfully!
				    }
				});
			}	
		}

	function showNoChromeExtError(){
			$("body").append(`<div class="chromeextnpopup">
        <div class="cetp-inner">
            <div class="cetp-ttl">
                Install Google Chrome Extension! <div class="cetp-close" onclick="$('.chromeextnpopup').hide()">x</div>
            </div>
            <div class="cetp-content">
                Our Google Chrome extension is needed to Send Messages in Background and to Send Bulk WhatsApp Messages. <br><br>
				You need to purchase <a target="_blank" href="https://apps.ulgebra.com/zoho/crm/whatsapp-web/pricing?src=crmwabulkpop#h.xi7gibhrktbj">one of our plans to send Bulk Messages.</a>
                <a href="https://chrome.google.com/webstore/detail/wa-web-for-zoho-crm-bulk/haphhpfcpfeagcmannpebjjjpdlbhflh" target="_blank"> <br><br>
                    <button class="cetp-install">
                        <span class="material-icons">get_app</span> Install Google Chrome Extension
                    </button>
                </a>
            </div>
        </div>
    </div>`);
		}


		function showNewUpdateLink(){
            document.getElementById("loader").style.display= "none";
            wcConfirm(`Please update the extension.<br><a href="https://writer.zohopublic.com/writer/published/m9ho5929b5193196a4c6cbafeee6667ac6d60" target="_blank"><button style="text-align:center;cursor:pointer;background-color: white;color: black;border-radius: 5px;border: none;margin-top: 15px;box-shadow: 0px 2px 5px rgba(0,0,0,0.4);cursor: pointer;" >See How to Update</button></a><br><br> Go To Settings -->Marketplace-->All-->Updates-->WhatsApp Web for Zoho CRM-->Click Update`,'','Okay',true,false);
            document.getElementById("container").style.display= "block";
        }

	function showNoHighPlanError(showCancel =  false, hideClassic = false){
			$("body").append(`<div id="highplanerr" class="chromeextnpopup">
        <div class="cetp-inner">
            <div class="cetp-ttl">
                Upgrade your plan!
            </div>
            <div class="cetp-content"> 
                Send Bulk WhatsApp messages to customers with attachments or automated messages with workflows.

                <br><br> To enable this feature you need to purchase <b>${hideClassic ? '' : 'classic or '}business or premium</b> plan of our chrome extension.
                
<a href="https://apps.ulgebra.com/zoho/crm/whatsapp-web/pricing?src=crmwabulkpop#h.xi7gibhrktbj" target="_blank"> <br><br>
                    <button class="cetp-install">
                        <span class="material-icons">launch</span> Purchase license
                    </button>
                </a>
				<button onclick="\$('#highplanerr').remove()" style="${showCancel ? '' : 'display:none;'} border: none;padding: 7px 15px;margin-left: 10px;box-shadow: 0px 1px 3px rgb(0 0 0 / 30%);position: relative;top: 35px;left: -8px;
				">Cancel</button>
                <br>   <br>   
    <a href="https://apps.ulgebra.com/contact" target="_blank" style="
    float: right;
">Contact Ulgebra</a>
            </div>
        </div>
    </div>`);
		}

		
function handleScheduleAction(){
	var seetingsLink = "https://crm.zoho.com/crm/settings/extensions/all/whatsappforzohocrm?tab=webInteg&subTab=marketPlace&nameSpace=whatsappforzohocrm&portalName=ulgebra";
	if(!chosenWASenderId){
		return;
	}
	if(chosenWASenderId == givenUserId){
		$('#wa-sch-btn').hide();
		$('#scheduleFeature').show();
		return;
	}
	else if(!givenUserId){
		wcConfirm(`Please configure userId and apiKey in <a href="${seetingsLink}" target="_blank"><button style="text-align:center;cursor:pointer;background-color: white;color: black;border-radius: 5px;border: none;margin-top: 15px;box-shadow: 0px 2px 5px rgba(0,0,0,0.4);cursor: pointer;" >Extension configure page</button></a>`,'','Okay',true,false);
	}
	else if(givenUserId && senders[givenUserId]){
		wcConfirm(`Credential configured for <b>${senders[givenUserId].name?senders[givenUserId].name:senders[givenUserId].displayName}</b> - WhatCetra account, <br>Please change userId and apiKey in <a href="${seetingsLink}" target="_blank"><button style="text-align:center;cursor:pointer;background-color: white;color: black;border-radius: 5px;border: none;margin-top: 15px;box-shadow: 0px 2px 5px rgba(0,0,0,0.4);cursor: pointer;" >Extension configure page</button></a>.`,'','Okay',true,false);
	}
	else if(givenUserId){
		wcConfirm(`Please update userId and apiKey in <a href="${seetingsLink}" target="_blank"><button style="text-align:center;cursor:pointer;background-color: white;color: black;border-radius: 5px;border: none;margin-top: 15px;box-shadow: 0px 2px 5px rgba(0,0,0,0.4);cursor: pointer;" >Extension configure page</button></a>.`,'','Okay',true,false);
	}
	
}
var chosenWASenderId = null;
function setWAfromPhoneNumber(isLocalNumber, displayName, id) {
	$('.attach-opt-shown').remove();
	if(isLocalNumber){
		chosenWASenderId = "whatsapp-web";
		$('.sender-tools-whatcetra').hide();
		$('.sender-tools-waweb').show();
		$('#wa-from-phone-label-val').text(`${displayName} - WhatsApp Web`);
		if(scheduledTime)
		$('.attach-opt-shownOut').css({'visibility': 'hidden'});
		else
		$('.attach-opt-shownOut').css({'visibility': 'visible'});
	}
	else{
		chosenWASenderId = id;
		if(chosenWASenderId != givenUserId){
			scheduledTime = null;
			$('#wa-sch-btn').show();
			$('#scheduleFeature').hide();
		}
		$('.attach-opt-shownOut').css({'visibility': 'visible'});
		$('.sender-tools-whatcetra').show();
		$('.sender-tools-waweb').hide();
		$('#wa-from-phone-label-val').text(`${displayName} - WhatCetra account`);
	}
	$('.fromNumChooserOuter').hide();
	if(sendBtnClickedForProceed){
		sendSMS();
	}
}
function renderWAWebSenderIds() {
	if(fromPhoneNumber){
		$('#fnco-web-wa-number').html(`${fromPhoneNumber} <span>(Connected via <a href="https://web.whatsapp.com" target="_blank">WhatsApp Web</a>)</span>`).on('click', (function(){
			setWAfromPhoneNumber(true, fromPhoneNumber, '');
		}));
	}else{
		$('#fnco-web-wa-number').html(`<div style="color:grey;cursor:not-allowed">WhatsApp Web is not connected <button class="err-wa-conn-label" onclick="showWWebNotConnectedInstructions(checkNumber, true)">Fix now</button></div>`).off();
	}
}
var senders={};
function renderWhatCetraSenderIds() {

	if(!whatcetraApp || !whatcetraApp.auth() && !whatcetraApp.auth().currentUser){
		waWebZohoCrmSignOut();
		return;
	}
	else {
		whatcetraApp.firestore().collection('/invites/'+whatcetraApp.auth().currentUser.email+'/org_invites').get().then(function (querySnapshot) {
          if(!querySnapshot.docs.length) {
              console.log('empty');
          }
          $('#fnco-whatcetra-sender-ids').html(`<div onclick="setWAfromPhoneNumber(false, '${whatcetraApp.auth().currentUser.displayName}', '${whatcetraApp.auth().currentUser.uid}')" id="wc-send-num-${whatcetraApp.auth().currentUser.uid}" class="fnco-item wc-sender-num">
               <div class="fnco-ico" style="background-image:url('${whatcetraApp.auth().currentUser.photoURL}')"></div>  ${whatcetraApp.auth().currentUser.displayName} - <span> Send via  <a href="https://app.whatcetra.com" target="_blank">WhatCetra</a> account</span>
              </div>`);
          senders[whatcetraApp.auth().currentUser.uid]=whatcetraApp.auth().currentUser;
          querySnapshot.forEach(item=>{ 
          	item = item.data();
          	senders[item.uid]=item;
          	$('#fnco-whatcetra-sender-ids').append(`<div onclick="setWAfromPhoneNumber(false, '${item.name}', '${item.uid}')" id="wc-send-num-${item.uid}" class="fnco-item wc-sender-num">
               <div class="fnco-ico" src="background-image:url('${item.photoUrl}')"></div> ${item.name} - <span>Send via <a href="https://app.whatcetra.com" target="_blank">WhatCetra</a> account</span>
              </div>`);
      	  });
		});
	}
	
}

		function googleTranslateElementInit() {
		  new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
		}

		function styling(tag)
		{
			document.execCommand(tag);
		}
	
		



		//document.addEventListener("DOMContentLoaded", function(event) {
        
			function check_charcountCreate(content_idCreate, max, e)
			{   
			    if(e.which != 8 && $('#'+content_idCreate).text().length > max)
			    {
			    	wcConfirm(`Message should be within 2000 characters.`,'','Okay',true,false);
	        		// document.getElementById("ErrorText").style.color="red";
			       // $('#'+content_id).text($('#'+content_id).text().substring(0, max));
			       e.preventDefault();
			    }
			}

       		function selectModuleCreate(module){
			document.getElementById("selectedmoduleCreate").innerText = module;
			document.getElementById("moduleFieldsCreate").innerText = "Insert "+module+" Fields";
			var customerData = [];
			var smsContent = document.getElementById("emailContentEmailCreate").innerText;
			if(smsContent){
				if(module == "Leads"){
					if(smsContent.indexOf("${Contacts.") != -1 || smsContent.indexOf("${Accounts.") != -1 || smsContent.indexOf("${Deals.") != -1 ){
						document.getElementById("emailContentEmailCreate").innerHTML ="";
					}
				}
				else if(module == "Contacts"){
					if(smsContent.indexOf("${Leads.") != -1 || smsContent.indexOf("${Accounts.") != -1 || smsContent.indexOf("${Deals.") != -1 ){
						document.getElementById("emailContentEmailCreate").innerHTML ="";
					}
				}
				else if(module == "Accounts"){
					if(smsContent.indexOf("${Contacts.") != -1 || smsContent.indexOf("${Leads.") != -1 || smsContent.indexOf("${Deals.") != -1 ){
						document.getElementById("emailContentEmailCreate").innerHTML ="";
					}
				}
				else if(module == "Deals"){
					if(smsContent.indexOf("${Contacts.") != -1 || smsContent.indexOf("${Accounts.") != -1 || smsContent.indexOf("${Leads.") != -1 ){
						document.getElementById("emailContentEmailCreate").innerHTML ="";
					}
				}
		    }		
			
			$('#dropdown-menu-emailCreate').html('');
			getFields(module).then(function(fields) {
				fields.forEach(function(field){
					addListItemCreate("dropdown-menu-emailCreate",field.field_label,"dropdown-item",module+"."+field.field_label);
				});
		    });
			

			$('.sender_FieldDiv').removeAttr("open");

		}
  //       function showEmail(editor){
		// 	for(var i=0; i<emailContent.length;i++){
		// 		if(emailContent[i].emailId == editor.id){
		// 			document.getElementById("emailContentEmailCreate").innerHTML = emailContent[i].emailContent;
		// 			document.getElementById("subjectEmail").innerHTML=emailContent[i].subject;
		// 			break;
		// 		}
		// 	}
		// }
        function saveTemplateCreate() {
        	var name = document.getElementById("templateNameCreate").value;
        	var templateModule = document.getElementById("selectedmoduleCreate").innerText;
        	var smsContent = document.getElementById("emailContentEmailCreate").innerText;
        	var othermodule=false;
        	if(templateModule == "Leads" && (smsContent.indexOf("${Contacts.") != -1 || smsContent.indexOf("${Accounts.") != -1 || smsContent.indexOf("${Deals.") != -1 )){
				othermodule=true;
			}
			else if(templateModule == "Contacts" && (smsContent.indexOf("${Leads.") != -1 || smsContent.indexOf("${Accounts.") != -1 || smsContent.indexOf("${Deals.") != -1 )){
				othermodule=true;
			}
			else if(templateModule == "Accounts" && (smsContent.indexOf("${Contacts.") != -1 || smsContent.indexOf("${Leads.") != -1 || smsContent.indexOf("${Deals.") != -1 )){
				othermodule=true;
			}
			else if(templateModule == "Deals" && (smsContent.indexOf("${Contacts.") != -1 || smsContent.indexOf("${Accounts.") != -1 || smsContent.indexOf("${Leads.") != -1 )){
				othermodule=true;
			}
			if(othermodule){
        		wcConfirm(`Message Contains Other Modules Merge Fields.Please change it.`,'','Okay',true,false);
				return ;
			}
        	if(name == ""){
        		wcConfirm(`Template Name cannot be empty.`,'','Okay',true,false);
				return ;
        	}
        	if(templateModule == ""){
        		wcConfirm(`Please Choose Module`,'','Okay',true,false);
				return ;
        	}
        	if(document.getElementById("emailContentEmailCreate").innerText.replace(/\n/g,"").replace(/\t/g,"").replace(/ /g,"") == ""){
        		wcConfirm(`Message cannot be empty.`,'','Okay',true,false);
        		return;
        	}
        	else{
	        	wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingLoaderInner">${sendingLoader}</div><div class="sendingLoaderHint">SMS Template is saving...</div></div>`,'','Okay',true,true);
	        	var func_name = "whatsappforzohocrm__sendwhatsapp";
	        	var message = document.getElementById("emailContentEmailCreate").innerText;

					var req_data = {"Name":name,"whatsappforzohocrm__WhatsApp_Message":message,"whatsappforzohocrm__Module_Name":templateModule}
					if(attachedfile && attachedfile.mediaUrl){
		                var mediaObj = {
		                    "url": attachedfile.mediaUrl,
		                    "name": attachedfile.fileMeta.name,
		                    "type": attachedfile.fileMeta.type,
		                    "size": attachedfile.fileMeta.size
		                } 
		                req_data["whatsappforzohocrm__Media"]=JSON.stringify(mediaObj);
		            }   
					ZOHO.CRM.API.insertRecord({Entity:"whatsappforzohocrm__WhatsApp_Templates",APIData:req_data,Trigger:["workflow"]}).then(function(response){
						var responseInfo	= response.data[0];
						var resCode			= responseInfo.code;
						if(resCode == 'SUCCESS'){
							wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingMsgSuccess">${verifiedTickSVG}</div><div class="sendingMsgSuccessHint">${'Your SMS Template saved successfully.'}</div></div>`,'','Okay',true,true);
							var recordId	= responseInfo.details.id;
							ZOHO.CRM.API.getRecord({Entity:"whatsappforzohocrm__WhatsApp_Templates",RecordID:recordId}).then(function(data2){
	    						smsTemplates[data2.data[0].id] = data2.data[0];
	    						setTimeout(function(){
								$('#templateList').prepend(`<li class="templateItem" onclick="showsms(this)" recordId="${getSafeString(data2.data[0].id)}"><span class="templateItemSpanName">${getSafeString(data2.data[0].Name)}</span></li>`);
								$('.createTemplateMainOuterDiv').hide();
								document.getElementById("emailContentEmailCreate").innerHTML = "";
								document.getElementById("templateNameCreate").value = "";
								$('#attachedfileCreate').text('');
								setTimeout(function(){wcConfirmHide(); }, 1500);
							}, 1500);
	    					});								
						}
						else{
							wcConfirm(`Opps! Something went wrong from server side. Please try after sometimes!!!`,'','Okay',true,false);
						}
					});
				}	
					
        }
		
        function addListItemCreate(id,text,className,value){

			let linode = '<li class="'+className+'"><button class="dropdown_Butt" onclick="insertCreate(this)">'+text+'<input type="hidden" value="'+value+'"></button></li>';
			$('#'+id).append(linode);

		}
		
		// function link(){
		// 	$("#linkForm").slideToggle("slow");
		// }
		// function image(){
		// 	$("#imageForm").slideToggle("slow");
		// }
		
		
		// function openlinkCreate(){
		// 	//sel = window.getSelection();
		//     if (sel && sel.rangeCount) {
		//         range = sel.getRangeAt(0);
		//       }  
		// 	if(range && range.commonAncestorContainer.wholeText){
		// 		if(range.commonAncestorContainer.parentNode.href){
		// 			document.getElementById("linkUrl").value = range.commonAncestorContainer.parentNode.href;
		// 			$("#linkForm").slideToggle("slow");
		// 		}
		// 	}	
		// }
		function insertCreate(bookingLink){

		    if (sel && sel.rangeCount && isDescendantCreate(sel.focusNode)){
		        let range = sel.getRangeAt(0);
		        range.collapse(true);
			    let span = document.createElement("span");
			    span.appendChild( document.createTextNode('${'+bookingLink.children[0].value+'}') );
				range.insertNode(span);
				range.setStartAfter(span);
		        range.collapse(true);
		        sel.removeAllRanges();
		        sel.addRange(range);
		    }   

		    $('details').removeAttr("open"); 

		}
		function isDescendantCreate(child) {
			var parent = document.getElementById("emailContentEmailCreate");
		     var node = child.parentNode;
		     while (node != null) {
		         if (node == parent || child == parent) {
		             return true;
		         }
		         node = node.parentNode;
		     }
		     return false;
		}



		function createTempViewCheck() {

			if($('.bottomTip').is(':visible') && $('.mainSearchBarHolder').is(':visible')) {
				$('.createTemplateMainOuterDiv').css({'top': '65px'});
			}
			else if($('.bottomTip').is(':visible') && !$('.mainSearchBarHolder').is(':visible')) {
				$('.createTemplateMainOuterDiv').css({'top': '42px'});
			}
			else if(!$('.bottomTip').is(':visible') && !$('.mainSearchBarHolder').is(':visible')) {
				$('.createTemplateMainOuterDiv').css({'top': '42px'});
			}
			else if(!$('.bottomTip').is(':visible') && $('.mainSearchBarHolder').is(':visible')) {
				$('.createTemplateMainOuterDiv').css({'top': '65px'});
			}

		}

function showProcess(text){
    $(".process-window-outer").show();
    $("#process-window-items").html(`<div class="process-window-item"> ${text}</div>`);
}

function processCompleted(){
    $(`#process-window-items`).html('');
    $(".process-window-outer").hide();    
}
