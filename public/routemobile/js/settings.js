    
var credentials = {};
var extensionName = 'Route Mobile';
var extensionAPI = 'routemobilesmsforzohocrm__';
var extensionFunction = 'sendsmsapi';
var extensionInvokeAPI = extensionAPI + extensionFunction;
var extensionCredential = extensionAPI + 'credentials';

document.addEventListener("DOMContentLoaded", async function(event) {

    ZOHO.embeddedApp.init().then(async function() {

        getAPIURL();

        ZOHO.CRM.API.getOrgVariable(extensionCredential).then(function(apiKeyData){   
            
            if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content && apiKeyData.Success.Content != "0"){
                credentials = JSON.parse(apiKeyData.Success.Content);
                if(credentials.apikey && credentials.senderId && credentials.extraId) {
                    CredentialsUpdateHide();
                    document.getElementById("apikey").value=credentials.apikey;
                    document.getElementById("senderId").value=credentials.senderId; 
                    document.getElementById("extraId").value=credentials.extraId; 
                }
                else {
                    $('.cancelSettingsButt').hide();
                    CredentialsUpdateShow();
                }
            }   
            document.getElementById("loader").style.display= "none";

        }); 
    });

});
        

async function getAPIURL() {

    let urlParams = new URLSearchParams(window.location.search);
    let serviceOrigin = urlParams.get('serviceOrigin');

    let getmap = {"nameSpace":"<portal_name.extension_namespace>"};
    let resp = await ZOHO.CRM.CONNECTOR.invokeAPI("crm.zapikey",getmap);
    let zapikey = JSON.parse(resp).response;
    let domain = "com";
    if(serviceOrigin.indexOf(".zoho.") != -1){
        domain = serviceOrigin.substring(serviceOrigin.indexOf(".zoho.")+6);
    }
    let webHookurl = `https://platform.zoho.${domain}/crm/v2/functions/${extensionInvokeAPI}/actions/execute?auth_type=apikey&zapikey=${zapikey}`;
    document.getElementById("webHookurl").innerText = webHookurl;

}
       
function updateOrgVariables(apiname, value){
    ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": apiname,"value": value}).then(function(res) {
        wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingMsgSuccess">${verifiedTickSVG}</div><div class="sendingMsgSuccessHint">${'Saved'}</div></div>`,'','Okay',true,true);
        CredentialsUpdateHide();
        setTimeout(function(){ wcConfirmHide(); }, 1500);
    });
}

function saveCreds(){

    let api_key = document.getElementById('apikey').value;
    let sender_id = document.getElementById('senderId').value;
    let extraId = document.getElementById('extraId').value;

    if(api_key == '' || api_key == null) {
        wcConfirm('Please enter Username..',"",'Okay',true,false);
        return;
    }
    else if(sender_id == '' || sender_id == null) {
        wcConfirm('Please enter Password..',"",'Okay',true,false);
        return;
    }
    else if(extraId == '' || extraId == null) {
        wcConfirm('Please enter Sender Id..',"",'Okay',true,false);
        return;
    }
    else if(api_key == credentials['apikey'] && sender_id == credentials['senderId'] && extraId == credentials['extraId']) {
        wcConfirm('No Changes..',"$('#Error').hide()",'Okay',true,false);
        return;
    }
    else {

        wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingLoaderInner">${loader}</div><div class="sendingLoaderHint">Saving...</div></div>`,'','Okay',true,true);

        credentials['apikey'] = api_key;
        credentials['senderId'] = sender_id;                
        credentials['extraId'] = extraId;
        updateOrgVariables(extensionCredential, credentials);

    }
    
}

var loader = `<div class="wcMsgLoadingInner" title="loading…"><svg class="wcMsgLoadingSVG" width="17" height="17" viewBox="0 0 46 46" role="status"><circle class="wcMsgLoadingSvgCircle" cx="23" cy="23" r="20" fill="none" stroke-width="6" style="stroke: rgb(57 82 234);"></circle></svg></div>`;

var verifiedTickSVG = `<svg class="sendingMsgSuccessSpan" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000"><g><rect fill="none" height="24" width="24"/></g><g><path d="M23,12l-2.44-2.79l0.34-3.69l-3.61-0.82L15.4,1.5L12,2.96L8.6,1.5L6.71,4.69L3.1,5.5L3.44,9.2L1,12l2.44,2.79l-0.34,3.7 l3.61,0.82L8.6,22.5l3.4-1.47l3.4,1.46l1.89-3.19l3.61-0.82l-0.34-3.69L23,12z M10.09,16.72l-3.8-3.81l1.48-1.48l2.32,2.33 l5.85-5.87l1.48,1.48L10.09,16.72z"/></g></svg>`;

        
function wcConfirm(htmlText, runFunc, confirmTitle, alart, buttonHide) {
    $('.wcConfirmOuter').remove();
    $('body').append(`<div class="wcConfirmOuter">
        <div class="wcConfirmInner divPopUpShow">
            <div class="wcConfirmBody">
                ${htmlText}
            </div>
            <div class="wcConfirmButt" style="display:${buttonHide?'none':'flex'};">
                <div class="wcConfirmCancel" style="display:${alart?'none':'block'};" onclick="wcConfirmHide()">Cancel</div>
                <div class="wcConfirmYes" onclick="wcConfirmHide();${runFunc}">${confirmTitle}</div>
            </div>
        </div>
    </div>`);
}

function wcConfirmHide() {

        $('.wcConfirmInner').removeClass('divPopUpHide').removeClass('divPopUpShow').addClass('divPopUpHide').hide();
        $('.wcConfirmOuter').fadeOut(0);
        setTimeout(function() { $('.wcConfirmInner').removeClass('divPopUpHide').removeClass('divPopUpShow').html('').hide().fadeOut(); $('.wcConfirmOuter').remove(); }, 300);

}

$(document).keypress(function(e) {

    if($('.wcConfirmYes').is(':visible'))
    if(e.which == 13) {
        $('.wcConfirmYes').click();
    }

});

function CredentialsUpdateShow() {
    $('.extensionSettingInput').show();
    $('.hideCredentialsDetails').hide();
    $('.editCredentialsDetails').hide();
    $('.saveSettingsButtOut').show();
}

function CredentialsUpdateHide() {

    if(credentials.apikey && credentials.senderId && credentials.extraId) {
        $("#apikeyHide").html(credentials.apikey.substring(0,5)+"&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;");
        $("#senderIdHide").html("&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;");
        $("#extraIdHide").html("&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;");

        $('.extensionSettingInput').hide();
        $('.hideCredentialsDetails').show();
        $('.editCredentialsDetails').show();        
        $('.cancelSettingsButt').show();
        $('.saveSettingsButtOut').hide();
    }
    
}