var appsConfig = {
    "ACCESS_KEY": undefined,
    "APP_UNIQUE_ID": undefined
};
var initialAppsConfig = {
    "ACCESS_KEY": undefined,
    "APP_UNIQUE_ID": undefined
};
var curId = 1000;
var errorId = 1000;
var initTries = 0;
var initProcessId = 1;
var serverURL = "https://sms.ulgebra.com";
var WebhookURL="";
var WebhookURL2="";
var calendlyURL="";
var channelsSettings=[];
var users=[];
var channelsLength;
var phoneFieldSettings={};
var oldApiKey = null;

const urlParams = new URLSearchParams(window.location.search);
const serviceOrigin = urlParams.get('serviceOrigin');
console.log(serviceOrigin);
function syncInputValues() {

}
function resolveCurrentProcessView(id) {
    id = id?id:"";
    $('#input-api-key'+id).val(appsConfig["ACCESS_KEY"+id]);
    if (valueExists(appsConfig["ACCESS_KEY"+id])) {
        $('#input-api-key'+id).attr({'readonly':true}).val(appsConfig["ACCESS_KEY"+id].substr(0,5)+"xxxxxxxxxxxxx");
        
        fetchWebhookInfoAndExecute((function(hookId){
            if(hookId === null){
                $("#incoming-integ-status"+id).css({"background-color": "grey", "color" : "white"}).html('<i class="material-icons">error</i> Reset configurate and save Access Key ');
                $("#crmMappingSettings").css({'opacity': '0.4'});
            }else{
                $("#incoming-integ-status"+id).css({"background-color": "green", "color" : "white"}).html('<i class="material-icons" style="float: left;margin-top: -2px;margin-right: 5px;">check_circle</i> MessageBird - Integration enabled');
                $("#crmMappingSettings").css({'opacity': '1'});
            }
        }),id);
    }
    else {
        $('#input-api-key'+id).removeAttr('readonly');
        $("#incoming-integ-status"+id).css({"background-color": "crimson", "color" : "white"}).html('<i class="material-icons">error</i> Provide Access Key to enable integration');
        $("#crmMappingSettings").css({'opacity': '0.4'});
    }
}
function showHelpWinow(manual){
    if(!valueExists(appsConfig.ACCESS_KEY)){
        showHelpItem('step-1');
    }
    else{
        if(manual){
            showHelpItem('step-youtube');
        }
    }
}
function resetPhoneNumerVal(id) {
    $('#phone-select-options').html("");
    $('#phone-select-label').text('Select a phone number');
    $('#phone-select-label').attr({'data-selectedval': 'null'});
}
function selectDropDownItem(elemId, val) {
    $('.dropdown-holder').hide();
    $('#' + elemId).text(val);
    $('#' + elemId).attr({'data-selectedval': val});
}

function showDropDown(elemId) {
    if($('#' + elemId).is(":visible")){
        $('#' + elemId).hide();
    }
    else{
        $('#' + elemId).show();
        if(elemId === 'phone-select-options'){
            fetchPhoneNumbersAndShow();
        }
    }
    
}
function valueExists(val) {
    return val !== null && val !== undefined && val.length > 1 && val!=="null";
}
function saveAPIKey(id) {
    id = id?id:"";
    var accessKey = $('#input-api-key'+id).val();
    
    if(!valueExists(accessKey)){
        showErroWindow('Access Key is missing','Kindly provide AUTH ID, then proceed to save.');
        return;
    }
    
    var authtokenChange = !valueExists(appsConfig["ACCESS_KEY"+id]);
    if(authtokenChange){
        
        var reqObj = {
                url: `https://rest.messagebird.com/balance`,
                headers: {
                    "Authorization": "AccessKey "+accessKey
                },
            };
            var phoneNumberProcess = curId++;
            showProcess(`Validating given configuration...`, phoneNumberProcess);

            ZOHO.CRM.HTTP.get(reqObj).then(function (response) {
                var responseJSON = JSON.parse(response);
                processCompleted(phoneNumberProcess);
                if (responseJSON.type != null) {
                    var authIdProcess = curId++;
                    showProcess('Saving MessageBird Access Key ...', authIdProcess);
                    appsConfig["ACCESS_KEY"+id] = accessKey;
                    initialAppsConfig["ACCESS_KEY"+id] = accessKey;
                    var apiname = 'messagebirdforzohocrm__accessKey';
                    var value = accessKey;
                    if(id){
                        apiname = "messagebirdforzohocrm__phonefields";
                        phoneFieldSettings["apiKey"+id]= accessKey;
                        value = phoneFieldSettings;
                    }
                    ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": apiname,"value": value}).then(function (res) {
                        console.log(res);
                        processCompleted(authIdProcess);
                        createApplicationInMessageBird(id);
                        getChannels(id); 
                        resolveCurrentProcessView(id);
                    }).catch(function (err) {
                        processCompleted(authIdProcess);
                        console.log(err);
                    });
                }
                else if(responseJSON.errors){
                    let errText = responseJSON.errors[0].description;
                    if(errText.indexOf("incorrect access_key")>0){
                        showInvalidCredsError();
                    }
                    else{
                       showErroWindow('Unable to Integrate', errText);
                    }
                }
                else{
                    showTryAgainError();
                }
            }).catch(function (err) {
                processCompleted(phoneNumberProcess);
                showTryAgainError();
                console.log(err);
            });
        }
}
function showTryAgainError(){
    showErroMessage('Try again later');
}
function showInvalidCredsError(){
    showErroMessage('Given MessageBird API Key is Invalid <br><br> Try again with proper MessageBird credentials from <a href="https://dashboard.messagebird.com/en/developers/access" title="Click to go to MessageBird dashboard" target="_blank" noopener nofollow>MessageBird dashboard</a>.');
}


function resetAllFields(id){
    if(id){
        if(valueExists(appsConfig["ACCESS_KEY"+id])){
            phoneFieldSettings["apiKey"+id]= '';
        ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "messagebirdforzohocrm__phonefields","value": phoneFieldSettings}).then(function (res) {
            fetchWebhookInfoAndExecute(deleteApplicationInMessageBird,id);
                    appsConfig.ACCESS_KEY = null;
                        console.log(res);
                        resolveCurrentProcessView(id);
                    }).catch(function (err) {
                        showTryAgainError();
                        console.log(err);
                    });
                }
    }
    else if(valueExists(appsConfig.ACCESS_KEY)){
    ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "messagebirdforzohocrm__accessKey","value": "null"}).then(function (res) {
        fetchWebhookInfoAndExecute(deleteApplicationInMessageBird);
                    appsConfig.ACCESS_KEY = null;
                        console.log(res);
                        resolveCurrentProcessView();
                    }).catch(function (err) {
                        showTryAgainError();
                        console.log(err);
                    });
                }
}

function fetchWebhookInfoAndExecute(callback,id){
    if(!appsConfig["ACCESS_KEY"+id] && !initialAppsConfig["ACCESS_KEY"+id]){
        return;
    }
    var reqObj = {
        url: `https://conversations.messagebird.com/v1/webhooks`,
        headers: {
            "Authorization": "AccessKey "+(appsConfig["ACCESS_KEY"+id] === null ? initialAppsConfig["ACCESS_KEY"+id] : appsConfig["ACCESS_KEY"+id])
        },
    };
    var applicationProcess = curId++;
    showProcess(`Checking Zoho CRM application in MessageBird ...`, applicationProcess);
    ZOHO.CRM.HTTP.get(reqObj).then(function (response) {
        processCompleted(applicationProcess);
        var callbackURL = WebhookURL;
        if(id && id == '2'){
            callbackURL = WebhookURL2;
        }
        var data = JSON.parse(response);
        if (!data.errors) {
            if (!(data instanceof Object)) {
                data = JSON.parse(data);
            }
            if(data.count === 0){
                return callback(null);
            }
            else{
              for(var i in data.items){
                  var webhookJSON = data.items[i];
                  if(webhookJSON.url === callbackURL){
                      callback(webhookJSON.id,id);
                  }
              }  
              return;
            }
        }
        else if(data.errors){
             let errText = data.errors[0].description;
                    if(errText.indexOf("incorrect access_key")>0){
                        showInvalidCredsError();
                    }
                    else{
                       showErroWindow('Unable to Integrate', errText);
                    }
        }
        else{
            showTryAgainError();
        }
    }).catch(function (err) {
        showTryAgainError();
        processCompleted(applicationProcess);
        console.log(err);
    });
}
function saveNamespace(id){
    id=id?id:"";
    phoneFieldSettings["namespace"+id]=document.getElementById("namespace"+id).value.trim();
    updateOrgVariables("messagebirdforzohocrm__phonefields",phoneFieldSettings);
}
function savePhoneField(module,phoneField){
    if(!phoneFieldSettings["phoneField"]){
        phoneFieldSettings["phoneField"] = {};
    }
    phoneFieldSettings["phoneField"][module]=phoneField;
    updateOrgVariables("messagebirdforzohocrm__phonefields",phoneFieldSettings);
}
function updateOrgVariables(apiname,value,key){
    showProcess(`Saving configuration...`,"saveChannel" );
    ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": apiname,"value": value}).then(function(res){
        processCompleted("saveChannel");
    });
}
function getWebhookURL(){
    var func_name = "messagebirdforzohocrm__sendmessage";
    var req_data ={"action": "getApiURL"};
    var getmap = {"nameSpace":"<portal_name.extension_namespace>"};
    return Promise.all([ZOHO.CRM.CONNECTOR.invokeAPI("crm.zapikey",getmap)]).then(function(resp){
        if(document.getElementById("namespace")){
            document.getElementById("namespace").value = phoneFieldSettings["namespace"]?phoneFieldSettings["namespace"]:"";
        }
        if(document.getElementById("namespace2")){
            document.getElementById("namespace2").value = phoneFieldSettings["namespace2"]?phoneFieldSettings["namespace2"]:"";
        }
        domain = "https://platform.zoho.com";
        if(serviceOrigin && serviceOrigin.indexOf('.zohosandbox.') != -1){
            domain = "https://plugin-messagebirdforzohocrm.zohosandbox.com";
        }
        else if(serviceOrigin){
            domain = "https://platform.zoho."+serviceOrigin.split('.')[2];
        }
        domain = domain.trim();
        var zapikey = JSON.parse(resp[0]).response;
        calendlyURL = domain+"/crm/v2/functions/messagebirdforzohocrm__calendlyhandler/actions/execute?auth_type=apikey&zapikey="+zapikey;
        WebhookURL = domain+"/crm/v2/functions/messagebirdforzohocrm__webhookhandler/actions/execute?auth_type=apikey&zapikey="+zapikey;
        WebhookURL2 = WebhookURL+"&type=2";
        var sendMessageapi=domain+"/crm/v2/functions/messagebirdforzohocrm__sendmessage/actions/execute?auth_type=apikey&zapikey="+zapikey;
        document.getElementById("webHookurl").innerText =sendMessageapi;
    });
}    

function deleteApplicationInMessageBird(webhookId,id){
    if(!valueExists(webhookId)){
        return;
    }
    var reqObj = {
        url: `https://conversations.messagebird.com/v1/webhooks/${webhookId}/`,
        headers: {
            "Authorization": "AccessKey "+(appsConfig["ACCESS_KEY"+id] === null ? initialAppsConfig["ACCESS_KEY"+id] : appsConfig["ACCESS_KEY"+id])
        },
    };
    var applicationProcess = curId++;
    showProcess(`Removing existing Zoho CRM application in MessageBird ...`, applicationProcess);
    ZOHO.CRM.HTTP.delete(reqObj).then(function (response) {
        processCompleted(applicationProcess);
        var responseJSON = JSON.parse(response);
        if (!responseJSON.errors) {
            resolveCurrentProcessView(id);
            window.location.reload();
        }
    }).catch(function (err) {
        showTryAgainError();
        processCompleted(applicationProcess);
        console.log(err);
    });
}

function closeAllErrorWindows(){
    $('.error-window-outer').remove();
}

function createApplicationInMessageBird(id){
    var callbackURL = WebhookURL;
    if(id && id == '2'){
        callbackURL = WebhookURL2;
    }
    var reqObj = {
        url: `https://conversations.messagebird.com/v1/webhooks`,
        headers: {
            "Authorization": "AccessKey "+appsConfig["ACCESS_KEY"+id],
            "Content-Type": "application/json"
        },
        body: {
            "url" : callbackURL,
            "events": ["message.created", "message.updated"]
        }
    };
    var applicationProcess = curId++;
    showProcess(`Configuring Zoho CRM application in MessageBird ...`, applicationProcess);
    ZOHO.CRM.HTTP.post(reqObj).then(function (response) {
        processCompleted(applicationProcess);
        var responseJSON = JSON.parse(response);
        if (!responseJSON.errors) {
            resolveCurrentProcessView(id); 
        }
        else if (responseJSON.errors){
            resetAllFields(id);
            let errText = responseJSON.errors[0].description;
            if(errText.indexOf("incorrect access_key")>0){
                showInvalidCredsError();
            }
            else{
                if(errText.indexOf("You cannot create more than")==0){
                    errText+=`<br><br> <a href="https://ulgebra.com/messagebird-cliq-help" target="_blank"><button style="color: #fff;background-color: royalblue;padding: 5px 20px 5px 10px;border-radius: 25px;box-shadow: 0px 5px 5px rgba(0,0,0,0.1);">Delete Unwanted Webhooks</button></a>`;
                }
                showErroWindow('Unable to Integrate', errText);
            }
        }
        else{
            resetAllFields(id);
            showTryAgainError();
        }
    }).catch(function (err) {
        showTryAgainError();
        processCompleted(applicationProcess);
        console.log(err);
    });
}

function showProcess(text, id){
    $(".process-window-outer").show();
    $("#process-window-items").append(`<div id="process-item-${id}" class="process-window-item"> <i class="material-icons spinnow">toys</i> ${text}</div>`);
}

function showErroMessage(html){
    showErroWindow('Unable to process your request', html);
}
function showErroWindow(title, html){
    var id = errorId++;
   $('body').append(`<div class="error-window-outer" style="display:block;" id="error-window-${id}">
            <div class="error-window-inner">
                <div class="error-window-title">
                    ${title}
                </div>
                <div class="error-window-detail">
                    ${html}
                </div>
                <div class="error-window-close" onclick="removeElem('#error-window-${id}')">
                   <i class="material-icons">close</i> Close
                </div>
            </div>
        </div>`);
}
function removeElem(sel){
    $(sel).remove();
}
function hideElem(sel){
    $(sel).hide();
}
function showHelpItem(helpId){
    $('#helpWindow').show();
    $('.help-window-item').hide();
    $("#"+helpId).show();
}
function processCompleted(id){
    $(`#process-item-${id}`).remove();
    if(($("#process-window-items").children().length) === 0){
        $(".process-window-outer").hide();
    }
}
function initializeFromConfigParams(){
    initTries++;
    if(initTries > 3){
        showErroMessage('Kindly refresh the page to start the configuration.');
        return;
    }
    var applicationProcess = curId++;
    if(initTries === 1){
        showProcess(`Initializing app ...`, initProcessId);
    }
        showProcess(`Fetching application configuration ...`, applicationProcess);
        Promise.all([ZOHO.CRM.API.getOrgVariable("messagebirdforzohocrm__accessKey"),getWebhookURL()]).then(function(apiKeyData){
            setPhoneFields();
            showCalendlySettings();

            if(apiKeyData[0] && apiKeyData[0].Success && apiKeyData[0].Success.Content && valueExists(apiKeyData[0].Success.Content)){
                processCompleted(applicationProcess);
                appsConfig.ACCESS_KEY = apiKeyData[0].Success.Content;
                initialAppsConfig.ACCESS_KEY = apiKeyData[0].Success.Content;
            }
            if(phoneFieldSettings["apiKey2"]){
                appsConfig.ACCESS_KEY2 = phoneFieldSettings["apiKey2"];
                initialAppsConfig.ACCESS_KEY2 = phoneFieldSettings["apiKey2"];
            }
            if(phoneFieldSettings.isSecondAccountSupported){
                $("#acc2name").show();
                $("#api-key-form2").show();
            }
            if(phoneFieldSettings.isSecondAccountSupported && !document.getElementById("namespace2").value){
                getChannels('2');
            }
            if(!document.getElementById("namespace").value){
                getChannels();
            }
            blockedChannels = phoneFieldSettings.blockedChannels?phoneFieldSettings.blockedChannels:[];
            getAllChannels();
            showHelpWinow(false);
            processCompleted(applicationProcess);
            processCompleted(initProcessId);
            // if(valueExists(appsConfig.APP_UNIQUE_ID)){
            //     processCompleted(initProcessId);
            // }
            // else{
            //     setTimeout(initializeFromConfigParams, 5000);
            // }
            if(phoneFieldSettings.isSecondAccountSupported){
                resolveCurrentProcessView('2');
            }
            resolveCurrentProcessView();
        }).catch(function (err) {
            showTryAgainError();
            processCompleted(applicationProcess);
            console.log(err);
        });

}
function saveChannel(saveButton){
    var saveElement = saveButton.parentElement;
    var channelIndex =saveElement.id.substr(8);
    var channelName =saveElement.children[0].children[0].children[0].value;
    var channelId = saveElement.children[2].children[0].value;
    var userId = saveElement.children[3].children[0].children[0].value;
    var isNewChannel = true;
     if(!valueExists(channelId)){
        showErroWindow('Channel ID is empty','Kindly provide Channel ID, then proceed to save.');
        return;
    }
    showProcess(`Saving Channel configuration...`,"saveChannel" );
    channelsSettings[channelIndex]={"channelName":channelName,"channelId":channelId,"userId":userId};
    var NewChannelSettings = [];
    for (var i =0; i < channelsSettings.length; i++) {
        if(channelsSettings[i]){
            NewChannelSettings.push(channelsSettings[i]);
        }    
    }
    ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "messagebirdforzohocrm__channels","value": NewChannelSettings}).then(function(resp){
        processCompleted("saveChannel");
    });
}
function deleteChannel(deleteIcon){
    var deleteElement = deleteIcon.parentElement;
    var channelIndex =deleteElement.id.substr(8);
    channelsSettings[channelIndex] = null;
    if(channelIndex < channelsSettings.length){
        var NewChannelSettings = [];
        for (var i =0; i < channelsSettings.length; i++) {
            if(channelsSettings[i]){
                NewChannelSettings.push(channelsSettings[i]);
            }    
        }
        ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "messagebirdforzohocrm__channels","value": NewChannelSettings});
    }
    deleteElement.parentElement.removeChild(deleteElement);
    // $('#channels').html('');
  
    //     $('#channels').append(`<div class="fieldMapp" id="channelsHeader">
    //                 <div  style="min-width:30%;text-align:left;float:left;">
    //                     <h5>Channel Name</h5>
    //                 </div>
    //                 <div  style="min-width:35%;text-align:left;float:left;">
    //                      <h5><a href="https://dashboard.messagebird.com/en/channels/" target="_blank" noopener nofollow>Channel Id</a></h5>
    //                 </div>
    //                 <div  style="min-width:30%;text-align:left;float:left;">
    //                      <h5>Allow Reply Access</h5>
    //                 </div>
    //             </div>`);
    if(channelsSettings.length == 0){
        document.getElementById("channelsHeader").style.display="none";
    }
    channelsLength = channelsSettings.length;
    // for (var i =0; i < channelsSettings.length; i++) {
    //     if(channelsSettings[i]){
    //         addChannelSetting(channelsSettings[i],i); 
    //     }
    // }
}
function addChannel(){
    var channels =["WhatsApp","Telegram","Line","WeChat","Facebook Messenger","Instagram","SMS","Email"];
    var options ="";
    channels.forEach(function(channel){
        options =options+'<option value="'+channel+'">'+channel+'</option>';
    });
    var accessUsers ='<option value="everyone">Everyone</option>';
    var allowedUser ='';
    users.forEach(function(user){
            accessUsers =accessUsers+'<option value="'+user.id+'">'+user.email+'</option>';
    });
    var channelUi = '<div class="fieldMapp" id="channel_'+channelsLength+'"><div class="fromField"><div class="styled-select slate fieldMap"><select >'+options+'</select></div></div><i style="float:left;min-width:4%;text-align:center;" class="material-icons ">arrow_forward</i><div class="toField"><input style="width:320px;height:35px;" type="text"></input></div><div class="fromField" style="width:23%;padding-left:5px;"><div class="styled-select slate fieldMap" style="width:230px;"><select name="access" style="width:230px;">'+accessUsers+'</select></div></div><div class="btn-save" class="btn-save" style="float:left;height:32px;" onclick="saveChannel(this)">Save</div><i style="float:left;min-width:4%;text-align:center;padding-top:5px;" class="material-icons" onclick="deleteChannel(this)" >delete</i></div>';
    // var channelUi = '<div class="fieldMapp" id="'+currentChannel+'"><div class="fromField"><div class="styled-select slate fieldMap"><select >'+options+'</select></div></div><i style="float:left;min-width:4%;text-align:center;" class="material-icons ">arrow_forward</i><i style="float:right;min-width:4%;text-align:center;" class="material-icons" onclick="deleteChannel(this)" >delete</i><div class="btn-save" style="float:right;min-width:10%;" onclick="saveChannel(this)">Save</div><div class="toField"><input style="width:320px;height:35px;" type="text"></input></div></div>';
    channelsSettings[channelsLength] = null;
    channelsLength = channelsLength+1;
    $('#channels').append(channelUi);
    document.getElementById("channelsHeader").style.display="block";
}
function addChannelSetting(channel,i){
    var channelNames =["WhatsApp","Telegram","Line","WeChat","Facebook Messenger","Instagram","SMS","Email"];
    var options ="";
    options =options+'<option value="'+channel.channelName+'">'+channel.channelName+'</option>';
    channelNames.forEach(function(channelName){
        if(channelName != channel.channelName){
            options =options+'<option value="'+channelName+'">'+channelName+'</option>';
        }
    });
    addOrDeleteChannel ='<i style="float:left;min-width:4%;text-align:center;padding-top:5px;cursor:pointer;" class="material-icons" onclick="deleteChannel(this)" >delete</i>';
    var accessUsers ='<option value="everyone">Everyone</option>';
    var allowedUser ='';
    users.forEach(function(user){
        if(user.id == channel.userId){
            allowedUser = '<option value="'+user.id+'">'+user.email+'</option>';
        }
        else{
            accessUsers =accessUsers+'<option value="'+user.id+'">'+user.email+'</option>';
        }
    });
    accessUsers=allowedUser+accessUsers;
    var channelUi = '<div class="fieldMapp" id="channel_'+i+'"><div class="fromField"><div class="styled-select slate fieldMap"><select >'+options+'</select></div></div><i style="float:left;min-width:4%;text-align:center;" class="material-icons ">arrow_forward</i><div class="toField"><input style="width:320px;height:35px;" type="text" value="'+channel.channelId+'" ></input></div><div class="fromField" style="width:23%;padding-left:5px;"><div class="styled-select slate fieldMap" style="width:230px;"><select style="width:230px;" name="access" >'+accessUsers+'</select></div></div><div class="btn-save" class="btn-save" style="float:left;height:32px;" onclick="saveChannel(this)">Save</div>'+addOrDeleteChannel+'</div>';
     // var channelUi = '<div class="fieldMapp" id="'+channel.channelName+'"><div class="fromField"><div class="styled-select slate fieldMap"><select >'+options+'</select></div></div><i style="float:left;min-width:4%;text-align:center;" class="material-icons ">arrow_forward</i>'+addOrDeleteChannel+'<div class="btn-save" style="float:right;min-width:10%;" onclick="saveChannel(this)">Save</div><div class="toField"><input style="width:320px;height:35px;" type="text" value="'+channel.channelId+'" ></input></div></div>';
    $('#channels').append(channelUi);
}
function setPhoneFields(){
    var moduleList =["Leads","Contacts"];
    Promise.all([ZOHO.CRM.META.getFields({"Entity":"Leads"}),ZOHO.CRM.META.getFields({"Entity":"Contacts"})]).then(function(fieldsData){
        for(let i=0;i<moduleList.length;i++){
            var options = '';
            var selectedOption;
            fieldsData[i].fields.forEach(function(field){
                if(field.data_type == "phone"){
                    if(phoneFieldSettings && phoneFieldSettings["phoneField"] && field.api_name == phoneFieldSettings["phoneField"][moduleList[i]]){
                        selectedOption=`<option value="${field.api_name}">${field.field_label}</option>`;
                    }
                    else{
                        options=`${options}<option value="${field.api_name}">${field.field_label}</option>`;
                    }
                }    
            });
            if(selectedOption){
                options=`${selectedOption}${options}`;
            }
            else{    
                options = `<option value="select">Select Phone Field</option>${options}`;
            }
            $("#crmMappingSettings").append(`<tr id="${moduleList[i]}PhoneField">
            <td>
                                <label>Default Phone Number Field for ${moduleList[i]}</label> <br>

    <i style="font-size:12px">to be considered while sending bulk or automated SMS &amp; WhatsApp messages</i>

</td>
                              <td>
                                <div class="styled-select slate">
                                <select  name="field5" onchange="savePhoneField('${moduleList[i]}',this.value)">
                                    ${options}
                                  </select>
                                </div>
                              </td>
                          </tr>`);
        }
         $("#contentDiv").append(`<div style="min-height:100px;"></div>`);
    });
}
function setCalPhoneFields(){
    var templateList="";
    var fields=["Text_reminder_number","Location"];
    for(let i=1;i<=10;i++){
        fields.push("Answer_"+i);
    }
    var selectedOption='<option value="Select PhoneField">Select PhoneField</option>';
    
    for(let i=0;i <fields.length;i++){
        if(fields[i] != phoneFieldSettings["watsappfield"]){
            templateList =templateList+ '<option value="'+fields[i]+'">'+fields[i]+'</option>';
        }
        else{
            selectedOption = '<option value="'+fields[i]+'">'+fields[i]+'</option>';
        }
    }
    $('#watsappfields').append(selectedOption+templateList);
}
function getCalTemplates(){
    ZOHO.CRM.API.searchRecord({Entity:"messagebirdforzohocrm__MessageBird_Templates",Type:"criteria",Query:"(messagebirdforzohocrm__Module_Name:equals:calendly)",delay:false}).then(function(data){
        var templateList="";
        var selectedOption='<option value="Select Template">Select Template</option>';
        if(data.data){
            for(let i=0;i <data.data.length;i++){
                if(data.data[i].id != phoneFieldSettings["calTemplateId"]){
                    templateList =templateList+ '<option value="'+data.data[i].id+'">'+data.data[i].Name+'</option>';
                }
                else{
                    selectedOption = '<option value="'+data.data[i].id+'">'+data.data[i].Name+'</option>';
                }
            }
        }
        $('#calTemplates').append(selectedOption+templateList);
    }) 
}
function showCalendlySettings(){
    if(!document.getElementById("calapikey")){
        $("#crmMappingSettings").append(`<tr>
                              <td>
                                <label>Calendly API Key<label>
                                    <br>
                                    
                                    <i style="font-size:12px">Send automated WhatsApp messages for your Calendly appointments</i>
                                    
                             </td>
                              <td>
                              
                                  <input type="text" class="cainput" placeHolder="Enter your Calendly API Key" id="calapikey" value="" onchange="saveCalendlyDetails('calendlyapikey',this.value)">
                                
                              
                                  <input type="text" class="cainput" placeHolder="Enter your WhatsApp ChannelId" id="watchannelId" value="" onchange="saveCalendlyDetails('watchannelId',this.value)">
                               
                                 <div class="styled-select slate">
                                 <select  name="field5" id="calTemplates" onchange="saveCalendlyDetails('calTemplateId',this.value)">
                                    
                                  </select>
                                  </div>
                                  <div style="margin-top:10px;" class="styled-select slate">
                                     <select  name="field5" id="watsappfields" onchange="saveCalendlyDetails('watsappfield',this.value)">
                                        
                                      </select>
                                  </div>
                              </td>
                          </tr>`);
    }
    getCalTemplates();
    setCalPhoneFields();
    if(phoneFieldSettings["calendlyapikey"]){
        document.getElementById("calapikey").value = phoneFieldSettings["calendlyapikey"];
    }
    if(phoneFieldSettings["watchannelId"]){
        document.getElementById("watchannelId").value = phoneFieldSettings["watchannelId"];
    }
}   
function saveCalendlyDetails(key,value){
    if(key == "calendlyapikey"){
        if(value){
            addCalendlyWebHook(value)
        }
        else{
            deleteCalendlyWebHook(phoneFieldSettings["calendlyapikey"] ,phoneFieldSettings["calendlyHookId"])
        } 
    }
    else{
        phoneFieldSettings[key]=value;
        updateOrgVariables("messagebirdforzohocrm__phonefields",phoneFieldSettings);
    }
}     
function addCalendlyWebHook(newApiKey){
    var apiKey = phoneFieldSettings["calendlyapikey"];
    var calendlyHookId = phoneFieldSettings["calendlyHookId"] 
    if(newApiKey)
    {    
        var reqObj = {
            url: `https://calendly.com/api/v1/hooks`,
            headers: {
                "X-TOKEN":newApiKey,
                "Content-Type": "application/json"
            },
            body: {
                "url" : calendlyURL,
                "events": ["invitee.created","invitee.canceled"]
            }
        };
        var applicationProcess = curId++;
        showProcess(`Configuring Zoho CRM application in Calendly ...`, applicationProcess);
        ZOHO.CRM.HTTP.post(reqObj).then(function (response) {
            console.log("response"+response);
            processCompleted(applicationProcess);
            if(JSON.parse(response).id){
                deleteCalendlyWebHook(apiKey,calendlyHookId);
                phoneFieldSettings["calendlyHookId"]=JSON.parse(response).id;
                phoneFieldSettings["calendlyapikey"]=newApiKey;
                updateOrgVariables("messagebirdforzohocrm__phonefields",phoneFieldSettings);
            }
            else{
                showErroMessage(response);
            }    
        });    
    }
}
function deleteCalendlyWebHook(apiKey,calendlyHookId){
    if(apiKey && calendlyHookId){
        var reqObj = {
            url: `https://calendly.com/api/v1/hooks/${calendlyHookId}`,
            headers: {
                "X-TOKEN":apiKey,
                "Content-Type": "application/json"
            },
        };
        ZOHO.CRM.HTTP.delete(reqObj).then(function (response) {
            console.log(response);
            if(phoneFieldSettings["calendlyHookId"] == calendlyHookId){
                phoneFieldSettings["calendlyHookId"]="";
            }
            if(phoneFieldSettings["calendlyapikey"] == apiKey){
                 phoneFieldSettings["calendlyapikey"]="";
            }    
            updateOrgVariables("messagebirdforzohocrm__phonefields",phoneFieldSettings);
        }); 
    }       
}
function redirectToSettings(){
      ZOHO.CRM.UI.Widget.open({Entity:"MessageBird"})
      .then(function(data){
        console.log(data);
      })
}
var blockedChannels =[];
async function getAllChannels(){
    try{
        var channels = await ZOHO.CRM.HTTP.get({
            url : "https://integrations.messagebird.com/v1/public/integrations",
            headers:{
                    Authorization:"AccessKey "+appsConfig["ACCESS_KEY"],
            }
        });
        var channelsListUi='';
        channels = JSON.parse(channels).items;
        channels.forEach(function(channel){
            if(channel.status == "active"){
                $('#channels').append('<div class="channelItem" id="'+channel.id+'"><span>'+(channel.name?channel.name:channel.id) +'('+channel.slug+')</span><span style="float:right;"><label  class="switch2"><input id="'+channel.id+'_enable" type="checkbox" onclick="enableDisableChannel('+"'"+channel.id+"'"+')"><span class="slider2 round"></span></label></span></div>');
                document.getElementById(channel.id+'_enable').checked = (blockedChannels.indexOf(channel.id) == -1);
            }
        })
    }
    catch(e){
        console.log(e);
    }    
}
function enableDisableChannel(channelId){
    event.stopPropagation();
    var value = document.getElementById(channelId+"_enable").checked;
    var eventIndex = blockedChannels.indexOf(channelId);
    if(value && eventIndex != -1){
        blockedChannels.splice(eventIndex, 1);
        //document.getElementById(channelId).style.backgroundColor="white";

    }
    else if(eventIndex === -1){
          blockedChannels.push(channelId);
          //document.getElementById(channelId).style.backgroundColor="#7777774d";
    }
    phoneFieldSettings["blockedChannels"]=blockedChannels;
    ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname":"messagebirdforzohocrm__phonefields","value":phoneFieldSettings});
}
function getChannels(id){
    try{
        id=id?id:'';
        if(!appsConfig["ACCESS_KEY"+id]){
            return;
        }
        var fetchchannelList = [];
        ["whatsapp"].forEach(function(channel){
            fetchchannelList.push(ZOHO.CRM.HTTP.get({
                url : "https://integrations.messagebird.com/v1/public/integrations/"+channel,
                headers:{
                        Authorization:"AccessKey "+appsConfig["ACCESS_KEY"+id],
                }
            }));
        })
        return Promise.all(fetchchannelList).then(function(data){
            console.log(data);
            var channelsList= JSON.parse(data[0]).items;
            var channelsListUi='';
            channelsList.forEach(function(channel){
                 if(channel.settings && channel.settings.channel && channel.settings.channel.namespace){
                    if(document.getElementById("namespace"+id)){
                        document.getElementById("namespace"+id).value = channel.settings.channel.namespace;
                        saveNamespace(id);
                    }    
                 }
                // channelsListUi =channelsListUi+ '<option value="'+channel.id+'">'+channel.name+'</option>';
            })
            //$('#contact-channel-target').append(channelsListUi);
        });
    }
    catch(e){
        console.log(e);
    }
}
window.onload = function () {
    window.document.body.insertAdjacentHTML( 'afterbegin', `<div class="contactme">If you need help for configuration of this app, kindly mail to <a href="mailto:ulgebra@zoho.com">ulgebra@zoho.com</a> or <a target="_blank" href="https://wa.me/917397415648?text=Hello%20Ulgebra,%20I%20have%20a%20query">WhatsApp Now</a><br><br><div class="ua-brand" style=" color: grey; "> <a href="https://www.ulgebra.com/?source=appsettings" target="_blank"> an <span class="brand-brand"> <img src="https://ulgebra.com/images/ulgebra/favs/favicon.png"> Ulgebra</span> product</a></div></div>`);
    
    ZOHO.embeddedApp.init().then(function(){
        const urlParams = new URLSearchParams(window.location.search);
        const tab = urlParams.get('tab');
        if(tab == "settings"){
            redirectToSettings();
        } 
        ZOHO.CRM.API.getOrgVariable("messagebirdforzohocrm__phonefields").then(function(resp){
            if(resp && resp.Success && resp.Success.Content && resp.Success.Content != "" ){
                phoneFieldSettings = JSON.parse(resp.Success.Content);
                initializeFromConfigParams();
            }  
        });      
        ZOHO.CRM.API.getOrgVariable("messagebirdforzohocrm__countryCode").then(function(apiKeyData){
            if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content != "0" ){
                document.getElementById("countyCode").value=apiKeyData.Success.Content;
            }   
        }); 
        ZOHO.CRM.API.getOrgVariable("messagebirdforzohocrm__Module").then(function(moduleData){
            if(moduleData && moduleData.Success && moduleData.Success.Content){
                document.getElementById("modules").value = moduleData.Success.Content;
            }   
        });
       
        ZOHO.CRM.API.getAllUsers({Type:"ActiveUsers"}).then(function(data){
            users = data.users;
            ZOHO.CRM.API.getOrgVariable("messagebirdforzohocrm__channels").then(function(apiKeyData){
                if(apiKeyData && apiKeyData.Success && valueExists(apiKeyData.Success.Content)){
                    channelsSettings = JSON.parse(apiKeyData.Success.Content);
                    channelsLength = channelsSettings.length;
                    if(channelsSettings.length){
                        for (var i =0; i < channelsSettings.length; i++) {
                            if(channelsSettings[i]){
                                addChannelSetting(channelsSettings[i],i); 
                            }
                        }
                    }    
                    else{
                        if(document.getElementById("channelsHeader")){
                            document.getElementById("channelsHeader").style.display="none";
                        }    
                        channelsLength=0;
                    }
                }   
            }); 
        });    
                
    });
};
