    
    var recordId;
    var recordModule;
    var ButtonPosition;
    var accessKey;
    var channelId;
    var existingConversations = [];
    var phoneFields =[];
                var RTCs = {
                    
                };
    var platform = "";
        var convId;
        var storedChannelSettings = {};
        var convChannels = {

        };
        var convData = {

        };
        var convLoaded = false;
        var userLoaded = false;
        var colors = ["#00b894","#fdcb6e","#e17055","#0984e3","#6c5ce7", "#e84393", "#2d3436", "#00cec9", "#d63031", "#b2bec3"];
        var accessConfiguredChannels = [];
        var allowedChannels = [];
        var currentUser = {};
        var messagebirdContactId;
        var ContactName;
        var storageRef = null;
        var db = null;
        var loginUser;
        var firebase;
        var approvedTemplates=[];
        var selectedChannel=null;
        var selectedTemplate=null;
        var initateChannelId=null;
        var namespace=null;
        var channelsList=[];
        var countryCode = "";
        var secondaryApp;
        var messageTemplates=[];
        var moduleFields=[];
        var currentRecord;
        var conversationModuleName;
        var messageBirdId;
        var phoneFieldsSetting={};
        var accessKey2=null;
        var messagebirdContactId2=null;
        var secondChannelsList=[];
        var namespace2=null;
        var currentAccessKey=null;
        document.addEventListener("DOMContentLoaded", function(event) {
            var firebaseConfig = {
                apiKey: "AIzaSyDsvl4lleRa8k-3UuDqltueXZy1V27f0Sw",
                authDomain: "ulgebra-license.firebaseapp.com",
                databaseURL: "https://ulgebra-license.firebaseio.com",
                projectId: "ulgebra-license",
                storageBucket: "ulgebra-license.appspot.com",
                messagingSenderId: "356364764905",
                appId: "1:356364764905:web:4bea988d613e73e1805278",
                measurementId: "G-54FN510HDW"
            };
            // // Initialize Firebase
            if(firebase){
              firebase.initializeApp(firebaseConfig);
              storageRef = firebase.storage().ref();
              db = firebase.firestore();
              firebase.auth().onAuthStateChanged(function(user) {
                if (user) {
                  loginUser = user;
                } 
              });  
            }  

async function getFields(entity) {
    return await ZOHO.CRM.META.getFields({"Entity":entity}).then(function(fields){
        return fields.fields;
    });
}

async function getRecord(entity, recordIds) {

    return await ZOHO.CRM.API.getRecord({Entity:entity,RecordID:recordIds}).then(function(data){
        return data;
    }); 

}
            ZOHO.embeddedApp.on("PageLoad", async function(record) {
                if(record.ButtonPosition == "DetailView"){
                    recordId = record.EntityId[0];
                }
                else{
                    recordId = record.EntityId;
                }  
                recordModule = record.Entity;
                conversationModuleName =recordModule.substring(0,recordModule.length-1);
                messageBirdId = "MessageBird_Id";
                if(recordModule == "Contacts" || recordModule == "Leads"){
                  conversationModuleName = "messagebirdforzohocrm__"+conversationModuleName;
                  messageBirdId = "messagebirdforzohocrm__"+messageBirdId;
                }
                // if(recordModule == "Contacts"){
                //    phoneFields = ["Mobile","Phone","Asst_Phone","Home_Phone","Other_Phone"];
                // }
                // else{
                //   phoneFields = ["Mobile","Phone"];
                // }


                ButtonPosition = record.ButtonPosition;
                ZOHO.CRM.META.getFields({"Entity":recordModule}).then(function(data){
                    moduleFields = data.fields;
                });    
                $("#prevConversations").html(`<span class="spinnow material-icons" style="font-size: 20px;display:inline-block;">toys</span> Loading conversations ...`);
                try{
                  var templateResp = await ZOHO.CRM.API.searchRecord({Entity:"messagebirdforzohocrm__MessageBird_Templates",Type:"criteria",Query:"(messagebirdforzohocrm__Module_Name:equals:"+recordModule+")",delay:false});
                  if(templateResp && templateResp.data){
                        messageTemplates = templateResp.data ;
                  }
                }
                catch(e){
                  console.log(e);
                }
                Promise.all([ZOHO.CRM.API.getRecord({Entity:recordModule,RecordID:recordId}),ZOHO.CRM.CONFIG.getCurrentUser(),ZOHO.CRM.API.getOrgVariable("messagebirdforzohocrm__accessKey"),ZOHO.CRM.API.getOrgVariable("messagebirdforzohocrm__channels"),ZOHO.CRM.API.getOrgVariable("messagebirdforzohocrm__phonefields")]).then(async function(datas){
                // ZOHO.CRM.API.getRecord({Entity:recordModule,RecordID:recordId}).then(function(data){
                    if(datas[4] && datas[4].Success && datas[4].Success.Content && datas[4].Success.Content != "0"){
                        phoneFieldsSetting =JSON.parse(datas[4].Success.Content);
                    }

                    var data =datas[0];
                    if(datas[0].data[0])
                    currentRecord = datas[0].data[0];
                    else {
                        let currentRecordRe = '';
                        currentRecordRe = ZOHO.CRM.API.getRecord({Entity:recordModule,RecordID:recordId});
                        currentRecord = currentRecordRe.data[0];
                    }
                    currentUser = datas[1];
                    Mobile = data.data[0].Mobile ;
                    var channelsResp = datas[3];
                    var apiKey = datas[2];
                    
                    messagebirdContactId = data.data[0][messageBirdId];
                    // if(!messagebirdContactId){
                    //     renderConversationList();
                    // }
                    if(phoneFieldsSetting && phoneFieldsSetting.isSecondAccountSupported && phoneFieldsSetting.apiKey2){
                      messagebirdContactId2= data.data[0]["MessageBird_Id2"];
                      accessKey2=phoneFieldsSetting.apiKey2;
                    }
                    ContactName = data.data[0].Full_Name;
                    if(apiKey && apiKey.Success && apiKey.Success.Content && !valueExists(apiKey.Success.Content)){
                        redirectToSettings();
                    }
                    else{
                        accessKey = apiKey.Success.Content;
                        currentAccessKey = accessKey;
                    }
                    var phoneList="";
                    var indexCount = 0;

let record = data.data[0];
await getFields(recordModule).then(async function(fields){
        
        let moduleFields = fields;
        
        let lookupModules = [];
        moduleFields.forEach(function(field){
            
            if(field.data_type == "phone") {
                if(record[field.api_name] != null)
                phoneFields.push(record[field.api_name]);
            }
            else if(field.data_type == "lookup"){
                if(record[field.api_name] != null)
                lookupModules.push(field);
            }   

        }); 

        if(lookupModules.length != 0) {

            for (let i = 0; i < lookupModules.length; i++) {
                let lookupModule = lookupModules[i].lookup.module.api_name;
                let lookupId = record[lookupModules[i].api_name].id;
                await getFields(lookupModule).then(async function(respFields) {
                    await getRecord(lookupModule, lookupId).then(async function(datarecord) {
                        
                        datarecord = datarecord.data[0];
                        respFields.forEach(function(field){

                            if(field.data_type == "phone") {
                                if(datarecord[field.api_name] != null)
                                phoneFields.push(datarecord[field.api_name]);
                            }       

                        });

                        if(i == lookupModules.length-1) {

                            phoneFields = phoneFields.filter((v, p) => phoneFields.indexOf(v) == p);
                            
                            phoneFields.forEach(function(field){
                                phoneList = phoneList +'<option '+(indexCount==0 ? 'selected': '')+' value="'+field+'">'+field+'</option>';
                                if(indexCount==0){
                                     $("#input-to-phone").val(field);
                                     checkPhoneNumber(field);
                                }
                                indexCount++;                            
                            });

                            $("#contact-phone-target").append(phoneList);

                        }

                    });

                });
            }

        }   
        else {

            phoneFields = phoneFields.filter((v, p) => phoneFields.indexOf(v) == p);
            phoneFields.forEach(function(field){
                phoneList = phoneList +'<option '+(indexCount==0 ? 'selected': '')+' value="'+field+'">'+field+'</option>';
                if(indexCount==0){
                     $("#input-to-phone").val(field);
                     checkPhoneNumber(field);
                }
                indexCount++;                            
            });

            $("#contact-phone-target").append(phoneList);
        }

    }); 
                    
                    getChannels();
                    getConversations().then(function(){
                      
                      renderConversationList();
                      // if(channelsResp && channelsResp.Success && channelsResp.Success.Content && !valueExists(channelsResp.Success.Content)){
                      //    // redirectToSettings();
                      // }
                      // else{
                      //     var userIds =  [];
                      //     var channelList="";
                      //     var channelsSettings = JSON.parse(channelsResp.Success.Content);
                      //     storedChannelSettings = channelsSettings;
                      //     for(let i=0;i <channelsSettings.length;i++){
                      //         accessConfiguredChannels.push(channelsSettings[i].channelId);
                      //         if(channelsSettings[i].userId != "everyone"){
                      //             userIds.push(channelsSettings[i].userId);
                      //         }
                      //         if(channelsSettings[i].channelId && (channelsSettings[i].userId == "everyone" || !channelsSettings[i].userId ||channelsSettings[i].userId == currentUser.users[0].id)){
                      //             allowedChannels.push(channelsSettings[i].channelId);
                      //             // if(!channelId){
                      //             //     channelList =channelList+ '<option selected value="'+channelsSettings[i].channelId+'">'+channelsSettings[i].channelName+'</option>'
                      //             //     channelId = channelsSettings[0].channelId;
                      //             //     platform = channelsSettings[0].channelName;
                      //             // }
                      //             // else{
                      //               // }
                      //         }
                      //     } 
                      //     if(channelList != ""){
                      //         $('#contact-channel-target').append(channelList);
                      //     } 
                      //     else if(existingConversations){
                      //          for(let i=0;i <existingConversations.length;i++){
                      //              existingConversations[i].userName = "Everyone";
                      //          } 
                      //     }
                      //     renderConversationList();
                      // }
                    });  
                }); 
            });
            ZOHO.embeddedApp.init().then(function(){
                ZOHO.CRM.API.getOrgVariable("messagebirdforzohocrm__countryCode").then(function(apiKeyData){
                    if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content != "0" ){
                        countryCode=apiKeyData.Success.Content;
                    }   
                }); 
            })
            if(document.getElementById("inputFile")){
              document.getElementById("inputFile").addEventListener("change", (function (event) {
                  $('.chatbox-outer').append(`<div class="loadingdiv"> <span class="spinnow material-icons" style="font-size: 20px;display:inline-block;">toys</span> file is uploading...</div>`);
                  if (((document.getElementById("inputFile").files[0].size / 1024) / 1024) > 10) {
                      $(".loadingdiv").remove();
                      $('.chatbox-outer').append(`<div style="background-color:red;" class="loadingdiv"> file size should be within 10 Mb.</div>`);
                      setTimeout((function(){ $(".loadingdiv").remove(); }), 2000);
                      return;
                  }
                  var file = document.getElementById("inputFile").files[0];
                  var docRef = db.collection("ulgebraUsers").doc(loginUser.uid);
                  docRef.get().then(function(doc) {
                      if (doc.exists && doc.data().trackSize) {
                          let userUsageData = doc.data();
                          if(((userUsageData.trackSize+parseInt(file.size)) <= (10*1024*1024)) && (userUsageData.trackCount < userUsageData.trackLimit) && (userUsageData.lastTrackCT+30000 < new Date().getTime())){
                              sendfile(document.getElementById("inputFile").files[0]);
                          }
                          else{
                            $(".loadingdiv").remove();
                            $('.chatbox-outer').append(`<div style="background-color:red;" class="loadingdiv"> your files upload limit exceeded,Please upgrade it.</div>`);
                            setTimeout((function(){ $(".loadingdiv").remove(); }), 2000);
                          }  
                          console.log("Document data:", doc.data());
                      } else {
                          sendfile(document.getElementById("inputFile").files[0]);
                          // doc.data() will be undefined in this case
                          console.log("No such document!");
                      }
                  }).catch(function(error) {
                      $(".loadingdiv").remove();
                      $('.chatbox-outer').append(`<div style="background-color:red;" class="loadingdiv">${error}</div>`);
                      setTimeout((function(){ $(".loadingdiv").remove(); }), 2000);
                      console.log("Error getting  document:", error);
                  });
              }));
            }
        });
   
    function redirectToSettings(){
      ZOHO.CRM.UI.Widget.open({Entity:"MessageBird"})
      .then(function(data){
          if(ButtonPosition == "DetailView"){
           setTimeout(function(){
              ZOHO.CRM.UI.Popup.close();
            },1000)
          } 
      })
    }
    function updateNamespace(){
        ZOHO.CRM.API.getOrgVariable("messagebirdforzohocrm__phonefields").then(function(phoneFieldsData){
            var phoneFieldsSetting={};
            if(phoneFieldsData && phoneFieldsData.Success && phoneFieldsData.Success.Content && phoneFieldsData.Success.Content != "0"){
                phoneFieldsSetting =JSON.parse(phoneFieldsData.Success.Content);
            }
            if(namespace){
                phoneFieldsSetting["namespace"]= namespace;
                ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "messagebirdforzohocrm__phonefields","value": phoneFieldsSetting});
            }
            else{
                namespace = phoneFieldsSetting["namespace"];
            }
            if(namespace2){
                phoneFieldsSetting["namespace2"]= namespace2;
                ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "messagebirdforzohocrm__phonefields","value": phoneFieldsSetting});
            }
            else{
                namespace2 = phoneFieldsSetting["namespace2"];
            }
        });
    }
    function getChannels(){
        var fetchchannelList = [];
        ["whatsapp","sms","whatsapp_sandbox","viber"].forEach(function(channel){
            fetchchannelList.push(ZOHO.CRM.HTTP.get({
                url : "https://integrations.messagebird.com/v1/public/integrations/"+channel,
                headers:{
                        Authorization:"AccessKey "+accessKey,
                }
            }));
        })
        if(accessKey2){
          ["whatsapp","sms","whatsapp_sandbox", "viber"].forEach(function(channel){
              fetchchannelList.push(ZOHO.CRM.HTTP.get({
                  url : "https://integrations.messagebird.com/v1/public/integrations/"+channel,
                  headers:{
                          Authorization:"AccessKey "+accessKey2,
                  }
              }));
          })
        }  
        return Promise.all(fetchchannelList).then(function(data){
            console.log(data);
            var i =0;
            data.forEach(function(item){
              if(i< 3){
                if(JSON.parse(item).items){
                  channelsList = channelsList.concat(JSON.parse(item).items);
                }
              }
              else{
                if(JSON.parse(item).items){
                  secondChannelsList = secondChannelsList.concat(JSON.parse(item).items);
                }
              } 
              i++; 
            })
            var channelsListUi='';
            channelsList.forEach(function(channel){
                if(channel.settings && channel.settings.channel && channel.settings.channel.namespace){
                    namespace = channel.settings.channel.namespace;
                }
                accessConfiguredChannels.push(channel.id);
                allowedChannels.push(channel.id);
                channelsListUi =channelsListUi+ '<option value="'+channel.id+'">'+channel.name+' ('+channel.slug+') '+'</option>';
            })
            secondChannelsList.forEach(function(channel){
                if(channel.settings && channel.settings.channel && channel.settings.channel.namespace){
                    namespace2 = channel.settings.channel.namespace;
                }
                accessConfiguredChannels.push(channel.id);
                allowedChannels.push(channel.id);
                channelsListUi =channelsListUi+ '<option value="2_'+channel.id+'">'+channel.name+' ('+channel.slug+')(Second Account) '+'</option>';
            })
            updateNamespace();
            $('#contact-channel-target').append(channelsListUi);
            $('#initiateConv').show();
        });
    }
    async function getConversations(){
        var fetchContactsList = [];
        if(messagebirdContactId){
            var request ={
                url : "https://conversations.messagebird.com/v1/conversations/contact/"+messagebirdContactId,
                headers:{
                        Authorization:"AccessKey "+accessKey,
                }
            }
            fetchContactsList.push(ZOHO.CRM.HTTP.get(request));
        }
        if(messagebirdContactId2 && accessKey2){
            var request ={
                url : "https://conversations.messagebird.com/v1/conversations/contact/"+messagebirdContactId2,
                headers:{
                        Authorization:"AccessKey "+accessKey2,
                }
            }
            fetchContactsList.push(ZOHO.CRM.HTTP.get(request));
        }

        
        fetchContactsList.push(ZOHO.CRM.API.searchRecord({Entity:"messagebirdforzohocrm__MessageBird_Conversations",Type:"criteria",Query:"("+conversationModuleName+":equals:"+recordId+")",delay:false}));
        return Promise.all(fetchContactsList).then(async function(data){
            console.log(data);
            var conversations1 = [];
            var convFetchList1=[];
            var conversations2 = [];
            var convFetchList2=[];
            var ind = 0;
            if(messagebirdContactId && data[0] && JSON.parse(data[0]).items){
                conversations1 = JSON.parse(data[0]).items;
                ind++;
            }
            if(messagebirdContactId2 && data[ind] && JSON.parse(data[ind]).items){
                conversations2 = JSON.parse(data[ind]).items;
                ind++;
            }
            if(data[ind] && data[ind].data){
                data[ind].data.forEach(function(mCon){
                    if(conversations1.indexOf(mCon.messagebirdforzohocrm__Conversation_Id) == -1 && conversations2.indexOf(mCon.messagebirdforzohocrm__Conversation_Id) == -1){
                      if(mCon.MessageBird_Account == "Second Account"){
                        conversations2.push(mCon.messagebirdforzohocrm__Conversation_Id);
                      }
                      else{
                        conversations1.push(mCon.messagebirdforzohocrm__Conversation_Id);
                      }
                    }
                });    
            } 
            conversations1.forEach(async function(convId){
                var request ={
                    url : "https://conversations.messagebird.com/v1/conversations/"+convId,
                    headers:{
                        Authorization:"AccessKey "+accessKey,
                    }
                }
                await convFetchList1.push(ZOHO.CRM.HTTP.get(request));
            })
            conversations2.forEach(async function(convId){
                var request ={
                    url : "https://conversations.messagebird.com/v1/conversations/"+convId,
                    headers:{
                        Authorization:"AccessKey "+accessKey2,
                    }
                }
                await convFetchList2.push(ZOHO.CRM.HTTP.get(request));
            })
            var conversationsDetails1 = await Promise.all(convFetchList1);
            if(conversationsDetails1 && conversationsDetails1.length){
                conversationsDetails1.forEach(function(con){
                  con = JSON.parse(con);
                  con.accessKey = accessKey;
                  existingConversations.push(JSON.stringify(con));
                })
            }
            var conversationsDetails2 = await Promise.all(convFetchList2);
            if(conversationsDetails2 && conversationsDetails2.length){
                conversationsDetails2.forEach(function(con){
                  con = JSON.parse(con);
                  con.accessKey = accessKey2;
                  existingConversations.push(JSON.stringify(con));
                })
            }
            return existingConversations
        });
    }  
    function openUploader(){
      if(firebase.auth().currentUser){
        document.getElementById("inputFile").click();
      }
      else{
          document.getElementById("firebaseui-auth-container").style.display="inline-block";
          var uiConfig = {
          callbacks: {
              // Called when the user has been successfully signed in.
              'signInSuccessWithAuthResult': function (authResult, redirectUrl) {
                  if (currentUser== null && authResult.user) {
                      openUploader();
                  }
                  // Do not redirect.
                  return false;
              },
              uiShown: function() {
                 /// $("#credential_picker_container").hide();
                  //$("#notSignedInError").hide();
                $(".loader").hide();
                 // $(".showAfterSignIn").show();
                  //document.getElementById('loader').style.display = 'none';
              }
          },
          // Opens IDP Providers sign-in flow in a popup.
          signInFlow: 'popup',
          signInOptions: [
              // Leave the lines as is for the providers you want to offer your users.
              {
                  // Google provider must be enabled in Firebase Console to support one-tap
                  // sign-up.
                  provider: firebase.auth.GoogleAuthProvider.PROVIDER_ID,
                  // Required to enable ID token credentials for this provider.
                  // This can be obtained from the Credentials page of the Google APIs
                  // console. Use the same OAuth client ID used for the Google provider
                  // configured with GCIP or Firebase Auth.
                  clientId: '356364764905-ua06urrrgp9qh0u61qqtb0tk6cv40jmm.apps.googleusercontent.com'
              },
          ],
          credentialHelper: firebaseui.auth.CredentialHelper.GOOGLE_YOLO,
          // tosUrl and privacyPolicyUrl accept either url string or a callback
          // function.
          // Terms of service url/callback.
          // tosUrl: './terms-of-service.html',
          // // Privacy policy url.

          // privacyPolicyUrl: './privacy-policy.html'

        };
          var ui = new firebaseui.auth.AuthUI(firebase.auth());
          ui.start('#firebaseui-auth-container', uiConfig);
      }
    }
    function sendfile(file){
        // File or Blob named mountains.jpg
        // Create the file metadata
        var metadata = {
          contentType: file.type,
        };

        // Upload file and metadata to the object 'images/mountains.jpg'
        var uploadTask = storageRef.child(loginUser.uid+'/' + file.name).put(file, metadata);

        // Listen for state changes, errors, and completion of the upload.
        uploadTask.on('state_changed', function(snapshot){
          // Observe state change events such as progress, pause, and resume
          // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
          var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          console.log('Upload is ' + progress + '% done');
          // switch (snapshot.state) {
          //   case firebase.storage.TaskState.PAUSED: // or 'paused'
          //     console.log('Upload is paused');
          //     break;
          //   case firebase.storage.TaskState.RUNNING: // or 'running'
          //     console.log('Upload is running');
          //     break;
          // }
        }, function(error) {
            $(".loadingdiv").text(error);
          // Handle unsuccessful uploads
        }, function() {
          // Handle successful uploads on complete
          // For instance, get the download URL: https://firebasestorage.googleapis.com/...
          uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
            console.log('File available at', downloadURL);
            $(".loadingdiv").remove();
            sendMessage(downloadURL,file);
          });
        });
    }
    function valueExists(val) {
        return val !== null && val !== undefined && val.length > 1 && val!=="null";
    }

    function getTimeString(previous) {
            previous = new Date(previous);
            var msPerMinute = 60 * 1000;
            var msPerHour = msPerMinute * 60;
            var msPerDay = msPerHour * 24;
            var msPerMonth = msPerDay * 30;
            var msPerYear = msPerDay * 365;

            var elapsed = new Date() - previous;

            if (elapsed < msPerMinute) {
                 return Math.round(elapsed/1000) + ' seconds ago';
            }

            else if (elapsed < msPerHour) {
                 return Math.round(elapsed/msPerMinute) + ' minutes ago';
            }

            else if (elapsed < msPerDay ) {
                 return Math.round(elapsed/msPerHour ) + ' hours ago';
            }

            else if (elapsed < msPerMonth) {
                return ' ' + Math.round(elapsed/msPerDay) + ' days ago';
            }

            else if (elapsed < msPerYear) {
                return ' ' + Math.round(elapsed/msPerMonth) + ' months ago';
            }

            else {
                return ' ' + Math.round(elapsed/msPerYear ) + ' years ago';
            }
        }
            
    //      function getTimeString(time){
    //   let msgTime = Date.parse(time);
    //   let timeSuffix = "am";
    //   if(isNaN(msgTime) || time.indexOf('Z')<0){
    //     return time;
    //   }
    //   let msgHour = new Date(msgTime).getHours();
    //   timeSuffix = msgHour>12 ? "pm" : "am";
    //   msgHour = msgHour>12 ? msgHour-12 : msgHour;
    //   let msgMins = new Date(msgTime).getMinutes();
    //   msgMins = msgMins<10 ? "0"+msgMins : msgMins;
    //   return msgHour+":"+msgMins+" "+timeSuffix;
    // }
          
          function initiateRTSForConvs(){
              console.log("Trying to initialize initiateRTSForConvs");
              if(typeof secondaryApp === undefined){
                  console.log("FIREBASE not yet present..");
                  setTimeout(initiateRTSForConvs, 2000);
              }
              else{
                  console.log("FIREBASE loaded..");
                  for(var item in existingConversations){
                        var obj = JSON.parse(existingConversations[item]);
                        initiateRTListeners(obj.id);
                    }
              }
          }
          
           function renderConversationList(initConId){
              initiateRTSForConvs();
              $("#prevConversations").html("");
             
                
              if(!existingConversations || existingConversations.length === 0){
                  $("#prevConversations").html("Try Later. No Conversations found.");
              }
              for(var item in existingConversations){
                  var obj = JSON.parse(existingConversations[item]);
                  var channelInfo;
                  obj.channels.forEach(function(channel){
                    if(channel.id == obj.lastUsedChannelId){
                      channelInfo=channel;
                    }
                  })
                  if(!channelInfo){
                    channelInfo = obj.channels[0];
                  }
                  $("#prevConversations").prepend(`<div class="conv-list-item" id="list-${obj.id}" onclick="openConversation('${obj.id}', '${channelInfo.id}')">
                            
                            <div class="conv-channel">
                                ${channelInfo.name}${(obj.accessKey == accessKey2)?" (Second Account)":""}
                            </div>
                            <div class="conv-number">
                                ${obj.contact.msisdn?obj.contact.msisdn:obj.contact.displayName}
                            </div>
                        </div>`);
              }
              if(initConId && document.getElementById(`list-${initConId}`)){
                document.getElementById(`list-${initConId}`).click();
              }
          }
          function getValidPhoneNumber(no){
              no = no.replace(/\D/g,'');
              $('#sms-form').append(`<div class="processingFull"> Validating Phone Number...</div>`);
              var request ={
                  url : "https://rest.messagebird.com/lookup/" + no,
                  headers:{
                        Authorization:"AccessKey "+accessKey,
                  }
              }
              return ZOHO.CRM.HTTP.get(request).then(function(phoneData){
                  $(".processingFull").remove();
                  var countryPrefix =null;
                  var no ;
                  try{
                      countryPrefix = JSON.parse(phoneData).countryPrefix;
                      if(JSON.parse(phoneData) && JSON.parse(phoneData).phoneNumber){
                        no = JSON.parse(phoneData).phoneNumber+"";
                      }
                  }
                  catch(e){
                    console.log(e);
                    countryPrefix = uawc_getCountryCode(no);
                  }    
                  if(!countryPrefix){
                    return false;
                  }
                  else{
                    return no;
                  }
              }); 
          }
          function uawc_getCountryCode(number) {
                var country_code = [1, 1242, 1246, 1264, 1268, 1284, 1340, 1345, 1441, 1473, 1649, 1664, 1758, 1787, 1809, 1868, 1869, 1876, 2, 20, 212, 213, 216, 218, 220, 221, 222, 223, 224, 226, 227, 228, 229, 231, 232, 233, 234, 236, 237, 238, 239, 240, 241, 242, 244, 245, 248, 249, 250, 251, 252, 253, 254, 256, 257, 258, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 269, 27, 290, 291, 297, 298, 299, 30, 31, 32, 33, 34, 350, 351, 352, 353, 354, 356, 357, 358, 359, 36, 370, 371, 372, 373, 374, 375, 376, 377, 378, 379, 380, 381, 385, 386, 387, 389, 39, 40, 41, 417, 42, 421, 43, 44, 45, 46, 47, 48, 49, 500, 501, 502, 503, 504, 505, 506, 507, 509, 51, 52, 53, 54, 55, 56, 57, 58, 590, 591, 592, 593, 594, 595, 596, 597, 598, 60, 61, 62, 63, 64, 65, 66, 670, 671, 672, 673, 674, 675, 676, 677, 678, 679, 680, 681, 682, 683, 686, 687, 688, 689, 691, 692, 7, 7, 7, 7, 7, 7880, 81, 82, 84, 850, 852, 853, 855, 856, 86, 880, 886, 90, 90392, 91, 94, 95, 960, 961, 962, 963, 964, 965, 966, 967, 968, 969, 971, 972, 973, 974, 975, 976, 977, 98, 993, 994, 996];
                if (number.toString().length > 10)
                {
                    num = number.toString();
                    var num_len = num.split(num.slice(-10))[0];
                    for (let c of country_code)
                    {
                        if (c + "" === num_len + "")
                        {
                            return c;
                        }
                    }
                }
                return null;
            }
          function getPlhrValue(){
            $("#custominput_"+plc).val();
          }
        function getParamFields(){
            var plhrs =[];
            var i =0;
            while(1){
                let plc = document.getElementById(`plhr_${i+1}`);
                if(plc){
                    if(plc.value == "custom_value"){
                        plhrs.push(document.getElementById(`custominput_${i+1}`).value);
                    }
                    else{
                        plhrs.push("${"+recordModule+"."+plc.value+"}");
                    }
                    i++;
                }
                else{
                    break;
                }
            }
            return plhrs;
        }
        function getAllIndexes(arr, val) {
            var indexes = [], i = -1;
            while ((i = arr.indexOf(val, i+1)) != -1){
                indexes.push(i);
            }
            return indexes;
        }
        function getParamValues(plhrs){
            moduleFields.forEach(function(field){
                var indexes = getAllIndexes(plhrs,"${"+recordModule+"."+field.field_label+"}");
                if(indexes.length){
                  var value = currentRecord[field.api_name];
                  if(value && value.name)
                  {
                      value = value.name;
                  }
                  if(!value){
                      value="";
                  }
                }
                indexes.forEach(function(index){
                  if(index != -1){
                    plhrs[index] =value;
                  }
                })
            });
            for(let i=0;i<plhrs.length;i++){
                plhrs[i] ={"default": plhrs[i]};
            }
            return plhrs;
        }

var isMediaTemplate = false;
          async function initiateMessage(){
              var phoneNumber = $("#input-to-phone").val();
              phoneNumber =phoneNumber.replace(/\D/g,'');
              if(phoneNumber){
                var validNumber = await getValidPhoneNumber(phoneNumber);
                if(!validNumber){
                    $('#sms-form').append(`<div class="processingFull"> Invalid Phone number.Please Make sure <b>Country Code</b> is added in <span onclick="redirectToSettings()" style="color:blue;cursor:pointer;">extension settings.</span></div>`);
                    setTimeout((()=>{$(".processingFull").remove();}), 5000);
                    return;
                }
                phoneNumber = validNumber+"";
              }  
              var text = $("#input-sms-content").val();
              if(!text.trim()){
                  $('#sms-form').append(`<div class="processingFull"> Message cannot be empty.</div>`);
                  setTimeout((()=>{$(".processingFull").remove();}), 2000);
                  return;
              }
              else if(!phoneNumber){
                  $('#sms-form').append(`<div class="processingFull"> Please Select Phone number.</div>`);
                  setTimeout((()=>{$(".processingFull").remove();}), 2000);
                  return;
              }
              else if(!initateChannelId || initateChannelId == "select_channel"){
                  $('#sms-form').append(`<div class="processingFull"> Please Select channel.</div>`);
                  setTimeout((()=>{$(".processingFull").remove();}), 2000);
                  return "";
              }
              if(!ContactName){
                ContactName = phoneNumber;
              }
              var parambody ={
                  "type" : "text",
                  "to": phoneNumber,
                  "channelId": initateChannelId,
                  "content":{
                      "text" : text
                  },
                  "source":{
                      "type" : "crm",
                      "email": currentUser['users'][0]['email'],
                      "name": currentUser['users'][0]['full_name'],
                      "zuid": currentUser['users'][0]['zuid']
                  }
              }
              if(selectedChannel && selectedChannel.slug.indexOf("whatsapp") != -1){
                var template = $("#approvedTemplates").val();
                if(template && template != "Select_Template"){
                    var paramValues =getParamValues(getParamFields());
                    for(let i=0;i<paramValues.length;i++){
                        if(!paramValues[i].default){
                            $('#sms-form').append(`<div class="processingFull"> {{${i+1}}} mapped value is empty, please check it.</div>`);
                            setTimeout((()=>{$(".processingFull").remove();}), 2000);
                            return;
                        }
                    }
                    
                    var currentNamespace = namespace;
                    var currentmContactId = messagebirdContactId;
                    if(currentAccessKey == accessKey2){
                        currentNamespace = namespace2;
                        currentmContactId = messagebirdContactId2;
                    }
                    if(!currentNamespace){
                        $('#sms-form').append(`<div class="processingFull"> Please enter your whatsapp account namespace in <span onclick="redirectToSettings()" style="color:blue;cursor:pointer;">extension settings.</span></div>`);
                        setTimeout((()=>{$(".processingFull").remove();}), 2000);
                        return "";
                    }
                    parambody = {
                      "to":phoneNumber,
                      "channelId":initateChannelId,
                      "type":"hsm",
                      "content":{
                              "hsm":{
                                  "namespace":currentNamespace,
                                  "templateName":selectedTemplate.name,
                                  "language":{"policy":"deterministic",
                                              "code":selectedTemplate.language
                                             },
                                  "params":paramValues
                              }
                          },
                          "source":{
                              "type" : "crm",
                              "email": currentUser['users'][0]['email'],
                              "name": currentUser['users'][0]['full_name'],
                              "zuid": currentUser['users'][0]['zuid']
                          }
                       };

                  if(selectedTemplate.name)
                  {
                      compData = null;
                      var request ={
                              url : "https://integrations.messagebird.com/v2/platforms/whatsapp/templates/"+selectedTemplate.name,
                              headers:{
                                      Authorization:"AccessKey "+currentAccessKey,
                              }
                      }
                      await ZOHO.CRM.HTTP.get(request)
                      .then(async function(data){
                          console.log(data);
                          data = JSON.parse(data);
                          let components = data[0].components;
                          let istextTemplate = await isTextTemplate(components);
                          if(!istextTemplate)
                          {
                              isMediaTemplate = true;
                              if(parambody.content.hsm["params"].length > 0)
                              {
                                  paramValueObj = await changeBodyParamsFormate(parambody.content.hsm["params"]);
                              }

                              var componentsData = await mediaTemplateData(components);

                              delete parambody.content.hsm["params"];
                              parambody.content.hsm["components"] = componentsData;
                              // url = "https://conversations.messagebird.com/v1/send";
                              let from = parambody["channelId"];
                              parambody["from"] = from;
                              delete parambody["channelId"]; 
                          }
                      });
                  }

                }
              }




              $('#sms-form').append(`<div class="processingFull"> Sending message...</div>`);
                                    var request ={
                                            url : "https://conversations.messagebird.com/v1/conversations/start",
                                            body:parambody,
                                            headers:{
                                                    Authorization:"AccessKey "+ (currentAccessKey ? currentAccessKey : accessKey),
                                            }
                                    }
                                    if(isMediaTemplate)
                                    {
                                      request.url =  "https://conversations.messagebird.com/v1/send";
                                    }
                                    ZOHO.CRM.HTTP.post(request)
                                    .then(async function(data){
                                        console.log(data);
                                       // {"id":"f02f3452fef74306bab8e9ae67ed165d","contactId":"cf3977cb0d814dcabac549b02995dc2a","contact":{"id":"cf3977cb0d814dcabac549b02995dc2a","href":"https://contacts.messagebird.com/v2/contacts/cf3977cb0d814dcabac549b02995dc2a","msisdn":918012178547,"displayName":"918012178547","firstName":"","lastName":"","customDetails":{},"attributes":{},"createdDatetime":"2020-04-22T16:26:18Z","updatedDatetime":"2020-05-03T07:41:36Z"},"channels":[{"id":"f8415c381cb241c68d44ac408fe96e9b","name":"Ulgebra","platformId":"whatsapp","status":"active","createdDatetime":"2020-08-17T10:46:16Z","updatedDatetime":"2020-08-19T12:21:41Z"},{"id":"3c890a848bfd49f1911ce3552825743a","name":"","platformId":"events","status":"active","createdDatetime":"2020-08-04T17:16:30Z","updatedDatetime":"0001-01-01T00:00:00Z"},{"id":"6c6f02fb7de34ffabdea7c3bb3835896","name":"Ulgebra SMS CHananel","platformId":"sms","status":"active","createdDatetime":"2020-08-26T08:50:42Z","updatedDatetime":"0001-01-01T00:00:00Z"},{"id":"18b6b9f547bd48eab0bb21776d695189","name":"WhatsApp Sandbox","platformId":"whatsapp_sandbox","status":"active","createdDatetime":"2020-02-16T10:55:23Z","updatedDatetime":"0001-01-01T00:00:00Z"},{"id":"04cc3df7fff04930986f85d0e3bc63a4","name":"ua-fb","platformId":"facebook","status":"active","createdDatetime":"2020-04-20T16:29:14Z","updatedDatetime":"0001-01-01T00:00:00Z"}],"status":"active","createdDatetime":"2020-04-22T16:26:18Z","updatedDatetime":"2020-08-31T17:26:39.312177033Z","lastReceivedDatetime":"2020-08-31T17:26:39.302797397Z","lastUsedChannelId":"18b6b9f547bd48eab0bb21776d695189","messages":{"totalCount":175,"href":"https://conversations.messagebird.com/v1/conversations/f02f3452fef74306bab8e9ae67ed165d/messages","lastMessageId":"83384d1f6ed84defa64330cd817ef773"}}
                                        data = JSON.parse(data);
                                        if(!data || (data.errors && data.errors.length)){
                                          if(data.errors[0] && data.errors[0].description){
                                            $(".processingFull").text(data.errors[0].description);
                                          }
                                          else{
                                            $(".processingFull").text("Something went wrong ,Please try later.");
                                          }
                                          setTimeout((()=>{$(".processingFull").remove()}),3000);
                                          return ;
                                        }
                                        convId = data.id;
                                        var isExist=false;
                                        if(isMediaTemplate && data.id)
                                        {
                                          // await setTimeout((()=>{$(".processingFull").remove();$('#existingConvListWindow').show();renderConversationList(convId,initateChannelId);}), 3000);
                                          // return;

                                          await ZOHO.CRM.HTTP.get({
                                              url : "https://conversations.messagebird.com/v1/messages/"+data.id,
                                              headers:{
                                                      Authorization:"AccessKey "+currentAccessKey,
                                              }
                                          }).then(async (res1)=>{
                                            console.log(JSON.parse(res1));
                                            let resData1 = JSON.parse(res1);
                                            await ZOHO.CRM.HTTP.get({
                                                url : "https://conversations.messagebird.com/v1/conversations/"+resData1.conversationId,
                                                headers:{
                                                        Authorization:"AccessKey "+currentAccessKey,
                                                }
                                            }).then(async (res2)=>{
                                              console.log(JSON.parse(res2));
                                              let resData2 = JSON.parse(res2);
                                              resData2.messages['lastMessageId'] =  data.id;
                                              data = resData2;
                                              return;
                                            });
                                            return;
                                          });
                                        }
                                        for(var item in existingConversations){
                                            let con = JSON.parse(existingConversations[item]);
                                            if(con.id == convId){
                                              isExist = true;
                                            }
                                        }
                                        var platform ="";
                                        for(var item in data.channels){
                                            if(data.channels[item].id == initateChannelId){
                                                platform = data.channels[item].platformId;
                                            }
                                        } 
                                        if(!isExist){
                                            if(!currentmContactId){
                                                if(accessKey && currentAccessKey == accessKey){
                                                  messagebirdContactId = data.contactId;
                                                  currentmContactId =messagebirdContactId;
                                                  var updatemap = {"id":recordId};
                                                  updatemap[messageBirdId]=messagebirdContactId;
                                                }
                                                else if(accessKey2 && currentAccessKey == accessKey2){
                                                  messagebirdContactId2 = data.contactId;
                                                  currentmContactId =messagebirdContactId2;
                                                  var updatemap = {"id":recordId};
                                                  updatemap["MessageBird_Id2"]=messagebirdContactId2;
                                                }
                                                await ZOHO.CRM.API.updateRecord({Entity:recordModule,APIData:updatemap});  
                                            }
                                            else if(currentmContactId != data.contactId){
                                              var conversationmap = {"Name":platform + " conversation","messagebirdforzohocrm__Conversation_Id":convId,"messagebirdforzohocrm__From":data.contact.msisdn+"","messagebirdforzohocrm__Platform":platform,"messagebirdforzohocrm__channelId":initateChannelId,"messagebirdforzohocrm__MessageBird_Contact_Id":data.contactId};
                                              conversationmap[conversationModuleName]=recordId;
                                              if(accessKey2 && currentAccessKey == accessKey2){
                                                conversationmap["MessageBird_Account"]="Second Account";
                                              }
                                              await ZOHO.CRM.API.insertRecord({Entity:"messagebirdforzohocrm__MessageBird_Conversations",APIData:conversationmap});
                                            }
                                            data.accessKey = currentAccessKey;
                                            existingConversations.push(JSON.stringify(data));
                                            initiateRTListeners(convId);
                                            setTimeout((()=>{$(".processingFull").remove();$('#existingConvListWindow').show();renderConversationList(convId,initateChannelId);}), 3000);
                                        } 
                                         
                                        let lastMessageId = "";
                                        try{
                                                lastMessageId = data.messages.lastMessageId;
                                        }
                                        catch(err){
                                            console.log(err);
                                        }

                                        var messageHistoryMap = {"Name":"message sent to "+ContactName,"messagebirdforzohocrm__Message_Id":lastMessageId,"messagebirdforzohocrm__Message":text,"messagebirdforzohocrm__Conversation_Id":convId,"messagebirdforzohocrm__MessageBird_Contact_Id":data.contactId,"messagebirdforzohocrm__Channel_Id":initateChannelId,"messagebirdforzohocrm__Direction":"Sent","messagebirdforzohocrm__Platform":platform, "messagebirdforzohocrm__Status":"Sent"};
                                        messageHistoryMap[conversationModuleName]=recordId;
                                        if(selectedTemplate && selectedTemplate.name){
                                          messageHistoryMap["CampaignName"]=selectedTemplate.name;
                                        }
                                        if(accessKey2 && currentAccessKey == accessKey2){
                                          messageHistoryMap["MessageBird_Account"]="Second Account";
                                        }
                                        await ZOHO.CRM.API.insertRecord({Entity:"messagebirdforzohocrm__Message_History",APIData:messageHistoryMap,Trigger:["workflow"]}); 
                                        setTimeout((()=>{$(".processingFull").remove();$('#existingConvListWindow').show();renderConversationList(convId,initateChannelId);}), 3000);

                                      });
              
          }


async function isTextTemplate(data)
{
    for(var i=0;i < data.length;i++)
    {
        let comp = data[i];
        let keys = Object.keys(comp);
        for(var j=0;j < keys.length;j++)
        {
            key = keys[j];
            if(key == "format" && (comp[key].toLowerCase() != "text" && comp[key].toLowerCase() != "none"))
            {
                return false;
            }
            if(key == "type" && comp[key].toLowerCase() == "buttons")
            {
              for(var k=0;k < comp.buttons.length;k++)
              {
                  let button = comp.buttons[k];
                  if(button.type == "URL")
                  {
                    return false;
                  }
              }
              
            }
        }
    }
    return true;
}

var paramValueObj = null;
async function mediaTemplateData(components)
{
    var componentsData = [];
    for(var i=0;i<components.length;i++)
    {
        let comp = components[i];
        let compType = comp.type;
        if(compType.toLowerCase() == "body")
        {
            componentsData[componentsData.length] = {
                                                        "type":"body",
                                                        "parameters": paramValueObj
                                                    };
        }
        if(compType.toLowerCase() == "header")
        {
            componentsData[componentsData.length] = JSON.parse(`{
                                                        "type":"header",
                                                        "parameters": [
                                                            {
                                                              "type": "${comp.format.toLowerCase()}",
                                                              "${comp.format.toLowerCase()}": {
                                                                "url":"${comp.example.header_url}"
                                                              }
                                                            }
                                                        ]
                                                    }`);
        }
        if(compType.toLowerCase() == "buttons" && comp.buttons.length > 0)
        {
            for(var j=0;j< comp.buttons.length;j++)
            {
                let button = comp.buttons[j];
                if(button.type == "URL")
                {
                    let isbtnVar = false;
                    if(button.url.includes("{{") == true && button.url.includes("}}") == true)
                    {
                        isbtnVar = true;
                        componentsData[componentsData.length] = JSON.parse(`{
                                                        "type":"button",
                                                        "sub_type": "url",
                                                        "parameters": [
                                                            {
                                                              "type": "text",
                                                              "text": "${"?"}"
                                                            }
                                                        ]
                                                    }`);
                    }
                }
            }
        }
        // if(compType.toLowerCase() == "footer")
        // {
        //     componentsData[componentsData.length] = {
        //                                                 "type":"footer",
        //                                                 "parameters": [
        //                                                     {
        //                                                       "type": "text",
        //                                                       "text": comp.text
        //                                                     }
        //                                                 ]
        //                                             }
        // }
    }
    return componentsData;
}

async function changeBodyParamsFormate(params)
{
    for(var i=0;i < params.length;i++)
    {
        let val = params[i].default;
        params[i] = {"type":"text","text":val};
    }

    return params;

}


          function updateLastCampaigName(campData){
            console.log(campData);
          }
          
          function sendMessage(file,fileMeta){
              
              var content = {};
              var text = "";
              if(file){
                var types=["image","audio","video"];
                var type = fileMeta.type.substring(0,fileMeta.type.indexOf("/"));
                if(types.indexOf(type) == -1){
                    type = "file";
                }
                content[type]={"url":file};
              }
              else{
                var text = $("#main-message-text").val();
                var type = "text";
                content["text"] = text;
              }
              if(!validVal(text.trim()) && !file){
                  $('.chatbox-outer').append(`<div class="loadingdiv" style="background-color:crimson"> Message is required!...</div>`);
                   setTimeout((function(){ $(".loadingdiv").remove(); }), 2000);
                  return;
              }
              $('.chatbox-outer').append(`<div class="loadingdiv"> <span class="spinnow material-icons" style="font-size: 20px;display:inline-block;">toys</span> Sending message...</div>`);
              var chosenChannelVal = $("#inp-chat-send-channel").val();
              var request ={
                                            url : "https://conversations.messagebird.com/v1/conversations/"+convId+"/messages",
                                            body:{
                                                  "type" : type,
                                                  "content":content,
                                                  "source":{
                                                      "type" : "crm",
                                                      "email": currentUser['users'][0]['email'],
                                                      "name": currentUser['users'][0]['full_name'],
                                                      "zuid": currentUser['users'][0]['zuid']
                                                  }
                                            },
                                            headers:{
                                                    Authorization:"AccessKey "+currentAccessKey,
                                            }
                                    }
                                    if(validVal(chosenChannelVal)){
                                        request.body.channelId = chosenChannelVal;
                                    }
                                    ZOHO.CRM.HTTP.post(request).then(function(data){
                                        if(typeof data === "string"){
                                            data = JSON.parse(data);
                                        }
                                        if(!ContactName){
                                          ContactName = "message sent";
                                        }
                                        else{
                                          ContactName = "message sent to "+ContactName
                                        }
                                        var messageHistoryMap = {"Name":ContactName,"messagebirdforzohocrm__Message_Id":data.id,"messagebirdforzohocrm__Message":data.content[type],"messagebirdforzohocrm__Conversation_Id":data.conversationId,"messagebirdforzohocrm__MessageBird_Contact_Id":messagebirdContactId,"messagebirdforzohocrm__Channel_Id":data.channelId,"messagebirdforzohocrm__Direction":data.direction,"messagebirdforzohocrm__Platform":data.platform, "messagebirdforzohocrm__Status":data.status};
                                        messageHistoryMap[conversationModuleName]=recordId;
                                        if(accessKey2 && currentAccessKey == accessKey2){
                                          messageHistoryMap["MessageBird_Account"]="Second Account";
                                        }
                                        ZOHO.CRM.API.insertRecord({Entity:"messagebirdforzohocrm__Message_History",APIData:messageHistoryMap,Trigger:["workflow"]});
                                        $("#main-message-text").val("");
                                        $(".loadingdiv").text('<span class="spinnow material-icons" style="font-size: 20px;display:inline-block;">toys</span> Message Sent, Checking Status...');
                                        //addNewMessageToWindow(data.conversationId, data.id, 'direction-out');
                                        $(".loadingdiv").remove();
                                        data.status = 'sent';
                                        data.updatedDatetime = new Date().toISOString();
                                        addMessageInBox(data, true);
                                       $(".chatmessages-inner").scrollTop($(".chatmessages-inner")[0].scrollHeight);
                                        //setTimeout((()=>{getConvs(0);}), 5000);
                                    });
              
          }

          function getConvInfo(CID,data){
              $('.chatbox-outer').append(`<div class="loadingdiv"> <span class="spinnow material-icons" style="font-size: 20px;display:inline-block;">toys</span> Fetching User...</div>`);
              $('.replychosediv').remove();
              $("#main-message-text").after('<div class="replychosediv">Reply via <span id="send-channels-holder"> <span class="spinnow material-icons" style="font-size: 20px;display:inline-block;">toys</span> loading ...</span></div>');
              $(".chat-no-perm-send").remove();
              $('.chataction').hide();
             
                                        convData = data;
                                        var channelList = `<select id="inp-chat-send-channel" onchange="chooseChannelNow()" value="${data.lastUsedChannelId}">`;
                                        var convAllowedChannels = [];
                                        for(var item in data.channels){
                                            var channel = data.channels[item];
                                            channel.uicolor = colors[item];
                                            channel.colorder = item;
                                            convChannels[channel.id] = channel;
                                            //$('.msgch-'+channel.id).css({'border-color': colors[item]});
                                            if(allowedChannels.indexOf(channel.id)>-1 || (accessConfiguredChannels.indexOf(channel.id) === -1)){
                                                convAllowedChannels.push(channel.id);
                                                channelList+=`<option ${data.lastUsedChannelId === channel.id ? 'selected' : ''} value="${channel.id}">${capitalizeFirstLetter(channel.platformId)} - ${channel.name}</option>`;
                                            }
                                        }
                                        channelList+='</select>';
                                        if(convAllowedChannels.length>0){
                                            $('.chataction').show();
                                            $("#send-channels-holder").html(channelList);
                                            if(data.lastUsedChannelId){
                                              setTimeout((function(){$(".btn-sendmessage").css({'background-color': $('.chatmessage-item.col-'+convChannels[data.lastUsedChannelId].colorder).css('border-color')})}), 1000);
                                            }
                                        }else{
                                            $('.chatbox-outer').append('<div class="chat-no-perm-send">You do not have permssion to send message to this channel</div>');
                                        }
                                        $(".loadingdiv").remove();
          }

          function chooseChannelNow(){
              var channelId = $("#inp-chat-send-channel").val();
              $(".btn-sendmessage").css({'background-color': $('.chatmessage-item.col-'+convChannels[channelId].colorder).css('border-color')});
          }

          function capitalizeFirstLetter(string) {
               return string.charAt(0).toUpperCase() + string.slice(1);
          }
          
          function escapeHTMLS(rawStr){
              return $('<textarea/>').text(rawStr).html();
          }
          
          function getConvs(offset){
              $("#initiateConvWindow").hide();
//              $('#existingConvListWindow').show();
              $("#existingConvWindow").show();
              var limit = 20;
              if(offset!==0){
                  $(`#loadmore-off-${offset}`).html('<span class="spinnow material-icons" style="font-size: 20px;display:inline-block;">toys</span> Loading...');
              }
              if(offset === 0){
                $('.chatbox-outer').append(`<div class="loadingdiv"> <span class="spinnow material-icons" style="font-size: 20px;display:inline-block;">toys</span> Loading Messages...</div>`);
              }
              var hasMoreMsgs = true;
              var request = {
                                            url : "https://conversations.messagebird.com/v1/conversations/"+convId+"/messages?offset="+offset+"&limit="+limit,
                                            params:{
                                                    
                                            },
                                            headers:{
                                                    Authorization:"AccessKey "+currentAccessKey,
                                            }
                                    }
                                    ZOHO.CRM.HTTP.get(request)
                                    .then(function(data){
                                       renderDataFromJSON(offset, limit, data, hasMoreMsgs);
                                    });
          }
          
          function renderDataFromJSON(offset, limit, data, hasMoreMsgs){
              if(offset === 0){
                                            $('.chatmessages-inner').html("");
                                        }
                                        if(offset!==0){
                                            $(`#loadmore-off-${offset}`).html('').addClass('loadedLoadmore').prop("onclick", null).off("click");
                                        }
                                        $(".loadingdiv").remove();
                                        var json = JSON.parse(data);
                                        if(!json.items){
                                          return;
                                        }
                                        hasMoreMsgs = json.items.length>0; 
                                        for(var i=0; i<json.items.length;i++){
                                            var msgObj = json.items[i];
                                           addMessageInBox(msgObj, false);
                                        }
                                        if(hasMoreMsgs){
                                            $('.chatmessages-inner').prepend(`<div id="loadmore-off-${offset+limit}" class="prevMessageBtn" onclick="getConvs(${offset+limit})">Load Previous</div>`);
                                        }
                                        if(offset===0){
                                            $(".chatmessages-inner").scrollTop($(".chatmessages-inner")[0].scrollHeight);
                                        }else{
                                            $(".chatmessages-inner").scrollTop($(`#loadmore-off-${offset}`).offset().top-150);
                                        }
          }
          function renderTemplateMessage(msgObj){
              try{
              var hsmJSON = msgObj.content.hsm;
              var request = {
                                            url : "https://integrations.messagebird.com/v1/public/whatsapp/templates/"+hsmJSON.templateName,
                                            params:{
                                                    
                                            },
                                            headers:{
                                                    Authorization:"AccessKey "+currentAccessKey,
                                            }
                                    }
                                    ZOHO.CRM.HTTP.get(request).then(function(data){
                                        if(JSON.parse(data)[0]){
                                                var content = JSON.parse(data)[0].content;
                                                var tempparams = msgObj.content.hsm.params;
                                                if(!namespace && msgObj.status!=="failed" && msgObj.status!=="rejected"){
                                                    namespace = msgObj.content.hsm.namespace;
                                                }
                                                if(tempparams){
                                                    for(let i = 0; i< tempparams.length; i++){
                                                        content = content.split('{{'+(i+1)+'}}').join(tempparams[i].default);
                                                    }
                                                }
                                                $("#msgitem-"+msgObj.id+" .chmsgcontenttext").text(content);
                                             $(".chatmessages-inner").scrollTop($(".chatmessages-inner")[0].scrollHeight);
                                        }     
                                    });
              }catch(err){
                  console.log(err);
              }
          }
          function addMessageInBox(msgObj, addAtLast){
                                    var msgType = msgObj.type;
                                    var content = escapeHTMLS(msgObj.content.text ? msgObj.content.text : "").replace(/(?:\r\n|\r|\n)/g, '<br>');
                                    var mediaHTML = "";
                                    var caption = "";
                                    if(msgType!=="text"){
                                        if(msgType==="location"){
                                            content = "http://maps.google.com/?ll="+msgObj.content[msgType].latitude+","+msgObj.content[msgType].longitude;
                                        }
                                       else if(msgType==="hsm"){
                                            content = `<span class="spinnow material-icons" style="font-size: 20px;display:inline-block;">toys</span> Template Message Loading ...`;
                                            renderTemplateMessage(msgObj);
                                        }
                                        else{
                                            if(msgType === "image"){
                                                mediaHTML = `<img src="${msgObj.content[msgType].url}"/>`;
                                            }
                                            else if(msgType === "video"){
                                                mediaHTML = `<video controls src="${msgObj.content[msgType].url}"/>`;
                                            }
                                            else if(msgType === "audio"){
                                                mediaHTML = `<audio controls src="${msgObj.content[msgType].url}"/>`;
                                            }
                                            else if(msgType === "file"){
                                                mediaHTML = `<div class="msg-attch"><span class="material-icons">attachment</span> <a target="_blank" href="${msgObj.content[msgType].url}">Click to download file</a></div>`;
                                            }
                                            if(msgObj.content[msgType] && msgObj.content[msgType].caption){
                                              caption = `<div>${msgObj.content[msgType].caption}</div>`;
                                            }
                                        }
                                    }
                                    var inComing = msgObj.direction === "received" ;
                                    var personHTML = "";
                                    if(msgObj.source !== undefined && msgObj.source.type === "crm"){
                                        let sourceObj = msgObj.source;
                                        personHTML = `<div class="chat-message-author" title="${sourceObj.email}">${sourceObj.name} <img class="cma-ppic" src="https://contacts.zoho.com/file?ID=${sourceObj.zuid}&fs=thumb"> </div>`;
                                    }
                                    let channelId = msgObj.channelId;
                                    var walimittext = `<a href="https://apps.ulgebra.com/whatsapp-business-api-integration/limitations#h.ytd5bu2d0930" target="_blank">See Why?</a>`;
                                        var msgHTML = (`<div id="msgitem-${msgObj.id}" title="Sent to : ${convChannels[channelId].name}" class="msgch-${channelId} col-${convChannels[channelId].colorder} msgsts-${msgObj.status} chatmessage-item ${inComing ? 'direction-in' :'direction-out' } ${msgObj.unread ? 'unread': ''}">
                        <div class="chatmessage-inner">
                            <div class="chatmessage-content">
                               ${personHTML} ${mediaHTML} ${caption} <span class="chmsgcontenttext"> ${content} </span>
                            </div>
                            <div class="chatmessage-time" title="${new Date(msgObj.updatedDatetime).toLocaleString()}">
                                <span class="fullTime">from <b>${msgObj.from} to ${msgObj.to}</b> via <b class="c-green">${msgObj.platform.toUpperCase()}</b></span>
                                <span class="smallTime">${getTimeString(msgObj.updatedDatetime)}</span>
                                ${inComing ? '' : '<span class="msgStatus">'+(getStatusHTML(msgObj.status))+  `${msgObj.error ? ' <i class="c-crimson">'+msgObj.error.description+'</i>': ''} </span>`}
                            </div>
                        </div>
                    </div>`);
                                    if(addAtLast){
                                        $("#MID-LOADING-"+msgObj.id).remove();
                                        $('.chatmessages-inner').append(msgHTML);
                                    }
                                    else{
                                        $('.chatmessages-inner').prepend(msgHTML);
                                    }
          }

          function getStatusHTML(status) {
    var walimittext = `<a href="https://apps.ulgebra.com/whatsapp-business-api-integration/limitations#h.ytd5bu2d0930" target="_blank">See Why?</a>`;
    switch (status) {
        case "failed":
            return  `<span class="material-icons">error_outline</span>`;
            break;
        case "rejected":
            return  `<span class="material-icons">block</span>`;
            break;
        case "pending":
            return  `<span class="material-icons">flight_takeoff</span>`;
            break;

        case "sent":
            return  `<span class="material-icons">done</span>`;
            break;

        case "delivered":
            return  `<span class="material-icons">done_all</span>`;
            break;

        case "read":
            return  `<span class="material-icons">done_all</span>`;
            break;

        default:
            return status;
            break;
    }
}

            function checkPhoneNumber(no){
                no = no.replace(/\D/g,'');
                if(!no){
                  return;
                }
                var request ={
                    url : "https://rest.messagebird.com/lookup/" + no,
                    headers:{
                          Authorization:"AccessKey "+currentAccessKey,
                    }
                }
                return ZOHO.CRM.HTTP.get(request).then(function(phoneData){
                    var countryPrefix = null;
                    try{
                      countryPrefix = JSON.parse(phoneData).countryPrefix;
                    }
                    catch(e){
                      console.log(e);
                      countryPrefix = uawc_getCountryCode(no);
                    }
                    if(!countryPrefix){
                        if(countryCode && countryCode != "0" && no.length > countryCode.length){
                            $("#input-to-phone").val(countryCode+""+no);
                        }
                    }
                }); 
            }
          function selectPhoneNumber(){
            checkPhoneNumber($("#contact-phone-target").val());
            $("#input-to-phone").val($("#contact-phone-target").val());
          }
          function selectChannelId(){
              initateChannelId = $("#contact-channel-target").val();
              if(document.getElementById("templatesList")){
                document.getElementById("templatesList").style.display="none";
                document.getElementById("input-sms-content").value="";
                document.getElementById("input-sms-content").disabled = false;
                $("#approvedTemplates").html("");
                $("#plmp").html("");
                if(initateChannelId.indexOf("2_") == -1){
                  currentAccessKey= accessKey;
                 channelsList.forEach(function(channel){
                  if(channel.id == initateChannelId){
                      selectedChannel = channel;
                      if(channel.slug.indexOf("whatsapp") != -1){
                          getApprovedTemplates();
                      }
                  }
                 });
                }
                else if(initateChannelId.indexOf("2_") == 0){
                 initateChannelId = initateChannelId.substring(2);
                 currentAccessKey = accessKey2;
                 secondChannelsList.forEach(function(channel){
                  if(channel.id == initateChannelId){
                      selectedChannel = channel;
                      if(channel.slug.indexOf("whatsapp") != -1){
                          getApprovedTemplates();
                      }
                  }
                 });
                } 
              } 
          }
          function getApprovedTemplates(){
            var request = {
                url : "https://integrations.messagebird.com/v1/public/whatsapp/templates",
                headers:{
                        Authorization:"AccessKey "+currentAccessKey,
                }
            }
            $('#sms-form').append(`<div class="processingFull"> Fetching WhatsApp Approved Templates...</div>`);
            ZOHO.CRM.HTTP.get(request).then(function(data){
                console.log(data);
                approvedTemplates = JSON.parse(data);
                let templatesUi ='';
                let selectedOption='<option value="Select_Template">Select Template</option>';
                approvedTemplates.forEach(function(template){
                    // if(templateId && templateId == template.id){
                    //     selectedTemplate = template;
                    //     selectedOption = '<option value="'+template.id+'">'+template.name+'</option>';
                    // }
                    // else{
                        templatesUi = templatesUi +'<option value="'+template.id+'">'+template.name+'</option>';
                    // }
                });
                $("#approvedTemplates").append(selectedOption+templatesUi);
                $(".processingFull").remove();
                
                document.getElementById("templatesList").style.display="block";
                //"[{"name":"issue_ticket_resolve","language":"en","status":"APPROVED","content":"Hi {{1}}, We hope your issue on the extension {{2}} got resolved. Kindly let us know your feedback by replying to this message. Reach us in case of any other clarifications also.","id":"401833644114121","category":"ISSUE_RESOLUTION","createdAt":"2020-08-20T17:40:29.420516905Z","updatedAt":"2020-08-20T17:44:05.305544373Z"}]"
            }); 

        }
        function addMap(content,plhrs){
          let plc = 0;
          while(1){
            if(content.indexOf("{{"+(plc+1)+"}}") != -1){
              plc++;
            }
            else{
              break;
            }
          }
          for(let i=0;i<plc;i++){
            let options = '<option value="custom_value">Custom Value</option>';
            let selectedOption = '';
            let customvalue = ''; 
            moduleFields.forEach(function(field){
              let field_label = field;
              if(field.field_label){
                field_label = field.field_label;
              }
              if(plhrs && plhrs[i] && plhrs[i].indexOf("."+field_label) != -1){
                selectedOption = '<option value="'+field_label+'">'+field_label+'</option>';
              } 
              else{
                options = options + '<option value="'+field_label+'">'+field_label+'</option>'
              }
            });
            if(plhrs && plhrs[i] && selectedOption == ''){
              customvalue = plhrs[i];
            }
            $("#plmp").append(`<div class="fieldMapp">
                  <div class="fMItem">
                     {{${i+1}}}
                  </div>
                  <div class="fMItem">
                  <i class="material-icons">east</i>
                  </div>
                  <div class="fMItem">
                        <select name="field4" id="plhr_${i+1}" onChange="setPlhdr(${i+1},this.value)">${selectedOption+options} </select>
                  </div>
                  <div class="fMItem">
                        <input id="custominput_${i+1}" value="${customvalue}" type="text"/>
                  </div>
              </div>`);
            // for(let i=0;i<plc;i++){
            //     $("#plmp").append(`<div class="fieldMapp" style="clear:both;margin-left:85px;">
            //         <div class="fromField" style="float:left;margin-top:5px;">
            //             <div>{{${i+1}}}</div>
            //         </div>
            //         <i style="float:left;min-width:4%;text-align:center;padding-left:10px;padding-right:10px;border:none;margin-top:5px;" class="material-icons ">arrow_forward</i>
            //         <div class="toField">
            //           <input id="custominput_${i+1}" style="width:200px;height:32px;float:left;margin-left:10px;margin-top:5px;" type="text"></input>
            //         </div>
            //     </div>`);
            // }   
              if(selectedOption == '' || !plhrs){
                document.getElementById(`custominput_${i+1}`).style.display="block";
              }
              else{
                document.getElementById(`custominput_${i+1}`).style.display="none";
              }
          } 
        }
        function setPlhdr(i,value){
          if(value == "custom_value"){
            document.getElementById(`custominput_${i}`).style.display="block";
          }
          else{
            document.getElementById(`custominput_${i}`).style.display="none";
          }
        }
        function getTemplateSettings(selectedTemplate){
          var settingsPlhrs =[];   
          messageTemplates.forEach(function(template){
            let settings = template.messagebirdforzohocrm__Template_Settings;
            if(settings && JSON.parse(settings).id == selectedTemplate.id){
              settingsPlhrs = JSON.parse(settings).plhrs;
            }
          })
          return settingsPlhrs;
        }
        function selectTemplate(){
            $("#plmp").html("");
            document.getElementById("input-sms-content").disabled = false;
            var templateId = $("#approvedTemplates").val();
            if(templateId == "Select_Template"){
                 document.getElementById("input-sms-content").value="";
                 document.getElementById("input-sms-content").disabled = false;
                 return "";
            }
            approvedTemplates.forEach(function(template){
                if(templateId && templateId == template.id){
                    selectedTemplate = template;
                }
            });    
            document.getElementById("input-sms-content").value=selectedTemplate.content;
            document.getElementById("input-sms-content").disabled = true;
            addMap(selectedTemplate.content,getTemplateSettings(selectedTemplate));
            
        }
          function openConversation(conversationId, convChannelId){
             for(var item in existingConversations){
                var obj = JSON.parse(existingConversations[item]);
                if(obj.id == conversationId){
                  currentAccessKey = obj.accessKey;
                  getConvInfo(conversationId,obj);
                }
              }    
              convId = conversationId;
              $(".conv-list-item").removeClass('selected');
              $("#list-"+conversationId).addClass('selected');
              //$('#existingConvListWindow').hide();
              $('.chatmessages-inner').html("");
              getConvs(0);
          }
          
          window.onload = function() {
            $(".chatmessages-inner").on('click', (function(){
                $('.chatmessage-item.unread').removeClass('unread');
            }));
        };
          
          function addScript( src ) {
            var s = document.createElement( 'script' );
            s.setAttribute( 'src', src );
            document.body.appendChild( s );
          }

          function initiateRTListeners(CID){
              try{
              if(Object.keys(RTCs).length === 0){
                   // Your web app's Firebase configuration
                var secondaryAppConfig = {
                  apiKey: "AIzaSyDaiS3ED9XFShewptQEkuGwvlZW8exrFuY",
                  authDomain: "messagebirdintegapp.firebaseapp.com",
                  databaseURL: "https://messagebirdintegapp.firebaseio.com",
                  projectId: "messagebirdintegapp",
                  storageBucket: "messagebirdintegapp.appspot.com",
                  messagingSenderId: "91346403563",
                  appId: "1:91346403563:web:02e4e7da8bb6d057c73eae",
                  measurementId: "G-4E9TX2Q887"
                };
                // Initialize Firebase
                secondaryApp = firebase.initializeApp(secondaryAppConfig, "secondary");

              }
              if(RTCs.hasOwnProperty(CID)){
                  console.log("RTC already inited "+RTCs[CID]);
                  return;
              }
              RTCs[CID] = 0;
            
                var starCountRef = secondaryApp.database().ref('cursors/'+CID+'/ts');
                      starCountRef.on('value', function(snapshot) {
                        RTCs[CID]++;
                        if(RTCs[CID] === 1){
                            return;
                        }
                        newRTEvent(CID, snapshot.val());
                      });
              }catch(err){
                  console.log(err);
              }
          }
          
          function addNewMessageToWindow(CID, MID, direction){
              if(convId !== CID){
                  console.log('Target conv window '+CID+' not open...ignoring');
                  return;
              }
              $('.chatmessages-inner').append(`<div class="chatmessage-item ${direction} ${direction == 'direction-in' ? 'unread' : ''}" id="MID-LOADING-${MID}"><div class="chatmessage-inner">
                            <div class="chatmessage-content">...</div></div>`);
              $(".chatmessages-inner").scrollTop($(".chatmessages-inner")[0].scrollHeight);
               var request = {
                                            url : "https://conversations.messagebird.com/v1/messages/"+MID,
                                            params:{
                                                    
                                            },
                                            headers:{
                                                    Authorization:"AccessKey "+currentAccessKey
                                            }
                                    }
                                    ZOHO.CRM.HTTP.get(request)
                                    .then(function(data){
                                       console.log(data);
                                       var jsonData = JSON.parse(data);
                                       jsonData.unread = true;
                                       addMessageInBox(jsonData, true);
                                       $(".chatmessages-inner").scrollTop($(".chatmessages-inner")[0].scrollHeight);
                                    });
          }
          
          function newRTEvent(CID, MID){
              addNewMessageToWindow(CID, MID, 'direction-in');
          }

          function validVal(x){
                return x !==undefined && x!==null && x!=="";
          }
