var GoogleAuth;
var currentUser = null;
var db = null;
var initTrackingId = null;

window.onload = (function () {   
     var firebaseConfig = {
        apiKey: "AIzaSyDsvl4lleRa8k-3UuDqltueXZy1V27f0Sw",
        authDomain: "ulgebra-license.firebaseapp.com",
        databaseURL: "https://ulgebra-license.firebaseio.com",
        projectId: "ulgebra-license",
        storageBucket: "ulgebra-license.appspot.com",
        messagingSenderId: "356364764905",
        appId: "1:356364764905:web:4bea988d613e73e1805278",
        measurementId: "G-54FN510HDW"
      };

    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    storageRef = firebase.storage().ref();
    db = firebase.firestore();

    firebase.auth().onAuthStateChanged(function (user) {
        console.log(user);
        user ? handleSignedInUser(user) : handleSignedOutUser();
         handleauthoriztion();
    });

    // handleClientLoad();
    var uiConfig = {
        callbacks: {
            // Called when the user has been successfully signed in.
            'signInSuccessWithAuthResult': function (authResult, redirectUrl) {
                if (currentUser == null && authResult.user) {
                    handleSignedInUser(authResult.user);
                     handleauthoriztion();
                }
                if (authResult.additionalUserInfo) {
                    document.getElementById('is-new-user').textContent =
                            authResult.additionalUserInfo.isNewUser ?
                            'New User' : 'Existing User';
                }
                // Do not redirect.
                return false;
            },
            uiShown: function () {
                $("#credential_picker_container").hide();
                $("#notSignedInError").hide();
                $(".loader").hide();
                $(".showAfterSignIn").show();
            }
        },
        signInFlow: 'popup',
        signInOptions: [
            {
                provider: firebase.auth.GoogleAuthProvider.PROVIDER_ID,
                clientId: '356364764905-ua06urrrgp9qh0u61qqtb0tk6cv40jmm.apps.googleusercontent.com'
            },
            {
                "provider": firebase.auth.EmailAuthProvider.PROVIDER_ID
            }
        ],
        credentialHelper: firebaseui.auth.CredentialHelper.GOOGLE_YOLO,
        tosUrl: './terms-of-service.html',
        privacyPolicyUrl: './privacy-policy.html'

    };
    var ui = new firebaseui.auth.AuthUI(firebase.auth());
    ui.start('#firebaseui-auth-container', uiConfig);

    $("#signoutBtn").click((function () {
        firebase.auth().signOut();
    }));

});

var handleSignedOutUser = function () {
    console.log("inside handle signed out");
   $("#username").text("Signed Out");
};

function handleauthoriztion(){
    console.log("inside handle authorization");
}

function doPageSignAction(){
    
}

var handleSignedInUser = function (user) {
    console.log("inside handle signed in");
    currentUser = firebase.auth().currentUser;
    $("#username").text(user.displayName+" : "+user.email);
};