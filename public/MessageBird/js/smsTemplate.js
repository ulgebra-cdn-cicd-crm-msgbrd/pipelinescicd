		var emailContent = [];
		var emailContentText = "";
		var startIndex =0;
		var endIndex = 0;
		var currentEditor="";
		var subject="";
		var sel;
		var range;
		var calendarsMap;
		var recordId;
		var recordModule;
		var ButtonPosition ;
		var accessKey = null;
		var approvedTemplates=[];
		var moduleFields =[];
        document.addEventListener("DOMContentLoaded", function(event) {
        	// var cusotmerData = ["Owner", "Email", "$currency_symbol", "Other_Phone", "Mailing_State", "$upcoming_activity", "Other_State", "Other_Country", "Last_Activity_Time", "Department", "$process_flow", "Assistant", "Mailing_Country", "id", "$approved", "Reporting_To", "$approval", "Other_City", "Created_Time", "$editable", "Home_Phone", "$status", "Created_By", "Secondary_Email", "Description", "Vendor_Name", "Mailing_Zip", "$photo_id", "Twitter", "Other_Zip", "Mailing_Street", "Salutation", "First_Name", "Full_Name", "Asst_Phone", "Record_Image", "Modified_By", "Skype_ID", "Phone", "Account_Name", "Email_Opt_Out", "Modified_Time", "Date_of_Birth", "Mailing_City", "Title", "Other_Street", "Mobile", "Last_Name", "Lead_Source", "Tag", "Fax"];
			
			ZOHO.embeddedApp.on("PageLoad", function(record) {
				ZOHO.CRM.META.getFields({"Entity":"Users"}).then(function(value){
				  	userFields = value.fields;
					value.fields.forEach(function(field){
						addListItem("dropdown-menu-user",field.field_label,"dropdown-item","Users."+field.field_label);
					});
				});
				if(record.EntityId){
	               	recordId = record.EntityId[0];
	               	recordModule = record.Entity;
	               	ButtonPosition = record.ButtonPosition;
	               	if(ButtonPosition == "DetailView" || ButtonPosition == "ListViewEachRecord"){
		               	ZOHO.CRM.API.getRecord({Entity:recordModule,RecordID:recordId})
						.then(function(data){
							document.getElementById("templateName").value = data.data[0].Name;
							selectModule(data.data[0].messagebirdforzohocrm__Module_Name);
							document.getElementById("emailContentEmail").innerText = data.data[0].messagebirdforzohocrm__Message;
							if(data.data[0].messagebirdforzohocrm__Template_Settings){
								setTemplateSettings(JSON.parse(data.data[0].messagebirdforzohocrm__Template_Settings));
							}
						})
					}
				}	
				else{
					selectModule("Contacts");
				}	
	        });
			
			$("#templateList").append('<li class="templateItem"  onclick="selectModule('+"'Calendly'"+')">Calendly</li>')
	        ZOHO.embeddedApp.init().then(function(){
                ZOHO.CRM.API.getOrgVariable("messagebirdforzohocrm__accessKey").then(function(apiKey){
                    if(apiKey && apiKey.Success && !valueExists(apiKey.Success.Content)){
                        document.getElementById("ErrorText").innerText = "Please enter your MessageBird ACCESS_KEY in extension settings page.";
                        document.getElementById("Error").style.display= "block";
                    }
                    else{
                        accessKey = apiKey.Success.Content;
                    }
                });
            });
	        const el = document.getElementById('emailContentEmail');

			el.addEventListener('paste', (e) => {
			  // Get user's pasted data
			  let data = e.clipboardData.getData('text/html') ||
			      e.clipboardData.getData('text/plain');
			  
			  // Filter out everything except simple text and allowable HTML elements
			  let regex = /<(?!(\/\s*)?()[>,\s])([^>])*>/g;
			  data = data.replace(regex, '');
			  
			  // Insert the filtered content
			  document.execCommand('insertHTML', false, data);

			  // Prevent the standard paste behavior
			  e.preventDefault();
			});
			var content_id = 'emailContentEmail';  
			max = 1600;
			//binding keyup/down events on the contenteditable div
			$('#'+content_id).keyup(function(e){ check_charcount(content_id, max, e); });
			$('#'+content_id).keydown(function(e){ check_charcount(content_id, max, e); });

			function check_charcount(content_id, max, e)
			{   
			    if(e.which != 8 && $('#'+content_id).text().length > max)
			    {
			    	document.getElementById("ErrorText").innerText = "Message should be within 2000 characters.";
	        		document.getElementById("Error").style.display= "block";
	        		// document.getElementById("ErrorText").style.color="red";
					setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
			       // $('#'+content_id).text($('#'+content_id).text().substring(0, max));
			       e.preventDefault();
			    }
			}
			var approvedCheckbox = document.querySelector('input[id="approved"]');
			if(approvedCheckbox){
				document.getElementById("approvedTemplates").style.display="none";
				approvedCheckbox.addEventListener('change', () => {
				  if(approvedCheckbox.checked) {
				  		getApprovedTemplates();
				  	    document.getElementById("plmp").style.display="block";
				  	    document.getElementById("approvedTemplates").style.display="block";
				  	    document.getElementById("emailContentEmail").setAttribute("contenteditable", false);
				  } else {
				  	    document.getElementById("plmp").style.display="none";
				  	    document.getElementById("approvedTemplates").style.display="none";
				  	    document.getElementById("emailContentEmail").setAttribute("contenteditable", true);
				  }
				});
				var selectedTemplate = document.getElementById("approvedTemplates");
				selectedTemplate.addEventListener('change', () => {
					selectTemplate();
				});
			}

        });
		function selectTemplate(){
			$("#plmp").html("");
			var selectedTemplate = document.getElementById("approvedTemplates");
			if(selectedTemplate){
			    approvedTemplates.forEach(function(template){
				  	if(template.id == selectedTemplate.value){
				  		document.getElementById("emailContentEmail").setAttribute("contenteditable", false);
				  		document.getElementById("templateName").value = template.name;
						document.getElementById("emailContentEmail").innerText = template.content ;
						addMap(template.content);
				  	}
			  	})
			}  	
		}
        function addMap(content,plhrs){
        	let plc = 0;
        	while(1){
        		if(content.indexOf("{{"+(plc+1)+"}}") != -1){
        			plc++;
        		}
        		else{
        			break;
        		}
        	}
        	for(let i=0;i<plc;i++){
        		let options = '<option value="custom_value">Custom Value</option>';
        		let selectedOption = '';
        		let customvalue = ''; 
        		moduleFields.forEach(function(field){
        			let field_label = field;
        			if(field.field_label){
        				field_label = field.field_label;
        			}
        			if(plhrs && plhrs[i] && plhrs[i].indexOf("."+field_label) != -1){
        				selectedOption = '<option value="'+field_label+'">'+field_label+'</option>';
        			} 
        			else{
	        			options = options + '<option value="'+field_label+'">'+field_label+'</option>'
        			}
	        	});
	        	if(plhrs && plhrs[i] && selectedOption == ''){
        			customvalue = plhrs[i];
        		}
        		$("#plmp").append(`<div class="fieldMapp" style="clear:both;margin-left:85px;">
	                <div class="fromField">
	                    <div>{{${i+1}}}</div>
	                </div>
	                <i style="float:left;min-width:4%;text-align:center;padding-left:10px;padding-right:10px;" class="material-icons ">arrow_forward</i>
	                <div class="toField">
	                    <div class="styled-select slate fieldMap" style="float:left;margin-left:10px;margin-top:5px;">
		                  <select  name="field4" id="plhr_${i+1}" onChange="setPlhdr(${i+1},this.value)">${selectedOption+options}
		                  </select>
		                </div>	
	                  <input id="custominput_${i+1}" value="${customvalue}" style="width:200px;height:32px;float:left;margin-left:10px;margin-top:5px;" type="text"></input>
	                </div>
	            </div>`);
	            if(selectedOption == '' || !plhrs){
	            	document.getElementById(`custominput_${i+1}`).style.display="block";
	            }
	            else{
	            	document.getElementById(`custominput_${i+1}`).style.display="none";
	            }
        	}	
        }
        function setPlhdr(i,value){
        	if(value == "custom_value"){
        		document.getElementById(`custominput_${i}`).style.display="block";
        	}
        	else{
        		document.getElementById(`custominput_${i}`).style.display="none";
        	}
        }
        function setTemplateSettings(settings){
        	document.querySelector('input[id="approved"]').checked=true;
        	document.getElementById("plmp").style.display="block";
			document.getElementById("approvedTemplates").style.display="block";
			document.getElementById("emailContentEmail").setAttribute("contenteditable", false);
			getApprovedTemplates(settings.id).then(function(template){
				addMap(template.content,settings.plhrs);
			})
		}
        function getApprovedTemplates(templateId){
        	var request = {
                url : "https://integrations.messagebird.com/v1/public/whatsapp/templates",
                headers:{
                        Authorization:"AccessKey "+accessKey,
                }
            }
            $("#approvedTemplates").html("");
            $("#plmp").html("");
            return ZOHO.CRM.HTTP.get(request).then(function(data){
            	console.log(data);
            	approvedTemplates = JSON.parse(data);
            	let templatesUi ='';
            	let selectedOption='<option value="Select_Template">Select Template</option>';
            	let selectedTemplate;
            	approvedTemplates.forEach(function(template){
            		if(templateId && templateId == template.id){
            			selectedTemplate = template;
            			selectedOption = '<option value="'+template.id+'">'+template.name+'</option>';
            		}
            		else{
            			templatesUi = templatesUi +'<option value="'+template.id+'">'+template.name+'</option>';
            		}
            	});
            	$("#approvedTemplates").append(selectedOption+templatesUi);
            	return selectedTemplate;
            	//"[{"name":"issue_ticket_resolve","language":"en","status":"APPROVED","content":"Hi {{1}}, We hope your issue on the extension {{2}} got resolved. Kindly let us know your feedback by replying to this message. Reach us in case of any other clarifications also.","id":"401833644114121","category":"ISSUE_RESOLUTION","createdAt":"2020-08-20T17:40:29.420516905Z","updatedAt":"2020-08-20T17:44:05.305544373Z"}]"
            });	

        }
        function valueExists(val) {
	        return val !== null && val !== undefined && val.length > 1 && val!=="null";
	    }
		function selectModule(module){
			document.getElementById("selectedmodule").innerText = module;
			document.getElementById("moduleFields").innerText = "Insert "+module+" Fields";
			var customerData = [];
			var smsContent = document.getElementById("emailContentEmail").innerText;
			if(module == "Leads"){
				if(smsContent.indexOf("${Contacts.") != -1 || smsContent.indexOf("${Events.") != -1){
					document.getElementById("emailContentEmail").innerHTML ="";
				}
			}
			else if(module == "Contacts"){
				if(smsContent.indexOf("${Leads.") != -1 || smsContent.indexOf("${Events.") != -1){
					document.getElementById("emailContentEmail").innerHTML ="";
				}
			}
			else if(module == "Events"){
				if(smsContent.indexOf("${Contacts.") != -1 || smsContent.indexOf("${Leads.") != -1){
					document.getElementById("emailContentEmail").innerHTML ="";
				}
				customerData = ["Receiver First Name","Receiver Last Name","Receiver Full Name"];
			}
			document.getElementById("dropdown-menu-email").innerHTML="";
			if(module == "Calendly"){
				var toFields=["Event_Id","Event_Name","Start_Time","End_Time","Invitee_Name","Invitee_Email","Text_reminder_number","Location","Invitee_Timezone","Reschedule_Link","Cancel_Link"];
				for(let i=1;i<=10;i++){
					toFields.push("Answer_"+i);
				}
				toFields.forEach(function(field){
					addListItem("dropdown-menu-email",field,"dropdown-item",module+"."+field);
				});
				moduleFields = toFields;
				selectTemplate();
			}
			else{
				ZOHO.CRM.META.getFields({"Entity":module}).then(function(data){
					customerData.forEach(function(field){
						addListItem("dropdown-menu-email",field,"dropdown-item",module+"."+field);
					});
					moduleFields = data.fields;
					data.fields.forEach(function(field){ 
						addListItem("dropdown-menu-email",field.field_label,"dropdown-item",module+"."+field.field_label);
					});
					selectTemplate();
				});	
			}	
		}

        function showEmail(editor){
			for(var i=0; i<emailContent.length;i++){
				if(emailContent[i].emailId == editor.id){
					document.getElementById("emailContentEmail").innerHTML = emailContent[i].emailContent;
					document.getElementById("subjectEmail").innerHTML=emailContent[i].subject;
					break;
				}
			}
		}
        function saveTemplate(){
        	var name = document.getElementById("templateName").value;
        	var templateModule = document.getElementById("selectedmodule").innerText;
        	var smsContent = document.getElementById("emailContentEmail").innerText;
        	var othermodule=false;
        	if(templateModule == "Leads" && (smsContent.indexOf("${Contacts.") != -1 || smsContent.indexOf("${Events.") != -1)){
				othermodule=true;
			}
			else if(templateModule == "Contacts" && (smsContent.indexOf("${Leads.") != -1 || smsContent.indexOf("${Events.") != -1)){
				othermodule=true;
			}
			else if(templateModule == "Events" && (smsContent.indexOf("${Contacts.") != -1 || smsContent.indexOf("${Leads.") != -1)){
				othermodule=true;
			}
			if(othermodule){
				document.getElementById("ErrorText").innerText = "Message Contains Other Modules Merge Fields.Please change it.";
        		document.getElementById("Error").style.display= "block";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 2000);
				return ;
			}
        	if(name == ""){
        		document.getElementById("ErrorText").innerText = "Template Name cannot be empty.";
        		document.getElementById("Error").style.display= "block";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
				return ;
        	}
        	if(templateModule == ""){
        		document.getElementById("ErrorText").innerText = "Please Choose Module";
        		document.getElementById("Error").style.display= "block";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
				return ;
        	}
        	if(document.getElementById("emailContentEmail").innerText.replace(/\n/g,"").replace(/\t/g,"").replace(/ /g,"") == ""){
        		document.getElementById("ErrorText").innerText = "Message cannot be empty.";
        		document.getElementById("Error").style.display= "block";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
        	}
        	else{
        		var message = document.getElementById("emailContentEmail").innerText;
				var req_data = {"Name":name,"messagebirdforzohocrm__Message":message,"messagebirdforzohocrm__Module_Name":templateModule};
        		var approvedCheckbox = document.querySelector('input[id="approved"]');
				if(approvedCheckbox && approvedCheckbox.checked){
        			var templateSettings = {"plhrs":[]};
					var selectedTemplate = document.getElementById("approvedTemplates");
					if(selectedTemplate && selectedTemplate.value != "Select_Template"){
						templateSettings.id = selectedTemplate.value;
						var plhrs =[];
						var i =0;
						while(1){
							let plc = document.getElementById(`plhr_${i+1}`);
							if(plc){
								if(plc.value == "custom_value"){
									templateSettings.plhrs.push(document.getElementById(`custominput_${i+1}`).value);
								}
								else{
									templateSettings.plhrs.push("${"+templateModule+"."+plc.value+"}");
								}
								i++;
							}
							else{
								break;
							}
						}
						req_data["messagebirdforzohocrm__Template_Settings"]=JSON.stringify(templateSettings);
					}
					else{
						req_data["messagebirdforzohocrm__Template_Settings"]="";
					}	
				} 
				else{
					req_data["messagebirdforzohocrm__Template_Settings"]="";
				} 
	        	document.getElementById("ErrorText").innerText = "SMS Template is saving...";
				document.getElementById("Error").style.display= "block";
	        	if(ButtonPosition){
	        		req_data["id"]=recordId;
					ZOHO.CRM.API.updateRecord({Entity:"messagebirdforzohocrm__MessageBird_Templates",APIData:req_data,Trigger:["workflow"]}).then(function(response){
						var responseInfo	= response.data[0];
						var resCode			= responseInfo.code;
						if(resCode == 'SUCCESS'){
							document.getElementById("ErrorText").innerHTML = "Your SMS Template saved successfully.";
							setTimeout(function(){
								ZOHO.CRM.UI.Popup.closeReload();
							}, 1500);
						}
						else{
							document.getElementById("ErrorText").innerText = "Opps! Something went wrong from server side. Please try after sometimes!!!";
			        		document.getElementById("Error").style.display= "block";
							setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
						}
					});
	        	}
	        	else{
					ZOHO.CRM.API.insertRecord({Entity:"messagebirdforzohocrm__MessageBird_Templates",APIData:req_data}).then(function(response){
						var responseInfo	= response.data[0];
						var resCode			= responseInfo.code;
						if(resCode == 'SUCCESS'){
							document.getElementById("ErrorText").innerText = "Your SMS Template saved successfully.";
							var recordId	= responseInfo.details.id;
							setTimeout(function(){
								ZOHO.CRM.UI.Record.open({Entity:"messagebirdforzohocrm__MessageBird_Templates",RecordID:recordId})
								.then(function(data){
									ZOHO.CRM.UI.Popup.close();
								})
							}, 1500);								
						}
						else{
							document.getElementById("ErrorText").innerText = "Opps! Something went wrong from server side. Please try after sometimes!!!";
			        		document.getElementById("Error").style.display= "block";
							setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
						}
					});
				}	
			}		
        }
		function googleTranslateElementInit() {
		  new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
		}
        function addListItem(id,text,className,value){
			if(className == "dropdown-item"){
				var linode = '<li class="'+className+'"><button class="'+className+'" onclick="insert(this)">'+text+'<input type="hidden" value="'+value+'"></button></li>';
			}
			else{
				var linode = '<li class="'+className+'">'+text+'</li>';
			}
			$('#'+id).append(linode);

        }
		function styling(tag)
		{
			document.execCommand(tag);
		}
		function link(){
			$("#linkForm").slideToggle("slow");
		}
		function image(){
			$("#imageForm").slideToggle("slow");
		}
		function addLink(){
			var href = document.getElementById("linkUrl").value;
		    if (range) {
				if(range.startOffset == range.endOffset){
					if(range.commonAncestorContainer.parentNode.href){
						range.commonAncestorContainer.parentNode.href=href;
					}
					else{
						var span = document.createElement('a');
						span.setAttribute('href',href);
						span.innerText = href;
						range.insertNode(span);
			        	range.setStartAfter(span);
			        }	
				}
				else{
					var data = range.commonAncestorContainer.data;
					var start = range.startOffset;
					var end = range.endOffset;
					range.commonAncestorContainer.data="";
					var span = document.createElement('span');
					span.appendChild( document.createTextNode(data.substring(0,start)) );
					var atag = document.createElement('a');
					atag.setAttribute('href',href);
					atag.innerText = data.substring(start,end);
					span.appendChild(atag);
					span.appendChild( document.createTextNode(data.substring(end)) );
					range.insertNode(span);
		        	range.setStartAfter(span);
				}
		        range.collapse(true);
		    }
			$("#linkForm").slideToggle("slow");
		}
		function addImage(){
			var href = document.getElementById("imageUrl").value;
			var span = document.createElement('img');
			span.setAttribute('src',href);
			span.innerText = href;
			range.insertNode(span);
        	range.setStartAfter(span);
			$("#imageForm").slideToggle("slow");
		}
		function openlink(){
			sel = window.getSelection();
		    if (sel && sel.rangeCount) {
		        range = sel.getRangeAt(0);
		      }  
			if(range && range.commonAncestorContainer.wholeText){
				if(range.commonAncestorContainer.parentNode.href){
					document.getElementById("linkUrl").value = range.commonAncestorContainer.parentNode.href;
					$("#linkForm").slideToggle("slow");
				}
			}	
		}
		function insert(bookingLink){
    		// var bookingLink = this;
			var range;
			if (sel && sel.rangeCount && isDescendant(sel.focusNode)){
		        range = sel.getRangeAt(0);
		        range.collapse(true);
    		    var span = document.createElement("span");
    		    span.appendChild( document.createTextNode('${'+bookingLink.children[0].value+'}') );
        		range.insertNode(span);
	    		range.setStartAfter(span);
		        range.collapse(true);
		        sel.removeAllRanges();
		        sel.addRange(range);
		    }    
		}
		function isDescendant(child) {
			var parent = document.getElementById("emailContentEmail");
		     var node = child.parentNode;
		     while (node != null) {
		         if (node == parent || child == parent) {
		             return true;
		         }
		         node = node.parentNode;
		     }
		     return false;
		}
		function enableSchedule(element){
			if(element.checked == true){
				document.getElementById("send").innerText="Schedule";
			}
			else{
				document.getElementById("send").innerText="Send";
			}
		}
		function openDatePicker(){
    		document.getElementById("dateTime").style.display= "block";
    		document.getElementById("Error").style.display= "block";
		}	
		function scheduleClose(){
    		document.getElementById("dateTime").style.display= "none";
    		document.getElementById("Error").style.display= "none";
    		document.getElementById("scheduleCheck").checked =true;
    		document.getElementById("send").innerText="Schedule";
    		var date = document.getElementById("datepicker").value;
    		var time = document.getElementById("timeList").value;
    		document.getElementById("scheduledDateTime").innerText=new Date(date).toDateString()+" at "+time;
		}


