		var sel;
		var range;
		var recordIds;
		var recordModule;
		var ButtonPosition;
		var smsTemplates;
		var templateId;
		var scheduledTime;
		var func_name;
		var countryCode;
		var moduleFields;
		var userFields;
		var authToken;
		var authId;
		var src;
		var currentRecords =[];
		var users =[];
		var channelId;
		var channelsSettings=[];
		var selectedTemplate;
		var namespace="";
		var selectedChannel = null;
		var phoneField = "Mobile";
		var channelsList=[];
		var approvedTemplatesSettings = [];
		var approvedTemplates =[];
		var conversationModuleName;
		var messageBirdId;
		var namespace2;
		var accessKey2;
		var currentAccessKey=null;
		var currentMsgBirdId = null;
        document.addEventListener("DOMContentLoaded", function(event) {
        	ZOHO.embeddedApp.on("PageLoad", function(record) {
               	recordIds = record.EntityId;
               	recordModule = record.Entity;
               	conversationModuleName = recordModule.substring(0,recordModule.length-1);
               	messageBirdId = "MessageBird_Id";
                if(recordModule == "Contacts" || recordModule == "Leads"){
                  conversationModuleName = "messagebirdforzohocrm__"+conversationModuleName;
                  messageBirdId = "messagebirdforzohocrm__"+messageBirdId;
                }
                currentMsgBirdId = messageBirdId;
               	ButtonPosition = record.ButtonPosition;
				selectModule(recordModule);
				ZOHO.CRM.META.getFields({"Entity":"Users"}).then(function(data){
					userFields = data.fields;
					data.fields.forEach(function(field){
						addListItem("dropdown-menu-user",field.field_label,"dropdown-item","Users."+field.field_label);
					});
				});	
				ZOHO.CRM.API.getAllUsers({Type:"AllUsers"}).then(function(data){
					users = data.users;
				});	

				
               	ZOHO.CRM.API.searchRecord({Entity:"messagebirdforzohocrm__MessageBird_Templates",Type:"criteria",Query:"(messagebirdforzohocrm__Module_Name:equals:"+recordModule+")",delay:false})
				.then(function(data){
					smsTemplates = data.data;
					
					smsTemplates.forEach(function(template){
						let settings = template.messagebirdforzohocrm__Template_Settings;
            			if(settings && JSON.parse(settings).id){
            				approvedTemplatesSettings.push(template);
            			}	
					})
					var templateList="";
					if(data.data){
						for(let i=0;i <data.data.length;i++){
							if(!data.data[i].messagebirdforzohocrm__Template_Settings){
								templateList =templateList+ '<li class="templateItem" id="'+data.data[i].id+'" onclick="showsms(this)"></li>';
							}
						}
						$('#templateList').append(templateList);
						if(templateList == ""){
							$('#templateList').append('<li style="text-align:center;">No Templates</li>');
						}
						else{
							for(let i=0;i <data.data.length;i++){
								if(!data.data[i].messagebirdforzohocrm__Template_Settings){
									document.getElementById(data.data[i].id).innerText = data.data[i].Name;
								}	
							}
						}
					}
					else{
						$('#templateList').append('<li style="text-align:center;">No Templates</li>');
					}	
				}) 
				ZOHO.CRM.API.getOrgVariable("messagebirdforzohocrm__phonefields").then(function(phoneFieldsData){
                      if(phoneFieldsData && phoneFieldsData.Success && phoneFieldsData.Success.Content && phoneFieldsData.Success.Content != "0"){
                        var phoneFieldsSetting =JSON.parse(phoneFieldsData.Success.Content);
                        if(!phoneFieldsSetting["phoneField"]){
                        	phoneFieldsSetting["phoneField"] ={};
                        }
                        phoneField = phoneFieldsSetting["phoneField"][recordModule];
                        namespace = phoneFieldsSetting["namespace"];
                        if(phoneFieldsSetting && phoneFieldsSetting.isSecondAccountSupported && phoneFieldsSetting.apiKey2){
	                        namespace2 = phoneFieldsSetting["namespace2"];
	                        accessKey2=phoneFieldsSetting.apiKey2;
	                    }
                      }
                });
                Promise.all([ZOHO.CRM.API.getRecord({Entity:recordModule,RecordID:recordIds}),ZOHO.CRM.API.getOrgVariable("messagebirdforzohocrm__accessKey"),ZOHO.CRM.API.getOrgVariable("messagebirdforzohocrm__channels"),ZOHO.CRM.CONFIG.getCurrentUser()])
				.then(function(data){
                    currentUser = data[3];
					var apiKey = data[1];
					var channels = data[2];
					if(apiKey && apiKey.Success && apiKey.Success.Content && !valueExists(apiKey.Success.Content)){
						document.getElementById("loader").style.display= "none";
						document.getElementById("container").style.display= "block";
						document.getElementById("ErrorText").innerText = "Please enter your Messageird AccessKey in extension settings page.";
			        	document.getElementById("Error").style.display= "block";
			        	redirectToSettings();
					}
					else{
                        accessKey = apiKey.Success.Content;
                        currentAccessKey = accessKey;
						currentRecords = data[0].data;
						// var channelList="";
						// var selectedChannelName="";

						// channelsSettings = JSON.parse(channels.Success.Content);
						// channelsSettings.forEach(function(channel){
						// 	if(channel.channelId && (channel.userId == "everyone" || !channel.userId ||channel.userId == data[3].users[0].id)){
						// 		if(!selectedChannelName){
						// 			selectedChannelName = channel.channelName;
						// 			selectedChannel = channel;
						// 			channelId = channel.channelId;
						// 		}	
						// 		channelList =channelList+ '<li class="templateItem" id="'+channel.channelId+'" onclick="selectChannel(this)">'+channel.channelName+'</li>';
						// 	}
						// })
						getChannels();
						
						ZOHO.CRM.API.getOrgVariable("messagebirdforzohocrm__countryCode").then(function(apiKey){
							if(apiKey && apiKey.Success && apiKey.Success.Content && apiKey.Success.Content != "0"){
								countryCode = apiKey.Success.Content;
							}
							document.getElementById("editNumberui").style.display= "none";
							document.getElementById("loader").style.display= "none";
							document.getElementById("container").style.display= "block";
							func_name = "plivosmsforzohocrm__bulksms";
						});	
							
					}	
				})
	        });
        	ZOHO.embeddedApp.init();
				// $('#timeList').append(timeOptions);
	   //  		$('#datepicker').datepicker().datepicker('setDate',new Date());
	   //  		var date = document.getElementById("datepicker").value;
	   //  		var time = document.getElementById("timeList").value;
	   //  		document.getElementById("scheduledDateTime").innerText=new Date(date).toDateString()+" at "+time +" ("+Intl.DateTimeFormat().resolvedOptions().timeZone+")";
        	const el = document.getElementById('emailContentEmail');

			el.addEventListener('paste', (e) => {
			  // Get user's pasted data
			  let data = e.clipboardData.getData('text/html') ||
			      e.clipboardData.getData('text/plain');
			  
			  // Filter out everything except simple text and allowable HTML elements
			  let regex = /<(?!(\/\s*)?()[>,\s])([^>])*>/g;
			  data = data.replace(regex, '');
			  
			  // Insert the filtered content
			  document.execCommand('insertHTML', false, data);

			  // Prevent the standard paste behavior
			  e.preventDefault();
			});
			var content_id = 'emailContentEmail';  
			max = 2000;
			//binding keyup/down events on the contenteditable div
			$('#'+content_id).keyup(function(e){ check_charcount(content_id, max, e); });
			$('#'+content_id).keydown(function(e){ check_charcount(content_id, max, e); });

			function check_charcount(content_id, max, e)
			{   
			    if(e.which != 8 && $('#'+content_id).text().length > max)
			    {
			    	document.getElementById("ErrorText").innerText = "Message should be within 2000 characters.";
	        		document.getElementById("Error").style.display= "block";
	        		// document.getElementById("ErrorText").style.color="red";
					setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
			       // $('#'+content_id).text($('#'+content_id).text().substring(0, max));
			       e.preventDefault();
			    }
			}
			
			var approvedTemplates = document.getElementById("approvedTemplates");
			approvedTemplates.addEventListener('change', () => {
				selectTemplate();
			});
        });
        var secondChannelsList=[];
        function getChannels(){
	        var fetchchannelList = [];
	        ["whatsapp","sms","whatsapp_sandbox"].forEach(function(channel){
	            fetchchannelList.push(ZOHO.CRM.HTTP.get({
	                url : "https://integrations.messagebird.com/v1/public/integrations/"+channel,
	                headers:{
	                        Authorization:"AccessKey "+accessKey,
	                }
	            }));
	        })
	        if(accessKey2){
		        ["whatsapp","sms","whatsapp_sandbox"].forEach(function(channel){
		            fetchchannelList.push(ZOHO.CRM.HTTP.get({
		                url : "https://integrations.messagebird.com/v1/public/integrations/"+channel,
		                headers:{
		                        Authorization:"AccessKey "+accessKey2,
		                }
		            }));
		        })
		    }    
	        return Promise.all(fetchchannelList).then(function(data){
	            console.log(data);
	            var i =0;
	            data.forEach(function(item){
	              if(i< 3){
	                if(JSON.parse(item).items){
	                  channelsList = channelsList.concat(JSON.parse(item).items);
	                }
	              }
	              else{
	                if(JSON.parse(item).items){
	                  secondChannelsList = secondChannelsList.concat(JSON.parse(item).items);
	                }
	              } 
	              i++; 
	            })
	            var channelListUi='';
	            channelsList.forEach(function(channel){
	                if(!selectedChannel){
						selectedChannel = channel;
						channelId = channel.id;
					}	
					channelListUi =channelListUi+ '<li class="templateItem" id="'+channel.id+'" onclick="selectChannel(this)">'+channel.name+'</li>';
				});
				secondChannelsList.forEach(function(channel){
	                if(!selectedChannel){
						selectedChannel = channel;
						channelId = channel.id;
					}	
					channelListUi =channelListUi+ '<li class="templateItem" id="'+channel.id+'" onclick="selectChannel(this,true)">'+channel.name+' (Second Account)</li>';
				});
				channelListUi =channelListUi+ '<li class="templateItem" id="short_code" onclick="selectChannel(this)">Use Short Code</li>';
				$('#channelList').append(channelListUi);
				document.getElementById("selectedChannel").innerText = selectedChannel.name;
				if(selectedChannel && selectedChannel.slug && selectedChannel.slug.indexOf("whatsapp") != -1){
					getApprovedTemplates();
				}
	        });
	    }
        function redirectToSettings(){
	      ZOHO.CRM.UI.Widget.open({Entity:"MessageBird"})
	      .then(function(data){
	      	setTimeout(function(){
	      	  ZOHO.CRM.UI.Popup.close();
	      	},1000)
	      })
	    }
	    function getTemplateSettings(selectedTemplate){
          var settingsPlhrs =[];   
          approvedTemplatesSettings.forEach(function(template){
            let settings = template.messagebirdforzohocrm__Template_Settings;
            if(settings && JSON.parse(settings).id == selectedTemplate.id){
              settingsPlhrs = JSON.parse(settings).plhrs;
            }
          })
          return settingsPlhrs;
        }
        function selectTemplate(){
			$("#plmp").html("");
			var seltemplate = document.getElementById("approvedTemplates");
			if(seltemplate){
			    approvedTemplates.forEach(function(template){
				  	if(template.id == seltemplate.value){
				  		selectedTemplate = template;
				  		document.getElementById("emailContentEmail").setAttribute("contenteditable", false);
						document.getElementById("emailContentEmail").innerText = template.content ;
						addMap(selectedTemplate.content,getTemplateSettings(selectedTemplate));
				  	}
			  	})
			}  	
		}
        function addMap(content,plhrs){
        	let plc = 0;
        	while(1){
        		if(content.indexOf("{{"+(plc+1)+"}}") != -1){
        			plc++;
        		}
        		else{
        			break;
        		}
        	}
        	for(let i=0;i<plc;i++){
        		let options = '<option value="custom_value">Custom Value</option>';
        		let selectedOption = '';
        		let customvalue = ''; 
        		moduleFields.forEach(function(field){
        			let field_label = field;
        			if(field.field_label){
        				field_label = field.field_label;
        			}
        			if(plhrs && plhrs[i] && plhrs[i].indexOf("."+field_label) != -1){
        				selectedOption = '<option value="'+field_label+'">'+field_label+'</option>';
        			} 
        			else{
	        			options = options + '<option value="'+field_label+'">'+field_label+'</option>'
        			}
	        	});
	        	if(plhrs && plhrs[i] && selectedOption == ''){
        			customvalue = plhrs[i];
        		}
        		$("#plmp").append(`<div class="fieldMapp" style="clear:both;margin-left:85px;">
	                <div class="fromField">
	                    <div>{{${i+1}}}</div>
	                </div>
	                <i style="float:left;min-width:4%;text-align:center;padding-left:10px;padding-right:10px;border:none;" class="material-icons ">arrow_forward</i>
	                <div class="toField">
	                    <div class="styled-select slate fieldMap" style="float:left;margin-left:10px;margin-top:5px;">
		                  <select  name="field4" id="plhr_${i+1}" onChange="setPlhdr(${i+1},this.value)">${selectedOption+options}
		                  </select>
		                </div>	
	                  <input id="custominput_${i+1}" value="${customvalue}" style="width:200px;height:32px;float:left;margin-left:10px;margin-top:5px;" type="text"></input>
	                </div>
	            </div>`);
	            if(selectedOption == '' || !plhrs){
	            	document.getElementById(`custominput_${i+1}`).style.display="block";
	            }
	            else{
	            	document.getElementById(`custominput_${i+1}`).style.display="none";
	            }
        	}	
        }
        function setPlhdr(i,value){
        	if(value == "custom_value"){
        		document.getElementById(`custominput_${i}`).style.display="block";
        	}
        	else{
        		document.getElementById(`custominput_${i}`).style.display="none";
        	}
        }
		function getApprovedTemplates(templateId){
        	var request = {
                url : "https://integrations.messagebird.com/v1/public/whatsapp/templates",
                headers:{
                        Authorization:"AccessKey "+currentAccessKey,
                }
            }
            $("#approvedTemplates").html("");
            $("#plmp").html("");
            return ZOHO.CRM.HTTP.get(request).then(function(data){
            	console.log(data);
            	approvedTemplates = JSON.parse(data);
            	let templatesUi ='';
            	let selectedOption='<option value="Select_Template">Select WhatsApp Approved Template</option>';
            	approvedTemplates.forEach(function(template){
            		if(templateId && templateId == template.id){
            			selectedTemplate = template;
            			selectedOption = '<option value="'+template.id+'">'+template.name+'</option>';
            		}
            		else{
            			templatesUi = templatesUi +'<option value="'+template.id+'">'+template.name+'</option>';
            		}
            	});
            	if(selectedOption+templatesUi != ""){
            		
            		document.getElementById("approvedTemplatesUi").style.display="block";
            	}
            	else{
            		document.getElementById("approvedTemplatesUi").style.display="none";
            	}
            	$("#approvedTemplates").append(selectedOption+templatesUi);
            	return selectedTemplate;
            	//"[{"name":"issue_ticket_resolve","language":"en","status":"APPROVED","content":"Hi {{1}}, We hope your issue on the extension {{2}} got resolved. Kindly let us know your feedback by replying to this message. Reach us in case of any other clarifications also.","id":"401833644114121","category":"ISSUE_RESOLUTION","createdAt":"2020-08-20T17:40:29.420516905Z","updatedAt":"2020-08-20T17:44:05.305544373Z"}]"
            });	

        }
		function selectModule(module){
			var customerData = [];
			document.getElementById("moduleFields").innerText = "Insert "+module+" Fields";
			document.getElementById("dropdown-menu-email").innerHTML="";
			ZOHO.CRM.META.getFields({"Entity":module}).then(async function(data){
				moduleFields = data.fields;
				var NumberList = "";
				let lookupModules = [];
				data.fields.forEach(async function(field){
					if(field.data_type == "phone"){
						if(phoneField && phoneField == field.api_name){
							savePhoneFields(field.field_label,field.api_name)
						}
	                    NumberList =NumberList+ `<li class="templateItem" onclick="savePhoneFields('${field.field_label}','${field.api_name}')" api_name=${field.api_name} lookup="false">${field.field_label}</li>`;
	                }

	          if(field.data_type == "lookup") {

	          	lookupModules.push(field);

	          }
					addListItem("dropdown-menu-email",field.field_label,"dropdown-item",module+"."+field.field_label);
				});

				if(lookupModules.length != 0) {
					for (let i = 0; i < lookupModules.length; i++) {
						let lookupModule = lookupModules[i].lookup.module.api_name;
						await getFields(lookupModule).then(async function(respFields) {
						
								respFields.forEach(function(lookupfield) {

									if(lookupfield.data_type == "phone") {
										if(phoneField && phoneField == lookupfield.api_name){
											savePhoneFields(lookupModule+'-'+lookupfield.field_label, lookupModule+'.'+lookupModules[i].api_name+'-'+lookupfield.api_name)
										}
					          
					          NumberList =NumberList+ `<li class="templateItem" onclick="savePhoneFields('${lookupModule+'-'+lookupfield.field_label}','${lookupModule+'.'+lookupModules[i].api_name+'-'+lookupfield.api_name}')" api_name=${lookupModule+'.'+lookupModules[i].api_name+'-'+lookupfield.api_name} lookup="true">${lookupModule+'-'+lookupfield.field_label}</li>`;
					                
									}

							});

						});

						if(i == lookupModules.length-1) {
							$('#phoneFieldList').append(NumberList);
						}

					}
				}
				else {
					$('#phoneFieldList').append(NumberList); 
				}

			});	
		}

async function getFields(entity) {
	return await ZOHO.CRM.META.getFields({"Entity":entity}).then(function(fields){
		return fields.fields;
	});
}

async function getRecord(entity, recordIds) {

	return await ZOHO.CRM.API.getRecord({Entity:entity,RecordID:recordIds}).then(function(data){
		return data;
	});	

}

		function savePhoneFields(label,selectedPhoneField){
	        phoneField = selectedPhoneField;
	        document.getElementById("selectedPhoneField").innerText =  label;
	           
	    }
        function updateOrgVariables(apiname,value,key){
    		document.getElementById("ErrorText").innerText = "Saving...";
    		document.getElementById("Error").style.display= "block";
    		ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": apiname,"value": value}).then(function(res){
    			document.getElementById("ErrorText").innerText = "Saved";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 500);
    		});
        }
        function showsms(editor){
			for(var i=0; i<smsTemplates.length;i++){
				if(smsTemplates[i].id == editor.id){
					templateId = smsTemplates[i].id;
					document.getElementById("selectedTemplate").innerText = smsTemplates[i].Name;
					document.getElementById("tooltiptext").innerText = smsTemplates[i].Name;
					
					document.getElementById("emailContentEmail").innerText = smsTemplates[i].messagebirdforzohocrm__Message;
					break;
				}
			}
		}
		function selectChannel(channel,isSecondAccount){
			currentAccessKey = accessKey;
			if(isSecondAccount){
				currentAccessKey=accessKey2;
			}
			if(channel && channel.id === "short_code"){
				document.getElementById("selectedChannel").innerText = "Use Short Code";
				channelId = channel.id;
				selectedChannel={"id":channel.id};
				$("#approvedTemplates").html("");
				document.getElementById("approvedTemplatesUi").style.display="none";
				$("#plmp").html("");
				$("#shortcode_input").show();

				return;
			}
			else{
				$("#shortcode_input").hide();
			}
			for(var i=0; i<channelsList.length;i++){
				if(channelsList[i].id == channel.id){
					channelId = channelsList[i].id;
					document.getElementById("selectedChannel").innerText = channelsList[i].name;
					selectedChannel = channelsList[i];
					if(channelsList[i].slug && channelsList[i].slug.indexOf("whatsapp") != -1){
						getApprovedTemplates()
					}
					else{
						$("#approvedTemplates").html("");
						document.getElementById("approvedTemplatesUi").style.display="none";
						$("#plmp").html("");
					}
					break;
				}
			}
			for(var i=0; i<secondChannelsList.length;i++){
				if(secondChannelsList[i].id == channel.id){
					channelId = secondChannelsList[i].id;
					document.getElementById("selectedChannel").innerText = secondChannelsList[i].name;
					selectedChannel = secondChannelsList[i];
					if(secondChannelsList[i].slug && secondChannelsList[i].slug.indexOf("whatsapp") != -1){
						getApprovedTemplates()
					}
					else{
						$("#approvedTemplates").html("");
						document.getElementById("approvedTemplatesUi").style.display="none";
						$("#plmp").html("");
					}
					break;
				}
			}
		}
		function valueExists(val) {
		    return val !== null && val !== undefined && val.length > 1 && val!=="null";
		}
        function validate(){
        	if(document.getElementById("emailContentEmail").innerText.length >2000){
    			document.getElementById("ErrorText").innerText = "Message should be within 2000 characters.";
        		document.getElementById("Error").style.display= "block";
        		document.getElementById("Error").style.color="red";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
				return false;
    		}
    		else if(scheduledTime && new Date(date+" "+time).getTime() < new Date().getTime()){
    			document.getElementById("ErrorText").innerText = "Schedule time should be in future.";
    			document.getElementById("Error").style.display= "block";
    			setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
    			return false;
    		}
        	else if(document.getElementById("emailContentEmail").innerText.replace(/\n/g,"").replace(/\t/g,"").replace(/ /g,"") == ""){
        		document.getElementById("ErrorText").innerText = "Message cannot be empty.";
        		document.getElementById("Error").style.display= "block";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
				return false;
        	}
        	else if(func_name == "plivosmsforzohocrm__smshandler" && (MobileNumber == null || MobileNumber == "")){
        		document.getElementById("ErrorText").innerText = "Mobile field is empty.";
		        document.getElementById("Error").style.display= "block";
		        setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
		        return false;
        	}
        	return true;
        }
        function getAllIndexes(arr, val) {
            var indexes = [], i = -1;
            while ((i = arr.indexOf(val, i+1)) != -1){
                indexes.push(i);
            }
            return indexes;
        }
        function getParamValues(plhrs,currentRecord){
            moduleFields.forEach(function(field){
                var indexes = getAllIndexes(plhrs,"${"+recordModule+"."+field.field_label+"}");
                if(indexes.length){
                  var value = currentRecord[field.api_name];
                  if(value && value.name)
                  {
                      value = value.name;
                  }
                  if(!value){
                      value="";
                  }
                }
                indexes.forEach(function(index){
                  if(index != -1){
                    plhrs[index] =value;
                  }
                })
            });
            for(let i=0;i<plhrs.length;i++){
                plhrs[i] ={"default": plhrs[i]};
            }
            return plhrs;
        }
        function getParamFields(){
			var plhrs =[];
			var i =0;
			while(1){
				let plc = document.getElementById(`plhr_${i+1}`);
				if(plc){
					if(plc.value == "custom_value"){
						plhrs.push(document.getElementById(`custominput_${i+1}`).value);
					}
					else{
						plhrs.push("${"+recordModule+"."+plc.value+"}");
					}
					i++;
				}
				else{
					break;
				}
			}
			return plhrs;
        }
        function checkMobileNumber(Mobile,currentRecord){
			if(Mobile){
				return Promise.all([getMobileNumber(Mobile,currentRecord)]).then(function(resp){
					if(resp[0].Mobile){
						Mobile = resp[0].Mobile.replace(/\D/g,'');
						var request ={
					        url : "https://rest.messagebird.com/lookup/" + Mobile,
					        headers:{
					              Authorization:"AccessKey "+currentAccessKey,
					        }
					    }
					    return ZOHO.CRM.HTTP.get(request).then(function(phoneData){
					    	var countryPrefix = null;
					    	try{
					    		countryPrefix = JSON.parse(phoneData).countryPrefix;
					    		if(JSON.parse(phoneData) && JSON.parse(phoneData).phoneNumber){
					    			Mobile = JSON.parse(phoneData).phoneNumber+"";
					    		}
					    	}
					    	catch(e){
					    		console.log(e);
		                    	countryPrefix = uawc_getCountryCode(Mobile);
					    	}
					    	if(!countryPrefix){
					    		if(countryCode && countryCode != "0" && Mobile.length > countryCode.length){
									Mobile = countryCode+""+Mobile;
								}
					    	}
					    	return {"Mobile":Mobile,"toModule":resp[0].toModule,"toId":resp[0].toId,"recipientName":resp[0].recipientName};
					    });	
					}
					else{
						return {};
					}    
				});
			}
			// else{
			// 	Mobile = Mobile.replace(/\D/g,'');
			// 	var request ={
			//         url : "https://rest.messagebird.com/lookup/" + Mobile,
			//         headers:{
			//               Authorization:"AccessKey "+currentAccessKey,
			//         }
			//     }
			//     return ZOHO.CRM.HTTP.get(request).then(function(phoneData){
			//     	var countryPrefix = null;
			//     	try{
			//     		countryPrefix = JSON.parse(phoneData).countryPrefix;
			//   			if(JSON.parse(phoneData) && JSON.parse(phoneData).phoneNumber){
			//     			Mobile = JSON.parse(phoneData).phoneNumber+"";
			//     		}	
			//     	}
			//     	catch(e){
			//     		console.log(e);
   //                  	countryPrefix = uawc_getCountryCode(Mobile);
			//     	}
			//     	if(!countryPrefix){
			//     		if(countryCode && countryCode != "0" && Mobile.length > countryCode.length){
			// 				Mobile = countryCode+""+Mobile;
			// 			}
			//     	}
			//     	return {"Mobile":Mobile};
			//     });	
			// }	
		}
		function uawc_getCountryCode(number) {
            var country_code = [1, 1242, 1246, 1264, 1268, 1284, 1340, 1345, 1441, 1473, 1649, 1664, 1758, 1787, 1809, 1868, 1869, 1876, 2, 20, 212, 213, 216, 218, 220, 221, 222, 223, 224, 226, 227, 228, 229, 231, 232, 233, 234, 236, 237, 238, 239, 240, 241, 242, 244, 245, 248, 249, 250, 251, 252, 253, 254, 256, 257, 258, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 269, 27, 290, 291, 297, 298, 299, 30, 31, 32, 33, 34, 350, 351, 352, 353, 354, 356, 357, 358, 359, 36, 370, 371, 372, 373, 374, 375, 376, 377, 378, 379, 380, 381, 385, 386, 387, 389, 39, 40, 41, 417, 42, 421, 43, 44, 45, 46, 47, 48, 49, 500, 501, 502, 503, 504, 505, 506, 507, 509, 51, 52, 53, 54, 55, 56, 57, 58, 590, 591, 592, 593, 594, 595, 596, 597, 598, 60, 61, 62, 63, 64, 65, 66, 670, 671, 672, 673, 674, 675, 676, 677, 678, 679, 680, 681, 682, 683, 686, 687, 688, 689, 691, 692, 7, 7, 7, 7, 7, 7880, 81, 82, 84, 850, 852, 853, 855, 856, 86, 880, 886, 90, 90392, 91, 94, 95, 960, 961, 962, 963, 964, 965, 966, 967, 968, 969, 971, 972, 973, 974, 975, 976, 977, 98, 993, 994, 996];
            if (number.toString().length > 10)
            {
                num = number.toString();
                var num_len = num.split(num.slice(-10))[0];
                for (let c of country_code)
                {
                    if (c + "" === num_len + "")
                    {
                        return c;
                    }
                }
            }
            return null;
        }
		function getMobileNumber(Mobile,currentRecord){
			if(Mobile.indexOf(".") != -1 && Mobile.indexOf("-") != -1){

				var toModule = Mobile.split('.')[0];
				var toModuleField = Mobile.split('.')[1].split('-')[0];
				var dealphoneField = Mobile.split('.')[1].split('-')[1];
				//var toModuleField = phoneField.substring(0,phoneField.indexOf("."));
				//var dealphoneField = phoneField.substring(phoneField.indexOf(".")+1);
				if(currentRecord[toModuleField] && currentRecord[toModuleField].id){
					var toId = currentRecord[toModuleField].id ;
					
					return ZOHO.CRM.API.getRecord({Entity:toModule,RecordID:toId}).then(function(contactData){
						if(contactData.data[0][dealphoneField] != null && contactData.data[0][dealphoneField] != ""){
							if(toModule == "Contacts" || toModule == "Leads" ){
								var recipientName = contactData.data[0].Full_Name;
							}
							else if(toModule == "Accounts"){
								var recipientName = contactData.data[0].Account_Name;
							}	
							return {"Mobile":contactData.data[0][dealphoneField],"toModule":toModule,"toId":toId,"recipientName":recipientName};
						}
						else{
							return {"Mobile":null};
						}
					});	
				}
				else{
					return {"Mobile":null};
				}
			}	
			else{
				return {"Mobile":currentRecord[Mobile]};
			}
		}


		var isMediaTemplate = false;

    async function sendSMS(){
        	var MobileNumber = document.getElementById("editNumber").value;
        	var date = document.getElementById("datepicker").value;
    		var time = document.getElementById("timeList").value;
    		var currentNamespace = namespace;
    		if(currentAccessKey == accessKey2){
    			currentMsgBirdId = "MessageBird_Id2";
    			currentNamespace = namespace2;
    		}
    		else{
    			currentMsgBirdId = messageBirdId;
    		}
    		if(!phoneField){
    			document.getElementById("ErrorText").innerHTML = `Please choose phone field`;
				document.getElementById("Error").style.display= "block";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
				return;
    		}
    		if(validate())
    		{
        		var notes = "";
        		if(func_name == "plivosmsforzohocrm__bulksms"){
        			MobileNumber = null;
        			notes = "\n("+recordModule +" having empty or invalid mobile number will be ignored.)"
        		}
	        	document.getElementById("ErrorText").innerText = "Sending... "+notes;
				document.getElementById("Error").style.display= "block";
	        	var message = document.getElementById("emailContentEmail").innerText;
                if(selectedChannel && selectedChannel.id == "short_code"){
                	if(!$("#shortcode_input").val()){
                		document.getElementById("ErrorText").innerHTML = `Please enter your short code`;
						document.getElementById("Error").style.display= "block";
						setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
						return;
                	}
                	sendMessageBirdSMS();
                	return;
                } 	
				if(selectedTemplate && selectedTemplate.id){
					if(!currentNamespace){
						document.getElementById("ErrorText").innerHTML = `Please add Your Official WhatsApp Namespace in <span onclick="redirectToSettings()" style="color:blue;">MessageBird Tab</span> to send template message.`;
    					document.getElementById("Error").style.display= "block";
						return;
					}
	                var approvedTemplateId = selectedTemplate.id;
	                var paramFields = getParamFields();
	            }   
	        	var successRecords=0;
	        	var recordsLength = currentRecords.length;
	        	var recordIndex = 0;
	        	var failedRecords=0;
	        	var error="";
	        	var historiesApiArray=[];
	        	var conversationsApiArray=[];
	        	var recordsUpdateApiArray=[];
	        	// currentRecords.forEach(async function(currentRecord){
				for(var i=0;i < currentRecords.length;i++)
				{
					var currentRecord = currentRecords[i];
	        		// if(defaultModules.indexOf(recordModule) != -1){
        				MobileNumber = currentRecord[phoneField];
        			// }
	        		// Promise.all([checkMobileNumber(MobileNumber,currentRecord)]).then(async function(numberresp){
					await checkMobileNumber(phoneField,currentRecord).then(async function(numberresp){
	        			MobileNumber = numberresp.Mobile;
		        		var recordId = currentRecord.id;
		        		var argumentsData = {
							"message" : message,
							"recordModule" : recordModule,
							"scheduledTime":scheduledTime,
							"templateId":templateId,
							"recordId" : recordId,
							"to":MobileNumber
						};
					    var to = argumentsData.to?argumentsData.to.replace(/\D/g,''):"";
						var parambody={};
						var type ="text";
						var text = "";
						if(approvedTemplateId && paramFields){
							let curParams = JSON.parse(JSON.stringify(paramFields));
							var paramValues = JSON.parse(JSON.stringify(getParamValues(curParams,currentRecord)));
							 text = message;
							for(let i=0;i<paramValues.length;i++){
								text = text.replace("{{"+(i+1)+"}}",paramValues[i].default);
							}
							parambody = {
	                            "to":to,
	                            "channelId":selectedChannel.id,
	                            "type":"hsm",
	                            "content":{
	                                    "hsm":{
	                                        "namespace":currentNamespace,
	                                        "templateName":selectedTemplate.name,
	                                        "language":{"policy":"deterministic",
	                                                    "code":selectedTemplate.language
	                                                   },
	                                        "params":paramValues
	                                    }
	                            },
	                            "source":{
				                      "type" : "crm",
				                      "email": currentUser['users'][0]['email'],
				                      "name": currentUser['users'][0]['full_name'],
				                      "zuid": currentUser['users'][0]['zuid']
				                }
	                        }; 
	                        type ="hsm";

							if(selectedTemplate.name)
							{
								compData = null;
								var request ={
										url : "https://integrations.messagebird.com/v2/platforms/whatsapp/templates/"+selectedTemplate.name,
										headers:{
												Authorization:"AccessKey "+currentAccessKey,
										}
								}
								await ZOHO.CRM.HTTP.get(request)
								.then(async function(data){
									console.log(data);
									data = JSON.parse(data);
									let components = data[0].components;
									let istextTemplate = await isTextTemplate(components);
									if(!istextTemplate)
									{
										isMediaTemplate = true;
										if(parambody.content.hsm["params"].length > 0)
										{
											paramValueObj = await changeBodyParamsFormate(parambody.content.hsm["params"]);
										}

										var componentsData = await mediaTemplateData(components);

										delete parambody.content.hsm["params"];
										parambody.content.hsm["components"] = componentsData;
										// url = "https://conversations.messagebird.com/v1/send";
										let from = parambody["channelId"];
										parambody["from"] = from;
										delete parambody["channelId"]; 
									}
								});
							}

						}
						else{
							text = JSON.parse(JSON.stringify(getMessageWithFields(argumentsData,currentRecord)));
							text = text.trim();
						
							if(text.length > 2000)
							{
								document.getElementById("ErrorText").innerText = "Message is Too Large.";
				        		document.getElementById("Error").style.display= "block";
								setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
								return ;
							}
							else if(text.length < 1)
							{
								document.getElementById("ErrorText").innerText = "Merge Fields value is empty.";
				        		document.getElementById("Error").style.display= "block";
								setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
								return;
							}
							parambody ={
								"type" : "text",
								"to": to,
								"channelId": selectedChannel.id,
								"content":{
								"text" : text
								}
				            }
						}	
					    // url = "https://whatsapp-sandbox.messagebird.com/v1/conversations/start";
					    url = "https://conversations.messagebird.com/v1/conversations/start";
						
					    var request ={
					        url : url,
					        body: parambody,
					        headers:{
					              Authorization:"AccessKey "+currentAccessKey,
					        }
					    }
						if(isMediaTemplate)
						{
							request.url =  "https://conversations.messagebird.com/v1/send";
						}
					    var data = await ZOHO.CRM.HTTP.post(request);
					    console.log(data);
				        recordIndex = recordIndex+1;
				        console.log("to");
				        console.log(to);
				        var ContactName = currentRecord.Full_Name;
                        if(!ContactName){
                          ContactName = "message sent to "+to;
                        }
                        else{
                          ContactName = "message sent to "+ContactName
                        }
						data = JSON.parse(data);
						if(isMediaTemplate && data.id)
						{
							// return;
							await ZOHO.CRM.HTTP.get({
								url : "https://conversations.messagebird.com/v1/messages/"+data.id,
								headers:{
										Authorization:"AccessKey "+currentAccessKey,
								}
							}).then(async (res1)=>{
							console.log(JSON.parse(res1));
							let resData1 = JSON.parse(res1);
							await ZOHO.CRM.HTTP.get({
								url : "https://conversations.messagebird.com/v1/conversations/"+resData1.conversationId,
								headers:{
										Authorization:"AccessKey "+currentAccessKey,
								}
							}).then(async (res2)=>{
								console.log(JSON.parse(res2));
								let resData2 = JSON.parse(res2);
								resData2.messages['lastMessageId'] =  data.id;
								data = resData2;
								return;
							});
							return;
							});
						}
				        if(data && data.toString().indexOf("errors") == -1)
				        {
							// data = JSON.parse(data);
							
				            successRecords=successRecords+1;
				            // data = JSON.parse(data);
				            convId = data.id;

				            if(!currentRecord[currentMsgBirdId]){
				            	var updatemap = {"id":recordId};
                                updatemap[currentMsgBirdId]=data.contactId;
					            recordsUpdateApiArray.push(updatemap);
					        }    
					        else if(currentRecord[currentMsgBirdId] != data.contactId){
					        	var conversationmap = {"Name":selectedChannel.slug + " conversation","messagebirdforzohocrm__Conversation_Id":data.id,"messagebirdforzohocrm__From":data.contact.msisdn+"","messagebirdforzohocrm__Platform":selectedChannel.slug,"messagebirdforzohocrm__channelId":selectedChannel.id,"messagebirdforzohocrm__MessageBird_Contact_Id":data.contactId};
                                conversationmap[conversationModuleName]=recordId;
                                if(currentAccessKey == accessKey2){
                                	 conversationmap["MessageBird_Account"]="Second Account";
                                }
                                var oldConvs = await ZOHO.CRM.API.searchRecord({Entity:"messagebirdforzohocrm__MessageBird_Conversations",Type:"criteria",Query:"(messagebirdforzohocrm__Conversation_Id:equals:"+convId+")and("+conversationModuleName+":equals:"+recordId+")",delay:false});
                                if(!oldConvs || !oldConvs.data || !oldConvs.data.length){
                                	conversationsApiArray.push(conversationmap);
                                }
					        }
					        let lastMessageId = "";
                            try{
                                lastMessageId = data.messages.lastMessageId;
                            }
                            catch(err){
                                console.log(err);
                            }
                             
				            var messageHistoryMap = {"Name":ContactName,"messagebirdforzohocrm__Message_Id":lastMessageId,"messagebirdforzohocrm__Message":text,"messagebirdforzohocrm__Conversation_Id":data.id,"messagebirdforzohocrm__MessageBird_Contact_Id":data.contactId,"messagebirdforzohocrm__Channel_Id":selectedChannel.id,"messagebirdforzohocrm__Direction":"Sent","messagebirdforzohocrm__Platform":selectedChannel.slug, "messagebirdforzohocrm__Status":"Sent"};
	                        messageHistoryMap[conversationModuleName]=recordId;
	                        if(selectedTemplate && selectedTemplate.name){
	                        	messageHistoryMap["CampaignName"]=selectedTemplate.name;
	                        }
	                        historiesApiArray.push(messageHistoryMap);
				        } 
				        else {
				        	error = data;
				        	if( JSON.parse(data) &&  JSON.parse(data).errors &&  JSON.parse(data).errors.length){
				        		error = JSON.parse(data).errors[0].description;
				        	}
				        	var messageHistoryMap = {"Name":ContactName,"messagebirdforzohocrm__Message":text,"messagebirdforzohocrm__Direction":"Sent","messagebirdforzohocrm__Platform":selectedChannel.slug, "messagebirdforzohocrm__Status":error};
	                        messageHistoryMap[conversationModuleName]=recordId;
	                        historiesApiArray.push(messageHistoryMap);
				        	failedRecords=failedRecords+1;
				        }
				        if(recordIndex == recordsLength){
				        	if(conversationsApiArray && conversationsApiArray.length){
                                await ZOHO.CRM.API.insertRecord({Entity:"messagebirdforzohocrm__MessageBird_Conversations",APIData:conversationsApiArray});
				        	}
				        	if(recordsUpdateApiArray && recordsUpdateApiArray.length){
					            await ZOHO.CRM.API.updateRecord({Entity:recordModule,APIData:recordsUpdateApiArray,Trigger:["workflow"]});
				        	}
				        	if(historiesApiArray && historiesApiArray.length){
	                        	await ZOHO.CRM.API.insertRecord({Entity:"messagebirdforzohocrm__Message_History",APIData:historiesApiArray,Trigger:["workflow"]});
				        	}
				        	if(successRecords == 0 && !failedRecords){
			                	document.getElementById("ErrorText").innerHTML = "Mobile field is empty or invalid for all choosen " + recordModule+ ".";
			                }
			                else if(failedRecords == recordsLength && error){
			                	document.getElementById("ErrorText").innerHTML = error+"";
			                	setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 4000);
			                }
			                else{
			                	document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your Message has been sent successfully.</div>';
			                	setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload(); }, 1500);
			                }
				        }
				    });    
				}		
				
			}		
        }
        function sendMessageBirdSMS(){
            var message = document.getElementById("emailContentEmail").innerText;
            var successRecords=0;
        	var recordsLength = currentRecords.length;
        	var recordIndex = 0;
        	var failedRecords=0;
        	var error="";
        	var historiesApiArray=[];
            currentRecords.forEach(async function(currentRecord){
    			var MobileNumber = currentRecord[phoneField];
        		Promise.all([checkMobileNumber(MobileNumber,currentRecord)]).then(async function(numberresp){
        			MobileNumber = numberresp[0].Mobile;
	        		var recordId = currentRecord.id;
	        		var argumentsData = {
						"message" : message,
						"recordModule" : recordModule,
						"templateId":templateId,
						"recordId" : recordId,
						"to":MobileNumber
					};
				    var to = MobileNumber?MobileNumber.replace(/\D/g,''):"";
					var parambody={};
					var text = JSON.parse(JSON.stringify(getMessageWithFields(argumentsData,currentRecord)));
					text = text.trim();
					if(text.length > 2000)
					{
						document.getElementById("ErrorText").innerText = "Message is Too Large.";
		        		document.getElementById("Error").style.display= "block";
						setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
						return ;
					}
					else if(text.length < 1)
					{
						document.getElementById("ErrorText").innerText = "Merge Fields value is empty.";
		        		document.getElementById("Error").style.display= "block";
						setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
						return;
					}
					var url = "https://rest.messagebird.com/messages";
					var parambody={
						"recipients":to,
						"originator":$("#shortcode_input").val(),
						"body":text
					}
					var request ={
				        url : url,
				        body: parambody,
				        headers:{
				              Authorization:"AccessKey "+currentAccessKey,
				        }
				    }
				    var data = await ZOHO.CRM.HTTP.post(request);
				    console.log(data);
				    recordIndex = recordIndex+1;
			        var ContactName = currentRecord.Full_Name;
                    if(!ContactName){
                    	ContactName = "message sent to "+to;
                    }
                    else{
                     	ContactName = "message sent to "+ContactName
                    }
                    data = JSON.parse(data);


                    if(data && !data.errors && data.recipients && data.recipients.items){
                    	successRecords=successRecords+1;
                    	var resp = data.recipients.items[0];
                    	var messageHistoryMap = {"Name":ContactName,"messagebirdforzohocrm__Message_Id":data.id+"-"+to,"messagebirdforzohocrm__Message":text,"messagebirdforzohocrm__Channel_Id":$("#shortcode_input").val(),"messagebirdforzohocrm__Direction":"Sent","messagebirdforzohocrm__Platform":"SMS", "messagebirdforzohocrm__Status":resp.status};
	                    messageHistoryMap[conversationModuleName]=recordId;
	                    historiesApiArray.push(messageHistoryMap);
                    }
                    else if(data && data.errors){ 
			        	error = data.errors[0].description;
			        	var messageHistoryMap = {"Name":ContactName,"messagebirdforzohocrm__Message":text,"messagebirdforzohocrm__Channel_Id":$("#shortcode_input").val(),"messagebirdforzohocrm__Direction":"Sent","messagebirdforzohocrm__Platform":"SMS", "messagebirdforzohocrm__Status":error};
                        messageHistoryMap[conversationModuleName]=recordId;
                        historiesApiArray.push(messageHistoryMap);
			        	failedRecords=failedRecords+1;
                    }
                    if(recordIndex == recordsLength){
			        	if(historiesApiArray && historiesApiArray.length){
                        	await ZOHO.CRM.API.insertRecord({Entity:"messagebirdforzohocrm__Message_History",APIData:historiesApiArray,Trigger:["workflow"]});
			        	}
			        	if(successRecords == 0 && !failedRecords){
		                	document.getElementById("ErrorText").innerHTML = "Mobile field is empty or invalid for all choosen " + recordModule+ ".";
		                }
		                else if(failedRecords == recordsLength && error){
		                	document.getElementById("ErrorText").innerHTML = error+"";
		                	setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 4000);
		                }
		                else{
		                	document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your Message has been sent successfully.</div>';
		                	setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload(); }, 1500);
		                }
			        }
				    
			    });
			});    		
        }
        function getMessageWithFields(messageDetails,currentRecord){
        	var message = messageDetails.message;
			var customerData=[];
			var module = messageDetails.recordModule;
			if(messageDetails.recordModule == "Leads")
			{
				customerData = ["Lead Id","Annual Revenue","City","Company","Country","Created By","Created Time","Description","Designation","Email","Email Opt Out","Fax","First Name","Full Name","Industry","Last Activity Time","Last Name","Lead Source","Lead Status","Mobile","Modified By","Modified Time","No of Employees","Owner","Phone","Rating","Record Image","Salutation","Secondary Email","Skype ID","State","Street","Tag","Twitter","Website","Zip Code"];
			}
			else if(messageDetails.recordModule == "Contacts")
			{
				customerData = ["Contact Id","Account Name","Assistant","Asst Phone","Owner","Created By","Created Time","Date of Birth","Department","Description","Email","Email Opt Out","Fax","First Name","Full Name","Home Phone","Last Activity Time","Last Name","Lead Source","Mailing City","Mailing Country","Mailing State","Mailing Street","Mailing Zip","Mobile","Modified By","Modified Time","Other City","Other Country","Other Phone","Other State","Other Street","Other Zip","Phone","Record Image","Reporting To","Salutation","Secondary Email","Skype ID","Title","Twitter","Vendor Name"];
			}
			moduleFields.forEach(function(field){
				var replace = "\\${"+module+"."+field.field_label+"}";
				var re = new RegExp(replace,"g");
				if(currentRecord[field.api_name] != null)
				{
					var value = currentRecord[field.api_name];
					if(value.name)
					{
						value = value.name;
					}
					
					message = message.replace(re,value);
				}
				else
				{
					message = message.toString().replace(re," ");
				}
			});	
			customerData.forEach(function(field){
				if(field == "Contact Id" || field == "Lead Id" || field == "Account Id" || field == "Deal Id")
				{
					var rfield = "id";
				}
				else
				{
					var rfield = field;
				}
				var replace = "\\${"+module+"."+field+"}";
				var re = new RegExp(replace,"g");
				if(currentRecord[rfield.replace(/ /g,"_")] != null)
				{
					var value = currentRecord[rfield.replace(/ /g,"_") + ""];
					if(value.name)
					{
						value = value.name;
					}
					
					message = message.replace(re,value);
				}
				else
				{
					message = message.toString().replace(re," ");
				}
			});
			if(currentRecord.Owner != null)
			{
				var ownerId = currentRecord.Owner.id;
			}
			if(ownerId != null)
			{
				var ownerUser;
				users.forEach(function(user){
					if(user.id == ownerId){
						ownerUser = user;
					}
				})
				if(ownerUser){
					userFields.forEach(function(field){
						var replace = "\\${Users."+field.field_label+"}";
						var re = new RegExp(replace,"g");
						if(ownerUser[field.api_name] != null)
						{
							var value = ownerUser[field.api_name];
							if(value.name)
							{
								value = value.name;
							}
							
							message = message.replace(re,value);
						}
						else
						{
							message = message.toString().replace(re," ");
						}
					});
					return message;
				}
			}
			else{
				return message;
			}
			
        }
		function googleTranslateElementInit() {
		  new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
		}
        function addListItem(id,text,className,value){
			if(className == "dropdown-item"){
				var linode = '<li class="'+className+'"><button class="'+className+'" onclick="insert(this)">'+text+'<input type="hidden" value="'+value+'"></button></li>';
			}
			else{
				var linode = '<li class="'+className+'">'+text+'</li>';
			}
			$('#'+id).append(linode);

        }
		function styling(tag)
		{
			document.execCommand(tag);
		}
		function link(){
			$("#linkForm").slideToggle("slow");
		}
		function image(){
			$("#imageForm").slideToggle("slow");
		}
		function addLink(){
			var href = document.getElementById("linkUrl").value;
		    if (range) {
				if(range.startOffset == range.endOffset){
					if(range.commonAncestorContainer.parentNode.href){
						range.commonAncestorContainer.parentNode.href=href;
					}
					else{
						var span = document.createElement('a');
						span.setAttribute('href',href);
						span.innerText = href;
						range.insertNode(span);
			        	range.setStartAfter(span);
			        }	
				}
				else{
					var data = range.commonAncestorContainer.data;
					var start = range.startOffset;
					var end = range.endOffset;
					range.commonAncestorContainer.data="";
					var span = document.createElement('span');
					span.appendChild( document.createTextNode(data.substring(0,start)) );
					var atag = document.createElement('a');
					atag.setAttribute('href',href);
					atag.innerText = data.substring(start,end);
					span.appendChild(atag);
					span.appendChild( document.createTextNode(data.substring(end)) );
					range.insertNode(span);
		        	range.setStartAfter(span);
				}
		        range.collapse(true);
		    }
			$("#linkForm").slideToggle("slow");
		}
		function addImage(){
			var href = document.getElementById("imageUrl").value;
			var span = document.createElement('img');
			span.setAttribute('src',href);
			span.innerText = href;
			range.insertNode(span);
        	range.setStartAfter(span);
			$("#imageForm").slideToggle("slow");
		}
		function openlink(){
			sel = window.getSelection();
		    if (sel && sel.rangeCount) {
		        range = sel.getRangeAt(0);
		      }  
			if(range && range.commonAncestorContainer.wholeText){
				if(range.commonAncestorContainer.parentNode.href){
					document.getElementById("linkUrl").value = range.commonAncestorContainer.parentNode.href;
					$("#linkForm").slideToggle("slow");
				}
			}	
		}
		function insert(bookingLink){
    		// var bookingLink = this;
			var range;

			if (sel && sel.rangeCount && isDescendant(sel.focusNode)){
		        range = sel.getRangeAt(0);
		        range.collapse(true);
    		    var span = document.createElement("span");
    		    span.appendChild( document.createTextNode('${'+bookingLink.children[0].value+'}') );
        		range.insertNode(span);
	    		range.setStartAfter(span);
		        range.collapse(true);
		        sel.removeAllRanges();
		        sel.addRange(range);
		    }    
		}
		function isDescendant(child) {
			var parent = document.getElementById("emailContentEmail");
		     var node = child.parentNode;
		     while (node != null) {
		         if (node == parent || child == parent) {
		             return true;
		         }
		         node = node.parentNode;
		     }
		     return false;
		}
		function enableSchedule(element){
			if(element.checked == true){
				document.getElementById("send").innerText="Schedule";
				var date = document.getElementById("datepicker").value;
    			var time = document.getElementById("timeList").value;
    			scheduledTime = new Date(date+" "+time).toISOString();
			}
			else{
				document.getElementById("send").innerText="Send";
				scheduledTime = undefined;
			}
		}
		function openDatePicker(){
			document.getElementById("ErrorText").innerHTML ="";
    		document.getElementById("dateTime").style.display= "block";
    		if(ButtonPosition == "DetailView"){
    			document.getElementById("dateTime").style.top= "84%";
    		}
    		else{
    			document.getElementById("dateTime").style.top= "60%";
    		}
    		document.getElementById("Error").style.display= "block";
		}	
		function scheduleClose(){
			var date = document.getElementById("datepicker").value;
    		var time = document.getElementById("timeList").value;
    		if(new Date(date+" "+time).getTime() < new Date().getTime()){
    			document.getElementById("ErrorText").innerText = "Schedule time should be in future.";
    		}
    		else{
    			document.getElementById("ErrorText").innerText = "";
	    		document.getElementById("dateTime").style.display= "none";
	    		document.getElementById("Error").style.display= "none";
	    		document.getElementById("scheduleCheck").checked =true;
	    		document.getElementById("send").innerText="Schedule";
	    		document.getElementById("scheduledDateTime").innerText=new Date(date).toDateString()+" at "+time +" ("+Intl.DateTimeFormat().resolvedOptions().timeZone+")";
	    		scheduledTime = new Date(date+" "+time).toISOString();
	    	}	
		}
		function cancel(){
			document.getElementById("Error").style.display= "none";
		}



		async function isTextTemplate(data)
		{
			for(var i=0;i < data.length;i++)
			{
				let comp = data[i];
				let keys = Object.keys(comp);
				for(var j=0;j < keys.length;j++)
				{
					key = keys[j];
					if(key == "format" && (comp[key].toLowerCase() != "text" && comp[key].toLowerCase() != "none"))
					{
						return false;
					}
					if(key == "type" && comp[key].toLowerCase() == "buttons")
					{
					  for(var k=0;k < comp.buttons.length;k++)
					  {
						  let button = comp.buttons[k];
						  if(button.type == "URL")
						  {
							return false;
						  }
					  }
					  
					}
				}
			}
			return true;
		}
		
		var paramValueObj = null;
		async function mediaTemplateData(components)
		{
			var componentsData = [];
			for(var i=0;i<components.length;i++)
			{
				let comp = components[i];
				let compType = comp.type;
				if(compType.toLowerCase() == "body")
				{
					componentsData[componentsData.length] = {
																"type":"body",
																"parameters": paramValueObj
															};
				}
				if(compType.toLowerCase() == "header" && comp.example)
				{
					componentsData[componentsData.length] = JSON.parse(`{
																"type":"header",
																"parameters": [
																	{
																	  "type": "${comp.format.toLowerCase()}",
																	  "${comp.format.toLowerCase()}": {
																		"url":"${comp.example.header_url}"
																	  }
																	}
																]
															}`);
				}
				if(compType.toLowerCase() == "header" && !comp.example)
				{
					console.log('Skipping since, not enough data');
				}
				if(compType.toLowerCase() == "buttons" && comp.buttons.length > 0)
				{
					for(var j=0;j< comp.buttons.length;j++)
					{
						let button = comp.buttons[j];
						if(button.type == "URL")
						{
							let isbtnVar = false;
							if(button.url.includes("{{") == true && button.url.includes("}}") == true)
							{
								isbtnVar = true;
								componentsData[componentsData.length] = JSON.parse(`{
																"type":"button",
																"sub_type": "url",
																"parameters": [
																	{
																	  "type": "text",
																	  "text": "${"?"}"
																	}
																]
															}`);
							}
						}
					}
				}
				// if(compType.toLowerCase() == "footer")
				// {
				//     componentsData[componentsData.length] = {
				//                                                 "type":"footer",
				//                                                 "parameters": [
				//                                                     {
				//                                                       "type": "text",
				//                                                       "text": comp.text
				//                                                     }
				//                                                 ]
				//                                             }
				// }
			}
			return componentsData;
		}
		
		async function changeBodyParamsFormate(params)
		{
			for(var i=0;i < params.length;i++)
			{
				let val = params[i].default;
				params[i] = {"type":"text","text":val};
			}
		
			return params;
		
		}
		
