var recordId = null;
var recordModule = null;
var xero_tenantId ;
document.addEventListener("DOMContentLoaded", function(event) {

		ZOHO.embeddedApp.on("PageLoad", async function(record) { 
			Loader.open();
						await resizeWidget("75%", "100%");    
						// await getCredential();   		
						recordId = record.EntityId;
		               	recordModule = record.Entity;
		               	// console.log(record); 
		               	await getRecordDetails(recordModule,recordId);   
		               	

		});

		ZOHO.embeddedApp.init().then();

});

async function resizeWidget(height,width)
{
	await ZOHO.CRM.UI.Resize({height:height,width:width}).then(function(data){
		// console.log(data);
	});
}
var customer_email = "";
async function  getRecordDetails(currentModule,recordId)
{
	await ZOHO.CRM.API.getRecord({Entity:currentModule,RecordID:recordId})
	.then(async function(response){
		// console.log(response);
		if(currentModule == "Sales_Orders")
		{
			await ZOHO.CRM.API.getRecord({Entity:"Contacts",RecordID:response.data[0].Contact_Name.id})
			.then(async function(response1){
				customer_email = response1.data[0].Email;
				return ;	
			});
		}
		else
		{
			customer_email = response.data[0].Email;
		}
		await getCredential();  
		// await getCustomers(response.data[0].Email);
	});
}

async function getCredential()
{
	await ZOHO.CRM.API.getOrgVariable("ulgxero__credentials").then(function(data){
        if(data && data.Success && data.Success.Content && data.Success.Content != "{}")
        {
        	var credentials = JSON.parse(data.Success.Content);
        	// if(credentials.tenantId)
        	// {
        	// 	xero_tenantId = credentials.tenantId;
        	// }
        	if(Object.keys(credentials.orgs).length > 0)
        	{
        		var orgs = credentials.orgs;
    //     		if(orgs)
				// {
					// $("#select_invoice_cusomer_opt_count_div").text(`${orgs.length} Contacts found by email`);
					// await orgs.forEach(async(org)=>{\
					for(var i=0;i < Object.keys(orgs).length;i++)
					{
						let tId = Object.keys(orgs)[i];
						let org = orgs[tId];
						$("#select_invoice_org_opt_div").append(`<div class="select_org_options" onclick="getOrg_invoices('${org.tenantId}','${org.tenantName}')">${org.tenantName}</div>`);
					}
					// });
					getOrg_invoices(orgs[Object.keys(orgs)[0]].tenantId,orgs[Object.keys(orgs)[0]].tenantName)
				// }
				// else
				// {
				// 	$("#select_invoice_org_opt_div").append(`<div class="select_org_options">No Organozations</div>`);
				// 	$("#select_org_name_div").text("No Organozations");
				// 	return  true;
				// }
        	}
        	else
			{
				$("#select_invoice_org_opt_div").append(`<div class="select_org_options">No Organozations</div>`);
				$("#select_org_name_div").text("No Organozations");
				return  true;
			}
        }      	
	});	
}

async function getCustomers(email)
{
	Loader.open();
	var dynamic_map =  {
		"tenantId" : xero_tenantId,
		"where_map" : encodeURIComponent('EmailAddress="'+email+'"')
	};
    await ZOHO.CRM.CONNECTOR.invokeAPI("ulgxero.oauthxero.get_xero_contact",dynamic_map)
	.then(async function(response){
		// response = JSON.parse(response);
		var res_data =JSON.parse(response.response).Contacts;
		// console.log(res_data);
		if(res_data)
		{
			$("#select_invoice_cusomer_opt_count_div").text(`${res_data.length} Contacts found by email`);
			await res_data.forEach(async(customer)=>{
				$("#select_invoice_cusomer_opt_div").append(`<div class="select_customer_options" onclick="getCustomer_invoices('${customer.ContactID}','${customer.Name}')">${customer.Name}</div>`);
			});

			// await getCustomer_invoices(datas[0].id,datas[0].name);
		}
		else
		{
			$("#select_invoice_cusomer_opt_div").append(`<div class="select_customer_options">No Contacts</div>`);
			$("#select_customer_name_div").text("No Contacts");
			return  true;
		}
		setTimeout(function(){Loader.close();},1500);
	    
	});
}

async function getOrg_invoices(tenantId,tenantName)
{
	if(!tenantId && !tenantName)
	{
		return true;
	}
	$("#select_invoice_org_opt_div").hide();
	$("#select_xero_org_name_div").text(tenantName);
	$("#select_xero_customer_name_div").text("Select customer");
	$("#select_invoice_cusomer_opt_div").empty();
	$("#xero_invoices_contains_div").empty();
	xero_tenantId = tenantId;
	await getCustomers(customer_email);
}

async function getCustomer_invoices(customer_id,customer_name)
{

	Loader.open();
	// $("#xero_invoice_full_div").attr("style","left:5%;top:10px;");
	$("#xero_customer_invoice_title_txt").attr("style","display:block;");
	$("#select_invoice_cusomer_opt_div").hide();
	$("#select_customer_dropdown_icon").attr("style","transform: rotate(0deg);");
	if(!customer_id && !customer_name)
	{
		return true;
	}
	$("#select_xero_customer_name_div").text(customer_name);
	$("#select_xero_customer_update_div").attr("onclick",`updateCustomer_popupDiv('${customer_id}')`);
	$("#xero_invoices_contains_div").empty();

	var dynamic_map =  {
		"tenantId" : xero_tenantId,
		"invoiceId" : "ContactIDs="+customer_id
	};
    await ZOHO.CRM.CONNECTOR.invokeAPI("ulgxero.oauthxero.get_xero_invoice_by_id",dynamic_map)
	.then(async function(data){
	    var res_data =JSON.parse(data.response).Invoices;
	    // console.log(res_data);
	    if(res_data.length > 0)
	    {
		    var invoices = res_data;
		    await invoices.forEach(async(invoice)=>{
		    	await show_invoice(invoice);
		    });
		}
		if(res_data.length == 0)
		{
			// console.log(data.length);
			await $("#xero_invoices_contains_div").append(`<div id="no_invoices">No Invoice</div>`);
		}
	    setTimeout(function(){Loader.close();},1500);
	});
}

async function show_invoice(invoice)
{
	await $("#xero_invoices_contains_div").append(`<div class="customer_invoice_item_div" id="customer_invoice_item_div_${invoice.InvoiceID}">
												  <div class="customer_invoice_status_info_div">
												  	<div class="customer_invoice_status_txt_div">Status</div>
												  	<div class="customer_invoice_status_val_div" id="customer_invoice_status_val_div_${invoice.InvoiceID}" style="color: ${await status_color(invoice.Status)}">${invoice.Status}</div>
												  </div>
												  <div class="customer_invoice_details_div">
												  	<div class="customer_inv&rec_num_div">
												  	   <div class="customer_invoice_num" id="customer_invoice_num_${invoice.InvoiceID}">Invoice No : ${invoice.InvoiceNumber? invoice.InvoiceNumber : "No Number"}</div>
												  	   <div class="customer_invoice_date_details_div" id="customer_invoice_date_details_div_${invoice.InvoiceID}">${"Date of issue : "+await getInvoiceStatusDate(invoice.Date)}</div>
												  	</div>
												  	<div class="customer_invoice_otherDetails_div">
												  		<div class="customer_invoice_period">Period : ${await getInvoiceStatusDate(invoice.Date)+" - "+await getInvoiceStatusDate(invoice.DueDate)}</div>
												  		<div class="invoice_total_amount_details_div" id="invoice_total_amount_details_div_${invoice.InvoiceID}">${invoice.AmountPaid > 0 ? "Amount Paid " : "Amount Due "} : ${await changeAmount(invoice.CurrencyCode,invoice.Total.toString())}</div>
												  		<div class="customer_invoice_pdf" id="customer_invoice_pdf_${invoice.InvoiceID}" style="display:none;"><div class="customer_invoice_pdf_btn_div" onclick="download_invoice_pdf('${invoice.InvoiceID}','${invoice.InvoiceNumber}')">Invoice pdf <span class="material-icons invoice_pdf_icon">launch</span></div></div>
												  	</div>
												  	<div class="customer_invoice_list_items" id="customer_invoice_list_items_${invoice.InvoiceID}" style="display:none">
												  		<div class="customer_invoice_list_item_title_div">
												  			<div class="ci_description_div">Description</div>
												  			<div class="ci_qty_div">Qty</div>
												  			<div class="ci_price_div">Unit price</div>
												  			<div class="ci_discount_div">Discount</div>
												  			<div class="ci_amount_div">Amount</div>												  		
												  		</div>
												  	</div>
												  	<div class="customer_invoice_calc_div" id="customer_invoice_calc_div_${invoice.InvoiceID}" style="display:none">
												  		<div class="customer_invoice_subtotal_div">
												  			<div class="customer_invoice_subtotal_txt_div">Subtotal</div>
												  			<div class="customer_invoice_subtotal_val_div">${await changeAmount(invoice.CurrencyCode,invoice.SubTotal.toString())}</div>
												  		</div>
												  		<div class="customer_invoice_tax_div" style="visible:${invoice.TotalTax ? "true":"false"}">
												  			<div class="customer_invoice_tax_txt_div">Tax </div>
												  			<div class="customer_invoice_tax_val_div">${invoice.TotalTax>0 ? ( await changeAmount(invoice.CurrencyCode,invoice.TotalTax.toString())) : ( await changeAmount(invoice.CurrencyCode,"000"))}</div>
												  		</div>
												  		<div class="customer_invoice_total_div">
												  			<div class="customer_invoice_amount_txt_div">${invoice.AmountPaid > 0 ? "Amount Paid" : "Amount Due"}</div>
												  			<div class="customer_invoice_amount_val_div">${await changeAmount(invoice.CurrencyCode,invoice.Total.toString())}</div>
												  		</div>
												  	</div>
												  	<div class="expand_invoice_item" id="expand_invoice_item_${invoice.InvoiceID}"><span class="material-icons expand_icon" id="expand_icon_${invoice.InvoiceID}" onclick="expand_invoice_item('${invoice.InvoiceID}')">expand_more</span></div>
												  </div>
											   </div>`);

	var items = invoice.LineItems;
	for(var item_index in items)
	{
		var item = items[item_index];
		await $(`#customer_invoice_list_items_${invoice.InvoiceID}`).append(`<div class="customer_invoice_list_item">
																		<div class="ci_li_description_div">${item.Description}</div>
															  			<div class="ci_li_qty_div">${item.Quantity}</div>
															  			<div class="ci_li_price_div">${await changeAmount(invoice.CurrencyCode,item.UnitAmount.toString())}</div>
															  			<div class="ci_li_discount_div">${item.DiscountRate > 0 ? (item.DiscountRate+" %") : ("0 %")}</div>
																	  	<div class="ci_li_amount_div">${await changeAmount(invoice.CurrencyCode,item.LineAmount.toString())}</div>
																	  </div>`);
	}
			


}

async function updateCustomer_popupDiv(customer_id)
{
	// console.log(customer_id);
	// $("body").append(`<div>
	// 					<div></div>
	// 				  </div>`);

}

async function download_invoice_pdf(invoice_id,invoice_no)
{
	// return;
	Loader.open();
	var  xero_invoice_id = invoice_id;
	if(xero_invoice_id)
	{
		let url = "https://go.xero.com/AccountsReceivable/View.aspx?InvoiceID="+invoice_id;
		// console.log(url);
		window.open(url,"_blank");
		Loader.close();
		return;
	}
	var getmap = {"nameSpace":"<portal_name.extension_namespace>"};
            var resp = await ZOHO.CRM.CONNECTOR.invokeAPI("crm.zapikey",getmap);
            var zapikey = JSON.parse(resp).response;
    var req_map = {"zapikey":zapikey,};
    var response = await ZOHO.CRM.CONNECTOR.invokeAPI("ulgxero.oauthxero.get_xero_access_token",req_map);
    // console.log(response);
    if(response.response == "" || response.response == null)
    {
    	// console.log(response);
    	Loader.close();
    	return ;
    }
    var hidden_token = JSON.parse(response.response).accessToken;

    var headers = {
    	"Xero-Tenant-Id":xero_tenantId,
    	"Accept":"application/pdf",
    	"Authorization":"Bearer " + hidden_token
    };

    var request ={
	     url : "https://api.xero.com/api.xro/2.0/Invoices/" + xero_invoice_id,
	     headers:headers
	}
	await ZOHO.CRM.HTTP.get(request)
	.then(async function(data){
		await ZOHO.CRM.API.attachFile({Entity:"Leads",RecordID:recordId,File:data,FileName:invoice_no+".txt"}).then(function(data1){
			// console.log(data1);
		});
	    // console.log(data);
	});
	Loader.close();
}

async function status_color(status)
{
	if(status == "paid")
	{
		return "#29d37a";
	}
	else if(status == "open")
	{
		return "#29ced3";
	}  
	else if(status == "draft")
	{
	    return "#7087a8";
	}
	else if(status == "void")
	{
		return "#c15e68";
	}
	else if(status == "uncollectible")
	{
		return "#c5b7cd";
	}
	else
	{
		return "#7087a8";
	}
}

async function getInvoiceStatusDate(date)
{
	var issue_date = date;
	issue_date = issue_date.split("/Date(")[1].split("+")[0];
	return " "+await getInvoiceDate(issue_date);
}


async function getConformation(confirm_name,callback)
{
	$("body").append(`<div id="confirm_box_full_div">
						<div id="confirm_box_div">
							<div id="confirm_box_text_div">Do you want ${confirm_name} a invoice?</div>
							<div id="confirm_box_inp_div">
								<div id="confirm_yes_btn">Yes</div>
								<div id="confirm_no_btn">No</div>
							</div>
						</div>
					  </div>`);

	$("#confirm_yes_btn").on('click',function(){
		$("#confirm_box_full_div").remove();
		callback(true);
		return;
	});
	$("#confirm_no_btn").on('click',function(){
		$("#confirm_box_full_div").remove();
		callback(false);
		return;
	});
	return;
}

async function expand_invoice_item(invoice_id)
{
	if($(`#customer_invoice_list_items_${invoice_id}`).is(":visible") == true)
	{
		$(`#customer_invoice_update_div_${invoice_id}`).delay(400).fadeOut();
		$(`#customer_invoice_list_items_${invoice_id}`).delay(400).fadeOut();
		$(`#customer_invoice_calc_div_${invoice_id}`).delay(400).fadeOut();
		$(`#customer_invoice_pdf_${invoice_id}`).delay(400).fadeOut();
		$(`#invoice_total_amount_details_div_${invoice_id}`).delay(500).fadeIn();
		$(`#expand_icon_${invoice_id}`).text("expand_more");
		$(`#expand_invoice_item_${invoice_id}`).attr("style","bottom: 0%;");
		$(`#customer_invoice_item_div_${invoice_id}`).attr("style","padding: 2px 2px 20px 2px;");
	}
	else
	{
		$(`#customer_invoice_update_div_${invoice_id}`).fadeIn();
		$(`#customer_invoice_list_items_${invoice_id}`).fadeIn();
		$(`#customer_invoice_calc_div_${invoice_id}`).fadeIn();
		// if(recordModule == "Leads")
		// {
			$(`#customer_invoice_pdf_${invoice_id}`).fadeIn();
		// }
		$(`#invoice_total_amount_details_div_${invoice_id}`).fadeOut();
		$(`#expand_icon_${invoice_id}`).text("expand_less");
		$(`#expand_invoice_item_${invoice_id}`).attr("style","bottom: -10%;");
		$(`#customer_invoice_item_div_${invoice_id}`).attr("style","padding: 10px 20px 40px 20px;");
	}

}

async function select_customer_opt()
{
	if($("#select_invoice_cusomer_opt_div").is(":visible") == true)
	{
		$("#select_invoice_cusomer_opt_div").hide();
		$("#select_customer_dropdown_icon").attr("style","transform: rotate(0deg);");

	}
	else
	{
		$("#select_invoice_cusomer_opt_div").show();
		$("#select_customer_dropdown_icon").attr("style","transform: rotate(180deg);");
	}
}

async function select_org_opt()
{
	if($("#select_invoice_org_opt_div").is(":visible") == true)
	{
		$("#select_invoice_org_opt_div").hide();
		$("#select_org_dropdown_icon").attr("style","transform: rotate(0deg);");

	}
	else
	{
		$("#select_invoice_org_opt_div").show();
		$("#select_org_dropdown_icon").attr("style","transform: rotate(180deg);");
	}
}

async function getInvoiceDate(time)
{
	time = Number(time);
	var inv_date = new Date(time);
	let date = inv_date.getDate();
	let month = inv_date.getMonth();
	let year = inv_date.getFullYear();

	switch(month)
	{
		case 0 : { month = "Jan"; break;}
		case 1 : { month = "Feb"; break;}
		case 2 : { month = "Mar"; break;}
		case 3 : { month = "Apr"; break;}
		case 4 : { month = "May"; break;}
		case 5 : { month = "Jun"; break;}
		case 6 : { month = "Jul"; break;}
		case 7 : { month = "Aug"; break;}
		case 8 : { month = "Sep"; break;}
		case 9 : { month = "Oct"; break;}
		case 10 : { month = "Nov"; break;}
		case 11 : { month = "Dec"; break;}
	}
	return ""+month+" "+date+" ,"+year;
}

async function changeAmount(currency,amount)
{
	amount = parseFloat(amount);
	var amt = amount.toFixed(2);
	var amount_str = country_symbols[currency.toUpperCase()].symbol+" "+amt;
	return amount_str;
}


/* Loader */
var Loader = {
        loader: null,
        body: null,
        html: '<span><svg width="40" height="40" version="1.1" xmlns="http://www.w3.org/2000/svg"><circle cx="20" cy="20" r="15"></svg></span>',
        cssClass: 'loader',
        check: function () {
                if (this.body == null) {
                        this.body = document.getElementsByTagName('body')[0];
                }
        },
        open: function () {
                this.check();
                if (!this.isOpen()) {
                        this.loader = document.createElement('div');
                        this.loader.setAttribute('id', 'loader');
                        this.loader.classList.add('loader_website');
                        this.loader.innerHTML = this.html;
                        this.body.append(this.loader);
                        this.body.classList.add(this.cssClass);
                }
                return this;
        },
        close: function () {
                this.check();
                if (this.isOpen()) {
                        this.body.classList.remove(this.cssClass);
                        this.loader.remove();
                }
                return this;
        },
        isOpen: function () {
                this.check();
                return this.body.classList.contains(this.cssClass);
        },
        ifOpened: function (callback, close) {
                this.check();
                if (this.isOpen()) {
                        if (!!close)
                                this.close();
                        if (typeof callback === 'function') {
                                callback();
                        }
                }
                return this;
        },
        ifClosed: function (callback, open) {
                this.check();
                if (!this.isOpen()) {
                        if (!!open)
                                this.open();
                        if (typeof callback === 'function') {
                                callback();
                        }
                }
                return this;
        }
};
    /* Loader */

var country_symbols = {
	"USD": {
		"symbol": "$",
		"name": "US Dollar",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "USD",
		"name_plural": "US dollars"
	},
	"CAD": {
		"symbol": "CA$",
		"name": "Canadian Dollar",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "CAD",
		"name_plural": "Canadian dollars"
	},
	"EUR": {
		"symbol": "€",
		"name": "Euro",
		"symbol_native": "€",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "EUR",
		"name_plural": "euros"
	},
	"AED": {
		"symbol": "AED",
		"name": "United Arab Emirates Dirham",
		"symbol_native": "د.إ.‏",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "AED",
		"name_plural": "UAE dirhams"
	},
	"AFN": {
		"symbol": "Af",
		"name": "Afghan Afghani",
		"symbol_native": "؋",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "AFN",
		"name_plural": "Afghan Afghanis"
	},
	"ALL": {
		"symbol": "ALL",
		"name": "Albanian Lek",
		"symbol_native": "Lek",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "ALL",
		"name_plural": "Albanian lekë"
	},
	"AMD": {
		"symbol": "AMD",
		"name": "Armenian Dram",
		"symbol_native": "դր.",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "AMD",
		"name_plural": "Armenian drams"
	},
	"ARS": {
		"symbol": "AR$",
		"name": "Argentine Peso",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "ARS",
		"name_plural": "Argentine pesos"
	},
	"AUD": {
		"symbol": "AU$",
		"name": "Australian Dollar",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "AUD",
		"name_plural": "Australian dollars"
	},
	"AZN": {
		"symbol": "man.",
		"name": "Azerbaijani Manat",
		"symbol_native": "ман.",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "AZN",
		"name_plural": "Azerbaijani manats"
	},
	"BAM": {
		"symbol": "KM",
		"name": "Bosnia-Herzegovina Convertible Mark",
		"symbol_native": "KM",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "BAM",
		"name_plural": "Bosnia-Herzegovina convertible marks"
	},
	"BDT": {
		"symbol": "Tk",
		"name": "Bangladeshi Taka",
		"symbol_native": "৳",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "BDT",
		"name_plural": "Bangladeshi takas"
	},
	"BGN": {
		"symbol": "BGN",
		"name": "Bulgarian Lev",
		"symbol_native": "лв.",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "BGN",
		"name_plural": "Bulgarian leva"
	},
	"BHD": {
		"symbol": "BD",
		"name": "Bahraini Dinar",
		"symbol_native": "د.ب.‏",
		"decimal_digits": 3,
		"rounding": 0,
		"code": "BHD",
		"name_plural": "Bahraini dinars"
	},
	"BIF": {
		"symbol": "FBu",
		"name": "Burundian Franc",
		"symbol_native": "FBu",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "BIF",
		"name_plural": "Burundian francs"
	},
	"BND": {
		"symbol": "BN$",
		"name": "Brunei Dollar",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "BND",
		"name_plural": "Brunei dollars"
	},
	"BOB": {
		"symbol": "Bs",
		"name": "Bolivian Boliviano",
		"symbol_native": "Bs",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "BOB",
		"name_plural": "Bolivian bolivianos"
	},
	"BRL": {
		"symbol": "R$",
		"name": "Brazilian Real",
		"symbol_native": "R$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "BRL",
		"name_plural": "Brazilian reals"
	},
	"BWP": {
		"symbol": "BWP",
		"name": "Botswanan Pula",
		"symbol_native": "P",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "BWP",
		"name_plural": "Botswanan pulas"
	},
	"BYN": {
		"symbol": "Br",
		"name": "Belarusian Ruble",
		"symbol_native": "руб.",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "BYN",
		"name_plural": "Belarusian rubles"
	},
	"BZD": {
		"symbol": "BZ$",
		"name": "Belize Dollar",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "BZD",
		"name_plural": "Belize dollars"
	},
	"CDF": {
		"symbol": "CDF",
		"name": "Congolese Franc",
		"symbol_native": "FrCD",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "CDF",
		"name_plural": "Congolese francs"
	},
	"CHF": {
		"symbol": "CHF",
		"name": "Swiss Franc",
		"symbol_native": "CHF",
		"decimal_digits": 2,
		"rounding": 0.05,
		"code": "CHF",
		"name_plural": "Swiss francs"
	},
	"CLP": {
		"symbol": "CL$",
		"name": "Chilean Peso",
		"symbol_native": "$",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "CLP",
		"name_plural": "Chilean pesos"
	},
	"CNY": {
		"symbol": "CN¥",
		"name": "Chinese Yuan",
		"symbol_native": "CN¥",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "CNY",
		"name_plural": "Chinese yuan"
	},
	"COP": {
		"symbol": "CO$",
		"name": "Colombian Peso",
		"symbol_native": "$",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "COP",
		"name_plural": "Colombian pesos"
	},
	"CRC": {
		"symbol": "₡",
		"name": "Costa Rican Colón",
		"symbol_native": "₡",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "CRC",
		"name_plural": "Costa Rican colóns"
	},
	"CVE": {
		"symbol": "CV$",
		"name": "Cape Verdean Escudo",
		"symbol_native": "CV$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "CVE",
		"name_plural": "Cape Verdean escudos"
	},
	"CZK": {
		"symbol": "Kč",
		"name": "Czech Republic Koruna",
		"symbol_native": "Kč",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "CZK",
		"name_plural": "Czech Republic korunas"
	},
	"DJF": {
		"symbol": "Fdj",
		"name": "Djiboutian Franc",
		"symbol_native": "Fdj",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "DJF",
		"name_plural": "Djiboutian francs"
	},
	"DKK": {
		"symbol": "Dkr",
		"name": "Danish Krone",
		"symbol_native": "kr",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "DKK",
		"name_plural": "Danish kroner"
	},
	"DOP": {
		"symbol": "RD$",
		"name": "Dominican Peso",
		"symbol_native": "RD$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "DOP",
		"name_plural": "Dominican pesos"
	},
	"DZD": {
		"symbol": "DA",
		"name": "Algerian Dinar",
		"symbol_native": "د.ج.‏",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "DZD",
		"name_plural": "Algerian dinars"
	},
	"EEK": {
		"symbol": "Ekr",
		"name": "Estonian Kroon",
		"symbol_native": "kr",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "EEK",
		"name_plural": "Estonian kroons"
	},
	"EGP": {
		"symbol": "EGP",
		"name": "Egyptian Pound",
		"symbol_native": "ج.م.‏",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "EGP",
		"name_plural": "Egyptian pounds"
	},
	"ERN": {
		"symbol": "Nfk",
		"name": "Eritrean Nakfa",
		"symbol_native": "Nfk",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "ERN",
		"name_plural": "Eritrean nakfas"
	},
	"ETB": {
		"symbol": "Br",
		"name": "Ethiopian Birr",
		"symbol_native": "Br",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "ETB",
		"name_plural": "Ethiopian birrs"
	},
	"GBP": {
		"symbol": "£",
		"name": "British Pound Sterling",
		"symbol_native": "£",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "GBP",
		"name_plural": "British pounds sterling"
	},
	"GEL": {
		"symbol": "GEL",
		"name": "Georgian Lari",
		"symbol_native": "GEL",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "GEL",
		"name_plural": "Georgian laris"
	},
	"GHS": {
		"symbol": "GH₵",
		"name": "Ghanaian Cedi",
		"symbol_native": "GH₵",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "GHS",
		"name_plural": "Ghanaian cedis"
	},
	"GNF": {
		"symbol": "FG",
		"name": "Guinean Franc",
		"symbol_native": "FG",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "GNF",
		"name_plural": "Guinean francs"
	},
	"GTQ": {
		"symbol": "GTQ",
		"name": "Guatemalan Quetzal",
		"symbol_native": "Q",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "GTQ",
		"name_plural": "Guatemalan quetzals"
	},
	"HKD": {
		"symbol": "HK$",
		"name": "Hong Kong Dollar",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "HKD",
		"name_plural": "Hong Kong dollars"
	},
	"HNL": {
		"symbol": "HNL",
		"name": "Honduran Lempira",
		"symbol_native": "L",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "HNL",
		"name_plural": "Honduran lempiras"
	},
	"HRK": {
		"symbol": "kn",
		"name": "Croatian Kuna",
		"symbol_native": "kn",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "HRK",
		"name_plural": "Croatian kunas"
	},
	"HUF": {
		"symbol": "Ft",
		"name": "Hungarian Forint",
		"symbol_native": "Ft",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "HUF",
		"name_plural": "Hungarian forints"
	},
	"IDR": {
		"symbol": "Rp",
		"name": "Indonesian Rupiah",
		"symbol_native": "Rp",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "IDR",
		"name_plural": "Indonesian rupiahs"
	},
	"ILS": {
		"symbol": "₪",
		"name": "Israeli New Sheqel",
		"symbol_native": "₪",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "ILS",
		"name_plural": "Israeli new sheqels"
	},
	"INR": {
		"symbol": "Rs",
		"name": "Indian Rupee",
		"symbol_native": "টকা",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "INR",
		"name_plural": "Indian rupees"
	},
	"IQD": {
		"symbol": "IQD",
		"name": "Iraqi Dinar",
		"symbol_native": "د.ع.‏",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "IQD",
		"name_plural": "Iraqi dinars"
	},
	"IRR": {
		"symbol": "IRR",
		"name": "Iranian Rial",
		"symbol_native": "﷼",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "IRR",
		"name_plural": "Iranian rials"
	},
	"ISK": {
		"symbol": "Ikr",
		"name": "Icelandic Króna",
		"symbol_native": "kr",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "ISK",
		"name_plural": "Icelandic krónur"
	},
	"JMD": {
		"symbol": "J$",
		"name": "Jamaican Dollar",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "JMD",
		"name_plural": "Jamaican dollars"
	},
	"JOD": {
		"symbol": "JD",
		"name": "Jordanian Dinar",
		"symbol_native": "د.أ.‏",
		"decimal_digits": 3,
		"rounding": 0,
		"code": "JOD",
		"name_plural": "Jordanian dinars"
	},
	"JPY": {
		"symbol": "¥",
		"name": "Japanese Yen",
		"symbol_native": "￥",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "JPY",
		"name_plural": "Japanese yen"
	},
	"KES": {
		"symbol": "Ksh",
		"name": "Kenyan Shilling",
		"symbol_native": "Ksh",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "KES",
		"name_plural": "Kenyan shillings"
	},
	"KHR": {
		"symbol": "KHR",
		"name": "Cambodian Riel",
		"symbol_native": "៛",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "KHR",
		"name_plural": "Cambodian riels"
	},
	"KMF": {
		"symbol": "CF",
		"name": "Comorian Franc",
		"symbol_native": "FC",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "KMF",
		"name_plural": "Comorian francs"
	},
	"KRW": {
		"symbol": "₩",
		"name": "South Korean Won",
		"symbol_native": "₩",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "KRW",
		"name_plural": "South Korean won"
	},
	"KWD": {
		"symbol": "KD",
		"name": "Kuwaiti Dinar",
		"symbol_native": "د.ك.‏",
		"decimal_digits": 3,
		"rounding": 0,
		"code": "KWD",
		"name_plural": "Kuwaiti dinars"
	},
	"KZT": {
		"symbol": "KZT",
		"name": "Kazakhstani Tenge",
		"symbol_native": "тңг.",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "KZT",
		"name_plural": "Kazakhstani tenges"
	},
	"LBP": {
		"symbol": "L.L.",
		"name": "Lebanese Pound",
		"symbol_native": "ل.ل.‏",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "LBP",
		"name_plural": "Lebanese pounds"
	},
	"LKR": {
		"symbol": "SLRs",
		"name": "Sri Lankan Rupee",
		"symbol_native": "SL Re",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "LKR",
		"name_plural": "Sri Lankan rupees"
	},
	"LTL": {
		"symbol": "Lt",
		"name": "Lithuanian Litas",
		"symbol_native": "Lt",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "LTL",
		"name_plural": "Lithuanian litai"
	},
	"LVL": {
		"symbol": "Ls",
		"name": "Latvian Lats",
		"symbol_native": "Ls",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "LVL",
		"name_plural": "Latvian lati"
	},
	"LYD": {
		"symbol": "LD",
		"name": "Libyan Dinar",
		"symbol_native": "د.ل.‏",
		"decimal_digits": 3,
		"rounding": 0,
		"code": "LYD",
		"name_plural": "Libyan dinars"
	},
	"MAD": {
		"symbol": "MAD",
		"name": "Moroccan Dirham",
		"symbol_native": "د.م.‏",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "MAD",
		"name_plural": "Moroccan dirhams"
	},
	"MDL": {
		"symbol": "MDL",
		"name": "Moldovan Leu",
		"symbol_native": "MDL",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "MDL",
		"name_plural": "Moldovan lei"
	},
	"MGA": {
		"symbol": "MGA",
		"name": "Malagasy Ariary",
		"symbol_native": "MGA",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "MGA",
		"name_plural": "Malagasy Ariaries"
	},
	"MKD": {
		"symbol": "MKD",
		"name": "Macedonian Denar",
		"symbol_native": "MKD",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "MKD",
		"name_plural": "Macedonian denari"
	},
	"MMK": {
		"symbol": "MMK",
		"name": "Myanma Kyat",
		"symbol_native": "K",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "MMK",
		"name_plural": "Myanma kyats"
	},
	"MOP": {
		"symbol": "MOP$",
		"name": "Macanese Pataca",
		"symbol_native": "MOP$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "MOP",
		"name_plural": "Macanese patacas"
	},
	"MUR": {
		"symbol": "MURs",
		"name": "Mauritian Rupee",
		"symbol_native": "MURs",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "MUR",
		"name_plural": "Mauritian rupees"
	},
	"MXN": {
		"symbol": "MX$",
		"name": "Mexican Peso",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "MXN",
		"name_plural": "Mexican pesos"
	},
	"MYR": {
		"symbol": "RM",
		"name": "Malaysian Ringgit",
		"symbol_native": "RM",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "MYR",
		"name_plural": "Malaysian ringgits"
	},
	"MZN": {
		"symbol": "MTn",
		"name": "Mozambican Metical",
		"symbol_native": "MTn",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "MZN",
		"name_plural": "Mozambican meticals"
	},
	"NAD": {
		"symbol": "N$",
		"name": "Namibian Dollar",
		"symbol_native": "N$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "NAD",
		"name_plural": "Namibian dollars"
	},
	"NGN": {
		"symbol": "₦",
		"name": "Nigerian Naira",
		"symbol_native": "₦",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "NGN",
		"name_plural": "Nigerian nairas"
	},
	"NIO": {
		"symbol": "C$",
		"name": "Nicaraguan Córdoba",
		"symbol_native": "C$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "NIO",
		"name_plural": "Nicaraguan córdobas"
	},
	"NOK": {
		"symbol": "Nkr",
		"name": "Norwegian Krone",
		"symbol_native": "kr",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "NOK",
		"name_plural": "Norwegian kroner"
	},
	"NPR": {
		"symbol": "NPRs",
		"name": "Nepalese Rupee",
		"symbol_native": "नेरू",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "NPR",
		"name_plural": "Nepalese rupees"
	},
	"NZD": {
		"symbol": "NZ$",
		"name": "New Zealand Dollar",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "NZD",
		"name_plural": "New Zealand dollars"
	},
	"OMR": {
		"symbol": "OMR",
		"name": "Omani Rial",
		"symbol_native": "ر.ع.‏",
		"decimal_digits": 3,
		"rounding": 0,
		"code": "OMR",
		"name_plural": "Omani rials"
	},
	"PAB": {
		"symbol": "B/.",
		"name": "Panamanian Balboa",
		"symbol_native": "B/.",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "PAB",
		"name_plural": "Panamanian balboas"
	},
	"PEN": {
		"symbol": "S/.",
		"name": "Peruvian Nuevo Sol",
		"symbol_native": "S/.",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "PEN",
		"name_plural": "Peruvian nuevos soles"
	},
	"PHP": {
		"symbol": "₱",
		"name": "Philippine Peso",
		"symbol_native": "₱",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "PHP",
		"name_plural": "Philippine pesos"
	},
	"PKR": {
		"symbol": "PKRs",
		"name": "Pakistani Rupee",
		"symbol_native": "₨",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "PKR",
		"name_plural": "Pakistani rupees"
	},
	"PLN": {
		"symbol": "zł",
		"name": "Polish Zloty",
		"symbol_native": "zł",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "PLN",
		"name_plural": "Polish zlotys"
	},
	"PYG": {
		"symbol": "₲",
		"name": "Paraguayan Guarani",
		"symbol_native": "₲",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "PYG",
		"name_plural": "Paraguayan guaranis"
	},
	"QAR": {
		"symbol": "QR",
		"name": "Qatari Rial",
		"symbol_native": "ر.ق.‏",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "QAR",
		"name_plural": "Qatari rials"
	},
	"RON": {
		"symbol": "RON",
		"name": "Romanian Leu",
		"symbol_native": "RON",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "RON",
		"name_plural": "Romanian lei"
	},
	"RSD": {
		"symbol": "din.",
		"name": "Serbian Dinar",
		"symbol_native": "дин.",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "RSD",
		"name_plural": "Serbian dinars"
	},
	"RUB": {
		"symbol": "RUB",
		"name": "Russian Ruble",
		"symbol_native": "₽.",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "RUB",
		"name_plural": "Russian rubles"
	},
	"RWF": {
		"symbol": "RWF",
		"name": "Rwandan Franc",
		"symbol_native": "FR",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "RWF",
		"name_plural": "Rwandan francs"
	},
	"SAR": {
		"symbol": "SR",
		"name": "Saudi Riyal",
		"symbol_native": "ر.س.‏",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "SAR",
		"name_plural": "Saudi riyals"
	},
	"SDG": {
		"symbol": "SDG",
		"name": "Sudanese Pound",
		"symbol_native": "SDG",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "SDG",
		"name_plural": "Sudanese pounds"
	},
	"SEK": {
		"symbol": "Skr",
		"name": "Swedish Krona",
		"symbol_native": "kr",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "SEK",
		"name_plural": "Swedish kronor"
	},
	"SGD": {
		"symbol": "S$",
		"name": "Singapore Dollar",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "SGD",
		"name_plural": "Singapore dollars"
	},
	"SOS": {
		"symbol": "Ssh",
		"name": "Somali Shilling",
		"symbol_native": "Ssh",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "SOS",
		"name_plural": "Somali shillings"
	},
	"SYP": {
		"symbol": "SY£",
		"name": "Syrian Pound",
		"symbol_native": "ل.س.‏",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "SYP",
		"name_plural": "Syrian pounds"
	},
	"THB": {
		"symbol": "฿",
		"name": "Thai Baht",
		"symbol_native": "฿",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "THB",
		"name_plural": "Thai baht"
	},
	"TND": {
		"symbol": "DT",
		"name": "Tunisian Dinar",
		"symbol_native": "د.ت.‏",
		"decimal_digits": 3,
		"rounding": 0,
		"code": "TND",
		"name_plural": "Tunisian dinars"
	},
	"TOP": {
		"symbol": "T$",
		"name": "Tongan Paʻanga",
		"symbol_native": "T$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "TOP",
		"name_plural": "Tongan paʻanga"
	},
	"TRY": {
		"symbol": "TL",
		"name": "Turkish Lira",
		"symbol_native": "TL",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "TRY",
		"name_plural": "Turkish Lira"
	},
	"TTD": {
		"symbol": "TT$",
		"name": "Trinidad and Tobago Dollar",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "TTD",
		"name_plural": "Trinidad and Tobago dollars"
	},
	"TWD": {
		"symbol": "NT$",
		"name": "New Taiwan Dollar",
		"symbol_native": "NT$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "TWD",
		"name_plural": "New Taiwan dollars"
	},
	"TZS": {
		"symbol": "TSh",
		"name": "Tanzanian Shilling",
		"symbol_native": "TSh",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "TZS",
		"name_plural": "Tanzanian shillings"
	},
	"UAH": {
		"symbol": "₴",
		"name": "Ukrainian Hryvnia",
		"symbol_native": "₴",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "UAH",
		"name_plural": "Ukrainian hryvnias"
	},
	"UGX": {
		"symbol": "USh",
		"name": "Ugandan Shilling",
		"symbol_native": "USh",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "UGX",
		"name_plural": "Ugandan shillings"
	},
	"UYU": {
		"symbol": "$U",
		"name": "Uruguayan Peso",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "UYU",
		"name_plural": "Uruguayan pesos"
	},
	"UZS": {
		"symbol": "UZS",
		"name": "Uzbekistan Som",
		"symbol_native": "UZS",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "UZS",
		"name_plural": "Uzbekistan som"
	},
	"VEF": {
		"symbol": "Bs.F.",
		"name": "Venezuelan Bolívar",
		"symbol_native": "Bs.F.",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "VEF",
		"name_plural": "Venezuelan bolívars"
	},
	"VND": {
		"symbol": "₫",
		"name": "Vietnamese Dong",
		"symbol_native": "₫",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "VND",
		"name_plural": "Vietnamese dong"
	},
	"XAF": {
		"symbol": "FCFA",
		"name": "CFA Franc BEAC",
		"symbol_native": "FCFA",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "XAF",
		"name_plural": "CFA francs BEAC"
	},
	"XOF": {
		"symbol": "CFA",
		"name": "CFA Franc BCEAO",
		"symbol_native": "CFA",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "XOF",
		"name_plural": "CFA francs BCEAO"
	},
	"YER": {
		"symbol": "YR",
		"name": "Yemeni Rial",
		"symbol_native": "ر.ي.‏",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "YER",
		"name_plural": "Yemeni rials"
	},
	"ZAR": {
		"symbol": "R",
		"name": "South African Rand",
		"symbol_native": "R",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "ZAR",
		"name_plural": "South African rand"
	},
	"ZMK": {
		"symbol": "ZK",
		"name": "Zambian Kwacha",
		"symbol_native": "ZK",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "ZMK",
		"name_plural": "Zambian kwachas"
	},
	"ZWL": {
		"symbol": "ZWL$",
		"name": "Zimbabwean Dollar",
		"symbol_native": "ZWL$",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "ZWL",
		"name_plural": "Zimbabwean Dollar"
	}
}

