// var emailContentText = "Hi ,\n\nHope you're well. I'm writing to know how our business can help you.\n\nPlease choose a event to schedule an appointment:\n\n${eventsUrls} See you soon!\n\n %ownerName% \n\n %companyName%";

	
		var credentials={};
		var defaultModules=["Leads","Contacts"];
        document.addEventListener("DOMContentLoaded", async function(event) {
        	ZOHO.embeddedApp.init().then(async function(){
        		getAPIURL();
        		ZOHO.CRM.API.getOrgVariable("textlocalsmsforzohocrm__crendential").then(function(apiKeyData){        			
	        		if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content && apiKeyData.Success.Content != "0"){
	        			credentials = JSON.parse(apiKeyData.Success.Content);
	        			document.getElementById("apikey").value=credentials.apikey?credentials.apikey:"";
	        			document.getElementById("senderId").value=credentials.senderId?credentials.senderId:"";
                        document.getElementById("domain").value=credentials.domain?credentials.domain:"";
	        		}	
	        		document.getElementById("loader").style.display= "none";
	        		document.getElementById("contentDiv").style.display= "block";
	        	});	
	        });    	
        });
        const urlParams = new URLSearchParams(window.location.search);
    	var serviceOrigin = urlParams.get('serviceOrigin');
        async function getAPIURL(){
        	var getmap = {"nameSpace":"<portal_name.extension_namespace>"};
            var resp = await ZOHO.CRM.CONNECTOR.invokeAPI("crm.zapikey",getmap);
    	    var zapikey = JSON.parse(resp).response;
        	var domain = "com";
        	if(serviceOrigin.indexOf(".zoho.") != -1){
        		domain = serviceOrigin.substring(serviceOrigin.indexOf(".zoho.")+6)
        	}
            var webHookurl = "https://platform.zoho."+domain+"/crm/v2/functions/textlocalsmsforzohocrm__sendsmsapi/actions/execute?auth_type=apikey&zapikey="+zapikey
			document.getElementById("webHookurl").innerText = webHookurl;
        }
       
        function updateOrgVariables(apiname,value,key){
			document.getElementById("ErrorText").innerText = "Saving...";
			document.getElementById("Error").style.display= "block";
    		ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": apiname,"value": value}).then(function(res){
    			document.getElementById("ErrorText").innerText = "Saved";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 500);
    		});
        }
        function saveCreds(key){
        	credentials[key]= document.getElementById(key).value;
        	updateOrgVariables("textlocalsmsforzohocrm__crendential",credentials);
        }