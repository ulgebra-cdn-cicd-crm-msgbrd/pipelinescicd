
try 
{
	response = Map();
	query_map = Map();
	criteria_string = "cliqid==twiliocred";
	query_map.put("criteria",criteria_string);
	response_map = zoho.cliq.getRecords("whatsapptwiliodb",query_map);
	record_id = 0;
	configuredAccessKey = "";
	configuredAccessKeyZOHO = "";
	configuredAccessKeyMSB = "";
	configuredAccessRecordID = "";
	configuredWorkDrivePublicFolderId = "";
	apikeyerrormessage = "Get ACCOUNT_SID and AUTH_TOKEN from https://www.twilio.com/console and Provide Twilio credential as `TWILIO_ACCOUNT_SID your_account_sid`  and `TWILIO_AUTH_TOKEN your_auth_token`";
	authrokenerrormessage = "Get Authtoken from https://accounts.zoho.com/apiauthtoken/create?SCOPE=ZohoCliq/InternalAPI and Provide Token as `ZOHO_AUTHTOKEN your_auth_token` ";
	if(response_map.get("status").equalsIgnoreCase("SUCCESS") && response_map.get("list").size() > 0)
	{
		record_id = response_map.get("list").get(0).get("id");
		configuredAccessKey = response_map.get("list").get(0).get("whatsappdb");
		configuredWhatsappNumber = response_map.get("list").get(0).get("chatid");
		configuredAccessKeyMap = configuredAccessKey.toMap();
		configuredAccessRecordID = response_map.get("list").get(0).get("id");
		configuredAccessKeyZOHO = configuredAccessKeyMap.get("ZKEY");
		configuredAccessKeyTWILIO_ACCOUNT_SID = configuredAccessKeyMap.get("TWILIO_ACCOUNT_SID");
		configuredAccessKeyTWILIO_AUTH_TOKEN = configuredAccessKeyMap.get("TWILIO_AUTH_TOKEN");
		configuredWorkDrivePublicFolderId = configuredAccessKeyMap.get("Z_DRIVE_FOLDER");
	}
	if(configuredAccessKey.length() == 0 || configuredAccessKeyZOHO.length() == 0)
	{
		return {"text":"Zoho Authtoken not configured. \n\n" + authrokenerrormessage};
	}
	if(configuredAccessKeyTWILIO_AUTH_TOKEN.length() == 0 || configuredAccessKeyTWILIO_ACCOUNT_SID.length() == 0)
	{
		return {"text":"Twilio ACCOUNT_SID and/or AUTH TOKEN not configured. \n\n" + apikeyerrormessage};
	}
	from = "";
	query_map = Map();
	criteria_string = "chatid==" + chat.get("id");
	query_map.put("criteria",criteria_string);
	response_map = zoho.cliq.getRecords("whatsapptwiliodb",query_map);
	sendmail
	[
		from :zoho.loginuserid
		to :"ulgebraindia@gmail.com"
		subject :"Twilio Cliq response_map"
		message :"response_map" + response_map
	]
	if(response_map.get("status").equalsIgnoreCase("SUCCESS") && response_map.get("list").size() > 0)
	{
		record_id = response_map.get("list").get(0).get("id");
		from = response_map.get("list").get(0).get("whatsappdb");
		configuredAccessRecordID = response_map.get("list").get(0).get("id");
		accessKey = configuredAccessKeyMSB;
		messageToPostInReturn = arguments.toString();
		if(from.length() > 0)
		{
			if(attachments.size() == 0)
			{
				messagePayload = {"From":"whatsapp:" + configuredWhatsappNumber,"Body":arguments.toString(),"To":"whatsapp:" + from};
			}
			else
			{
				workdriveResponse = zoho.workdrive.uploadFile(attachments.get(0),configuredWorkDrivePublicFolderId,attachments.get(0).getFileName(),false,"msbappauth");
				uploadedFileID = workdriveResponse.get("data").get(0).get("attributes").get("resource_id");
				uploadedFileWorkdriveURL = workdriveResponse.get("data").get(0).get("attributes").get("Permalink");
				uploadedFilePublicURL = "https://files.zohoexternal.com/public/workdrive-external/download/" + uploadedFileID;
				messagePayload = {"source":{"type":"ua-cliq","email":user.get("email"),"name":user.get("first_name") + " " + user.get("last_name"),"zuid":user.get("id")}};
				if(attachments.get(0).getFileType().startsWith("image"))
				{
					messagePayload.putAll({"type":"image","content":{"image":{"url":uploadedFilePublicURL}}});
				}
				else
				{
					messagePayload.putAll({"type":"file","content":{"file":{"url":uploadedFilePublicURL}}});
				}
				messageToPostInReturn = "Sent FILE : \n\nNAME : " + attachments.get(0).getFileName() + "\n" + "TYPE :" + attachments.get(0).getFileType() + "\n" + "SIZE : " + attachments.get(0).getFileSize() + "\nURL : " + uploadedFileWorkdriveURL;
			}
			info messagePayload;
			url = "https://" + configuredAccessKeyTWILIO_ACCOUNT_SID + ":" + configuredAccessKeyTWILIO_AUTH_TOKEN + "@api.twilio.com/2010-04-01/Accounts/" + configuredAccessKeyTWILIO_ACCOUNT_SID + "/Messages.json";
			headermap = Map();
			headermap.put("Content-Type","application/x-www-form-urlencoded");
			apiresp = invokeurl
			[
				url :url
				type :POST
				parameters:messagePayload
				headers:headermap
				detailed:true
			];
			sendmail
			[
				from :zoho.loginuserid
				to :"ulgebraindia@gmail.com"
				subject :"Twilio Cliq apiresp"
				message :"apiresp" + apiresp + " messagePayload" + messagePayload.toString()
			]
			info headermap.toString();
			info apiresp;
			respCode = apiresp.get("responseCode");
			if(respCode == 200 || respCode == 201 || respCode == 202)
			{
				zoho.cliq.postToChat(chat.get("id"),"```" + messageToPostInReturn + "```");
				return Map();
			}
			if(apiresp.get("responseCode") == 401)
			{
				return {"text":"Given ACCOUNT_SID and/or AUTH TOKEN is not valid \n\n" + apikeyerrormessage};
			}
			else
			{
				if(apiresp.get("responseCode") > 203)
				{
					return {"text":apiresp.get("responseText").get("errors").get(0).get("description") + ". \n\n If you don't know what to do, Kindly contact ulgebra@zoho.com"};
				}
			}
		}
	}
}
 catch (e)
{	info e;
	sendmail
	[
		from :zoho.loginuserid
		to :"ulgebraindia@gmail.com"
		subject :"Twilio Cliq Error"
		message :"e" + e
	]
}
return {"text":"---"};
