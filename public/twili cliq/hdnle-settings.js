
response = Map();
query_map = Map();
criteria_string = "cliqid==twiliocred";
query_map.put("criteria",criteria_string);
response_map = zoho.cliq.getRecords("whatsapptwiliodb",query_map);
record_id = 0;
configuredAccessKey = "";
configuredAccessKeyMap = Map();
configuredAccessKeyZOHO = "";
configuredAccessKeyMSB = "";
configuredWorkDrivePublicFolderId = "";
configuredAccessRecordID = "";
configuredWhatsAppNumber = "";
WhatsAppNumberConfigurMessage = "Give your WhatsApp Business Number `WHATSAPP_BUSINESS_NUMBER your_whatsapp_business_number`";
workdriveerrormessage = " Go to https://workdrive.zoho.com. Create a folder (any name) under My Folders. After creating, click that folder and click Share. Change visibility of the folder to \"Anyone on internet\". Copy the URL and Provide here as `WORKSPACE_FOLDER_URL your_copied_url`";
apikeyerrormessage = "Get ACCOUNT_SID and AUTH_TOKEN from https://www.twilio.com/console and Provide Twilio credential as `TWILIO_ACCOUNT_SID_AND_AUTH_TOKEN your_account_sid your_auth_token`";
authrokenerrormessage = "Get Authtoken from https://accounts.zoho.com/apiauthtoken/create?SCOPE=ZohoCliq/InternalAPI and Provide Token as `ZOHO_AUTHTOKEN your_auth_token` ";
if(response_map.get("status").equalsIgnoreCase("SUCCESS") && response_map.get("list").size() > 0)
{
	record_id = response_map.get("list").get(0).get("id");
	configuredAccessKey = response_map.get("list").get(0).get("whatsappdb");
	configuredWhatsAppNumber = response_map.get("list").get(0).get("chatid");
	configuredAccessKeyMap = configuredAccessKey.toMap();
	configuredAccessRecordID = response_map.get("list").get(0).get("id");
	configuredAccessKeyZOHO = configuredAccessKeyMap.get("ZKEY");
	configuredAccessKeyTWILIO_ACCOUNT_SID = configuredAccessKeyMap.get("TWILIO_ACCOUNT_SID");
	configuredAccessKeyTWILIO_AUTH_TOKEN = configuredAccessKeyMap.get("TWILIO_AUTH_TOKEN");
	configuredWorkDrivePublicFolderId = configuredAccessKeyMap.get("Z_DRIVE_FOLDER");
	response.put("text","Hi there, \n Integration status : ENABLED. \n Start with available message commands");
}
if(message.startsWith("DELETE_APIKEYS"))
{
	info zoho.cliq.deleteRecord("whatsapptwiliodb",configuredAccessRecordID);
	return {"text":"API Keys deleted. \n\n " + authrokenerrormessage};
}
if(configuredAccessKey.length() == 0 || configuredAccessKeyZOHO.length() == 0 || message.startsWith("ZOHO_AUTHTOKEN "))
{
	if(!message.startsWith("ZOHO_AUTHTOKEN "))
	{
		return {"text":"Zoho Authtoken not configured. \n\n" + authrokenerrormessage};
	}
	else
	{
		accessKey = message.removeFirstOccurence("ZOHO_AUTHTOKEN ");
		if(accessKey.trim().length() == 0)
		{
			return {"text":"Invalid Zoho Authtoken. \n\n" + authrokenerrormessage};
		}
		configuredAccessKeyMap = Map();
		configuredAccessKeyMap.put("ZKEY",accessKey.trim());
		valuesValueMap = {"cliqid":"twiliocred","whatsappdb":configuredAccessKeyMap.toString()};
		recordcall = zoho.cliq.createRecord("whatsapptwiliodb",valuesValueMap);
		if(configuredAccessKeyZOHO.length() > 0)
		{
			webhookurl = "https://cliq.zoho.com/api/v2/bots/twiliowhatsappbot/incoming?authtoken=" + configuredAccessKeyZOHO;
			return {"text":"Success!! Zoho AuthToken is configured. \n\nTo receive incoming message from whatsapp, configure this url " + webhookurl + " \nin Twilio WhatsApp Console \nhttps://www.twilio.com/console/sms/whatsapp/"};
		}
	}
}
if(configuredAccessKeyTWILIO_ACCOUNT_SID == null || configuredAccessKeyTWILIO_ACCOUNT_SID.length() == 0 || message.startsWith("TWILIO_ACCOUNT_SID_AND_AUTH_TOKEN "))
{
	if(!message.startsWith("TWILIO_ACCOUNT_SID_AND_AUTH_TOKEN "))
	{
		return {"text":"Twilio ACCOUNT_SID and/or AUTH TOKEN not configured. \n\n" + apikeyerrormessage};
	}
	else
	{
		twilioCred = message.removeFirstOccurence("TWILIO_ACCOUNT_SID_AND_AUTH_TOKEN ");
		accountSid = twilioCred.getPrefix(" ");
		authToken = twilioCred.getSuffix(" ");
		response.put("text","Validating Twilio Credential..." + accountSid);
		headermap = Map();
		headermap.put("Content-Type","application/x-www-form-urlencoded");
		url = "https://" + accountSid + ":" + authToken + "@api.twilio.com/2010-04-01/Accounts/" + accountSid + "/Messages.json";
		apiresp = invokeurl
		[
			url :url
			type :GET
			headers:headermap
			detailed:true
		];
		respCode = apiresp.get("responseCode");
		if(respCode == 200 || respCode == 201 || respCode == 202)
		{
			configuredAccessKeyMap.put("TWILIO_ACCOUNT_SID",accountSid.trim());
			configuredAccessKeyMap.put("TWILIO_AUTH_TOKEN",authToken.trim());
			valuesValueMap = {"cliqid":"twiliocred","whatsappdb":configuredAccessKeyMap.toString()};
			recordcall = zoho.cliq.updateRecord("whatsapptwiliodb",configuredAccessRecordID,valuesValueMap);
			return {"text":"Success!! Twilio Credential is configured. \n\n" + workdriveerrormessage};
		}
		if(apiresp.get("responseCode") == 401)
		{
			return {"text":"Given API Key is not valid \n\n" + apikeyerrormessage};
		}
		else
		{
			if(apiresp.get("responseCode") > 203)
			{
				return {"text":apiresp.get("responseText").get("errors").get(0).get("description") + ". \n\n If you don't know what to do, Kindly contact ulgebra@zoho.com"};
			}
		}
		info apiresp;
	}
}
if(configuredWorkDrivePublicFolderId == null || configuredWorkDrivePublicFolderId.length() == 0 || message.startsWith("WORKSPACE_FOLDER_URL "))
{
	if(!message.startsWith("WORKSPACE_FOLDER_URL "))
	{
		return {"text":"Zoho WorkDrive folder is not configured. You need to provide a workdrive folder URL, then only you can send attachments to MessageBird Conversations. \n\n Instruction: \n" + workdriveerrormessage};
	}
	else
	{
		folderId = message.removeFirstOccurence("WORKSPACE_FOLDER_URL ");
		folderId = folderId.subString(folderId.lastIndexOf("/") + 1);
		info folderId + " is folderid";
		samplefile = "test for messagebird".toFile("testfile.txt");
		workdriveResponse = zoho.workdrive.uploadFile(samplefile,folderId,"testfile.txt",false,"msbappauth");
		if(workdriveResponse.containsKey("errors"))
		{
			return {"text":"Given Workspace URL is not valid or public. Kindly give proper URL."};
		}
		uploadedFileWorkdriveURL = workdriveResponse.get("data").get(0).get("attributes").get("Permalink");
		if(uploadedFileWorkdriveURL != null && uploadedFileWorkdriveURL.length() > 0)
		{
			configuredAccessKeyMap.put("Z_DRIVE_FOLDER",folderId.trim());
			valuesValueMap = {"cliqid":"twiliocred","whatsappdb":configuredAccessKeyMap.toString()};
			recordcall = zoho.cliq.updateRecord("whatsapptwiliodb",configuredAccessRecordID,valuesValueMap);
			return {"text":"Workdrive folder is saved. Now you can send attachments in messages"};
		}
		else
		{
			return {"text":"Given Workspace URL is not valid or public. Kindly give proper URL."};
		}
	}
}
if((configuredWhatsAppNumber == null || configuredWhatsAppNumber.length() == 0) && !message.startsWith("WHATSAPP_BUSINESS_NUMBER "))
{
	return {"text":WhatsAppNumberConfigurMessage};
}

if(message.startsWith("WHATSAPP_BUSINESS_NUMBER "))
{
	whatsappNumber = message.removeFirstOccurence("WHATSAPP_BUSINESS_NUMBER ").trim();
	if(whatsappNumber.length() > 0)
	{
		configuredAccessKeyMap.put("WHATSAPP_BUSINESS_NUMBER",whatsappNumber);
		valuesValueMap = {"chatid":whatsappNumber};
		recordcall = zoho.cliq.updateRecord("whatsapptwiliodb",configuredAccessRecordID,valuesValueMap);
		return {"text":"WhatsApp Business Numer is configured."};
	}
}
info configuredAccessKeyMSB;
return {"text":"Settings are all fine. \n\n No response can be decided"};
