var extensionName = 'zoho-crm-twilio';
var extensionAPI = 'whatsappforzohocrm0__';

var extensionFunction = 'sendsmsapi';
var extensionInvokeAPI = extensionAPI + extensionFunction;
var extensionCredential = extensionAPI + 'inSettings';

var inSettings = {};
var authId;
var authToken;

var checkModules = [];
var checkModulesNames = [];
var createModule = "";



async function reAuthorizedToConnector() {

          

                $('.authorizationFaild').hide();
                $('.extraSettingPage').show();

                await modulesListLoad();
                await modulesListLoadCreate();

                await ZOHO.CRM.API.getOrgVariable(extensionCredential).then(function(apiKeyData){   
            
                    if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content && apiKeyData.Success.Content != "0"){
                        credentials = JSON.parse(apiKeyData.Success.Content);
                        if(credentials.modules && credentials.createModule) {

                            checkModules = credentials.modules;
                            createModule = credentials.createModule;
                            
                            for(let i=0; i<checkModules.length;i++) {
                                let checkModule = checkModules[i];
                                $('#modulesListIncomingSup_'+checkModule).find('.modulesListIncomingSup').click();
                            }

                            $('#modulesListIncomingCreateSup_'+createModule).find('.modulesListIncomingCreateSup').click();
                        }
                        else {
                            // $('.cancelSettingsButt').hide();
                            // CredentialsUpdateShow();
                        }
                    }   

                }); 

                $('.outerOfSettingDetails').show();

                document.getElementById("loader").style.display= "none";
            

        

}

async function modulesListLoad() {
    await ZOHO.CRM.META.getModules().then(function(data){
        data.modules.forEach(function(module){
            addListItem("modulesListIncoming",module.module_name,"dropdown-item",module.api_name);
        });
    });
}

function addListItem(id,text,className,value){

    let linode = `<li class="${className}" id="modulesListIncomingSup_${value}"><button class="dropdown_Butt modulesListIncomingSup" onmouseover="checkModuleList_onmouseover(this)" onmouseout="checkModuleList_onmouseonmouseout(this)" onShow="false" onclick="insert(this)" value="${value}" text="${text}">${text}<span class="addModuleToCheckListSVG" style="display: none;"><svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="18px" fill="#45596b" style="fill: #45596b;"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M18 13h-5v5c0 .55-.45 1-1 1s-1-.45-1-1v-5H6c-.55 0-1-.45-1-1s.45-1 1-1h5V6c0-.55.45-1 1-1s1 .45 1 1v5h5c.55 0 1 .45 1 1s-.45 1-1 1z"></path></svg></span></button></li>`;
    $('#'+id).append(linode);

}

function checkModuleList_onmouseover(selected) {

    if($(selected).attr("onShow") == "false") {
        $(selected).find('.addModuleToCheckListSVG').show();
    }
    else {
        //$(selected).find('.addModuleToCheckListSVG').hide();
    }

}

function checkModuleList_onmouseonmouseout(selected) {

    if($(selected).attr("onShow") == "false") {
        $(selected).find('.addModuleToCheckListSVG').hide();
    }
    else {
        //$(selected).find('.addModuleToCheckListSVG').hide();
    }

}

function insert(selected) {
    if($(selected).attr("onShow") == "false") {
        //$('.checkModuleHoverDiffClass').click().removeClass('checkModuleHoverDiffClass');
        $(selected).attr("onShow", "true");
        $(selected).parent().addClass('checkModuleHoverDiffClass');
        $(selected).find('.addModuleToCheckListSVG').html(`<svg class="addModuleToCheckListSVGSvg" xmlns="http://www.w3.org/2000/svg" height="17px" viewBox="0 0 24 24" width="24px" fill="#9f1212"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M7 19h10c.55 0 1 .45 1 1s-.45 1-1 1H7c-.55 0-1-.45-1-1s.45-1 1-1z"/></svg>`).css({'right': '3px', 'top': '-1px'}).show();
        if(!checkModules.length || checkModules.indexOf($(selected).attr("value")) == -1)
        checkModules.push($(selected).attr("value"));
        if(!checkModulesNames.length || checkModulesNames.indexOf($(selected).attr("text")) == -1)
        checkModulesNames.push($(selected).attr("text"));
        let setVal = {"modules":checkModules,"createModule": createModule};
        updateOrgVariables(extensionCredential, setVal);
        if(checkModulesNames.length)
        $('.outerOfSettingDetailsBSup1BSelected').html(checkModulesNames.toString().replaceAll(',', ', '));
        else
        $('.outerOfSettingDetailsBSup1BSelected').html(`<span class="emptyouterOfSettingDetailsBSelected">No Modules</span>`);  
    }
    else {
        $(selected).attr("onShow", "false");
        $(selected).parent().removeClass('checkModuleHoverDiffClass');
        $(selected).find('.addModuleToCheckListSVG').html(`<svg class="addModuleToCheckListSVGSvg" xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="18px" fill="#45596b" style="fill: #45596b;"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M18 13h-5v5c0 .55-.45 1-1 1s-1-.45-1-1v-5H6c-.55 0-1-.45-1-1s.45-1 1-1h5V6c0-.55.45-1 1-1s1 .45 1 1v5h5c.55 0 1 .45 1 1s-.45 1-1 1z"></path></svg>`).css({'right': '8px', 'top': '5px'});
        checkModules = checkModules.filter(item => item !== $(selected).attr("value"));
        checkModulesNames = checkModulesNames.filter(item => item !== $(selected).attr("text"));
        let setVal = {"modules":checkModules,"createModule": createModule};
        updateOrgVariables(extensionCredential, setVal);
        if(checkModulesNames.length)
        $('.outerOfSettingDetailsBSup1BSelected').html(checkModulesNames.toString().replaceAll(',', ', '));
        else
        $('.outerOfSettingDetailsBSup1BSelected').html(`<span class="emptyouterOfSettingDetailsBSelected">No Modules</span>`); 
    }
}

async function modulesListLoadCreate() {
    await ZOHO.CRM.META.getModules().then(function(data){
        data.modules.forEach(function(module){
            addListItemCreate("modulesListIncomingCreate",module.module_name,"dropdown-item",module.api_name);
        });
    });
}

function addListItemCreate(id,text,className,value){

    let linode = `<li class="${className}" id="modulesListIncomingCreateSup_${value}"><button class="dropdown_Butt modulesListIncomingCreateSup" onmouseover="checkModuleListCreate_onmouseover(this)" onmouseout="checkModuleListCreate_onmouseonmouseout(this)" onShow="false" onclick="insertCreate(this)" value="${value}" text="${text}">${text}<span class="addModuleToCheckListSVGCreate" style="display: none;"><svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="18px" fill="#45596b" style="fill: #45596b;"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M18 13h-5v5c0 .55-.45 1-1 1s-1-.45-1-1v-5H6c-.55 0-1-.45-1-1s.45-1 1-1h5V6c0-.55.45-1 1-1s1 .45 1 1v5h5c.55 0 1 .45 1 1s-.45 1-1 1z"></path></svg></span></button></li>`;
    $('#'+id).append(linode);

}

function checkModuleListCreate_onmouseover(selected) {

    if($(selected).attr("onShow") == "false") {
        $(selected).find('.addModuleToCheckListSVGCreate').show();
    }
    else {
        //$(selected).find('.addModuleToCheckListSVG').hide();
    }

}

function checkModuleListCreate_onmouseonmouseout(selected) {

    if($(selected).attr("onShow") == "false") {
        $(selected).find('.addModuleToCheckListSVGCreate').hide();
    }
    else {
        //$(selected).find('.addModuleToCheckListSVG').hide();
    }

}

function insertCreate(selected) {
    if($(selected).attr("onShow") == "false") {
        $('.checkModuleCreateHoverDiffClass').removeClass('checkModuleCreateHoverDiffClass');
        $('.modulesListIncomingCreateSup').attr("onShow", "false");
        $('.modulesListIncomingCreateSup').find('.addModuleToCheckListSVGCreate').html(`<svg class="addModuleToCheckListSVGCreateSvg" xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="18px" fill="#45596b" style="fill: #45596b;"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M18 13h-5v5c0 .55-.45 1-1 1s-1-.45-1-1v-5H6c-.55 0-1-.45-1-1s.45-1 1-1h5V6c0-.55.45-1 1-1s1 .45 1 1v5h5c.55 0 1 .45 1 1s-.45 1-1 1z"></path></svg>`).css({'right': '8px', 'top': '5px'}).hide();
        
        $(selected).attr("onShow", "true");
        $(selected).parent().addClass('checkModuleCreateHoverDiffClass');
        $(selected).find('.addModuleToCheckListSVGCreate').html(`<svg class="addModuleToCheckListSVGCreateSvg" xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="18px" fill="#1c6b1c"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M17.3 6.3c-.39-.39-1.02-.39-1.41 0l-5.64 5.64 1.41 1.41L17.3 7.7c.38-.38.38-1.02 0-1.4zm4.24-.01l-9.88 9.88-3.48-3.47c-.39-.39-1.02-.39-1.41 0-.39.39-.39 1.02 0 1.41l4.18 4.18c.39.39 1.02.39 1.41 0L22.95 7.71c.39-.39.39-1.02 0-1.41h-.01c-.38-.4-1.01-.4-1.4-.01zM1.12 14.12L5.3 18.3c.39.39 1.02.39 1.41 0l.7-.7-4.88-4.9c-.39-.39-1.02-.39-1.41 0-.39.39-.39 1.03 0 1.42z"/></svg>`).css({'right': '8px', 'top': '5px'}).show();
        createModule = $(selected).attr("value");
        let setVal = {"modules":checkModules,"createModule": createModule};
        updateOrgVariables(extensionCredential, setVal);
        if(createModule)
        $('.outerOfSettingDetailsBSup2BSelected').html($(selected).attr("text"));
        else
        $('.outerOfSettingDetailsBSup2BSelected').html(`<span class="emptyouterOfSettingDetailsBSelected">No Module</span>`); 
    }
    else {
        $(selected).attr("onShow", "false");
        $(selected).parent().removeClass('checkModuleCreateHoverDiffClass');
        $(selected).find('.addModuleToCheckListSVGCreate').html(`<svg class="addModuleToCheckListSVGCreateSvg" xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="18px" fill="#45596b" style="fill: #45596b;"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M18 13h-5v5c0 .55-.45 1-1 1s-1-.45-1-1v-5H6c-.55 0-1-.45-1-1s.45-1 1-1h5V6c0-.55.45-1 1-1s1 .45 1 1v5h5c.55 0 1 .45 1 1s-.45 1-1 1z"></path></svg>`).css({'right': '8px', 'top': '5px'});
        createModule = "";
        let setVal = {"modules":checkModules,"createModule": createModule};
        updateOrgVariables(extensionCredential, setVal);
        if(createModule)
        $('.outerOfSettingDetailsBSup2BSelected').html($(selected).attr("text"));
        else
        $('.outerOfSettingDetailsBSup2BSelected').html(`<span class="emptyouterOfSettingDetailsBSelected">No Module</span>`); 
    }
}


function mainBodyClickFunction(e) {

    if($(e.target).hasClass('addModuleToCheckListSVG') || $(e.target).hasClass('addModuleToCheckListSVGSvg') || $(e.target).hasClass('dropdown_Butt') || $(e.target).hasClass('dropdown-item') || $(e.target).hasClass('dropdown-menu') || $(e.target).hasClass('dropListButt') || $(e.target).hasClass('choose') || $(e.target).hasClass('arrowIcon')){

    }
    else{
        $('.chooseModule').removeAttr("open");
    }

    if($(e.target).hasClass('addModuleToCheckListSVGCreate') || $(e.target).hasClass('addModuleToCheckListSVGCreateSvg') || $(e.target).hasClass('dropdown_Butt') || $(e.target).hasClass('dropdown-item') || $(e.target).hasClass('dropdown-menu') || $(e.target).hasClass('dropListButtCreate') || $(e.target).hasClass('createChoose') || $(e.target).hasClass('dp_arrowIcon')){

    }
    else{
        $('.createModuleDrop').removeAttr("open");
    }

}