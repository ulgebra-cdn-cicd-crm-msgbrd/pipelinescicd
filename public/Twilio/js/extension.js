var appsConfig = {
    "ACCOUNT_SID": undefined,
    "AUTH_TOKEN": undefined
};
var initialAppsConfig = {
    "ACCOUNT_SID": undefined,
    "AUTH_TOKEN": undefined
};
var curId = 1000;
var errorId = 1000;
var initTries = 0;
var initProcessId = 1;
var serverURL = "https://sms.ulgebra.com";
var WebhookURL="";
var channelsSettings=[];
var users=[];
var channelsLength;
var phoneFieldSettings;
function syncInputValues() {

}
function closeAllErrorWindows(){
    $('.error-window-outer').remove();
}
function showTryAgainError(){
    showErroMessage('Try again later');
}
function showInvalidCredsError(){
    showErroMessage('Given Twilio credential is Invalid <br><br> Try again with proper Twilio credential from <a href="https://www.twilio.com/console" title="Click to go to Twilio console" target="_blank" noopener nofollow>Twilio console</a>.');
}
function saveWhatsappNumber(){
    var no = $("#whatsappNumber").val();
    no = no.replace(/\D/g,'');
    $("#whatsappNumber").val(no);
    updateOrgVariables("whatsappforzohocrm0__whatsappNumber",no);
}
function savePhoneField(module,phoneField){
    phoneFieldSettings["phoneField"][module]=phoneField;
    updateOrgVariables("whatsappforzohocrm0__phonefields",phoneFieldSettings);
}
function updateOrgVariables(apiname,value,key){
    showProcess(`Saving configuration...`,"saveChannel" );
    if(apiname == "whatsappforzohocrm0__whatsappNumber"){
       value = [value];
    }
    ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": apiname,"value": value}).then(function(res){
        processCompleted("saveChannel");
    });
}

function showProcess(text, id){
    $(".process-window-outer").show();
    $("#process-window-items").append(`<div id="process-item-${id}" class="process-window-item">${text}</div>`);
}

function showErroMessage(html){
    showErroWindow('Unable to process your request', html);
}
function showErroWindow(title, html){
    var id = errorId++;
   $('body').append(`<div class="error-window-outer" id="error-window-${id}">
            <div class="error-window-inner">
                <div class="error-window-title">
                    ${title}
                </div>
                <div class="error-window-detail">
                    ${html}
                </div>
                <div class="error-window-close" onclick="removeElem('#error-window-${id}')">
                   <i class="material-icons">close</i> Close
                </div>
            </div>
        </div>`);
}
function removeElem(sel){
    $(sel).remove();
}
function hideElem(sel){
    $(sel).hide();
}
function showHelpItem(helpId){
    $('#helpWindow').show();
    $('.help-window-item').hide();
    $("#"+helpId).show();
}
function processCompleted(id){
    $(`#process-item-${id}`).remove();
    if(($("#process-window-items").children().length) === 0){
        $(".process-window-outer").hide();
    }
}
function showHelpWinow(manual){
    if(!valueExists(appsConfig.ACCESS_KEY)){
        showHelpItem('step-1');
    }
    else{
        if(manual){
            showHelpItem('step-youtube');
        }
    }
}
function resetPhoneNumerVal() {
    $('#phone-select-options').html("");
    $('#phone-select-label').text('Select a phone number');
    $('#phone-select-label').attr({'data-selectedval': 'null'});
}
function selectDropDownItem(elemId, val) {
    $('.dropdown-holder').hide();
    $('#' + elemId).text(val);
    $('#' + elemId).attr({'data-selectedval': val});
}

function showDropDown(elemId) {
    if($('#' + elemId).is(":visible")){
        $('#' + elemId).hide();
    }
    else{
        $('#' + elemId).show();
        if(elemId === 'phone-select-options'){
            fetchPhoneNumbersAndShow();
        }
    }
    
}
function valueExists(val) {
    return val !== null && val !== undefined && val.length > 1 && val!=="null";
}
function deleteChannel(deleteIcon){
    var deleteElement = deleteIcon.parentElement;
    var channelIndex =deleteElement.id.substr(8);
    if(channelIndex < channelsSettings.length){
        channelsSettings.splice(channelIndex,1);
        ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "messagebirdforzohocrm__channels","value": channelsSettings});
    }
    deleteElement.parentElement.removeChild(deleteElement);
    $('#channels').html('');
  
        $('#channels').append(`<div class="fieldMapp" id="channelsHeader">
                    <div  style="min-width:30%;text-align:left;float:left;">
                        <h5>Channel Name</h5>
                    </div>
                    <div  style="min-width:35%;text-align:left;float:left;">
                         <h5><a href="https://dashboard.messagebird.com/en/channels/" target="_blank" noopener nofollow>Channel Id</a></h5>
                    </div>
                    <div  style="min-width:30%;text-align:left;float:left;">
                         <h5>Allow Reply Access</h5>
                    </div>
                </div>`);
    if(channelsSettings.length == 0){
        document.getElementById("channelsHeader").style.display="none";
    }
    channelsLength = channelsSettings.length;
    for (var i =0; i < channelsSettings.length; i++) {
        if(channelsSettings[i]){
            addChannelSetting(channelsSettings[i],i); 
        }
    }
}
function addChannel(){
    var channels =["WhatsApp","Telegram","Line","WeChat","Facebook Messenger","Instagram","SMS","Email"];
    var options ="";
    channels.forEach(function(channel){
        options =options+'<option value="'+channel+'">'+channel+'</option>';
    });
    var accessUsers ='<option value="everyone">Everyone</option>';
    var allowedUser ='';
    users.forEach(function(user){
            accessUsers =accessUsers+'<option value="'+user.id+'">'+user.email+'</option>';
    });
    var channelUi = '<div class="fieldMapp" id="channel_'+channelsLength+'"><div class="fromField"><div class="styled-select slate fieldMap"><select >'+options+'</select></div></div><i style="float:left;min-width:4%;text-align:center;" class="material-icons ">arrow_forward</i><div class="toField"><input style="width:320px;height:35px;" type="text"></input></div><div class="fromField" style="width:23%;padding-left:5px;"><div class="styled-select slate fieldMap" style="width:230px;"><select name="access" style="width:230px;">'+accessUsers+'</select></div></div><div class="btn-save" class="btn-save" style="float:left;height:32px;" onclick="saveChannel(this)">Save</div><i style="float:left;min-width:4%;text-align:center;padding-top:5px;" class="material-icons" onclick="deleteChannel(this)" >delete</i></div>';
    // var channelUi = '<div class="fieldMapp" id="'+currentChannel+'"><div class="fromField"><div class="styled-select slate fieldMap"><select >'+options+'</select></div></div><i style="float:left;min-width:4%;text-align:center;" class="material-icons ">arrow_forward</i><i style="float:right;min-width:4%;text-align:center;" class="material-icons" onclick="deleteChannel(this)" >delete</i><div class="btn-save" style="float:right;min-width:10%;" onclick="saveChannel(this)">Save</div><div class="toField"><input style="width:320px;height:35px;" type="text"></input></div></div>';
    channelsLength = channelsLength+1;
    $('#channels').append(channelUi);
    document.getElementById("channelsHeader").style.display="block";
}
function saveChannel(saveButton){
    var saveElement = saveButton.parentElement;
    var channelIndex =saveElement.id.substr(8);
    var channelName =saveElement.children[0].children[0].children[0].value;
    var channelId = saveElement.children[2].children[0].value;
    var userId = saveElement.children[3].children[0].children[0].value;
    var isNewChannel = true;
    showProcess(`Saving Channel configuration...`,"saveChannel" );
    channelsSettings[channelIndex]={"channelName":channelName,"channelId":channelId,"userId":userId};
    ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "messagebirdforzohocrm__channels","value": channelsSettings}).then(function(resp){
        processCompleted("saveChannel");
    });
}
function addChannelSetting(channel,i){
    var channelNames =["WhatsApp","Telegram","Line","WeChat","Facebook Messenger","Instagram","SMS","Email"];
    var options ="";
    options =options+'<option value="'+channel.channelName+'">'+channel.channelName+'</option>';
    channelNames.forEach(function(channelName){
        if(channelName != channel.channelName){
            options =options+'<option value="'+channelName+'">'+channelName+'</option>';
        }
    });
    addOrDeleteChannel ='<i style="float:left;min-width:4%;text-align:center;padding-top:5px;cursor:pointer;" class="material-icons" onclick="deleteChannel(this)" >delete</i>';
    var accessUsers ='<option value="everyone">Everyone</option>';
    var allowedUser ='';
    users.forEach(function(user){
        if(user.id == channel.userId){
            allowedUser = '<option value="'+user.id+'">'+user.email+'</option>';
        }
        else{
            accessUsers =accessUsers+'<option value="'+user.id+'">'+user.email+'</option>';
        }
    });
    accessUsers=allowedUser+accessUsers;
    var channelUi = '<div class="fieldMapp" id="channel_'+i+'"><div class="fromField"><div class="styled-select slate fieldMap"><select >'+options+'</select></div></div><i style="float:left;min-width:4%;text-align:center;" class="material-icons ">arrow_forward</i><div class="toField"><input style="width:320px;height:35px;" type="text" value="'+channel.channelId+'" ></input></div><div class="fromField" style="width:23%;padding-left:5px;"><div class="styled-select slate fieldMap" style="width:230px;"><select style="width:230px;" name="access" >'+accessUsers+'</select></div></div><div class="btn-save" class="btn-save" style="float:left;height:32px;" onclick="saveChannel(this)">Save</div>'+addOrDeleteChannel+'</div>';
     // var channelUi = '<div class="fieldMapp" id="'+channel.channelName+'"><div class="fromField"><div class="styled-select slate fieldMap"><select >'+options+'</select></div></div><i style="float:left;min-width:4%;text-align:center;" class="material-icons ">arrow_forward</i>'+addOrDeleteChannel+'<div class="btn-save" style="float:right;min-width:10%;" onclick="saveChannel(this)">Save</div><div class="toField"><input style="width:320px;height:35px;" type="text" value="'+channel.channelId+'" ></input></div></div>';
    $('#channels').append(channelUi);
}
function getWebhookURL(){
    var func_name = "whatsappforzohocrm0__sendwhatsapp";
    var req_data ={"action": "getApiURL"};
    var getmap = {"nameSpace":"<portal_name.extension_namespace>"};
    return Promise.all([ZOHO.CRM.CONNECTOR.invokeAPI("crm.zapikey",getmap),ZOHO.CRM.FUNCTIONS.execute(func_name, req_data)]).then(function(resp){
        if(resp[1]){
            let output = resp[1].details.output;
            if(output){
                domain =  output.substring(22,output.indexOf("/crm/v2/functions/"));
            }
        }
        domain = domain.trim();
        var zapikey = JSON.parse(resp[0]).response;
        var incomingWebhookURL = "https://platform.zoho."+domain+"/crm/v2/functions/whatsappforzohocrm0__whatsapphandler/actions/execute?auth_type=apikey&zapikey="+zapikey;
        WebhookURL = "https://platform.zoho."+domain+"/crm/v2/functions/whatsappforzohocrm0__sendwhatsapp/actions/execute?auth_type=apikey&zapikey="+zapikey;
        document.getElementById("webHookurl").innerText =WebhookURL;
        document.getElementById("incomingwebHookurl").innerText =incomingWebhookURL;
    });
}    
function resetAllFields(){
    if(valueExists(appsConfig.ACCOUNT_SID) || valueExists(appsConfig.AUTH_TOKEN)){
        ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "whatsappforzohocrm0__Twilio_Credentials","value": {"ACCOUNT_SID":"","AUTH_TOKEN":""}}).then(function (res) {
        appsConfig.ACCOUNT_SID = null;
        appsConfig.AUTH_TOKEN = null;
            console.log(res);
            resolveCurrentProcessView();
        }).catch(function (err) {
            showTryAgainError();
            console.log(err);
        });
    }
}
function resolveCurrentProcessView() {
    $('#input-api-key').val(appsConfig.ACCOUNT_SID);
    $('#input-auth-token').val(appsConfig.AUTH_TOKEN);
    if (valueExists(appsConfig.ACCOUNT_SID) && valueExists(appsConfig.AUTH_TOKEN) ) {
        $('#input-api-key').attr({'readonly':true})
        $('#input-auth-token').attr({'readonly':true}).val(appsConfig.AUTH_TOKEN.substr(0,5)+"xxxxxxxxxxxxx");
        var url = "https://api.twilio.com/2010-04-01/Accounts/"+appsConfig.ACCOUNT_SID+"/Messages.json";
        var request = {
            url: url,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": 'Basic ' + btoa(appsConfig.ACCOUNT_SID + ":" + appsConfig.AUTH_TOKEN)
            },
        };
        var phoneNumberProcess = curId++;
        showProcess(`Validating given configuration...`, phoneNumberProcess);
        ZOHO.CRM.HTTP.get(request).then(function(resp){
            var responseJSON = JSON.parse(resp);
            processCompleted(phoneNumberProcess);
            if (!responseJSON.code) {
                $("#incoming-integ-status").removeClass('c-crimson').removeClass('c-orange').removeClass('c-silver').addClass('c-green').html('<i class="material-icons">error</i> Twilio WhatsApp - Integration enabled');
            }
            else{
                $("#incoming-integ-status").removeClass('c-orange').removeClass('c-green').removeClass('c-crimson').addClass('c-silver').html('<i class="material-icons">error</i> Reset configurate and save Access Key ');
            }   
        });     
    }
    else {
        showHelpWinow();
        $('#input-api-key').removeAttr('readonly');
        $('#input-auth-token').removeAttr('readonly');
        $("#incoming-integ-status").removeClass('c-silver').removeClass('c-orange').removeClass('c-green').addClass('c-crimson').html('<i class="material-icons">error</i> Provide ACCOUNT_SID and AUTH_TOKEN to enable integration');
    }
}
function saveAPIKey() {
    var accountSid = $('#input-api-key').val();
    var authToken =$('#input-auth-token').val();
    if(!valueExists(accountSid)){
        showErroWindow('ACCOUNT SID is missing','Kindly provide ACCOUNT SID, then proceed to save.');
        return;
    }
    if(!valueExists(authToken)){
        showErroWindow('AUTH TOKEN is missing','Kindly provide AUTH TOKEN, then proceed to save.');
        return;
    }      
    var authtokenChange = (!valueExists(appsConfig.ACCOUNT_SID) || !valueExists(appsConfig.AUTH_TOKEN));
    if(authtokenChange){
        url = "https://api.twilio.com/2010-04-01/Accounts/"+accountSid+"/Messages.json";
        var request = {
            url: url,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": 'Basic ' + btoa(accountSid + ":" + authToken)
            },
        };
        var phoneNumberProcess = curId++;
        showProcess(`Validating given configuration...`, phoneNumberProcess);

        ZOHO.CRM.HTTP.get(request).then(function(resp){
            var responseJSON = JSON.parse(resp);
            processCompleted(phoneNumberProcess);
            if (!responseJSON.code) {
                var authIdProcess = curId++;
                showProcess('Saving Twilio Credentials...', authIdProcess);
                appsConfig.ACCOUNT_SID = accountSid;
                initialAppsConfig.ACCOUNT_SID = accountSid;
                appsConfig.AUTH_TOKEN = authToken;
                initialAppsConfig.AUTH_TOKEN = authToken;

                ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "whatsappforzohocrm0__Twilio_Credentials","value": {"ACCOUNT_SID":accountSid,"AUTH_TOKEN":authToken}}).then(function (res) {
                    console.log(res);
                    processCompleted(authIdProcess);
                    $('#input-api-key').attr({'readonly':true});
                    $('#input-auth-token').attr({'readonly':true}).val(appsConfig.AUTH_TOKEN.substr(0,5)+"xxxxxxxxxxxxx");
                    $("#incoming-integ-status").removeClass('c-crimson').removeClass('c-orange').removeClass('c-silver').addClass('c-green').html('<i class="material-icons">error</i> Twilio WhatsApp - Integration enabled');
                }).catch(function (err) {
                    processCompleted(authIdProcess);
                    console.log(err);
                });
            }
            else if(responseJSON.code){
                 showInvalidCredsError();
            }
            else{
                showTryAgainError();
            }
        }).catch(function (err) {
            processCompleted(phoneNumberProcess);
            showTryAgainError();
            console.log(err);
        });
    }
}
function initializeFromConfigParams(){
    var applicationProcess = curId++;
    showProcess(`Initializing app ...`, initProcessId);
    showProcess(`Fetching application configuration ...`, applicationProcess);
    Promise.all([ZOHO.CRM.API.getOrgVariable("whatsappforzohocrm0__Twilio_Credentials"),getWebhookURL()]).then(function(apiKeyData){
        if(apiKeyData[0] && apiKeyData[0].Success && apiKeyData[0].Success.Content && valueExists(apiKeyData[0].Success.Content)){
            appsConfig.ACCOUNT_SID = JSON.parse(apiKeyData[0].Success.Content).ACCOUNT_SID;
            initialAppsConfig.ACCOUNT_SID = JSON.parse(apiKeyData[0].Success.Content).ACCOUNT_SID;
            appsConfig.AUTH_TOKEN = JSON.parse(apiKeyData[0].Success.Content).AUTH_TOKEN;
            initialAppsConfig.AUTH_TOKEN = JSON.parse(apiKeyData[0].Success.Content).AUTH_TOKEN;
        }
        processCompleted(applicationProcess);
        processCompleted(initProcessId);
        resolveCurrentProcessView();
    }).catch(function (err) {
        showTryAgainError();
        processCompleted(applicationProcess);
        console.log(err);
    });

}
function setPhoneFields(){
    var moduleList =["Leads","Contacts"];
    ZOHO.CRM.API.getOrgVariable("whatsappforzohocrm0__phonefields").then(function(apiKeyData){
        if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content && apiKeyData.Success.Content != "" ){
            phoneFieldSettings = JSON.parse(apiKeyData.Success.Content);
            Promise.all([ZOHO.CRM.META.getFields({"Entity":"Leads"}),ZOHO.CRM.META.getFields({"Entity":"Contacts"})]).then(function(fieldsData){
                for(let i=0;i<moduleList.length;i++){
                    var options = '';
                    var selectedOption;
                    fieldsData[i].fields.forEach(function(field){
                        if(field.data_type == "phone"){
                            if(phoneFieldSettings && phoneFieldSettings["phoneField"] && field.api_name == phoneFieldSettings["phoneField"][moduleList[i]]){
                                selectedOption=`<option value="${field.api_name}">${field.field_label}</option>`;
                            }
                            else{
                                options=`${options}<option value="${field.api_name}">${field.field_label}</option>`;
                            }
                        }    
                    });
                    if(selectedOption){
                        options=`${selectedOption}${options}`;
                    }
                    else{    
                        options = `<option value="select">Select Phone Field</option>${options}`;
                    }
                    $("#settingsTable").append(`<tr id="${moduleList[i]}PhoneField">
                                      <td>
                                        <label>Configure Phone Field for ${moduleList[i]}<label>
                                      </td>
                                      <td>
                                        <div class="styled-select slate">
                                        <select  name="field5" onchange="savePhoneField('${moduleList[i]}',this.value)">
                                            ${options}
                                          </select>
                                        </div>
                                      </td>
                                  </tr>`);
                }
                 $("#settingsTable").append(`<div style="min-height:100px;"></div>`);
            });
        }    
    });   
}
window.onload = function () {
    const urlParams = new URLSearchParams(window.location.search);
    const serviceOrigin = urlParams.get('serviceOrigin');
    window.document.body.insertAdjacentHTML( 'afterbegin', `<div class="contactme">If you need help for configuration of this app, kindly mail to <a href="mailto:ulgebra@zoho.com">ulgebra@zoho.com</a> or <a target="_blank" href="https://wa.me/917397415648?text=Hello%20Ulgebra,%20I%20have%20a%20query">WhatsApp Now</a><br><br><div class="ua-brand" style=" color: grey; "> <a href="https://www.ulgebra.com/?source=appsettings" target="_blank"> an <span class="brand-brand"> <img src="https://ulgebra.com/images/ulgebra/favs/favicon.png"> Ulgebra</span> product</a></div></div>`);
    ZOHO.embeddedApp.init().then(function(){
        initializeFromConfigParams();
        setPhoneFields();
        ZOHO.CRM.API.getOrgVariable("whatsappforzohocrm0__whatsappNumber").then(function(apiKeyData){
            if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content != "0" ){
                document.getElementById("whatsappNumber").value=JSON.parse(apiKeyData.Success.Content).length?JSON.parse(apiKeyData.Success.Content)[0]:"";
            }   
        }); 
        ZOHO.CRM.API.getOrgVariable("whatsappforzohocrm0__module").then(function(moduleData){
            if(moduleData && moduleData.Success && moduleData.Success.Content){
                document.getElementById("modules").value = moduleData.Success.Content;
            }   
        });
    });
};
