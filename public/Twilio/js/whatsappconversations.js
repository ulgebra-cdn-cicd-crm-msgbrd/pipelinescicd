    var recordId;
    var recordModule;
    var ButtonPosition;
    var accessKey;
    var channelId;
    var existingConversations = [];
    var phoneFields = [];
    var RTCs = {

    };
    var platform = "";
    var convId;
    var storedChannelSettings = {};
    var currentUser = {};
    var appsConfig = {
        "ACCOUNT_SID": undefined,
        "AUTH_TOKEN": undefined
    };
    var messages = [];
    var toNumberList = [];
    var Mobile;
    var smsTemplates = [];
    var user;
    var currentRecord;
    var moduleFields=[];
    var userFields =[];
    var next_page_uri = null;
    var fromPhoneNumber = null;
    var phoneNumbers = [];
    document.addEventListener("DOMContentLoaded", function(event) {
        var firebaseConfig = {
            apiKey: "AIzaSyDkfrxT-4n_iFi_WDUzi9HIHGS0tCSZ_O8",
            authDomain: "twilio-e770e.firebaseapp.com",
            databaseURL: "https://twilio-e770e.firebaseio.com",
            projectId: "twilio-e770e",
            storageBucket: "twilio-e770e.appspot.com",
            messagingSenderId: "971525371883",
            appId: "1:971525371883:web:4f5e39f2231c9992890bae"
          };
            // // Initialize Firebase
            if(document.getElementById("inputFile")){
              firebase.initializeApp(firebaseConfig);
              storageRef = firebase.storage().ref();
            } 
        ZOHO.embeddedApp.on("PageLoad", function(record) {
            recordId = record.EntityId;
            recordModule = record.Entity;
            if (recordModule == "Contacts") {
                phoneFields = ["Mobile", "Phone", "Asst_Phone", "Home_Phone", "Other_Phone"];
            } else {
                phoneFields = ["Mobile", "Phone"];
            }
            ButtonPosition = record.ButtonPosition;
            $("#prevConversations").html('<span class="spinnow material-icons" style="font-size: 20px;display:inline-block;">toys</span> Loading conversations ...');
            Promise.all([ZOHO.CRM.API.searchRecord({
                Entity: "whatsappforzohocrm0__WhatsApp_Templates",
                Type: "criteria",
                Query: "(whatsappforzohocrm0__Module_Name:equals:" + recordModule + ")",
                delay: false
            }),ZOHO.CRM.META.getFields({"Entity":recordModule}),ZOHO.CRM.META.getFields({"Entity":"Users"}), ZOHO.CRM.API.getRecord({Entity: recordModule,RecordID: recordId})]).then(function(data) {
                smsTemplates = data[0].data;
                moduleFields = data[1].fields;
                userFields = data[2].fields;
                currentRecord=data[3].data[0];
                var templateList = "";
                var approvedTemplateList = "";
                if (smsTemplates) {
                    for (let i = 0; i < smsTemplates.length; i++) {
                        if(smsTemplates[i].whatsappforzohocrm0__Approved_by_WhatsApp) {
                            approvedTemplateList = `${approvedTemplateList}<option value="${smsTemplates[i].id}" id="approved-${smsTemplates[i].id}"></option>`;
                        }
                        templateList = `${templateList}<option value="${smsTemplates[i].id}" id="${smsTemplates[i].id}"></option>`;
                    }
                    $('#templateList').append(templateList);
                    $('#approvedTemplateList').append(approvedTemplateList);
                    for (let i = 0; i < smsTemplates.length; i++) {
                        document.getElementById(smsTemplates[i].id).innerText = smsTemplates[i].Name;
                        if (smsTemplates[i].whatsappforzohocrm0__Approved_by_WhatsApp) {
                            document.getElementById("approved-" + smsTemplates[i].id).innerText = smsTemplates[i].Name;
                        }
                    }
                } 
                else {
                    $('#templateList').append(`<option value="" >No Templates</option>`);
                    $('#approvedTemplateList').append(`<option value="" >No WhatsApp Approved Templates</option>`);
                }
                var phoneList="";
                phoneNumbers = [];
                var indexCount = 0;
                moduleFields.forEach(function(field){
                    if(field.data_type == "phone" && currentRecord[field.api_name] != null && currentRecord[field.api_name] != ""){
                        phoneList = phoneList +'<option '+(indexCount==0 ? 'selected': '')+' value="'+currentRecord[field.api_name]+'">'+currentRecord[field.api_name]+'</option>';
                        if(phoneNumbers.indexOf(currentRecord[field.api_name])== -1){
                            phoneNumbers.push(currentRecord[field.api_name]);
                        }
                        indexCount==0 ?$("#input-to-phone").val(currentRecord[field.api_name]):"";
                        indexCount++;
                    }
                });
                $("#contact-phone-target").append(phoneList);
                if(currentRecord.Mobile != null && currentRecord.Mobile != ""){
                    Mobile = currentRecord.Mobile;
                }
                else{
                    Mobile = currentRecord.Phone;
                }
                
                if(currentRecord.Owner != null)
                {
                    var userFetch =  ZOHO.CRM.API.getUser({ID:currentRecord.Owner.id});
                }
                Promise.all([ZOHO.CRM.API.getOrgVariable("whatsappforzohocrm0__Twilio_Credentials"), ZOHO.CRM.API.getOrgVariable("whatsappforzohocrm0__whatsappNumber"),userFetch]).then(function(apiKeyData) {
                    if (apiKeyData[0] && apiKeyData[0].Success && apiKeyData[0].Success.Content && valueExists(apiKeyData[0].Success.Content)) {
                        appsConfig.ACCOUNT_SID = JSON.parse(apiKeyData[0].Success.Content).ACCOUNT_SID;
                        appsConfig.AUTH_TOKEN = JSON.parse(apiKeyData[0].Success.Content).AUTH_TOKEN;
                    }
                    if(!appsConfig.ACCOUNT_SID || !appsConfig.AUTH_TOKEN){

                          $("#existingConvListWindow").html("Please fill the twilio credentials in <button onclick='goToWhatsappTab()'> WhatsApp tab.</button>");
                             return;
                    }
                    if (apiKeyData[1] && apiKeyData[1].Success && apiKeyData[1].Success.Content != "0") {
                        existingConversations = JSON.parse(apiKeyData[1].Success.Content);
                        fromPhoneNumber = existingConversations[0];
                        $("#input-from-phone").val(fromPhoneNumber);
                    }
                    if(!fromPhoneNumber){
                        $("#existingConvListWindow").html("Please fill the twilio whatsapp number in <button onclick='goToWhatsappTab()'> WhatsApp tab.</button>");
                        return;
                    }
                    if(apiKeyData[2]){
                        user = apiKeyData[2].users[0];
                    }
                    renderConversationList();
                });
            });   

        });
        
        ZOHO.embeddedApp.init();
        if(document.getElementById("inputFile")){
          document.getElementById("inputFile").addEventListener("change", (function (event) {
              $('.chatbox-outer').append(`<div class="loadingdiv"> <span class="spinnow material-icons" style="font-size: 20px;display:inline-block;">toys</span> file is uploading...</div>`);
              var file = document.getElementById("inputFile").files[0];
              if (((file.size / 1024) / 1024) > 10) {
                  $(".loadingdiv").remove();
                  $('.chatbox-outer').append(`<div style="background-color:red;" class="loadingdiv"> file size should be within 10 Mb.</div>`);
                  setTimeout((function(){ $(".loadingdiv").remove(); }), 2000);
                  return;
              }
              if(file.type.indexOf("image") == -1 && file.type.indexOf("audio") == -1 && file.type.indexOf("video") == -1 && file.type.indexOf("pdf") == -1){
                  $(".loadingdiv").remove();
                  $('.chatbox-outer').append(`<div style="background-color:red;" class="loadingdiv"> This file type not supported.</div>`);
                  setTimeout((function(){ $(".loadingdiv").remove(); }), 2000);
                  return;
              }
              sendfile(file);
          }));
        }
    });
     function goToWhatsappTab(){
            ZOHO.CRM.UI.Widget.open({Entity:"WhatsApp"})
            .then(function(data){
                console.log(data)
            });
        }
    function selectTemplate(templateId, textAreaId) {
        if(!templateId){
            $("#" + textAreaId).val("");
            return;
        }
        for (var i = 0; i < smsTemplates.length; i++) {
            if (smsTemplates[i].id == templateId) {
                $("#" + textAreaId).val(getMessageWithFields(smsTemplates[i].whatsappforzohocrm0__Message));
                break;
            }
        }
    }
    function getMessageWithFields(message){
        var customerData=[];
        var module = recordModule;
        moduleFields.forEach(function(field){
            var replace = "\\${"+module+"."+field.field_label+"}";
            var re = new RegExp(replace,"g");
            if(currentRecord[field.api_name] != null)
            {
                var value = currentRecord[field.api_name];
                if(value.name)
                {
                    value = value.name;
                }
                
                message = message.replace(re,value);
            }
            else
            {
                message = message.toString().replace(re," ");
            }
        }); 
        if(user)
        {
            userFields.forEach(function(field){
                var replace = "\\${Users."+field.field_label+"}";
                var re = new RegExp(replace,"g");
                if(user[field.api_name] != null)
                {
                    var value = user[field.api_name];
                    if(value.name)
                    {
                        value = value.name;
                    }
                    
                    message = message.replace(re,value);
                }
                else
                {
                    message = message.toString().replace(re," ");
                }
            });
            return message.trim();
        }
        else{
            return message.trim();
        }
        
    }
    function openUploader(){
        document.getElementById("inputFile").click();
    }
    function sendfile(file){
        // File or Blob named mountains.jpg
        // Create the file metadata
        var metadata = {
          contentType: file.type,
        };

        // Upload file and metadata to the object 'images/mountains.jpg'
        var uploadTask = storageRef.child(file.name).put(file, metadata);

        // Listen for state changes, errors, and completion of the upload.
        uploadTask.on('state_changed', function(snapshot){
          // Observe state change events such as progress, pause, and resume
          // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
          var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          console.log('Upload is ' + progress + '% done');
          // switch (snapshot.state) {
          //   case firebase.storage.TaskState.PAUSED: // or 'paused'
          //     console.log('Upload is paused');
          //     break;
          //   case firebase.storage.TaskState.RUNNING: // or 'running'
          //     console.log('Upload is running');
          //     break;
          // }
        }, function(error) {
            $(".loadingdiv").text(error);
          // Handle unsuccessful uploads
        }, function() {
          // Handle successful uploads on complete
          // For instance, get the download URL: https://firebasestorage.googleapis.com/...
          uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
            console.log('File available at', downloadURL);
            $(".loadingdiv").remove();
            sendMessage(downloadURL,file);
          });
        });
    }
    function valueExists(val) {
        return val !== null && val !== undefined && val.length > 1 && val !== "null";
    }

    function getTimeString(time) {
        let msgTime = Date.parse(time);
        let timeSuffix = "am";
        if (isNaN(msgTime) || time.indexOf('Z') < 0) {
            return time;
        }
        let msgHour = new Date(msgTime).getHours();
        timeSuffix = msgHour > 12 ? "pm" : "am";
        msgHour = msgHour > 12 ? msgHour - 12 : msgHour;
        let msgMins = new Date(msgTime).getMinutes();
        msgMins = msgMins < 10 ? "0" + msgMins : msgMins;
        return msgHour + ":" + msgMins + " " + timeSuffix;
    }

    function initiateRTSForConvs() {
        console.log("Trying to initialize initiateRTSForConvs");
        if (typeof firebase === undefined) {
            console.log("FIREBASE not yet present..");
            setTimeout(initiateRTSForConvs, 2000);
        } else {
            console.log("FIREBASE loaded..");
            for (var item in existingConversations) {
                var obj = existingConversations[item];
                initiateRTListeners(obj);
            }
        }
    }

    function renderConversationList() {
        initiateRTSForConvs();
        $("#prevConversations").html("");   
        if(!existingConversations || existingConversations.length === 0){
            $("#prevConversations").html("Try Later. No Conversations found.");
        }
        else{
            for (var item in phoneNumbers) {
                var obj = phoneNumbers[item];
                $("#prevConversations").prepend(`<div class="conv-list-item" id="list-${obj}" onclick="openConversation('${obj}')">
                                    <div class="conv-channel">
                                       WhatsApp
                                    </div>
                                    <div class="conv-number">
                                        ${fromPhoneNumber} - ${obj}
                                    </div>
                                </div>`);
            }
        }    
    }

    function initiateMessage() {
        var phoneNumber = $("#input-to-phone").val();
        var text = $("#input-sms-content").val();
        text = decodeURIComponent(encodeURIComponent(text).replaceAll("%C2%A0","%20"));

        if(!phoneNumber || !text){
            alert("Please check Phone and message field.")
            return;
        }
        convId = phoneNumber;
        $('#sms-form').append(`<div class="processingFull"> Sending message...</div>`);
        fromPhoneNumber = fromPhoneNumber.indexOf('whatsapp') == -1 ? "whatsapp:+" + fromPhoneNumber.replace(/\D/g,'') : fromPhoneNumber;
        convId = convId.replace(/\D/g,'');
        convId = convId.indexOf('+') < 0 ? "+"+convId.replace(/\D/g,'') : convId;
        var url = "https://api.twilio.com/2010-04-01/Accounts/" + appsConfig.ACCOUNT_SID + "/Messages.json";
        var request = {
            url: url,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": 'Basic ' + btoa(appsConfig.ACCOUNT_SID + ":" + appsConfig.AUTH_TOKEN)
            },
            body: {
                "From": fromPhoneNumber,
                "Body": text,
                "To": "whatsapp:" + convId
            },
        };
        ZOHO.CRM.HTTP.post(request).then(function(resp) {
            setTimeout((() => {
                $(".processingFull").remove();
                $('.chatmessages-inner').html("");
                getConvs(0);
            }), 3000);
        });

    }

    function sendMessage(mediaUrl,fileMeta) {
        
        var text = $("#main-message-text").val().trim();
        text = decodeURIComponent(encodeURIComponent(text).replaceAll("%C2%A0","%20"));
        if((text && text.length) || mediaUrl)
        {
            $('.chatbox-outer').append(`<div class="loadingdiv"> <span class="spinnow material-icons" style="font-size: 20px;display:inline-block;">toys</span> Sending message...</div>`);

            var url = "https://api.twilio.com/2010-04-01/Accounts/" + appsConfig.ACCOUNT_SID + "/Messages.json";
            fromPhoneNumber = fromPhoneNumber.indexOf('whatsapp') == -1 ? "whatsapp:+" + fromPhoneNumber.replace(/\D/g,'') : fromPhoneNumber;
            convId = convId.replace(/\D/g,'');
            convId = convId.indexOf('+') < 0 ? "+"+convId.replace(/\D/g,'') : convId;
           
            var body= {
                "From": fromPhoneNumber,
                "Body": text,
                "To": "whatsapp:" + convId
                };
            if(mediaUrl){
                body["MediaUrl"]= mediaUrl;
            }   
            var request = {
                url: url,
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "Authorization": 'Basic ' + btoa(appsConfig.ACCOUNT_SID + ":" + appsConfig.AUTH_TOKEN)
                },
                body: body
            };
            ZOHO.CRM.HTTP.post(request).then(function(resp) {
                resp = JSON.parse(resp);
                if(resp.sid){
                    let from = resp.from.substring(resp.from.indexOf('whatsapp:')+9); 
                    //"{"sid": "SMe942a5d1f0a74e6fb294297aa4372588", "date_created": "Sat, 16 May 2020 04:27:07 +0000", "date_updated": "Sat, 16 May 2020 04:27:07 +0000", "date_sent": null, "account_sid": "ACd14afb5b4dbcbe6b0e79bcf69fe13a72", "to": "whatsapp:+918012178547", "from": "whatsapp:+14155238886", "messaging_service_sid": null, "body": "c", "status": "queued", "num_segments": "1", "num_media": "0", "direction": "outbound-api", "api_version": "2010-04-01", "price": null, "price_unit": null, "error_code": null, "error_message": null, "uri": "/2010-04-01/Accounts/ACd14afb5b4dbcbe6b0e79bcf69fe13a72/Messages/SMe942a5d1f0a74e6fb294297aa4372588.json", "subresource_uris": {"media": "/2010-04-01/Accounts/ACd14afb5b4dbcbe6b0e79bcf69fe13a72/Messages/SMe942a5d1f0a74e6fb294297aa4372588/Media.json"}}"
                    var messageMap = {"Name":resp.sid,"whatsappforzohocrm0__Message":resp.body,"whatsappforzohocrm0__From":resp.from,"whatsappforzohocrm0__Recipient_Phone":resp.to,"whatsappforzohocrm0__Status":resp.status,"whatsappforzohocrm0__Direction":"Outgoing"};
                    messageMap["whatsappforzohocrm0__"+recordModule.substring(0,recordModule.length-1)]=recordId;
                    ZOHO.CRM.API.insertRecord({Entity:"whatsappforzohocrm0__WhatsApp_History",APIData:messageMap,Trigger:["workflow"]});
                }
                $("#main-message-text").val("");
                $('#templateList').prop('selectedIndex', 0);
                //$(".loadingdiv").text('Message Sent, Checking Status...');
                // setTimeout((() => {
                    //getConvs(0);
                    addMessageInBox(resp, true,mediaUrl,fileMeta);
                    $(".loadingdiv").remove();
                    $(".chatmessages-inner").scrollTop($(".chatmessages-inner")[0].scrollHeight);
                // }), 3000);
            });
        }
        else{
             $('.chatbox-outer').append(`<div class="loadingdiv"> Message Cannot be empty</div>`);
            setTimeout((() => {
                $(".loadingdiv").remove();
            }), 1000); 
        }    
    }

    function escapeHTMLS(rawStr) {
        return $('<textarea/>').text(rawStr).html();
    }

    function getConvs(offset,loadMore) {
        $("#initiateConvWindow").hide();
        //              $('#existingConvListWindow').show();
        $("#existingConvWindow").show();
        if (loadMore) {
            $(`#loadmore-off-${offset}`).html('Loading...');
        }
        else{
            $('.chatbox-outer').append(`<div class="loadingdiv"> <span class="spinnow material-icons" style="font-size: 20px;display:inline-block;">toys</span> Loading Messages...</div>`);
        }
        if(loadMore && next_page_uri){
            url = next_page_uri;
        }
        else{
            url = "https://api.twilio.com/2010-04-01/Accounts/" + appsConfig.ACCOUNT_SID + "/Messages.json?PageSize=1000";
        }
        var request = {
            url: url,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": 'Basic ' + btoa(appsConfig.ACCOUNT_SID + ":" + appsConfig.AUTH_TOKEN)
            },
        };
        ZOHO.CRM.HTTP.get(request).then(function(resp) {
            renderDataFromJSON(loadMore, resp);
        });
    }

    function renderDataFromJSON(loadMore,data) {
        if (loadMore) {
            $(`#loadmore-off-${offset}`).html('').addClass('loadedLoadmore').prop("onclick", null).off("click");
        }
        else{
            $('.chatmessages-inner').html("");
        }
        $(".loadingdiv").remove();
        var json = JSON.parse(data);
        var hasMoreMsgs = false;
        if(json.next_page_uri != null && json.next_page_uri != "null"){
            hasMoreMsgs = true;
            next_page_uri = json.next_page_uri;
        }
        for (var i = 0; i < json.messages.length; i++) {
            var msgObj = json.messages[i];
            addMessageInBox(msgObj, false);
        }
        if (hasMoreMsgs) {
            $('.chatmessages-inner').prepend(`<div  class="prevMessageBtn" onclick="getConvs(0,${hasMoreMsgs})">Load Previous</div>`);
        }
        if (!loadMore) {
             $(".chatmessages-inner").scrollTop($(".chatmessages-inner")[0].scrollHeight);
        }
    }
// https://api.twilio.com/2010-04-01/Accounts/AC7711d81f4dbcd8637bfa30cba8856e5d/Messages/MM587e7da33b8947a2bc9e9eeda34a4b75/Media.json
    function addMessageInBox(msgObj, addAtLast,mediaUrl,fileMeta) {
        // var msgType = msgObj.type;
        msgObj.from = msgObj.from.indexOf('whatsapp') > -1 ? "whatsapp:" + msgObj.from.split(":")[1].replace(/\D/g,'') : msgObj.from;
        msgObj.to = msgObj.to.indexOf('whatsapp') > -1 ? "whatsapp:" + msgObj.to.split(":")[1].replace(/\D/g,'') : msgObj.to;
        Mobile = convId.replace(/\D/g,'');
        if((msgObj.from == "whatsapp:"+Mobile) || (msgObj.to == "whatsapp:"+Mobile)){

            //(msgObj.from == "whatsapp:"+Mobile) || (msgObj.to == "whatsapp:"+Mobile)
            var content = escapeHTMLS(msgObj.body ? msgObj.body : "");
            content = content.replaceAll("\n","<br>");
            var mediaHTML = "";
            var inComing = msgObj.direction === "inbound";
                    if(fromPhoneNumber == null){
                        if(inComing){
                            fromPhoneNumber = msgObj.to;
                        }
                        else{
                            fromPhoneNumber = msgObj.from;
                        }
                    }
                    var dataTime = msgObj.date_sent?msgObj.date_sent:msgObj.date_created;
                    var walimittext = `<a href="https://apps.ulgebra.com/whatsapp-business-api-integration/limitations#h.ytd5bu2d0930" target="_blank">See Why?</a>`;
            var msgHTML = (`<div class="chatmessage-item ${inComing ? 'direction-in' :'direction-out' } msgsts-${msgObj.status} ${msgObj.unread ? 'unread': ''}">
                                <div class="chatmessage-inner">
                                    <div class="chatmessage-content" id="media-${msgObj.sid}">
                                        ${content} 
                                    </div>
                                    <div class="chatmessage-time" title="${new Date(dataTime).toLocaleString()}">
                                        <span class="fullTime">${new Date(dataTime).toLocaleString()}</span>
                                        <span class="smallTime">${new Date(dataTime).toLocaleString()}</span>
                                        ${inComing ? '' : '<span class="msgStatus">'+(msgObj.status=="failed" ? " FAILED to send. "+walimittext : msgObj.status )+'</span>'}
                                    </div>
                                </div>
                            </div>`);
            $(".chatmessages-inner").scrollTop($(".chatmessages-inner")[0].scrollHeight);
            if (addAtLast) {
                $("#MID-LOADING-" + msgObj.sid).remove();
                $('.chatmessages-inner').append(msgHTML);
            } else {
                $('.chatmessages-inner').prepend(msgHTML);
            }
            if(parseInt(msgObj.num_media)> 0){
                if(addAtLast && mediaUrl){
                    $(`#media-${msgObj.sid}`).prepend(getMediaHtml(mediaUrl,fileMeta.type));
                }
                else{
                    $(`#media-${msgObj.sid}`).prepend('<div class="medPlaceHoldr"><span class="spinnow material-icons" style="font-size: 20px;display:inline-block;">toys</span> Loading attachment ...</div>');
                    var request = {
                        url: "https://api.twilio.com"+msgObj.subresource_uris.media,
                        headers: {
                            "Content-Type": "application/x-www-form-urlencoded",
                            "Authorization": 'Basic ' + btoa(appsConfig.ACCOUNT_SID + ":" + appsConfig.AUTH_TOKEN)
                        },
                    };
                    ZOHO.CRM.HTTP.get(request).then(function(resp) {
                      //  {"first_page_uri": "/2010-04-01/Accounts/AC7711d81f4dbcd8637bfa30cba8856e5d/Messages/MM587e7da33b8947a2bc9e9eeda34a4b75/Media.json?PageSize=50&Page=0", "end": 0, "media_list": [{"sid": "MEcc4d2065eb3437106d9b21126c48da3d", "account_sid": "AC7711d81f4dbcd8637bfa30cba8856e5d", "parent_sid": "MM587e7da33b8947a2bc9e9eeda34a4b75", "content_type": "image/png", "date_created": "Fri, 04 Sep 2020 17:16:20 +0000", "date_updated": "Fri, 04 Sep 2020 17:16:20 +0000", "uri": "/2010-04-01/Accounts/AC7711d81f4dbcd8637bfa30cba8856e5d/Messages/MM587e7da33b8947a2bc9e9eeda34a4b75/Media/MEcc4d2065eb3437106d9b21126c48da3d.json"}], "previous_page_uri": null, "uri": "/2010-04-01/Accounts/AC7711d81f4dbcd8637bfa30cba8856e5d/Messages/MM587e7da33b8947a2bc9e9eeda34a4b75/Media.json?PageSize=50&Page=0", "page_size": 50, "start": 0, "next_page_uri": null, "page": 0}
                        resp = JSON.parse(resp);
                        if(resp.media_list && resp.media_list.length){
                            resp.media_list.forEach(function(media){
                                let url = "https://api.twilio.com"+media.uri.slice(0,-5);
                                $(`#media-${msgObj.sid} .medPlaceHoldr`).remove();
                                $(`#media-${msgObj.sid}`).prepend(getMediaHtml(url,media.content_type)+" <br>");
                            });
                        }
                       console.log(resp);
                    });
                }    
            }    
        }    
    }
    function getMediaHtml(url,content_type){
        var mediaHTML = "";
        if(content_type.indexOf("image") != -1){
            mediaHTML = `<img src="${url}"/>`;
        }
        else if(content_type.indexOf("video") != -1){
            mediaHTML = `<video controls src="${url}"/>`;
        }
        else if(content_type.indexOf("audio") != -1){
            mediaHTML = `<audio controls src="${url}"/>`;
        }
        else{
            mediaHTML = `<div class="msg-attch"><span class="material-icons">attachment</span> <a target="_blank" href="${url}">Click to download file</a></div>`;
        }
        return mediaHTML;
    }
    function selectPhoneNumber() {
        $("#input-to-phone").val($("#contact-phone-target").val());
    }

    function selectChannelId() {
        initateChannelId = $("#contact-channel-target").val();
    }

    function openConversation(conversationId) {
        convId = conversationId;
        $('.chatmessages-inner').html("");
        $('.chataction').hide();
        $(".conv-list-item").removeClass('selected');
        $("#list-" + conversationId).addClass('selected');
        $(".chat-no-perm-send").remove();
        $('.chatbox-outer').append('<div class="chat-no-perm-send">You do not have permssion to send message to this channel</div>');
        //$('#existingConvListWindow').hide();
        $('.chataction').show();
        $(".chat-no-perm-send").remove();
        // for(let i=0;i <storedChannelSettings.length;i++){
        //    if(storedChannelSettings[i].channelId && (storedChannelSettings[i].userId == "everyone" || !storedChannelSettings[i].userId ||storedChannelSettings[i].userId == currentUser.users[0].id)){
        //        $('.chataction').show();
        //         $(".chat-no-perm-send").remove();
        //    }
        // }
        getConvs(0);
    }

    window.onload = function() {
        addScript('https://cdn.firebase.com/js/client/2.2.1/firebase.js');
        addScript('https://www.gstatic.com/firebasejs/7.14.1/firebase-app.js');
        addScript('https://www.gstatic.com/firebasejs/7.14.1/firebase-auth.js');
        addScript('https://www.gstatic.com/firebasejs/7.14.1/firebase-firestore.js');
        addScript('https://www.gstatic.com/firebasejs/7.14.1/firebase-database.js');
        $(".chatmessages-inner").on('click', (function() {
            $('.chatmessage-item.unread').removeClass('unread');
        }));
    };

    function addScript(src) {
        var s = document.createElement('script');
        s.setAttribute('src', src);
        document.body.appendChild(s);
    }
    function initiateRTListeners(CID) {
        try{
        if (Object.keys(RTCs).length === 0) {
            // Your web app's Firebase configuration
            var firebaseConfig = {
                apiKey: "AIzaSyClCRLLkwkO8J9V4-4IFCmMHhQkm9WWMpA",
                authDomain: "crmtwilio-726ad.firebaseapp.com",
                databaseURL: "https://crmtwilio-726ad.firebaseio.com",
                projectId: "crmtwilio-726ad",
                storageBucket: "crmtwilio-726ad.appspot.com",
                messagingSenderId: "35161355224",
                appId: "1:35161355224:web:3d5175ab671d7eec2538ad",
                measurementId: "G-G31X03J6MJ"
            };
            // Initialize Firebase
            firebase.initializeApp(firebaseConfig);
        }
        if(RTCs.hasOwnProperty(CID)){
          console.log("RTC already inited "+RTCs[CID]);
          return;
        }
        RTCs[CID] = 0;
            
        var starCountRef = firebase.database().ref('cursors/'+CID+'/ts');
        starCountRef.on('value', function(snapshot) {
            RTCs[CID]++;
            if(RTCs[CID] === 1){
                return;
            }
            newRTEvent(CID, snapshot.val());
        });
        }catch(exc){
            console.log(exc);
        }
    }

    function addNewMessageToWindow(CID, MID, direction) {
        if (fromPhoneNumber !== CID) {
            console.log('Target conv window ' + CID + ' not open...ignoring');
            return;
        }
        // $('.chatmessages-inner').append(`<div class="chatmessage-item ${direction} ${direction == 'direction-in' ? 'unread' : ''}" id="MID-LOADING-${MID}"><div class="chatmessage-inner">
        //                         <div class="chatmessage-content">...</div></div>`);
     
        var request = {

            url: `https://api.twilio.com/2010-04-01/Accounts/${appsConfig.ACCOUNT_SID}/Messages/${MID}.json`,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": 'Basic ' + btoa(appsConfig.ACCOUNT_SID + ":" + appsConfig.AUTH_TOKEN)
            },
        }
        ZOHO.CRM.HTTP.get(request)
            .then(function(data) {
                //"{"body": "Base", "num_segments": "1", "direction": "inbound", "from": "whatsapp:+918012178547", "to": "whatsapp:+14155238886", "date_updated": "Fri, 15 May 2020 17:11:14 +0000", "price": null, "error_message": null, "uri": "/2010-04-01/Accounts/ACd14afb5b4dbcbe6b0e79bcf69fe13a72/Messages/SM19a204d40a824ad583062d2c63485071.json", "account_sid": "ACd14afb5b4dbcbe6b0e79bcf69fe13a72", "num_media": "0", "status": "received", "messaging_service_sid": null, "sid": "SM19a204d40a824ad583062d2c63485071", "date_sent": "Fri, 15 May 2020 17:11:14 +0000", "date_created": "Fri, 15 May 2020 17:11:12 +0000", "error_code": null, "price_unit": "USD", "api_version": "2010-04-01", "subresource_uris": {"media": "/2010-04-01/Accounts/ACd14afb5b4dbcbe6b0e79bcf69fe13a72/Messages/SM19a204d40a824ad583062d2c63485071/Media.json", "feedback": "/2010-04-01/Accounts/ACd14afb5b4dbcbe6b0e79bcf69fe13a72/Messages/SM19a204d40a824ad583062d2c63485071/Feedback.json"}}"
                console.log(data);
                var jsonData = JSON.parse(data);
                jsonData.unread = true;
                addMessageInBox(jsonData, true);
                $(".chatmessages-inner").scrollTop($(".chatmessages-inner")[0].scrollHeight);
            });
    }

    function newRTEvent(CID, MID) {
        addNewMessageToWindow(CID, MID, 'direction-in');
    }
