// var emailContentText = "Hi ,\n\nHope you're well. I'm writing to know how our business can help you.\n\nPlease choose a event to schedule an appointment:\n\n${eventsUrls} See you soon!\n\n %ownerName% \n\n %companyName%";
		var emailContent = [];
		var emailContentText = "";
		var startIndex =0;
		var endIndex = 0;
		var currentEditor="";
		var subject="";
		var sel;
		var range;
		var calendarsMap;
		var recordId;
		var currentRecords;
		var recordModule;
		var ButtonPosition;
		var smsTemplates;
		var templateId;
		var dealContactId;
		var dealAccountId;
		var recipientName;
		var countryCode;
		var moduleFields;
		var userFields;
		var historyFields;
		var users = [];
		var phoneField="Mobile";
		var module_name = "CustomModule";
		var orgId;
		var extensionVersion = "4.0";
		var userEmail = null;
		var defaultModules = ["Contacts","Leads","Accounts"]
		var appsConfig={};
		var attachedfile={};
		var extensionName = 'zoho-crm-twilio';
		var showWhatsAppSandboxNumber = false;
		var isSMS= false;
        document.addEventListener("DOMContentLoaded", function(event) {
        	var firebaseConfig = {
	            apiKey: "AIzaSyDkfrxT-4n_iFi_WDUzi9HIHGS0tCSZ_O8",
	            authDomain: "twilio-e770e.firebaseapp.com",
	            databaseURL: "https://twilio-e770e.firebaseio.com",
	            projectId: "twilio-e770e",
	            storageBucket: "twilio-e770e.appspot.com",
	            messagingSenderId: "971525371883",
	            appId: "1:971525371883:web:4f5e39f2231c9992890bae"
	        };
	            // // Initialize Firebase
            if(document.getElementById("inputFile")){
              firebase.initializeApp(firebaseConfig);
              storageRef = firebase.storage().ref();
            } 
        	ZOHO.embeddedApp.on("PageLoad", function(record) {
               	recordId = record.EntityId;
               	recordModule = record.Entity;
               	ButtonPosition = record.ButtonPosition;
               	Promise.all([ZOHO.CRM.META.getModules(),ZOHO.CRM.API.getOrgVariable("whatsappforzohocrm0__Twilio_Credentials")]).then(function(modulesData){
               		modulesData[0].modules.forEach(function(module){
               			if(recordModule.indexOf("CustomModule") != -1 && recordModule == module.module_name){
               				recordModule = module.api_name;
               			}
               		});
					if(modulesData[1] && modulesData[1].Success && modulesData[1].Success.Content){
						appsConfig = JSON.parse(modulesData[1].Success.Content);
						if(!valueExists(appsConfig.ACCOUNT_SID) || !valueExists(appsConfig.AUTH_TOKEN)){
							document.getElementById("ErrorText").innerText = "Please Enter your Twilio Credentials in Extension settings page.";
    	        			document.getElementById("Error").style.display= "block";
						}
					}    
					    		
                   	ZOHO.CRM.API.getOrgVariable("whatsappforzohocrm0__phonefields").then(function(phoneFieldsData){
    					if(phoneFieldsData && phoneFieldsData.Success && phoneFieldsData.Success.Content){
    						var phoneFieldsSetting =JSON.parse(phoneFieldsData.Success.Content);
            				phoneField = phoneFieldsSetting["phoneField"][recordModule];
            				countryCode = phoneFieldsSetting["countryCode"];
            				if(!phoneField){
            					document.getElementById("ErrorText").innerText = "Please Choose Phone Field in Extension settings page.";
    	        				document.getElementById("Error").style.display= "block";
            				}
    					}
    				});
    				ZOHO.CRM.META.getFields({"Entity":"Users"}).then(function(data){
    					userFields = data.fields;
    					data.fields.forEach(function(field){
    						addListItem("dropdown-menu-user",field.field_label,"dropdown-item","Users."+field.field_label);
    					});
    				});	
    				ZOHO.CRM.API.getAllUsers({Type:"AllUsers"}).then(function(data){
    					users = data.users;
    				});	
                   	ZOHO.CRM.API.searchRecord({Entity:"whatsappforzohocrm0__WhatsApp_Templates",Type:"criteria",Query:"(whatsappforzohocrm0__Module_Name:equals:"+recordModule+")",delay:false})
    				.then(function(data){
    					smsTemplates = data.data;
    					var templateList="";
    					if(data.data){
    						for(let i=0;i <data.data.length;i++){
    							templateList =templateList+ '<li class="templateItem" id="'+data.data[i].id+'" onclick="showsms(this)"></li>';
    						}
    						$('#templateList').append(templateList);
    						for(let i=0;i <data.data.length;i++){
    							document.getElementById(data.data[i].id).innerText = data.data[i].Name;
    						}
    					}
    					else{
    						$('#templateList').append('<li style="text-align:center;">No Templates</li>');
    					}
    				});			
    				selectModule(recordModule);
                    ZOHO.CRM.API.getRecord({Entity:recordModule,RecordID:recordId}).then(function(data2){
    					currentRecords = data2.data;
    					document.getElementById("loader").style.display= "none";
    					document.getElementById("container").style.display= "block";
    				});
               	});	
               

	        });
        	ZOHO.embeddedApp.init().then(function(){
        		var getmap = {"nameSpace":"<portal_name.extension_namespace>"};
                Promise.all([ZOHO.CRM.CONNECTOR.invokeAPI("crm.zapikey",getmap),ZOHO.CRM.CONFIG.getOrgInfo()]).then(function(res){
			        appsConfig.APP_UNIQUE_ID = JSON.parse(res[0]).response;
			        appsConfig.UA_DESK_ORG_ID = res[1].org[0].zgid;
			        renderPhoneNumbersFromSubscription();
			        document.getElementById("loader").style.display= "none";
    				document.getElementById("container").style.display= "block";
			    }).catch(function (err) {
			        console.log(err);
			    });
        	})
        	const el = document.getElementById('emailContentEmail');

			el.addEventListener('paste', (e) => {
			  // Get user's pasted data
			  let data = e.clipboardData.getData('text/html') ||
			      e.clipboardData.getData('text/plain');
			  
			  // Filter out everything except simple text and allowable HTML elements
			  let regex = /<(?!(\/\s*)?()[>,\s])([^>])*>/g;
			  data = data.replace(regex, '');
			  
			  // Insert the filtered content
			  document.execCommand('insertHTML', false, data);

			  // Prevent the standard paste behavior
			  e.preventDefault();
			});
			var content_id = 'emailContentEmail';  
			max = 2000;
			//binding keyup/down events on the contenteditable div
			$('#'+content_id).keyup(function(e){ check_charcount(content_id, max, e); });
			$('#'+content_id).keydown(function(e){ check_charcount(content_id, max, e); });

			function check_charcount(content_id, max, e)
			{   
			    if(e.which != 8 && $('#'+content_id).text().length > max)
			    {
			    	document.getElementById("ErrorText").innerText = "Message should be within 2000 characters.";
	        		document.getElementById("Error").style.display= "block";
	        		// document.getElementById("ErrorText").style.color="red";
					setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
			       // $('#'+content_id).text($('#'+content_id).text().substring(0, max));
			       e.preventDefault();
			    }
			}
			if(document.getElementById("inputFile")){
	          document.getElementById("inputFile").addEventListener("change", (function (event) {
	          	  
	              $('#attachedfile').append(`<div class="loadingdiv"> file is uploading...</div>`);
	              var file = document.getElementById("inputFile").files[0];
	              if (((file.size / 1024) / 1024) > 10) {
	                  $(".loadingdiv").remove();
	                  $('#attachedfile').append(`<div style="background-color:red;" class="loadingdiv"> file size should be within 10 Mb.</div>`);
	                  setTimeout((function(){ $(".loadingdiv").remove(); }), 2000);
	                  return;
	              }
	              if(file.type.indexOf("image") == -1 && file.type.indexOf("audio") == -1 && file.type.indexOf("video") == -1 && file.type.indexOf("pdf") == -1){
	                  $(".loadingdiv").remove();
	                  $('#attachedfile').append(`<div style="background-color:red;" class="loadingdiv"> This file type not supported.</div>`);
	                  setTimeout((function(){ $(".loadingdiv").remove(); }), 2000);
	                  return;
	              }
	              sendfile(file);
	          }));
	        }
        });
		
      
		function renderPhoneNumbersFromSubscription(){
		    if(!appsConfig.UA_DESK_ORG_ID || !appsConfig.APP_UNIQUE_ID){
		        return false;
		    }
		    phoneFetched = true;
		    // $("#WTR-PH-NUMS").addClass('waitToResolve');
		    fetch("https://us-central1-ulgebra-license.cloudfunctions.net/makeTwilioHTTPCall", {
		        "method" :"POST",
		        headers: {
		            'Content-Type': 'application/json;charset=utf-8'
		        },
		        body: JSON.stringify({
		            "url": "https://messaging.twilio.com/v1/Services/{{SERVICE_SID}}/PhoneNumbers",
		            "method": "GET",
		            "orgId": appsConfig.UA_DESK_ORG_ID,
		            "secContext": appsConfig.APP_UNIQUE_ID,
		            "appCode": extensionName
		        })
		    })
		    .then(function (response) {
		        // $("#WTR-PH-NUMS").removeClass('waitToResolve');
		        return response.json();
		    })
		    .then(function (myJson) {
		        console.log(myJson);
		        if(myJson.error){
		            showErroMessage(myJson.error.message);
		            return true;
		       }
		       $(".phoneItems").html("");
		       if(showWhatsAppSandboxNumber){
		            renderPhoneNumber({
		                "phone_number": "+14155238886",
		                "capabilities": [
		                    "WhatsApp Sandbox"
		                ],
		                "country_code": "US",
		                "sid": null
		            });
		       }
		       myJson.data.phone_numbers.forEach((item)=>{
		           console.log(item);
		           let classes = "";
		           item.capabilities.forEach(item=>{
		               classes += "phch-"+item+" ";
		           });
		           renderPhoneNumber(item);
		//           $("#chat_to_phone").append(`<option value="${item.phone_number}">${item.phone_number} ( ${item.country_code} )</option>`);
		       });
		    })
		    .catch(function (error) {
		      console.log("Error: " + error);
		    });
		}
		function renderPhoneNumber(obj){
			$('#numberList').append(`<li class="templateItem" id="${obj.sid}" onclick="chooseChannel('${obj.phone_number}', '${obj.capabilities.join(", ").toLowerCase()}')">${obj.phone_number} (${obj.capabilities.join(", ")})</li>`);
		}
		function chooseChannel(no,channel){
			isSMS = channel.indexOf("sms") != -1?true: channel.indexOf("whatsapp") != -1?false:true;
			appsConfig.Mobile=no;
			document.getElementById("selectedNumber").innerText =  no;
		}
		
        function openUploader(){
	        document.getElementById("inputFile").click();
	    }
	    function sendfile(file){
	        // File or Blob named mountains.jpg
	        // Create the file metadata
	        var metadata = {
	          contentType: file.type,
	        };

	        // Upload file and metadata to the object 'images/mountains.jpg'
	        var uploadTask = storageRef.child(file.name).put(file, metadata);

	        // Listen for state changes, errors, and completion of the upload.
	        uploadTask.on('state_changed', function(snapshot){
	          // Observe state change events such as progress, pause, and resume
	          // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
	          var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
	          console.log('Upload is ' + progress + '% done');
	          // switch (snapshot.state) {
	          //   case firebase.storage.TaskState.PAUSED: // or 'paused'
	          //     console.log('Upload is paused');
	          //     break;
	          //   case firebase.storage.TaskState.RUNNING: // or 'running'
	          //     console.log('Upload is running');
	          //     break;
	          // }
	        }, function(error) {
	            $(".loadingdiv").text(error);
	          // Handle unsuccessful uploads
	        }, function() {
	          // Handle successful uploads on complete
	          // For instance, get the download URL: https://firebasestorage.googleapis.com/...
	          uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
	            console.log('File available at', downloadURL);
	            $(".loadingdiv").remove();
	            attachedfile = {"mediaUrl":downloadURL,"fileMeta":file};
	            $('#attachedfile').append(`<div > ${file.name}</div>`);
	          });
	        });
	    }
		function valueExists(val) {
		    return val !== null && val !== undefined && val.length > 1 && val!=="null";
		}
        function showAnnounceMent(type){
            if(type === "install_chrome_extension"){
                $('body').append(`<div id="announcetip" class="bottomTip" >Automatically Send Single &amp; Bulk WhatsApp messages in background <div class="bottomTipActions"><a href="https://chrome.google.com/webstore/detail/wa-web-for-zoho-crm-bulk/nkihbgbbbakefbgjnokdicilnhcdeckp" target="_blank">Install Google Chrome Plugin </a></div>
                    <span onclick="$('#announcetip').hide()" class="tt-close">X</span>
                </div>`);
            }
             if(type === "purchase_license"){
                $('body').append(`<div id="announcetip" class="bottomTip" >Thank you for installing chrome plugin. <br> <br> Automatically Send Single &amp; Bulk WhatsApp messages in background <div class="bottomTipActions"><a href="https://creator.zohopublic.com/ulgebra/ulgebra-zoho-crm-bulk-whatsapp-plugin-purchase/form-perma/Ulgebra_Zoho_CRM_Bulk_Whatsapp_Plugin_License/DhPCXY73ebeHUBRVCgggEYHDu4H8ZDSure1a7HOp2Ttajdyrw0pDaQkM5FqTfxsFNVvuEAGjm9FQe92ZrTXGVdxMwteM8utKjyOt" target="_blank">Request Access License</a></div>
                    <span onclick="$('#announcetip').hide()" class="tt-close">X</span>
                </div>`);
            }
			/* $('body').append(`<div id="announcetip" class="bottomTip" ><div class="bottomTipActions">Temporarily stopped chrome extension. Will be enabled soon.<a href="https://forms.gle/kXX6wJnyvYLEd2nbA" target="_blank">Submit Feedback</a> </div> </div>`);*/
        }

		function selectModule(module,record){
			document.getElementById("moduleFields").innerText = "Insert "+module+" Fields";
			var customerData = [];
			var phoneFields = [];	
			var phoneFieldsApiNames=[];	
			ZOHO.CRM.META.getFields({"Entity":module}).then(function(data){
				moduleFields = data.fields;
				data.fields.forEach(function(field){
					customerData.push(field.field_label);
				})
				document.getElementById("dropdown-menu-email").innerHTML="";
				customerData.forEach(function(field){
					addListItem("dropdown-menu-email",field,"dropdown-item",module+"."+field);
				});	
			});	
		}
        function updateOrgVariables(apiname,value,key){
    		if(apiname == "whatsappforzohocrm__openwithwebordesktop"){
    			document.getElementById("web").checked = (value=="web");
    			document.getElementById("desktop").checked =(value=="desktop");
    		}
    		ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": apiname,"value": value});
        }
        function showsms(editor){
			for(var i=0; i<smsTemplates.length;i++){
				if(smsTemplates[i].id == editor.id){
					templateId = smsTemplates[i].id;
					document.getElementById("selectedTemplate").innerText = smsTemplates[i].Name;
					document.getElementById("tooltiptext").innerText = smsTemplates[i].Name;
					
					document.getElementById("emailContentEmail").innerText = smsTemplates[i].whatsappforzohocrm0__Message;
					break;
				}
			}
		}
        function checkMobileNumber(Mobile,currentRecord){
			if(!Mobile){
				return Promise.all([getMobileNumber(Mobile,currentRecord)]).then(function(resp){
					if(resp[0].Mobile){
						Mobile = resp[0].Mobile.replace(/\D/g,'');
						var request ={
					        url : "https://rest.messagebird.com/lookup/" + Mobile,
					        headers:{
					              Authorization:"AccessKey 7NMPor0R8DofSHH61SpViNNqQ",
					        }
					    }
					    return ZOHO.CRM.HTTP.get(request).then(function(phoneData){
					    	if(JSON.parse(phoneData).countryPrefix == null){
					    		if(countryCode && countryCode != "0" && Mobile.length > countryCode.length){
									Mobile = countryCode+""+Mobile;
								}
					    	}
					    	return {"Mobile":Mobile,"toModule":resp[0].toModule,"toId":resp[0].toId,"recipientName":resp[0].recipientName};
					    });	
					}
					else{
						return {};
					}    
				});
			}
			else{
				Mobile = Mobile.replace(/\D/g,'');
				var request ={
			        url : "https://rest.messagebird.com/lookup/" + Mobile,
			        headers:{
			              Authorization:"AccessKey 7NMPor0R8DofSHH61SpViNNqQ",
			        }
			    }
			    return ZOHO.CRM.HTTP.get(request).then(function(phoneData){
			    	if(JSON.parse(phoneData).countryPrefix == null){
			    		if(countryCode && countryCode != "0" && Mobile.length > countryCode.length && Mobile.substring(0,countryCode.length) != countryCode){
							Mobile = countryCode+""+Mobile;
						}
			    	}
			    	return {"Mobile":Mobile};
			    });	
			}	
		}
		function getMobileNumber(Mobile,currentRecord){
			if(phoneField.indexOf(".") != -1){
				var toModuleField = phoneField.substring(0,phoneField.indexOf("."));
				var dealphoneField = phoneField.substring(phoneField.indexOf(".")+1);
				if(currentRecord[toModuleField] && currentRecord[toModuleField].id){
					var toId = currentRecord[toModuleField].id ;
					var toModule;
					moduleFields.forEach(function(field){
						if(field.api_name == toModuleField){
							toModule = field.lookup.module.api_name;
						}
					});
					return ZOHO.CRM.API.getRecord({Entity:toModule,RecordID:toId}).then(function(contactData){
						if(contactData.data[0][dealphoneField] != null && contactData.data[0][dealphoneField] != ""){
							if(toModule == "Contacts" || toModule == "Leads" ){
								var recipientName = contactData.data[0].Full_Name;
							}
							else if(toModule == "Accounts"){
								var recipientName = contactData.data[0].Account_Name;
							}	
							return {"Mobile":contactData.data[0][dealphoneField],"toModule":toModule,"toId":toId,"recipientName":recipientName};
						}
						else{
							return {"Mobile":null};
						}
					});	
				}
				else{
					return {"Mobile":null};
				}
			}	
			else{
				return {"Mobile":currentRecord[phoneField]};
			}
		}
		
        function sendSMS(){
        	var sendBulkWhatsAppList={};
        	var MobileNumber;
        	var toModule ;
        	var toId ;
    		if(document.getElementById("emailContentEmail").innerText.length >1600){
    			document.getElementById("ErrorText").innerText = "Message should be within 2000 characters.";
        		document.getElementById("Error").style.display= "block";
        		document.getElementById("Error").style.color="red";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
    		}
        	else if(!attachedfile.mediaUrl && document.getElementById("emailContentEmail").innerText.replace(/\n/g,"").replace(/\t/g,"").replace(/ /g,"") == ""){
        		document.getElementById("ErrorText").innerText = "Message cannot be empty.";
        		document.getElementById("Error").style.display= "block";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
        	}
        	else{
        		var	notes = "\n("+recordModule +" without mobile number will be ignored.)";
	        	document.getElementById("ErrorText").innerText = "Sending... "+notes;
				document.getElementById("Error").style.display= "block";
	        	var message = document.getElementById("emailContentEmail").innerText;
				var successRecords=0;
	        	var recordsLength = currentRecords.length;
	        	var recordIndex = 0;
	        	var failedRecords = 0;
				currentRecords.forEach(function(currentRecord){
					if(defaultModules.indexOf(recordModule) != -1){
        				MobileNumber = currentRecord[phoneField];
        			}
	        		Promise.all([checkMobileNumber(MobileNumber,currentRecord)]).then(function(numberresp){
	        			MobileNumber = numberresp[0].Mobile;
        				toModule = numberresp[0].toModule;
        				toId = numberresp[0].toId;
        				recipientName = numberresp[0].recipientName;
		        		var recordId = currentRecord.id;
		        		var argumentsData = {
							"message" : message,
							"recordModule" : recordModule,
							"templateId":templateId,
							"recordId" : recordId,
							"to":MobileNumber
						};
						var filledMessage = JSON.parse(JSON.stringify(getMessageWithFields(argumentsData,currentRecord)));
						if(!recipientName && currentRecord.Full_Name){
							recipientName = currentRecord.Full_Name;
						}
						if(recipientName){
							var name = "WhatsApp to " + recipientName;
						}
						else{
							var name = "WhatsApp to " + MobileNumber;
						}
						filledMessage = filledMessage.trim();
						var req_data={"Name":name,"Message":filledMessage,"MobileNumber":MobileNumber,"whatsappforzohocrm__Module":recordModule,"whatsappforzohocrm__Recipient_Id":recordId};
						if(filledMessage.length > 2000)
						{
							document.getElementById("ErrorText").innerText = "Message is Too Large.";
			        		document.getElementById("Error").style.display= "block";
							setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
							return ;
						}
						else if(!attachedfile.mediaUrl && filledMessage.length < 1)
						{
							document.getElementById("ErrorText").innerText = "Merge Fields value is empty.";
			        		document.getElementById("Error").style.display= "block";
							setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
							return;
						}
						to = argumentsData.to?argumentsData.to.replace(/\D/g,''):"";
						// console.log("mm"+filledMessage);
						
						if(MobileNumber){
							successRecords=successRecords+1;
							var url = "https://api.twilio.com/2010-04-01/Accounts/" + appsConfig.ACCOUNT_SID + "/Messages.json";

				            req_data.MobileNumber =isSMS?req_data.MobileNumber.replace(/\D/g,''): "whatsapp:+" +  req_data.MobileNumber.replace(/\D/g,'');
				            var body= {
				                "To": req_data.MobileNumber,
			                    "Body":req_data.Message,
			                    "From": isSMS?appsConfig.Mobile.replace(/\D/g,''):"whatsapp:+" + appsConfig.Mobile.replace(/\D/g,'')
				            };
				            if(attachedfile.mediaUrl){
				                body["MediaUrl"]= attachedfile.mediaUrl;
				            }
				            var request = {
				                url: url,
				                headers: {
				                    "Content-Type": "application/x-www-form-urlencoded",
				                    "Authorization": 'Basic ' + btoa(appsConfig.ACCOUNT_SID + ":" + appsConfig.AUTH_TOKEN)
				                },
				                body: body
				            };
				            ZOHO.CRM.HTTP.post(request).then(function(resp) {
				            	if(JSON.parse(resp).status == 400){
					            	failedRecords = failedRecords+1;
					            }	
					            else{
					            	resp = JSON.parse(resp);
					            	if(resp.sid){
									    var channel = "sms";
									    if(resp.from.indexOf("whatsapp:") != -1){
									        channel = "whatsapp";
									    }
										var messageMap = {"Name":resp.sid,"whatsappforzohocrm0__Message":req_data.Message,"whatsappforzohocrm0__From":resp.from.replace(/\D/g,''),"whatsappforzohocrm0__Recipient_Phone":resp.to.replace(/\D/g,''),"whatsappforzohocrm0__Status":"Sent","whatsappforzohocrm0__Direction":"Outgoing","whatsappforzohocrm0__Channel":channel};
										messageMap["whatsappforzohocrm0__"+recordModule.substring(0,recordModule.length-1)]=recordId;
										ZOHO.CRM.API.insertRecord({Entity:"whatsappforzohocrm0__WhatsApp_History",APIData:messageMap,Trigger:["workflow"]});
									}
							    }
					            if(recordsLength == failedRecords){
									document.getElementById("ErrorText").innerHTML = "Mobile field is empty or invalid for all choosen " + argumentsData.recordModule+ ".";
				            		return;
				            	}
				            	recordIndex = recordIndex+1;
				            	if(recordsLength == recordIndex){
				            		document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your Message has been sent successfully.</div>';
									setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload(); }, 1500);
				            	}
		            			
				               
				            });
						}
						else{
							failedRecords = failedRecords+1;
						}
						if(recordsLength == failedRecords){
							document.getElementById("ErrorText").innerHTML = "Mobile field is empty or invalid for all choosen " + argumentsData.recordModule+ ".";
		            	}
					});	
				});
			}		
        }
        function sendWhatsapp(index, url){
        	recordMap =bulkInsertList[index];
        	document.getElementById(index+"_send").style.display="none";
        	document.getElementById(index+"_sending").style.display="inline-block";
        	ZOHO.CRM.API.insertRecord({Entity:"whatsappforzohocrm__WhatsApp_History",APIData:recordMap,Trigger:["workflow"]}).then(function(data){
        	 	document.getElementById(index+"_sending").style.display="none";
        	 	document.getElementById(index+"_sent").style.display="inline-block";
        	 	window.open(url,"_blank");
        	});	
        }
        function getMessageWithFields(messageDetails,currentRecord){
        	var message = JSON.parse(JSON.stringify(messageDetails.message));
			var customerData=[];
			var module = messageDetails.recordModule;
			if(messageDetails.recordModule == "Leads")
			{
				customerData = ["Lead Id","Annual Revenue","City","Company","Country","Created By","Created Time","Description","Designation","Email","Email Opt Out","Fax","First Name","Full Name","Industry","Last Activity Time","Last Name","Lead Source","Lead Status","Mobile","Modified By","Modified Time","No of Employees","Owner","Phone","Rating","Record Image","Salutation","Secondary Email","Skype ID","State","Street","Tag","Twitter","Website","Zip Code"];
			}
			else if(messageDetails.recordModule == "Contacts")
			{
				customerData = ["Contact Id","Account Name","Assistant","Asst Phone","Owner","Created By","Created Time","Date of Birth","Department","Description","Email","Email Opt Out","Fax","First Name","Full Name","Home Phone","Last Activity Time","Last Name","Lead Source","Mailing City","Mailing Country","Mailing State","Mailing Street","Mailing Zip","Mobile","Modified By","Modified Time","Other City","Other Country","Other Phone","Other State","Other Street","Other Zip","Phone","Record Image","Reporting To","Salutation","Secondary Email","Skype ID","Title","Twitter","Vendor Name"];
			}
			moduleFields.forEach(function(field){
				var replace = "\\${"+module+"."+field.field_label+"}";
				var re = new RegExp(replace,"g");
				if(currentRecord[field.api_name] != null)
				{
					var value = currentRecord[field.api_name];
					if(value.name)
					{
						value = value.name;
					}
					
					message = message.replace(re,value);
				}
				else
				{
					message = message.toString().replace(re," ");
				}
			});	
			customerData.forEach(function(field){
				if(field == "Contact Id" || field == "Lead Id" || field == "Account Id" || field == "Deal Id")
				{
					var rfield = "id";
				}
				else
				{
					var rfield = field;
				}
				var replace = "\\${"+module+"."+field+"}";
				var re = new RegExp(replace,"g");
				if(currentRecord[rfield.replace(/ /g,"_")] != null)
				{
					var value = currentRecord[rfield.replace(/ /g,"_") + ""];
					if(value.name)
					{
						value = value.name;
					}
					
					message = message.replace(re,value);
				}
				else
				{
					message = message.toString().replace(re," ");
				}
			});
			if(currentRecord.Owner != null)
			{
				var ownerId = currentRecord.Owner.id;
			}
			if(ownerId != null)
			{
				var currentUser;
				users.forEach(function(user){
					if(user.id == ownerId){
						currentUser = user;
					}
				})
				if(currentUser){
					userFields.forEach(function(field){
						var replace = "\\${Users."+field.field_label+"}";
						var re = new RegExp(replace,"g");
						if(currentUser[field.api_name] != null)
						{
							var value = currentUser[field.api_name];
							if(value.name)
							{
								value = value.name;
							}
							
							message = message.replace(re,value);
						}
						else
						{
							message = message.toString().replace(re," ");
						}
					});
					return message;
				}
			}
			else{
				return message;
			}
			
        }
		function googleTranslateElementInit() {
		  new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
		}
        function addListItem(id,text,className,value){
			if(className == "dropdown-item"){
				var linode = '<li class="'+className+'"><button class="'+className+'" onclick="insert(this)">'+text+'<input type="hidden" value="'+value+'"></button></li>';
			}
			else{
				var linode = '<li class="'+className+'">'+text+'</li>';
			}
			$('#'+id).append(linode);

        }
		function styling(tag)
		{
			document.execCommand(tag);
		}
		function link(){
			$("#linkForm").slideToggle("slow");
		}
		function image(){
			$("#imageForm").slideToggle("slow");
		}
		function addLink(){
			var href = document.getElementById("linkUrl").value;
		    if (range) {
				if(range.startOffset == range.endOffset){
					if(range.commonAncestorContainer.parentNode.href){
						range.commonAncestorContainer.parentNode.href=href;
					}
					else{
						var span = document.createElement('a');
						span.setAttribute('href',href);
						span.innerText = href;
						range.insertNode(span);
			        	range.setStartAfter(span);
			        }	
				}
				else{
					var data = range.commonAncestorContainer.data;
					var start = range.startOffset;
					var end = range.endOffset;
					range.commonAncestorContainer.data="";
					var span = document.createElement('span');
					span.appendChild( document.createTextNode(data.substring(0,start)) );
					var atag = document.createElement('a');
					atag.setAttribute('href',href);
					atag.innerText = data.substring(start,end);
					span.appendChild(atag);
					span.appendChild( document.createTextNode(data.substring(end)) );
					range.insertNode(span);
		        	range.setStartAfter(span);
				}
		        range.collapse(true);
		    }
			$("#linkForm").slideToggle("slow");
		}
		function addImage(){
			var href = document.getElementById("imageUrl").value;
			var span = document.createElement('img');
			span.setAttribute('src',href);
			span.innerText = href;
			range.insertNode(span);
        	range.setStartAfter(span);
			$("#imageForm").slideToggle("slow");
		}
		function openlink(){
			sel = window.getSelection();
		    if (sel && sel.rangeCount) {
		        range = sel.getRangeAt(0);
		      }  
			if(range && range.commonAncestorContainer.wholeText){
				if(range.commonAncestorContainer.parentNode.href){
					document.getElementById("linkUrl").value = range.commonAncestorContainer.parentNode.href;
					$("#linkForm").slideToggle("slow");
				}
			}	
		}
		function insert(bookingLink){
    		// var bookingLink = this;
			var range;

			if (sel && sel.rangeCount && isDescendant(sel.focusNode)){
		        range = sel.getRangeAt(0);
		        range.collapse(true);
    		    var span = document.createElement("span");
    		    span.appendChild( document.createTextNode('${'+bookingLink.children[0].value+'}') );
        		range.insertNode(span);
	    		range.setStartAfter(span);
		        range.collapse(true);
		        sel.removeAllRanges();
		        sel.addRange(range);
		    }    
		}
		function isDescendant(child) {
			var parent = document.getElementById("emailContentEmail");
		     var node = child.parentNode;
		     while (node != null) {
		         if (node == parent || child == parent) {
		             return true;
		         }
		         node = node.parentNode;
		     }
		     return false;
		}
		function enableSchedule(element){
			if(element.checked == true){
				document.getElementById("send").innerText="Schedule";
				var date = document.getElementById("datepicker").value;
    			var time = document.getElementById("timeList").value;
    			scheduledTime = new Date(date+" "+time).toISOString();
			}
			else{
				document.getElementById("send").innerText="Send";
				scheduledTime = undefined;
			}
		}
		function openDatePicker(){
    		document.getElementById("dateTime").style.display= "block";
    		if(ButtonPosition == "DetailView"){
    			document.getElementById("dateTime").style.top= "84%";
    		}
    		else{
    			document.getElementById("dateTime").style.top= "60%";
    		}
    		document.getElementById("Error").style.display= "block";
		}	
		function scheduleClose(){
			var date = document.getElementById("datepicker").value;
    		var time = document.getElementById("timeList").value;
    		if(new Date(date+" "+time).getTime() < new Date().getTime()){
    			document.getElementById("ErrorText").innerText = "Schedule time should be in future.";
    		}
    		else{
    			document.getElementById("ErrorText").innerText = "";
	    		document.getElementById("dateTime").style.display= "none";
	    		document.getElementById("Error").style.display= "none";
	    		document.getElementById("scheduleCheck").checked =true;
	    		document.getElementById("send").innerText="Schedule";
	    		document.getElementById("scheduledDateTime").innerText=new Date(date).toDateString()+" at "+time +" ("+Intl.DateTimeFormat().resolvedOptions().timeZone+")";
	    		scheduledTime = new Date(date+" "+time).toISOString();
	    	}	
		}
		function cancel(){
			document.getElementById("Error").style.display= "none";
		}
		
		
