		var sel;
		var range;
		var recordIds;
		var recordModule;
		var ButtonPosition;
		var smsTemplates;
		var templateId;
		var scheduledTime;
		var func_name;
		var countryCode;
		var moduleFields;
		var userFields;
		var authToken;
		var authId;
		var src;
		var currentRecords =[];
		var users =[];
		var channelId;
		var channelsSettings=[];
        document.addEventListener("DOMContentLoaded", function(event) {
        	var timeList=["12:00 AM","12:15 AM","12:30 AM","12:45 AM","01:00 AM","01:15 AM","01:30 AM","01:45 AM","02:00 AM","02:15 AM","02:30 AM","02:45 AM","03:00 AM","03:15 AM","03:30 AM","03:45 AM","04:00 AM","04:15 AM","04:30 AM","04:45 AM","05:00 AM","05:15 AM","05:30 AM","05:45 AM","06:00 AM","06:15 AM","06:30 AM","06:45 AM","07:00 AM","07:15 AM","07:30 AM","07:45 AM","08:00 AM","08:15 AM","08:30 AM","08:45 AM","09:00 AM","09:15 AM","09:30 AM","09:45 AM","10:00 AM","10:15 AM","10:30 AM","10:45 AM","11:00 AM","11:15 AM","11:30 AM","11:45 AM","12:00 PM","12:15 PM","12:30 PM","12:45 PM","01:00 PM","01:15 PM","01:30 PM","01:45 PM","02:00 PM","02:15 PM","02:30 PM","02:45 PM","03:00 PM","03:15 PM","03:30 PM","03:45 PM","04:00 PM","04:15 PM","04:30 PM","04:45 PM","05:00 PM","05:15 PM","05:30 PM","05:45 PM","06:00 PM","06:15 PM","06:30 PM","06:45 PM","07:00 PM","07:15 PM","07:30 PM","07:45 PM","08:00 PM","08:15 PM","08:30 PM","08:45 PM","09:00 PM","09:15 PM","09:30 PM","09:45 PM","10:00 PM","10:15 PM","10:30 PM","10:45 PM","11:00 PM","11:15 PM","11:30 PM","11:45 PM"];
    		var timeOptions ="";
    		timeList.forEach(function(time){
    			 timeOptions = timeOptions +"<option  value='"+time+"'>"+time+"</option>"
    		});
        	// var cusotmerData = ["Owner", "Email", "$currency_symbol", "Other_Phone", "Mailing_State", "$upcoming_activity", "Other_State", "Other_Country", "Last_Activity_Time", "Department", "$process_flow", "Assistant", "Mailing_Country", "id", "$approved", "Reporting_To", "$approval", "Other_City", "Created_Time", "$editable", "Home_Phone", "$status", "Created_By", "Secondary_Email", "Description", "Vendor_Name", "Mailing_Zip", "$photo_id", "Twitter", "Other_Zip", "Mailing_Street", "Salutation", "First_Name", "Full_Name", "Asst_Phone", "Record_Image", "Modified_By", "Skype_ID", "Phone", "Account_Name", "Email_Opt_Out", "Modified_Time", "Date_of_Birth", "Mailing_City", "Title", "Other_Street", "Mobile", "Last_Name", "Lead_Source", "Tag", "Fax"];
        	ZOHO.embeddedApp.on("PageLoad", function(record) {
               	recordIds = record.EntityId;
               	recordModule = record.Entity;
               	ButtonPosition = record.ButtonPosition;
				selectModule(recordModule);
				ZOHO.CRM.META.getFields({"Entity":"Users"}).then(function(data){
					userFields = data.fields;
					data.fields.forEach(function(field){
						addListItem("dropdown-menu-user",field.field_label,"dropdown-item","Users."+field.field_label);
					});
				});	
				ZOHO.CRM.API.getAllUsers({Type:"AllUsers"}).then(function(data){
					users = data.users;
				});	

				
               	ZOHO.CRM.API.searchRecord({Entity:"messagebirdforzohocrm__MessageBird_Templates",Type:"criteria",Query:"(messagebirdforzohocrm__Module_Name:equals:"+recordModule+")"})
				.then(function(data){
					smsTemplates = data.data;
					var templateList="";
					if(data.data){
						for(let i=0;i <data.data.length;i++){
							templateList =templateList+ '<li class="templateItem" id="'+data.data[i].id+'" onclick="showsms(this)"></li>';
						}
						$('#templateList').append(templateList);
						if(templateList == ""){
							$('#templateList').append('<li style="text-align:center;">No Templates</li>');
						}
						else{
							for(let i=0;i <data.data.length;i++){
								document.getElementById(data.data[i].id).innerText = data.data[i].Name;
							}
						}
					}
					else{
						$('#templateList').append('<li style="text-align:center;">No Templates</li>');
					}	
				}) 
                Promise.all([ZOHO.CRM.API.getRecord({Entity:recordModule,RecordID:recordIds}),ZOHO.CRM.API.getOrgVariable("messagebirdforzohocrm__accessKey"),ZOHO.CRM.API.getOrgVariable("messagebirdforzohocrm__channels"),ZOHO.CRM.CONFIG.getCurrentUser()])
				.then(function(data){
					var apiKey = data[1];
					var channels = data[2];
					if(apiKey && apiKey.Success && apiKey.Success.Content && !valueExists(apiKey.Success.Content)){
						document.getElementById("loader").style.display= "none";
						document.getElementById("container").style.display= "block";
						document.getElementById("ErrorText").innerText = "Please enter your Messageird AccessKey in extension settings page.";
			        	document.getElementById("Error").style.display= "block";
					}
					else if(channels && channels.Success && channels.Success.Content && !valueExists(channels.Success.Content)){
						document.getElementById("loader").style.display= "none";
						document.getElementById("container").style.display= "block";
						document.getElementById("ErrorText").innerText = "Please configure your Messageird channels in extension settings page.";
			        	document.getElementById("Error").style.display= "block";
					}
					else{
						var channelList="";
						var selectedChannelName="";
						channelsSettings = JSON.parse(channels.Success.Content);
						channelsSettings.forEach(function(channel){
							if(channel.channelId && (channel.userId == "everyone" || !channel.userId ||channel.userId == data[3].users[0].id)){
								if(!selectedChannelName){
									selectedChannelName = channel.channelName;
									channelId = channel.channelId;
								}	
								channelList =channelList+ '<li class="templateItem" id="'+channel.channelId+'" onclick="selectChannel(this)">'+channel.channelName+'</li>';
							}
						})
						if(channelList == ""){
							document.getElementById("loader").style.display= "none";
							document.getElementById("container").style.display= "block";
							document.getElementById("ErrorText").innerText = "Please configure your Messageird channels in extension settings page.";
			        		document.getElementById("Error").style.display= "block";
						}
						else{
							$('#channelList').append(channelList);
							document.getElementById("selectedChannel").innerText = selectedChannelName;
							accessKey = apiKey.Success.Content;
							currentRecords = data[0].data;
							if(record.ButtonPosition == "DetailView"){
		            			func_name = "plivosmsforzohocrm__smshandler";
								if(data[0].data[0].Mobile == null || data[0].data[0].Mobile == ""){
									document.getElementById("loader").style.display= "none";
									document.getElementById("container").style.display= "block";
									document.getElementById("ErrorText").innerText = "Mobile field is empty.";
					        		document.getElementById("Error").style.display= "block";
								}
								else{
									var Mobile = data[0].data[0].Mobile;
									Mobile = Mobile.replace(/\D/g,'');
									document.getElementById("editNumber").value =  Mobile;
									var request ={
								        url : "https://rest.messagebird.com/lookup/" + Mobile,
								        headers:{
								              Authorization:"AccessKey "+accessKey,
								        }
								    }
								    ZOHO.CRM.HTTP.get(request).then(function(phoneData){
								    	if(JSON.parse(phoneData).countryPrefix == null){
								    		ZOHO.CRM.API.getOrgVariable("messagebirdforzohocrm__countryCode").then(function(apiKey){
												if(apiKey && apiKey.Success && apiKey.Success.Content && apiKey.Success.Content != "0"){
													countryCode = apiKey.Success.Content;
													Mobile=countryCode+""+Mobile;
													document.getElementById("editNumber").value =  Mobile;
												}
												document.getElementById("loader").style.display= "none";
												document.getElementById("container").style.display= "block";
											});	
								    	}
								    	else{
								    		document.getElementById("loader").style.display= "none";
											document.getElementById("container").style.display= "block";
								    	}
								    });	
								}    
							}
							else{
								ZOHO.CRM.API.getOrgVariable("messagebirdforzohocrm__countryCode").then(function(apiKey){
									if(apiKey && apiKey.Success && apiKey.Success.Content && apiKey.Success.Content != "0"){
										countryCode = apiKey.Success.Content;
									}
									document.getElementById("editNumberui").style.display= "none";
									document.getElementById("loader").style.display= "none";
									document.getElementById("container").style.display= "block";
									func_name = "plivosmsforzohocrm__bulksms";
								});	
							}	
						}	
					}	
				})
	        });
        	ZOHO.embeddedApp.init();
				// $('#timeList').append(timeOptions);
	   //  		$('#datepicker').datepicker().datepicker('setDate',new Date());
	   //  		var date = document.getElementById("datepicker").value;
	   //  		var time = document.getElementById("timeList").value;
	   //  		document.getElementById("scheduledDateTime").innerText=new Date(date).toDateString()+" at "+time +" ("+Intl.DateTimeFormat().resolvedOptions().timeZone+")";
        	const el = document.getElementById('emailContentEmail');

			el.addEventListener('paste', (e) => {
			  // Get user's pasted data
			  let data = e.clipboardData.getData('text/html') ||
			      e.clipboardData.getData('text/plain');
			  
			  // Filter out everything except simple text and allowable HTML elements
			  let regex = /<(?!(\/\s*)?()[>,\s])([^>])*>/g;
			  data = data.replace(regex, '');
			  
			  // Insert the filtered content
			  document.execCommand('insertHTML', false, data);

			  // Prevent the standard paste behavior
			  e.preventDefault();
			});
			var content_id = 'emailContentEmail';  
			max = 2000;
			//binding keyup/down events on the contenteditable div
			$('#'+content_id).keyup(function(e){ check_charcount(content_id, max, e); });
			$('#'+content_id).keydown(function(e){ check_charcount(content_id, max, e); });

			function check_charcount(content_id, max, e)
			{   
			    if(e.which != 8 && $('#'+content_id).text().length > max)
			    {
			    	document.getElementById("ErrorText").innerText = "Message should be within 2000 characters.";
	        		document.getElementById("Error").style.display= "block";
	        		// document.getElementById("ErrorText").style.color="red";
					setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
			       // $('#'+content_id).text($('#'+content_id).text().substring(0, max));
			       e.preventDefault();
			    }
			}
        });

		function selectModule(module){
			var customerData = [];
			document.getElementById("moduleFields").innerText = "Insert "+module+" Fields";
			document.getElementById("dropdown-menu-email").innerHTML="";
			ZOHO.CRM.META.getFields({"Entity":module}).then(function(data){
				moduleFields = data.fields;
				data.fields.forEach(function(field){
					addListItem("dropdown-menu-email",field.field_label,"dropdown-item",module+"."+field.field_label);
				});
			});	
		}
		
        function updateOrgVariables(apiname,value,key){
    		document.getElementById("ErrorText").innerText = "Saving...";
    		document.getElementById("Error").style.display= "block";
    		ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": apiname,"value": value}).then(function(res){
    			document.getElementById("ErrorText").innerText = "Saved";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 500);
    		});
        }
        function showsms(editor){
			for(var i=0; i<smsTemplates.length;i++){
				if(smsTemplates[i].id == editor.id){
					templateId = smsTemplates[i].id;
					document.getElementById("selectedTemplate").innerText = smsTemplates[i].Name;
					document.getElementById("tooltiptext").innerText = smsTemplates[i].Name;
					
					document.getElementById("emailContentEmail").innerText = smsTemplates[i].messagebirdforzohocrm__Message;
					break;
				}
			}
		}
		function selectChannel(channel){
			for(var i=0; i<channelsSettings.length;i++){
				if(channelsSettings[i].channelId == channel.id){
					channelId = channelsSettings[i].channelId;
					document.getElementById("selectedChannel").innerText = channelsSettings[i].channelName;
					break;
				}
			}
		}
		function valueExists(val) {
		    return val !== null && val !== undefined && val.length > 1 && val!=="null";
		}
        function sendSMS(){
        	var MobileNumber = document.getElementById("editNumber").value;
        	var date = document.getElementById("datepicker").value;
    		var time = document.getElementById("timeList").value;
    		if(document.getElementById("emailContentEmail").innerText.length >2000){
    			document.getElementById("ErrorText").innerText = "Message should be within 2000 characters.";
        		document.getElementById("Error").style.display= "block";
        		document.getElementById("Error").style.color="red";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
    		}
    		else if(scheduledTime && new Date(date+" "+time).getTime() < new Date().getTime()){
    			document.getElementById("ErrorText").innerText = "Schedule time should be in future.";
    			document.getElementById("Error").style.display= "block";
    			setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
    		}
        	else if(document.getElementById("emailContentEmail").innerText.replace(/\n/g,"").replace(/\t/g,"").replace(/ /g,"") == ""){
        		document.getElementById("ErrorText").innerText = "Message cannot be empty.";
        		document.getElementById("Error").style.display= "block";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
        	}
        	else if(func_name == "plivosmsforzohocrm__smshandler" && (MobileNumber == null || MobileNumber == "")){
        		document.getElementById("ErrorText").innerText = "Mobile field is empty.";
		        document.getElementById("Error").style.display= "block";
		        setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
        	}
        	else{
        		var notes = "";
        		if(func_name == "plivosmsforzohocrm__bulksms"){
        			MobileNumber = null;
        			notes = "\n("+recordModule +" having empty or invalid mobile number will be ignored.)"
        		}
	        	document.getElementById("ErrorText").innerText = "Sending... "+notes;
				document.getElementById("Error").style.display= "block";
	        	var message = document.getElementById("emailContentEmail").innerText;
	        	var successRecords=0;
	        	var recordsLength = currentRecords.length;
	        	var recordIndex = 0;
	        	currentRecords.forEach(function(currentRecord){
	        		if(func_name == "plivosmsforzohocrm__bulksms"){
	        			MobileNumber = currentRecord.Mobile;
	        			if(MobileNumber){
		        			MobileNumber = MobileNumber.replace(/\D/g,'');

							if(MobileNumber.length > countryCode.length && MobileNumber.substring(0,countryCode.length) != countryCode){
								MobileNumber = countryCode+""+MobileNumber;
							}
						}	
	        		}
	        		var recordId = currentRecord.id;
	        		var argumentsData = {
						"message" : message,
						"recordModule" : recordModule,
						"scheduledTime":scheduledTime,
						"templateId":templateId,
						"recordId" : recordId,
						"to":MobileNumber
					};
					message = getMessageWithFields(argumentsData,currentRecord);
					message = message.trim();
					if(message.length > 2000)
					{
						document.getElementById("ErrorText").innerText = "Message is Too Large.";
		        		document.getElementById("Error").style.display= "block";
						setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
						return ;
					}
					else if(message.length < 1)
					{
						document.getElementById("ErrorText").innerText = "Merge Fields value is empty.";
		        		document.getElementById("Error").style.display= "block";
						setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
						return;
					}
					if(argumentsData.scheduledTime != null)
					{
						time = argumentsData.scheduledTime.substring(0,19) + "+00:00";
						req_data["plivosmsforzohocrm__Scheduled_Time"]=time.toString();
						req_data["plivosmsforzohocrm__Status"]="Scheduled";
					}
					if(!argumentsData.scheduledTime)
					{
					    to = argumentsData.to?argumentsData.to.replace(/\D/g,''):"";
					    // url = "https://whatsapp-sandbox.messagebird.com/v1/conversations/start";
					    url = "https://conversations.messagebird.com/v1/conversations/start";
					    var request ={
					        url : url,
					        body:{
					              "type" : "text",
					              "to": to,
					              "channelId": channelId,
					              "content":{
					                  "text" : message
					              }
					        },
					        headers:{
					              Authorization:"AccessKey "+accessKey,
					        }
					    }
					    ZOHO.CRM.HTTP.post(request).then(function(data){
					        recordIndex = recordIndex+1;
					        if(data && data.toString().indexOf("errors") == -1)
					        {
					            successRecords=successRecords+1;
					            convId = JSON.parse(data).id;
					            var config={Entity:recordModule,APIData:{"id": recordId,"messagebirdforzohocrm__MessageBird_Id":JSON.parse(data).contactId},Trigger:["workflow"]}
					            ZOHO.CRM.API.updateRecord(config);
               					ZOHO.CRM.API.searchRecord({Entity:"messagebirdforzohocrm__MessageBird_Conversations",Type:"criteria",Query:"(messagebirdforzohocrm__Conversation_Id:equals:"+convId+")"})
								.then(function(convers){
						            if(!convers.data){
						            	var channels = JSON.parse(data).channels;
										for(let i=0;i <channels.length;i++){
											if(channels[i].id == channelId){
						            			var platform = channels[i].platformId;
											}
										}
						                var conversationmap = {"Name":platform + " conversation","messagebirdforzohocrm__Conversation_Id":convId,"messagebirdforzohocrm__From":to,"messagebirdforzohocrm__Platform":platform};
						                conversationmap["messagebirdforzohocrm__"+recordModule.substring(0,recordModule.length-1)]=recordId;
						                ZOHO.CRM.API.insertRecord({Entity:"messagebirdforzohocrm__MessageBird_Conversations",APIData:conversationmap}).then(function(response){
						                    var responseInfo  = response.data[0];
						                    var resCode     = responseInfo.code;
						                    if(resCode == 'SUCCESS'){
						                        if(func_name != "plivosmsforzohocrm__bulksms"){
						                        	document.getElementById("Error").style.display= "none";
						                        	document.getElementById("container").style.display= "none";
						                        	document.getElementById("chatHistory").style.display= "block";
						                        	openConversation(convId);
						                            // document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your SMS has been sent successfully.</div>';
						                            // setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload(); }, 1500);
						                        } 
						                    }
						                    else{
						                        if(func_name != "plivosmsforzohocrm__bulksms"){
						                            document.getElementById("ErrorText").innerText = "Opps! Something went wrong from server side. Please try after sometimes!!!";
						                            setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
						                        } 
						                    }
						                });
						                if(recordIndex == recordsLength && func_name == "plivosmsforzohocrm__bulksms"){
						                      document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your SMS has been sent successfully.</div>';
						                      setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload(); }, 1500);
						                }
						            } 
						            else{
						                if(recordIndex == recordsLength){
						                  document.getElementById("Error").style.display= "none";
						                  if(func_name != "plivosmsforzohocrm__bulksms"){
						                  	  document.getElementById("container").style.display= "none";
							                  document.getElementById("chatHistory").style.display= "block";
							                  openConversation(convId);
							               } 
							               else{
							                  document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your SMS has been sent successfully.</div>';
							                  setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload(); }, 1500);
							               }   
						                }
						            }
						        });    
					         } 
					         else{
					              if(recordIndex == recordsLength && func_name == "plivosmsforzohocrm__bulksms"){
					                  if(successRecords == 0){
					                      document.getElementById("ErrorText").innerHTML = "Mobile field is empty or invalid for all choosen " + argumentsData.recordModule+ ".";
					                  }
					                  else{
					                      document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your SMS has been sent successfully.</div>';
					                      setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload(); }, 1500);
					                  }
					              }
					              if(data.toString().indexOf("errors") != -1)
					              {
					                  if(func_name != "plivosmsforzohocrm__bulksms"){
					                      document.getElementById("ErrorText").innerHTML = JSON.parse(data).errors[0].description;
					                      setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
					                  } 
					              }
					              else{
					                  if(func_name != "plivosmsforzohocrm__bulksms"){
					                      document.getElementById("ErrorText").innerText = "Opps! Something went wrong from server side. Please try after sometimes!!!";
					                      setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
					                  } 
					              }
					         }  
					    });	
					}
					else
					{
						ZOHO.CRM.API.insertRecord({Entity:"plivosmsforzohocrm__SMS_History",APIData:req_data,Trigger:["workflow"]}).then(function(response){
							recordIndex = recordIndex+1;
							var responseInfo	= response.data[0];
							var resCode			= responseInfo.code;
							if(resCode == 'SUCCESS'){
								successRecords=successRecords+1;
								if(func_name != "plivosmsforzohocrm__bulksms"){
									document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your SMS has been scheduled successfully.</div>';
									setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload();}, 1500);
								}	
								
							}
							else{
								if(func_name != "plivosmsforzohocrm__bulksms"){
									document.getElementById("ErrorText").innerText = "Opps! Something went wrong from server side. Please try after sometimes!!!";
					        		document.getElementById("Error").style.display= "block";
									setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
								}	
							}
							if(recordIndex == recordsLength && func_name == "plivosmsforzohocrm__bulksms"){
								document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your SMS has been scheduled successfully.</div>';
								setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload();}, 1500);
							}
						});
					}
				});		
			}		
        }
        function getMessageWithFields(messageDetails,currentRecord){
        	var message = messageDetails.message;
			var customerData=[];
			var module = messageDetails.recordModule;
			if(messageDetails.recordModule == "Leads")
			{
				customerData = ["Lead Id","Annual Revenue","City","Company","Country","Created By","Created Time","Description","Designation","Email","Email Opt Out","Fax","First Name","Full Name","Industry","Last Activity Time","Last Name","Lead Source","Lead Status","Mobile","Modified By","Modified Time","No of Employees","Owner","Phone","Rating","Record Image","Salutation","Secondary Email","Skype ID","State","Street","Tag","Twitter","Website","Zip Code"];
			}
			else if(messageDetails.recordModule == "Contacts")
			{
				customerData = ["Contact Id","Account Name","Assistant","Asst Phone","Owner","Created By","Created Time","Date of Birth","Department","Description","Email","Email Opt Out","Fax","First Name","Full Name","Home Phone","Last Activity Time","Last Name","Lead Source","Mailing City","Mailing Country","Mailing State","Mailing Street","Mailing Zip","Mobile","Modified By","Modified Time","Other City","Other Country","Other Phone","Other State","Other Street","Other Zip","Phone","Record Image","Reporting To","Salutation","Secondary Email","Skype ID","Title","Twitter","Vendor Name"];
			}
			moduleFields.forEach(function(field){
				var replace = "\\${"+module+"."+field.field_label+"}";
				var re = new RegExp(replace,"g");
				if(currentRecord[field.api_name] != null)
				{
					var value = currentRecord[field.api_name];
					if(value.name)
					{
						value = value.name;
					}
					
					message = message.replace(re,value);
				}
				else
				{
					message = message.toString().replace(re," ");
				}
			});	
			customerData.forEach(function(field){
				if(field == "Contact Id" || field == "Lead Id" || field == "Account Id" || field == "Deal Id")
				{
					var rfield = "id";
				}
				else
				{
					var rfield = field;
				}
				var replace = "\\${"+module+"."+field+"}";
				var re = new RegExp(replace,"g");
				if(currentRecord[rfield.replace(/ /g,"_")] != null)
				{
					var value = currentRecord[rfield.replace(/ /g,"_") + ""];
					if(value.name)
					{
						value = value.name;
					}
					
					message = message.replace(re,value);
				}
				else
				{
					message = message.toString().replace(re," ");
				}
			});
			if(currentRecord.Owner != null)
			{
				var ownerId = currentRecord.Owner.id;
			}
			if(ownerId != null)
			{
				var currentUser;
				users.forEach(function(user){
					if(user.id == ownerId){
						currentUser = user;
					}
				})
				if(currentUser){
					userFields.forEach(function(field){
						var replace = "\\${Users."+field.field_label+"}";
						var re = new RegExp(replace,"g");
						if(currentUser[field.api_name] != null)
						{
							var value = currentUser[field.api_name];
							if(value.name)
							{
								value = value.name;
							}
							
							message = message.replace(re,value);
						}
						else
						{
							message = message.toString().replace(re," ");
						}
					});
					return message;
				}
			}
			else{
				return message;
			}
			
        }
		function googleTranslateElementInit() {
		  new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
		}
        function addListItem(id,text,className,value){
			if(className == "dropdown-item"){
				var linode = '<li class="'+className+'"><button class="'+className+'" onclick="insert(this)">'+text+'<input type="hidden" value="'+value+'"></button></li>';
			}
			else{
				var linode = '<li class="'+className+'">'+text+'</li>';
			}
			$('#'+id).append(linode);

        }
		function styling(tag)
		{
			document.execCommand(tag);
		}
		function link(){
			$("#linkForm").slideToggle("slow");
		}
		function image(){
			$("#imageForm").slideToggle("slow");
		}
		function addLink(){
			var href = document.getElementById("linkUrl").value;
		    if (range) {
				if(range.startOffset == range.endOffset){
					if(range.commonAncestorContainer.parentNode.href){
						range.commonAncestorContainer.parentNode.href=href;
					}
					else{
						var span = document.createElement('a');
						span.setAttribute('href',href);
						span.innerText = href;
						range.insertNode(span);
			        	range.setStartAfter(span);
			        }	
				}
				else{
					var data = range.commonAncestorContainer.data;
					var start = range.startOffset;
					var end = range.endOffset;
					range.commonAncestorContainer.data="";
					var span = document.createElement('span');
					span.appendChild( document.createTextNode(data.substring(0,start)) );
					var atag = document.createElement('a');
					atag.setAttribute('href',href);
					atag.innerText = data.substring(start,end);
					span.appendChild(atag);
					span.appendChild( document.createTextNode(data.substring(end)) );
					range.insertNode(span);
		        	range.setStartAfter(span);
				}
		        range.collapse(true);
		    }
			$("#linkForm").slideToggle("slow");
		}
		function addImage(){
			var href = document.getElementById("imageUrl").value;
			var span = document.createElement('img');
			span.setAttribute('src',href);
			span.innerText = href;
			range.insertNode(span);
        	range.setStartAfter(span);
			$("#imageForm").slideToggle("slow");
		}
		function openlink(){
			sel = window.getSelection();
		    if (sel && sel.rangeCount) {
		        range = sel.getRangeAt(0);
		      }  
			if(range && range.commonAncestorContainer.wholeText){
				if(range.commonAncestorContainer.parentNode.href){
					document.getElementById("linkUrl").value = range.commonAncestorContainer.parentNode.href;
					$("#linkForm").slideToggle("slow");
				}
			}	
		}
		function insert(bookingLink){
    		// var bookingLink = this;
			var range;

			if (sel && sel.rangeCount && isDescendant(sel.focusNode)){
		        range = sel.getRangeAt(0);
		        range.collapse(true);
    		    var span = document.createElement("span");
    		    span.appendChild( document.createTextNode('${'+bookingLink.children[0].value+'}') );
        		range.insertNode(span);
	    		range.setStartAfter(span);
		        range.collapse(true);
		        sel.removeAllRanges();
		        sel.addRange(range);
		    }    
		}
		function isDescendant(child) {
			var parent = document.getElementById("emailContentEmail");
		     var node = child.parentNode;
		     while (node != null) {
		         if (node == parent || child == parent) {
		             return true;
		         }
		         node = node.parentNode;
		     }
		     return false;
		}
		function enableSchedule(element){
			if(element.checked == true){
				document.getElementById("send").innerText="Schedule";
				var date = document.getElementById("datepicker").value;
    			var time = document.getElementById("timeList").value;
    			scheduledTime = new Date(date+" "+time).toISOString();
			}
			else{
				document.getElementById("send").innerText="Send";
				scheduledTime = undefined;
			}
		}
		function openDatePicker(){
			document.getElementById("ErrorText").innerHTML ="";
    		document.getElementById("dateTime").style.display= "block";
    		if(ButtonPosition == "DetailView"){
    			document.getElementById("dateTime").style.top= "84%";
    		}
    		else{
    			document.getElementById("dateTime").style.top= "60%";
    		}
    		document.getElementById("Error").style.display= "block";
		}	
		function scheduleClose(){
			var date = document.getElementById("datepicker").value;
    		var time = document.getElementById("timeList").value;
    		if(new Date(date+" "+time).getTime() < new Date().getTime()){
    			document.getElementById("ErrorText").innerText = "Schedule time should be in future.";
    		}
    		else{
    			document.getElementById("ErrorText").innerText = "";
	    		document.getElementById("dateTime").style.display= "none";
	    		document.getElementById("Error").style.display= "none";
	    		document.getElementById("scheduleCheck").checked =true;
	    		document.getElementById("send").innerText="Schedule";
	    		document.getElementById("scheduledDateTime").innerText=new Date(date).toDateString()+" at "+time +" ("+Intl.DateTimeFormat().resolvedOptions().timeZone+")";
	    		scheduledTime = new Date(date+" "+time).toISOString();
	    	}	
		}
		function cancel(){
			document.getElementById("Error").style.display= "none";
		}

