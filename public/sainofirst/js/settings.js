// var emailContentText = "Hi ,\n\nHope you're well. I'm writing to know how our business can help you.\n\nPlease choose a event to schedule an appointment:\n\n${eventsUrls} See you soon!\n\n %ownerName% \n\n %companyName%";
		var emailContentForContacts = [];
		var emailContentForLeads = [];
		var emailContentText = "";
		var customVariable = "";
		var startIndex =0;
		var endIndex = 0;
		var currentEditor="";
		var subject="";
		var name="";
		var sel;
		var range;
		var smsTemplates = [];
		var templateSettings;
		var phoneFieldsSetting;
		var credentials={};
		var defaultModules=["Leads","Contacts"];
        document.addEventListener("DOMContentLoaded", async function(event) {
        	ZOHO.embeddedApp.init().then(async function(){
        		getAPIURL();
        		ZOHO.CRM.API.getOrgVariable("sainofirstsmsforzohocrm__Api_Key").then(function(apiKeyData){        			
	        		if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content && apiKeyData.Success.Content != "0"){
	        			credentials = JSON.parse(apiKeyData.Success.Content);
	        			document.getElementById("Token").value=credentials.Token?credentials.Token:"";
	        			document.getElementById("senderid").value=credentials.senderid?credentials.senderid:"";
	        			document.getElementById("route").value=credentials.route?credentials.route:"";
	        		}	
	        	});	
	        	var historyModules=[];
	        	var historyFields = await ZOHO.CRM.META.getFields({"Entity":"sainofirstsmsforzohocrm__SMS_History"});
	        	historyFields.fields.forEach(function(field){
					if(field.data_type == "lookup"){
						if(historyModules.indexOf(field.lookup.module.api_name) == -1){
							historyModules.push(field.lookup.module.api_name);
						}
					}
				});

	        	var DealsPhoneList ='';
	        	var modules=[];
	        	ZOHO.CRM.META.getModules().then(function(userModules){
	        		var fetchList =[];
	        		userModules.modules.forEach(function(module){
	        			if(historyModules.indexOf(module.api_name)!= -1 && module.api_supported && module.visible){
	        				modules.push(module.api_name);
	        			}
	        		});
	        		var fieldListUi="";
	        		modules.forEach(function(module){
	        			fieldListUi= `${fieldListUi} <div  class="singleLine" style="margin-left:25px;padding-top:25px;">
													<div class="textLable">${module}</div>
													<div class="template">
													<div class="dropdown" >
															<button  type="button" class="phoneField"  data-toggle="dropdown"><span style="float:left;width:220px;" class="choose" id="selected${module}Phone">  Choose Phone Field</span>
															<span class="arrowIcon"></span>
															</button>
															<ul class="dropdown-menu" id="${module}PhoneList" style="min-width:252px;">
															</ul>
														</div>
													</div>  
													</div>`;
	        		});
	        		$("#phoneFieldList").append(fieldListUi);
	        		if(modules.indexOf("Deals") != -1 && modules.indexOf("Accounts") == -1){
	        			modules.push("Accounts");
					}
					modules.forEach(function(module){
						fetchList.push(ZOHO.CRM.META.getFields({"Entity":module}));
					})
		        	Promise.all(fetchList).then(function(data){ 
						for(let i=0;i<data.length;i++){
							var phoneList='';
							if(modules[i] == "Contacts" || modules[i] == "Accounts"){
								data[i].fields.forEach(function(field){
									if(field.data_type == "phone"){
										DealsPhoneList =DealsPhoneList+ `<li class="templateItem" onclick="savePhoneFields('Deals','${modules[i]}.${field.api_name}')">${modules[i]} ${field.field_label}</li>`;
									}	
								});	
							}
							data[i].fields.forEach(function(field){
								if(field.data_type == "phone"){
									phoneList =phoneList+ `<li class="templateItem" onclick="savePhoneFields('${modules[i]}','${field.api_name}')">${field.field_label}</li>`;
								}	 
							});	
							if($(`#${modules[i]}PhoneList`)){
								$(`#${modules[i]}PhoneList`).append(phoneList);
							}
						}
						$(`#DealsPhoneList`).append(DealsPhoneList);
						ZOHO.CRM.API.getOrgVariable("sainofirstsmsforzohocrm__phoneFields").then(function(phoneFieldsData){
							if(phoneFieldsData && phoneFieldsData.Success && phoneFieldsData.Success.Content && phoneFieldsData.Success.Content != "0"){
		        				phoneFieldsSetting =JSON.parse(phoneFieldsData.Success.Content);
		        				let phoneFields = phoneFieldsSetting.phoneField;
		        				Object.keys(phoneFields).forEach(function(key){
		        					if(document.getElementById("selected"+key+"Phone")){
		        						document.getElementById("selected"+key+"Phone").innerText = phoneFields[key].replace(/\./g," ").replace(/\_/g," ");;
		        					}
		        				});
		        				document.getElementById("salesSignal").checked = (phoneFieldsSetting.enable == true);
		        			}
		        			ZOHO.CRM.API.searchRecord({Entity:"sainofirstsmsforzohocrm__SMS_Templates",Type:"criteria",Query:"(sainofirstsmsforzohocrm__Module_Name:equals:Events)"})
							.then(function(data){
								smsTemplates = data.data;
								if(data.data){
									var templateList ="";
									for(let i=0;i <data.data.length;i++){ 
										if(phoneFieldsSetting && phoneFieldsSetting.templateId == data.data[i].id){
											document.getElementById("selectedTemplate").innerText = data.data[i].Name;
											document.getElementById("tooltiptext").innerText = data.data[i].Name;
										}
										templateList =templateList+ '<li class="templateItem" id="'+data.data[i].id+'" onclick="saveTemplateVariable(this)"></li>';
										// templateList =templateList+ '<option  value="'+data.data[i].id+'"">'+data.data[i].Name+'</option>';
									}
									$('#templateList').append(templateList);
									if(templateList == ""){
										$('#templateList').append('<li style="text-align:center;">No Templates</li>');
									}
									else{
										for(let i=0;i <data.data.length;i++){ 
											document.getElementById(data.data[i].id).innerText = data.data[i].Name;
										}
									}	
								}
								else{
									$('#templateList').append('<li style="text-align:center;">No Templates</li>');
								}
							});	
						});
					});
					document.getElementById("loader").style.display= "none";
	        		document.getElementById("contentDiv").style.display= "block";
				}); 	
	        });    	
        });
        function getAPIURL(){
   			var func_name = "sainofirstsmsforzohocrm__bulksms";
    		var req_data ={
				"arguments": JSON.stringify({
				"action": "getApiURL",	
				})
			};
			ZOHO.CRM.FUNCTIONS.execute(func_name, req_data)
			.then(function(data){
				document.getElementById("webHookurl").innerText = JSON.parse(data.details.output).webHookURL;
				// if(document.getElementById("incomingurl")){
				// 	document.getElementById("incomingurl").innerText = data.details.output+"&incoming=true";
				// }
				// if(document.getElementById("statusurl")){
				// 	document.getElementById("statusurl").innerText = data.details.output+"&statuscall=true";
				// }
			});
        }
        function savePhoneFields(model,phoneField){
        	phoneFieldsSetting.phoneField[model]=phoneField;
        	if(document.getElementById("selected"+model+"Phone")){
        		document.getElementById("selected"+model+"Phone").innerText = phoneField.replace(/\./g," ").replace(/\_/g," ");;;
        	}
        	updateOrgVariables("sainofirstsmsforzohocrm__phoneFields",phoneFieldsSetting);
        }
        function saveTemplateVariable(editor){
        	for(var i=0; i<smsTemplates.length;i++){
				if(smsTemplates[i].id == editor.id){
					templateId = smsTemplates[i].id;
					document.getElementById("selectedTemplate").innerText = smsTemplates[i].Name;
					document.getElementById("tooltiptext").innerText = smsTemplates[i].Name;
					phoneFieldsSetting["templateId"] = editor.id;
					updateOrgVariables("sainofirstsmsforzohocrm__phoneFields",phoneFieldsSetting);
					break; 
				}
			}
        }
        function updateOrgVariables(apiname,value,key){
			document.getElementById("ErrorText").innerText = "Saving...";
			document.getElementById("Error").style.display= "block";
    		ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": apiname,"value": value}).then(function(res){
    			document.getElementById("ErrorText").innerText = "Saved";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 500);
    		});
        }
        function saveCreds(key){
        	credentials[key]= document.getElementById(key).value;
        	updateOrgVariables("sainofirstsmsforzohocrm__Api_Key",credentials);
        }
        function saveEventSettings(isEnable){
        	phoneFieldsSetting["enable"] = isEnable;
			updateOrgVariables("sainofirstsmsforzohocrm__phoneFields",phoneFieldsSetting);
        }
