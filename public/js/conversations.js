    var recordId;
    var recordModule;
    var ButtonPosition;
    var accessKey;
    var channelId;
    var existingConversations = [];
    var phoneFields = [];
    var RTCs = {

    };
    var platform = "";
    var convId;
    var storedChannelSettings = {};
    var currentUser = {};
    var appsConfig = {
        "ACCOUNT_SID": undefined,
        "AUTH_TOKEN": undefined
    };
    var messages = [];
    var toNumberList = [];
    var Mobile;
    var smsTemplates = [];
    var user;
    var currentRecord;
    var moduleFields=[];
    var userFields =[];
    var next_page_uri = null;
    var fromPhoneNumber = null;
    var phoneNumbers = [];
    var historyFields=[];
    var isAttachmentOpened= false;
    var reply;
    var planName="";
    var extensionVersion;
    var origin = ".com";
    var zsckey="";
    var waMsgCrdleft = 0;
    var convWindowMode = 'whatsapp_web';
    try{
        if(window.location.ancestorOrigins && window.location.ancestorOrigins[0]){
            origin = window.location.ancestorOrigins[0];
            if(origin.indexOf("crmplus") != -1){
                origin=origin.substring("https://crmplus.zoho".length);
            }
            else{
                origin=origin.substring("https://crm.zoho".length);
            }
        }		
    }
    catch(e){
        console.log(e);
    }		
    var curId = 1000;
    var pluginurl="https://platform.zoho"+origin+"/crm/v2/functions/whatsappforzohocrm__sendwhatsapp/actions/execute?auth_type=apikey&zapikey=";
    var editorExtensionId = "nkihbgbbbakefbgjnokdicilnhcdeckp";
    document.addEventListener("DOMContentLoaded", function(event) {
    
        ZOHO.embeddedApp.on("PageLoad", function(record) {
            recordId = record.EntityId;
            recordModule = record.Entity;
            ButtonPosition = record.ButtonPosition;
            $("#prevConversations").html("Loading conversations ...");
            var getmap = {"nameSpace":"<portal_name.extension_namespace>"};
            Promise.all([ZOHO.CRM.API.searchRecord({
                Entity: "whatsappforzohocrm__WhatsApp_Templates",
                Type: "criteria",
                Query: "(whatsappforzohocrm__Module_Name:equals:" + recordModule + ")",
                delay: "false"
            }),ZOHO.CRM.META.getFields({"Entity":recordModule}),ZOHO.CRM.META.getFields({"Entity":"Users"}), ZOHO.CRM.API.getRecord({Entity: recordModule,RecordID: recordId}),ZOHO.CRM.CONNECTOR.invokeAPI("crm.zapikey",getmap),ZOHO.CRM.CONFIG.getCurrentUser(),ZOHO.CRM.CONFIG.getOrgInfo()]).then(function(data) {
                pluginurl = pluginurl+JSON.parse(data[4]).response;
                smsTemplates = data[0].data;
                moduleFields = data[1].fields;
                userFields = data[2].fields;
                currentRecord=data[3].data[0];
                userEmail = data[5].users[0].email;
                zsckey = JSON.parse(data[4]).response;
                orgId = data[6].org[0].zgid;
                var templateList = "";
                var approvedTemplateList = "";
                
                if (smsTemplates) {
                    for (let i = 0; i < smsTemplates.length; i++) {
                        templateList = `${templateList}<option value="${smsTemplates[i].id}" id="${smsTemplates[i].id}"></option>`;
                    }
                    $('#templateList').append(templateList);
                    $('#approvedTemplateList').append(approvedTemplateList);
                    for (let i = 0; i < smsTemplates.length; i++) {
                        document.getElementById(smsTemplates[i].id).innerText = smsTemplates[i].Name;
                    }
                } 
                else {
                    $('#templateList').append(`<option value="" >No Templates</option>`);
                    $('#approvedTemplateList').append(`<option value="" >No WhatsApp Approved Templates</option>`);
                }
                var phoneList="";
                phoneNumbers = [];
                moduleFields.forEach(function(field){
                    if(field.data_type == "phone" && currentRecord[field.api_name] != null && currentRecord[field.api_name] != ""){
                        phoneList = phoneList +'<option value="'+currentRecord[field.api_name]+'">'+currentRecord[field.api_name]+'</option>';
                        if(phoneNumbers.indexOf(currentRecord[field.api_name])== -1){
                            phoneNumbers.push(currentRecord[field.api_name]);
                        }
                    }
                });
                $("#contact-phone-target").append(phoneList);
                if(currentRecord.Mobile != null && currentRecord.Mobile != ""){
                    Mobile = currentRecord.Mobile;
                }
                else{
                    Mobile = currentRecord.Phone;
                }
                
                if(currentRecord.Owner != null)
                {
                    var userFetch =  ZOHO.CRM.API.getUser({ID:currentRecord.Owner.id});
                }
                Promise.all([userFetch]).then(function(apiKeyData) {
                    if(apiKeyData[2]){
                        user = apiKeyData[2].users[0];
                    }
                    executeFirebaseThings();
                });
                ZOHO.CRM.META.getFields({"Entity":"whatsappforzohocrm__WhatsApp_History"}).then(function(data){
                    historyFields = data.fields;
                }); 
            });   
        });
        ZOHO.embeddedApp.init();
        $("#main-message-text").on('keypress', function (e) {
            if (e.keyCode === 13 && !e.shiftKey) {
                if ($("#enterToSendCheck").is(":checked")) {
                    event.preventDefault();
                    sendMessage(false);
                    return false;
                }
            }
        });
    });
    
     function goToWhatsappTab(){
            ZOHO.CRM.UI.Widget.open({Entity:"WhatsApp"})
            .then(function(data){
                console.log(data)
            });
        }
    function selectTemplate(templateId, textAreaId) {
        if(!templateId){
            $("#" + textAreaId).val("");
            return;
        }
        for (var i = 0; i < smsTemplates.length; i++) {
            if (smsTemplates[i].id == templateId) {
                $("#" + textAreaId).val(smsTemplates[i].whatsappforzohocrm__WhatsApp_Message);
                break;
            }
        }
    }
    
    function valueExists(val) {
        return val !== null && val !== undefined && val.length > 1 && val !== "null";
    }

    function getTimeString(time) {
        let msgTime = Date.parse(time);
        let timeSuffix = "am";
        if (isNaN(msgTime) || time.indexOf('Z') < 0) {
            return time;
        }
        let msgHour = new Date(msgTime).getHours();
        timeSuffix = msgHour > 12 ? "pm" : "am";
        msgHour = msgHour > 12 ? msgHour - 12 : msgHour;
        let msgMins = new Date(msgTime).getMinutes();
        msgMins = msgMins < 10 ? "0" + msgMins : msgMins;
        return msgHour + ":" + msgMins + " " + timeSuffix;
    }

    // function initiateRTSForConvs() {
    //     console.log("Trying to initialize initiateRTSForConvs");
    //     if (typeof firebase === undefined) {
    //         console.log("FIREBASE not yet present..");
    //         setTimeout(initiateRTSForConvs, 2000);
    //     } else {
    //         console.log("FIREBASE loaded..");
    //         initiateRTListeners(fromPhoneNumber);
    //     }
    // }
    function openAttachment(){
        isAttachmentOpened = true;
        chrome.runtime.sendMessage(editorExtensionId,{"message":"openAttachment","no":convId},function (response){
            console.log(response);
        });    
    }
    async function renderConversationList() {
        if(!isAutomation){
            var state = await checkState(renderConversationList);
            if(!state){
                return;
            }
        }

        $("#prevConversations").html("");   
        renderPhoneNumbersInConvList();
        
    }

    function renderPhoneNumbersInConvList(){
        $("#prevConversations").html("");
        for (var item in phoneNumbers) {
            var obj = phoneNumbers[item];
            obj = obj.replace(/\D/g,'');
            if(isAutomation){
                initiateRTListeners(obj);  
            }
            else if(planName && planName.indexOf("premium")==0){
                initiateRTListenersPremium(obj);  
            }
            if(convWindowMode === "whatsapp_web"){
                $("#prevConversations").prepend(`<div class="conv-list-item" id="list-${obj}" onclick="openConversation('${obj}')">
                                <div class="conv-channel">
                                   WhatsApp
                                </div>
                                <div class="conv-number">
                                    ${fromPhoneNumber} - ${obj}
                                </div>
                                <div id="conv-channel_refresh">
                                    <svg class="reload_svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-rotate-cw" ><polyline points="23 4 23 10 17 10"></polyline><path d="M20.49 15a9 9 0 1 1-2.12-9.36L23 10"></path></svg>
                                </div>
                            </div>`);
            }
            else{
                $("#prevConversations").prepend(`<div class="conv-list-item" id="list-${obj}" onclick="openConversation('${obj}')">
                                <div class="conv-channel">
                                   WhatCetra
                                </div>
                                <div class="conv-number">
                                    ${obj}
                                </div>
                                <div id="conv-channel_refresh">
                                    <svg class="reload_svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-rotate-cw" ><polyline points="23 4 23 10 17 10"></polyline><path d="M20.49 15a9 9 0 1 1-2.12-9.36L23 10"></path></svg>
                                </div>
                            </div>`);
            }
            
        }
    }
    
    function sendMessage(sendManually){
        if(!sendManually){
            if(waMsgCrdleft <= 0){
                showNoCreditLeftError();
                return;
            }
        }
        var sendBulkWhatsAppList={};
        var Mobile =convId.replace(/\D/g,'');
        // Mobile = Mobile.substring(Mobile.indexOf("(")+1,Mobile.indexOf(")"));
        var text = $("#main-message-text").val().trim();
        if(text.length >2000){
            document.getElementById("ErrorText").innerText = "Message should be within 2000 characters.";
            document.getElementById("Error").style.display= "block";
            document.getElementById("Error").style.color="red";
            setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
        }
        else if(text.replace(/\n/g,"").replace(/\t/g,"").replace(/ /g,"") == ""){
            document.getElementById("ErrorText").innerText = "Message cannot be empty.";
            document.getElementById("Error").style.display= "block";
            setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
        }
        else if(Mobile == null || Mobile == ""){
            document.getElementById("ErrorText").innerText = "Mobile field is empty.";
            document.getElementById("Error").style.display= "block";
            setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
        }
        else{

            var notes = "";
            $('.chatbox-outer').append(`<div class="loadingdiv"> Sending message...</div>`);
            var message = text;
            // message =message.replace(/<b>/g, "*").replace(/<\/b>/g, "*").replace(/<strike>/g, "~").replace(/<\/strike>/g, "~");
            recordId = recordId;
            
            var arguments = {
                "action": "sendSMS",    
                "message" : message,
                "recordModule" : recordModule,
                "recordId" : recordId,
                "to":Mobile
            };
            var recipientName="";
            getMessageWithFields(arguments).then(function(message){
                if(!recipientName && currentRecord.Name){
                    recipientName = currentRecord.Name;
                }
                if(recipientName){
                    var name = "WhatsApp to " + recipientName;
                }
                else{
                    var name = "WhatsApp to " + Mobile;
                }
                var req_data={"Name":name,"whatsappforzohocrm__WhatsApp_Message":message,"whatsappforzohocrm__Sender_Phone":fromPhoneNumber,"whatsappforzohocrm__Recipient_Number":Mobile,"whatsappforzohocrm__Module":recordModule,"whatsappforzohocrm__Recipient_Id":recordId,"whatsappforzohocrm__Status":"Sent"};
               
                historyFields.forEach(function(field){
                    if(field.data_type == "lookup" && field.lookup.module.api_name == recordModule){
                        req_data[field.api_name]=recordId;
                    }
                });
                // if(toModule && toId){
                //     req_data["whatsappforzohocrm__"+toModule]=toId;
                // }
                var isAllowed = waMsgCrdleft > 0;
                ZOHO.CRM.API.insertRecord({Entity:"whatsappforzohocrm__WhatsApp_History",APIData:req_data,Trigger:["workflow"]}).then(function(response){
                    var responseInfo    = response.data[0];
                    var resCode         = responseInfo.code;
                    if(resCode == 'SUCCESS'){
                        if(isAllowed  && (reply || isAutomation)) {
                            var details = responseInfo.details;
                            if(recordModule){
                                var url = "https://crm.zoho.com/crm/tab/"+recordModule+"/"+details.id;
                            }
                            sendBulkWhatsAppList[details.id+""]={"n":req_data.whatsappforzohocrm__Recipient_Number.replace(/\D/g,''),"m":getSanitizedContent(req_data.whatsappforzohocrm__WhatsApp_Message),"dt":new Date().getTime(),"u":url};
                            logSUBActivityLog(1);
                            $.ajax({url: "https://us-central1-wawebbulk.cloudfunctions.net/updateLICCA?src=crm-wa-cui&email="+userEmail+"&mcc=1", success: function(result){
									console.log(result);
							}});
                            if(isAttachmentOpened){
                                isAttachmentOpened= false;
                                sendBulkWhatsAppList["queueIsStopped"]=false;
                            }
                            if(!isAutomation){
                                chrome.runtime.sendMessage(editorExtensionId, sendBulkWhatsAppList,function(response) {
                                    console.log(response);
                                    if (response.status != "success"){
                                        console.log(response);
                                      // handleError(url);
                                    }
                                });
                            }
                            else{
                                addToQueue(sendBulkWhatsAppList);
                            }    
                            $("#main-message-text").val("");
                            $('#templateList').prop('selectedIndex', 0);
                            if(!isAutomation){
                                addMessageInBox({"ty":"chat","t":Date.now(),"m":message,"id":"true_"+Mobile}, true);
                            }
                            else{
                                req_data["id"]=responseInfo.details.id;
                                req_data["Created_Time"]=responseInfo.details.Created_Time;
                                addMessageInBoxFromCRM(req_data, true);
                            }
                            $(".loadingdiv").remove();
                            $(".chatmessages-inner").scrollTop($(".chatmessages-inner")[0].scrollHeight);
                        }
                        else{
                            Mobile = Mobile.replace(/\D/g,'');
                            var url = "https://web.whatsapp.com/send?phone=" + Mobile + "&text=" + encodeURIComponent(message);
                            window.open(url,"_blank");
                            $("#main-message-text").val("");
                            $('#templateList').prop('selectedIndex', 0);
                             addMessageInBox({"m":message,"id":"true_"+Mobile}, true);
                            $(".loadingdiv").remove();
                            $(".chatmessages-inner").scrollTop($(".chatmessages-inner")[0].scrollHeight);
                        }   
                    }
                    else{
                        $("#main-message-text").val("");
                        $('#templateList').prop('selectedIndex', 0);
                        $(".loadingdiv").remove();
                        $(".chatmessages-inner").scrollTop($(".chatmessages-inner")[0].scrollHeight);
                        document.getElementById("ErrorText").innerText = "Opps! Something went wrong from server side. Please try after sometimes!!!";
                        document.getElementById("Error").style.display= "block";
                        setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
                    }
                }); 
            }); 
        }       
    }
    function getSanitizedContent(msg){
        if(extensionVersion){
            $("body").append(`<img style="display:none" src="https://us-central1-slackappp-39d3d.cloudfunctions.net/zdeskww?debug=wawebext-conv::cx-mail:${userEmail}:::-cexv:${extensionVersion.version}::" />`);
        }
        if(extensionVersion && parseFloat(extensionVersion.version) > 8){
            console.log('extension version is > 8');
            return encodeURIComponent(msg);
        }
        else{
            console.log('extension version is not > 8');
            return msg;
        }
    }
    function getMessageWithFields(messageDetails){
        var message = messageDetails.message;
        var to = messageDetails.to.replace(/[^0-9]/g, "").replace(/ /g, "");
        var customerData=[];
        var module = messageDetails.recordModule;
        if(messageDetails.recordModule == "Leads")
        {
            customerData = ["Lead Id","Annual Revenue","City","Company","Country","Created By","Created Time","Description","Designation","Email","Email Opt Out","Fax","First Name","Full Name","Industry","Last Activity Time","Last Name","Lead Source","Lead Status","Mobile","Modified By","Modified Time","No of Employees","Owner","Phone","Rating","Record Image","Salutation","Secondary Email","Skype ID","State","Street","Tag","Twitter","Website","Zip Code"];
        }
        else if(messageDetails.recordModule == "Contacts")
        {
            customerData = ["Contact Id","Account Name","Assistant","Asst Phone","Owner","Created By","Created Time","Date of Birth","Department","Description","Email","Email Opt Out","Fax","First Name","Full Name","Home Phone","Last Activity Time","Last Name","Lead Source","Mailing City","Mailing Country","Mailing State","Mailing Street","Mailing Zip","Mobile","Modified By","Modified Time","Other City","Other Country","Other Phone","Other State","Other Street","Other Zip","Phone","Record Image","Reporting To","Salutation","Secondary Email","Skype ID","Title","Twitter","Vendor Name"];
        }
        else if(messageDetails.recordModule == "Accounts")
        {
            customerData = ["Account Id","Account Name","Account Number","Account Site","Account Type","Annual Revenue","Billing City","Billing Code","Billing Country","Billing State","Billing Street","Created By","Created Time","Description","Employees","Fax","Industry","Last Activity Time","Modified By","Modified Time","Owner","Ownership","Parent Account","Phone","Rating","Record Image","SIC Code","Shipping City","Shipping Code","Shipping Country","Shipping State","Shipping Street","Tag","Ticker Symbol","Website"];
        }
        else if(messageDetails.recordModule == "Deals")
        {
            customerData = ["Deal Id","Account Name","Amount","Campaign Source","Closing Date","Contact Name","Created By","Created Time","Deal Name","Description","Expected Revenue","Last Activity Time","Lead Conversion Time","Lead Source","Modified By","Modified Time","Next Step","Overall Sales Duration","Owner","Probability","Sales Cycle Duration","Stage","Tag","Type"];
        }
        moduleFields.forEach(function(field){
            var replace = "\\${"+module+"."+field.field_label+"}";
            var re = new RegExp(replace,"g");
            if(currentRecord[field.api_name] != null)
            {
                var value = currentRecord[field.api_name];
                if(value.name)
                {
                    value = value.name;
                }
                
                message = message.replace(re,value);
            }
            else
            {
                message = message.toString().replace(re," ");
            }
        }); 
        customerData.forEach(function(field){
            if(field == "Contact Id" || field == "Lead Id" || field == "Account Id" || field == "Deal Id")
            {
                var rfield = "id";
            }
            else
            {
                var rfield = field;
            }
            var replace = "\\${"+module+"."+field+"}";
            var re = new RegExp(replace,"g");
            if(currentRecord[rfield.replace(/ /g,"_")] != null)
            {
                var value = currentRecord[rfield.replace(/ /g,"_") + ""];
                if(value.name)
                {
                    value = value.name;
                }
                
                message = message.replace(re,value);
            }
            else
            {
                message = message.toString().replace(re," ");
            }
        });
        if(currentRecord.Owner != null)
        {
            var ownerId = currentRecord.Owner.id;
        }
        if(ownerId != null)
        {
            return ZOHO.CRM.API.getUser({ID:ownerId}).then(function(data){
                if(data != "")
                {
                    var user = data.users[0];
                    userFields.forEach(function(field){
                        var replace = "\\${Users."+field.field_label+"}";
                        var re = new RegExp(replace,"g");
                        if(user[field.api_name] != null)
                        {
                            var value = user[field.api_name];
                            if(value.name)
                            {
                                value = value.name;
                            }
                            
                            message = message.replace(re,value);
                        }
                        else
                        {
                            message = message.toString().replace(re," ");
                        }
                    });
                    return message;
                }
            }); 
        }
        else{
            return message;
        }
    }

    function escapeHTMLS(rawStr) {
        return $('<textarea/>').text(rawStr).html();
    }

    function getConvs(offset,loadMore) {
        $("#initiateConvWindow").hide();
        //              $('#existingConvListWindow').show();
        $("#existingConvWindow").show();
       var limit = 3;
       if(offset!==0){
          $(`#loadmore-off-${offset}`).html('Loading...');
      }
      if(offset === 0){
        $('.chatbox-outer').append(`<div class="loadingdiv"> Loading Messages...</div>`);
      }
        // if(loadMore && next_page_uri){
        //     url = next_page_uri;
        // }
        // else{
        //     url = "https://api.twilio.com/2010-04-01/Accounts/" + appsConfig.ACCOUNT_SID + "/Messages.json?PageSize=1000";
        // }
        Mobile = convId.replace(/\D/g,'');
        if(!isAutomation){
            localStorage.setItem("zww_id",Mobile+"@c.us");
            localStorage.setItem("zww_action","getChats_"+Date.now());
            // chrome.runtime.sendMessage(editorExtensionId, { "message": "getChats","no": Mobile,"scroll":offset},function (data){
            //     console.log(data);

                // renderDataFromJSON(offset, limit, data);
                // var r = confirm("Save Chats to Zoho CRM WhatsApp History Module!");
                // if (r == true) {
                //   saveChats(data);
                // } else {
                //   console.log("no");
                // }
            // }); 
        }
        else{
            criteria = "((whatsappforzohocrm__Sender_Phone:equals:"+fromPhoneNumber+")and(whatsappforzohocrm__Recipient_Number:equals:"+convId+"))or((whatsappforzohocrm__Sender_Phone:equals:"+convId+")and(whatsappforzohocrm__Recipient_Number:equals:"+fromPhoneNumber+"))"
            ZOHO.CRM.API.searchRecord({Entity:"whatsappforzohocrm__WhatsApp_History",Type:"criteria",Query:criteria,delay:"false"},(offset+1)+"","100").then(function(data){
                console.log(data);
                renderDataFromJSONforCRM(offset, limit, data);
            })
        }
    }
    window.addEventListener('storage', (event) => {
        console.log(event);
        if(event.key.indexOf("zww_chatsLoaded") == 0 && localStorage.getItem("zww_chatsLoaded")){
            var chats = localStorage.getItem("zww_chats")?JSON.parse(localStorage.getItem("zww_chats")):[];
            renderDataFromJSON(0, 0, chats);
        }
    });
    function saveChats(chats){
        console.log(chats);
        // var req_data={"Name":name,"whatsappforzohocrm__WhatsApp_Message":filledMessage,"whatsappforzohocrm__Recipient_Number":MobileNumber,"whatsappforzohocrm__Module":recordModule,"whatsappforzohocrm__Recipient_Id":recordId,"whatsappforzohocrm__Status":"Success"};
        // ZOHO.CRM.API.insertRecord({Entity:"whatsappforzohocrm__WhatsApp_History",APIData:bulkInsertList,Trigger:["workflow"]}).then(function(data){
    }
    function renderDataFromJSONforCRM(offset, limit,data) {
        if(offset === 0){
            $('.chatmessages-inner').html("");
        }
        if(offset!==0){
            $(`#loadmore-off-${offset}`).html('').addClass('loadedLoadmore').prop("onclick", null).off("click");
        }
        $(".loadingdiv").remove();
        var json  = data;
        if(!json){
            json=[];
        }
        hasMoreMsgs = true;
        for (var i = json.length-1; i >= 0; i--) {
            var msgObj = json[(json.length-1)-i];
            if(msgObj.whatsappforzohocrm__WhatsApp_Message){
                addMessageInBoxFromCRM(msgObj, false)
            }
        }
        if(hasMoreMsgs){
            $('.chatmessages-inner').prepend(`<div id="loadmore-off-${offset+1}" class="prevMessageBtn" onclick="getConvs(${offset+1})">Load Previous</div>`);
        }
        if(offset===0){
            $(".chatmessages-inner").scrollTop($(".chatmessages-inner")[0].scrollHeight);
        }else if($(`#loadmore-off-${offset}`).length){
            $(".chatmessages-inner").scrollTop($(`#loadmore-off-${offset}`).offset().top-150);
        }
    }
    function renderDataFromJSON(offset, limit,data) {
        if(offset!==0){
            $(`#loadmore-off-${offset}`).html('').addClass('loadedLoadmore').prop("onclick", null).off("click");
        }
        $('.chatmessages-inner').html("");
        $(".loadingdiv").remove();
        // var json = JSON.parse(data); 
        var json  = data;
        if(!json){
            json=[];
        }
        hasMoreMsgs = true;
        for (var i = json.length-1; i >= 0; i--) {
            var msgObj = json[i];
            addMessageInBox(msgObj, false);
        }
        if(hasMoreMsgs){
            $('.chatmessages-inner').prepend(`<div id="loadmore-off-${offset+limit}" class="prevMessageBtn" onclick="getConvs(${offset+limit})">Load Previous</div>`);
        }
        if(offset===0){
            $(".chatmessages-inner").scrollTop($(".chatmessages-inner")[0].scrollHeight);
        }else if($(`#loadmore-off-${offset}`).length){
            $(".chatmessages-inner").scrollTop($(`#loadmore-off-${offset}`).offset().top-150);
        }
    }
    function addMessageInBoxFromCRM(msgObj, addAtLast,mediaUrl,fileMeta) {
            var content = escapeHTMLS(msgObj.whatsappforzohocrm__WhatsApp_Message ? msgObj.whatsappforzohocrm__WhatsApp_Message : "");
            content = content.replace("\n","<br>");
            var mediaHTML = "";
            var inComing = msgObj.whatsappforzohocrm__Recipient_Number  == fromPhoneNumber;
            var status = inComing?"Received":"Sent";
            status = msgObj.whatsappforzohocrm__Status?msgObj.whatsappforzohocrm__Status:status;
                    var dataTime = msgObj.Created_Time;
                    
            var msgHTML = (`<div class="chatmessage-item ${inComing ? 'direction-in' :'direction-out' } ${addAtLast? 'unread': '' }">
                                <div class="chatmessage-inner">
                                    <div class="chatmessage-content" id="media-${msgObj.id}">
                                        ${mediaHTML}${content} 
                                    </div>
                                    <div class="chatmessage-time" title="${dataTime}">
                                        <span class="fullTime">${dataTime}</span>
                                        <span class="smallTime">${getTimeString(dataTime)}</span>
                                        ${inComing ? '' : '<span class="msgStatus">'+(status)+'</span>'}
                                    </div>
                                </div>
                            </div>`);
            $(".chatmessages-inner").scrollTop($(".chatmessages-inner")[0].scrollHeight);
            if (addAtLast) {
                if(document.getElementById("MID-LOADING-" + msgObj.id)){
                    document.getElementById("MID-LOADING-" + msgObj.id).remove();
                } 
                $('.chatmessages-inner').append(msgHTML);
            } else {
                $('.chatmessages-inner').prepend(msgHTML);
            }
    }
    function addMessageInBoxPremium(msgObj, addAtLast,mediaUrl,fileMeta) {
        var content = escapeHTMLS(msgObj.text ? msgObj.text : "");
        content = content.replace("\n","<br>");
        var mediaHTML = "";
        var inComing = msgObj.to  == fromPhoneNumber;
        var status = inComing?"Received":"Sent";
        var dataTime = new Date();

                
        var msgHTML = (`<div class="chatmessage-item ${inComing ? 'direction-in' :'direction-out' } ${addAtLast? 'unread': '' }">
                            <div class="chatmessage-inner">
                                <div class="chatmessage-content" id="media-${msgObj.id}">
                                    ${mediaHTML}${content} 
                                </div>
                                <div class="chatmessage-time" title="${dataTime}">
                                    <span class="fullTime">${dataTime}</span>
                                    <span class="smallTime">${getTimeString(dataTime)}</span>
                                    ${inComing ? '' : '<span class="msgStatus">'+(status)+'</span>'}
                                </div>
                            </div>
                        </div>`);
        $(".chatmessages-inner").scrollTop($(".chatmessages-inner")[0].scrollHeight);
        if (addAtLast) {
            if(document.getElementById("MID-LOADING-" + msgObj.id)){
                document.getElementById("MID-LOADING-" + msgObj.id).remove();
            } 
            $('.chatmessages-inner').append(msgHTML);
        } else {
            $('.chatmessages-inner').prepend(msgHTML);
        }
    }
    function addMessageInBox(msgObj, addAtLast,mediaUrl,fileMeta) {
        if(msgObj.ty === "image" || msgObj.ty === "chat"){
            if(Mobile && msgObj.id && msgObj.id.indexOf(Mobile) !== -1){
                var content = escapeHTMLS(msgObj.m ? msgObj.m : "");
                content = content.replace("\n","<br>");
                var mediaHTML = "";
                var inComing = msgObj.id.indexOf("false_") != -1;
                var status = inComing?"Received":"Sent";
                        var dataTime = new Date(parseInt(msgObj.t)).toLocaleTimeString().toLowerCase();
                                   // var msgHTML = (`<div class="chatmessage-item ${inComing ? 'direction-in' :'direction-out' } ${msgObj.unread ? 'unread': ''}">
                        if(msgObj.ty === "image"){
                            content="";
                            mediaHTML = `<img src="${msgObj.src}" style="height: 20;"/>`;
                            // mediaHTML = `<a href="${msgObj.src2}" target="_blank"><img src="${msgObj.src}" style="height: 20;"/></a>`;
                        }
                var msgHTML = (`<div class="chatmessage-item ${inComing ? 'direction-in' :'direction-out' } ${addAtLast? 'unread': '' }">
                                    <div class="chatmessage-inner">
                                        <div class="chatmessage-content" id="media-${msgObj.id}">
                                            ${mediaHTML}${content} 
                                        </div>
                                        <div class="chatmessage-time" title="${dataTime}">
                                            <span class="fullTime">${dataTime}</span>
                                            <span class="smallTime">${(dataTime)}</span>
                                            ${inComing ? '' : '<span class="msgStatus">'+(status)+'</span>'}
                                        </div>
                                    </div>
                                </div>`);
                $(".chatmessages-inner").scrollTop($(".chatmessages-inner")[0].scrollHeight);
                if (addAtLast) {
                    if(document.getElementById("MID-LOADING-" + msgObj.id)){
                        document.getElementById("MID-LOADING-" + msgObj.id).remove();
                    } 
                    $('.chatmessages-inner').append(msgHTML);
                } else {
                    $('.chatmessages-inner').prepend(msgHTML);
                }
            }
        }    
    }
    function getMediaHtml(url,content_type){
        var mediaHTML = "";
        if(content_type.indexOf("image") != -1){
            mediaHTML = `<img src="${url}"/>`;
        }
        else if(content_type.indexOf("video") != -1){
            mediaHTML = `<video controls src="${url}"/>`;
        }
        else if(content_type.indexOf("audio") != -1){
            mediaHTML = `<audio controls src="${url}"/>`;
        }
        else{
            mediaHTML = `<div class="msg-attch"><span class="material-icons">attachment</span> <a target="_blank" href="${url}">Click to download file</a></div>`;
        }
        return mediaHTML;
    }
    function selectPhoneNumber() {
        $("#input-to-phone").val($("#contact-phone-target").val());
    }

    function selectChannelId() {
        initateChannelId = $("#contact-channel-target").val();
    }

    function openConversation(conversationId) {
        convId = conversationId;
        convId = convId.replace(/\D/g,'');
        $('#singleWCChatLoadWindow').hide();
        $('.chatmessages-inner').html("");
        $('.chataction').hide();
        $(".conv-list-item").removeClass('selected');
        $("#list-" + conversationId).addClass('selected');
        if(convWindowMode === "whatcetra"){
            showWhatCetraConvWindow(conversationId);
            return;
        }
        $(".chat-no-perm-send").remove();
        $('.chatbox-outer').append('<div class="chat-no-perm-send">You do not have permssion to send message to this channel</div>');
       $('#existingConvWindow').css({'display': 'inline-block'});
        $('.chataction').show();
        $(".chat-no-perm-send").remove();
        // for(let i=0;i <storedChannelSettings.length;i++){
        //    if(storedChannelSettings[i].channelId && (storedChannelSettings[i].userId == "everyone" || !storedChannelSettings[i].userId ||storedChannelSettings[i].userId == currentUser.users[0].id)){
        //        $('.chataction').show();
        //         $(".chat-no-perm-send").remove();
        //    }
        // }
        getConvs(0);
    }

    function showWhatCetraConvWindow(phoneNo){
        $('#existingConvWindow').hide();
        $("#singleWCChatLoadWindow").attr({'src': `https://app.whatcetra.com/?contactNumber=${phoneNo}&embed=true#Chat`}).css({'display': 'inline-block'});
    }

    function changeWindowMode(){
        if(convWindowMode === "whatsapp_web"){
            convWindowMode = "whatcetra";
            $("#conv_windowmode").text("WhatCetra");
            $("#conv_windowmode_btn").text("View WhatsApp Chats");
        }
        else{
            convWindowMode = "whatsapp_web";
            $("#conv_windowmode").text("WhatsApp Web");
            $("#conv_windowmode_btn").text("View WhatCetra Chats");
        }
        renderPhoneNumbersInConvList();
        $('#existingConvWindow').hide();
    }

    window.onload = function() {
        $(".chatmessages-inner").on('click', (function() {
            $('.chatmessage-item.unread').removeClass('unread');
        }));
    };

    function addScript(src) {
        var s = document.createElement('script');
        s.setAttribute('src', src);
        document.body.appendChild(s);
    }
    function initiateRTListeners(CID) {
       
        if(RTCs.hasOwnProperty(CID)){
          console.log("RTC already inited "+RTCs[CID]);
          return;
        }
        RTCs[CID] = 0;
        var user = secondaryApp.auth().currentUser;
        if(user){
            var starCountRef = database.ref("users/"+user.uid+"/numbers/"+atno+"/incoming/"+CID);
            starCountRef.on('value', function(snapshot) {
                RTCs[CID]++;
                if(RTCs[CID] === 1){
                    return;
                }
                newRTEvent(CID, snapshot.val());
            });
        }    
    }
    function initiateRTListenersPremium(CID){
        if(RTCs.hasOwnProperty(CID)){
          console.log("RTC already inited "+RTCs[CID]);
          return;
        }
        RTCs[CID] = 0;
        let zkey =zsckey.replaceAll(".","");
        var starCountRef = premiumDatabase.ref("wweb-incoming/"+zkey+"/"+CID+"::"+fromPhoneNumber);
        starCountRef.on('value', function(snapshot) {
            RTCs[CID]++;
            if(RTCs[CID] === 1){
                return;
            }
            newRTEvent(CID, snapshot.val());
        });
          
    }
    function doIncomingThings(){
        var firebaseConfig = {
            apiKey: "AIzaSyDsvl4lleRa8k-3UuDqltueXZy1V27f0Sw",
            authDomain: "ulgebra-license.firebaseapp.com",
            databaseURL: "https://ulgebra-license.firebaseio.com",
            projectId: "ulgebra-license",
            storageBucket: "ulgebra-license.appspot.com",
            messagingSenderId: "356364764905",
            appId: "1:356364764905:web:4bea988d613e73e1805278",
            measurementId: "G-54FN510HDW"
        };
        // Initialize Firebase
        premiumApp = firebase.initializeApp(firebaseConfig,"secondary");
        premiumDatabase = premiumApp.database();
    }
    function addNewMessageToWindow(CID, MID, direction) {
        if(isAutomation){
            if (fromPhoneNumber !== MID.whatsappforzohocrm__Recipient_Number || MID.whatsappforzohocrm__Sender_Phone != convId) {
                console.log('Target conv window ' + CID + ' not open...ignoring');
                return;
            }
            $('.chatmessages-inner').append(`<div class="chatmessage-item ${direction} ${direction == 'direction-in' ? 'unread' : ''}" id="MID-LOADING-${MID.id}"><div class="chatmessage-inner">
                                    <div class="chatmessage-content">...</div></div>`);
            addMessageInBoxFromCRM(MID,true);
            $(".chatmessages-inner").scrollTop($(".chatmessages-inner")[0].scrollHeight);
        }
        else if(planName && planName.indexOf("premium") == 0){
            MID.id = new Date().getTime();
            if (fromPhoneNumber !== MID.to || MID.from != convId) {
                console.log('Target conv window ' + CID + ' not open...ignoring');
                return;
            }
            $('.chatmessages-inner').append(`<div class="chatmessage-item ${direction} ${direction == 'direction-in' ? 'unread' : ''}" id="MID-LOADING-${MID.id}"><div class="chatmessage-inner">
                                    <div class="chatmessage-content">...</div></div>`);
            addMessageInBoxPremium(MID,true);
            $(".chatmessages-inner").scrollTop($(".chatmessages-inner")[0].scrollHeight);
        }
    }

    function newRTEvent(CID, MID) {
        addNewMessageToWindow(CID, MID, 'direction-in');
    }

    function executeFirebaseThings(){
        try{

            firebase.initializeApp(firebaseConfig);
            
            //firebase.analytics();
            db = firebase.firestore();

            db.collection('LicenseUsers').doc(userEmail).get().then((data)=>{
                if(!data.exists){
						fetchLICDetails(userEmail, showNewPlanDetails);
						return;
					}
                peySts = data.data().type;
                $('.bottomTipActions').prepend(' <b> '+peySts.substring(0, peySts.indexOf("-")).toUpperCase()+' </b> plan ');
                isAllowed = data.data().type.indexOf("basic") == 0 || data.data().type.indexOf("classic") == 0;
                planName = data.data().type;

                if(data.data().type.indexOf("business")==0 || data.data().type.indexOf("premium")==0 || data.data().type.indexOf("classic")==0){
							$.ajax({url: "https://us-central1-wawebbulk.cloudfunctions.net/checkUALIC?src=crm-wa-bulk&email="+userEmail, success: function(result){
							if(result){
								if(result.mcn > 0){
                                    waMsgCrdleft = result.mcn;
									$('.bottomTipActions').prepend('<span class="oldlicban">credit remaining : <b>'+(result.mcn)+" </b> &nbsp;&nbsp;</span>");
								}
							}
							if(result && result.mcn <=0) {
								document.write(`You have sent all messages which was allowed in this plan. Kindly request more credits to continue. <a href="https://apps.ulgebra.com/contact" target="_blank">Contact Now</a> <br><br> <a href="https://apps.ulgebra.com/zoho/crm/whatsapp-web/pricing" target="_blank">View Pricing Plans</a> `);
							}
							 //  executeFirebaseThings();
    					    if(result && result.status == "valid"){
    					    	isAllowed = true;
    					    }
    					}});
					}

                if(data.data().type.indexOf("business")==0 || data.data().type.indexOf("premium")==0){
                    attPur = true;
                    isAllowed = true;

                }else{
                    showNoHighPlanError();
                }
                if(data.data().type.indexOf("premium")==0){
                    doIncomingThings();
                }
                if(data.data().type.indexOf("automation")==0){
                    attPur = true;
                    isAllowed = true;
                    isAutomation = true;
                    doAutomationThings();
                } 
                else{
                    //renderConversationList();
                    // checkVersion();
                }
                if(!isAllowed){
							fetchLICDetails(userEmail, showNewPlanDetails);
				}
            }).catch((err)=>{
                showNoHighPlanError();
                $('.bottomTipActions').prepend(' <b> Basic </b> plan ');
            });

            db.collection('appActivityLog').doc(userEmail).collection('history').add({
                't' : Date.now(),
                'a': 'WWBULK-CONV',
                'cev' : extensionVersion && extensionVersion.version ? extensionVersion.version : '-'
            });
        }
        catch(err){
            console.log(err);
            $('.bottomTipActions').prepend(' <b> Basic </b> plan ');
            loadDebugInfo(":unable-to-fetch-firebase:"+userEmail);
        }

        try{

            db.collection('appActivityLog').doc(userEmail).get().then((data)=>{ 
                data = data.data();
                if(data === undefined){
                    db.collection('appActivityLog').doc(userEmail).get().then((data)=>{ 
                        data = data.data();
                    });
                }
            });

        }
        catch(exx){
            console.log(exx);
        }
    }

    function checkVersion(){
        if(!!window.chrome){
            chrome.runtime.sendMessage(editorExtensionId, { "message": "version" },function (reply2){
                console.log(reply2);
                reply = reply2;
                extensionVersion = reply;
                if(extensionVersion){
                    checkWhatcetraVersion();
                    // setTimeout(function(){
                    //     renderConversationList();
                    // },2000);
                }
                else{
                    checkVersionLatest();
                }
            }); 
        }
        else{
            showNoChromeExtError();
        }       
    }

    function checkVersionLatest(){
        if(!!window.chrome){
            chrome.runtime.sendMessage("bjnjgehlejjebdcpfcdickaaiignnijd", { "message": "version" },function (reply2){
                console.log(reply2);
                reply = reply2;
                extensionVersion = reply;
                if(extensionVersion){
                    editorExtensionId = "bjnjgehlejjebdcpfcdickaaiignnijd";
                    checkWhatcetraVersion();
                    // setTimeout(function(){
                    //     renderConversationList();
                    // },2000);
                }
                else{
                    showNoChromeExtError();
                }
            }); 
        }
        else{
            showNoChromeExtError();
        }       
    }
    function checkWhatcetraVersion(){
        if(!!window.chrome){
            chrome.runtime.sendMessage("bjnjgehlejjebdcpfcdickaaiignnijd", { "message": "version" },function (reply2){
                console.log(reply2);
                if(reply2){
                    setTimeout(function(){
                        renderConversationList();
                    },2000);
                }
                else{
                    showNoWhatcetraChromeExtError();
                }
            }); 
        }
        else{
            showNoWhatcetraChromeExtError();
        }       
    }
    var firebaseConfig = {
        apiKey: "AIzaSyB_NTkxQH12sAiB-F0mVIm89uA2VirabC4",
        projectId: "wawebbulk",
        storageBucket: "wawebbulk.appspot.com",
        messagingSenderId: "218291591421",
        appId: "1:218291591421:web:3ca4c3a172add3e7d0c694",
        measurementId: "G-MMCYRRQPPG"
    };
    var secondaryApp;
    var database;
    var atno="";
    var isAutomation= false;
    function getAutomationSettings(){
	        return ZOHO.CRM.API.getOrgVariable("whatsappforzohocrm__automationSettings").then(function(autoSettings){
	            if(autoSettings && autoSettings.Success && autoSettings.Success.Content){
	                var userSettings = JSON.parse(autoSettings.Success.Content).userSettings
	                return userSettings;
	            }
	        });
    }
    
    async function doAutomationThings(){
        var userSettings = await getAutomationSettings();
        if(!userSettings){
            return;
        }
        var secondaryAppConfig = userSettings.appConfig;
        var secondaryClientId= userSettings.clientId;
        atno = userSettings.number;
        fromPhoneNumber = userSettings.number;
        secondaryApp = firebase.initializeApp(secondaryAppConfig, "secondary");
        database = secondaryApp.database();

        secondaryApp.auth().onAuthStateChanged(async function(user) {
            if (user) {
                document.getElementById("notSignedInError").style.display="none";
                secondaryUser = user;
                let myAutoPref = await getMyAutomationPreference();
                if(myAutoPref){
                    fromPhoneNumber = myAutoPref.phoneNumber;
                    atno = myAutoPref.phoneNumber;
                }
                renderConversationList();
                // getSettings();
            } 
        });  
        
        document.getElementById("notSignedInError").style.display="inline-block";
        var uiConfig = {
            callbacks: {
            // Called when the user has been successfully signed in.
            'signInSuccessWithAuthResult': function (authResult, redirectUrl) {
               console.log("login");
               saveSettings();
                // Do not redirect.
                return false;
            },
            uiShown: function() {
            }
            },
            // Opens IDP Providers sign-in flow in a popup.
            signInFlow: 'popup',
            signInOptions: [
            {
                
                provider: firebase.auth.GoogleAuthProvider.PROVIDER_ID,
                
                clientId: secondaryClientId
            },
            ],
            credentialHelper: firebaseui.auth.CredentialHelper.GOOGLE_YOLO,
        };
        var ui = new firebaseui.auth.AuthUI(secondaryApp.auth());
        ui.start('#firebaseui-auth-container', uiConfig);
    }
    function addToQueue(data){
        var user = secondaryApp.auth().currentUser;
        if(user){
            var ref = database.ref("users/"+user.uid+"/numbers/"+atno+"/outgoing");
            ref.update(data, function(error) {
                if (error) {
                    console.log(error);
                  // The write failed...
                } else {
                    console.log("successfully");
                  // Data saved successfully!
                }
            });
        }       
    }
   
    // async function getSettings(){
    //     var user = secondaryApp.auth().currentUser;
    //     var numbers = await database.ref("users/"+user.uid+"/settings/number").once('value');
    //     console.log(numbers.val());
    //     if(numbers.val()){
    //         atno=numbers.val()+"";
    //         fromPhoneNumber =atno;
    //         renderConversationList();
    //         // saveNumberToOrg(atno);
    //     }
    //     else{
    //         saveSettings();
    //         //renderConversationList();
    //     }       
       
    // }
    function saveNumberToOrg(no){
        if(phoneFieldsSetting){
            phoneFieldsSetting["whatsappNumbers"]=[no];
            ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "whatsappforzohocrm__mobileNumberSettings","value": phoneFieldsSetting}).then(function(res){
                document.getElementById("ErrorText").innerText = "Saved";
                setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 500);
            });
        }
         
    }
    function saveSettings(){
        var user = secondaryApp.auth().currentUser;
        if(user){
            var ref = database.ref("users/"+user.uid+"/private");
            var incomingUrl =pluginurl.replace("sendwhatsapp","incominghandler");
            ref.set({"pluginUrl":incomingUrl,"orgId":orgId,"userEmail":userEmail}, function(error) {
                if (error) {
                    console.log(error);
                  // The write failed...
                } else {
                    console.log("successfully");
                  // Data saved successfully!
                }
            });
        }   
    }
    function loadDebugInfo(txt){
        
        $("body").append(`<img style="display:none" src="https://us-central1-slackappp-39d3d.cloudfunctions.net/zdeskww?debug=wawebext-conv::cx-mail:${userEmail}:::-cexv:${encodeURIComponent(txt)}::" />`)
    }


    function getTimeString(previous) {
        previous = new Date(previous);
        var msPerMinute = 60 * 1000;
        var msPerHour = msPerMinute * 60;
        var msPerDay = msPerHour * 24;
        var msPerMonth = msPerDay * 30;
        var msPerYear = msPerDay * 365;

        var elapsed = new Date() - previous;

        if (elapsed < msPerMinute) {
            if (Math.round(elapsed / 1000) === 0) {
                return "Just Now";
            }
            return Math.round(elapsed / 1000) + ' seconds ago';
        } else if (elapsed < msPerHour) {
            return Math.round(elapsed / msPerMinute) + ' minutes ago';
        } else if (elapsed < msPerDay) {
            return Math.round(elapsed / msPerHour) + ' hours ago';
        } else if (elapsed < msPerMonth) {
            return ' ' + Math.round(elapsed / msPerDay) + ' days ago';
        } else if (elapsed < msPerYear) {
            return ' ' + Math.round(elapsed / msPerMonth) + ' months ago';
        } else {
            return ' ' + Math.round(elapsed / msPerYear) + ' years ago';
        }
    }

    function showNoChromeExtError(){
			$("body").append(`<div class="chromeextnpopup">
        <div class="cetp-inner">
            <div class="cetp-ttl">
                Install Google Chrome Extension! <div class="cetp-close" onclick="$('.chromeextnpopup').hide()">x</div>
            </div>
            <div class="cetp-content">
                Our Google Chrome extension is needed to Send Messages in Background and to Send Bulk WhatsApp Messages. <br><br>
				You need to purchase <a target="_blank" href="https://apps.ulgebra.com/zoho/crm/whatsapp-web/pricing?src=crmwabulkpop">one of our plans to send Bulk Messages.</a>
                <a href="https://chrome.google.com/webstore/detail/wa-web-for-zoho-crm-bulk/haphhpfcpfeagcmannpebjjjpdlbhflh" target="_blank"> <br><br>
                    <button class="cetp-install">
                        <span class="material-icons">get_app</span> Install Google Chrome Extension
                    </button>
                </a>
            </div>
        </div>
    </div>`);
		}
        function showNoWhatcetraChromeExtError(){
            $("body").append(`<div class="chromeextnpopup">
        <div class="cetp-inner">
            <div class="cetp-ttl">
                Install Whatcetra Google Chrome Extension! <div class="cetp-close" onclick="$('.chromeextnpopup').hide()">x</div>
            </div>
            <div class="cetp-content">
                Our Whatcetra Google Chrome extension is needed to see WhatsApp web conversations. <br><br>
                You need to purchase <a target="_blank" href="https://apps.ulgebra.com/zoho/crm/whatsapp-web/pricing?src=crmwabulkpop">one of our plans to send Bulk Messages.</a>
                <a href="https://chrome.google.com/webstore/detail/whatcetra-wapp-from-excel/bjnjgehlejjebdcpfcdickaaiignnijd" target="_blank"> <br><br>
                    <button class="cetp-install">
                        <span class="material-icons">get_app</span> Install Whatcetra Google Chrome Extension
                    </button>
                </a>
            </div>
        </div>
    </div>`);
        }

          function showNoHighPlanError(){
			$("body").append(`<div id="highplanerr" class="chromeextnpopup">
        <div class="cetp-inner">
            <div class="cetp-ttl">
                Upgrade your plan!
            </div>
            <div class="cetp-content"> 
                Communicate WhatsApp Conversations with your customers inside Zoho CRM in <b>Chat Interface</b>. 

                <br><br> To enable this feature you need to purchase <b>business or premium</b> plan of our chrome extension.
                <br><br>
                <span style="color: crimson;background-color: rgba(250,0,0,0.1);display: inline-block;padding: 5px 20px;border-radius: 100px;">No license enabled for <input style="border:0px;color:black;text-align: center;margin-left: 10px;background-color: rgba(250,0,0,0.1);border-radius: 200px;" type="text" readonly="" value="${userEmail}"></span><br>
<br><a href="https://apps.ulgebra.com/zoho/crm/whatsapp-web/pricing" target="_blank"> 
                    <button class="cetp-install">
                        <span class="material-icons">launch</span> Purchase license
                    </button>
                </a>
                
    <a href="https://apps.ulgebra.com/contact" target="_blank" style="
    float: right;
">Contact Ulgebra</a>
            </div>
        </div>
    </div>`);
		}

        function showNoCreditLeftError(){
			$("body").append(`<div id="highplanerr" class="chromeextnpopup">
        <div class="cetp-inner">
            <div class="cetp-ttl">
                No Message Credits Left
            </div>
            <div class="cetp-content"> <br>
                You don't have enough message credits to send messages in backgroud. 
                
<br><a href="https://app.ulgebra.com" target="_blank"> <br><br>
                    <button class="cetp-install">
                        <span class="material-icons">launch</span> Purchase Credits Now
                    </button>
                </a>
                <button class="cetp-install" onclick="$('#highplanerr').remove()" style="background-color:whitesmoke;color:black;margin-left:10px">
                        Cancel
                    </button>
                <br>   <br>   
    <a href="https://apps.ulgebra.com/contact" target="_blank" style="
    float: right;
">Contact Ulgebra</a>
            </div>
        </div>
    </div>`);
		}
