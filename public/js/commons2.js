var curId = 1000;
var appCommonSettings = null;
var myAutomationPrefrenece = undefined;
var selectors= {"pcs":'#side header [data-icon="default-user"]',"be":"#app .app-wrapper-web span","Re":"[data-icon=send]","In":'._3J6wB:not(._2ctZG)',"StartChat":'._3J6wB:not(._2ctZG) svg',"Im":"#side header img","at":'[data-icon="clip"]',"atIm":'[data-icon="attach-image-old"]',"atd":'[data-icon="attach-document-old"]',"om":".message-out","fb":'[data-icon="forward-chat"]',
                            "mb":"._1gL0z","fd":"._2Nr6U .zoWT4 .ggj6brxn.gfz4du6o.r7fjleex.g0rxnol2.lhj4utae.le5p0ye3.l7jjieqr.i0jNr","fi":"input[type=file]","bl":"._1YfMk ._31enr","Ig":"._3ExzF","cw":"._1bR5a","if":".message-in","ic":"_31gEB","cwa":'._3XpKm._20zqk',"asr":'._1-59F',"cl":".GDTQm","ma":"._17Osw","ht":"#main header ._21nHd span","cr":"._210SC"};  

// var selectors= {"pcs":'#side header [data-icon="default-user"]',"be":"#app .app-wrapper-web span","Re":"[data-icon=send]","In":'._3NCh_:not(._1MNyt)',"StartChat":'._3NCh_:not(._1MNyt) svg',"Im":"#side header img","at":'[data-icon="clip"]',"atIm":'[data-icon="attach-image-old"]',"atd":'[data-icon="attach-document-old"]',"om":".message-out","fb":'[data-icon="forward-chat"]',
                            // "mb":"._1gL0z","fd":"._3NCh_._1MNyt ._2aBzC ._35k-1._1adfa._3-8er","fi":"input[type=file]","bl":"._3SUQR .gaV0P","Ig":"._3ExzF","cw":"._1bR5a","if":".message-in","ic":"_31gEB","cwa":'._3XpKm._20zqk',"asr":'._1-59F',"cl":".GDTQm","ma":"._17Osw","ht":"#main header ._2KQyF span","cr":"._210SC"};





	async function getMyPreference() {
        showProcess(`Getting prefrence ...`);
			
        await firebase.firestore().collection('LicenseUsers').doc(userEmail).collection(zsckey).doc('settings').get().then((data)=>{
            processCompleted();
            appCommonSettings = data.data();
        }).catch(res=>{
            processCompleted();
            console.log(res);
        });                 
			
	}
    async function getMyAutomationPreference(){
        showProcess(`Getting automation prefrence ...`);

		secondaryApp.firestore().collection('waapp').doc(zsckey).collection("users").doc(secondaryApp.auth().currentUser.uid).get().then((data)=>{
            processCompleted();
			let appConfig = data.data();
            myAutomationPrefrenece = appConfig;
		}).catch(res=>{
            processCompleted();
			console.log(res);
		});

	}
    function saveMyPreference(prefObj){
        showProcess(` Saving prefrence ...`);
 
            firebase.firestore().collection('LicenseUsers').doc(userEmail).collection(zsckey).doc('settings').set(prefObj, {'merge': true}).then((data)=>{ 
                processCompleted();
                console.log(data, "pref is saved");
            }).catch(res=>{
                console.log(res);
                processCompleted();
                //showErrorMessage("Unable to set prefrence");
            });
           
	}

async function checkIfWhatsAppVerified(){
    try{
        if (firebase.apps.length === 0) {
            console.log('firebase not initialized, retrying...');
            return;
        }
        if(!appCommonSettings){
            await getMyPreference();
        }
        if(!appCommonSettings || !appCommonSettings.phoneNumber){
            showWhatsAppVerificationForm();
        }
    }catch(exc){
        console.log(exc);
    }
}

function verifyMyWhatsAppNumber(){
    if(!$("#uawcwa_value_countrycode").val()){
        alert("Please select country code!");
        return;
    }
    if(!$("#uawcwa_value_number").val() || $("#uawcwa_value_number").val().length < 6){
        alert("Please enter valid mobile number!");
        return;
    }
    let myPhoneNumber = $("#uawcwa_value_countrycode").val() + "" + $("#uawcwa_value_number").val();
    if(myPhoneNumber) {
        let verificationMessage = {};
        verificationMessage[Date.now()] = {
            "n": "917397415648",
            "m": "Hello Ulgebra, This is my WhatsApp number. Kindly Verify.",
            "dt": Date.now()
        };
        chrome.runtime.sendMessage(editorExtensionId, verificationMessage,function(response) {
			if (!response.success){
				console.log('some error');
			}
		});

        let updateObj = {};
  
        updateObj['phoneNumber'] = myPhoneNumber;
        updateObj['countryCode'] = $("#uawcwa_value_countrycode").val();
        saveMyPreference(updateObj);

        $("#verify_wa_number_popup").remove();
    }
}
    

function showWhatsAppVerificationForm(){

			$("body").append(`<div id="verify_wa_number_popup" class="chromeextnpopup">
        <div class="cetp-inner">
            <div class="cetp-ttl" style="
    color: green;
">Verification of WhatsApp Number<div class="cetp-close" onclick="$('.chromeextnpopup').hide()">x</div>
            </div>
            <div class="cetp-content">
    <div style="
    margin-bottom: 10px;
">
        <div style="
    font-size: 14px;
">Your WhatsApp Number</div>
    <div>
        <select required="" style="padding: 6px 5px;vertical-align: middle;border: 1px solid silver;" id="uawcwa_value_countrycode">
                                        <option value="">Choose Country</option>
                                        <option value="213">Algeria (+213)</option><option value="376">Andorra (+376)</option><option value="244">Angola (+244)</option><option value="1264">Anguilla (+1264)</option><option value="1268">Antigua &amp; Barbuda (+1268)</option><option value="54">Argentina (+54)</option><option value="374">Armenia (+374)</option><option value="297">Aruba (+297)</option><option value="61">Australia (+61)</option><option value="43">Austria (+43)</option><option value="994">Azerbaijan (+994)</option><option value="1242">Bahamas (+1242)</option><option value="973">Bahrain (+973)</option><option value="880">Bangladesh (+880)</option><option value="1246">Barbados (+1246)</option><option value="375">Belarus (+375)</option><option value="32">Belgium (+32)</option><option value="501">Belize (+501)</option><option value="229">Benin (+229)</option><option value="1441">Bermuda (+1441)</option><option value="975">Bhutan (+975)</option><option value="591">Bolivia (+591)</option><option value="387">Bosnia Herzegovina (+387)</option><option value="267">Botswana (+267)</option><option value="55">Brazil (+55)</option><option value="673">Brunei (+673)</option><option value="359">Bulgaria (+359)</option><option value="226">Burkina Faso (+226)</option><option value="257">Burundi (+257)</option><option value="855">Cambodia (+855)</option><option value="237">Cameroon (+237)</option><option value="1">Canada (+1)</option><option value="238">Cape Verde Islands (+238)</option><option value="1345">Cayman Islands (+1345)</option><option value="236">Central African Republic (+236)</option><option value="56">Chile (+56)</option><option value="86">China (+86)</option><option value="57">Colombia (+57)</option><option value="242">Congo (+242)</option><option value="682">Cook Islands (+682)</option><option value="506">Costa Rica (+506)</option><option value="385">Croatia (+385)</option><option value="53">Cuba (+53)</option><option value="90392">Cyprus North (+90392)</option><option value="357">Cyprus South (+357)</option><option value="42">Czech Republic (+42)</option><option value="45">Denmark (+45)</option><option value="253">Djibouti (+253)</option><option value="1809">Dominican Republic (+1809)</option><option value="593">Ecuador (+593)</option><option value="20">Egypt (+20)</option><option value="503">El Salvador (+503)</option><option value="240">Equatorial Guinea (+240)</option><option value="291">Eritrea (+291)</option><option value="372">Estonia (+372)</option><option value="251">Ethiopia (+251)</option><option value="500">Falkland Islands (+500)</option><option value="298">Faroe Islands (+298)</option><option value="679">Fiji (+679)</option><option value="358">Finland (+358)</option><option value="33">France (+33)</option><option value="594">French Guiana (+594)</option><option value="689">French Polynesia (+689)</option><option value="241">Gabon (+241)</option><option value="220">Gambia (+220)</option><option value="7880">Georgia (+7880)</option><option value="49">Germany (+49)</option><option value="233">Ghana (+233)</option><option value="350">Gibraltar (+350)</option><option value="30">Greece (+30)</option><option value="299">Greenland (+299)</option><option value="1473">Grenada (+1473)</option><option value="590">Guadeloupe (+590)</option><option value="671">Guam (+671)</option><option value="502">Guatemala (+502)</option><option value="224">Guinea (+224)</option><option value="245">Guinea - Bissau (+245)</option><option value="592">Guyana (+592)</option><option value="509">Haiti (+509)</option><option value="504">Honduras (+504)</option><option value="852">Hong Kong (+852)</option><option value="36">Hungary (+36)</option><option value="354">Iceland (+354)</option><option value="91">India (+91)</option><option value="62">Indonesia (+62)</option><option value="98">Iran (+98)</option><option value="964">Iraq (+964)</option><option value="353">Ireland (+353)</option><option value="972">Israel (+972)</option><option value="39">Italy (+39)</option><option value="1876">Jamaica (+1876)</option><option value="81">Japan (+81)</option><option value="962">Jordan (+962)</option><option value="254">Kenya (+254)</option><option value="686">Kiribati (+686)</option><option value="850">Korea North (+850)</option><option value="82">Korea South (+82)</option><option value="965">Kuwait (+965)</option><option value="996">Kyrgyzstan (+996)</option><option value="856">Laos (+856)</option><option value="371">Latvia (+371)</option><option value="961">Lebanon (+961)</option><option value="266">Lesotho (+266)</option><option value="231">Liberia (+231)</option><option value="218">Libya (+218)</option><option value="417">Liechtenstein (+417)</option><option value="370">Lithuania (+370)</option><option value="352">Luxembourg (+352)</option><option value="853">Macao (+853)</option><option value="389">Macedonia (+389)</option><option value="261">Madagascar (+261)</option><option value="265">Malawi (+265)</option><option value="60">Malaysia (+60)</option><option value="960">Maldives (+960)</option><option value="223">Mali (+223)</option><option value="356">Malta (+356)</option><option value="692">Marshall Islands (+692)</option><option value="596">Martinique (+596)</option><option value="222">Mauritania (+222)</option><option value="269">Mayotte (+269)</option><option value="52">Mexico (+52)</option><option value="691">Micronesia (+691)</option><option value="373">Moldova (+373)</option><option value="377">Monaco (+377)</option><option value="976">Mongolia (+976)</option><option value="1664">Montserrat (+1664)</option><option value="212">Morocco (+212)</option><option value="258">Mozambique (+258)</option><option value="95">Myanmar (+95)</option><option value="264">Namibia (+264)</option><option value="674">Nauru (+674)</option><option value="977">Nepal (+977)</option><option value="31">Netherlands (+31)</option><option value="687">New Caledonia (+687)</option><option value="64">New Zealand (+64)</option><option value="505">Nicaragua (+505)</option><option value="227">Niger (+227)</option><option value="234">Nigeria (+234)</option><option value="683">Niue (+683)</option><option value="672">Norfolk Islands (+672)</option><option value="670">Northern Marianas (+670)</option><option value="47">Norway (+47)</option><option value="968">Oman (+968)</option><option value="680">Palau (+680)</option><option value="507">Panama (+507)</option><option value="675">Papua New Guinea (+675)</option><option value="595">Paraguay (+595)</option><option value="51">Peru (+51)</option><option value="63">Philippines (+63)</option><option value="48">Poland (+48)</option><option value="351">Portugal (+351)</option><option value="1787">Puerto Rico (+1787)</option><option value="974">Qatar (+974)</option><option value="262">Reunion (+262)</option><option value="40">Romania (+40)</option><option value="250">Rwanda (+250)</option><option value="378">San Marino (+378)</option><option value="239">Sao Tome &amp; Principe (+239)</option><option value="966">Saudi Arabia (+966)</option><option value="221">Senegal (+221)</option><option value="381">Serbia (+381)</option><option value="248">Seychelles (+248)</option><option value="232">Sierra Leone (+232)</option><option value="65">Singapore (+65)</option><option value="421">Slovak Republic (+421)</option><option value="386">Slovenia (+386)</option><option value="677">Solomon Islands (+677)</option><option value="252">Somalia (+252)</option><option value="27">South Africa (+27)</option><option value="34">Spain (+34)</option><option value="94">Sri Lanka (+94)</option><option value="290">St. Helena (+290)</option><option value="1869">St. Kitts (+1869)</option><option value="1758">St. Lucia (+1758)</option><option value="249">Sudan (+249)</option><option value="597">Suriname (+597)</option><option value="268">Swaziland (+268)</option><option value="46">Sweden (+46)</option><option value="41">Switzerland (+41)</option><option value="963">Syria (+963)</option><option value="886">Taiwan (+886)</option><option value="66">Thailand (+66)</option><option value="228">Togo (+228)</option><option value="676">Tonga (+676)</option><option value="1868">Trinidad &amp; Tobago (+1868)</option><option value="216">Tunisia (+216)</option><option value="90">Turkey (+90)</option><option value="993">Turkmenistan (+993)</option><option value="1649">Turks &amp; Caicos Islands (+1649)</option><option value="688">Tuvalu (+688)</option><option value="44">UK (+44)</option><option value="1">United States of America (+1)</option><option value="256">Uganda (+256)</option><option value="380">Ukraine (+380)</option><option value="971">United Arab Emirates (+971)</option><option value="598">Uruguay (+598)</option><option value="7">Uzbekistan (+7)</option><option value="678">Vanuatu (+678)</option><option value="379">Vatican City (+379)</option><option value="58">Venezuela (+58)</option><option value="84">Virgin Islands - US (+1340)</option><option value="681">Wallis &amp; Futuna (+681)</option><option value="969">Yemen (North)(+969)</option><option value="967">Yemen (South)(+967)</option><option value="260">Zambia (+260)</option><option value="263">Zimbabwe (+263)</option>
                                    </select>
        <input type="number" id="uawcwa_value_number" placeholder="Enter phone number" style="
    padding: 5px 10px;
    border-radius: 3px;
    border: 1px solid silver;
    box-shadow: inset 0px 0px 5px rgba(0,0,0,0.1);
">
    </div>
</div>
    <span style="
    font-size: 14px;
    color: rgb(86,86,86);
">
    We'll send a test WhatsApp message from your number to Ulgebra Number for verification.
    </span>
                <div style="
    margin-top: 15px;
">
                    <button onclick="verifyMyWhatsAppNumber()" class="cetp-install" style="
    background-color: green;
">
                        <span class="material-icons">send</span>Send Verification Message</button>
</div>
          
            </div>
        </div>
    </div>`);
                if(fromPhoneNumber){
                    let number = fromPhoneNumber.replace(/\D/g, '');
                    let countryCode = uawc_getCountryCode(number);
                    if (countryCode) {
                        let num = number.toString();
                        number = num.substring(countryCode.toString().length);
                        $("#uawcwa_value_countrycode").val(countryCode);
                    }
                    $("#uawcwa_value_number").val(number);
                }
		}

        function uawc_getCountryCode(number) {
                let country_code = [1, 1242, 1246, 1264, 1268, 1284, 1340, 1345, 1441, 1473, 1649, 1664, 1758, 1787, 1809, 1868, 1869, 1876, 2, 20, 212, 213, 216, 218, 220, 221, 222, 223, 224, 226, 227, 228, 229, 231, 232, 233, 234, 236, 237, 238, 239, 240, 241, 242, 244, 245, 248, 249, 250, 251, 252, 253, 254, 256, 257, 258, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 269, 27, 290, 291, 297, 298, 299, 30, 31, 32, 33, 34, 350, 351, 352, 353, 354, 356, 357, 358, 359, 36, 370, 371, 372, 373, 374, 375, 376, 377, 378, 379, 380, 381, 385, 386, 387, 389, 39, 40, 41, 417, 42, 421, 43, 44, 45, 46, 47, 48, 49, 500, 501, 502, 503, 504, 505, 506, 507, 509, 51, 52, 53, 54, 55, 56, 57, 58, 590, 591, 592, 593, 594, 595, 596, 597, 598, 60, 61, 62, 63, 64, 65, 66, 670, 671, 672, 673, 674, 675, 676, 677, 678, 679, 680, 681, 682, 683, 686, 687, 688, 689, 691, 692, 7, 7, 7, 7, 7, 7880, 81, 82, 84, 850, 852, 853, 855, 856, 86, 880, 886, 90, 90392, 91, 94, 95, 960, 961, 962, 963, 964, 965, 966, 967, 968, 969, 971, 972, 973, 974, 975, 976, 977, 98, 993, 994, 996];
                if (number.toString().length > 10)
                {
                    num = number.toString();
                    let num_len = num.split(num.slice(-10))[0];
                    for (let c of country_code)
                    {
                        if (c + "" === num_len + "")
                        {
                            return c;
                        }
                    }
                }
                return null;
            }
        

        var wwebconnectedWithChrome = false;
		function checkState(){
	     
	           
	            return chrome.runtime.sendMessage(editorExtensionId,{"message":"checkState"},function (response){
	                
	                    console.log(response);
                        
	                    if (response && response.connected == true && response.ownerNo){
                            wwebconnectedWithChrome = true;
	                        fromPhoneNumber = response.ownerNo;
                            checkIfWhatsAppVerified();
	                        return true;
	                    }
	                    else if(response && response.connected == true && !response.ownerNo){
                            wwebconnectedWithChrome = true;
	                        showWWebNotConnectedInstructions();
	                        return false;
	                    }
	                    else{
                            if(response && response.connected == false){
                                setTimeout(function(){
                                    if(selectors && pluginurl){
                                        chrome.runtime.sendMessage(editorExtensionId,{"message":"setSelectors","s":selectors,"url":pluginurl},function (response){
                                            console.log(response);
                                        });
                                    }    
                                },2000)
                            }   
                            showWWebNotConnectedInstructions();
	                        return false;
	                    }
                        if(typeof renderWAWebSenderIds === 'function'){
                            renderWAWebSenderIds();
                        }
	                });
	                
	           
	    }

function showWWebNotConnectedInstructions(){

    $('.fromNumChooserOuter').hide();
    if(wwebconnectedWithChrome){
        wcConfirm(`<div style="display: flex; align-items: center; justify-content: center; flex-direction: column;">It seems you have not set profile picture in WhatsApp. Please upload a profile picture in WhatsApp to continue. <br>
            <button style="text-align:center;cursor:pointer;background-color: white;color: black;border-radius: 5px;border: none;margin-top: 15px;box-shadow: 0px 2px 5px rgba(0,0,0,0.4);cursor: pointer;" onclick="checkState()">Click to Check again</button> 
            <button style="text-align:center;cursor:pointer;background-color: black;color: white;border-radius: 5px;border: none;margin-top: 15px;box-shadow: 0px 2px 5px rgba(0,0,0,0.4);cursor: pointer;" onclick="wcConfirmHide();">Cancel</button> 
            <br><br><br><i style="
    font-size: 12px;
">If it's connected properly and this error shown, restart your chrome browser and try again or <a target="_blank" href="https://apps.ulgebra.com/contact" style="
    color: white;
    text-decoration: underline;
">Contact Us</a></i></div>`,'','Okay',true,true);
    }
    else{
        wcConfirm(`<div style="display: flex; align-items: center; justify-content: center; flex-direction: column;">Please make sure  
    <ul style="
    margin-top: 5px;
">
        <li>
            WhatsApp Web tab is <b> connected with your phone</b> </li> <li> <b>Only one</b> WhatsApp Web tab is open </li>
    </ul>  
    <button style="text-align:center;cursor:pointer;background-color: white;color: black;border-radius: 5px;border: none;margin-top: 15px;box-shadow: 0px 2px 5px rgba(0,0,0,0.4);cursor: pointer;" onclick="checkState()">Click to Check again</button> 
    <button style="text-align:center;cursor:pointer;background-color: black;color: white;border-radius: 5px;border: none;margin-top: 15px;box-shadow: 0px 2px 5px rgba(0,0,0,0.4);cursor: pointer;" onclick="wcConfirmHide();">Cancel</button> 
     <br><br><br><i style="
    font-size: 12px;
">If it's connected properly and this error shown, refresh WhatsApp Web and try or restart your chrome browser and try again or <a target="_blank" href="https://apps.ulgebra.com/contact" style="
    color: white;
    text-decoration: underline;
">Contact Us</a></i></div>`,'','Okay',true,true);
    }
    
}

var licDetJSON = {};
var licPlanArray = [];
var licCurCredit = 0;
var licSupportedPlans = {
"price_1MD7LdHo9p6j6FgRPYsmz81j":{
    "type": "premium"
},
"price_1MD7LPHo9p6j6FgRfGDPwkN4":{
    "type": "business"
},
"price_1MD7L0Ho9p6j6FgRNjwI5zbM":{
    "type": "classic"
},
"price_1MD7KGHo9p6j6FgRoW22fPuA":{
    "type": "premium"
},
"price_1MD7K1Ho9p6j6FgRYAv8Fcqs":{
    "type": "business"
},
"price_1MD7JnHo9p6j6FgRDd70I9mx":{
    "type": "classic"
},
"price_1MD7JJHo9p6j6FgRqxs3Fi6p":{
    "type": "premium"
},
"price_1MD7J2Ho9p6j6FgRF6bNPD3l":{
    "type": "business"
},
"price_1MD7IYHo9p6j6FgRaxwHoSvy":{
    "type": "classic"
},
"price_1MD4ZGHo9p6j6FgRDXA4Ru7n":{
    "type": "premium"
},
"price_1MD4YvHo9p6j6FgRPduIL6iW":{
    "type": "business"
},
"price_1MD4YhHo9p6j6FgRttPrBtus":{
    "type": "classic"
},
"price_1MD4YNHo9p6j6FgRr39DrO7Q":{
    "type": "premium"
},
"price_1MD4Y3Ho9p6j6FgRUllD2Cov":{
    "type": "business"
},
"price_1MD4XnHo9p6j6FgRNvvEIVyo":{
    "type": "classic"
},
"price_1IdrL2Ho9p6j6FgR5qyjJOo0":{
 "type": "premium"
},"price_1IP0aDHo9p6j6FgRibv4isf0":{
"type": "premium"
},"price_1I6CrpHo9p6j6FgRISpRviN4":{
"type": "premium"
},"price_1HyZblHo9p6j6FgRQzDdcl81":{
"type": "premium"
},"price_1HyZbTHo9p6j6FgRGfOwxuwQ":{
"type": "premium"
},"price_1HyZbFHo9p6j6FgRVHU989Un":{
"type": "premium"
},"price_1IP0ZkHo9p6j6FgRqHyLCI4z":{
"type": "business"
},"price_1I6CrFHo9p6j6FgRhnRWESX1":{
"type": "business"
},"price_1HyZaxHo9p6j6FgRnWjTs3we":{
"type": "business"
},"price_1HyZadHo9p6j6FgRe0Ai3LrT":{
"type": "business"
},"price_1HyZEtHo9p6j6FgRAvpfOvqM":{
"type": "business"
},"price_1HyZAgHo9p6j6FgReqwmnUp7":{
"type": "classic"
},"price_1HyZAgHo9p6j6FgRqQGZavms":{
"type": "classic"
},"price_1IP0YzHo9p6j6FgRg15L9O7H":{
"type": "classic"
},"price_1I6CmKHo9p6j6FgRQv6Cpet2":{
"type": "classic"
},"price_1HyeEsHo9p6j6FgRoSkOo1kP":{
"type": "classic"
},"price_1HyeESHo9p6j6FgRy1Pwz7FH":{
"type": "classic"
}};
var licChosenSubId = null;
var licChosenPlanId = null;
function fetchLICDetails(userId, callbackFunc){

    //let planVSsubmap = {};
	fetch("https://us-central1-ulgebra-license.cloudfunctions.net/getUALicensedUserSubs?userId="+userId)
      .then(function (response) {  
          return response.json();
      })
      .then(function (myJson) {
          licDetJSON = myJson;
          if(licDetJSON && Object.keys(licDetJSON.subscriptions).length > 0) {
          	for(let item in licDetJSON.subscriptions){
                if(licSupportedPlans.hasOwnProperty(licDetJSON.subscriptions[item].priceId)){
                    // licPlanArray.push(licDetJSON.subscriptions[item].priceId);
                    // planVSsubmap[licDetJSON.subscriptions[item].priceId] = item;
                    licChosenPlanId = licDetJSON.subscriptions[item].priceId;
                    licChosenSubId = item;
                    break;
                }
          	}
          }
          // for(let item in licSupportedPlans){
          //     if(licPlanArray.includes(item)){
          //         licChosenPlanId = item;
          //         licChosenSubId = planVSsubmap[item];
          //         break;
          //     }
          // }
          if(licChosenSubId){
            tabOnCreditActivity(callbackFunc);
          }
          if(typeof callbackFunc === "function"){
          	callbackFunc(licDetJSON, licPlanArray);
          }
      })
      .catch(function (error) {
          console.log("Error: ", error);
       });
}

var uaLICFirebaseAppConfig = {
        apiKey: "AIzaSyDsvl4lleRa8k-3UuDqltueXZy1V27f0Sw",
        authDomain: "ulgebra-license.firebaseapp.com",
        databaseURL: "https://ulgebra-license.firebaseio.com",
        projectId: "ulgebra-license",
        storageBucket: "ulgebra-license.appspot.com",
        messagingSenderId: "356364764905",
        appId: "1:356364764905:web:4bea988d613e73e1805278",
        measurementId: "G-54FN510HDW"
};

var uaLICFirebaseApp = null;

function tabOnCreditActivity(callbackFunc){
    if (!firebase) {
        console.log('firebase not initialized, retrying...');
        setTimeout(tabOnCreditActivity, 2000);
        return;
    }
    if(!uaLICFirebaseApp){
        uaLICFirebaseApp = firebase.initializeApp(uaLICFirebaseAppConfig, "ualicapp");
    }
	uaLICFirebaseApp.database().ref('ualic_user_credits/' + licDetJSON.subscriptions[licChosenSubId].licOwner).on('value', function(snapshot){
		licCurCredit = snapshot.val();
        waMsgCrdleft = licCurCredit;
		$('.creditRemainDisp').text(licCurCredit);
        if(typeof doOnCreditActivity === "function"){
			doOnCreditActivity(licCurCredit);
		}
		if(typeof callbackFunc === "function"){
			callbackFunc(licCurCredit);
		}
    });
}

function logSUBActivityLog(priority){
    if(!licChosenSubId){
        return;
    }
	fetch("https://us-central1-ulgebra-license.cloudfunctions.net/uaAPPSubActivity?behalf="+licDetJSON.subscriptions[licChosenSubId].licOwner+"&priority="+priority+"&digi="+Date.now())
      .then(function (response) {
          return response.json();
      })
      .then(function (myJson) {
          console.log(myJson);
      })
      .catch(function (error) {
          console.log("Error: ", error);
       });
}


function showNewPlanDetails() {
			if(!licChosenPlanId) {
                
                    $("#send").after(`<div id="nocrediterror" style="
    padding: 9px 10px;
    margin: 10px 0px;
    background-color: rgba(250,0,0,0.3);
    color: crimson;
    border-radius: 30px;
    font-size: 13px;
    /* clear: both; */
    float: right;
    display: inline-block;
">You don't have plan or message credits, so WhatsApp will be opened in new tab to send manually</div>`);

                
                if(typeof showNoHighPlanError === "function"){
                    showNoHighPlanError();
                }
                else{
                    $('.bottomTipActions').prepend('<b>NO PLAN PURCHASED</b>');
                }
                return;
			}
            else {

                $('.oldlicban').hide();
                $('.bottomTipActions').prepend(' <b> '+ licSupportedPlans[licChosenPlanId].type.toUpperCase() +' </b> plan, credit remaining : <b class="creditRemainDisp"></b> &nbsp;&nbsp;');

                isAllowed = licSupportedPlans[licChosenPlanId].type === "basic" || licSupportedPlans[licChosenPlanId].type === "classic";
                        if(licSupportedPlans[licChosenPlanId].type === "business" || licSupportedPlans[licChosenPlanId].type === "premium"){
                            attPur = true;
                            isAllowed = true;
                            $("#highplanerr").remove();
                        }

            }
			
		}

        function doOnCreditActivity(){
            if(licCurCredit<1){
                document.write(`<h1 style="color:crimson">No Credits</h1>You do not have enough message credits to send messages. Kindly buy more credits to continue. <a href="https://app.ulgebra.com" target="_blank">Purchase Now</a> <br><br> <a href="https://apps.ulgebra.com/zoho/crm/whatsapp-web/pricing" target="_blank">View Pricing Plans</a> `);
            }
        }

        function doEditorStyleApply(actionType){
            var styleMap = {'bold': '*', 'italic': '_', 'strike': '~'};
            var actionVar = styleMap[actionType];
            $('#emailContentEmail').text(($('#emailContentEmail').text().substring(0, window.getSelection().getRangeAt(0).startOffset))+actionVar+window.getSelection().getRangeAt(0).toString()+actionVar+ ($('#emailContentEmail').text().substring(window.getSelection().getRangeAt(0).endOffset, $('#emailContentEmail').text().length)));
        }
