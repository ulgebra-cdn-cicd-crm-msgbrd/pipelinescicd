// var emailContentText = "Hi ,\n\nHope you're well. I'm writing to know how our business can help you.\n\nPlease choose a event to schedule an appointment:\n\n${eventsUrls} See you soon!\n\n %ownerName% \n\n %companyName%";
		var emailContent = [];
		var emailContentText = "";
		var startIndex =0;
		var endIndex = 0;
		var currentEditor="";
		var subject="";
		var sel;
		var range;
		var calendarsMap;
		var recordId;
		var currentRecords;
		var recordModule = "Records";
		var ButtonPosition;
		var smsTemplates;
		var templateId;
		var dealContactId;
		var dealAccountId;
		var recipientName;
		var countryCode;
		var moduleFields;
		var userFields;
		var historyFields;
		var users = [];
		var phoneField="";
		var webOrDesktop ="web";
		var bulkInsertList={};
		var isAllowed = false;
		var module_name = "CustomModule";
		var orgId;
		var extensionVersion = null;
		var userEmail = null;
		var defaultModules = ["Contacts","Leads","Accounts"];
		var editorExtensionId = "nkihbgbbbakefbgjnokdicilnhcdeckp";
		var attachmentId = new Date().getTime();
		var isAttachmentAdded = false;
		var fields=[];
		var records=[];
        document.addEventListener("DOMContentLoaded", function(event) {
        	var email = "ulgebra@zoho.com";
        	$.ajax({url: "https://us-central1-wawebbulk.cloudfunctions.net/checkUALIC?src=crm-wa-bulk&email="+email, success: function(result){
							  // executeFirebaseThings();
			    if(result && result.status == "valid"){
			    	isAllowed = true;
			    }
				isAllowed = true;
			}});
        	
			if(!!window.chrome){
				chrome.runtime.sendMessage(editorExtensionId, { "message": "portal" },function (portal){
					console.log(portal);
					if(portal && portal.version){
						extensionVersion = {"version":portal.version};
					}
					var portalSettings = portal.portalSettings;
					if(portalSettings){
						if(portalSettings.fields && portalSettings.fields.length){
							fields = portalSettings.fields;
						}
						if(portalSettings.records && portalSettings.records.length){
							records = portalSettings.records;
						}
						handleRecords(fields);
					}	
					showForcedAlert();
					
					document.getElementsByClassName("footer")[0].insertAdjacentHTML('afterbegin', `<br>
	    <div id="attachDiv" style="">
	        <input onchange="getAttachments()" type="checkbox" id="wwebattach" name="wwebattach" style="
	    height: 20px;
	    width: 20px;
		cursor: pointer;
	    vertical-align: middle;
	    margin-top: -2px;
	"> <label for="wwebattach" style="
	    margin-left: 5px;
	    cursor: pointer;
	">Include attachments with this message</label>
	    </div>`);
						
				});		
					
			}		
        	const el = document.getElementById('emailContentEmail');

			el.addEventListener('paste', (e) => {
			  // Get user's pasted data
			  let data = e.clipboardData.getData('text/html') ||
			      e.clipboardData.getData('text/plain');
			  
			  // Filter out everything except simple text and allowable HTML elements
			  let regex = /<(?!(\/\s*)?()[>,\s])([^>])*>/g;
			  data = data.replace(regex, '');
			  
			  // Insert the filtered content
			  document.execCommand('insertHTML', false, data);

			  // Prevent the standard paste behavior
			  e.preventDefault();
			});
			var content_id = 'emailContentEmail';  
			max = 2000;
			//binding keyup/down events on the contenteditable div
			$('#'+content_id).keyup(function(e){ check_charcount(content_id, max, e); });
			$('#'+content_id).keydown(function(e){ check_charcount(content_id, max, e); });

			function check_charcount(content_id, max, e)
			{   
			    if(e.which != 8 && $('#'+content_id).text().length > max)
			    {
			    	document.getElementById("ErrorText").innerText = "Message should be within 2000 characters.";
	        		document.getElementById("Error").style.display= "block";
	        		// document.getElementById("ErrorText").style.color="red";
					setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
			       // $('#'+content_id).text($('#'+content_id).text().substring(0, max));
			       e.preventDefault();
			    }
			}
        });
		
        function showAnnounceMent(type){
            if(type === "install_chrome_extension"){
                $('body').append(`<div id="announcetip" class="bottomTip" >Automatically Send Single &amp; Bulk WhatsApp messages in background <div class="bottomTipActions"><a href="https://chrome.google.com/webstore/detail/wa-web-for-zoho-crm-bulk/nkihbgbbbakefbgjnokdicilnhcdeckp" target="_blank">Install Google Chrome Plugin </a></div>
                    <span onclick="$('#announcetip').hide()" class="tt-close">X</span>
                </div>`);
            }
             if(type === "purchase_license"){
                $('body').append(`<div id="announcetip" class="bottomTip" >Thank you for installing chrome plugin. <br> <br> Automatically Send Single &amp; Bulk WhatsApp messages in background <div class="bottomTipActions"><a href="https://creator.zohopublic.com/ulgebra/ulgebra-zoho-crm-bulk-whatsapp-plugin-purchase/form-perma/Ulgebra_Zoho_CRM_Bulk_Whatsapp_Plugin_License/DhPCXY73ebeHUBRVCgggEYHDu4H8ZDSure1a7HOp2Ttajdyrw0pDaQkM5FqTfxsFNVvuEAGjm9FQe92ZrTXGVdxMwteM8utKjyOt" target="_blank">Request Access License</a></div>
                    <span onclick="$('#announcetip').hide()" class="tt-close">X</span>
                </div>`);
            }
			/* $('body').append(`<div id="announcetip" class="bottomTip" ><div class="bottomTipActions">Temporarily stopped chrome extension. Will be enabled soon.<a href="https://forms.gle/kXX6wJnyvYLEd2nbA" target="_blank">Submit Feedback</a> </div> </div>`);*/
        }
        function handleRecords(fields){
        	
        	document.getElementById("dropdown-menu-email").innerHTML="";
			var phoneList="";
			fields.forEach(function(field){
				if(field){
					addListItem("dropdown-menu-email",field,"dropdown-item",field);
					phoneList =phoneList+ '<li class="templateItem" onclick="setNumber(this)">'+field+'</li>';
				}
			});
			$("#phoneList").html("");
			$("#phoneList").append(phoneList);
        }
        function setNumber(phone){
        	phoneField = phone.innerText;
        	document.getElementById("selectedPhone").innerText = phone.innerText;
			document.getElementById("tooltiptext").innerText = phone.innerText;
        }
		function getAttachments(){
			if(!isAttachmentAdded){
				$("#attachDiv").append(`<span id="wa-attach-tip" style="display: block;margin: 5px;border: 1px dotted green;padding: 7px 15px;background-color: rgba(0,200,0,0.1);word-break: unset;border-radius: 26px;font-size: 14px;color: green;">You will be redirected to WhatsApp tab for sending attachments after you click Send.</span>`);
				isAttachmentAdded = true;
			}
			else{
				$("#wa-attach-tip").remove();
				isAttachmentAdded = false;
			}
		}
        function checkMobileNumber(Mobile,currentRecord){
			if(!Mobile){
				return Promise.all([getMobileNumber(Mobile,currentRecord)]).then(function(resp){
					if(resp[0].Mobile){
						Mobile = resp[0].Mobile.replace(/\D/g,'');
						var request ={
					        url : "https://rest.messagebird.com/lookup/" + Mobile,
					        headers:{
					              Authorization:"AccessKey 7NMPor0R8DofSHH61SpViNNqQ",
					        }
					    }
					    return ZOHO.CRM.HTTP.get(request).then(function(phoneData){
					    	if(JSON.parse(phoneData).countryPrefix == null){
					    		if(countryCode && countryCode != "0" && Mobile.length > countryCode.length && Mobile.substring(0,countryCode.length) != countryCode){
									Mobile = countryCode+""+Mobile;
								}
					    	}
					    	return {"Mobile":Mobile,"toModule":resp[0].toModule,"toId":resp[0].toId,"recipientName":resp[0].recipientName};
					    });	
					}
					else{
						return {};
					}    
				});
			}
			else{
				Mobile = Mobile.replace(/\D/g,'');
				var request ={
			        url : "https://rest.messagebird.com/lookup/" + Mobile,
			        headers:{
			              Authorization:"AccessKey 7NMPor0R8DofSHH61SpViNNqQ",
			        }
			    }
			    return ZOHO.CRM.HTTP.get(request).then(function(phoneData){
			    	if(JSON.parse(phoneData).countryPrefix == null){
			    		if(countryCode && countryCode != "0" && Mobile.length > countryCode.length && Mobile.substring(0,countryCode.length) != countryCode){
							Mobile = countryCode+""+Mobile;
						}
			    	}
			    	return {"Mobile":Mobile};
			    });	
			}	
		}
		function getMobileNumber(Mobile,currentRecord){
			if(phoneField && phoneField.indexOf(".") != -1){
				var toModuleField = phoneField.substring(0,phoneField.indexOf("."));
				var dealphoneField = phoneField.substring(phoneField.indexOf(".")+1);
				if(currentRecord[toModuleField] && currentRecord[toModuleField].id){
					var toId = currentRecord[toModuleField].id ;
					var toModule;
					moduleFields.forEach(function(field){
						if(field.api_name == toModuleField){
							toModule = field.lookup.module.api_name;
						}
					});
					return ZOHO.CRM.API.getRecord({Entity:toModule,RecordID:toId}).then(function(contactData){
						if(contactData.data[0][dealphoneField] != null && contactData.data[0][dealphoneField] != ""){
							if(toModule == "Contacts" || toModule == "Leads" ){
								var recipientName = contactData.data[0].Full_Name;
							}
							else if(toModule == "Accounts"){
								var recipientName = contactData.data[0].Account_Name;
							}	
							return {"Mobile":contactData.data[0][dealphoneField],"toModule":toModule,"toId":toId,"recipientName":recipientName};
						}
						else{
							return {"Mobile":null};
						}
					});	
				}
				else{
					return {"Mobile":null};
				}
			}	
			else{
				return {"Mobile":currentRecord[phoneField]};
			}
		}
        function sendSMS(){
        	var sendBulkWhatsAppList={};
        	var MobileNumber;
        	var toModule ;
        	var toId ;
        	if(!phoneField){
        		document.getElementById("ErrorText").innerText = "Please Choose Phone Field.";
        		document.getElementById("Error").style.display= "block";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
        	}
    		else if(document.getElementById("emailContentEmail").innerText.length >1600){
    			document.getElementById("ErrorText").innerText = "Message should be within 2000 characters.";
        		document.getElementById("Error").style.display= "block";
        		document.getElementById("Error").style.color="red";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
    		}
        	else if(document.getElementById("emailContentEmail").innerText.replace(/\n/g,"").replace(/\t/g,"").replace(/ /g,"") == ""){
        		document.getElementById("ErrorText").innerText = "Message cannot be empty.";
        		document.getElementById("Error").style.display= "block";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
        	}
        	else{
        		var	notes = "\n("+recordModule +" without mobile number will be ignored.)";
	        	document.getElementById("ErrorText").innerText = "Sending... "+notes;
				document.getElementById("Error").style.display= "block";
	        	var message = document.getElementById("emailContentEmail").innerText;
	        	var id = new Date().getTime();
	        	var phoneFieldIndex =fields.indexOf(phoneField);
				records.forEach(function(currentRecord){
					if(currentRecord){
						var filledMessage = JSON.parse(JSON.stringify(getMessageWithFields(message,currentRecord)));
						filledMessage = filledMessage.trim();
						if(filledMessage.length > 2000)
						{
							document.getElementById("ErrorText").innerText = "Message is Too Large.";
			        		document.getElementById("Error").style.display= "block";
							setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
							return ;
						}
						else if(filledMessage.length < 1)
						{
							document.getElementById("ErrorText").innerText = "Merge Fields value is empty.";
			        		document.getElementById("Error").style.display= "block";
							setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
							return;
						}
						var to = currentRecord[phoneFieldIndex]?currentRecord[phoneFieldIndex].replace(/\D/g,''):"";
						if(to && filledMessage){
							
							sendBulkWhatsAppList[id]={"n":to,"m":getSanitizedContent(filledMessage),"u":"","dt":new Date().getTime()};
							if(attachmentId && isAttachmentAdded){
	        					sendBulkWhatsAppList[id+""].t = attachmentId;
	        				} 
	        				id++;   
						}
					}		
				});
				if(!Object.keys(sendBulkWhatsAppList).length){
					document.getElementById("ErrorText").innerText = "Phone field is empty or invalid for all chosen records.";
	        		document.getElementById("Error").style.display= "block";
					setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
					return;
				}
				// if(extensionVersion){
			 //        if(!isAllowed){
			 //            showAnnounceMent('purchase_license');
			 //        }else{
				// 		showAnnounceMent('maintenence');
				// 	}
			 //    }
			 //    else{ 
			 //        showAnnounceMent('install_chrome_extension');
			 //    }
				if(!!window.chrome && isAllowed && extensionVersion){
					let selectors= {"be":"#app .app-wrapper-web span","Re":"[data-icon=send]","In":'[data-animate-modal-body="true"]',"Im":"#side header img","at":'[title="Attach"]',"atIm":'[data-icon="attach-image-old"]',"atd":'[data-icon="attach-document-old"]',"om":".message-out","fb":'[data-icon="forward-chat"]',
"mb":"._2-aNW","fd":".G_MLO._2O_kh ._210SC ._357i8 span","fi":"input[type=file]"};
		        	chrome.runtime.sendMessage(editorExtensionId,{"message":"setSelectors","s":selectors},function (response){
						console.log(response);
						if (response.status != "success"){
						  // handleError(url); 
						   console.log(response);
						}
					});
					
    				var requestJson = sendBulkWhatsAppList;
	        		if(attachmentId && isAttachmentAdded){
	        			requestJson = {"message": "getAttachments","t":attachmentId,"sendBulkWhatsAppList":sendBulkWhatsAppList};
	        		}		
    				chrome.runtime.sendMessage(editorExtensionId,requestJson,function (response){
						console.log(response);
						if (response.status != "success"){
						  // handleError(url);
						   console.log(response);
						}
					});
					document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your Message has been sent successfully.</div>';
		      	    setTimeout(function(){document.getElementById("emailContentEmail").innerText="";document.getElementById("Error").style.display= "none"; }, 1500);

		        }
		        else {
					if(!extensionVersion){
						loadDebugInfo(":no-chrome-ext::uasr:"+navigator.userAgent);
					}
		          	var urlList="";
		          	bulkInsertList = sendBulkWhatsAppList;
					Object.keys(sendBulkWhatsAppList).forEach(function(history, index){
						var record = sendBulkWhatsAppList[history];
						var url = 'https://web.whatsapp.com/send?phone=' + record.n + '&text=' + encodeURIComponent(record.m);
						record.url = url;
						urlList = `${urlList}<div class="whatsappLink">
								<div class="contactName">${record.n} </div>
								<div class="sendButton" style="cursor:pointer;" onclick="sendWhatsapp(${history})" id="${history}_send"> Send Now</div>
								<div class="sendButton sent" id="${history}_sent"> Sent </div>
								<div class="sendButton sending" id="${history}_sending"> Sending</div>
							</div>`
					});
					$("#whatsAppUrlList").append(urlList);
					document.getElementById("Error").style.display= "none";
					document.getElementById("container").style.display= "none";
					document.getElementById("whatsAppUrlList").style.display= "block";
		        }
			}		
        }
        function sendWhatsapp(index){
        	recordMap =bulkInsertList[index];
        	document.getElementById(index+"_send").style.display="none";
        	document.getElementById(index+"_sending").style.display="inline-block";
        	ZOHO.CRM.API.insertRecord({Entity:"whatsappforzohocrm__WhatsApp_History",APIData:recordMap,Trigger:["workflow"]}).then(function(data){
        	 	document.getElementById(index+"_sending").style.display="none";
        	 	document.getElementById(index+"_sent").style.display="inline-block";
        	 	window.open(recordMap.url,"_blank");
        	});	
        }
        function getMessageWithFields(messageDetails,currentRecord){
        	var message = JSON.parse(JSON.stringify(messageDetails));
			var customerData=fields;
			for(let i =0;i<customerData.length;i++){
				if(customerData[i]){
					var replace = "\\${"+customerData[i]+"}";
					var re = new RegExp(replace,"g");
					if(currentRecord[i] != null)
					{
						message = message.replace(re,currentRecord[i]);
					}
					else
					{
						message = message.toString().replace(re," ");
					}
				}		
			}
			return message;
        }
		function googleTranslateElementInit() {
		  new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
		}
        function addListItem(id,text,className,value){
			if(className == "dropdown-item"){
				var linode = '<li class="'+className+'"><button class="'+className+'" onclick="insert(this)">'+text+'<input type="hidden" value="'+value+'"></button></li>';
			}
			else{
				var linode = '<li class="'+className+'">'+text+'</li>';
			}
			$('#'+id).append(linode);

        }
		function styling(tag)
		{
			document.execCommand(tag);
		}
		function link(){
			$("#linkForm").slideToggle("slow");
		}
		function image(){
			$("#imageForm").slideToggle("slow");
		}
		function addLink(){
			var href = document.getElementById("linkUrl").value;
		    if (range) {
				if(range.startOffset == range.endOffset){
					if(range.commonAncestorContainer.parentNode.href){
						range.commonAncestorContainer.parentNode.href=href;
					}
					else{
						var span = document.createElement('a');
						span.setAttribute('href',href);
						span.innerText = href;
						range.insertNode(span);
			        	range.setStartAfter(span);
			        }	
				}
				else{
					var data = range.commonAncestorContainer.data;
					var start = range.startOffset;
					var end = range.endOffset;
					range.commonAncestorContainer.data="";
					var span = document.createElement('span');
					span.appendChild( document.createTextNode(data.substring(0,start)) );
					var atag = document.createElement('a');
					atag.setAttribute('href',href);
					atag.innerText = data.substring(start,end);
					span.appendChild(atag);
					span.appendChild( document.createTextNode(data.substring(end)) );
					range.insertNode(span);
		        	range.setStartAfter(span);
				}
		        range.collapse(true);
		    }
			$("#linkForm").slideToggle("slow");
		}
		function addImage(){
			var href = document.getElementById("imageUrl").value;
			var span = document.createElement('img');
			span.setAttribute('src',href);
			span.innerText = href;
			range.insertNode(span);
        	range.setStartAfter(span);
			$("#imageForm").slideToggle("slow");
		}
		function openlink(){
			sel = window.getSelection();
		    if (sel && sel.rangeCount) {
		        range = sel.getRangeAt(0);
		      }  
			if(range && range.commonAncestorContainer.wholeText){
				if(range.commonAncestorContainer.parentNode.href){
					document.getElementById("linkUrl").value = range.commonAncestorContainer.parentNode.href;
					$("#linkForm").slideToggle("slow");
				}
			}	
		}
		function insert(bookingLink){
    		// var bookingLink = this;
			var range;

			if (sel && sel.rangeCount && isDescendant(sel.focusNode)){
		        range = sel.getRangeAt(0);
		        range.collapse(true);
    		    var span = document.createElement("span");
    		    span.appendChild( document.createTextNode('${'+bookingLink.children[0].value+'}') );
        		range.insertNode(span);
	    		range.setStartAfter(span);
		        range.collapse(true);
		        sel.removeAllRanges();
		        sel.addRange(range);
		    }    
		}
		function isDescendant(child) {
			var parent = document.getElementById("emailContentEmail");
		     var node = child.parentNode;
		     while (node != null) {
		         if (node == parent || child == parent) {
		             return true;
		         }
		         node = node.parentNode;
		     }
		     return false;
		}
		function enableSchedule(element){
			if(element.checked == true){
				document.getElementById("send").innerText="Schedule";
				var date = document.getElementById("datepicker").value;
    			var time = document.getElementById("timeList").value;
    			scheduledTime = new Date(date+" "+time).toISOString();
			}
			else{
				document.getElementById("send").innerText="Send";
				scheduledTime = undefined;
			}
		}
		function openDatePicker(){
    		document.getElementById("dateTime").style.display= "block";
    		if(ButtonPosition == "DetailView"){
    			document.getElementById("dateTime").style.top= "84%";
    		}
    		else{
    			document.getElementById("dateTime").style.top= "60%";
    		}
    		document.getElementById("Error").style.display= "block";
		}	
		function scheduleClose(){
			var date = document.getElementById("datepicker").value;
    		var time = document.getElementById("timeList").value;
    		if(new Date(date+" "+time).getTime() < new Date().getTime()){
    			document.getElementById("ErrorText").innerText = "Schedule time should be in future.";
    		}
    		else{
    			document.getElementById("ErrorText").innerText = "";
	    		document.getElementById("dateTime").style.display= "none";
	    		document.getElementById("Error").style.display= "none";
	    		document.getElementById("scheduleCheck").checked =true;
	    		document.getElementById("send").innerText="Schedule";
	    		document.getElementById("scheduledDateTime").innerText=new Date(date).toDateString()+" at "+time +" ("+Intl.DateTimeFormat().resolvedOptions().timeZone+")";
	    		scheduledTime = new Date(date+" "+time).toISOString();
	    	}	
		}
		function cancel(){
			document.getElementById("Error").style.display= "none";
		}
		function getSanitizedContent(msg){
			if(extensionVersion){
				$("body").append(`<img style="display:none" src="https://us-central1-slackappp-39d3d.cloudfunctions.net/zdeskww?debug=wawebext-bulk::cx-mail:${userEmail}:::-cexv:${extensionVersion.version}::" />`);
			}
			if(extensionVersion && parseFloat(extensionVersion.version) > 8){
				console.log('extension version is > 8');
				return encodeURIComponent(msg);
			}
			else{
				console.log('extension version is not > 8');
				return msg;
			}
		}

		function setSeenAlert(alertId){
			$('.extensionVersionCheck').hide();
			try {
              localStorage.setItem("alertseen", alertId);
            }
            catch(err) {
                loadDebugInfo(":unable-to-set-alertseen:");
            }
		}


		function showForcedAlert(){
		    // var seenalert = "";
		    // try {
            //     seenalert = localStorage.getItem("alertseen");
            // }
            // catch(err) {
            //     loadDebugInfo(":unable-to-get-alertseen:");
            // }
			// if(seenalert!=="1.0"){
			// 	$("body").append(`
			// 		<div class="extensionVersionCheck">
			// 			<div class="alerttl">New Chrome Extension Version</div>
			// 				Kindly ensure you have the latest version of our chrome extension.  <br><br>
			// 				If you have installed our chrome extension, old chrome extension will not work. Kindly remove and install new chrome extension.
			// 			<br><br>
			// 			<span style="color:green">Latest Version : 9.0</span>
			// 			<br><br>
			// 			<a href="https://chrome.google.com/webstore/detail/wa-web-for-zoho-crm-bulk/nkihbgbbbakefbgjnokdicilnhcdeckp" target="_blank">New Version Extension Link</a> <br><br><div class="closealertdia" onclick="$('.extensionVersionCheck').hide()">Close alert</div><div class="closealertdia" onclick="setSeenAlert('1.0')">Done &amp; Never Show Again</div>
						
			// 			</div>
			// 	`);
			// }
		}

		function loadDebugInfo(txt){
			
			$("body").append(`<img style="display:none" src="https://us-central1-slackappp-39d3d.cloudfunctions.net/zdeskww?debug=wawebext-bulk::cx-mail:${userEmail}:::-cexv:${encodeURIComponent(txt)}::" />`)
		}


		function executeFirebaseThings(){
			try{
				firebase.initializeApp(firebaseConfig);
				//firebase.analytics();
				db = firebase.firestore();
				db.collection('appActivityLog').doc(userEmail).collection('history').add({
					't' : Date.now(),
					'a': 'WWBULK-TEST',
					'cev' : extensionVersion ? extensionVersion.version : '-'
				});
			}
			catch(err){
				console.log(err);
			}
		}

		var s = document.createElement("script");
		s.type = "text/javascript";
		s.src = "https://www.gstatic.com/firebasejs/7.15.2/firebase-app.js";
		document.head.appendChild(s);
		
		var fko = document.createElement("script");
		fko.type = "text/javascript";
		fko.src = "https://www.gstatic.com/firebasejs/7.15.3/firebase-firestore.js";
		document.head.appendChild(fko);


		var firebaseConfig = {
			apiKey: "AIzaSyB_NTkxQH12sAiB-F0mVIm89uA2VirabC4",
			projectId: "wawebbulk",
			storageBucket: "wawebbulk.appspot.com",
			messagingSenderId: "218291591421",
			appId: "1:218291591421:web:3ca4c3a172add3e7d0c694",
			measurementId: "G-MMCYRRQPPG"
		};


		//document.addEventListener("DOMContentLoaded", function(event) { 
			
//   $("body").append(`<!-- The core Firebase JS SDK is always required and must be listed first -->
// <script src="https://www.gstatic.com/firebasejs/7.19.0/firebase-app.js"></script>

// <!-- TODO: Add SDKs for Firebase products that you want to use
//      https://firebase.google.com/docs/web/setup#available-libraries -->
// <script src="https://www.gstatic.com/firebasejs/7.19.0/firebase-analytics.js"></script>

// <script>
//   // Your web app's Firebase configuration
//   
//   // Initialize Firebase
//   firebase.initializeApp(firebaseConfig);
//   firebase.analytics();
// </script>`);
//});

		

