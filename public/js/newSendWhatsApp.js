// var emailContentText = "Hi ,\n\nHope you're well. I'm writing to know how our business can help you.\n\nPlease choose a event to schedule an appointment:\n\n${eventsUrls} See you soon!\n\n %ownerName% \n\n %companyName%";
		var emailContent = [];
		var emailContentText = "";
		var startIndex =0;
		var endIndex = 0;
		var currentEditor="";
		var subject="";
		var sel;
		var range;
		var calendarsMap;
		var recordId;
		var currentRecord;
		var recordModule;
		var ButtonPosition;
		var smsTemplates;
		var templateId;
		var scheduledTime;
		var Mobile;
		var dealContactId;
		var dealAccountId;
		var recipientName;
		var moduleFields;
		var userFields;
		var historyFields;
		var isAllowed = false;
		var module_name = "CustomModule";
		var orgId;
		var defaultModules = ["Contacts","Leads","Accounts","Vendors"];
		var singleCountryCode;
		var countryCode;
		var extensionVersion = "4.0";
		var reply;
		var userEmail = null;
		var peySts;
		var zsckey;
		var phoneFieldsSetting={};
		var fromPhoneNumber="";
		var editorExtensionId = "nkihbgbbbakefbgjnokdicilnhcdeckp";
		var origin = ".com";
		var waMsgCrdleft = 0;
		var customPhoneFields = [];
		var popupLocation = "single-message";
        var attPur = false;
		var loginUserId=null;
		var isLoginUser = false;

		var givenUserId = null;
		var currentRecords=[];
		var attachedfile=null;
		try{
			if(window.location.ancestorOrigins && window.location.ancestorOrigins[0]){
				origin = window.location.ancestorOrigins[0];
				if(origin.indexOf("crmplus") != -1){
					origin=origin.substring("https://crmplus.zoho".length);
				}
				else{
					origin=origin.substring("https://crm.zoho".length);
				}
			}			
		}
		catch(e){
			console.log(e);
		}		
		var pluginurl="https://platform.zoho"+origin+"/crm/v2/functions/whatsappforzohocrm__sendwhatsapp/actions/execute?auth_type=apikey&zapikey=";
        document.addEventListener("DOMContentLoaded", function(event) {
        	if(window.location.href.indexOf("https://ulgebra-cdn-cicd-crm-msgbrd.gitlab.io") == -1 && window.location.href.indexOf("http://localhost:8000") == -1 ){
			    showNewUpdateLink();
			    return;
			}
        	var timeList=["12:00 AM","12:15 AM","12:30 AM","12:45 AM","01:00 AM","01:15 AM","01:30 AM","01:45 AM","02:00 AM","02:15 AM","02:30 AM","02:45 AM","03:00 AM","03:15 AM","03:30 AM","03:45 AM","04:00 AM","04:15 AM","04:30 AM","04:45 AM","05:00 AM","05:15 AM","05:30 AM","05:45 AM","06:00 AM","06:15 AM","06:30 AM","06:45 AM","07:00 AM","07:15 AM","07:30 AM","07:45 AM","08:00 AM","08:15 AM","08:30 AM","08:45 AM","09:00 AM","09:15 AM","09:30 AM","09:45 AM","10:00 AM","10:15 AM","10:30 AM","10:45 AM","11:00 AM","11:15 AM","11:30 AM","11:45 AM","12:00 PM","12:15 PM","12:30 PM","12:45 PM","01:00 PM","01:15 PM","01:30 PM","01:45 PM","02:00 PM","02:15 PM","02:30 PM","02:45 PM","03:00 PM","03:15 PM","03:30 PM","03:45 PM","04:00 PM","04:15 PM","04:30 PM","04:45 PM","05:00 PM","05:15 PM","05:30 PM","05:45 PM","06:00 PM","06:15 PM","06:30 PM","06:45 PM","07:00 PM","07:15 PM","07:30 PM","07:45 PM","08:00 PM","08:15 PM","08:30 PM","08:45 PM","09:00 PM","09:15 PM","09:30 PM","09:45 PM","10:00 PM","10:15 PM","10:30 PM","10:45 PM","11:00 PM","11:15 PM","11:30 PM","11:45 PM"];
	        var timeOptions ="";
	        timeList.forEach(function(time){
	             timeOptions = timeOptions +"<option  value='"+time+"'>"+time+"</option>"
	        });
	        $('#timeList').append(timeOptions);
	        $('#datepicker').datepicker().datepicker('setDate',new Date());
	        var date = document.getElementById("datepicker").value;
	        var time = document.getElementById("timeList").value;
	        document.getElementById("scheduledDateTime").innerText=new Date(date).toDateString()+" at "+time +" ("+Intl.DateTimeFormat().resolvedOptions().timeZone+")";

        	// var cusotmerData = ["Owner", "Email", "$currency_symbol", "Other_Phone", "Mailing_State", "$upcoming_activity", "Other_State", "Other_Country", "Last_Activity_Time", "Department", "$process_flow", "Assistant", "Mailing_Country", "id", "$approved", "Reporting_To", "$approval", "Other_City", "Created_Time", "$editable", "Home_Phone", "$status", "Created_By", "Secondary_Email", "Description", "Vendor_Name", "Mailing_Zip", "$photo_id", "Twitter", "Other_Zip", "Mailing_Street", "Salutation", "First_Name", "Full_Name", "Asst_Phone", "Record_Image", "Modified_By", "Skype_ID", "Phone", "Account_Name", "Email_Opt_Out", "Modified_Time", "Date_of_Birth", "Mailing_City", "Title", "Other_Street", "Mobile", "Last_Name", "Lead_Source", "Tag", "Fax"];
        	var cusotmerData = ["Contact Id","Account Name", "Assistant", "Asst Phone", "Owner", "Created By", "Created Time", "Date of Birth", "Department", "Description", "Email", "Email Opt Out", "Fax", "First Name", "Full Name", "Home Phone", "Last Activity Time", "Last Name", "Lead Source", "Mailing City", "Mailing Country", "Mailing State", "Mailing Street", "Mailing Zip", "Mobile", "Modified By", "Modified Time", "Other City", "Other Country", "Other Phone", "Other State", "Other Street", "Other Zip", "Phone", "Record Image", "Reporting To", "Salutation", "Secondary Email", "Skype ID", "Title", "Twitter", "Vendor Name"];
			cusotmerData.forEach(function(field){
				addListItem("dropdown-menu-email",field,"dropdown-item","Contacts."+field);
			});	
			if(!!window.chrome){
				showForcedAlert();
			}
            showAnnounceMent('install_chrome_extension');
        	ZOHO.embeddedApp.on("PageLoad", function(record) {
               	recordId = record.EntityId;
               	recordModule = record.Entity;
               	ButtonPosition = record.ButtonPosition;
               	if(record.ButtonPosition == "DetailView"){
	               	ZOHO.CRM.UI.Resize({height:"570",width:"730"}).then(function(data){
						console.log(data);
					});
				}	
               	var getmap = {"nameSpace":"<portal_name.extension_namespace>"};
               	Promise.all([ZOHO.CRM.META.getModules(),ZOHO.CRM.CONFIG.getOrgInfo(),ZOHO.CRM.CONFIG.getCurrentUser(),ZOHO.CRM.CONNECTOR.invokeAPI("crm.zapikey",getmap)]).then(function(data){
               		data[0].modules.forEach(function(module){
               			if(module.api_name == "whatsappforzohocrm__WhatsApp_History"){
               				module_name = module.module_name;
               			}
               		});
               		orgId = data[1].org[0].zgid;
               		zsckey =JSON.parse(data[3]).response;
               		pluginurl = pluginurl+JSON.parse(data[3]).response;
					userEmail = data[2].users[0].email;
					loginUserId = data[2].users[0].id;
				    executeFirebaseThings();
               		
				});
    //            	ZOHO.CRM.API.getOrgVariable("whatsappforzohocrm__openwithwebordesktop").then(function(apiKeyData){
				// 	if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content){
				// 		var value = apiKeyData.Success.Content;
				// 		document.getElementById("web").checked = (value=="web");
    // 					document.getElementById("desktop").checked =(value=="desktop");
				// 	}
				// });
				ZOHO.CRM.META.getFields({"Entity":"Users"}).then(function(data){
					userFields = data.fields;
					data.fields.forEach(function(field){
						addListItem("dropdown-menu-user",field.field_label,"dropdown-item","Users."+field.field_label);
					});
				});	
				ZOHO.CRM.META.getFields({"Entity":"whatsappforzohocrm__WhatsApp_History"}).then(function(data){
					historyFields = data.fields;
				});	
				
               	ZOHO.CRM.API.searchRecord({Entity:"whatsappforzohocrm__WhatsApp_Templates",Type:"criteria",Query:"(whatsappforzohocrm__Module_Name:equals:"+recordModule+")",delay:false})
				.then(function(data){
					smsTemplates = data.data;
					var templateList="";
					if(data.data){
						for(let i=0;i <data.data.length;i++){
							templateList =templateList+ '<li class="templateItem" id="'+data.data[i].id+'" onclick="showsms(this)"></li>';
						}
						$('#templateList').append(templateList);
						for(let i=0;i <data.data.length;i++){
							document.getElementById(data.data[i].id).innerText = data.data[i].Name;
						}
					}
					else{
						$('#templateList').append('<li style="text-align:center;">No Templates</li>');
					}
				});			
                ZOHO.CRM.API.getRecord({Entity:recordModule,RecordID:recordId[0]})
				.then(function(data2){
					currentRecord = data2.data[0];
					currentRecords.push(currentRecord);
					selectModule(recordModule,data2.data[0]);
				});
				Promise.all([ZOHO.CRM.API.getOrgVariable("whatsappforzohocrm__userId"),ZOHO.CRM.API.getOrgVariable("whatsappforzohocrm__apikey")]).then(function(apiKeyData){
                    var userId = apiKeyData[0];
                    var apiKey = apiKeyData[1];
                    if(apiKey && apiKey.Success && apiKey.Success.Content && userId && userId.Success && userId.Success.Content){
                    	givenUserId = userId.Success.Content;
                        // document.getElementById("scheduleFeature").style.display="block";
                    }
                });
	        });
        	ZOHO.embeddedApp.init();
        	const el = document.getElementById('emailContentEmail');

			el.addEventListener('paste', (e) => {
			  // Get user's pasted data
			  let data = e.clipboardData.getData('text/html') ||
			      e.clipboardData.getData('text/plain');
			  
			  // Filter out everything except simple text and allowable HTML elements
			  let regex = /<(?!(\/\s*)?()[>,\s])([^>])*>/g;
			  data = data.replace(regex, '');
			  
			  // Insert the filtered content
			  document.execCommand('insertHTML', false, data);

			  // Prevent the standard paste behavior
			  e.preventDefault();
			});
			var content_id = 'emailContentEmail';  
			max = 3000;
			//binding keyup/down events on the contenteditable div
			$('#'+content_id).keyup(function(e){ check_charcount(content_id, max, e); });
			$('#'+content_id).keydown(function(e){ check_charcount(content_id, max, e); });

			function check_charcount(content_id, max, e)
			{   
			    if(e.which != 8 && $('#'+content_id).text().length > max)
			    {
			    	document.getElementById("ErrorText").innerText = "Message should be within 3000 characters.";
	        		document.getElementById("Error").style.display= "block";
	        		// document.getElementById("ErrorText").style.color="red";
					setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
			       // $('#'+content_id).text($('#'+content_id).text().substring(0, max));
			       e.preventDefault();
			    }
			}
        });

		function selectModule(module,record){
			document.getElementById("moduleFields").innerText = "Insert "+module+" Fields";
			var customerData = [];
			var phoneFields = [];	
			var phoneField; 
			Promise.all([ZOHO.CRM.META.getFields({"Entity":module}),ZOHO.CRM.API.getOrgVariable("whatsappforzohocrm__mobileNumberSettings")]).then(function(data){
				if(data[1] && data[1].Success && data[1].Success.Content){
					phoneFieldsSetting =JSON.parse(data[1].Success.Content);
					if(phoneFieldsSetting && phoneFieldsSetting.customPhoneFieldsMap && phoneFieldsSetting.customPhoneFieldsMap[recordModule]){
						customPhoneFields = phoneFieldsSetting.customPhoneFieldsMap[module];
					}
					if(phoneFieldsSetting["phoneField"]){
    					phoneField = phoneFieldsSetting["phoneField"][module];
					}
					isLoginUser = phoneFieldsSetting["isLoginUser"];
    				singleCountryCode = phoneFieldsSetting["singleCountryCode"]?phoneFieldsSetting["singleCountryCode"]:"";
				}
				moduleFields = data[0].fields;
				data[0].fields.forEach(function(field){
					customerData.push(field.field_label);
				})
				document.getElementById("dropdown-menu-email").innerHTML="";
				customerData.forEach(function(field){
					addListItem("dropdown-menu-email",field,"dropdown-item",module+"."+field);
				});	
				var NumberList = "";
				var selectedNumber = "";
				var lookupModules=[];
				data[0].fields.forEach(function(field){
					if(record[field.api_name] != null){
						if(field.data_type == "phone" || customPhoneFields.indexOf(field.api_name) != -1){
							let number = record[field.api_name];
							number = number.replace(/\D/g,'');
							if(selectedNumber == "" || (phoneField && phoneField == field.api_name)){
								selectedNumber = field.field_label+'('+number+')';
							}
							NumberList =NumberList+ '<li class="templateItem" onclick="setNumber(this)">'+field.field_label+'('+number+') </li>';
						}
						else if(field.data_type == "lookup" && defaultModules.indexOf(field.lookup.module.api_name) != -1){
							lookupModules.push(field);
						}	
					}	
				});	
				if(defaultModules.indexOf(module) != -1){
					if(NumberList == ""){
						document.getElementById("ErrorText").innerText = "Mobile field is empty.";
						document.getElementById("Error").style.display= "block";
					}
					else{
						setNumber({"innerText":selectedNumber});
					}
					$('#NumberList').append(NumberList);
					document.getElementById("loader").style.display= "none";
					document.getElementById("container").style.display= "block";
				}	
				else{
					var fetchList =[];
					lookupModules.forEach(function(lookupField){
						fetchList.push(ZOHO.CRM.META.getFields({"Entity":lookupField.lookup.module.api_name}),ZOHO.CRM.API.getRecord({Entity:lookupField.lookup.module.api_name,RecordID:record[lookupField.api_name].id}));
					});
					Promise.all(fetchList).then(function(dataFields){
						for(let i=0;i<lookupModules.length;i++){
							dataFields[2*i+0].fields.forEach(function(field){
								if(field.data_type == "phone" && dataFields[2*i+1].data[0][field.api_name] != null){
									let number = dataFields[2*i+1].data[0][field.api_name];
									number = number.replace(/\D/g,'');
									if(selectedNumber == "" || (phoneField && phoneField == lookupModules[i].lookup.module.api_name+"."+field.api_name)){
										selectedNumber = lookupModules[i].lookup.module.api_name+' '+field.field_label+'('+number+')';
									}
									NumberList =NumberList+ '<li class="templateItem" onclick="setNumber(this)">'+lookupModules[i].lookup.module.api_name+' '+field.field_label+'('+number+') </li>';
								}	
							});	
						}
						if(NumberList == ""){
							document.getElementById("ErrorText").innerText = "Mobile field is empty.";
							document.getElementById("Error").style.display= "block";
						}
						else{
							setNumber({"innerText":selectedNumber});
						}
						$('#NumberList').append(NumberList);
						document.getElementById("loader").style.display= "none";
						document.getElementById("container").style.display= "block";
					});
				}

			});	
		}
		function setNumber(editor){
			var selectedNumber =editor.innerText.substring(editor.innerText.indexOf("(")+1,editor.innerText.indexOf(")"));
			if(selectedNumber){
    			selectedNumber = selectedNumber.replace(/\D/g,'');
				checkPhoneNumber(selectedNumber);
			}	
			document.getElementById("selectedNumber").innerText =  editor.innerText;
			document.getElementById("editNumber").value = selectedNumber;
		}
		function selectCountryCode(countryPrefix){
			countryCode = countryPrefix;
			saveCountryCode(countryCode);
		}
		function saveCountryCode(singleCountryCode){
	         phoneFieldsSetting["singleCountryCode"]=singleCountryCode;
	         updateOrgVariables("whatsappforzohocrm__mobileNumberSettings",phoneFieldsSetting);
	    }
		function checkPhoneNumber(no){
			var request ={
				url : "https://rest.messagebird.com/lookup/" + no,
				headers:{
					Authorization:"AccessKey 7NMPor0R8DofSHH61SpViNNqQ",
				}
			}
			return ZOHO.CRM.HTTP.get(request).then(function(phoneData){
				if(JSON.parse(phoneData).countryPrefix != null){
					isValidPhone =  true;
		    		var phoneNumber = JSON.parse(phoneData).phoneNumber.toString();
		    		countryCode = JSON.parse(phoneData).countryPrefix.toString();
		    		no = phoneNumber.substring(phoneNumber.indexOf(countryCode)+countryCode.length);
		    	}
		    	else{
		    		no = (no && no[0] == "0")?no.substring(1):no;
		    		countryCode = singleCountryCode;
		    	}
		    	document.getElementById("countryCode").value = countryCode;
				document.getElementById("editNumber").value =  no;
			}); 
		}
        function updateOrgVariables(apiname,value,key){
    		if(apiname == "whatsappforzohocrm__openwithwebordesktop"){
    			document.getElementById("web").checked = (value=="web");
    			document.getElementById("desktop").checked =(value=="desktop");
    		}
    		ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": apiname,"value": value});
        }
        function showsms(editor){
			for(var i=0; i<smsTemplates.length;i++){
				if(smsTemplates[i].id == editor.id){
					templateId = smsTemplates[i].id;
					document.getElementById("selectedTemplate").innerText = smsTemplates[i].Name;
					
					document.getElementById("emailContentEmail").innerText = smsTemplates[i].whatsappforzohocrm__WhatsApp_Message;
					break;
				}
			}
		}

		function getSanitizedContent(msg){
			if(extensionVersion){
				$("body").append(`<img style="display:none" src="https://us-central1-slackappp-39d3d.cloudfunctions.net/zdeskww?debug=wawebext-single::cx-mail:${userEmail}:::-cexv:${extensionVersion.version}::" />`);
			}
			if(extensionVersion && parseFloat(extensionVersion.version) > 8){
				console.log('extension version is > 8');
				return encodeURIComponent(msg);
			}
			else{
				console.log('extension version is not > 8');
				return msg;
			}
		}

		function loadDebugInfo(txt){
			$("body").append(`<img style="display:none" src="https://us-central1-slackappp-39d3d.cloudfunctions.net/zdeskww?debug=wawebext-single::cx-mail:${userEmail}:::-cexv:${encodeURIComponent(txt)}::" />`)
		}
		
		var sendBtnClickedForProceed = false;
        function sendSMS(){
        	if(!chosenWASenderId){
        		sendBtnClickedForProceed = true;
        		$('.fromNumChooserOuter').fadeIn();
        		return;
        	}
        	
        	var sendBulkWhatsAppList={};
        	var Mobile = document.getElementById("editNumber").value;
			if(Mobile){
				Mobile = Mobile.replace(/\D/g,'');
				Mobile = countryCode+""+Mobile;
				if(countryCode === "254" && Mobile.indexOf("2540") === 0){
                    Mobile = "254"+Mobile.substring(4);
                }
                else if(countryCode === "54" && Mobile.indexOf("549") !== 0){
                    Mobile = "549"+Mobile.substring(2);
                }
                else if(countryCode === "52" && Mobile.indexOf("521") !== 0){
                    Mobile = "521"+Mobile.substring(2);
                }
			} 	
        	var toModule = recordModule?document.getElementById("selectedNumber").innerText.substring(0,7):"";
        	if(toModule == "Contact"){
        		var toId = dealContactId;
        	}
        	else if(toModule == "Account"){
        		var toId = dealAccountId;
        	}
        	// Mobile = Mobile.substring(Mobile.indexOf("(")+1,Mobile.indexOf(")"));
            var func_name = "whatsappforzohocrm__sendwhatsapp";
        	var date = document.getElementById("datepicker").value;
    		var time = document.getElementById("timeList").value;
    		if(document.getElementById("emailContentEmail").innerText.length >3000){
    			document.getElementById("ErrorText").innerText = "Message should be within 3000 characters.";
        		document.getElementById("Error").style.display= "block";
        		document.getElementById("Error").style.color="red";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
    		}
        	else if(document.getElementById("emailContentEmail").innerText.replace(/\n/g,"").replace(/\t/g,"").replace(/ /g,"") == ""){
        		document.getElementById("ErrorText").innerText = "Message cannot be empty.";
        		document.getElementById("Error").style.display= "block";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
        	}
        	else if(scheduledTime && new Date(date+" "+time).getTime() < new Date().getTime()){
	            document.getElementById("ErrorText").innerText = "Schedule time should be in future.";
	            document.getElementById("Error").style.display= "block";
	            setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
	        }
        	else if(func_name == "whatsappforzohocrm__sendwhatsapp" && (Mobile == null || Mobile == "")){
        		document.getElementById("ErrorText").innerText = "Mobile field is empty.";
		        document.getElementById("Error").style.display= "block";
		        setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
        	}
        	else{

        		var notes = "";
	        	document.getElementById("ErrorText").innerText = "Sending... "+notes;
				document.getElementById("Error").style.display= "block";
	        	var message = document.getElementById("emailContentEmail").innerText;
	        	// message =message.replace(/<b>/g, "*").replace(/<\/b>/g, "*").replace(/<strike>/g, "~").replace(/<\/strike>/g, "~");
	        	recordId = recordId[0];
	    		
				var arguments = {
					"action": "sendSMS",	
					"message" : message,
					"recordModule" : recordModule,
					"scheduledTime":scheduledTime,
					"templateId":templateId,
					"recordId" : recordId,
					"to":Mobile,
					"toModule":toModule,
					"toId":toId
				};
				getMessageWithFields(arguments).then(function(message){
					if(!recipientName && currentRecord.Name){
						recipientName = currentRecord.Name;
					}
					if(recipientName){
						var name = "WhatsApp to " + recipientName;
					}
					else{
						var name = "WhatsApp to " + Mobile;
					}
					var req_data={"Name":name,"whatsappforzohocrm__WhatsApp_Message":message,"whatsappforzohocrm__Recipient_Number":Mobile,"whatsappforzohocrm__Sender_Phone":fromPhoneNumber,"whatsappforzohocrm__Module":recordModule,"whatsappforzohocrm__Recipient_Id":recordId,"whatsappforzohocrm__Status":"Sent"};
					if(scheduledTime != null)
                    {
                        var time = scheduledTime.substring(0,19) + "+00:00";
                        req_data["whatsappforzohocrm__Scheduled_Time"]=time.toString();  
                        req_data["whatsappforzohocrm__Status"]="Scheduled";
                    }
					historyFields.forEach(function(field){
						if(field.data_type == "lookup" && field.lookup.module.api_name == recordModule){
							req_data[field.api_name]=recordId;
						}
					});
					if(toModule && toId){
						req_data["whatsappforzohocrm__"+toModule]=toId;
					}
					ZOHO.CRM.API.insertRecord({Entity:"whatsappforzohocrm__WhatsApp_History",APIData:req_data,Trigger:["workflow"]}).then(function(response){
						var responseInfo	= response.data[0];
						var resCode			= responseInfo.code;
						var licEnabled = isAllowed;
						isAllowed = waMsgCrdleft > 0;
						if(resCode == 'SUCCESS'){
                            if(scheduledTime){
                                document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your Messages have been Scheduled successfully.</div>';
                                setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload(); }, 1500);
                                return;
                            }    
							var details = responseInfo.details;
							if(chosenWASenderId!=="whatsapp-web"){
								var mediaObj = null;
                                var user = whatcetraApp.auth().currentUser;  
                                var agent = {
                                    name: user.displayName,
                                    email: user.email,
                                    id:user.uid,
                                    photoUrl:user.photoURL
                                }
                                if(attPur && attachedfile && attachedfile.mediaUrl){
                                    mediaObj = {
                                        "url": attachedfile.mediaUrl,
                                        "name": attachedfile.fileMeta.name,
                                        "type": attachedfile.fileMeta.type,
                                        "size": attachedfile.fileMeta.size
                                    }    
                                    attachedfile = null;
                                }   
                                sendBulkWhatsAppList[details.id+""]={"contactNumber":req_data.whatsappforzohocrm__Recipient_Number,"text":req_data.whatsappforzohocrm__WhatsApp_Message,"createdTime":new Date().getTime(),"agent":agent,"media":mediaObj};
                                addToWhatcetraQueue(sendBulkWhatsAppList);
							}
							else if(isAllowed  && reply) {
								extensionVersion = reply;
								
								if(module_name && orgId){
				        			var url = "https://crm.zoho.com/crm/org"+orgId+"/tab/"+module_name+"/"+details.id;
				        		}
								sendBulkWhatsAppList[details.id+""]={"n":req_data.whatsappforzohocrm__Recipient_Number,"m":getSanitizedContent(req_data.whatsappforzohocrm__WhatsApp_Message),"u":url,"dt":new Date().getTime()};
								logSUBActivityLog(1);
								$.ajax({url: "https://us-central1-wawebbulk.cloudfunctions.net/updateLICCA?src=crm-wa-single&email="+userEmail+"&mcc=1", success: function(result){
									console.log(result);
								}});
								//sendBulkWhatsAppList["queueIsStopped"]=false;
								chrome.runtime.sendMessage(editorExtensionId, sendBulkWhatsAppList,function(response) {
									if (!response.success){
									  //handleError(url);
									}
								});
								
								document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your Message has been sent successfully in the background <br> via Chrome Extension '+(licEnabled ? '': '(7 days free trial enabled)')+'.</div>';
								setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload(); }, 1500);
							}
							else{
								if(!reply){
									loadDebugInfo(":no-chrome-ext::uasr:"+navigator.userAgent);
								}

								Mobile = Mobile.replace(/\D/g,'');
								if(document.getElementById("web").checked){
									var url = "https://web.whatsapp.com/send?phone=" + Mobile + "&text=" + encodeURIComponent(message);
								}
								else{
									var url = "https://wa.me/" + Mobile + "?text=" + encodeURIComponent(message);
								}
								window.open(url,"_blank");
								document.getElementById("Error").style.display= "none";
								setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload(); }, 1000);
							}	
						}
						else{
							document.getElementById("ErrorText").innerText = "Opps! Something went wrong from server side. Please try after sometimes!!!";
				    		document.getElementById("Error").style.display= "block";
							setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
						}
					});
				});	
			}		
        }
        function getMessageWithFields(messageDetails){
        	var message = messageDetails.message;
			var to = messageDetails.to.replace(/[^0-9]/g, "").replace(/ /g, "");
			var customerData=[];
			var module = messageDetails.recordModule;
			if(messageDetails.recordModule == "Leads")
			{
				customerData = ["Lead Id","Annual Revenue","City","Company","Country","Created By","Created Time","Description","Designation","Email","Email Opt Out","Fax","First Name","Full Name","Industry","Last Activity Time","Last Name","Lead Source","Lead Status","Mobile","Modified By","Modified Time","No of Employees","Owner","Phone","Rating","Record Image","Salutation","Secondary Email","Skype ID","State","Street","Tag","Twitter","Website","Zip Code"];
			}
			else if(messageDetails.recordModule == "Contacts")
			{
				customerData = ["Contact Id","Account Name","Assistant","Asst Phone","Owner","Created By","Created Time","Date of Birth","Department","Description","Email","Email Opt Out","Fax","First Name","Full Name","Home Phone","Last Activity Time","Last Name","Lead Source","Mailing City","Mailing Country","Mailing State","Mailing Street","Mailing Zip","Mobile","Modified By","Modified Time","Other City","Other Country","Other Phone","Other State","Other Street","Other Zip","Phone","Record Image","Reporting To","Salutation","Secondary Email","Skype ID","Title","Twitter","Vendor Name"];
			}
			else if(messageDetails.recordModule == "Accounts")
			{
				customerData = ["Account Id","Account Name","Account Number","Account Site","Account Type","Annual Revenue","Billing City","Billing Code","Billing Country","Billing State","Billing Street","Created By","Created Time","Description","Employees","Fax","Industry","Last Activity Time","Modified By","Modified Time","Owner","Ownership","Parent Account","Phone","Rating","Record Image","SIC Code","Shipping City","Shipping Code","Shipping Country","Shipping State","Shipping Street","Tag","Ticker Symbol","Website"];
			}
			else if(messageDetails.recordModule == "Deals")
			{
				customerData = ["Deal Id","Account Name","Amount","Campaign Source","Closing Date","Contact Name","Created By","Created Time","Deal Name","Description","Expected Revenue","Last Activity Time","Lead Conversion Time","Lead Source","Modified By","Modified Time","Next Step","Overall Sales Duration","Owner","Probability","Sales Cycle Duration","Stage","Tag","Type"];
			}
			moduleFields.forEach(function(field){
				try{
					var replace = "\\${"+module+"."+field.field_label.replace(/\s+/g, " ")+"}";
					var re = new RegExp(replace,"g");
					if(currentRecord[field.api_name] != null)
					{
						var value = currentRecord[field.api_name];
						if(field.data_type == "datetime"){
							value = new Date(value).toLocaleString();
						}
						if(value.name)
						{
							value = value.name;
						}
						
						message = message.replace(re,value);
					}
					else
					{
						message = message.toString().replace(re," ");
					}
				}catch(err){
								loadDebugInfo(":unable-to-replace-plc:"+err);
				}
			});	
			customerData.forEach(function(field){
				try{
				if(field == "Contact Id" || field == "Lead Id" || field == "Account Id" || field == "Deal Id")
				{
					var rfield = "id";
				}
				else
				{
					var rfield = field;
				}
				var replace = "\\${"+module+"."+field+"}";
				var re = new RegExp(replace,"g");
				if(currentRecord[rfield.replace(/ /g,"_")] != null)
				{
					var value = currentRecord[rfield.replace(/ /g,"_") + ""];
					if(value.name)
					{
						value = value.name;
					}
					
					message = message.replace(re,value);
				}
				else
				{
					message = message.toString().replace(re," ");
				}
				}catch(err){
					loadDebugInfo(":unable-to-replace-plc:"+err);
				}
			});
			var ownerId;
			if(isLoginUser && loginUserId){
				ownerId = loginUserId
			}
			else if(currentRecord.Owner != null)
			{
				ownerId = currentRecord.Owner.id;
			}
			if(ownerId != null)
			{
				return ZOHO.CRM.API.getUser({ID:ownerId}).then(function(data){
					if(data != "")
					{
						var user = data.users[0];
						userFields.forEach(function(field){
							try{
								var replace = "\\${Users."+field.field_label.replace(/\s+/g, " ")+"}";
								var re = new RegExp(replace,"g");
								if(user[field.api_name] != null)
								{
									var value = user[field.api_name];
									if(value.name)
									{
										value = value.name;
									}
									
									message = message.replace(re,value);
								}
								else
								{
									message = message.toString().replace(re," ");
								}
							}catch(err){
								loadDebugInfo(":unable-to-replace-plc:"+err);
							}
						});
						return message;
					}
				});	
			}
			else{
				return message;
			}
			
        }
		function googleTranslateElementInit() {
		  new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
		}
        function addListItem(id,text,className,value){
			if(className == "dropdown-item"){
				var linode = '<li class="'+className+'"><button class="'+className+'" onclick="insert(this)">'+text+'<input type="hidden" value="'+value+'"></button></li>';
			}
			else{
				var linode = '<li class="'+className+'">'+text+'</li>';
			}
			$('#'+id).append(linode);

        }
		function styling(tag)
		{
			document.execCommand(tag);
		}
		function link(){
			$("#linkForm").slideToggle("slow");
		}
		function image(){
			$("#imageForm").slideToggle("slow");
		}
		function addLink(){
			var href = document.getElementById("linkUrl").value;
		    if (range) {
				if(range.startOffset == range.endOffset){
					if(range.commonAncestorContainer.parentNode.href){
						range.commonAncestorContainer.parentNode.href=href;
					}
					else{
						var span = document.createElement('a');
						span.setAttribute('href',href);
						span.innerText = href;
						range.insertNode(span);
			        	range.setStartAfter(span);
			        }	
				}
				else{
					var data = range.commonAncestorContainer.data;
					var start = range.startOffset;
					var end = range.endOffset;
					range.commonAncestorContainer.data="";
					var span = document.createElement('span');
					span.appendChild( document.createTextNode(data.substring(0,start)) );
					var atag = document.createElement('a');
					atag.setAttribute('href',href);
					atag.innerText = data.substring(start,end);
					span.appendChild(atag);
					span.appendChild( document.createTextNode(data.substring(end)) );
					range.insertNode(span);
		        	range.setStartAfter(span);
				}
		        range.collapse(true);
		    }
			$("#linkForm").slideToggle("slow");
		}
		function addImage(){
			var href = document.getElementById("imageUrl").value;
			var span = document.createElement('img');
			span.setAttribute('src',href);
			span.innerText = href;
			range.insertNode(span);
        	range.setStartAfter(span);
			$("#imageForm").slideToggle("slow");
		}
		function openlink(){
			sel = window.getSelection();
		    if (sel && sel.rangeCount) {
		        range = sel.getRangeAt(0);
		      }  
			if(range && range.commonAncestorContainer.wholeText){
				if(range.commonAncestorContainer.parentNode.href){
					document.getElementById("linkUrl").value = range.commonAncestorContainer.parentNode.href;
					$("#linkForm").slideToggle("slow");
				}
			}	
		}
		function insert(bookingLink){
    		// var bookingLink = this;
			var range;

			if (sel && sel.rangeCount && isDescendant(sel.focusNode)){
		        range = sel.getRangeAt(0);
		        range.collapse(true);
    		    var span = document.createElement("span");
    		    span.appendChild( document.createTextNode('${'+bookingLink.children[0].value+'}') );
        		range.insertNode(span);
	    		range.setStartAfter(span);
		        range.collapse(true);
		        sel.removeAllRanges();
		        sel.addRange(range);
		    }    
		}
		function isDescendant(child) {
			var parent = document.getElementById("emailContentEmail");
		     var node = child.parentNode;
		     while (node != null) {
		         if (node == parent || child == parent) {
		             return true;
		         }
		         node = node.parentNode;
		     }
		     return false;
		}
		function enableSchedule(element){
			if(element.checked == true){
				document.getElementById("send").innerText="Schedule";
				var date = document.getElementById("datepicker").value;
    			var time = document.getElementById("timeList").value;
    			scheduledTime = new Date(date+" "+time).toISOString();
			}
			else{
				document.getElementById("send").innerText="Send";
				scheduledTime = undefined;
			}
		}
		function openDatePicker(){
    		document.getElementById("dateTime").style.display= "block";
    		if(ButtonPosition == "DetailView"){
    			document.getElementById("dateTime").style.top= "84%";
    		}
    		else{
    			document.getElementById("dateTime").style.top= "60%";
    		}
    		document.getElementById("Error").style.display= "block";
		}	
		function scheduleClose(){
			var date = document.getElementById("datepicker").value;
    		var time = document.getElementById("timeList").value;
    		if(new Date(date+" "+time).getTime() < new Date().getTime()){
    			document.getElementById("ErrorText").innerText = "Schedule time should be in future.";
    		}
    		else{
    			document.getElementById("ErrorText").innerText = "";
	    		document.getElementById("dateTime").style.display= "none";
	    		document.getElementById("Error").style.display= "none";
	    		document.getElementById("scheduleCheck").checked =true;
	    		document.getElementById("send").innerText="Schedule";
	    		document.getElementById("scheduledDateTime").innerText=new Date(date).toDateString()+" at "+time +" ("+Intl.DateTimeFormat().resolvedOptions().timeZone+")";
	    		scheduledTime = new Date(date+" "+time).toISOString();
	    	}	
		}
		function cancel(){
			document.getElementById("Error").style.display= "none";
		}
		function showAnnounceMent(type){
		 	if(type === "feedback"){
		 		$('body').append(`<div id="announcetip" class="bottomTip" ><div class="bottomTipActions"><a href="https://wa.me/917397415648?text=Hello%20Ulgebra,%20I%20have%20a%20query" target="_blank">Contact Developer</a> </div> </div>`);
		 	}
		    else if(type === "new_update"){
		    	 $('body').append(`<div id="announcetip" class="bottomTip" >Update New version : Automatic & Bulk & Mobile App WhatsApp Message feature added <div class="bottomTipActions"><a href="https://wa.me/917397415648?text=Hello%20Ulgebra,%20I%20have%20a%20query" target="_blank">Contact Developer</a>  </div> </div>`);
            }
            if(type === "install_chrome_extension"){
                $('body').append(`<div id="announcetip" class="bottomTip" >Purchase plan for Sending Bulk & Automated WhatsApp Messages<div class="bottomTipActions"><span id="creditspace"></span><a style="margin:0px 5px" href="https://apps.ulgebra.com/zoho/crm/whatsapp-web/pricing" target="_blank">Pricing Plans </a>   <a style="margin:0px 5px" href="https://wa.me/917397415648?text=Hello%20Ulgebra,%20I%20have%20a%20query" target="_blank">Contact Developer</a> </div>
                    <span onclick="$('#announcetip').hide()" class="tt-close">X</span>
                </div>`);
            }
             if(type === "purchase_license"){
                $('body').append(`<div id="announcetip" class="bottomTip" >Purchase plan for Sending Bulk & Automated WhatsApp Messages <div class="bottomTipActions"><a href="https://forms.gle/4fXu5XgLrF5tt46R8" target="_blank">Purchase License</a> <a href="https://apps.ulgebra.com/zoho/crm/whatsapp-web/pricing" style="margin:0px 5px" target="_blank">Pricing Plans</a> </div>
                    <span onclick="$('#announcetip').hide()" class="tt-close">X</span>
                </div>`);
            }
			/*$('body').append(`<div id="announcetip" class="bottomTip" ><div class="bottomTipActions">Temporarily stopped chrome extension. Will be enabled soon.<a href="https://forms.gle/kXX6wJnyvYLEd2nbA" target="_blank">Submit Feedback</a> </div> </div>`);*/
        }

		function setSeenAlert(alertId){
			$('.extensionVersionCheck').hide();
			try {
              localStorage.setItem("alertseen", alertId);
            }
            catch(err) {
                loadDebugInfo(":unable-to-set-alertseen:");
            }
		}


		function showForcedAlert(){
// 		    var seenalert = "";
// 		    try {
//                 seenalert = localStorage.getItem("alertseen");
//             }
//             catch(err) {
//                 loadDebugInfo(":unable-to-get-alertseen:");
//             }
// 			if(seenalert!=="1.0"){
// 				$("body").append(`
// 					<div class="extensionVersionCheck">
// 						<div class="alerttl">New Chrome Extension Version</div>
// 							Kindly ensure you have the latest version of our chrome extension.  <br><br>
// 							If you have installed our chrome extension, old chrome extension will not work. Kindly remove and install new chrome extension.
// 						<br><br>
// 						<span style="color:green">Latest Version : 9.0</span>
// 						<br><br>
// 						<a href="https://chrome.google.com/webstore/detail/wa-web-for-zoho-crm-bulk/nkihbgbbbakefbgjnokdicilnhcdeckp" target="_blank">New Version Extension Link</a> <br><br><div class="closealertdia" onclick="$('.extensionVersionCheck').hide()">Close alert</div><div class="closealertdia" onclick="setSeenAlert('1.0')">Done &amp; Never Show Again</div>
						
// 						</div>
// 				`);
// 			}
		}

		// function executeFirebaseThings(){
		// 	firebase.initializeApp(firebaseConfig);
  		// 	firebase.analytics();
		// }

		// var s = document.createElement("script");
		// s.type = "text/javascript";
		// s.src = "https://www.gstatic.com/firebasejs/7.5.0/firebase-app.js";
		// document.head.appendChild(s);

		// var k = document.createElement("script");
		// k.type = "text/javascript";
		// k.src = "https://www.gstatic.com/firebasejs/7.5.0/firebase-analytics.js";
		// document.head.appendChild(k);

		// var firebaseConfig = {
		// 	apiKey: "AIzaSyB_NTkxQH12sAiB-F0mVIm89uA2VirabC4",
		// 	projectId: "wawebbulk",
		// 	storageBucket: "wawebbulk.appspot.com",
		// 	messagingSenderId: "218291591421",
		// 	appId: "1:218291591421:web:3ca4c3a172add3e7d0c694",
		// 	measurementId: "G-MMCYRRQPPG"
		// };

// 		document.addEventListener("DOMContentLoaded", function(event) { 
// 			//executeFirebaseThings();
// //   $("body").append(`<!-- The core Firebase JS SDK is always required and must be listed first -->
// // <script src="https://www.gstatic.com/firebasejs/7.19.0/firebase-app.js"></script>

// // <!-- TODO: Add SDKs for Firebase products that you want to use
// //      https://firebase.google.com/docs/web/setup#available-libraries -->
// // <script src="https://www.gstatic.com/firebasejs/7.19.0/firebase-analytics.js"></script>

// // <script>
// //   // Your web app's Firebase configuration
// //   
// //   // Initialize Firebase
// //   firebase.initializeApp(firebaseConfig);
// //   firebase.analytics();
// // </script>`);
// });

        function executeFirebaseThings(){
 			try{

				firebase.initializeApp(firebaseConfig);
				//firebase.analytics();
				db = firebase.firestore();

				db.collection('LicenseUsers').doc(userEmail).get().then((data)=>{ 
					if(!data.exists || !data.data() || !data.data().type){
						fetchLICDetails(userEmail, showNewPlanDetails);
						checkVersion();
						return;
					}
					else{
						peySts = data.data().type;
						isAllowed = data.data().type.indexOf("basic") == 0 || data.data().type.indexOf("classic") == 0;
						if(data.data().type.indexOf("business")==0 || data.data().type.indexOf("premium")==0 || data.data().type.indexOf("classic")==0){
							$.ajax({url: "https://us-central1-wawebbulk.cloudfunctions.net/checkUALIC?src=crm-wa-bulk&email="+userEmail, success: function(result){
							if(result){
								if(result.mcn > 0){
									waMsgCrdleft = result.mcn;
									$('.bottomTipActions').prepend('<span class="oldlicban">credit remaining : <b>'+(result.mcn)+" </b> &nbsp;&nbsp;</span>");
									
								}
							}
							if(result && result.mcn <=0) {
								document.write(`You have sent all messages which was allowed in this plan. Kindly request more credits to continue. <a href="https://apps.ulgebra.com/contact" target="_blank">Contact Now</a> <br><br> <a href="https://apps.ulgebra.com/zoho/crm/whatsapp-web/pricing" target="_blank">View Pricing Plans</a> `);
							}
							 //  executeFirebaseThings();
    					    if(result && result.status == "valid"){
    					    	isAllowed = true;
    					    }
    					}});
					}
						if(data.data().type.indexOf("business")==0 || data.data().type.indexOf("premium")==0){
							attPur = true;
							isAllowed = true;

							

						}
						
						checkVersion();
						
						if(!isAllowed){
							fetchLICDetails(userEmail, showNewPlanDetails);
						}
					}	
				});

				db.collection('appActivityLog').doc(userEmail).collection('history').add({
					't' : Date.now(),
					'a': 'WWBULK-SINGLE',
					'cev' : extensionVersion && extensionVersion.version ? extensionVersion.version : '-'
				});
			}
			catch(err){
				console.log(err);
				loadDebugInfo(":unable-to-fetch-firebase:"+userEmail);
			}
		}
		function checkVersion(){
			if(!!window.chrome){
				chrome.runtime.sendMessage(editorExtensionId, { "message": "version" },function (reply2){
					console.log(reply2);
					reply = reply2;
					extensionVersion = reply2;
					if(!extensionVersion){
						checkVersionLatest();
					}
					else{
						setTimeout(function(){
							checkNumber();
						},2000);
					}
				});	
			}else{
				showNoChromeExtError();
			}
		}	

		function checkVersionLatest(){
			if(!!window.chrome){
				chrome.runtime.sendMessage("haphhpfcpfeagcmannpebjjjpdlbhflh", { "message": "version" },function (reply2){
					console.log(reply2);
					reply = reply2;
					extensionVersion = reply2;
					if(!extensionVersion){
						showNoChromeExtError();
					}
					else{
						editorExtensionId = "haphhpfcpfeagcmannpebjjjpdlbhflh";
						setTimeout(function(){
							checkNumber();
						},2000);
					}
				});	
			}else{
				showNoChromeExtError();
			}
		}	
		
		function showNoChromeExtError(){
			$("body").append(`<div class="chromeextnpopup">
        <div class="cetp-inner">
            <div class="cetp-ttl">
                Install Google Chrome Extension! <div class="cetp-close" onclick="$('.chromeextnpopup').hide()">x</div>
            </div>
            <div class="cetp-content">
                Our Google Chrome extension is needed to Send Messages in Background and to Send Bulk WhatsApp Messages. <br><br>
				You need to purchase <a target="_blank" href="https://apps.ulgebra.com/zoho/crm/whatsapp-web/pricing?src=crmwasinglepop">one of our plans to send Bulk Messages.</a>
                <a href="https://chrome.google.com/webstore/detail/wa-web-for-zoho-crm-bulk/haphhpfcpfeagcmannpebjjjpdlbhflh" target="_blank">
                    <button class="cetp-install">
                        <span class="material-icons">get_app</span> Install Google Chrome Extension
                    </button>
                </a>
            </div>
        </div>
    </div>`);
		}


		var firebaseConfig = {
			apiKey: "AIzaSyB_NTkxQH12sAiB-F0mVIm89uA2VirabC4",
			projectId: "wawebbulk",
			storageBucket: "wawebbulk.appspot.com",
			messagingSenderId: "218291591421",
			appId: "1:218291591421:web:3ca4c3a172add3e7d0c694",
			measurementId: "G-MMCYRRQPPG"
		};

		function showNewUpdateLink(){
            document.getElementById("loader").style.display= "none";
            document.getElementById("ErrorText").insertAdjacentHTML("beforeend",`Please update the extension.<br><a href="https://writer.zohopublic.com/writer/published/m9ho5929b5193196a4c6cbafeee6667ac6d60" target="_blank"><button style="text-align:center;cursor:pointer;background-color: white;color: black;border-radius: 5px;border: none;margin-top: 15px;box-shadow: 0px 2px 5px rgba(0,0,0,0.4);cursor: pointer;" >See How to Update</button></a><br><br> Go To Settings -->Marketplace-->All-->Updates-->WhatsApp Web for Zoho CRM-->Click Update`);
            document.getElementById("Error").style.display= "block";
            document.getElementById("container").style.display= "block";
        }


var chosenWASenderId = null;
function setWAfromPhoneNumber(isLocalNumber, displayName, id) {
	$('.attach-opt-shown').remove();
	if(isLocalNumber){
		chosenWASenderId = "whatsapp-web";
		$('.sender-tools-whatcetra').hide();
		$('.sender-tools-waweb').show();
		$('#wa-from-phone-label-val').text(`${displayName} - WhatsApp Web`);
	}
	else{
		chosenWASenderId = id;
		if(chosenWASenderId != givenUserId){
			scheduledTime = null;
			$('#wa-sch-btn').show();
			$('#scheduleFeature').hide();
		}
		
		$('.sender-tools-whatcetra').show();
		$('.sender-tools-waweb').hide();
		$('#wa-from-phone-label-val').text(`${displayName} - WhatCetra account`);
	}
	$('.fromNumChooserOuter').hide();
	if(sendBtnClickedForProceed){
		sendSMS();
	}
}
function renderWAWebSenderIds() {
	if(fromPhoneNumber){
		$('#fnco-web-wa-number').html(`${fromPhoneNumber} <span>(Connected via <a href="https://web.whatsapp.com" target="_blank">WhatsApp Web</a>)</span>`).on('click', (function(){
			setWAfromPhoneNumber(true, fromPhoneNumber, '');
		}));
	}else{
		$('#fnco-web-wa-number').html(`<div style="color:grey;cursor:not-allowed">WhatsApp Web is not connected <button class="err-wa-conn-label" onclick="showWWebNotConnectedInstructions(checkNumber, true)">Fix now</button></div>`).off();
	}
}
var senders={};
function renderWhatCetraSenderIds() {

	if(!whatcetraApp || !whatcetraApp.auth() && !whatcetraApp.auth().currentUser){
		$('.wc-sender-num').remove();
		return;
	}
	whatcetraApp.firestore().collection('/invites/'+whatcetraApp.auth().currentUser.email+'/org_invites').get().then(function (querySnapshot) {
          if(!querySnapshot.docs.length) {
              console.log('empty');
          }
          $('#fnco-whatcetra-sender-ids').append(`<div onclick="setWAfromPhoneNumber(false, '${whatcetraApp.auth().currentUser.displayName}', '${whatcetraApp.auth().currentUser.uid}')" id="wc-send-num-${whatcetraApp.auth().currentUser.uid}" class="fnco-item wc-sender-num">
               <div class="fnco-ico" style="background-image:url('${whatcetraApp.auth().currentUser.photoURL}')"></div>  ${whatcetraApp.auth().currentUser.displayName} - <span> Send via  <a href="https://app.whatcetra.com" target="_blank">WhatCetra</a> account</span>
              </div>`);
          senders[whatcetraApp.auth().currentUser.uid]=whatcetraApp.auth().currentUser;
          querySnapshot.forEach(item=>{ 
          	item = item.data();
          	senders[item.uid]=item;
          	$('#fnco-whatcetra-sender-ids').append(`<div onclick="setWAfromPhoneNumber(false, '${item.name}', '${item.uid}')" id="wc-send-num-${item.uid}" class="fnco-item wc-sender-num">
               <div class="fnco-ico" src="background-image:url('${item.photoUrl}')"></div> ${item.name} - <span>Send via <a href="https://app.whatcetra.com" target="_blank">WhatCetra</a> account</span>
              </div>`);
      	  });
});
}
	
function handleScheduleAction(){
	var seetingsLink = "https://crm.zoho.com/crm/settings/extensions/all/whatsappforzohocrm?tab=webInteg&subTab=marketPlace&nameSpace=whatsappforzohocrm&portalName=ulgebra";
	if(!chosenWASenderId){
		return;
	}
	if(chosenWASenderId == givenUserId){
		$('#wa-sch-btn').hide();
		$('#scheduleFeature').show();
		return;
	}
	else if(!givenUserId){
		document.getElementById("ErrorText").innerHTML = `Please configure userId and apiKey in <a href="${seetingsLink}" target="_blank"><button style="text-align:center;cursor:pointer;background-color: white;color: black;border-radius: 5px;border: none;margin-top: 15px;box-shadow: 0px 2px 5px rgba(0,0,0,0.4);cursor: pointer;" >Extension configure page</button></a>`;
	}
	else if(givenUserId && senders[givenUserId]){
		document.getElementById("ErrorText").innerHTML = `Credential configured for <b>${senders[givenUserId].name?senders[givenUserId].name:senders[givenUserId].displayName}</b> - WhatCetra account, <br>Please change userId and apiKey in <a href="${seetingsLink}" target="_blank"><button style="text-align:center;cursor:pointer;background-color: white;color: black;border-radius: 5px;border: none;margin-top: 15px;box-shadow: 0px 2px 5px rgba(0,0,0,0.4);cursor: pointer;" >Extension configure page</button></a>.`;
	}
	else if(givenUserId){
		document.getElementById("ErrorText").innerHTML = `Please update userId and apiKey in <a href="${seetingsLink}" target="_blank"><button style="text-align:center;cursor:pointer;background-color: white;color: black;border-radius: 5px;border: none;margin-top: 15px;box-shadow: 0px 2px 5px rgba(0,0,0,0.4);cursor: pointer;" >Extension configure page</button></a>.`;
	}
	document.getElementById("Error").style.display= "block";
	setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 3000);
}