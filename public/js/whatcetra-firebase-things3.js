

var currentUser = null;
var whatcetraApp = null;
var attachedfile={}

var APP = {
    initialize: function () {
        APP.renderEngine.header();
    },
    renderEngine: {
        header: function () {
            if($('.bottomTip').is(':visible'))
            popupSizeSet(800, 654);
            else
            popupSizeSet(800, 625);
            $("body").prepend(`
            <header id="whatcetraHeader">
                <div class="mainSearchBarHolder">
                    <div class="siteName">
                        <a style="color:black" href="https://app.whatcetra.com" target="_blank"> <div class="extensionLogoDiv">
                            <div class="extensionLogo"><img src="wc2.png" class="wcH-logoImg" style="width: 100%;"><span class="extensionLogoText">WhatCetra</span></div>
                            </div>
                        </a>
                    </div>
            <div class="contactSite">
                <a title="Contact Developer" href="https://apps.ulgebra.com/contact" target="_blank" title="Contact Developers"> <span class="material-icons">contact_support</span> Help</a>
            </div>
                    <div class="signUserInfoHolder signedIn">
                        <div class="userInfoBtn">
                            <img id="signInUserImg"/>
                        </div>
                        <div class="signedUserOptions">
                            <div class="signedUserOptionsInr">
                                <a id="signedProfileUserLink" class="listVidLink" href="./user.html">
                                    <div class="suo-item">
                                        <span class="material-icons">account_circle</span> <span id="signInUserName"> My Account </span>
                                    </div>
                                </a>
                                <div class="suo-item" id="signoutBtn" onClick="waWebZohoCrmSignOut();">
                                        <span class="material-icons">login</span> <span id="signInUserName"> Sign out </span>
                                    </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="pageTitle">
                    ${''}
                </div> 
                
                <div id="notSignedInError">
                    <div class="signTtl">Sign in to WhatCetra to Continue...<div style="display: flex; align-items: center; justify-content: center; height: 85px;">
                    <div id="wloader"><div class="wcMsgLoadingInner" title="loading…"><svg class="wcMsgLoadingSVG" width="17" height="17" viewBox="0 0 46 46" role="status"><circle class="wcMsgLoadingSvgCircle" cx="23" cy="23" r="20" fill="none" stroke-width="6" style="stroke: rgb(57 82 234);"></circle></svg></div></div> <br>
                    <div  style="display: none" id="firebaseui-auth-container"></div>
                        </div>
                    </div>
                    <button class="fb-sign-cancel-btn" onclick="$('#notSignedInError').hide();">Cancel</button>
                </div>
                <div class="ua-email-ver-holder" style="
                        display: none;
                        position: fixed;
                        top: 0px;
                        bottom: 0px;
                        left: 0px;
                        right: 0px;
                        background-color: rgba(0,0,0,0.4);
                        z-index: 10;
                        text-align: center;
                    ">
                            <div class="ua-email-ver-inr" style="
                        background-color: white;
                        display: inline-block;
                        margin-top: 5%;
                        padding: 10px 20px;
                        border-radius: 3px;
                        text-align: left;
                        min-width: 30%;
                        box-shadow: 0px 10px 25px rgba(0,0,0,0.5);
                    ">
                                <div class="ua-email-ver-ttl" style="
                        font-size: 25px;
                        margin: 10px 0px 15px;
                        color: crimson;
                    ">
                                    Verify your email!
                                </div>
                                <div class="ua-email-ver-tip">
                                    Check your inbox <b id="ua-email-id"></b> and verify your email. <div style="margin: 15px 0px 15px;"> <button onclick="sendReVerifyMail()" style="
                        padding: 5px 15px;
                        box-shadow: 0px 5px 5px rgba(0,0,0,0.5);
                        border: none;
                        background-color: royalblue;
                        color: white;
                        border-radius: 3px;
                        font-size: 14px;
                        cursor: pointer;
                    ">Resend Mail</button>
                    <button onclick="whatcetraApp.auth().signOut();window.location.reload()" style="
                        padding: 5px 15px;
                        box-shadow: 0px 5px 5px rgba(0,0,0,0.5);
                        border: none;
                        background-color: royalblue;
                        color: white;
                        border-radius: 3px;
                        font-size: 14px;
                        cursor: pointer;
                    ">Refresh</button> 

                    <i id="sentmailstatustext" style="
                        margin: 10px;
                        color: green;
                        display: none;
                    ">Email is sent!</i>

                    </div>
                    <div style="
    height: 54px;
    display: flex;
    justify-content: flex-end;
    align-items: center;
    position: relative;
    right: 5px;
"><button onclick="$('.ua-email-ver-holder').hide();" style="
                        padding: 5px 15px;
                        box-shadow: 0px 5px 5px rgba(0,0,0,0.5);
                        border: none;
                        background-color: #444a5c;
                        color: white;
                        border-radius: 3px;
                        font-size: 14px;
                        cursor: pointer;
                    ">Okay</button></div>
                                </div>
                            </div>
                        </div>
            </header>
            `);

            createTempViewCheck();
    
        }
    }
};

function waWebZohoCrmSignOut() {
    if(whatcetraApp && whatcetraApp.auth()) {
        whatcetraApp.auth().signOut();
    }
    $("#whatcetraHeader").hide();
    $('#fnco-whatcetra-sender-ids').html(`<div class="fnco-not-loggedin signedOut" style="display: block;">
                 Sign in to choose common WhatsApp numbers <br>of your WhatCetra account <br>
                 <div class="fnco-loggedin-btn fncol-signin" onclick="showSignInOption()">Sign In</div>
                 <a href="https://app.whatcetra.com" target="_blank"><div class="fnco-loggedin-btn fncol-signup">Create an account</div></a>
              </div>`);
    chosenWASenderId = null;
    $('#wa-from-phone-label-val').text(`Select from number`);
    //window.location.reload();
    createTempViewCheck();
    if($('.bottomTip').is(':visible'))
    popupSizeSet(800, 602);
    else
    popupSizeSet(800, 575);
}

function createTempViewCheck() {

    if($('.bottomTip').is(':visible') && $('.mainSearchBarHolder').is(':visible')) {
        $('.createTemplateMainOuterDiv').css({'top': '65px'});
    }
    else if($('.bottomTip').is(':visible') && !$('.mainSearchBarHolder').is(':visible')) {
        $('.createTemplateMainOuterDiv').css({'top': '42px'});
    }
    else if(!$('.bottomTip').is(':visible') && !$('.mainSearchBarHolder').is(':visible')) {
        $('.createTemplateMainOuterDiv').css({'top': '42px'});
    }
    else if(!$('.bottomTip').is(':visible') && $('.mainSearchBarHolder').is(':visible')) {
        $('.createTemplateMainOuterDiv').css({'top': '65px'});
    }

}


window.addEventListener('load',function () {


    let firebaseConfig = {
        apiKey: "AIzaSyAjyLueIDBhJxL8spP9jg7Ptz4orjq7a70",
        authDomain: "app-whatcetra.firebaseapp.com",
        databaseURL: "https://app-whatcetra-default-rtdb.firebaseio.com",
        projectId: "app-whatcetra",
        storageBucket: "app-whatcetra.appspot.com",
        messagingSenderId: "814272818584",
        appId: "1:814272818584:web:80ce01a6f9032deef14fed",
        measurementId: "G-0XTCZQGNYC"
      };       
    
    
    
    whatcetraApp = firebase.initializeApp(firebaseConfig,"whatcetra");
    whatcetraApp.auth().onAuthStateChanged(function (user) {

        if(user && !user.emailVerified){
            $('.ua-email-ver-holder').show();
            console.log("User email is not verified");
            user.sendEmailVerification();
        }
        if(user){
            (typeof renderWhatCetraSenderIds != "undefined")?renderWhatCetraSenderIds():"";
            startWhatcetra();
        }
        
        user ? handleSignedInUser(user) :"";

    });


});

function startWhatcetra(){

    let whatcetraUi = null;

    if(!document.getElementById("whatcetraHeader") || document.getElementById("whatcetraHeader").style.display == "none"){
        APP.initialize();
        $('.userInfoBtn').click(function () {
            let hidden = $(".signedUserOptions");
            if (hidden.hasClass('visible')) {
                hidden.animate({"right": "-750px"}).removeClass('visible');
            } else {
                hidden.animate({"right": "0px"}).addClass('visible');
            }
        });
        $(`<br><br>`).insertAfter(".mainSearchBarHolder");
        
        
    }
    if(!whatcetraApp.auth().currentUser){    
        // handleClientLoad();
        let uiConfig = {
            callbacks: {
                // Called when the user has been successfully signed in.
                'signInSuccessWithAuthResult': function (authResult, redirectUrl) {
                    if (authResult.user) {
                        handleSignedInUser(authResult.user);
                    }
                    if (authResult.additionalUserInfo) {
                        // document.getElementById('is-new-user').textContent =
                        //         authResult.additionalUserInfo.isNewUser ?
                        //         'New User' : 'Existing User';
                    }
                    // Do not redirect.
                    return false;
                },
                uiShown: function () {
                    
                    $("#notSignedInError").hide();
                    $("#wloader").hide();
                    $(".showAfterSignIn").show();
                }
            },
            signInFlow: 'popup',
            signInOptions: [
                {
                    provider: firebase.auth.GoogleAuthProvider.PROVIDER_ID,
                    clientId: '814272818584-ke57jr9h9c2073cqp3hjtgf1h5k2ku65.apps.googleusercontent.com'
                },
                {
                    "provider": firebase.auth.EmailAuthProvider.PROVIDER_ID
                }
            ],
            //credentialHelper: firebaseui.auth.CredentialHelper.GOOGLE_YOLO,
            credentialHelper: firebaseui.auth.CredentialHelper.NONE,
            tosUrl: './terms-of-service.html',
            privacyPolicyUrl: './privacy-policy.html'

        };
        let fbaseauth = whatcetraApp.auth();
 
        if(!whatcetraUi){
            whatcetraUi = new firebaseui.auth.AuthUI(fbaseauth);
        }    
        whatcetraUi.start('#firebaseui-auth-container', uiConfig);

    }    
}


function showSignInOption() {
    if(!whatcetraApp.auth().currentUser){  
        startWhatcetra();
        handleSignedOutUser();
        return;
    } 
    else {
        renderWhatCetraSenderIds();
    }
}

function addToWhatcetraQueue(data){
    let user = whatcetraApp.auth().currentUser;
    if(user){        
        let ref = whatcetraApp.database().ref('whatcetra/queued/' + chosenWASenderId +'/com-whatsapp');
        ref.update(data, function(error) {
            if (error) {
                console.log(error);
              // The write failed...
            } else {
                // $.ajax({url: "https://us-central1-app-whatcetra.cloudfunctions.net/updateLICCA?src="+mode+"&email="+userEmail+"&mcc="+Object.keys(sendBulkWhatsAppList).length, success: function(result){
                //     console.log(result);
                // }});   
                console.log("successfully");
              // Data saved successfully!
            }

            return `Your Message has been sent successfully in the background <br> via Chrome Extension.`;
            //wcConfirm('<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your Messages have been sent successfully.</div>','','Okay',true,false);
            //setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload(); }, 1500);
        });
    }     
    else {
        wcConfirm('Sign in to WhatCetra to Continue...','','Okay',true,false);
        return false;
    }  
}
function openUploader(){
    if(!whatcetraApp.auth().currentUser){
        startWhatcetra();
        handleSignedOutUser();
        return;
    }
    if(attPur){
        if($('.createTemplateMainOuterDiv').is(':visible'))
        document.getElementById("inputFileAttachCreate").click();
        else        
        document.getElementById("inputFileAttach").click();
    }
    else{
        showNoHighPlanError(true, true);
    }
}
function fileSizeCheckWc(fileSize) {
    if(fileSize < 1000000){
        fileSize = fileSize/1024 ;
    }else{
        fileSize = fileSize/(1024*1024) ;
        if(fileSize > 5) {
            alert(`file size is ${fileSize.toFixed(2)}Mb.
            Please select file less than 5Mb.`);
            return false;
        }
    }
    return true;
}


function attachFileChange(e) {

    let FileButton;
    if($('.createTemplateMainOuterDiv').is(':visible'))
    FileButton = document.getElementById("inputFileAttachCreate");
    else
    FileButton = document.getElementById("inputFileAttach");

    let file = e.files[0];
    if(!fileSizeCheckWc(file.size)) {
        return false;
    }
    let metadata = {
                      customMetadata: {
                        'file_name': file.name,
                        'file_size': file.size
                      }
                    }

    let fileTyle;
    if(file.type.includes('image/')) {
        fileTyle = 'image';
    }
    else if(e.files[0].type.includes('audio/')) {
        fileTyle = 'audio';
    }
    else if(file.type.includes('video/')) {
        fileTyle = 'video';
    }
    else if(file.type.includes('application/')) {
        if(file.type.includes('pdf')) {
            fileTyle = 'application/pdf';
        }
        else if(file.type.includes('zip')) {
            fileTyle = 'application/zip';
        }
        else if(file.type.includes('excel')) {
            fileTyle = 'application/excel';
        }
        else {
            fileTyle = 'application/msdownload';
        }
    }
    else if(file.type.includes('text/')) {
        fileTyle = 'text';
    }
    fileUploadComplete = false;
    let uniqFileName = file.name+',wcUniqNameId='+Date.now();
    let task = whatcetraApp.storage().ref('whatcetra/'+currentUser.uid+'/'+fileTyle+'/'+uniqFileName).put(file, metadata);
    if($('.createTemplateMainOuterDiv').is(':visible'))
    $('#attachedfileCreate').html(`<div class="loadingdiv"> file is uploading...</div>`);
    else
    $('#attachedfile').html(`<div class="loadingdiv"> file is uploading...</div>`);
    task.on('state_changed',
        function progress(snapshot){
            switch (snapshot.state) {
              case firebase.storage.TaskState.PAUSED: // or 'paused'
                //console.log('Upload is paused');
                break;
              case firebase.storage.TaskState.RUNNING: // or 'running'
                //console.log('Upload is running');
                break;
            }
        },
        function error(err){
            console.log("file not uploaded.")
            //fileUploadComplete = false;
        },
        function complete(){
            if($('.createTemplateMainOuterDiv').is(':visible'))
            document.getElementById("inputFileAttachCreate").value = "";
            else
            document.getElementById("inputFileAttach").value = "";
            let storageRef =  whatcetraApp.storage().ref('whatcetra/'+currentUser.uid+'/'+fileTyle+'/'+uniqFileName);
            storageRef.getDownloadURL().then(imgURL =>{
                attachedfile = {"mediaUrl":imgURL,"fileMeta":file};
                $('.loadingdiv').remove();
                if($('.createTemplateMainOuterDiv').is(':visible'))
                $('#attachedfileCreate').html(`${file.name}`);
                else
                $('#attachedfile').html(`${file.name}`);
                fileUploadComplete = true;
                
            });

        }
    );

}

var handleSignedOutUser = function () {
    $("#notSignedInError").show();
    $("#whatcetraHeader").hide();
    $('#wloader').hide();
    $(".fnco-not-loggedin").show();
    $("#firebaseui-auth-container").show();
    $(".showAfterSignIn").hide();
    
};

var handleSignedInUser = function (user) {
    //$("#credential_picker_container").hide();
    currentUser = whatcetraApp.auth().currentUser;
    $("#notSignedInError").hide();
    $(".showAfterSignIn").show();
    $("#whatcetraHeader").show();
    $(".fnco-not-loggedin").hide();
    if(currentUser.photoURL){
        $("#signInUserImg").attr({
            "src": currentUser.photoURL
        });
    }
    else{
        $("#signInUserImg").attr({
            "src": "https://cdn.iconscout.com/icon/free/png-256/account-profile-avatar-man-circle-round-user-30452.png"
        });
    }    
    $("#ua-email-id").text(currentUser.email);
    $("#signInUserName").text(currentUser.displayName);
    $("#signedProfileUserLink").attr({
        "href": "/user?id=" + currentUser.uid
    });
    
    
};

function sendReVerifyMail(){
    currentUser.sendEmailVerification();
    $("#sentmailstatustext").hide(500).show(500);
}
