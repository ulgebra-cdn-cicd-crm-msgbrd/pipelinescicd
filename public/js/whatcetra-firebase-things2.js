var curId = 1000;
var errorId = 1000;
var initTries = 0;
var initProcessId = 1;
var storageRef = null;
var whatsappWebCheckNotNeeded = true;
var APP = {
    initialize: function () {
        APP.renderEngine.header();
    },
    renderEngine: {
        header: function () {
            if($('.bottomTip').is(':visible')) {
                if(module == extensionTemplate || ButtonPosition == "CreateOrCloneView") {
                    popupSizeSet(800, 500);
                }
                else {
                    popupSizeSet(800, 677);
                }
            }
            else {
                if(module == extensionTemplate || ButtonPosition == "CreateOrCloneView") {
                    popupSizeSet(800, 500);
                }
                else {
                    popupSizeSet(800, 575);
                }
            }
            
            $("body").prepend(`
            <header id="whatcetraHeader">
                <div class="mainSearchBarHolder">
                    <div class="siteName">
                        <div class="siteLogo"></div> <a style="color:black" href="https://app.whatcetra.com" target="_blank"> ${document.querySelector('meta[name="brandname"]') ? document.querySelector('meta[name="brandname"]').content : "WhatCetra"} </a>
                    </div>
            <div class="contactSite">
                <a title="Contact Developer" href="https://apps.ulgebra.com/contact" target="_blank" title="Contact Developers"> <span class="material-icons">contact_support</span> Help</a>
            </div>
                    <div class="signUserInfoHolder signedIn">
                        <div class="userInfoBtn">
                            <img id="signInUserImg"/>
                        </div>
                        <div class="signedUserOptions">
                            <div class="signedUserOptionsInr">
                                <a id="signedProfileUserLink" class="listVidLink" href="./user.html">
                                    <div class="suo-item">
                                        <span class="material-icons">account_circle</span> <span id="signInUserName"> My Account </span>
                                    </div>
                                </a>
                                <div class="suo-item" id="signoutBtn" onClick="waWebZohoCrmSignOut();">
                                        <span class="material-icons">login</span> <span id="signInUserName"> Sign out </span>
                                    </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="pageTitle">
                    ${document.title}
                </div>
                <div class="signedOut signOutbackWindow"></div>
                <div id="notSignedInError">
                    <div class="signTtl">Sign in to ${document.querySelector('meta[name="brandname"]') ? document.querySelector('meta[name="brandname"]').content : "WhatCetra"} to Continue...<div>
                    <div id="wloader"><div class="wcMsgLoadingInner" title="loading…"><svg class="wcMsgLoadingSVG" width="17" height="17" viewBox="0 0 46 46" role="status"><circle class="wcMsgLoadingSvgCircle" cx="23" cy="23" r="20" fill="none" stroke-width="6" style="stroke: rgb(57 82 234);"></circle></svg></div></div> <br>
                    <div  style="display: none" id="firebaseui-auth-container"></div>
                        </div>
                    </div>
                    <button class="fb-sign-cancel-btn" onclick="$('#notSignedInError').hide();$('.signOutbackWindow').hide()">Cancel</button>
                </div>
                <div class="ua-email-ver-holder" style="
                        display: none;
                        position: fixed;
                        top: 0px;
                        bottom: 0px;
                        left: 0px;
                        right: 0px;
                        background-color: rgba(0,0,0,0.4);
                        z-index: 10;
                        text-align: center;
                    ">
                            <div class="ua-email-ver-inr" style="
                        background-color: white;
                        display: inline-block;
                        margin-top: 5%;
                        padding: 10px 20px;
                        border-radius: 3px;
                        text-align: left;
                        min-width: 30%;
                        box-shadow: 0px 10px 25px rgba(0,0,0,0.5);
                    ">
                                <div class="ua-email-ver-ttl" style="
                        font-size: 25px;
                        margin: 10px 0px 15px;
                        color: crimson;
                    ">
                                    Verify your email!
                                </div>
                                <div class="ua-email-ver-tip">
                                    Check your inbox <b id="ua-email-id"></b> and verify your email. <div style="margin: 15px 0px 15px;"> <button onclick="sendReVerifyMail()" style="
                        padding: 5px 15px;
                        box-shadow: 0px 5px 5px rgba(0,0,0,0.5);
                        border: none;
                        background-color: royalblue;
                        color: white;
                        border-radius: 3px;
                        font-size: 14px;
                        cursor: pointer;
                    ">Resend Mail</button>
                    <button onclick="whatcetraApp.auth().signOut();window.location.reload()" style="
                        padding: 5px 15px;
                        box-shadow: 0px 5px 5px rgba(0,0,0,0.5);
                        border: none;
                        background-color: royalblue;
                        color: white;
                        border-radius: 3px;
                        font-size: 14px;
                        cursor: pointer;
                    ">Refresh</button> 

                    <i id="sentmailstatustext" style="
                        margin: 10px;
                        color: green;
                        display: none;
                    ">Email is sent!</i>

                    </div>
                                </div>
                            </div>
                        </div>
            </header>
            `);

            createTempViewCheck();
    
        }
    }
};

function waWebZohoCrmSignOut() {
    whatcetraApp.auth().signOut();
    $("#whatcetraHeader").hide();
    //window.location.reload();
    createTempViewCheck();
    // if($('.bottomTip').is(':visible'))
    // popupSizeSet(800, 625);
    // else
    // popupSizeSet(800, 540);
    
    if($('.bottomTip').is(':visible')) {
        if(module == extensionTemplate || ButtonPosition == "CreateOrCloneView") {
            popupSizeSet(800, 466);
        }
        else {
            popupSizeSet(800, 625);
        }
    }
    else {
        if(module == extensionTemplate || ButtonPosition == "CreateOrCloneView") {
            popupSizeSet(800, 466);
        }
        else {
            popupSizeSet(800, 540);
        }
    }

}

function createTempViewCheck() {

    if(module == extensionTemplate || ButtonPosition == "CreateOrCloneView") {

        $('.createTemplateMainOuterDiv').css({'top': '46px', 'left': '0px'});

    }
    else {

        if($('.bottomTip').is(':visible') && $('.mainSearchBarHolder').is(':visible')) {
            $('.createTemplateMainOuterDiv').css({'top': '65px'});
        }
        else if($('.bottomTip').is(':visible') && !$('.mainSearchBarHolder').is(':visible')) {
            $('.createTemplateMainOuterDiv').css({'top': '42px'});
        }
        else if(!$('.bottomTip').is(':visible') && !$('.mainSearchBarHolder').is(':visible')) {
            $('.createTemplateMainOuterDiv').css({'top': '42px'});
        }
        else if(!$('.bottomTip').is(':visible') && $('.mainSearchBarHolder').is(':visible')) {
            $('.createTemplateMainOuterDiv').css({'top': '65px'});
        }

    }

}

var GoogleAuth;
var currentUser = null;
var whatcetraDb = null;
let isMobileView = mobileCheck();
var initTrackingId = null;
var whatcetraApp = null;
window.addEventListener('load',function () {
    var styles = (`
        .mainSearchBarHolder{
            height: 50px;
        }
        .pageTitle{
            display: none;
        }
        .ext{
            margin-top: 5px;
            padding: 5px 10px;
            border-radius: 3px;
            border: none;
            box-shadow: 0px 1px 5px rgb(0 0 0 / 40%);
            background: crimson;
            color: white;
            cursor: pointer;
        }
            
        `);

        var styleSheet = document.createElement("style");
        styleSheet.type = "text/css";
        styleSheet.innerText = styles;
        document.head.appendChild(styleSheet);
    /// $('body').append("<audio autoplay src='ZHpyi6oDmZM/vWvTRTsVMiZbhRUPeHLmqbC25mI3/1593485308379.mpeg'></audio>");
    var firebaseConfig = {
        apiKey: "AIzaSyAjyLueIDBhJxL8spP9jg7Ptz4orjq7a70",
        authDomain: "app-whatcetra.firebaseapp.com",
        databaseURL: "https://app-whatcetra-default-rtdb.firebaseio.com",
        projectId: "app-whatcetra",
        storageBucket: "app-whatcetra.appspot.com",
        messagingSenderId: "814272818584",
        appId: "1:814272818584:web:80ce01a6f9032deef14fed",
        measurementId: "G-0XTCZQGNYC"
      };
    // Initialize Firebase
    (typeof renderWhatCetraSenderIds != "undefined")?renderWhatCetraSenderIds():"";
        
    
    
    whatcetraApp = firebase.initializeApp(firebaseConfig,"whatcetra");
    storageRef = whatcetraApp.storage().ref();
    whatcetraDb = whatcetraApp.firestore();
    whatcetraApp.auth().onAuthStateChanged(function (user) {
        if(user && !user.emailVerified){
            $('.ua-email-ver-holder').show();
            console.log("User email is not verified");
            user.sendEmailVerification();
        }
        if(user){
            (typeof renderWhatCetraSenderIds != "undefined")?renderWhatCetraSenderIds():"";
            startWhatcetra();
        }
        $('.waitTillAuthCheck').removeClass('waitToResolve');
        user ? handleSignedInUser(user) :"";
        handleauthoriztion();
    });
});
var whatcetraUi = null;
function startWhatcetra(){
    if(!document.getElementById("whatcetraHeader") || document.getElementById("whatcetraHeader").style.display == "none"){
        APP.initialize();
        $('.userInfoBtn').click(function () {
            var hidden = $(".signedUserOptions");
            if (hidden.hasClass('visible')) {
                hidden.animate({"right": "-750px"}).removeClass('visible');
            } else {
                hidden.animate({"right": "0px"}).addClass('visible');
            }
        });
        $(`<br><br>`).insertAfter(".mainSearchBarHolder");
        
        
    }
    if(!whatcetraApp.auth().currentUser){    
        // handleClientLoad();
        var uiConfig = {
            callbacks: {
                // Called when the user has been successfully signed in.
                'signInSuccessWithAuthResult': function (authResult, redirectUrl) {
                    if (currentUser == null && authResult.user) {
                        handleSignedInUser(authResult.user);
                         handleauthoriztion();
                    }
                    if (authResult.additionalUserInfo) {
                        document.getElementById('is-new-user').textContent =
                                authResult.additionalUserInfo.isNewUser ?
                                'New User' : 'Existing User';
                    }
                    // Do not redirect.
                    return false;
                },
                uiShown: function () {
                    $("#credential_picker_container").hide();
                    $("#notSignedInError").hide();
                    $(".loader").hide();
                    $(".showAfterSignIn").show();
                }
            },
            signInFlow: 'popup',
            signInOptions: [
                {
                    provider: firebase.auth.GoogleAuthProvider.PROVIDER_ID,
                    clientId: '814272818584-ke57jr9h9c2073cqp3hjtgf1h5k2ku65.apps.googleusercontent.com'
                },
                {
                    "provider": firebase.auth.EmailAuthProvider.PROVIDER_ID
                }
            ],
            //credentialHelper: firebaseui.auth.CredentialHelper.GOOGLE_YOLO,
            credentialHelper: firebaseui.auth.CredentialHelper.NONE,
            tosUrl: './terms-of-service.html',
            privacyPolicyUrl: './privacy-policy.html'

        };
        var fbaseauth = whatcetraApp.auth();
        
        // try{
        //     fbaseauth.setPersistence(firebase.auth.Auth.Persistence.SESSION);
        //     uiConfig.credentialHelper = firebaseui.auth.CredentialHelper.GOOGLE_YOLO;
        // }
        // catch(ex){
        //     console.log(ex);
        //     fbaseauth.setPersistence(firebase.auth.Auth.Persistence.NONE);
        //     uiConfig.credentialHelper = firebaseui.auth.CredentialHelper.NONE;
        // }
 
        if(!whatcetraUi){
            whatcetraUi = new firebaseui.auth.AuthUI(fbaseauth);
        }    
        whatcetraUi.start('#firebaseui-auth-container', uiConfig);
    }    
}
function handleauthoriztion(){
    if(whatcetraApp.auth().currentUser){
        // sendWhatcetra();
    }
}

function showSignInOption() {
    if(!whatcetraApp.auth().currentUser){  
        startWhatcetra();
        handleSignedOutUser();
        return;
    } 
}

async function sendWhatcetra(){
    if(!whatcetraApp.auth().currentUser){  
        startWhatcetra();
        handleSignedOutUser();  
        return;
    }    
    bulkInsertList = [];
    var sendBulkWhatsAppList={};
    var MobileNumber;
    var toModule ;
    var toId ;

    // var date = document.getElementById("datepicker").value;
    // var time = document.getElementById("timeList").value;
    if(document.getElementById("emailContentEmail").innerText.length >3600){
        wcConfirm('Message should be within 3000 characters.','','Okay',true,false);
        return;
    }
    else if(scheduledTime && new Date(scheduledTime).getTime() < new Date().getTime()){
        wcConfirm('Schedule time should be in future.','','Okay',true,false);
        return;
    }
    else if(document.getElementById("emailContentEmail").innerText.replace(/\n/g,"").replace(/\t/g,"").replace(/ /g,"") == ""){
        wcConfirm('Message cannot be empty.','','Okay',true,false);
        return;
    }
    else{
        var notes = "\n("+module +" without mobile number will be ignored.)";
        wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingLoaderInner">${sendingLoader}</div><div class="sendingLoaderHint">Sending...<br>Messages will be sent only for valid numbers.</div></div>`,'','Okay',true,true);
        var message = document.getElementById("emailContentEmail").innerText;
        var successRecords=0;
        var recordsLength = currentRecords.length;
        var recordIndex = 0;
        var toModuleRecords =[];
        if(phoneField.indexOf(".") != -1){
            toModuleField = phoneField.substring(0,phoneField.indexOf("."));
            moduleFields.forEach(function(field){
                if(field.api_name == toModuleField){
                    toModule = field.lookup.module.api_name;
                }
            });
            if(toModuleField && toModule){
                var toModuleIds =[];
                currentRecords.forEach(function(currentRecord){
                    if(currentRecord[toModuleField] && currentRecord[toModuleField].id){
                        toModuleIds.push(currentRecord[toModuleField].id);
                    }
                }); 
                await ZOHO.CRM.API.getRecord({Entity:toModule,RecordID:toModuleIds}).then(function(data2){
                    toModuleRecords = data2.data;
                    toModuleRecords.forEach(function(record){
                        toModuleRecordsJSON[record.id] = record;
                    }); 
                });
            }
        }
        currentRecords.forEach(function(currentRecord){
            if(defaultModules.indexOf(module) != -1){
                MobileNumber = currentRecord[phoneField];
            }
            Promise.all([checkMobileNumber(MobileNumber,currentRecord)]).then(function(numberresp){
                MobileNumber = numberresp[0].Mobile;
                if(MobileNumber){
                    MobileNumber = MobileNumber.replace(/\D/g,'');
                }   
                toModule = numberresp[0].toModule;
                toId = numberresp[0].toId;
                recipientName = numberresp[0].recipientName;
                var recordId = currentRecord.id;
                var argumentsData = {
                    "message" : message,
                    "module" : module,
                    "templateId":templateId,
                    "recordId" : recordId,
                    "to":MobileNumber
                };
                var filledMessage = JSON.parse(JSON.stringify(getMessageWithFields(argumentsData,currentRecord)));
                if(!recipientName && currentRecord.Full_Name){
                    recipientName = currentRecord.Full_Name;
                }
                if(recipientName){
                    var name = "WhatsApp to " + recipientName;
                }
                else{
                    var name = "WhatsApp to " + MobileNumber;
                }
                var req_data={"Name":name,"whatsappforzohocrm__WhatsApp_Message":filledMessage,"whatsappforzohocrm__Recipient_Number":MobileNumber,"whatsappforzohocrm__Sender_Phone":fromPhoneNumber,"whatsappforzohocrm__Module":module,"whatsappforzohocrm__Recipient_Id":recordId,"whatsappforzohocrm__Status":"Sent"};
                if(attPur && attachedfile && attachedfile.mediaUrl){
                    req_data["whatsappforzohocrm__Media_URL"]=attachedfile.mediaUrl;
                }    
                historyFields.forEach(function(field){
                    if(field.data_type == "lookup" && field.lookup.module.api_name == module){
                        req_data[field.api_name]=recordId;
                    }
                });
                if(module == "Deals" && toModule && toId){
                    req_data["whatsappforzohocrm__"+toModule]=toId;
                }
                if(scheduledTime != null)
                {
                    time = scheduledTime.substring(0,19) + "+00:00";
                    req_data["whatsappforzohocrm__Scheduled_Time"]=time.toString();
                    req_data["whatsappforzohocrm__Status"]="Scheduled";
                }
                filledMessage = filledMessage.trim();
                if(filledMessage.length > 3000)
                {
                    wcConfirm('Message is Too Large.','','Okay',true,false);
                    return ;
                }
                else if(filledMessage.length < 1)
                {
                    wcConfirm('Merge Fields value is empty.','','Okay',true,false);
                    return;
                }
                to = argumentsData.to?argumentsData.to.replace(/\D/g,''):"";
                console.log("mm"+filledMessage);
                recordIndex = recordIndex+1;
                if(MobileNumber && MobileNumber.length < 20){
                    successRecords=successRecords+1;
                    bulkInsertList.push(req_data);
                }
                if(recordIndex == recordsLength){
                    if(successRecords == 0){
                        wcConfirm("Mobile field is empty or invalid for all choosen " + argumentsData.module+ ".",'','Okay',true,false);
                    }
                    else{
                        if(!isAllowed){
                            showAnnounceMent('purchase_license');
                        }else{
                            showAnnounceMent('maintenence');
                        }
                        if (isAllowed) {
                            ZOHO.CRM.API.insertRecord({Entity:"whatsappforzohocrm__WhatsApp_History",APIData:bulkInsertList,Trigger:["workflow"]}).then(function(data){
                                if(scheduledTime){
                                    
                                    
                        wcConfirm('<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your Messages have been scheduled successfully.</div>','','Okay',true,false);
                                    setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload(); }, 1500);
                                    return;
                                }    
                                var mediaObj = null;
                                var user = whatcetraApp.auth().currentUser;  
                                var agent = {
                                    name: user.displayName,
                                    email: user.email,
                                    id:user.uid,
                                    photoUrl:user.photoURL
                                }
                                if(attPur && attachedfile && attachedfile.mediaUrl){
                                    mediaObj = {
                                        "url": attachedfile.mediaUrl,
                                        "name": attachedfile.fileMeta.name,
                                        "type": attachedfile.fileMeta.type,
                                        "size": attachedfile.fileMeta.size
                                    }    
                                    attachedfile = null;
                                }   
                                for(let i=0;i<data.data.length;i++){
                                    if(data.data[i].code == "SUCCESS"){
                                        var details = data.data[i].details;
                                        sendBulkWhatsAppList[new Date().getTime()+i]={"contactNumber":bulkInsertList[i].whatsappforzohocrm__Recipient_Number,"text":bulkInsertList[i].whatsappforzohocrm__WhatsApp_Message,"createdTime":new Date().getTime(),"agent":agent,"media":mediaObj};
                                    }
                                }
                                addToWhatcetraQueue(sendBulkWhatsAppList);
                            }).catch(function(e){
                                console.log(e);
                            })
                        }
                        else {
                            var urlList="";
                            bulkInsertList.forEach(function(history, index){
                                if(webOrDesktop == "web"){
                                    var url = 'https://web.whatsapp.com/send?phone=' + history.whatsappforzohocrm__Recipient_Number + '&text=' + encodeURIComponent(history.whatsappforzohocrm__WhatsApp_Message);
                                }
                                else{
                                    var url = 'https://wa.me/' + history.whatsappforzohocrm__Recipient_Number + '?text=' + encodeURIComponent(history.whatsappforzohocrm__WhatsApp_Message);
                                }
                                history.url = url;
                                urlList = `${urlList}<div class="whatsappLink">
                                        <div class="contactName">${history.Name.substring(12)} </div>
                                        <div class="sendButton" style="cursor:pointer;" onclick="sendWhatsapp(${index})" id="${index}_send"> Send Now</div>
                                        <div class="sendButton sent" id="${index}_sent"> Sent </div>
                                        <div class="sendButton sending" id="${index}_sending"> Sending</div>
                                    </div>`
                            });
                            $("#whatsAppUrlList").append(urlList);
                            document.getElementById("container").style.display= "none";
                            document.getElementById("whatsAppUrlList").style.display= "block";
                        }

                    }   
                }
            }); 
        });
    }
      
}

function addToWhatcetraQueue(data,mode){
    var user = whatcetraApp.auth().currentUser;
    if(user){        
        var ref = whatcetraApp.database().ref('whatcetra/queued/' + chosenWASenderId +'/com-whatsapp');
        ref.update(data, function(error) {
            if (error) {
                console.log(error);
              // The write failed...
            } else {
                // $.ajax({url: "https://us-central1-app-whatcetra.cloudfunctions.net/updateLICCA?src="+mode+"&email="+userEmail+"&mcc="+Object.keys(sendBulkWhatsAppList).length, success: function(result){
                //     console.log(result);
                // }});   
                console.log("successfully");
              // Data saved successfully!
            }

            wcConfirm('<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your Messages have been sent successfully.</div>','','Okay',true,false);
            setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload(); }, 1500);
        });
    }       
}
function openUploader(isTemplate){
    if(isTemplate && !whatcetraApp.auth().currentUser){
        startWhatcetra();
        handleSignedOutUser();
        return;
    }
    if(attPur){
        if($('.createTemplateMainOuterDiv').is(':visible'))
        document.getElementById("inputFileAttachCreate").click();
        else        
        document.getElementById("inputFileAttach").click();
    }
    else{

        wcConfirm(`<div class="sendingLoaderOuter" style="padding-top: 15px;height: 75px;"><div class="sendingMsgSuccess">${`<svg xmlns="http://www.w3.org/2000/svg" height="40" width="40"><path d="M19.375 16.25q1.833 0 4.021 1.271 2.187 1.271 3.687 3.687h-3.875q-.25 0-.375.146t-.125.313q0 .25.125.375t.375.125h4.375q.5 0 .813-.313.312-.312.312-.812v-4.417q0-.167-.146-.313-.145-.145-.312-.145-.25 0-.375.145-.125.146-.125.313v3.917q-1.625-2.709-4.021-3.98-2.396-1.27-4.354-1.27-2.958 0-5.146 1.77-2.187 1.771-2.854 4.355-.083.291.042.521.125.229.333.229.167 0 .312-.105.146-.104.188-.354.542-2.291 2.521-3.875 1.979-1.583 4.604-1.583ZM20 34.417q-3 0-5.625-1.125t-4.583-3.084q-1.959-1.958-3.084-4.583T5.583 20q0-3 1.125-5.625t3.084-4.583q1.958-1.959 4.583-3.084T20 5.583q3 0 5.625 1.125t4.583 3.084q1.959 1.958 3.084 4.583T34.417 20q0 3-1.125 5.625t-3.084 4.583q-1.958 1.959-4.583 3.084T20 34.417Z"/></svg>`}</div><div class="sendingMsgSuccessHint" style="top: 5px;"><a href="https://apps.ulgebra.com/zoho/crm/whatsapp-web/pricing" target="_blank" style="text-shadow: 0px 0px 1px rgb(0 0 0 / 70%);filter: contrast(0.5);text-decoration: underline;">${'Upgrade'}</a> ${'&nbsp;your plan to Business or Premium to access this feature.'}</div></div>`, '', 'Okay', true, false);
        showNoHighPlanError(true, true);
       /* if(!document.getElementById("wa-attach-tip")){
            $("#attachDiv").append(`
            <span id="wa-attach-tip" style="display: block;margin: 5px;border: 1px dotted crimson;padding: 7px 15px;background-color: rgba(200,0,0,0.1);word-break: unset;border-radius: 26px;font-size: 14px;color: crimson;">You need to purchase license to access this feature. <a href="https://apps.ulgebra.com/hubspot/whatsapp-web/whatsapp-web-for-hubspot" target="_blank">More info</a><div class="" style="
                    margin-top: 5px;
                "><a href="https://forms.gle/3k6bCPSRWBpEfifL6" target="_blank" style="
                    background-color: green;
                    color: white;
                    padding: 4px 15px 5px;
                    border-radius: 25px;
                    box-shadow: 0 2px 2px rgba(0,0,0,0.2);
                ">Request Access </a>
                </div> </span>
            `);
        }   */ 
    }
}
function fileSizeCheckWc(fileSize) {
    if(fileSize < 1000000){
        fileSize = fileSize/1024 ;
    }else{
        fileSize = fileSize/(1024*1024) ;
        if(fileSize > 5) {
            alert(`file size is ${fileSize.toFixed(2)}Mb.
            Please select file less than 5Mb.`);
            return false;
        }
    }
    return true;
}

var attachedfile={}
function attachFileChange(e) {

    var FileButton;
    if($('.createTemplateMainOuterDiv').is(':visible'))
    FileButton = document.getElementById("inputFileAttachCreate");
    else
    FileButton = document.getElementById("inputFileAttach");

    var file = e.files[0];
    if(!fileSizeCheckWc(file.size)) {
        return false;
    }
    var metadata = {
                      customMetadata: {
                        'file_name': file.name,
                        'file_size': file.size
                      }
                    }

    let fileTyle;
    if(file.type.includes('image/')) {
        fileTyle = 'image';
    }
    else if(e.files[0].type.includes('audio/')) {
        fileTyle = 'audio';
    }
    else if(file.type.includes('video/')) {
        fileTyle = 'video';
    }
    else if(file.type.includes('application/')) {
        if(file.type.includes('pdf')) {
            fileTyle = 'application/pdf';
        }
        else if(file.type.includes('zip')) {
            fileTyle = 'application/zip';
        }
        else if(file.type.includes('excel')) {
            fileTyle = 'application/excel';
        }
        else {
            fileTyle = 'application/msdownload';
        }
    }
    else if(file.type.includes('text/')) {
        fileTyle = 'text';
    }
    fileUploadComplete = false;
    let uniqFileName = file.name+',wcUniqNameId='+Date.now();
    var task = whatcetraApp.storage().ref('whatcetra/'+currentUser.uid+'/'+fileTyle+'/'+uniqFileName).put(file, metadata);
    if($('.createTemplateMainOuterDiv').is(':visible'))
    $('#attachedfileCreate').html(`<div class="loadingdiv"> file is uploading...</div>`);
    else
    $('#attachedfile').html(`<div class="loadingdiv"> file is uploading...</div>`);
    task.on('state_changed',
        function progress(snapshot){
            switch (snapshot.state) {
              case firebase.storage.TaskState.PAUSED: // or 'paused'
                //console.log('Upload is paused');
                break;
              case firebase.storage.TaskState.RUNNING: // or 'running'
                //console.log('Upload is running');
                break;
            }
        },
        function error(err){
            console.log("file not uploaded.")
            fileUploadComplete = true;
        },
        function complete(){
            if($('.createTemplateMainOuterDiv').is(':visible'))
            document.getElementById("inputFileAttachCreate").value = "";
            else
            document.getElementById("inputFileAttach").value = "";
            let storageRef =  whatcetraApp.storage().ref('whatcetra/'+currentUser.uid+'/'+fileTyle+'/'+uniqFileName);
            storageRef.getDownloadURL().then(imgURL =>{
                attachedfile = {"mediaUrl":imgURL,"fileMeta":file};
                $('.loadingdiv').remove();
                if($('.createTemplateMainOuterDiv').is(':visible'))
                $('#attachedfileCreate').html(`${file.name}`);
                else
                $('#attachedfile').html(`${file.name}`);
                fileUploadComplete = true;
                  // sendMessage('message', true, {
                  //   "url": imgURL,
                  //   "name": file.name,
                  //   "type": file.type,
                  //   "size": file.size
                  // });
            });

        }
    );

}

var handleSignedOutUser = function () {
    $("#notSignedInError").show();
    $(".signedIn").hide();
    $('#wloader').hide();
    $(".signedOut").show();
    $("#firebaseui-auth-container").show();
    $(".showAfterSignIn").hide();
    $("#credential_picker_container").show();
};

function getSafeString(rawStr) {
    if (!rawStr || rawStr.trim() === "") {
        return "";
    }
    return $('<textarea/>').text(rawStr).html();
}

function doPageSignAction(){
    
}

var handleSignedInUser = function (user) {
    $("#credential_picker_container").hide();
    currentUser = whatcetraApp.auth().currentUser;
    $("#notSignedInError").hide();
    $(".showAfterSignIn").show();
    $(".signedIn").show();
    $(".signedOut").hide();
    if(currentUser.photoURL){
        $("#signInUserImg").attr({
            "src": currentUser.photoURL
        });
    }
    else{
        $("#signInUserImg").attr({
            "src": "https://cdn.iconscout.com/icon/free/png-256/account-profile-avatar-man-circle-round-user-30452.png"
        });
    }    
    $("#ua-email-id").text(currentUser.email);
    $("#signInUserName").text(currentUser.displayName);
    $("#signedProfileUserLink").attr({
        "href": "/user?id=" + currentUser.uid
    });
    currentUser = whatcetraApp.auth().currentUser;
    doPageSignAction(true);
};



function sendReVerifyMail(){
    currentUser.sendEmailVerification();
    $("#sentmailstatustext").hide(500).show(500);
}


function showElem(elem) {
    if (isMobileView) {
        elem.slideDown();
    } else {
        elem.show();
    }
}

function hideElem(elem) {
    if (isMobileView) {
        elem.slideUp();
    } else {
        elem.hide();
    }
}

function removeElem(elem){
    elem.remove();
}

function hideElemNoAnimation(elem) {
    elem.hide();
}

function mobileCheck() {
    let check = false;
    (function (a) {
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4)))
            check = true;
    })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
}

function getQueryParamValue(q) {
    let query = new URLSearchParams(window.location.search);
    return query.get(q);
}

function showProcess(text, id){
    $(".process-window-outer").show().css({'display': 'flex'});
    $("#process-window-items").append(`<div id="process-item-${id}" class="process-window-item">${text}</div>`);
}

function processCompleted(id){
    $(`#process-item-${id}`).remove();
    if(($("#process-window-items").children().length) === 0){
        $(".process-window-outer").hide();
    }
}

function showTryAgainError(){
    showErroMessage('Try again later');
}

function showInvalidCredsError(){
    showErroMessage('Given Plivo Auth ID or Authtoken is Invalid <br><br> Try again with proper Plivo credentials from <a href="https://console.plivo.com/dashboard/" title="Click to go to plivo dashboard" target="_blank" noopener nofollow>Plivo dashboard</a>.');
}

function showErroMessage(html){
    showErroWindow('Unable to process your request', html);
}

function showErroWindow(title, html){
    var id = errorId++;
   $('body').append(`<div class="error-window-outer" id="error-window-${id}">
            <div class="error-window-inner">
                <div class="error-window-title">
                    ${title}
                </div>
                <div class="error-window-detail">
                    ${html}
                </div>
                <div class="error-window-close" onclick="removeElem('#error-window-${id}')">
                   <i class="material-icons">close</i> Close
                </div>
            </div>
        </div>`);
}

function valueExists(val) {
    return val !== null && val !== undefined && val.length > 0 && val!=="null";
}

function closeAllErrorWindows(){
    $('.error-window-outer').remove();
}
