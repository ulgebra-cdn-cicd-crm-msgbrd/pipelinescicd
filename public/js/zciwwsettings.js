var extensionId= "haphhpfcpfeagcmannpebjjjpdlbhflh";
var inExtensionId= "njeihbodihkkobpnmekmkplikbkmehed";
var waExtensionId= "bjnjgehlejjebdcpfcdickaaiignnijd";
var isExInstalled = false;
var isInExtInstalled = false;
var userEmail="";
var mobileNumberSettings={};
var zsckey = "";
var serviceOrigin="";
document.addEventListener("DOMContentLoaded", function(event) {
	const urlParams = new URLSearchParams(window.location.search);
	const tab = urlParams.get('tab');
	serviceOrigin = urlParams.get('serviceOrigin'); 
	if(tab == "web"){
		defaultModules = ["Leads","Contacts","Accounts"];
	} 
	ZOHO.embeddedApp.init().then(function(){
		ZOHO.CRM.CONFIG.getCurrentUser().then((res)=>{
			currentUser = res['users'][0]; 
			userEmail = res['users'][0].email;
			checkInExtension();
			connectNow();
		});
		var getmap = {"nameSpace":"<portal_name.extension_namespace>"};
		ZOHO.CRM.CONNECTOR.invokeAPI("crm.zapikey",getmap).then(function(resp){
			zsckey = JSON.parse(resp).response;
			showWebhookURL();
		});
	});
});

function checkInExtension(){
	chrome.runtime.sendMessage(inExtensionId, { "message": "version" },function (reply){
		console.log(reply);
		if(reply && reply.version){
			$(".insideInstalled").show();
			$(".insideNotInstalled").hide();
			$(".insideinstall").hide();
			
		}
	});
}
function connectNow(fromInside){
	if(!userEmail){
		alert("Please try again");
		return;
	}
	$('#connect-btn').text('Authorizing...');
	
	$('.zinside-btn').text('Authorized...');
	$('.zinside-btn').text('Connecting Chrome Extension...');
	var userDomainURL = serviceOrigin.replace("crmplus", "crm");
	chrome.runtime.sendMessage(inExtensionId, {"keysList":["crm_url","useremail"],"message":"getStorage"}, function(response) {
		console.log(response);
		if(!response.crm_url || !response.useremail || (response.crm_url != crm_url) || (response.useremail != userEmail)){
			chrome.runtime.sendMessage(inExtensionId, {"data":{"zsckey" : zsckey, "useremail" : userEmail, "crmdomain":userDomainURL,"crm_url":crm_url},"message":"setStorage"}, function(response) {
				console.log(response);
				if(!fromInside){
					connectNow(true);
				}
				else{
					alert("something went wrong, please try again");
				}
			});
		}
		else{
			$(".insideConnected").show();
			$(".insideNotConnected").hide();
			$(".insideConnect").hide();
		}

	});
}
var sandbox = false;
var crm_url="";
function showWebhookURL(){
	var domain="com";
	if(serviceOrigin && serviceOrigin.indexOf(".zoho.") != -1){
       let baseUrl = serviceOrigin;
       domain = baseUrl.substring(baseUrl.indexOf(".zoho.")+".zoho.".length);
    }
    if(serviceOrigin && serviceOrigin.indexOf("zohosandbox.com") != -1){
        domain = "https://plugin-zohocrminsidewhatsappweb.zohosandbox.com";
        sandbox = true;
    }
    else{
    	domain = "https://platform.zoho."+domain.trim();
    }
	crm_url =domain+"/crm/v2/functions/zohocrminsidewhatsappweb__whatsappwebhandler/actions/execute?auth_type=apikey&zapikey="+zsckey;		
}
