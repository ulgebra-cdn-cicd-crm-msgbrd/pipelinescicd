({
    doInit: function(component, event, helper) {
        var recId = component.get("v.recordId");
        var objectName = "Lead";
        if (!recId) {
            component.find("forceRecord").getNewRecord(
                "WhatsApp_Template__c",
                null,
                false,
                $A.getCallback(function() {
                    var rec = component.get("v.newTemplate");
                    console.log("rec"+JSON.stringify(rec));
                    var error = component.get("v.recordError");
                    if (error || (rec === null)) {
                        console.log("Error initializing record template: " + error);
                        return;
                    }
                    else{
                        component.set("v.newTemplate.Object_Name__c","Lead");
                    }
                })
            );
      
            var action = component.get("c.getRecordDetails");
            action.setParams({
                "objectName": objectName,
                "recordId":"template"
            });
            
            // Add callback behavior for when response is received
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var fieldsMapList = response.getReturnValue();
                    component.set("v.fieldsMapList", fieldsMapList);
                    var fieldsList=[];
                    fieldsMapList.forEach(function(field){
                        field = JSON.parse(field);
                        fieldsList.push(field.label);
                    });
                    component.set("v.mergeFields", fieldsList);
                }
                else { 
                    console.log("Failed with state: " + state);
                }
            });
            $A.enqueueAction(action);
        }
        else{
            component.set("v.modalContext", "Edit");
        }   
    },
    handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            console.log("You loaded a record in");
            var action = component.get("c.getRecordDetails");
            var objectName = component.get("v.newTemplate.Object_Name__c");
            console.log("object="+objectName);
            action.setParams({
                "objectName": objectName,
                "recordId":"template"
            });
            // Add callback behavior for when response is received
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var fieldsMapList = response.getReturnValue();
                    component.set("v.fieldsMapList", fieldsMapList);
                    var fieldsList=[];
                    fieldsMapList.forEach(function(field){
                        field = JSON.parse(field);
                        fieldsList.push(field.label);
                    });
                    console.log("resp: " + response.getReturnValue());
                    component.set("v.mergeFields", fieldsList);
                }
                else { 
                    console.log("Failed with state: " + state);
                }
            });
            $A.enqueueAction(action);
        } 
    },
	selectObject : function(component, event, helper) {
		var action = component.get("c.getRecordDetails");
        var objectName = component.get("v.newTemplate.Object_Name__c");
        console.log("object="+objectName);
        action.setParams({
            "objectName": objectName,
            "recordId":"template"
        });
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var fieldsMapList = response.getReturnValue();
                component.set("v.fieldsMapList", fieldsMapList);
                var fieldsList=[];
                fieldsMapList.forEach(function(field){
                    field = JSON.parse(field);
                    fieldsList.push(field.label);
                });
                console.log("resp: " + response.getReturnValue());
                component.set("v.mergeFields", fieldsList);
            }
            else { 
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
	},
    storeTemplate : function(component, event, helper) {
        var tempRec = component.find("forceRecord");
        tempRec.saveRecord($A.getCallback(function(result) {
            console.log(result.state);
            var resultsToast = $A.get("e.force:showToast");
            if (result.state === "SUCCESS") {
                resultsToast.setParams({
                    "title": "Saved",
                    "message": "The record was saved."
                });
                resultsToast.fire();
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": result.recordId
                });
                navEvt.fire();                
            } else if (result.state === "ERROR") {
                console.log('Error: ' + JSON.stringify(result.error));
                resultsToast.setParams({
                    "title": "Error",
                    "message": "There was an error saving the record: " + JSON.stringify(result.error)
                });
                resultsToast.fire();
            } else {
                console.log('Unknown problem, state: ' + result.state + ', error: ' + JSON.stringify(result.error));
            }
        }));
    },
    cancelDialog : function(component, helper) {
        var recId = component.get("v.recordId");
        if (!recId) {
            var homeEvt = $A.get("e.force:navigateToObjectHome");
            homeEvt.setParams({
                "scope": "Property__c"
            });
            homeEvt.fire();
        } else {
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": recId
            });
            navEvt.fire();
        }
	},
    insertMergeField: function(cmp, event) {
        var triggerCmp = cmp.find("message");
        var objectName = cmp.get("v.newTemplate.Object_Name__c");
        if(objectName == null){
            objectName = "Lead";
        }
        if (triggerCmp) {
            var source = event.getSource();
            var label = source.get("v.label");
            var msg = triggerCmp.get("v.value");
            if(msg == null){
                msg ='';
            }
            msg=msg+" {!"+objectName+"."+label+"}";
            triggerCmp.set("v.value", msg);
        }
    },
})