({
    doInit: function(component, event, helper) {
        var myPageRef = component.get("v.pageReference");
        var accs = myPageRef.state.c__listofContacts;
        console.log('listofContacts',JSON.stringify(accs));
        component.set("v.listofContacts",accs);
        
        var listAction = component.get("c.getListDetails");
        // Add callback behavior for when response is received
        listAction.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("templates: " + response.getReturnValue());
                //component.set("v.templates", response.getReturnValue());
            }
            else{
                 console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(listAction);
    },
    selectObject: function(component, event,helper){
        var objectName = event.getSource().get("v.value");
        console.log("eni"+objectName);
        helper.getFields(component,objectName,"template",function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var fieldsMapList = response.getReturnValue();
                component.set("v.fieldsMapList", fieldsMapList);
             	var phoneFields =[];
                var fieldsList=[];
               
                fieldsMapList.forEach(function(field){
                    field = JSON.parse(field);
                    fieldsList.push(field.label);
                    if(field.fieldType == "PHONE"){
                        phoneFields.push(field.label);
                    }
                });
                console.log("resp: " + response.getReturnValue());
                component.set("v.phoneFields", phoneFields);
                component.set("v.mergeFields", fieldsList);
            }
            else { 
                triggerCmp.set("v.label", state);
                console.log("Failed with state: " + state);
            }
        });
        helper.getFields(component,"azurna__SMS_History__c","template",function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.historyFields", response.getReturnValue());
                console.log("historyFields: " + response.getReturnValue());
            }
            else { 
                console.log("Failed with state: " + state);
            }
        });
        var templateAction = component.get("c.getTemplates");
        console.log("object"+objectName);
        templateAction.setParams({
            "objectName": objectName,
        });
        // Add callback behavior for when response is received
        templateAction.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("templates: " + response.getReturnValue());
                component.set("v.templates", response.getReturnValue());
            }
            else{
                 console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(templateAction);
    },
    sendWhatsapp : function(component, event, helper) {
        var message = component.find("message");
        if(!message.checkValidity()){
            message.reportValidity();
            return;
        }
    	var objectName = component.get("v.sObjectName");
        var currentRecord = component.get("v.contact");
        var msg = component.find("message").get("v.v alue");
        var fieldsMapList = component.get("v.fieldsMapList");
        fieldsMapList.forEach(function(field){
            field = JSON.parse(field);
            try{
                var label = field.label.replace(/\s/g, '');
                var replace = "{!"+objectName+"."+label+"}";
                var re = new RegExp(replace,"g");
                if(currentRecord[field.apiName] != null)
                {
                    msg = msg.replace(re,currentRecord[field.apiName]);
                }
                else
                {
                    msg = msg.toString().replace(re," ");
                }
            }
            catch(e){
                console.log("Error on regex replace ="+field.label+" error="+e);
			}
        });
        var Mobile =component.find("phoneNumber").get("v.value");
        var url = "https://web.whatsapp.com/send?phone=" + Mobile + "&text=" + encodeURIComponent(msg);
        var action = component.get("c.saveHistory");
        var history = component.get("v.newHistory");
        history.azurna__Recipient_Phone__c=Mobile;
        history.azurna__Message__c=msg;
        history.azurna__Status__c="Sent";
        component.get("v.historyFields").forEach(function(field){
            field = JSON.parse(field);
            if(field.objectName != null && field.objectName == objectName){
                history[field.apiName]= component.get("v.recordId");
            }
        });
        action.setParams({
            "hist": history
        });
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var triggerCmp = component.find("phone");
            var state = response.getState();
            if (state === "SUCCESS") {
                 //triggerCmp.set("v.label",JSON.stringify(response.getReturnValue()));
                console.log("resp: " + response.getReturnValue());
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Saved",
                    "message": "The SMS History record was saved."
                });
                resultsToast.fire();
                window.open(url,"_blank");
        		var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": component.get("v.recordId")
                });
                navEvt.fire();   
                // Close the action panel
                //var dismissActionPanel = $A.get("e.force:closeQuickAction");
                //dismissActionPanel.fire();
                
            }
            else{
                 triggerCmp.set("v.label", state);
                 console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);     
    },
    checkValidityOfphone : function(component, event, helper) {
      console.log('checkValidityOfphone called');
      var inp = event.getSource();
      var phone=inp.get('v.value');
      helper.getResponse(component, phone);
    },
    updatePhoneNumber: function(cmp, event, helper) {
        var source = event.getSource();
        var label = source.get("v.value");
        var phone = label.substring(label.indexOf("(")+1,label.length-1);
        helper.getResponse(cmp, phone);
        //cmp.find("phoneNumber").set("v.value",phone);
    },
    insertMergeField: function(cmp, event) {
        var triggerCmp = cmp.find("message");
        var objectName = cmp.get("v.sObjectName");
        var val = event.getParam("value");
        val = val.replace(/\s/g, '');
        var msg = triggerCmp.get("v.value");
        if(msg == null){
            msg = '';
        }
        msg=msg+" {!"+objectName+"."+val+"}";
        triggerCmp.set("v.value", msg);
    }, 
    showTemplate: function(cmp, event) { 
        console.log("ss"+event.getSource().get("v.value"));
        if(event.getSource().get("v.value") =="select"){
            cmp.find("message").set("v.value", "");
            return;
        }
        var templates = cmp.get("v.templates");
        var selectedTemplate = templates.find(function(template){
           if(template.Id == event.getSource().get("v.value")){
               return true;
           }
        });
        var msg =selectedTemplate.azurna__Message__c;  
        console.log("msg="+msg);
        cmp.find("message").set("v.value", msg);
    }
})