({
	 getFields : function(component, objectName,recordId, callback) {
        // Update the items via a server-side action
        var action = component.get("c.getRecordDetails");
         action.setParams({"objectName" : objectName,"recordId":recordId});
        // Set any optional callback and enqueue the action
        if (callback) {
            action.setCallback(this, callback);
        }
        $A.enqueueAction(action);
    },
    getResponse: function(component, phone) {
        // create a server side action.       
        var action = component.get("c.getPhoneValidation");
       
        action.setParams({
            "url": 'https://rest.messagebird.com/lookup/' + phone
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log("resp="+JSON.stringify(response.getReturnValue()));
                var resp = response.getReturnValue();
                var inputField = component.find('phoneNumber');
                if(resp.countryPrefix == null){
                	inputField.setCustomValidity("Please add CountryCode");
                	inputField.reportValidity();
                }
                else{
                    inputField.setCustomValidity("");
                    inputField.reportValidity();
                }
                // set the response(return Map<String,object>) to response attribute.      
               
            }
        });
 
        $A.enqueueAction(action);
    },
})