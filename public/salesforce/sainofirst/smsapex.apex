public class smsApex {
    @AuraEnabled
    public static list<listViewWrapper> listValues(string objectInfo){
 
        list<listViewWrapper> oListViewWrapper = new list<listViewWrapper>();
 
        for(ListView lv : [SELECT id, Name, DeveloperName FROM ListView
                           WHERE sObjectType = : objectInfo ORDER By Name ASC]){ 
            listViewWrapper oWrapper = new listViewWrapper();
            oWrapper.label = lv.Name;
            oWrapper.developerName = lv.DeveloperName;
            oListViewWrapper.add(oWrapper);
        }
        
        return oListViewWrapper; 
    }
 
    /*wrapper class to store listView details*/ 
    public class listViewWrapper{
        @AuraEnabled public string label{get;set;} 
        @AuraEnabled public string developerName{get;set;} 
    }  
    @AuraEnabled
    public static List<String> getRecordDetails(String objectName,String recordId) {
        try{
            Map<String, Schema.SObjectField> fields = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();            
            string query = '';        
            List<String> fieldMaps = new List<String>();
            for(String fieldName : fields.keySet()) {
                query = query+fieldName+', ';
                Schema.SObjectField field = fields.get(fieldName);
                Schema.DescribeFieldResult fieldDescribe = field.getDescribe();
                Map<String, Object> result = new Map<String, Object>();          
                result.put('apiName',fieldDescribe.getName());
                result.put('label',fieldDescribe.getLabel());
                result.put('fieldType',fieldDescribe.getType());
                if(String.valueOf(fieldDescribe.getType()) == 'REFERENCE' ){
                    result.put('objectName',String.valueOf(fieldDescribe.getReferenceTo()).substringBetween('(',')'));
                }
                fieldMaps.add(JSON.Serialize(result));
            }   
            query = query.substring(0,query.length()-2);
            if(recordId != '' && recordId != 'template'){
                fieldMaps.add(0,JSON.Serialize(Database.query('SELECT ' + query + ' FROM ' + objectName+' WHERE Id=:recordId')[0]));
            }    
            return fieldMaps;  
        }
        catch(Exception e){
            List<String> error = new List<String>();
            error.add(JSON.Serialize(e.getMessage()));
            return error;
        }
    }
    @AuraEnabled
    public static List<SMS_Template__c> getTemplates(String objectName) {
        return [SELECT Id, Name, Message__c FROM SMS_Template__c WHERE Object_Name__c=:objectName];
    }
    @AuraEnabled
    public static String sendSMS (SMS_History__c hist) {
        try{
            sainofirst_credential__c sc = sainofirst_credential__c.getOrgDefaults();
            String senderid=sc.Sender_Id__c;
            String route=sc.Route__c;
            String Token =sc.Token__c;
        
            String message = (String)hist.get('Message__c');
            String encodedstring = EncodingUtil.urlEncode(message,'UTF-8');
            String url = 'https://api.sainofirst.com/api/apis/bulk-sms?token=' + Token + '&senderid=' + senderid + '&route=' + route + '&number=' + hist.get('Recipient_Phone__c') + '&message=' + encodedstring;
            
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(url);
            req.setMethod('GET');
            req.setHeader('Content-Type','application/json');
            HttpResponse res = h.send(req);
            System.debug('response:--> ' + res.getBody());
            String response = JSON.Serialize(res.getBody());
            if(response.indexOf('Sms sent successfully') != -1){
                upsert hist;
            }
            return response;
        }
        catch(Exception e){
            return JSON.Serialize(e.getMessage());
        }
    }
    @AuraEnabled
    public static String sendBulkSMS (Map<String,String> bulkSMSDetails) {
        
        try{ 
            sainofirst_credential__c sc = sainofirst_credential__c.getOrgDefaults();
            String senderid=sc.Sender_Id__c;
            String route=sc.Route__c;
            String Token =sc.Token__c;
            
            String objectName = (String)bulkSMSDetails.get('objectName');
            String phoneField = (String)bulkSMSDetails.get('phoneField');
            String lookupField = objectName+'__c';
            
              System.debug('phoneField:--> '+phoneField);
            System.debug('objectName:--> '+objectName);
             System.debug('Token:--> '+Token);
           /* Map<String, Schema.SObjectField> historyfields = Schema.getGlobalDescribe().get('SMS_History__c').getDescribe().fields.getMap();            
            System.debug('historyfields:--> ' + historyfields);
            
            List<String> historyfieldMaps = new List<String>();
            for(String fieldName : historyfields.keySet()) {
                Schema.SObjectField field = historyfields.get(fieldName);
            	Schema.DescribeFieldResult fieldDescribe = field.getDescribe();
            	if(String.valueOf(fieldDescribe.getType()) == 'REFERENCE' ){
                    if(objectName == String.valueOf(fieldDescribe.getReferenceTo()).substringBetween('(',')')){
                    	lookupField = fieldDescribe.getName();
                    }
            	}
            }    */
            
            Map<String, Schema.SObjectField> fields = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();            
            string query = '';        
            List<String> fieldMaps = new List<String>();
            for(String fieldName : fields.keySet()) {
                query = query+fieldName+', ';
            }    
            query = query.substring(0,query.length()-2);
            List<Object> recordIds = (List<Object>)JSON.deserializeUntyped(bulkSMSDetails.get('recordIds'));
            Set<Id> recordIdSet = new Set<Id>();
            for (integer i = 0; i < recordIds.size(); i++) {
                recordIdSet.add((Id)recordIds[i]);
            }
            List<sObject> objects = Database.query('SELECT ' + query + ' FROM ' + objectName+' WHERE Id IN :recordIdSet');
            String successIds = '';
            List<SMS_History__c> histories = new List<SMS_History__c>();
            for(sObject obj :objects){
                String message = (String)bulkSMSDetails.get('msg');
                String num = '';
                for(String fieldName : fields.keySet()) {
                    Schema.SObjectField field = fields.get(fieldName);
                    Schema.DescribeFieldResult fieldDescribe = field.getDescribe();
                    if(fieldDescribe.getLabel() == phoneField && obj.get(fieldDescribe.getName()) != null){
                        num = (String)obj.get(fieldDescribe.getName());
                    }
                    String label = fieldDescribe.getLabel();
                    label = label.replaceAll(' ', '');
                    if(obj.get(fieldDescribe.getName()) != null){
                      
                    	message = message.replaceAll('\\{!'+objectName+'.'+label+'\\}',String.valueOf(obj.get(fieldDescribe.getName())));
                    }
                    else{
                        message = message.replaceAll('\\{!'+objectName+'.'+label+'\\}','');
                    }                                 
                }
                if(num != ''){
                    String encodedstring = EncodingUtil.urlEncode(message,'UTF-8');
                                                    
                    String url = 'https://api.sainofirst.com/api/apis/bulk-sms?token=' + Token + '&senderid=' + senderid + '&route=' + route + '&number=' + num + '&message=' + encodedstring;
                    
                    Http h = new Http();
                    HttpRequest req = new HttpRequest();
                    req.setEndpoint(url);
                    req.setMethod('GET');
                    req.setHeader('Content-Type','application/json');
                    HttpResponse res = h.send(req);
                    System.debug('response:--> ' + res.getBody());
                    String response = JSON.Serialize(res.getBody());
                    if(response.indexOf('Sms sent successfully') != -1){
                        successIds = successIds +(String)obj.get('Id')+',';
                        SMS_History__c hist = new SMS_History__c(Recipient_Phone__c = num,Message__c = message); 
                       	hist.put(lookupField,obj.get('Id'));
                        histories.add(hist);
                    }
                }    
            }
            for(SMS_History__c history :histories){
                insert history;
            }    
            if(successIds.length() > 0){
            	successIds = successIds.removeEnd(',');
            }    
            return JSON.Serialize(successIds);
       }
        catch(Exception e){
            System.debug('bulkerror:--> ' + e);
            return JSON.Serialize(e.getMessage());
        }
    }
    @AuraEnabled
	public static Map <String,Object> getPhoneValidation(String url) {
 
		// Instantiate a new http object
		Http h = new Http();
 
		// Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
		HttpRequest req = new HttpRequest();
		req.setEndpoint(url);
		req.setMethod('GET');
        req.setHeader('Authorization','AccessKey 7NMPor0R8DofSHH61SpViNNqQ');
 
		// Send the request, and return a response
		HttpResponse res = h.send(req);
		System.debug('response:--> ' + res.getBody());
 
		// Deserialize the JSON string into collections of primitive data types.
		Map < String,
		Object > resultsMap = (Map < String, Object > ) JSON.deserializeUntyped(res.getBody());
		system.debug('resultsMap-->' + resultsMap);
 
		return resultsMap;
	}
}