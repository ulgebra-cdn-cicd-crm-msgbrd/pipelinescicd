({
    doInit: function(component, event, helper) {
        var myPageRef = component.get("v.pageReference");
        var accs = myPageRef.state.c__listofReocordIds;
        var objectName =  myPageRef.state.c__objectName;
        console.log('listofReocordIds',JSON.stringify(accs));
        console.log('objectName',objectName);
        if(objectName){
            component.find("object").set("v.value", objectName);
            component.set("v.showObject",false);
            component.set("v.showOthers",true);
            
            component.set("v.listofReocordIds",accs.split(','));
            helper.getFields(component,objectName,"template",function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var fieldsMapList = response.getReturnValue();
                    component.set("v.fieldsMapList", fieldsMapList);
                    var phoneFields =[];
                    var fieldsList=[];
                   
                    fieldsMapList.forEach(function(field){
                        field = JSON.parse(field);
                        fieldsList.push(field.label);
                        if(field.fieldType == "PHONE"){
                            phoneFields.push(field.label);
                        }
                    });
                    console.log("resp: " + response.getReturnValue());
                    component.set("v.phoneFields", phoneFields);
                    if(phoneFields.length){
                    	component.find("phone").set("v.value",phoneFields[0]);
                    }    
                    component.set("v.mergeFields", fieldsList);
                }
                else { 
                    triggerCmp.set("v.label", state);
                    console.log("Failed with state: " + state);
                }
            });
            var templateAction = component.get("c.getTemplates");
            console.log("object"+objectName);
            templateAction.setParams({
                "objectName": objectName,
            });
            // Add callback behavior for when response is received
            templateAction.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    console.log("templates: " + response.getReturnValue());
                    component.set("v.templates", response.getReturnValue());
                }
                else{
                     console.log("Failed with state: " + state);
                }
            });
            $A.enqueueAction(templateAction);
        }
        else{
            var action = component.get("c.listValues");
            action.setParams({
                "objectInfo" : component.get("v.objectInfo")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var listViewResult = response.getReturnValue();
                    if(listViewResult.length > 0){
                        // set listViewResult attribute with response
                        component.set("v.listViewResult",listViewResult);
                        // set first value as default value
                        component.set("v.currentListViewName", "azurna__"+listViewResult[0].developerName);
                        // rendere list view on component
                        component.set("v.bShowListView", true);     
                    }            } 
                else if (state === "INCOMPLETE") {
                }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
            });
            $A.enqueueAction(action);
        }
     },
    reInit : function(component, event, helper) {
    	$A.get('e.force:refreshView').fire();
    },
    onPicklistChange: function(component, event, helper) {
        // unrenders listView 
        component.set("v.bShowListView", false); 
        
        // get current selected listview Name 
        var lstViewName = event.getSource().get("v.value"); 
        
        // set new listName for listView
        console.log("ss"+lstViewName);
        component.set("v.currentListViewName","azurna__"+lstViewName);
        
        // rendere list view again with new listNew  
        component.set("v.bShowListView", true); 
    },
    selectObject: function(component, event,helper){
        var objectName = event.getSource().get("v.value");
        if(objectName == "select"){
           component.set("v.bShowListView", false); 
           component.set("v.listViewResult",[]);
           return;
        }
        console.log("en"+objectName);
        component.set("v.objectInfo",objectName);
        var action = component.get("c.listValues");
        action.setParams({
            "objectInfo" : component.get("v.objectInfo")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var listViewResult = response.getReturnValue();
                if(listViewResult.length > 0){
                    // set listViewResult attribute with response
                    component.set("v.bShowListView", false); 
                    component.set("v.listViewResult",listViewResult);
                    // set first value as default value
                    component.set("v.currentListViewName", "azurna__"+listViewResult[0].developerName);
                    // rendere list view on component
                    component.set("v.bShowListView", true);     
                }            } 
            else if (state === "INCOMPLETE") {
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    sendWhatsapp : function(component, event, helper) {
        var message = component.find("message");
        if(!message.checkValidity()){
            message.reportValidity();
            return;
        }
    	var objectName = component.get('v.objectInfo');
        var phoneField = component.find("phone").get("v.value");
        var msg = component.find("message").get("v.value");
        var recordIds = component.get("v.listofReocordIds");
        var action = component.get("c.sendBulkSMS");
        var bulkSMSDetails = {"msg":msg,"objectName":objectName,"phoneField":phoneField,"recordIds":JSON.stringify(recordIds)};
        console.log("aa"+JSON.stringify(bulkSMSDetails));
        action.setParams({
            "bulkSMSDetails": bulkSMSDetails
        });
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
               console.log("smsresp: " + response.getReturnValue());
                var resultsToast = $A.get("e.force:showToast");
                var successIds = response.getReturnValue().split(",");
                if(successIds.length > 0){
                     resultsToast.setParams({
                        "title": "Saved",
                        "message": successIds.length+" SMS History records was saved."
                    });
                }
                else{
                     resultsToast.setParams({
                        "title": "Error",
                        "message": "Something went Wrong. Please check sainofirst credential or Phone number."
                    });
                }  
                resultsToast.fire();              
        		//var navEvt = $A.get("e.force:navigateToSObject");
                //navEvt.setParams({
                //    "recordId": component.get("v.recordId")
                //});
                //navEvt.fire();   
                // Close the action panel
                //var dismissActionPanel = $A.get("e.force:closeQuickAction");
                //dismissActionPanel.fire();
                
            }
            else{
                 console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);   
    },
    checkValidityOfphone : function(component, event, helper) {
      console.log('checkValidityOfphone called');
      var inp = event.getSource();
      var phone=inp.get('v.value');
      helper.getResponse(component, phone);
    },
    insertMergeField: function(cmp, event) {
        var triggerCmp = cmp.find("message");
        var objectName = component.get('v.objectInfo');
        var val = event.getParam("value");
        val = val.replace(/\s/g, '');
        var msg = triggerCmp.get("v.value");
        if(msg == null){
            msg = '';
        }
        msg=msg+" {!"+objectName+"."+val+"}";
        triggerCmp.set("v.value", msg);
    }, 
    showTemplate: function(cmp, event) { 
        console.log("ss"+event.getSource().get("v.value"));
        if(event.getSource().get("v.value") =="select"){
            cmp.find("message").set("v.value", "");
            return;
        }
        var templates = cmp.get("v.templates");
        var selectedTemplate = templates.find(function(template){
           if(template.Id == event.getSource().get("v.value")){
               return true;
           }
        });
        var msg =selectedTemplate.azurna__Message__c;  
        console.log("msg="+msg);
        cmp.find("message").set("v.value", msg);
    }
})