({
    doInit: function(component, event, helper) {
        // Create the action
      

        var action = component.get("c.getContacts");
        var recordId = component.get("v.recordId");

        action.setParams({
            "recordId": recordId
        });
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
           

            var state = response.getState();
           
            if (state === "SUCCESS") {
            	
                console.log("resp: " + response.getReturnValue());
               // component.set("v.contact", response.getReturnValue());
            }
            else { 
               
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
        var fieldsAction = component.get("c.getFieldsList");
 
        fieldsAction.setParams({
            "objectName": "Contact"
        });
        // Add callback behavior for when response is received
        fieldsAction.setCallback(this, function(response) {
           
            var state = response.getState();
           
            if (state === "SUCCESS") {
                var fieldsMapList = response.getReturnValue();
                var phoneFields =[];
                var fieldsList=[];
                fieldsMapList.forEach(function(field){
                    field = JSON.parse(field);
                    fieldsList.push(field.label);
                    if(field.fieldType == "PHONE"){
                        phoneFields.push(field.label);
					}
                });
            	
                console.log("resp: " + response.getReturnValue());
                component.set("v.phoneFields", phoneFields);
                component.set("v.mergeFields", fieldsList);
            }
            else { 
                triggerCmp.set("v.label", "saen");
                console.log("Failed with state: " + state);
            }
        });
        // Send action off to be executed
        $A.enqueueAction(fieldsAction);
        
    },
    sendWhatsapp : function(component, event, helper) {
    	var cmpMsg = component.find("msg");
    	$A.util.removeClass(cmpMsg, 'hide');
    	
        var comments = component.find("comments").get("v.value");
        var Mobile ="918012178547";
        var url = "https://web.whatsapp.com/send?phone=" + Mobile + "&text=" + encodeURIComponent(comments);
		window.open(url,"_blank");
    },
    updateTriggerLabel: function(cmp, event) {
        var triggerCmp = cmp.find("phone");
        if (triggerCmp) {
            var source = event.getSource();
            var label = source.get("v.label");
            triggerCmp.set("v.label", label);
        }
    },
    insertMergeField: function(cmp, event) {
        var triggerCmp = cmp.find("comments");
        if (triggerCmp) {
            var source = event.getSource();
            var label = source.get("v.label");
            triggerCmp.set("v.value", label);
        }
    }
})