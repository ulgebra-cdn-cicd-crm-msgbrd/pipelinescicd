var recordId;
var recordModule;
var zsckey;
function makeAcuityCall(data){
	if(data && !data.method){
		data.method = "GET";
	}
	data.zapikey = zsckey;
	return fetch("https://us-central1-ulgebra-license.cloudfunctions.net/makeAcuityHTTPRequestFromZoho", {
        "method" :"POST",
        "body":JSON.stringify(data) 
      })
    .then(function (response) {  
        console.log(response);
        return response.json();
    })
    .then(function (responseJSON) {
        console.log(responseJSON);
        if(responseJSON && responseJSON.error && responseJSON.error.message == "user_not_found"){
			$("#acuityAuthorize").show();
			return null;
        }
        return responseJSON;
    })
    .catch(function (error) {
        console.log("Error: " + error);
        return false;
    });
}
document.addEventListener("DOMContentLoaded", function(event) { 
    ZOHO.embeddedApp.on("PageLoad", function(record) {
       	recordId = record.EntityId[0];
       	recordModule = record.Entity;
    });

    ZOHO.embeddedApp.init().then(async function(){
		try{
			var getmap = {"nameSpace":"<portal_name.extension_namespace>"};
			var resp = await ZOHO.CRM.CONNECTOR.invokeAPI("crm.zapikey",getmap);
			zsckey = JSON.parse(resp).response;
			var apiKeyData = await makeAcuityCall({"url":"https://acuityscheduling.com/api/v1/me"});
			if(apiKeyData && apiKeyData.status == 200){
        		var bookingUrl = apiKeyData.data.schedulingPage;
				ZOHO.CRM.API.getRecord({"Entity":recordModule,"RecordID":recordId+""}).then(async function(customerData){
					if(recordModule == "Deals"){
						if(customerData.data[0].Contact_Name){
							var contactId = customerData.data[0].Contact_Name.id;
							var customerData = await ZOHO.CRM.API.getRecord({"Entity":"Contacts","RecordID":contactId+""});
						}
						var CalendarsMapDetails = await ZOHO.CRM.API.getOrgVariable("acuityschedulingforzohocrm__CalendarsMap");
						if(CalendarsMapDetails && CalendarsMapDetails.Success && CalendarsMapDetails.Success.Content){
							var calendarsMap = JSON.parse(CalendarsMapDetails.Success.Content);
							if(calendarsMap && calendarsMap.dealFieldId){
								var dealFieldId = calendarsMap.dealFieldId;
							}
						}	
					}
					var customer = "&";
					if(dealFieldId){
						customer = customer +"field:"+dealFieldId+"="+recordId+"&";
					}
					if(customerData.data[0].Email != null){
						customer = customer +"email="+encodeURIComponent(customerData.data[0].Email)+"&";
					}
					if(customerData.data[0].First_Name != null){
						customer = customer +"first_name="+encodeURIComponent(customerData.data[0].First_Name)+"&";
					}
					if(customerData.data[0].Phone != null){
						customer = customer +"phone="+encodeURIComponent(customerData.data[0].Phone)+"&";
					}
					else if(customerData.data[0].Mobile != null){
						customer = customer +"phone="+encodeURIComponent(customerData.data[0].Mobile)+"&";
					}
					customer = customer +"last_name="+encodeURIComponent(customerData.data[0].Last_Name);

					var iframe = document.createElement('iframe');
					iframe.src = bookingUrl+customer;
					iframe.width = "100%";
					iframe.height = "100%";
					iframe.frameborder="0";
					iframe.id = "myframe";
					iframe.style ="overflow:hidden;height:100%;width:100%" ;
					document.body.appendChild(iframe);
					document.getElementById("myframe").onload = function(){
						document.getElementById("loader").style.display = "none";
					}
				});
			}	
		}
		catch(e){
			console.log(e);
		}
	});
});