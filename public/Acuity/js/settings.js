// var emailContentText = "Hi ,\n\nHope you're well. I'm writing to know how our business can help you.\n\nPlease choose a event to schedule an appointment:\n\n${eventsUrls} See you soon!\n\n %ownerName% \n\n %companyName%";
		var emailContentForContacts = [];
		var emailContentForLeads = [];
		var emailContentText = "";
		var startIndex =0;
		var endIndex = 0;
		var currentEditor="";
		var subject="";
		var sel;
		var range;
		var calendarsMap;
		var zsckey;
		var crm_url="";
        const urlParams = new URLSearchParams(window.location.search);
		var serviceOrigin = urlParams.get('serviceOrigin'); 
		function makeAcuityCall(data){
			if(data && !data.method){
				data.method = "GET";
			}
			data.zapikey = zsckey;
			return fetch("https://us-central1-ulgebra-license.cloudfunctions.net/makeAcuityHTTPRequestFromZoho", {
		        "method" :"POST",
		        "body":JSON.stringify(data)
		      })
		    .then(function (response) {  
		        console.log(response);
		        return response.json();
		    })
		    .then(function (responseJSON) {
		        console.log(responseJSON);
		        if(responseJSON && responseJSON.error && responseJSON.error.message == "user_not_found"){
		        	 $("#acuityAuthorize").show();
		        	 $("#acuityTable").hide();
		        	 return null;
		        }
		        return responseJSON;
		        // if (responseJSON.type != null) {
		        //     return true;
		        // }    
		        // else if(responseJSON.errors){
		        //     let errText = responseJSON.errors[0].description;
		        //     if(errText.indexOf("incorrect access_key")>0){
		        //         showErroWindow('Invalid Access Key', errText);
		        //     }
		        //     else{
		        //        showErroWindow('Unable to Integrate', errText);
		        //     }
		        //     return false;
		        // }
		    })
		    .catch(function (error) {
		        console.log("Error: " + error);
		        return false;
		    });
		}
		async function checkWebhook(){
			var isWebhookFound = false;
			var webhooks = await makeAcuityCall({"url":"https://acuityscheduling.com/api/v1/webhooks","method":"GET"});
			if(webhooks && webhooks.data && webhooks.data.length){
				webhooks.data.forEach(function(webhook){
					if(webhook.target == crm_url && webhook.status == "active"){
						isWebhookFound = true;
					}
				})
			}
			if(!isWebhookFound){
				await makeAcuityCall({"url":"https://acuityscheduling.com/api/v1/webhooks","method":"POST","data":{"event":"appointment.changed","target":crm_url}});
			}
		}
		function AuthorizeAcuity(){
			var acuityurl = "https://acuityscheduling.com/oauth2/authorize?response_type=code&scope=api-v1&client_id=voGW0lZ67jEzzB4O&redirect_uri=https://us-central1-ulgebra-license.cloudfunctions.net/oauthCallbackAcuity&state=acuityforzohocrm-"+zsckey;
            window.open(acuityurl,"_blank");
		}
        document.addEventListener("DOMContentLoaded", function(event) {
        	
        	ZOHO.embeddedApp.init().then(async function(){
    		    var getmap = {"nameSpace":"<portal_name.extension_namespace>"};
		        var resp = await ZOHO.CRM.CONNECTOR.invokeAPI("crm.zapikey",getmap);
		        zsckey = JSON.parse(resp).response;
		        var domain="com";
				if(serviceOrigin && serviceOrigin.indexOf(".zoho.") != -1){
			       let baseUrl = serviceOrigin;
			       domain = baseUrl.substring(baseUrl.indexOf(".zoho.")+".zoho.".length);
			    }
			    if(serviceOrigin && serviceOrigin.indexOf("zohosandbox.com") != -1){
			        domain = "https://plugin-calendlyforzohocrm.zohosandbox.com";
			        sandbox = true;
			    }
			    else{
			        domain = "https://platform.zoho."+domain.trim();
			    }
				crm_url =domain+ "/crm/v2/functions/acuityschedulingforzohocrm__acuitynotificiationhandler/actions/execute?auth_type=apikey&zapikey="+zsckey;
		        await checkWebhook();
        		var calendars;
        		var calendarDetails = await makeAcuityCall({"url":"https://acuityscheduling.com/api/v1/calendars"});
				if(calendarDetails && calendarDetails.status == 200){
					calendars = calendarDetails.data;
					ZOHO.CRM.API.getAllUsers({Type:"AllUsers"}).then(function(data){
						ZOHO.CRM.API.getOrgVariable("acuityschedulingforzohocrm__CalendarsMap").then(function(CalendarsMapDetails){
							if(CalendarsMapDetails && CalendarsMapDetails.Success && CalendarsMapDetails.Success.Content){
								// {"activityMapEnable":"true","activityMap":{"3388882":[]},"blocktimeMapEnable":"true","blocktimeMap":{"ulgebra@zoho.com":["3388882"]}}
								calendarsMap = JSON.parse(CalendarsMapDetails.Success.Content);
								// calendarsMap = {"initialSyncActivityStatus":"Request","initialSyncBlockTimeStatus":"Request","activityMapEnable":"true","activityMap":{"3388882":["ulgebra@zoho.com"]},"blocktimeMapEnable":"true","blocktimeMap":{"ulgebra@zoho.com":["3388882"]}};
								// if(calendarsMap != "0" && calendarsMap != "" && calendarsMap && calendarsMap.){
									var dropdown = "";
									var acuityDropdown = "";
									dropdown = '<option value="null">select</option>';
									acuityDropdown = '<option value="null">select</option>';
								    data.users.forEach(function(user){
                                        if(user.status == "active"){
								    	    dropdown = dropdown+'<option value="'+user.id+'">'+user.email+'</option>';
                                        }    
								    });

								    	// dropdown = dropdown+'<option value="'+'jkxds@sndbfs.sfgsd'+'">'+'jkxds@sndbfs.sfgsd'+'</option>';
								    $('#activityMap').append('<div class="calendar"><h6><u>Acuity Calendars</u></h6></div><i class="arrow material-icons">trending_flat</i><div class="calendar"><h6><u>Zoho CRM users</u></h6></div>');
								    calendars.forEach(function(calendar){
								    	$('#activityMap').append('<div class="calendar">'+calendar.name+'('+calendar.id+')'+'</div><i class="arrow material-icons">trending_flat</i><div class="calendar styled-select slate"><select id="calendarsmap'+calendar.id+'" onchange="updateOrgVariables('+"'acuityschedulingforzohocrm__CalendarsMap'"+',this,'+"'activityMap'"+')"></select></div>');
								    	$('#calendarsmap'+calendar.id).append(dropdown);
								    	if(calendarsMap && calendarsMap.activityMap && calendarsMap.activityMap[calendar.id] && calendarsMap.activityMap[calendar.id][0]){
								    		document.getElementById('calendarsmap'+calendar.id).value = calendarsMap.activityMap[calendar.id][0];
								    	}
								    	else{
								    		document.getElementById('calendarsmap'+calendar.id).value = "null";
								    	}
								    	acuityDropdown = acuityDropdown+'<option value="'+calendar.id+'">'+calendar.name+'('+calendar.id+')'+'</option>';
							    	});
							    	$('#blocktimeMap').append('<div class="calendar"><h6><u>Zoho CRM users</u></h6></div><i class="arrow material-icons">trending_flat</i><div class="calendar"><h6><u>Acuity Calendars</u></h6></div>');
							    	data.users.forEach(function(user){
                                        if(user.status == "active"){
                                            $('#blocktimeMap').append('<div class="calendar">'+user.email+'</div><i class="arrow material-icons">trending_flat</i><div class="calendar styled-select slate"><select id="calendarsmap'+user.id+'" onchange="updateOrgVariables('+"'acuityschedulingforzohocrm__CalendarsMap'"+',this,'+"'blocktimeMap'"+')"></select></div>');
                                            $('#calendarsmap'+user.id).append(acuityDropdown);
                                            if(calendarsMap && calendarsMap.blocktimeMap && calendarsMap.blocktimeMap[user.id] && calendarsMap.blocktimeMap[user.id][0]){
                                                document.getElementById('calendarsmap'+user.id).value = calendarsMap.blocktimeMap[user.id][0];
                                            }
                                            else{
                                                document.getElementById('calendarsmap'+user.id).value = "null";
                                            }
                                        }    
								    });
								    if(calendarsMap){
									    document.getElementById("activityMapEnable").checked = calendarsMap.activityMapEnable;
									    document.getElementById("blocktimeMapEnable").checked = calendarsMap.blocktimeMapEnable;
									    if(!calendarsMap.activityMapEnable){
									    	document.getElementById("activityMapEnableOpaque").classList.add("opaque");
									    }
									    if(!calendarsMap.blocktimeMapEnable){
									    	document.getElementById("blocktimeMapEnableOpaque").classList.add("opaque");
									    }
									}  
									else{
										document.getElementById("activityMapEnable").checked = false;
									    document.getElementById("blocktimeMapEnable").checked = false;
									    document.getElementById("activityMapEnableOpaque").classList.add("opaque");
									    document.getElementById("blocktimeMapEnableOpaque").classList.add("opaque");
									}  
								// }	
							    // initialSyncKeys =["initialSyncActivity","initialSyncBlockTime"];
							    // initialSyncKeys.forEach(function(key){
								   //  if(calendarsMap[key+'Status'] == "Request"){
								   //  	document.getElementById(key+'Success').style.display = "none";
								   //  	document.getElementById(key+'Requested').style.display = "none";
								   //  }
								   //  else if(calendarsMap[key+'Status'] == "Requested"){
								   //  	document.getElementById(key+'Success').style.display = "none";
								   //  	document.getElementById(key+'Request').style.display = "none";
								   //  }
								   //  else if(calendarsMap[key+'Status'] == "completed"){
								   //  	document.getElementById(key).style.display = "none";
								   //  }
							    // });
						    }	
					    });	
					});
				}
				var userDetails= await makeAcuityCall({"url":"https://acuityscheduling.com/api/v1/me"});
            	if(userDetails && userDetails.status == 200){
                	var schedulingPage = userDetails.data.schedulingPage;
                	addListItem("EmailForContacts","Scheduling Link","dropdown-header");
            		addListItem("EmailForContacts","Scheduling Link","dropdown-item",schedulingPage+"&%customerInfo%");
            		addListItem("EmailForLeads","Scheduling Link","dropdown-header");
            		addListItem("EmailForLeads","Scheduling Link","dropdown-item",schedulingPage+"&%customerInfo%");
            		addListItem("WhatsAppForContacts","Scheduling Link","dropdown-header");
            		addListItem("WhatsAppForContacts","Scheduling Link","dropdown-item",schedulingPage+"&%customerInfo%");
            		addListItem("WhatsAppForLeads","Scheduling Link","dropdown-header");
            		addListItem("WhatsAppForLeads","Scheduling Link","dropdown-item",schedulingPage+"&%customerInfo%");
            		var calendarDetails = makeAcuityCall({"url":"https://acuityscheduling.com/api/v1/calendars"});
            		if(calendarDetails && calendarDetails.data){
	            		var calendars = calendarDetails.data;
	            		addListItem("EmailForContacts","Calendar Scheduling Links","dropdown-header");
	            		addListItem("EmailForLeads","Calendar Scheduling Links","dropdown-header");
	            		addListItem("WhatsAppForContacts","Calendar Scheduling Links","dropdown-header");
	            		addListItem("WhatsAppForLeads","Calendar Scheduling Links","dropdown-header");
	            		// var calendarsLinks = "";
	            		// var calendarsLinksForWhatsapp = "";
	            		calendars.forEach(function(calendar){
	            			addListItem("EmailForContacts",calendar.name,"dropdown-item",schedulingPage+"&calendarID="+calendar.id+"&%customerInfo%");
	            			addListItem("EmailForLeads",calendar.name,"dropdown-item",schedulingPage+"&calendarID="+calendar.id+"&%customerInfo%");
	            			addListItem("WhatsAppForContacts",calendar.name,"dropdown-item",schedulingPage+"&calendarID="+calendar.id+"&%customerInfo%");
	            			addListItem("WhatsAppForLeads",calendar.name,"dropdown-item",schedulingPage+"&calendarID="+calendar.id+"&%customerInfo%");
	            			// calendarsLinksForWhatsapp = calendarsLinksForWhatsapp+schedulingPage+"&calendarID="+calendar.id+"&%customerInfo%<br>";
	            			// calendarsLinks = calendarsLinks+"<a href='"+schedulingPage+"&calendarID="+calendar.id+"&%customerInfo%"+"'>"+calendar.name+'('+calendar.id+')'+"</a><br>";
	            		});
	            	}	
            	}	
            	var eventDetails = await makeAcuityCall({"url":"https://acuityscheduling.com/api/v1/appointment-types"});
            	if(eventDetails && eventDetails.response){
	        		var events = JSON.parse(eventDetails.response);
	        		addListItem("EmailForContacts","Service Scheduling Links","dropdown-header");
	        		addListItem("EmailForLeads","Service Scheduling Links","dropdown-header");
	        		addListItem("WhatsAppForContacts","Service Scheduling Links","dropdown-header");
	        		addListItem("WhatsAppForLeads","Service Scheduling Links","dropdown-header");
	        		var eventsLinks = "";
	        		var eventsLinksForWhatsapp = "";
	        		events.forEach(function(event){
	        			if(event.active == true){
	            			addListItem("EmailForContacts",event.name,"dropdown-item",event.schedulingUrl+"&%customerInfo%");
	            			addListItem("EmailForLeads",event.name,"dropdown-item",event.schedulingUrl+"&%customerInfo%");
	            			addListItem("WhatsAppForContacts",event.name,"dropdown-item",event.schedulingUrl+"&%customerInfo%");
	            			addListItem("WhatsAppForLeads",event.name,"dropdown-item",event.schedulingUrl+"&%customerInfo%");
	            			eventsLinksForWhatsapp = eventsLinksForWhatsapp+event.schedulingUrl+"&%customerInfo%<br>";
	            			eventsLinks = eventsLinks+"<a href='"+event.schedulingUrl+"&%customerInfo%"+"'>"+event.name+"</a><br>";
	            		}	
	        		});
	        	}	
    			var cusotmerData = ["Full_Name","First_Name","Last_Name"];
    			ZOHO.CRM.API.getOrgVariable("acuityschedulingforzohocrm__email_content_for_contacts").then(function(messageData){
        			var cusotmerData = ["Full_Name","First_Name","Last_Name"];
	    			addListItem("EmailForContacts","Contacts Fields","dropdown-header");
	    			cusotmerData.forEach(function(field){
	    				addListItem("EmailForContacts",field,"dropdown-item","variable");
	    			});
	    			emailContentForContacts = JSON.parse(messageData.Success.Content);
	    			for(let i=0;i <emailContentForContacts.length;i++){
						if(i==0){
			    			document.getElementById("emailContentEmailForContacts").innerHTML =emailContentForContacts[0].emailContent.replace(/<br>/g,"\n").replace("%eventsLinks%",eventsLinks);
		        			document.getElementById("subjectEmailForContacts").innerHTML =emailContentForContacts[0].subject;
						}
						else{
							cloneEditor(i,emailContentForContacts[i],"EmailForContacts");
						}
					}	
				});
				ZOHO.CRM.API.getOrgVariable("acuityschedulingforzohocrm__email_content_for_leads").then(function(messageData){
					var cusotmerData = ["Full_Name","First_Name","Last_Name"];
	    			addListItem("EmailForLeads","Leads Fields","dropdown-header");
	    			cusotmerData.forEach(function(field){
	    				addListItem("EmailForLeads",field,"dropdown-item","variable");
	    			});
	    			emailContentForLeads = JSON.parse(messageData.Success.Content);
					for(let i=0;i <emailContentForLeads.length;i++){
						if(i==0){
							document.getElementById("emailContentEmailForLeads").innerHTML =emailContentForLeads[0].emailContent.replace(/<br>/g,"\n").replace("%eventsLinks%",eventsLinks);
							document.getElementById("subjectEmailForLeads").innerHTML =emailContentForLeads[0].subject;
						}
						else{
							cloneEditor(i,emailContentForLeads[i],"EmailForLeads");
						}
					}
					document.getElementById("loader").style.display = "none";
					document.getElementById("contentDiv").style.display = "block";
					document.getElementById("google_translate_element").style.display = "block";
				});
				ZOHO.CRM.API.getOrgVariable("acuityschedulingforzohocrm__Whatapp_message_for_contact").then(function(messageData){
					document.getElementById("emailContentWhatsAppForContacts").innerHTML =messageData.Success.Content.replace(/<br>/g,"\n").replace("%eventsLinks%",eventsLinksForWhatsapp);
					var cusotmerData = ["Full_Name","First_Name","Last_Name"];
	    			addListItem("WhatsAppForContacts","Contacts Fields","dropdown-header");
	    			cusotmerData.forEach(function(field){
	    				addListItem("WhatsAppForContacts",field,"dropdown-item","variable");
	    			});
				});
				ZOHO.CRM.API.getOrgVariable("acuityschedulingforzohocrm__Whatapp_message_for_lead").then(function(messageData){
					document.getElementById("emailContentWhatsAppForLeads").innerHTML =messageData.Success.Content.replace(/<br>/g,"\n").replace("%eventsLinks%",eventsLinksForWhatsapp);
					var cusotmerData = ["Full_Name","First_Name","Last_Name"];
	    			addListItem("WhatsAppForLeads","Leads Fields","dropdown-header");
	    			cusotmerData.forEach(function(field){
	    				addListItem("WhatsAppForLeads",field,"dropdown-item","variable");
	    			});
				});	
	            ZOHO.CRM.API.getOrgVariable("acuityschedulingforzohocrm__Module").then(function(moduleData){
	            	if(moduleData && moduleData.Success && moduleData.Success.Content){
						document.getElementById("modules").value = moduleData.Success.Content;
					}	
	            });	
	            ZOHO.CRM.API.getOrgVariable("acuityschedulingforzohocrm__Salessignal").then(function(salesSignalData){
	            	if(salesSignalData && salesSignalData.Success && salesSignalData.Success.Content){
						document.getElementById("salesSignal").checked = (salesSignalData.Success.Content == "true");
					}	
	            });	
	            ZOHO.CRM.API.getOrgVariable("acuityschedulingforzohocrm__Activity").then(function(activityData){
	            	if(activityData && activityData.Success && activityData.Success.Content){
						var apiKey = activityData.Success.Content;
						document.getElementById("activities").value = activityData.Success.Content;
						if(activityData.Success.Content == "Tasks"){
							document.getElementById("note").innerHTML = "Appointment Start date will be mapped as a Due Date for the Task record in zoho CRM";
						}
						else{
							document.getElementById("note").innerHTML ="";
						}
					}	
	            });	
	           
	        });    	
        });
		// function initialSync(model){
		// 	calendarsMap['initialSync'+model+'Status'] = "Requested";
		// 	document.getElementById('initialSync'+model+'Request').style.display = "none";
		// 	document.getElementById('initialSync'+model+'Requested').style.display = "block";
		// 	var func_name = "acuityschedulingforzohocrm__acuityblocktimeapi";
  //   		var req_data ={
		// 		"arguments": JSON.stringify({
		// 		"model" : model,
		// 		"task" : "initialSync"
		// 		})
		// 		};

		// 		ZOHO.CRM.FUNCTIONS.execute(func_name, req_data)
		// 		.then(function(data){
		// 		        console.log("REST API function invoked from widget."+data);
		// 		})
		// }
		function cloneEditor(i,email,editor){
			var element = document.getElementById("emailEditor"+editor);
			var deleteButton = document.createElement("i");
			deleteButton.classList.add("edit");
			deleteButton.classList.add("material-icons");
			deleteButton.classList.add("notranslate");
			deleteButton.id = "deleteIcon"+editor+"_"+i;
			deleteButton.innerText = "delete";
			deleteButton.addEventListener("click", function(){ deleteEditor(this); });
			var clone = element.cloneNode(true);
			clone.id =element.id+"_"+i;
			clone.children[1].id = element.children[1].id+"_"+i;
			clone.children[3].id =element.children[3].id+"_"+i;
			clone.children[4].children[0].id = element.children[4].children[0].id+"_"+i;
			clone.children[4].children[0].children[0].id=element.children[4].children[0].children[0].id+"_"+i;
			clone.children[4].children[1].id=element.children[4].children[1].id+"_"+i;
			clone.children[4].children[1].children[0].id=element.children[4].children[1].children[0].id+"_"+i;
			clone.children[4].children[2].id=element.children[4].children[2].id+"_"+i;
			clone.children[4].children[3].children[0].id=element.children[4].children[3].children[0].id+"_"+i;
			clone.children[4].children[3].children[1].id=element.children[4].children[3].children[1].id+"_"+i;
			clone.children[4].children[3].children[2].id=element.children[4].children[3].children[2].id+"_"+i;
			var addButton = clone.children[4].children[3].children[1];
			addButton.parentElement.removeChild(addButton);
			clone.children[4].children[3].insertBefore(deleteButton, clone.children[4].children[3].children[1]); 
			if(email){
				clone.children[1].innerHTML = email.subject;
				clone.children[4].children[2].innerHTML=email.emailContent;
			}
			var parent = document.getElementById("emailEditors"+editor);
			parent.appendChild(clone);
		}
		
		function googleTranslateElementInit() {
		  new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
		}
        function addListItem(parent,text,className,value){
        	
			// var node = document.createElement("LI");
			// node.classList.add(className);
			if(className == "dropdown-item"){
				// var buttonNode = document.createElement("BUTTON");
				// buttonNode.classList.add(className);
				// var textnode = document.createTextNode(text);
				// buttonNode.appendChild(textnode);
				// buttonNode.addEventListener("click", insert);
				// if(value){
				// 	var x = document.createElement("INPUT");
				// 	x.setAttribute("type", "hidden");
				// 	x.setAttribute("value",value);
	   //      		buttonNode.appendChild(x);
	   //      	}
	   //      	node.appendChild(buttonNode);	
				var linode = '<li class="'+className+'"><button class="'+className+'" onclick="insert(this)">'+text+'<input type="hidden" value="'+value+'"></button></li>';
			}
			else{
				// var textnode = document.createTextNode(text);
				// node.appendChild(textnode);
				var linode = '<li class="'+className+'">'+text+'</li>';
			}
			// document.getElementById("dropdown-menu-email"+parent).appendChild(node);
			 $('#dropdown-menu-email'+parent).append(linode);

        }
        function updateOrgVariables(apiname,value,key){
    	
    		if(apiname == "acuityschedulingforzohocrm__CalendarsMap"){
    			if(key.indexOf("Enable") != -1){
    				calendarsMap[key] = value;
    				ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": apiname,"value": JSON.stringify(calendarsMap)});
    				var activityMapEnable = document.getElementById(key+'Opaque').classList.toggle("opaque");
    			}
    			else{
    				if(!calendarsMap[key]){
    					calendarsMap[key] ={};
    				}
    				calendarsMap[key][value.id.substring(12)]=[];
    				if(value.value != "null"){
    					calendarsMap[key][value.id.substring(12)][0] = value.value;
    				}
    				ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": apiname,"value": JSON.stringify(calendarsMap)});
    			}
    		}
    		else{
	    		ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": apiname,"value": value});
	    		if(apiname == "acuityschedulingforzohocrm__Activity"){
	    			if(value == "Tasks"){
	    				document.getElementById("note").innerHTML = "Appointment Start date will be mapped as a Due Date for the Task record in zoho CRM";
	    			}
	    			else{
	    				document.getElementById("note").innerHTML = "";
	    			}
	    		}	
	    	}	
        }
		function styling(tag)
		{
			document.execCommand(tag);
		}
		function link(){
			$("#linkForm"+currentEditor).slideToggle("slow");
		}
		function image(){
			$("#imageForm"+currentEditor).slideToggle("slow");
		}
		function addLink(){
			var href = document.getElementById("linkUrl"+currentEditor).value;
		    if (range) {
				if(range.startOffset == range.endOffset){
					if(range.commonAncestorContainer.parentNode.href){
						range.commonAncestorContainer.parentNode.href=href;
					}
					else{
						var span = document.createElement('a');
						span.setAttribute('href',href);
						span.innerText = href;
						range.insertNode(span);
			        	range.setStartAfter(span);
			        }	
				}
				else{
					var data = range.commonAncestorContainer.data;
					var start = range.startOffset;
					var end = range.endOffset;
					range.commonAncestorContainer.data="";
					var span = document.createElement('span');
					span.appendChild( document.createTextNode(data.substring(0,start)) );
					var atag = document.createElement('a');
					atag.setAttribute('href',href);
					atag.innerText = data.substring(start,end);
					span.appendChild(atag);
					span.appendChild( document.createTextNode(data.substring(end)) );
					range.insertNode(span);
		        	range.setStartAfter(span);
				}
		        range.collapse(true);
		    }
			$("#linkForm"+currentEditor).slideToggle("slow");
		}
		function addImage(){
			var href = document.getElementById("imageUrl"+currentEditor).value;
			var span = document.createElement('img');
			span.setAttribute('src',href);
			span.innerText = href;
			range.insertNode(span);
        	range.setStartAfter(span);
			$("#imageForm"+currentEditor).slideToggle("slow");
		}
		function openlink(){
			sel = window.getSelection();
		    if (sel && sel.rangeCount) {
		        range = sel.getRangeAt(0);
		      }  
			if(range && range.commonAncestorContainer.wholeText){
				if(range.commonAncestorContainer.parentNode.href){
					document.getElementById("linkUrl"+currentEditor).value = range.commonAncestorContainer.parentNode.href;
					$("#linkForm"+currentEditor).slideToggle("slow");
				}
			}	
		}
		function insert(bookingLink){
    		// var bookingLink = this;
			var range;
			if (sel && sel.rangeCount) {
		        range = sel.getRangeAt(0);
		        range.collapse(true);
	    		if(bookingLink.children[0].value == "variable"){
	    		    var span = document.createElement("span");
	    		    span.appendChild( document.createTextNode('%'+bookingLink.innerText+'%') );
	        		range.insertNode(span);
	    		}
	    		else{
	    			if(currentEditor.indexOf("WhatsApp") != -1){
	    				var span = document.createElement("span");
		    		    span.appendChild( document.createTextNode(bookingLink.children[0].value));
		        		range.insertNode(span);
	    			}
	    			else{
	    				var span = document.createElement('a');
						span.setAttribute('href',bookingLink.children[0].value);
						span.innerText = bookingLink.innerText;
						range.insertNode(span);
	    			}	
	    		}
	    		range.setStartAfter(span);
		        range.collapse(true);
		        sel.removeAllRanges();
		        sel.addRange(range);
		    }    
		}
		function showEditor(editor){
			currentEditor = editor.id.substring(8);
			emailContentText = document.getElementById("emailContent"+currentEditor).innerHTML;
			var coll = document.getElementsByClassName("edit")
			for(var i=0, len=coll.length; i<len; i++)
		    {
		        coll[i].style["display"] = "none";
		    }
			// document.getElementById("editIcon"+editor).style.display= "none";
			document.getElementById("editorbuttons"+currentEditor).style.display= "block";
			document.getElementById("editButtons"+currentEditor).style.display= "block";
			document.getElementById("emailContent"+currentEditor).contentEditable = "true";
			if(currentEditor.indexOf("Email") != -1){
				document.getElementById("subject"+currentEditor).contentEditable = "true";
				subject = document.getElementById("subject"+currentEditor).innerHTML;
			}	
			$('#emailContent'+currentEditor).focus();
		}
		function addEditor(editor){
			// var emailContent = document.getElementById("emailContent"+editor).innerHTML;
			// var subject = document.getElementById("subject"+editor).innerHTML;
			// var element = document.getElementById("emailEditor"+editor.id.substring(7));
			// var email ={"emailContent":emailContent,"subject":subject}
			if(editor.id.substring(7).indexOf("Contacts") != -1){
				var emailId = parseInt(emailContentForContacts[emailContentForContacts.length-1].emailId)+1;
				var customVariable ="acuityschedulingforzohocrm__email_content_for_contacts";
			}
			else{
				var emailId = parseInt(emailContentForLeads[emailContentForLeads.length-1].emailId)+1;
				var customVariable ="acuityschedulingforzohocrm__email_content_for_leads";
			}
			cloneEditor(emailId,"",editor.id.substring(7));
			currentEditor =editor.id.substring(7)+"_"+emailId;
			saveEmailContent(customVariable);
		}
		function saveEmailContent(customVariable){
			emailContentText =JSON.parse(JSON.stringify(document.getElementById("emailContent"+currentEditor).innerHTML).replace(/\\n/gi, "<br>"));
			var emailContent = emailContentText;
			if(customVariable.indexOf("email") != -1){
				subject = document.getElementById("subject"+currentEditor).innerHTML;
				var emailId = (currentEditor.indexOf("_") == -1)?0:currentEditor.substring(currentEditor.indexOf("_")+1);
				emailContent = {"subject":subject,"emailContent":emailContentText,"emailId":emailId};
				if(currentEditor.indexOf("Contacts") != -1){
					for(var i=0; i<emailContentForContacts.length;i++){
						if(emailContentForContacts[i].emailId == emailId){
							emailContentForContacts[i]=emailContent;
							break;
						}
					}
					if(i == emailContentForContacts.length){
						emailContentForContacts.push(emailContent);
					}
					updateOrgVariables(customVariable,emailContentForContacts);
				}
				else{
					for(var i=0; i<emailContentForLeads.length;i++){
						if(emailContentForLeads[i].emailId == emailId){
							emailContentForLeads[i]=emailContent;
							break;
						}
					}
					if(i == emailContentForLeads.length){
						emailContentForLeads.push(emailContent);
					}
					updateOrgVariables(customVariable,emailContentForLeads);
				}	
			}
			else{
				updateOrgVariables(customVariable,emailContent);
			}
			cancelEditor(currentEditor);
		}
		function deleteEditor(editor){
			var emailId = editor.id.substring(editor.id.indexOf("_")+1);
			if(editor.id.indexOf("Contacts") != -1){
				for(var i=0; i<emailContentForContacts.length;i++){
					if(emailContentForContacts[i].emailId == emailId){
						emailContentForContacts.splice(i,1);
						break;
					}
				}
				updateOrgVariables("acuityschedulingforzohocrm__email_content_for_contacts",emailContentForContacts);
			}
			else{
				for(var i=0; i<emailContentForLeads.length;i++){
					if(emailContentForLeads[i].emailId == emailId){
						emailContentForLeads.splice(i,1);
						break;
					}
				}
				updateOrgVariables("acuityschedulingforzohocrm__email_content_for_leads",emailContentForLeads);
			}	
			var editorElement = document.getElementById("emailEditor"+editor.id.substring(10));
			editorElement.parentElement.removeChild(editorElement);
		}
		function cancelEditor(editor){
			// currentEditor = editor.parentElement.id.substring(13);
			document.getElementById("emailContent"+currentEditor).innerHTML = emailContentText;
			var coll = document.getElementsByClassName("edit")
			for(var i=0, len=coll.length; i<len; i++)
		    {
		        coll[i].style["display"] = "block";
		    }
			// document.getElementById("editIcon"+editor).style.display= "block";
			document.getElementById("editorbuttons"+currentEditor).style.display= "none";
			document.getElementById("editButtons"+currentEditor).style.display= "none";
			document.getElementById("emailContent"+currentEditor).contentEditable = "false";
			$("#linkForm"+currentEditor).hide("slow");
			$("#imageForm"+currentEditor).hide("slow");
			if(currentEditor.indexOf("Email") != -1){
				document.getElementById("subject"+currentEditor).contentEditable = "false";
				document.getElementById("subject"+currentEditor).innerHTML = subject;
			}
			currentEditor ="";	
		}
