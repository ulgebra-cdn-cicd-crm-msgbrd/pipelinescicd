var appsConfig = {
    "AUTH_ID": undefined,
    "AUTH_TOKEN": undefined,
    "PHONE_NUMBER": undefined,
    "APPLICATION_ID": undefined,
    "APP_UNIQUE_ID": undefined
};
var initialAppsConfig = {
    "AUTH_ID": undefined,
    "AUTH_TOKEN": undefined,
    "PHONE_NUMBER": undefined,
    "APPLICATION_ID": undefined,
    "APP_UNIQUE_ID": undefined
};
var curId = 1000;
var errorId = 1000;
var initTries = 0;
var initProcessId = 1;
function syncInputValues() {
//    appsConfig.AUTH_ID = $('#input-api-key').val();
//    appsConfig.AUTH_TOKEN = $('#input-api-authtoken').val();
//    var authtokenChange = initialAppsConfig.AUTH_ID !== appsConfig.AUTH_ID || initialAppsConfig.AUTH_TOKEN !== appsConfig.AUTH_TOKEN;
//    if(authtokenChange){
//        appsConfig.PHONE_NUMBER = "null";
//    }
//    resolveCurrentProcessView();
}
function resolveCurrentProcessView() {
    $('#input-api-key').val(appsConfig.AUTH_ID);
    $('#input-api-authtoken').val("");
    $('#phone-select-label').text(appsConfig.PHONE_NUMBER);
    $('#phone-select-label').attr({'data-selectedval': appsConfig.PHONE_NUMBER});
    if (valueExists(appsConfig.AUTH_ID) && valueExists(appsConfig.AUTH_TOKEN)) {
        $('#input-api-key').attr({'readonly':true});
        $('#input-api-authtoken').attr({'readonly':true}).val(appsConfig.AUTH_TOKEN.substr(0,5)+"xxxxxxxxxxxxx");
            $('#show-on-apikey-valid').show();
            if(!valueExists(appsConfig.PHONE_NUMBER)){
                resetPhoneNumerVal();
                $("#incoming-integ-status").removeClass('c-orange').removeClass('c-green').removeClass('c-crimson').addClass('c-silver').html('<i class="material-icons">error</i> Configure <b class="c-black">SMS - From phone number</b> to enable Incoming SMS ');
            }
            else{
                $("#incoming-integ-status").removeClass('c-crimson').removeClass('c-green').removeClass('c-silver').addClass('c-orange').html('<i class="material-icons">error</i> Integration enabled');
            }
    }
    else {
        $('#input-api-key').removeAttr('readonly');
        $('#input-api-authtoken').removeAttr('readonly');
        $("#incoming-integ-status").removeClass('c-silver').removeClass('c-orange').removeClass('c-green').addClass('c-crimson').html('<i class="material-icons">error</i> Provide AUTH ID & Auth Token');
        $('#show-on-apikey-valid').hide();
    }
}
function showHelpWinow(manual){
    if(!valueExists(appsConfig.AUTH_ID)){
        showHelpItem('step-1');
    }
    else if(manual && !valueExists(appsConfig.PHONE_NUMBER)){
            showHelpItem('step-3');
    }
    else{
        if(manual){
            showHelpItem('step-youtube');
        }
    }
}
function resetPhoneNumerVal() {
    $('#phone-select-options').html("");
    $('#phone-select-label').text('Select a phone number');
    $('#phone-select-label').attr({'data-selectedval': 'null'});
}
function selectDropDownItem(elemId, val) {
    $('.dropdown-holder').hide();
    $('#' + elemId).text(val);
    $('#' + elemId).attr({'data-selectedval': val});
}

function showDropDown(elemId) {
    if($('#' + elemId).is(":visible")){
        $('#' + elemId).hide();
    }
    else{
        $('#' + elemId).show();
        if(elemId === 'phone-select-options'){
            fetchPhoneNumbersAndShow();
        }
    }
    
}
function valueExists(val) {
    return val !== null && val !== undefined && val.length > 1 && val!=="null";
}
function showTryAgainError(){
    showErroMessage('Try again later');
}
function showInvalidCredsError(){
    showErroMessage('Given Plivo Auth ID or Authtoken is Invalid <br><br> Try again with proper Plivo credentials from <a href="https://console.plivo.com/dashboard/" title="Click to go to plivo dashboard" target="_blank" noopener nofollow>Plivo dashboard</a>.');
}

function associateAPPToPhoneNumber(){
    if(!valueExists(appsConfig.APPLICATION_ID)){
        showErroWindow('Something went wrong', 'Try resetting the app configuration and try again <br><br><div class="help-step-btn bg-crimson" onclick="closeAllErrorWindows();resetAllFields()">Reset app configuration</div>');
        return;
    }
     var reqObj = {
        url: `https://api.plivo.com/v1/Account/${appsConfig.AUTH_ID}/Number/${appsConfig.PHONE_NUMBER}/`,
        type: "POST",
        headers: {
            "Authorization": 'Basic ' + btoa(appsConfig.AUTH_ID + ":" + appsConfig.AUTH_TOKEN),
            "Content-Type": "application/json"
        },
        body: {
            "app_id" : appsConfig.APPLICATION_ID
        }
    };
    //$('#phone-select-options').html(`<div class="statusMsg">Loading...</div>`);
    var phoneNumberProcess = curId++;
    showProcess(`Configuring Incoming SMS for ${appsConfig.PHONE_NUMBER} to CRM`, phoneNumberProcess);
    ZOHO.CRM.HTTP.post(reqObj).then(function (response) {
        var responseJSON = JSON.parse(response);
        processCompleted(phoneNumberProcess);
        if (response.indexOf("changed")!= -1) {
            fetchPhoneNumbersAndShow(true);
        }
        else{
            showErroWindow('Associate phone numbers to CRM App in Plivo', `Kindly associate phone numbers to Zoho CRM application manually in <a href="https://console.plivo.com/phone-numbers/" title="Click to go to plivo dashboard" target="_blank" noopener nofollow>Plivo phone numbers configuration</a>.`);
        }
    }).catch(function (err) {
        showTryAgainError();
        processCompleted(phoneNumberProcess);
        console.log(err);
    });
}

function resetAllFields(){
    if(valueExists(initialAppsConfig.AUTH_ID)){
       
    ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "plivosmsforzohocrm__AUTH_ID","value": "null"}).then(function(res){
        appsConfig.AUTH_ID = null;
                        console.log(res);
                        resolveCurrentProcessView();
                    }).catch(function (err) {
                        showTryAgainError();
                        console.log(err);
                    });
                }
                if(valueExists(initialAppsConfig.AUTH_TOKEN)){
                    ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "plivosmsforzohocrm__AUTH_TOKEN","value": "null"}).then(function(res){
                        appsConfig.AUTH_TOKEN = null;
                        console.log(res);
                        resolveCurrentProcessView();
                    }).catch(function (err) {
                        showTryAgainError();
                        console.log(err);
                    });
                }
                if(valueExists(initialAppsConfig.PHONE_NUMBER)){
                     ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "plivosmsforzohocrm__plivo_number","value": "null"}).then(function(res){
                        appsConfig.PHONE_NUMBER = null;
                        console.log(res);
                        resolveCurrentProcessView();
                    }).catch(function (err) {
                        showTryAgainError();
                        console.log(err);
                    });
                    deleteApplicationInPlivo();
                }
                else if(valueExists(initialAppsConfig.APPLICATION_ID)){
                     deleteApplicationInPlivo();
                }
}
function deleteApplicationInPlivo(){
    if(!valueExists(appsConfig.APPLICATION_ID)){
        return;
    }
    var reqObj = {
        url: `https://api.plivo.com/v1/Account/${appsConfig.AUTH_ID}/Application/${appsConfig.APPLICATION_ID}/`,
        type: "DELETE",
        headers: {
            "Authorization": 'Basic ' + btoa(appsConfig.AUTH_ID + ":" + appsConfig.AUTH_TOKEN)
        },
        body: {}
    };
    var applicationProcess = curId++;
    showProcess(`Removing CRM application in Plivo ...`, applicationProcess);
    ZOHO.CRM.HTTP.delete(reqObj).then(function (response) {
        processCompleted(applicationProcess);
        if (response) {
                    ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "plivosmsforzohocrm__APPLICATION_ID","value": "null"}).then(function(res){
                        appsConfig.APPLICATION_ID = "null";
                        console.log(res);
                        resolveCurrentProcessView();
                    }).catch(function (err) {
                        showTryAgainError();
                        console.log(err);
                    });
        }
    }).catch(function (err) {
        showTryAgainError();
        processCompleted(applicationProcess);
        console.log(err);
    });
}
function closeAllErrorWindows(){
    $('.error-window-outer').remove();
}
function getOldApplication(){
    var reqObj = {
        url: `https://api.plivo.com/v1/Account/${appsConfig.AUTH_ID}/Application/`,
        type: "GET",
        headers: {
            "Authorization": 'Basic ' + btoa(appsConfig.AUTH_ID + ":" + appsConfig.AUTH_TOKEN),
            "Content-Type": "application/json"
        },
    };
    appId = null;
    return ZOHO.CRM.HTTP.get(reqObj).then(function (response) {
        JSON.parse(response).objects.forEach(function(application){
            if(application.app_name == "Zoho CRM Integration"){
                appId = application.app_id;
            }
        });
        return appId;
        console.log(response);
     });
}
function createApplicationInPlivo(){
    var callbackURL = appsConfig.replyUrl;
    var reqObj = {
        url: `https://api.plivo.com/v1/Account/${appsConfig.AUTH_ID}/Application/`,
        type: "POST",
        headers: {
            "Authorization": 'Basic ' + btoa(appsConfig.AUTH_ID + ":" + appsConfig.AUTH_TOKEN),
            "Content-Type": "application/json"
        },
        body: {
            "app_name" : "Zoho CRM Integration",
            "answer_url": callbackURL+"&action=answer",
            "message_url" : callbackURL+"&action=message",
            "default_number_app" : true,
            "default_endpoint_app": true
        }
    };
    var applicationProcess = curId++;
    showProcess(`Configuring CRM application in Plivo ...`, applicationProcess);
     ZOHO.CRM.HTTP.post(reqObj).then(function (response) {
        if(JSON.parse(response).error == "Application with name Zoho CRM Integration already exists for this account"){
            getOldApplication().then(function(oldApplicationId){
                if(oldApplicationId){
                    processCompleted(applicationProcess);
                    appsConfig.APPLICATION_ID = oldApplicationId;
                    initialAppsConfig.APPLICATION_ID =  oldApplicationId;
                    if(valueExists(appsConfig.APPLICATION_ID)){
                        ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "plivosmsforzohocrm__APPLICATION_ID","value": appsConfig.APPLICATION_ID}).then(function(res){
                            resolveCurrentProcessView();
                                console.log(res);
                            }).catch(function (err) {
                                console.log(err);
                            });
                    }
                }
                else{
                    processCompleted(applicationProcess);
                    showErroWindow('Delete the Zoho CRM Integration application' , 'Kindly delete the  Zoho CRM Integration application in <a href="https://console.plivo.com/sms/applications/" title="Click to go to plivo dashboard" target="_blank" noopener nofollow>Plivo Applications configuration</a>.');
                }    
            });
        }
        else{
            processCompleted(applicationProcess);
            if (response) {
                var data = JSON.parse(response);
                console.log(data);
                appsConfig.APPLICATION_ID = data.app_id;
                initialAppsConfig.APPLICATION_ID =  data.app_id;
                if(valueExists(appsConfig.APPLICATION_ID)){
                    ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "plivosmsforzohocrm__APPLICATION_ID","value": appsConfig.APPLICATION_ID}).then(function(res){
                        resolveCurrentProcessView();
                            console.log(res);
                        }).catch(function (err) {
                            console.log(err);
                        });
                }
            }
        }    
    }).catch(function (err) {
        showTryAgainError();
        processCompleted(applicationProcess);
        console.log(err);
    });
}

function showProcess(text, id){
    $(".process-window-outer").show();
    $("#process-window-items").append(`<div id="process-item-${id}" class="process-window-item">${text}</div>`);
}

function showErroMessage(html){
    showErroWindow('Unable to process your request', html);
}
function showErroWindow(title, html){
    var id = errorId++;
   $('body').append(`<div class="error-window-outer" id="error-window-${id}">
            <div class="error-window-inner">
                <div class="error-window-title">
                    ${title}
                </div>
                <div class="error-window-detail">
                    ${html}
                </div>
                <div class="error-window-close" onclick="removeElem('#error-window-${id}')">
                   <i class="material-icons">close</i> Close
                </div>
            </div>
        </div>`);
}
function removeElem(sel){
    $(sel).remove();
}
function hideElem(sel){
    $(sel).hide();
}
function showHelpItem(helpId){
    $('#helpWindow').show();
    $('.help-window-item').hide();
    $("#"+helpId).show();
}
function processCompleted(id){
    $(`#process-item-${id}`).remove();
    if(($("#process-window-items").children().length) === 0){
        $(".process-window-outer").hide();
    }
}