var extensionName = 'Plivo';
var extensionAPI = 'plivosmsforzohocrm__';

// var extensionConnectorName = 'ringcentral';
// var extensionConnector = extensionAPI.split('__')[0]+"."+extensionConnectorName;

var extensionFunction = 'sendsmsapi';
var extensionInvokeAPI = extensionAPI + extensionFunction;
var extensionCredential = extensionAPI + 'inSettings';
var extensionCredentialOut = extensionAPI + 'credentials';

var inSettings = {};
var authId;
var authToken;

async function invokeConnector(ApiName, body) {

    return await ZOHO.CRM.CONNECTOR.invokeAPI(extensionConnector+"."+ApiName, body).then(function(data) {

        if(data && data.response)
        return JSON.parse(data.response);
        else
        return false;

    });

}

async function isAuthorizedToConnector() {
    
    return await ZOHO.CRM.CONNECTOR.isConnectorAuthorized(extensionConnector).then(function(data) {
        
        if(data && data != "true")
        return false;
        else
        return true;

    });

}

async function authorizedToConnector() {

    return await ZOHO.CRM.CONNECTOR.authorize(extensionConnector).then(function(data) {
        
        if(data && data != "true")
        return false;
        else
        return true;
        
    });

}

var checkModules = [];
var checkModulesNames = [];
var createModule = "";

async function newAuthorizedToConnector() {

    await authorizedToConnector().then(async function(Authorized) {

        document.getElementById("loader").style.display= "flex";
        isAuthorized = Authorized;
        if(!Authorized)
        window.location.reload();
        else {
            await reAuthorizedToConnector();
        }

    });

}

document.addEventListener("DOMContentLoaded", async function(event) {

    await ZOHO.embeddedApp.init().then(async function() {

        await getAPIURL();
        

        await reAuthorizedToConnector();

        await ZOHO.CRM.API.getOrgVariable("plivosmsforzohocrm__AUTH_ID").then(async function(apiKeyData){   
            
            if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content && apiKeyData.Success.Content != "null" && apiKeyData.Success.Content != "0"){
                apikey = apiKeyData.Success.Content;
                if(apikey) {

                	await ZOHO.CRM.API.getOrgVariable("plivosmsforzohocrm__AUTH_TOKEN").then(function(apiKeyData){   
			            
			            if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content && apiKeyData.Success.Content != "null" && apiKeyData.Success.Content != "0"){
			                senderId = apiKeyData.Success.Content;
			                if(senderId) {
			                    CredentialsUpdateHide();
			                    authId = apikey;
			                    authToken = senderId;
			                    document.getElementById("apikey").value=authId;
			                    document.getElementById("senderId").value=authToken; 
			                    //document.getElementById("extraId").value=credentials.extraId; 
			                }
			                else {
			                    $('.cancelSettingsButt').hide();
			                    CredentialsUpdateShow();
			                }
			            }   
			            else {
		                    $('.cancelSettingsButt').hide();
		                    CredentialsUpdateShow();
		                }
			            document.getElementById("loader").style.display= "none";

			        });
                }
                else {
                    $('.cancelSettingsButt').hide();
                    CredentialsUpdateShow();
                }
            }
            else {
                $('.cancelSettingsButt').hide();
                CredentialsUpdateShow();
            }  
            document.getElementById("loader").style.display= "none";

            ZOHO.CRM.API.getOrgVariable("plivosmsforzohocrm__plivo_number").then(function(apiKeyData){
			        		if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content && valueExists(apiKeyData.Success.Content)){
			        			 appsConfig.PHONE_NUMBER = apiKeyData.Success.Content;
		                   		 initialAppsConfig.PHONE_NUMBER = apiKeyData.Success.Content;
		                   		 fetchPhoneNumbersAndShow();
			        			// document.getElementById("plivonumber").value=apiKeyData.Success.Content;
			        		}	

			        		resolveCurrentProcessView();
			        	});	
			        	ZOHO.CRM.API.getOrgVariable("plivosmsforzohocrm__APPLICATION_ID").then(function(apiKeyData){
			        		if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content && valueExists(apiKeyData.Success.Content)){
			        			 appsConfig.APPLICATION_ID = apiKeyData.Success.Content;
		                   		 initialAppsConfig.APPLICATION_ID = apiKeyData.Success.Content;
			        		}	
			        		document.getElementById("loader").style.display= "none";
		        			document.getElementById("contentDiv").style.display= "block";
			        	});	
			        	
			        	ZOHO.CRM.API.getOrgVariable("plivosmsforzohocrm__EventReminderSettings").then(function(salesSignalData){
			        		document.getElementById("loader").style.display= "none";
		        			document.getElementById("contentDiv").style.display= "block";
			        		if(salesSignalData && salesSignalData.Success && salesSignalData.Success.Content && salesSignalData.Success.Content != "0"){
			        			templateSettings =JSON.parse(salesSignalData.Success.Content);
			        			document.getElementById("salesSignal").checked = (templateSettings.enable == true);
			        		}	
				        	ZOHO.CRM.API.searchRecord({Entity:"plivosmsforzohocrm__SMS_Templates",Type:"criteria",Query:"(plivosmsforzohocrm__Module_Name:equals:Events)"})
							.then(function(data){
								smsTemplates = data.data;
								if(data.data){
									var templateList ="";
									for(let i=0;i <data.data.length;i++){ 
										if(templateSettings.templateId == data.data[i].id){
											document.getElementById("selectedTemplate").innerText = data.data[i].Name;
											document.getElementById("tooltiptext").innerText = data.data[i].Name;
										}
										templateList =templateList+ '<li class="templateItem" id="'+data.data[i].id+'" onclick="saveTemplateVariable(this)"></li>';
										// templateList =templateList+ '<option  value="'+data.data[i].id+'"">'+data.data[i].Name+'</option>';
									}
									$('#templateList').append(templateList);
									if(templateList == ""){
										$('#templateList').append('<li style="text-align:center;">No Templates</li>');
									}
									else{
										for(let i=0;i <data.data.length;i++){ 
											document.getElementById(data.data[i].id).innerText = data.data[i].Name;
										}
									}	
								}
								else{
									$('#templateList').append('<li style="text-align:center;">No Templates</li>');
								}
							});	
			        	});

        });

    });

});


async function reAuthorizedToConnector() {

          

                $('.authorizationFaild').hide();
                $('.extraSettingPage').show();

                await modulesListLoad();
                await modulesListLoadCreate();

                await ZOHO.CRM.API.getOrgVariable(extensionCredential).then(function(apiKeyData){   
            
                    if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content && apiKeyData.Success.Content != "0"){
                        credentials = JSON.parse(apiKeyData.Success.Content);
                        if(credentials.modules && credentials.createModule) {

                            checkModules = credentials.modules;
                            createModule = credentials.createModule;
                            
                            for(let i=0; i<checkModules.length;i++) {
                                let checkModule = checkModules[i];
                                $('#modulesListIncomingSup_'+checkModule).find('.modulesListIncomingSup').click();
                            }

                            $('#modulesListIncomingCreateSup_'+createModule).find('.modulesListIncomingCreateSup').click();
                        }
                        else {
                            // $('.cancelSettingsButt').hide();
                            // CredentialsUpdateShow();
                        }
                    }   

                }); 

                $('.outerOfSettingDetails').show();

                document.getElementById("loader").style.display= "none";
            

        

}

function textCopyInCommand(selected) {
    $('.webHookurlCopiedDiv').hide();
    let copyFrom = $('<textarea/>');
    copyFrom.text($(selected).attr('data-copyText'));
    $('body').append(copyFrom);
    copyFrom.select();
    document.execCommand('copy');
    copyFrom.remove();

    $('.copyTextDiv').fadeIn(500);
    $('.copyTextDiv').fadeOut(1500);
}
        

async function getAPIURL() {

    // let urlParams = new URLSearchParams(window.location.search);
    // let serviceOrigin = urlParams.get('serviceOrigin');

    // let getmap = {"nameSpace":"<portal_name.extension_namespace>"};
    // let resp = await ZOHO.CRM.CONNECTOR.invokeAPI("crm.zapikey",getmap);
    // let zapikey = JSON.parse(resp).response;
    // let domain = "com";
    // if(serviceOrigin.indexOf(".zoho.") != -1){
    //     domain = serviceOrigin.substring(serviceOrigin.indexOf(".zoho.")+6);
    // }
    // let webHookurl = `https://platform.zoho.${domain}/crm/v2/functions/${extensionInvokeAPI}/actions/execute?auth_type=apikey&zapikey=${zapikey}`;
    // document.getElementById("webHookurl").innerText = webHookurl;

    // $('.webHookurlCopyLogo').attr('data-copyText', webHookurl);

    var func_name = "plivosmsforzohocrm__bulksms";
	var req_data ={
		"arguments": JSON.stringify({
		"action": "getApiURL",	
		})
	};
	ZOHO.CRM.FUNCTIONS.execute(func_name, req_data)
	.then(function(data){
		let replyUrl = JSON.parse(data.details.output).replyUrl;
		let webHookurl = JSON.parse(data.details.output).webHookURL;//{"webHookURL":webHookURL,"replyUrl":replyUrl}
		document.getElementById("webHookurl").innerText = webHookurl;
		$('.webHookurlCopyLogo').attr('data-copyText', webHookurl);
	});

}
       
function updateOrgVariables(apiname, value, finish){
    ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": apiname,"value": value}).then(function(res) {
        if(finish) {
        	wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingMsgSuccess">${verifiedTickSVG}</div><div class="sendingMsgSuccessHint">${'Saved'}</div></div>`,'','Okay',true,true);
	        CredentialsUpdateHide();
	        setTimeout(function(){ wcConfirmHide(); }, 1500);
        }
    });
}

async function saveCreds(){

    let api_key = document.getElementById('apikey').value;
    let sender_id = document.getElementById('senderId').value;
    //let extraId = document.getElementById('extraId').value;

    if(api_key == '' || api_key == null) {
        wcConfirm('Please enter Plivo Auth Id..',"",'Okay',true,false);
        return;
    }
    else if(sender_id == '' || sender_id == null) {
        wcConfirm('Please enter Plivo Auth Token..',"",'Okay',true,false);
        return;
    }
    // else if(extraId == '' || extraId == null) {
    //     wcConfirm('Please enter Sender Id..',"",'Okay',true,false);
    //     return;
    // }
    else if(api_key == authId && sender_id == authToken) {
        wcConfirm('No Changes..',"$('#Error').hide()",'Okay',true,false);
        return;
    }
    else {

        wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingLoaderInner">${loader}</div><div class="sendingLoaderHint">Saving...</div></div>`,'','Okay',true,true);

        authId = api_key;
        authToken = sender_id;                
        //credentials['extraId'] = extraId;
        await updateOrgVariables("plivosmsforzohocrm__AUTH_ID", api_key, false);
        await updateOrgVariables("plivosmsforzohocrm__AUTH_TOKEN", sender_id, true);

    }
    
}

var loader = `<div class="wcMsgLoadingInner" title="loading…"><svg class="wcMsgLoadingSVG" width="17" height="17" viewBox="0 0 46 46" role="status"><circle class="wcMsgLoadingSvgCircle" cx="23" cy="23" r="20" fill="none" stroke-width="6" style="stroke: rgb(57 82 234);"></circle></svg></div>`;

var verifiedTickSVG = `<svg class="sendingMsgSuccessSpan" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000"><g><rect fill="none" height="24" width="24"/></g><g><path d="M23,12l-2.44-2.79l0.34-3.69l-3.61-0.82L15.4,1.5L12,2.96L8.6,1.5L6.71,4.69L3.1,5.5L3.44,9.2L1,12l2.44,2.79l-0.34,3.7 l3.61,0.82L8.6,22.5l3.4-1.47l3.4,1.46l1.89-3.19l3.61-0.82l-0.34-3.69L23,12z M10.09,16.72l-3.8-3.81l1.48-1.48l2.32,2.33 l5.85-5.87l1.48,1.48L10.09,16.72z"/></g></svg>`;

        
function wcConfirm(htmlText, runFunc, confirmTitle, alart, buttonHide) {
    $('.wcConfirmOuter').remove();
    $('body').append(`<div class="wcConfirmOuter">
        <div class="wcConfirmInner divPopUpShow">
            <div class="wcConfirmBody">
                ${htmlText}
            </div>
            <div class="wcConfirmButt" style="display:${buttonHide?'none':'flex'};">
                <div class="wcConfirmCancel" style="display:${alart?'none':'block'};" onclick="wcConfirmHide()">Cancel</div>
                <div class="wcConfirmYes" onclick="wcConfirmHide();${runFunc}">${confirmTitle}</div>
            </div>
        </div>
    </div>`);
}

function wcConfirmHide() {

        $('.wcConfirmInner').removeClass('divPopUpHide').removeClass('divPopUpShow').addClass('divPopUpHide').hide();
        $('.wcConfirmOuter').fadeOut(0);
        setTimeout(function() { $('.wcConfirmInner').removeClass('divPopUpHide').removeClass('divPopUpShow').html('').hide().fadeOut(); $('.wcConfirmOuter').remove(); }, 300);

}

$(document).keypress(function(e) {

    if($('.wcConfirmYes').is(':visible'))
    if(e.which == 13) {
        $('.wcConfirmYes').click();
    }

});

function CredentialsUpdateShow() {
    $('.extensionSettingInput').show();
    $('.hideCredentialsDetails').hide();
    $('.editCredentialsDetails').hide();
    $('.saveSettingsButtOut').show();
}

function CredentialsUpdateHide() {

    if(authId && authToken) {
        $("#apikeyHide").html(authId.substring(0,5)+"&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;");
        $("#senderIdHide").html("&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;");
        //$("#extraIdHide").html("&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;");

        $('.extensionSettingInput').hide();
        $('.hideCredentialsDetails').show();
        $('.editCredentialsDetails').show();        
        $('.cancelSettingsButt').show();
        $('.saveSettingsButtOut').hide();
    }
    
}

async function modulesListLoad() {
    await ZOHO.CRM.META.getModules().then(function(data){
        data.modules.forEach(function(module){
            addListItem("modulesListIncoming",module.module_name,"dropdown-item",module.api_name);
        });
    });
}

function addListItem(id,text,className,value){

    let linode = `<li class="${className}" id="modulesListIncomingSup_${value}"><button class="dropdown_Butt modulesListIncomingSup" onmouseover="checkModuleList_onmouseover(this)" onmouseout="checkModuleList_onmouseonmouseout(this)" onShow="false" onclick="insert(this)" value="${value}" text="${text}">${text}<span class="addModuleToCheckListSVG" style="display: none;"><svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="18px" fill="#45596b" style="fill: #45596b;"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M18 13h-5v5c0 .55-.45 1-1 1s-1-.45-1-1v-5H6c-.55 0-1-.45-1-1s.45-1 1-1h5V6c0-.55.45-1 1-1s1 .45 1 1v5h5c.55 0 1 .45 1 1s-.45 1-1 1z"></path></svg></span></button></li>`;
    $('#'+id).append(linode);

}

function checkModuleList_onmouseover(selected) {

    if($(selected).attr("onShow") == "false") {
        $(selected).find('.addModuleToCheckListSVG').show();
    }
    else {
        //$(selected).find('.addModuleToCheckListSVG').hide();
    }

}

function checkModuleList_onmouseonmouseout(selected) {

    if($(selected).attr("onShow") == "false") {
        $(selected).find('.addModuleToCheckListSVG').hide();
    }
    else {
        //$(selected).find('.addModuleToCheckListSVG').hide();
    }

}

function insert(selected) {
    if($(selected).attr("onShow") == "false") {
        //$('.checkModuleHoverDiffClass').click().removeClass('checkModuleHoverDiffClass');
        $(selected).attr("onShow", "true");
        $(selected).parent().addClass('checkModuleHoverDiffClass');
        $(selected).find('.addModuleToCheckListSVG').html(`<svg class="addModuleToCheckListSVGSvg" xmlns="http://www.w3.org/2000/svg" height="17px" viewBox="0 0 24 24" width="24px" fill="#9f1212"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M7 19h10c.55 0 1 .45 1 1s-.45 1-1 1H7c-.55 0-1-.45-1-1s.45-1 1-1z"/></svg>`).css({'right': '3px', 'top': '-1px'}).show();
        if(!checkModules.length || checkModules.indexOf($(selected).attr("value")) == -1)
        checkModules.push($(selected).attr("value"));
        if(!checkModulesNames.length || checkModulesNames.indexOf($(selected).attr("text")) == -1)
        checkModulesNames.push($(selected).attr("text"));
        let setVal = {"modules":checkModules,"createModule": createModule};
        updateOrgVariables(extensionCredential, setVal);
        if(checkModulesNames.length)
        $('.outerOfSettingDetailsBSup1BSelected').html(checkModulesNames.toString().replaceAll(',', ', '));
        else
        $('.outerOfSettingDetailsBSup1BSelected').html(`<span class="emptyouterOfSettingDetailsBSelected">No Modules</span>`);  
    }
    else {
        $(selected).attr("onShow", "false");
        $(selected).parent().removeClass('checkModuleHoverDiffClass');
        $(selected).find('.addModuleToCheckListSVG').html(`<svg class="addModuleToCheckListSVGSvg" xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="18px" fill="#45596b" style="fill: #45596b;"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M18 13h-5v5c0 .55-.45 1-1 1s-1-.45-1-1v-5H6c-.55 0-1-.45-1-1s.45-1 1-1h5V6c0-.55.45-1 1-1s1 .45 1 1v5h5c.55 0 1 .45 1 1s-.45 1-1 1z"></path></svg>`).css({'right': '8px', 'top': '5px'});
        checkModules = checkModules.filter(item => item !== $(selected).attr("value"));
        checkModulesNames = checkModulesNames.filter(item => item !== $(selected).attr("text"));
        let setVal = {"modules":checkModules,"createModule": createModule};
        updateOrgVariables(extensionCredential, setVal);
        if(checkModulesNames.length)
        $('.outerOfSettingDetailsBSup1BSelected').html(checkModulesNames.toString().replaceAll(',', ', '));
        else
        $('.outerOfSettingDetailsBSup1BSelected').html(`<span class="emptyouterOfSettingDetailsBSelected">No Modules</span>`); 
    }
}

async function modulesListLoadCreate() {
    await ZOHO.CRM.META.getModules().then(function(data){
        data.modules.forEach(function(module){
            addListItemCreate("modulesListIncomingCreate",module.module_name,"dropdown-item",module.api_name);
        });
    });
}

function addListItemCreate(id,text,className,value){

    let linode = `<li class="${className}" id="modulesListIncomingCreateSup_${value}"><button class="dropdown_Butt modulesListIncomingCreateSup" onmouseover="checkModuleListCreate_onmouseover(this)" onmouseout="checkModuleListCreate_onmouseonmouseout(this)" onShow="false" onclick="insertCreate(this)" value="${value}" text="${text}">${text}<span class="addModuleToCheckListSVGCreate" style="display: none;"><svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="18px" fill="#45596b" style="fill: #45596b;"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M18 13h-5v5c0 .55-.45 1-1 1s-1-.45-1-1v-5H6c-.55 0-1-.45-1-1s.45-1 1-1h5V6c0-.55.45-1 1-1s1 .45 1 1v5h5c.55 0 1 .45 1 1s-.45 1-1 1z"></path></svg></span></button></li>`;
    $('#'+id).append(linode);

}

function checkModuleListCreate_onmouseover(selected) {

    if($(selected).attr("onShow") == "false") {
        $(selected).find('.addModuleToCheckListSVGCreate').show();
    }
    else {
        //$(selected).find('.addModuleToCheckListSVG').hide();
    }

}

function checkModuleListCreate_onmouseonmouseout(selected) {

    if($(selected).attr("onShow") == "false") {
        $(selected).find('.addModuleToCheckListSVGCreate').hide();
    }
    else {
        //$(selected).find('.addModuleToCheckListSVG').hide();
    }

}

function insertCreate(selected) {
    if($(selected).attr("onShow") == "false") {
        $('.checkModuleCreateHoverDiffClass').removeClass('checkModuleCreateHoverDiffClass');
        $('.modulesListIncomingCreateSup').attr("onShow", "false");
        $('.modulesListIncomingCreateSup').find('.addModuleToCheckListSVGCreate').html(`<svg class="addModuleToCheckListSVGCreateSvg" xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="18px" fill="#45596b" style="fill: #45596b;"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M18 13h-5v5c0 .55-.45 1-1 1s-1-.45-1-1v-5H6c-.55 0-1-.45-1-1s.45-1 1-1h5V6c0-.55.45-1 1-1s1 .45 1 1v5h5c.55 0 1 .45 1 1s-.45 1-1 1z"></path></svg>`).css({'right': '8px', 'top': '5px'}).hide();
        
        $(selected).attr("onShow", "true");
        $(selected).parent().addClass('checkModuleCreateHoverDiffClass');
        $(selected).find('.addModuleToCheckListSVGCreate').html(`<svg class="addModuleToCheckListSVGCreateSvg" xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="18px" fill="#1c6b1c"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M17.3 6.3c-.39-.39-1.02-.39-1.41 0l-5.64 5.64 1.41 1.41L17.3 7.7c.38-.38.38-1.02 0-1.4zm4.24-.01l-9.88 9.88-3.48-3.47c-.39-.39-1.02-.39-1.41 0-.39.39-.39 1.02 0 1.41l4.18 4.18c.39.39 1.02.39 1.41 0L22.95 7.71c.39-.39.39-1.02 0-1.41h-.01c-.38-.4-1.01-.4-1.4-.01zM1.12 14.12L5.3 18.3c.39.39 1.02.39 1.41 0l.7-.7-4.88-4.9c-.39-.39-1.02-.39-1.41 0-.39.39-.39 1.03 0 1.42z"/></svg>`).css({'right': '8px', 'top': '5px'}).show();
        createModule = $(selected).attr("value");
        let setVal = {"modules":checkModules,"createModule": createModule};
        updateOrgVariables(extensionCredential, setVal);
        if(createModule)
        $('.outerOfSettingDetailsBSup2BSelected').html($(selected).attr("text"));
        else
        $('.outerOfSettingDetailsBSup2BSelected').html(`<span class="emptyouterOfSettingDetailsBSelected">No Module</span>`); 
    }
    else {
        $(selected).attr("onShow", "false");
        $(selected).parent().removeClass('checkModuleCreateHoverDiffClass');
        $(selected).find('.addModuleToCheckListSVGCreate').html(`<svg class="addModuleToCheckListSVGCreateSvg" xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="18px" fill="#45596b" style="fill: #45596b;"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M18 13h-5v5c0 .55-.45 1-1 1s-1-.45-1-1v-5H6c-.55 0-1-.45-1-1s.45-1 1-1h5V6c0-.55.45-1 1-1s1 .45 1 1v5h5c.55 0 1 .45 1 1s-.45 1-1 1z"></path></svg>`).css({'right': '8px', 'top': '5px'});
        createModule = "";
        let setVal = {"modules":checkModules,"createModule": createModule};
        updateOrgVariables(extensionCredential, setVal);
        if(createModule)
        $('.outerOfSettingDetailsBSup2BSelected').html($(selected).attr("text"));
        else
        $('.outerOfSettingDetailsBSup2BSelected').html(`<span class="emptyouterOfSettingDetailsBSelected">No Module</span>`); 
    }
}


function mainBodyClickFunction(e) {

    if($(e.target).hasClass('addModuleToCheckListSVG') || $(e.target).hasClass('addModuleToCheckListSVGSvg') || $(e.target).hasClass('dropdown_Butt') || $(e.target).hasClass('dropdown-item') || $(e.target).hasClass('dropdown-menu') || $(e.target).hasClass('dropListButt') || $(e.target).hasClass('choose') || $(e.target).hasClass('arrowIcon')){

    }
    else{
        $('.chooseModule').removeAttr("open");
    }

    if($(e.target).hasClass('addModuleToCheckListSVGCreate') || $(e.target).hasClass('addModuleToCheckListSVGCreateSvg') || $(e.target).hasClass('dropdown_Butt') || $(e.target).hasClass('dropdown-item') || $(e.target).hasClass('dropdown-menu') || $(e.target).hasClass('dropListButtCreate') || $(e.target).hasClass('createChoose') || $(e.target).hasClass('dp_arrowIcon')){

    }
    else{
        $('.createModuleDrop').removeAttr("open");
    }

}

function fetchPhoneNumbersAndShow(adviseManualConfiguration) {
		    if(!valueExists(authId) || !valueExists(authToken)){
		        showErroMessage('Provide valid Plivo AUTH ID and AUTHTOKEN');
		        return;
		    }
		    var reqObj = {
		        url: `https://api.plivo.com/v1/Account/${authId}/Number/`,
		        type: "GET",
		        headers: {
		            "Authorization": 'Basic ' + btoa(authId + ":" + authToken)
		        },
		        postBody: {}
		    };
		    $('#phone-select-options').html(`<div class="statusMsg">Loading...</div>`);
		    ZOHO.CRM.HTTP.get(reqObj).then(function (response) {
		        if (response.toString().indexOf(authId) != -1) {
		            var data = JSON.parse(response);
		            if (!(data instanceof Object)) {
		                data = JSON.parse(data);
		            }
		            $('#phone-select-options').html("");
		            var phoneNumberArray = data.objects;
		            var configuredIncomingNumbers = "";
		            if(phoneNumberArray.length === 0){
		                showErroWindow('No phone numbers has been purchased in Plivo', `Kindly purchase phone numbers in <a href="https://console.plivo.com/phone-numbers/search/" title="Click to go to purchase SMS number" target="_blank" noopener nofollow>Plivo phone numbers configuration</a> and try again<br><br><div class="help-step-btn bg-green" onclick="closeAllErrorWindows();fetchPhoneNumbersAndShow(true)">
		                              <i class="material-icons">refresh</i>  Purchased Numbers, Check Again
		                            </div>`);
		                return;
		            }
		            else{
		                for (var i in phoneNumberArray) {
		                    var obj = phoneNumberArray[i];
		                    if(valueExists(obj.application) && obj.application.indexOf(appsConfig.APPLICATION_ID)>1){
		                        configuredIncomingNumbers += obj.number;
		                    }
		                    if(obj.sms_enabled){
		                        $('#phone-select-options').append(`<div class="dropdown-select-item" onclick="selectDropDownItem('phone-select-label','${obj.number}')">${obj.country} - ${obj.number}</div>`);
		                    }
		                }
		            }
		            if(valueExists(configuredIncomingNumbers)){
		                    // $("#incoming-integ-status").removeClass('c-orange').removeClass('c-crimson').removeClass('c-silver').addClass('c-green').html(`<i class="material-icons">check_circle</i> SMS Sent to ${configuredIncomingNumbers} will be notified in CRM `);
		                }
		                else{
		                    if(adviseManualConfiguration ===  true){
		                        showErroWindow('Associate phone numbers to CRM App in Plivo', `Kindly associate phone numbers to Zoho CRM application manually in <a href="https://console.plivo.com/phone-numbers/" title="Click to go to plivo dashboard" target="_blank" noopener nofollow>Plivo phone numbers configuration</a>.<br><br><div class="help-step-btn bg-green" onclick="closeAllErrorWindows();fetchPhoneNumbersAndShow(true)">
		                              <i class="material-icons">refresh</i>  Done, Check Status
		                            </div>`);
		                    }
		                    $("#incoming-integ-status").removeClass('c-orange').removeClass('c-crimson').removeClass('c-green').addClass('c-silver').html('<i class="material-icons">error</i> None of Plivo phone numbers configured for this application');
		                }
		        }else if(JSON.parse(response)["statusCode"] === 401){

		                     showInvalidCredsError();
		                }
		                else{
		                	showDropDown('phone-select-options');
		                    showErroWindow('Unable to Plivo fetch mobile numbers', 'Ensure you have purchased Plivo mobile numbers from <a href="https://console.plivo.com/phone-numbers/search/" title="Click to go to purchase SMS number" target="_blank" noopener nofollow>Plivo phone numbers configuration</a> or Try again later.');
		                }
		    }).catch(function (err) {
		    	showDropDown('phone-select-options');
		        showErroWindow('Unable to fetch Plivo mobile numbers', 'Ensure you have provided correct AUTH ID & AUTHTOKEN. <br> <br> Ensure you have purchased Plivo mobile numbers from <a href="https://console.plivo.com/phone-numbers/search/" title="Click to go to purchase SMS number" target="_blank" noopener nofollow>Plivo phone numbers configuration</a> or Try again later.');
		        console.log(err);
		    });
		}