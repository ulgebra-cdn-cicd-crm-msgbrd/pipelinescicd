const urlParams = new URLSearchParams(window.location.search);
// const data = JSON.parse(urlParams.get('contact'));
// const contactId = data.associatedObjectId;
// const portalId =data.portalId;
var uploadFiles = [];
const contactId = urlParams.get('contactId');
const serviceOrigin = urlParams.get("serviceOrigin");

var rootFolderId = null;
// urlParams.get('rootFolderId');
var currentFolderId = null;
var filesLength = 0;
var extensionName = 'googledriveforzohocrm';
var rootFolderIdMap = {};
var recordId = null;
var recordModule = null;
var settingsMap={};
var zapikey="";
var syncedAttachmentsmap={};
var attachmentRecordId;

var folderIdApiName = "googledriveforzohocrm0__Google_Drive_Folder_Id";
var folderApiName = "googledriveforzohocrm0__Google_Drive_Folder";
var folderId = null;
var currentRecord = null;
var recordName = '';
var teamDriveAuthorized = false;
var appDriveAuthorized = false;
document.addEventListener("DOMContentLoaded",function (event) {
    ZOHO.embeddedApp.on("PageLoad", async function (record) {
        recordId = record.EntityId;
        recordModule = record.Entity;
        try{
            currentRecord = await ZOHO.CRM.API.getRecord({Entity:record.Entity,RecordID:record.EntityId});
            currentRecord = currentRecord.data[0];
            recordName = currentRecord.Full_Name?(currentRecord.Full_Name+' ('+recordId+')'):recordId;
            if(recordModule == "Deals"){
                recordName = currentRecord.Deal_Name?(currentRecord.Deal_Name+' ('+recordId+')'):recordId;
            }
            if(currentRecord && currentRecord[folderIdApiName]){
                folderId = currentRecord[folderIdApiName]; 
            }
        }
        catch(e){
            console.log(e);
        }
        var getmap = {"nameSpace":"<portal_name.extension_namespace>"};
        var resp = await ZOHO.CRM.CONNECTOR.invokeAPI("crm.zapikey",getmap);
        zapikey = JSON.parse(resp).response;
        try{
            await ZOHO.CRM.API.searchRecord({Entity:"googledriveforzohocrm0__Attachments_Sync_Settings",Type:"criteria",Query:"(Name:equals:"+recordModule+")and(googledriveforzohocrm0__RecordId:equals:"+recordId+")",delay:false}).then(function(data){
                if(data && data.data && data.data[0] && data.data[0].googledriveforzohocrm0__Synced_Attachments){
                    attachmentRecordId= data.data[0].id;
                    syncedAttachmentsmap= data.data[0].googledriveforzohocrm0__Synced_Attachments;
                    try{
                        syncedAttachmentsmap = JSON.parse(syncedAttachmentsmap);
                    }
                    catch(e){
                        console.log(e); 
                    }
                }
                return true;
            });      
        }
        catch(e){
            console.log(e);
        }          
        ZOHO.CRM.API.getOrgVariable("googledriveforzohocrm0__settings").then(async function(settings){
            if(settings && settings.Success && settings.Success.Content){
                settingsMap = JSON.parse(settings.Success.Content);
                // if(settingsMap && settingsMap.version == "17" && !settingsMap.ver11_authorized){
                //     swal("Please Revoke and Authorize Google Drive in Extension configure page.", {
                //       buttons: ["Cancel", "Go to configure page"],
                //     }).then(function(resp){
                //         if (resp) {
                //             window.open(serviceOrigin+"/crm/settings/extensions/all/googledriveforzohocrm0?tab=webInteg&subTab=marketPlace&nameSpace=googledriveforzohocrm0&portalName=admin240");
                //             console.log(resp);
                //         }
                //     });        
                //     return ;
                // }
                rootFolderId = settingsMap.rootFolderId;
                currentFolderId = rootFolderId;
            }
            try{
                var ressp = await ZOHO.CRM.CONNECTOR.invokeAPI("googledriveforzohocrm0.googledriveconnector.gethiddenkey",{"zapikey":zapikey});
                console.log(ressp);
                teamDriveAuthorized = true;
            }
            catch(e){
                console.log(e);
                if(e.message == "Authorization Exception"){
                    teamDriveAuthorized = false;
                }
            }
            if(!teamDriveAuthorized){
                try{
                    var ressp = await ZOHO.CRM.CONNECTOR.invokeAPI("googledriveforzohocrm0.googledriveconnector0.getkeyforcomdomain",{"zapikey":zapikey});
                    console.log(ressp);
                    appDriveAuthorized = true;
                }
                catch(e){
                    console.log(e);
                    if(e.message == "Authorization Exception"){
                        appDriveAuthorized = false;
                        swal("Please Authorize Google Drive in Extension configure page.", {
                          buttons: ["Cancel", "Go to configure page"],
                        }).then(function(resp){
                            if (resp) {
                                window.open(serviceOrigin+"/crm/settings/extensions/all/googledriveforzohocrm0?tab=webInteg&subTab=marketPlace&nameSpace=googledriveforzohocrm0&portalName=admin240");
                                console.log(resp);
                            }
                        });        
                        return ;
                    }
                }
            }
            if(rootFolderId) {
                $('.contentBodyOuter').show();
                $("#rootfolderSection").hide();
                // if(folderId){
                //     getFilesByParent(folderId); 
                //     try{
                //         var config={
                //             Entity:recordModule,
                //             APIData:{
                //                 "id": recordId,
                //             },
                //             Trigger:["workflow"]
                //         }
                //         if(currentRecord && !currentRecord[folderApiName]){ 
                //             config["APIData"][folderApiName]= "https://drive.google.com/drive/u/0/folders/"+folderId; 
                //             await ZOHO.CRM.API.updateRecord(config);
                //         }
                //     }
                //     catch(e){
                //         console.log(e);
                //     }    
                // }
                // else{
                    checkModuleFolder();
                // }
            } else {
                $('.contentBodyOuter').hide();
                $("#rootfolderSection").show();
                getRootFolders();
            }
        });
    });
    ZOHO.embeddedApp.init();

});

function invokeConnectorApi(apiName,data){
    if(teamDriveAuthorized){
        return ZOHO.CRM.CONNECTOR.invokeAPI("googledriveforzohocrm0.googledriveconnector."+apiName,data).then(function(resp){
            return resp;
        })
    }
    else{
        var apiMethod="";
        var urlMap = {"url":""};
        urlMap.url = "drive/v3/files/"+data.fileId;
        urlMap.body = data.body?data.body:{};
        if(apiName == "updatefile"){
            apiMethod = "drivepatch";
        }
        else if(apiName == "deletefile"){
            apiMethod = "drivedelete";
        }
        else if(apiName == "getfilebyid"){
            apiMethod = "driveget";
        }
        else if(apiName == "getfiles"){
            apiMethod = "driveget";
            urlMap.url = "drive/v3/files?"+data.queryParams;
        }
        else if(apiName == "createfile"){
            apiMethod = "drivepost";
            urlMap.url = "drive/v3/files?"+data.queryParams;
        }
        return ZOHO.CRM.CONNECTOR.invokeAPI("googledriveforzohocrm0.googledriveconnector0."+apiMethod,urlMap).then(function(resp){
            return resp;
        })
    }
}
function showLoading(isLoading,message){
    if(isLoading){
        $("#loadingAnimationFullView").show();
    }
    else{
         $("#loadingAnimationFullView").hide();
    }
}
var crmAttachments=[];
function syncAttachments(){
    crmAttachments=[];
    showLoading(true);
    ZOHO.CRM.API.getRelatedRecords({Entity:recordModule,RecordID:recordId,RelatedList:"Attachments",page:1,per_page:200})
    .then(async function(data){
        console.log(data);
        if(data && data.data && data.data.length){
            var accessToken = await getAccessToken();
            var attachmentsToSync =0;
            var validcrmAttachments=[]
            data.data.forEach(function(attachment){
                if(!syncedAttachmentsmap[attachment["$file_id"]]){
                    crmAttachments.push(attachment["$file_id"]);
                    validcrmAttachments.push(attachment)
                } 
            });
            filesLength =validcrmAttachments.length;
            if(validcrmAttachments && validcrmAttachments.length){
                validcrmAttachments.forEach(function(attachment){
                    ZOHO.CRM.API.getFile({id:attachment["$file_id"]}).then(function(file){
                        console.log(file);
                        file.name = attachment["File_Name"];
                        uploadFile(file,"",accessToken,attachment["$file_id"]);
                    });
                })
            }
            else{
                swal("No CRM attachments left to Sync.");
                showLoading(false);
            }
        }
        else{
            swal("No CRM attachments found to Sync.");
            showLoading(false);
        }
    })
}
var copiedFileId = null;
function moveFile(fileId){
    copiedFileId = fileId;
    copiedFileParentId=currentFolderId;
}
function pasteFile(fileId){
   $("#pastebtn").hide();    
    showLoading(true);
    var data={"fileId":fileId+"?addParents="+currentFolderId+"&supportsAllDrives=true","body":{}};
    // ZOHO.CRM.CONNECTOR.invokeAPI("googledriveforzohocrm0.googledriveconnector.updatefile",data)
    invokeConnectorApi("updatefile",data)
    .then(function(data){
        copiedFileId = null;
        console.log(data);
        getFilesByParent(currentFolderId);
    })
}
function pasteFileToAnotherFolder(fileId, folderId){
    showLoading(true);
    var data={"fileId":fileId+"?addParents="+folderId+"&supportsAllDrives=true","body":{}};
    $("#"+fileId).css({'opacity': '0.75'});
    // ZOHO.CRM.CONNECTOR.invokeAPI("googledriveforzohocrm0.googledriveconnector.updatefile",data)
    invokeConnectorApi("updatefile",data)
    .then(function(data){
        showLoading(false);
        console.log(data);
        $("#"+fileId).slideUp();
    }).catch(err=>{
        showLoading(false);
        $("#"+fileId).css({'opacity': '1'});
    });
}

//   async function handleauthoriztion(){
//     if(firebase.auth().currentUser){
//         userId = firebase.auth().currentUser.uid;
//         db.collection("ulgebraUsers").doc(userId).collection("extensions").doc(extensionName).get().then(function(doc){
//           if(doc.exists && doc.data().isHubAuth){
//             db.collection("hubspotSettings").doc(userId).get().then(function(settingsRef){
//               if(settingsRef.exists && settingsRef.data().rootFolderIdMap){
//                   rootFolderIdMap= settingsRef.data().rootFolderIdMap;
//               }
//               handleClientLoad();
//             });
//           }
//           else{
//             showErroMessage("Please Authorize the hubspot in <a  href='https://app.azurna.com/hubspot/googledrive/settings' target='_blank'><button class='ext'>Extension Settings</button></a>");
//           }
//         })
//     }    
// }  



function deleteFileAPI(paramMap) {
    showLoading(true);
    var data={"fileId":paramMap.fileId+"?supportsAllDrives=true"};
    // return ZOHO.CRM.CONNECTOR.invokeAPI("googledriveforzohocrm0.googledriveconnector.deletefile",data)
    return invokeConnectorApi("deletefile",data)
    .then(function(data){
        console.log(data);
        showLoading(false);
        return data;
    })
}

function updateFileAPI(paramMap) {
    showLoading(true);
    var data={"fileId":paramMap.fileId+"?supportsAllDrives=true","body":{"name":paramMap.name}};
    // return ZOHO.CRM.CONNECTOR.invokeAPI("googledriveforzohocrm0.googledriveconnector.updatefile",data)
    return invokeConnectorApi("updatefile",data)
    .then(function(data){
        console.log(data);
        showLoading(false);
        return data;
    })
}

function getFileAPI(paramMap) {
    showLoading(true);
    var data={"fileId":paramMap.fileId+"?supportsAllDrives=true&fileds="+paramMap.fields};
    // return ZOHO.CRM.CONNECTOR.invokeAPI("googledriveforzohocrm0.googledriveconnector.getfilebyid",data)
    return invokeConnectorApi("getfilebyid",data)
    .then(function(data){
        console.log(data);
        showLoading(false);
        if(data.status_code == 200){
            return JSON.parse(data.response);
        }
        return null;
    });
}
//queryParams: "pageSize=10&fields=nextPageToken,files(id,name,parents)&q=name%3D'Leads'%20and%20'1YdvUax6ji8c0fW5QdxzVk1EQF9A07mkZ'%20in%20parents"
//queryParams: "pageSize=10&fields=nextPageToken,files(id,name,parents)&q=name%20%3D'4490020000000065078'and'1P0fYzZl2NAAARCG0NrO1ety3RWqoFtZ7'in%20parents"
function getFilesListAPI(paramMap) {
    showLoading(true);
    var queryParams = getQueryParam(paramMap);
    var data={"queryParams":queryParams+"&includeItemsFromAllDrives=true&supportsAllDrives=true"};
    // return ZOHO.CRM.CONNECTOR.invokeAPI("googledriveforzohocrm0.googledriveconnector.getfiles",data)
    return invokeConnectorApi("getfiles",data)
    .then(function(data){
        console.log(data);
        showLoading(false);
        if(data.status_code == 200){
            try{
                var files = JSON.parse(data.response).files;
                if (files && files.length > 0) {
                    for (var i = 0; i < files.length; i++) {
                        filesListMap[files[i].id] = files[i];
                    }
                }
            }
            catch(e){
                console.log(e);
            }
            return JSON.parse(data.response);
        }
        return null;
    })
}
function getQueryParam(paramMap){
    var queryString='';
    Object.keys(paramMap).forEach(function(param){
        queryString=queryString?(queryString+"&"+param+"="+paramMap[param]):(param+"="+paramMap[param]);
    })
    return queryString;
}

function createFolderAPI(paramMap) {
    showLoading(true);
    var data={"body":paramMap.resource,"queryParams":"supportsAllDrives=true"};
    // return ZOHO.CRM.CONNECTOR.invokeAPI("googledriveforzohocrm0.googledriveconnector.createfile",data)
    return invokeConnectorApi("createfile",data)
    .then(function(data){
        console.log(data);
        showLoading(false);
        if(data.status_code == 200){
            return JSON.parse(data.response);
        }
        try{
            var error = JSON.parse(data.response).error.errors[0].message;
            if(error.indexOf("File not found: "+rootFolderId) != -1){
                resetRootFolder();
            }
        }
        catch(e){
            console.log(e);
        }    
        return null;
    })
}
async function resetRootFolder(){
    settingsMap["rootFolderId"]="";
    await ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "googledriveforzohocrm0__settings","value":settingsMap});
    window.location.reload();
}
// function uploadFileAPI(paramMap) {
//     var data={"body":paramMap.file,"queryParams":paramMap.queryParams+"&supportsAllDrives=true"};
//     return ZOHO.CRM.CONNECTOR.invokeAPI("googledriveforzohocrm0.googledriveconnector.uploadfile",data)
//     .then(function(data){
//         console.log(data);
//         if(data.status_code == 200){
//             return JSON.parse(data.response);
//         }
//         return null;
//     })
// }
function getAccessToken(){
    var data={"zapikey":zapikey};
    var funName = "googledriveforzohocrm0.googledriveconnector.gethiddenkey";
    var connectorName ="googledriveconnector";
    if(!teamDriveAuthorized){
        funName = "googledriveforzohocrm0.googledriveconnector0.getkeyforcomdomain";
        connectorName ="googledriveconnector0";
    }
    if(serviceOrigin.indexOf("zoho.com.au") != -1){
        funName = "googledriveforzohocrm0."+connectorName+".getkeyforaudomain";
    }
    else if(serviceOrigin.indexOf("zoho.com.cn") != -1){
        funName = "googledriveforzohocrm0."+connectorName+".getkeyforcndomain";
    }
    else if(serviceOrigin.indexOf("zoho.eu") != -1){
        funName = "googledriveforzohocrm0."+connectorName+".getkeyforeudomain";
    }
    else if(serviceOrigin.indexOf("zoho.in") != -1){
        funName = "googledriveforzohocrm0."+connectorName+".getkeyforindomain";
    }

    try{
        if(!teamDriveAuthorized) {
            funName = "googledriveforzohocrm0.googledriveconnector0.get_token_com";
        }
        else {
            funName = "googledriveforzohocrm0.googledriveconnector.get_token_com";
        }
    }
    catch(err){
        console.log(err);
    }

    return ZOHO.CRM.CONNECTOR.invokeAPI(funName,data)
    .then(function(data){
        console.log(data);
        if(data.status_code == 200){
            return JSON.parse(data.response).accessToken;
        }
        return null;
    })
}


async function upload() {
    showLoading(true);
    var accessToken = await getAccessToken();
    if (accessToken && uploadFiles && uploadFiles.length) {
        filesLength = uploadFiles.length;
        uploadFiles.forEach(function (file) {
            uploadFile(file,"",accessToken);
        })
    } else {
        showLoading(false);
        swal ("Please select file to upload")
    }
}

function deleteFile(fileId) {
    var text ="";
    if(openedFolderFilesArr[fileId]){
        text = 'Delete  "' + openedFolderFilesArr[fileId].name + '"  file from your Drive?'
    }
    else if(openedFoldersArr[fileId]){
        text = 'Delete  "' + openedFoldersArr[fileId].name + '"  folder from your Drive?'
    }
    else{
        return;
    }
    swal(text, {
      buttons: ["Oh no!", true],
    }).then(function(resp){
        if (resp) {
            deleteFileAPI({
                "fileId": fileId
            }).then(function (response) {
                console.log(response);
                $("#" + fileId).remove();
            }).catch(function (e) {
                console.log(e);
            })
        }
    });    

}

function editNameOuterClickCheck(e) {
    if ($(e.target).hasClass('name-edit') || $(e.target).hasClass('name-save') || $(e.target).hasClass('hit-editDiv') || $(e.target).hasClass('editFileButt')) {

    } else {
        $('.singleFileStyle').each(function () {
            $(this).find('.name_uploadedFile').show();
            $(this).find('.editFieldOpen').hide();
            $(this).find('.editFileButt').parent().show();
        });
        //$('.fileOpenedInnerDiv').hide();
    }

    if ($(e.target).hasClass('fileOpenedInnerDiv') || $(e.target).hasClass('fileOpenedInnerDivAudio') || $(e.target).hasClass('nameOfOpenedFile') || $(e.target).parent().hasClass('nameOfOpenedFile') || $(e.target).hasClass('closeOfOpenedFile') || $(e.target).parent().hasClass('closeOfOpenedFile') || $(e.target).hasClass('selectedOPenedFile') || $(e.target).hasClass('documenOpenedFile') || $(e.target).hasClass('driveFileDocImg') || $(e.target).parent().hasClass('driveFileDocImg') || $(e.target).hasClass('driveFileDocName') || $(e.target).hasClass('driveFileDocLink') || $(e.target).parent().hasClass('driveFileDocLink') || $(e.target).hasClass('draiveFileDocType') || $(e.target).hasClass('detailsOfOpenedFile') || $(e.target).hasClass('hintOfOpenedFile') || $(e.target).hasClass('downloadOpenFile') || $(e.target).parent().hasClass('downloadOpenFile') || $(e.target).parent().hasClass('selectedOPenedFile') || $(e.target).hasClass('name_uploadedFile') || $(e.target).hasClass('uploadedFileType') || $(e.target).parent().hasClass('uploadedFileType') || $(e.target).hasClass('updateFieldStyle') || $(e.target).hasClass('uploadedFileSize') || $(e.target).hasClass('editFileButtDiv') || $(e.target).hasClass('deleteFileButtDiv') || $(e.target).hasClass('uploadedFileLinkSub') || $(e.target).hasClass('singleFileStyle')) {

    } else {
        $('.fileOpenedInnerDiv').hide();
        $('.selectedOPenedFile').html('');
    }
}

function editFile(fileId) {
    $('.singleFileStyle').each(function () {
        if ($(this).attr('id') != fileId) {
            $(this).find('.name_uploadedFile').show();
            $(this).find('.editFieldOpen').hide();
            $(this).find('.editFileButt').parent().show();
        }
    })
    $('#name_' + fileId).hide();
    $("#editfield_" + fileId).css({
        'display': 'flex'
    });
    $("#editName_" + fileId).show();
    $("#editName_" + fileId).val($("#name_" + fileId).text()).focus();
    $("#save_" + fileId).show();
    $("#edit_" + fileId).hide();
}

function updateFile(fileId) {
    var name = $("#editName_" + fileId).val();
    updateFileAPI({
        "fileId": fileId,
        "name": name
    }).then(function (response) {
        console.log(response);
        $("#editfield_" + fileId).css({
            'display': 'none'
        });
        $("#editName_" + fileId).hide();
        $("#save_" + fileId).hide();
        $("#edit_" + fileId).show();
        $("#name_" + fileId).text(name);
        $('#name_' + fileId).show();
    }).catch(function (e) {
        console.log(e);
    })


}

function exiteditFile(fileId) {
    $("#editfield_" + fileId).css({
        'display': 'none'
    });
    $("#editName_" + fileId).hide();
    $("#save_" + fileId).hide();
    $("#edit_" + fileId).show();
    $('#name_' + fileId).show();
}

function uploadFile(file, parentId,accessToken,crmFileId) {
    var metadata = {
        'name': file.name, // Filename at Google Drive
        'mimeType': file.type, // mimeType at Google Drive
        'parents': [currentFolderId]
    };
    if (parentId) {
        metadata['parents'] = [parentId];
    }
    if(crmFileId){
        metadata["appProperties"]={"zohocrmId":crmFileId};
    }
    // var accessToken = gapi.auth.getToken().access_token; // Here gapi is used for retrieving the access token.
    var form = new FormData();
    form.append('metadata', new Blob([JSON.stringify(metadata)], {
        type: 'application/json'
    }));
    form.append('file',file);
    // //uploadFileAPI({"file":form,"queryParams":"uploadType=multipart&fields=id,name,thumbnailLink,size,parents"}).then(function(val){
    fetch('https://www.googleapis.com/upload/drive/v3/files?supportsAllDrives=true&uploadType=multipart&fields=id,name,thumbnailLink,size,parents', {
        method: 'POST',
        headers: new Headers({ 'Authorization': 'Bearer ' + accessToken }),
        body: form,
    }).then((res) => {
        return res.json();
    }).then(function(val) {
        console.log(val);
        filesLength--;
        if(filesLength == 0){
          showLoading(false);
          $("#uploadfile").html(`<h3>Drag and Drop file here<br/>Or<br/>Click to select file</h3>`);
          uploadFiles=[];
          $('.uploadButtonDiv').hide();
          getFilesByParent(currentFolderId);
        }
    });
}

function getFileById(fileId) {
    getFileAPI({
        'fileId': fileId,
        'fields': "id,name,trashed",
    }).then(function (response) {
        console.log(response);
    }).catch(function (e) {
        console.log(e);
    })
}

var parentsList = [];
var filesListMap={};
function handleFolderPath(parentId) {
    if(copiedFileId && copiedFileParentId != parentId){
        $("#pastebtn").show();
    }
    else{
        $("#pastebtn").hide();
    }
    var pathIndex = parentsList.indexOf(parentId);
    if (pathIndex == -1) {
        // if($('#folderPathHolder').children().last().attr('id') == parentId) {
        //   return false;
        // }
        parentsList.push(parentId);
        if (parentId == parentsList[0]) {
            $('#folderPathHolder').html('');
            var filesUi = `<span id="${parentId}" onClick="getFilesByParent('${parentId}')" class="homeFolderPathDiv"><span class="material-icons homeFolderPath">house</span>Home </span>`;
            $('#folderPathHolder').append(filesUi);
        } else{
            var filesUi = `<span class="splitFolderPath">/</span><span id="${parentId}" onClick="getFilesByParent('${parentId}')"></span>`;
            $('#folderPathHolder').append(filesUi);
            document.getElementById(parentId).innerText = filesListMap[parentId].name;
        }    
    } else {
        parentsList.splice(pathIndex + 1);
        $('#folderPathHolder').find("#" + parentId).nextAll().remove();
    }
}

function getFileSize(fileSize) {
    fileSize = fileSize / (1024 * 1024);
    if (fileSize < 1) {
        fileSize = fileSize * 1024;
        return fileSize.toFixed(0) + ' KB';
    } else if (fileSize >= 0)
        return fileSize.toFixed(0) + ' MB';
    else
        return 'Folder';
}

function showFile(fileId, event, selected) {

    if ($(event.target).hasClass('name-edit') || $(event.target).hasClass('name-save') || $(event.target).hasClass('hit-editDiv') || $(event.target).hasClass('editFileButt') || $(event.target).hasClass('uploadedFileLinkSubSpan') || $(event.target).hasClass('deleteFileButt')) {
        return false;
    } else {
        if ($('.name-edit').is(':visible'))
            return false;
    }

    $('.fileOpenedInnerDiv').removeClass('fileOpenedInnerDivAudio');
    $(".selectedOPenedFile").find('.loadingAnimation').remove();
    $(".selectedOPenedFile").append(loaderDiv);
    let left = event.pageX + 350 > $('#content').width() ? $('#content').width() - 350 : event.pageX - 15;
    let top = event.pageY < 220 ? $(selected).position().top + 20 : $(selected).position().top - 150;
    if (event.offsetY < 10)
        top = top + event.offsetY - 12;
    else if (event.offsetY < 30)
        top = top + event.offsetY - 10;
    else
        top = top + event.offsetY - 20;
    $('.fileOpenedInnerDiv').css({
        position: "absolute",
        top: top,
        left: left,
        display: 'flex'
    });

    let fileName = openedFolderFilesArr[fileId].name;
    let fileType = openedFolderFilesArr[fileId].mimeType.split('image/');
    fileType = fileType[fileType.length - 1];
    let fileView = openedFolderFilesArr[fileId].webContentLink.split('&export=download')[0];

    if ($('#' + fileId).attr('data-type') == 'image') {
        $('.selectedOPenedFile').html(`<img src="${fileView}" style="max-height:100%;max-width:80%;">`).css({
            'z-index': '5'
        });
        $('.downloadOpenFile span').attr('onclick', `download_file('${fileView}','${fileId}')`);
        $('.nameOfOpenedFile span').text(fileName);
        $('.hintOfOpenedFile').text(fileType);
    } else if ($('#' + fileId).attr('data-type') == 'videocam') {
        $('.selectedOPenedFile').html(`<video src="${fileView}" style="height:100%;outline:0;" controls autoplay>`).css({
            'z-index': '15'
        });
        $('.downloadOpenFile span').attr('onclick', `download_file('${fileView}','${fileId}')`);
        $('.nameOfOpenedFile span').text(fileName);
        $('.hintOfOpenedFile').text(fileType);
    } else if ($('#' + fileId).attr('data-type') == 'audiotrack') {
        top = event.pageY < 130 ? $(selected).position().top + 20 : $(selected).position().top - 55;
        if (event.offsetY < 10)
            top = top + event.offsetY - 7;
        else if (event.offsetY < 30)
            top = top + event.offsetY - 10;
        else
            top = top + event.offsetY - 20;
        $('.fileOpenedInnerDiv').addClass('fileOpenedInnerDivAudio').css({
            top: top
        });
        $('.selectedOPenedFile').html(`<audio src="${fileView}" style="height:100%;outline:0;width: 100%;" controls autoplay>`).css({
            'z-index': '15'
        });
        $('.downloadOpenFile span').attr('onclick', `download_file('${fileView}','${fileId}')`);
        $('.nameOfOpenedFile span').text(fileName);
        $('.hintOfOpenedFile').text(fileType);
    } else if ($('#' + fileId).attr('data-type') == 'picture_as_pdf') {
        $('.selectedOPenedFile').html(`<iframe src="${fileView}#toolbar=0&navpanes=0&scrollbar=0" type="application/pdf"
                  frameBorder="0"
                  scrolling="auto"
                  height="100%"
                  width="100%" style="
                  position: absolute;
                  left: -8px;
                  top: -2px;
                  width: calc(100% + 30px);
                  height: calc(200% + 4px);
              "></iframe>`).css({
            'z-index': '11'
        });
        $('.downloadOpenFile span').attr('onclick', `download_file('${fileView}','${fileId}')`);
        $('.nameOfOpenedFile span').text(fileName);
        $('.hintOfOpenedFile').text('pdf');
    } else {
        top = event.pageY < 130 ? $(selected).position().top + 20 : $(selected).position().top - 55;
        if (event.offsetY < 10)
            top = top + event.offsetY - 15;
        else if (event.offsetY < 30)
            top = top + event.offsetY - 5;
        else
            top = top + event.offsetY - 20;
        $('.fileOpenedInnerDiv').addClass('fileOpenedInnerDivAudio').css({
            top: top
        });
        if (fileType.includes('zip'))
            fileType = 'zip';
        else if (fileType.includes('sheet'))
            fileType = 'sheet';
        else if (fileType.includes('application'))
            fileType = 'exe';
        $('.selectedOPenedFile').html(`<div class="documenOpenedFile">
                          <div class="driveFileDocImg"><span class="material-icons ">
                      description
                              </span></div><div class="driveFileDocName" id="driveFileDocName_${fileId}"></div>
                          <div class="driveFileDocLink">
                      <span class="material-icons" onclick="download_file('${fileView}','${fileId}')">
                      download
                      </span>
                          </div>
                      </div><div class="draiveFileDocType">${fileType}</div>`).css({
            'z-index': '17'
        });
        $('#driveFileDocName_'+fileId).text(fileName);
        $('.downloadOpenFile span').attr('onclick', `download_file('${fileView}','${fileId}')`);
        $('.nameOfOpenedFile span').text(fileName);
        $('.hintOfOpenedFile').text(fileType);
    }
    console.log(fileId);
}

function getFileType(mimeType) {
    if (mimeType.includes('video'))
        return 'videocam';
    else if (mimeType.includes('msdownload'))
        return 'shield';
    else if (mimeType.includes('image'))
        return 'image';
    else if (mimeType.includes('pdf'))
        return 'picture_as_pdf';
    else if (mimeType.includes("openxmlformats") || mimeType.includes("officedocumentt") || mimeType.includes(".spreadsheetml") || mimeType.includes("sheet") || mimeType.includes("xlsx"))
        return 'grid_on';
    else if (mimeType.includes('zip'))
        return 'snippet_folder';
    else if (mimeType.includes('audio'))
        return 'audiotrack';
    else
        return 'description';
}

var openedFolderFilesArr;
var openedFoldersArr={};

var scrollNextPageToken;

function getRootFolders() {
    getFilesListAPI({
        'pageSize':100,
        'fields':"nextPageToken,files(id,name,trashed,mimeType,parents,size,trashingUser,iconLink,owners,sharingUser,contentHints,thumbnailLink,webViewLink,webContentLink,appProperties)",
        'q':encodeURIComponent("mimeType='application/vnd.google-apps.folder' and trashed != true")
    }).then(function (response) {
        console.log(response);
        var folders = response.files;
        var foldersUi = `<option value data-fName="${'Select Folder'}" id="folder_select">Select Folder</option>`;
        folders.forEach(function (folder) {
            foldersUi = `${foldersUi}<option value="${folder.id}" data-fName="${folder.name}" id="folder_${folder.id}">${folder.name}</option>`;
        });
        $("#rootFolderId").append(foldersUi);
    }).catch(function (e) {
        console.log(e);
    })
}
async function selectRootFolder() {

    if (!$("#rootFolderId").val()) {
        $('.contentBodyOuter').hide();
        $('.selectedRootFolderName').text($("#rootFolderId").find(':selected').attr('data-fName'));
        rootFolderId = null;
        currentFolderId = null;
        return false;
    }
    // $('.contentBodyOuter').show();
    rootFolderId = $("#rootFolderId").val();
    $('.selectedRootFolderName').text($("#rootFolderId").find(':selected').attr('data-fName'));
    // currentFolderId = rootFolderId;
    // contactId = $("#rootFolderId").find(':selected').attr('data-fName');
    settingsMap["rootFolderId"]=rootFolderId;
    await ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "googledriveforzohocrm0__settings","value":settingsMap});
    window.location.reload();
}

function addRootFolder(name) {

    if (name.trim() == '') {
        $('#newrootfolder').focus();

        swal("Please enter valid folder name")
        return false;
    }
    var fileMetadata = {'name':name,'mimeType':'application/vnd.google-apps.folder'};
    createFolderAPI({
        resource: fileMetadata,
    }).then(async function (response) {
        if(response){
            console.log('Created Folder Id: ', response.id);
            rootFolderId = response.id;
            settingsMap["rootFolderId"]=response.id;
            await ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "googledriveforzohocrm0__settings","value":settingsMap});
            window.location.reload();
        }
        else{        
            console.log('Error creating the folder, ' + response);
        }
    });
    $('#newfolder').val('');
}
var openedFolderFilesArrUpload = {};

function getFilesByParentUpload(parentId) {

    openedFolderFilesArrUpload = {};
    currentFolderId = parentId;
    var q = encodeURIComponent("'" + currentFolderId + "' in parents and trashed != true");
    getFilesListAPI({
        'pageSize':10,
        'fields':"nextPageToken,files(id,name,trashed,mimeType,parents,size,trashingUser,iconLink,owners,sharingUser,contentHints,thumbnailLink,webViewLink,webContentLink)",
        'q':q
    }).then(function (response) {
        var files = response.files;
        if (files && files.length > 0) {

            for (var i = 0; i < files.length; i++) {
                openedFolderFilesArrUpload[files[i].id] = files[i];
            }

        } else {

        }
    }).catch(function (e) {
        console.log(e);
        if (e && e.result && e.result.error && e.result.error.message && e.result.error.message.indexOf("File not found") != -1) {}
    })
}


function getFilesByParent(parentId) {

    // if($('#folderPathHolder').children().last().attr('id') == parentId) {
    //   return false;
    // }
    openedFolderFilesArr = {};
    handleFolderPath(parentId);
    currentFolderId = parentId;
    var q = encodeURIComponent("'"+currentFolderId+"' in parents and trashed != true");
    getFilesListAPI({
        'pageSize':10,
        'fields': "nextPageToken,files(id,name,trashed,mimeType,parents,size,trashingUser,iconLink,owners,sharingUser,contentHints,thumbnailLink,webViewLink,webContentLink,appProperties)",
        'q':q
    }).then(function (response) {
        appendPre('Files:');
        var files = response.files;
        scrollNextPageToken = response.nextPageToken;
        if(files && files.length > 0) {
            var filesUi = `
            <div class="fileOpenedInnerDiv" style="display: none;">
                <div class="nameOfOpenedFile"><span>extra-wc.svg</span></div>
                <div class="closeOfOpenedFile"><span class="material-icons" onclick="$('.selectedOPenedFile').html('');$('.fileOpenedInnerDiv').hide();">close</span></div>
                <div class="selectedOPenedFile"></div>
                <div class="detailsOfOpenedFile">
                    <div class="hintOfOpenedFile">PNG</div>
                </div>
                <div class="downloadOpenFile"><span class="material-icons">save_alt</span></div>
            </div>
            <div class="singleFileStyleHead">
                <div class="singleFileStyleHeadSub singleFileStyleTypeSub">Type</div>
                <div class="singleFileStyleHeadSub singleFileStyleNameSub">Name</div>
                <div class="singleFileStyleHeadSub singleFileStyleSizeSub">Size</div>
                <div class="singleFileStyleHeadSub singleFileStyleEditSub">Edit</div>
                <div class="singleFileStyleHeadSub singleFileStyleRemoveSub">Delete</div>
            </div><div class="contentInnerMain" onscroll="getFilesByParentNext('${parentId}')">`;
            for (var i = 0; i < files.length; i++) {
                if(crmAttachments && crmAttachments.length && files[i] && files[i].appProperties && files[i].appProperties.zohocrmId && crmAttachments.indexOf(files[i].appProperties.zohocrmId) != -1){
                    syncedAttachmentsmap[files[i].appProperties.zohocrmId] = files[i].id;
                }
                filesUi = `${filesUi}${getFilesByParentList(files[i])}`;
            }
            if(crmAttachments && crmAttachments.length){
                try{
                    crmAttachments=[];
                    if(attachmentRecordId){
                        ZOHO.CRM.API.updateRecord({Entity:"googledriveforzohocrm0__Attachments_Sync_Settings",APIData:{"id":attachmentRecordId,"Name":recordModule,"googledriveforzohocrm0__RecordId":recordId,"googledriveforzohocrm0__Synced_Attachments":syncedAttachmentsmap},Trigger:["workflow"]})
                    }
                    else{
                        ZOHO.CRM.API.insertRecord({Entity:"googledriveforzohocrm0__Attachments_Sync_Settings",APIData:{"Name":recordModule,"googledriveforzohocrm0__RecordId":recordId,"googledriveforzohocrm0__Synced_Attachments":syncedAttachmentsmap},Trigger:["workflow"]})
                    }
                }
                catch(e){
                    console.log(e);
                }
            }
            var pre = document.getElementById('content');
            pre.innerHTML = filesUi + '</div>';
            for (var j = 0; j < files.length; j++) {
                document.getElementById("name_"+files[j].id).innerText =files[j].name;
            }
        } else {
            var pre = document.getElementById('content');
            pre.innerHTML = "No files found";
        }
    }).catch(function (e) {
        console.log(e);
        if (e && e.result && e.result.error && e.result.error.message && e.result.error.message.indexOf("File not found") != -1) {}
    })
}

async function getFilesByParentNext(parentId) {

    if ($('.contentInnerMain').scrollTop() != 0 && $('.contentInnerMain')[0].scrollHeight - $('.contentInnerMain').scrollTop() >= $('.contentInnerMain').outerHeight()) {
        $('.fileOpenedInnerDiv').hide();
        $('.selectedOPenedFile').html('');
    }

    if (scrollNextPageToken)
        if ($('.contentInnerMain').outerHeight() + $('.contentInnerMain').scrollTop() >= $('.contentInnerMain')[0].scrollHeight - 2) {
            $(".contentInnerMain").attr('onscroll', 'null');
            $(".contentInnerMain").append(loaderDiv);
            currentFolderId = parentId;
            var q = encodeURIComponent("'"+currentFolderId+"' in parents and trashed != true");
            await getFilesListAPI({
                'pageToken':scrollNextPageToken,
                'pageSize':10,
                'fields':"nextPageToken,files(id,name,trashed,mimeType,parents,size,trashingUser,iconLink,owners,sharingUser,contentHints,thumbnailLink,webViewLink,webContentLink)",
                'q':q
            }).then(function (response) {
                $(".contentInnerMain").find('.loadingAnimation').remove();
                var files = response.files;
                scrollNextPageToken = response.nextPageToken;
                if (files && files.length > 0) {
                    for (var i = 0; i < files.length; i++) {
                        $('.contentInnerMain').append(getFilesByParentList(files[i]));
                    }
                    $(".contentInnerMain").attr('onscroll', `getFilesByParentNext('${parentId}'`);
                }
            }).catch(function (e) {
                console.log(e);
                if (e && e.result && e.result.error && e.result.error.message && e.result.error.message.indexOf("File not found") != -1) {}
            })
        }
}

function moveDroppedFile(ev, folderId){
    ev.preventDefault();
    $('#'+folderId).css({'background-color': 'white', 'color': 'black'});
    var fileId = ev.dataTransfer.getData("fileId");
    pasteFileToAnotherFolder(fileId, folderId);
}

function setDroppedFileToMove(ev, fileId){
      ev.dataTransfer.setData("fileId", fileId);
}

function allowDrop(ev, fileId) {
  ev.preventDefault();
  $('#'+fileId).css({'background-color': 'grey', 'color':'white'});
}

function dragLeft(ev, fileId) {
  ev.preventDefault();
  $('#'+fileId).css({'background-color': 'white', 'color': 'black'});
}

function getFilesByParentList(file) {

    //var file = files[i];
    let filesUi;
    if (file.mimeType == 'application/vnd.google-apps.folder') {
        openedFoldersArr[file.id] = file;
        filesUi = `<div id="${file.id}" class="folderItemDiv singleFileStyle" ondragleave="dragLeft(event, '${file.id}')" ondragover="allowDrop(event, '${file.id}')"  ondrop="moveDroppedFile(event, '${file.id}')" onClick="getFilesByParentCheck('${file.id}',event)">
                <div class="uploadedFileType"><span class="material-icons uploadedFileTypeSpan">folder</span></div>
                <div id="name_${file.id}" class="name_uploadedFile"></div>
                <div id="editfield_${file.id}" class="editFieldOpen">
                  <input id="editName_${file.id}" type="text" class="name-edit" onkeypress="return (event.which==13 || event.keyCode==13) ? $(this).parent().find('.name-save').click() : true;"></input>
                  <button class="name-save" onclick="updateFile('${file.id}')" id="save_${file.id}">✓</button>
                  <button class="hit-editDiv" onclick="exiteditFile('${file.id}')">×</button>
                </div>                
                <div class="updateFieldStyle">
                  <div class="uploadedFileSize">Folder</div>
                  <div class="uploadedFileLinkSub"><span class="material-icons">folder_open</span></div>
                  
                  <div onClick="editFile('${file.id}')" id="edit_${file.id}" class="editFileButtDiv"><span class="material-icons editFileButt">edit</span></div>
                  <div onClick="deleteFile('${file.id}')" id="delete_${file.id}" class="deleteFileButtDiv"><span class="material-icons deleteFileButt">delete</span></div>
                </div>
                </div>`;
    } else {
        openedFolderFilesArr[file.id] = file;
        filesUi = `<div id="${file.id}" draggable="true" ondragstart="setDroppedFileToMove(event, '${file.id}')" class="fileItemDiv singleFileStyle" data-type="${getFileType(file.mimeType)}">
                <div class="uploadedFileType"><button onClick="selectFileForBulkAction('${file.id}')" class="fim-checkbox"></button><span class="material-icons uploadedFileTypeSpan">${getFileType(file.mimeType)}</span></div>
                <div id="name_${file.id}" onClick="showFile('${file.id}', event, this)" data-type="${getFileType(file.mimeType)}" class="name_uploadedFile"></div> 
                <div id="editfield_${file.id}" class="editFieldOpen">
                  <input id="editName_${file.id}" type="text" class="name-edit" onkeypress="return (event.which==13 || event.keyCode==13) ? $(this).parent().find('.name-save').click() : true;"></input>
                  <button class="name-save" onclick="updateFile('${file.id}')" id="save_${file.id}">✓</button>
                  <button class="hit-editDiv" onclick="exiteditFile('${file.id}')">×</button>
                </div>                
                <div class="updateFieldStyle">
                  <div class="uploadedFileSize">${getFileSize(file.size)}</div>
                  <div class="uploadedFileLinkSub"><span class="material-icons uploadedFileLinkSubSpan" onclick="download_file('${file.webContentLink}','${file.id}')">download</span></div>
                  
                  <div onClick="editFile('${file.id}')" id="edit_${file.id}" class="editFileButtDiv"><span class="material-icons editFileButt">edit</span></div>
                  <div onClick="deleteFile('${file.id}')" id="delete_${file.id}" class="deleteFileButtDiv"><span class="material-icons deleteFileButt">delete</span></div>
              </div>
                </div>`;
    }

    return filesUi;

}

var bulkActionFileIds = [];

function selectFileForBulkAction(fileId){
    if(!bulkActionFileIds.includes(fileId)){
        bulkActionFileIds.push(fileId);
        $("#"+fileId).addClass('fileIsInClipBoard');
    }
    else{
        bulkActionFileIds.splice(bulkActionFileIds.indexOf(fileId), 1);
        $("#"+fileId).removeClass('fileIsInClipBoard');
    }
    refreshBulkActionClipBoardUI();
}

function refreshBulkActionClipBoardUI(){
    $("#bulkActionClipBoardFilesCount").text(bulkActionFileIds.length);
    if(bulkActionFileIds.length >0 ){
        $("#bulkActionClipBoard").show();
    }else{
        $("#bulkActionClipBoard").hide();
    }
}

function doClipBoardAction(actionType){
    if(actionType === "delete"){
        bulkActionFileIds.forEach(item=>{
            deleteFile(item);
        });
    }
    if(actionType === "move"){
        $('#bact-move').hide();
        $('#bact-paste').show();
        return;
    }
    if(actionType === "paste"){
        bulkActionFileIds.forEach(item=>{
            pasteFile(item);
        });
    }
    bulkActionFileIds = [];
    $('.fileIsInClipBoard').removeClass('fileIsInClipBoard');
    refreshBulkActionClipBoardUI();
    $('#bact-move').show();
    $('#bact-paste').hide();
}


function download_file(fileURL,fileId) {
    // for non-IE
    if (!window.ActiveXObject) {
        var save = document.createElement('a');
        save.href = fileURL;
        save.target = '_blank';
        var filename = fileURL.substring(fileURL.lastIndexOf('/') + 1);
        save.download = filesListMap[fileId].name || filename;
        if (navigator.userAgent.toLowerCase().match(/(ipad|iphone|safari)/) && navigator.userAgent.search("Chrome") < 0) {
            document.location = save.href;
            // window event not working here
        } else {
            var evt = new MouseEvent('click', {
                'view': window,
                'bubbles': true,
                'cancelable': false
            });
            save.dispatchEvent(evt);
            (window.URL || window.webkitURL).revokeObjectURL(save.href);
        }
    }

    // for IE < 11
    else if (!!window.ActiveXObject && document.execCommand) {
        var _window = window.open(fileURL, '_blank');
        _window.document.close();
        _window.document.execCommand('SaveAs', true, filesListMap[fileId].name || fileURL)
        _window.close();
    }
}

function getFilesByParentCheck(parentId, e) {
    if ($(e.target).hasClass('name-edit') || $(e.target).hasClass('name-save') || $(e.target).hasClass('hit-editDiv') || $(e.target).hasClass('editFileButt') || $(e.target).hasClass('uploadedFileLinkSubSpan') || $(e.target).hasClass('deleteFileButt')) {
        return false;
    } else {
        if ($('.name-edit').is(':visible'))
            return false;
    }
    getFilesByParent(parentId);
}
function checkModuleFolder(){
    if (!recordModule) {
        return;
    }
    var q = encodeURIComponent("name='"+recordModule+"' and '"+rootFolderId+"' in parents and mimeType='application/vnd.google-apps.folder' and trashed != true");
    getFilesListAPI({
        'pageSize':10,
        'fields':"nextPageToken,files(id,name,parents,trashed)",
        'q':q
    }).then(function (response) {
        $('#content').html("<div class='noFilesFound'>Fetching files...</div>");
        var files = response.files;
        if (files && files.length > 0) {
            getContactFolder(files[0].id)
        } else {
            createFolder(recordModule,true);
            appendPre("No files found");
        }
    }).catch(function (e) {
        console.log(e);
        if (e && e.result && e.result.error && e.result.error.message && e.result.error.message.indexOf("File not found") != -1) {
            createFolder(recordModule,true);
        }
    })
}

async function getContactFolder(parentId) {
    if (!recordName) {
        return;
    } 
    var q = encodeURIComponent("name contains '"+recordName+"' and '"+parentId+"' in parents and trashed != true");
    getFilesListAPI({
        'pageSize':10,
        'fields':"nextPageToken,files(id,name,parents,trashed)",
        'q':q
    }).then(async function (response) {
        $('#content').html("<div class='noFilesFound'>Fetching files...</div>");
        if(response){
            var files = response.files;
            if (files && files.length > 0) {
                try{
                    var config={
                        Entity:recordModule,
                        APIData:{
                            "id": recordId,
                        },
                        Trigger:["workflow"]
                    }
                    if(!folderId || (folderId != files[0].id)){
                        folderId = files[0].id;
                        config["APIData"][folderIdApiName]= files[0].id;
                    }
                    if(currentRecord && (!currentRecord[folderApiName] || (currentRecord[folderApiName] != "https://drive.google.com/drive/u/0/folders/"+files[0].id))){ 
                        config["APIData"][folderApiName]= "https://drive.google.com/drive/u/0/folders/"+files[0].id;  
                    }
                    if(config["APIData"][folderIdApiName] || config["APIData"][folderApiName]){
                       await ZOHO.CRM.API.updateRecord(config);
                    }

                }
                catch(e){
                    console.log(e);
                }    
                getFilesByParent(files[0].id, files[0].name);
            } else {
                currentFolderId = parentId;
                createFolder(recordName,false,true);
                appendPre("No files found");
            }
        }    
    }).catch(function (e) {
        console.log(e);
        if (e && e.result && e.result.error && e.result.error.message && e.result.error.message.indexOf("File not found") != -1) {
            createFolder(recordName);
        }
    })
}

function createFolder(name,isModule,isRecord) {
    //var parentId = '0AA6q8Vv3d_A1Uk9PVA';//some parentId of a folder under which to create the new folder
    //let folderName = $('#newfolder').val();
    if (name.trim() == '') {
        $('#newfolder').focus();
        swal("Please Enter Valid folder name")
        return false;
    }
    var fileMetadata = {'name':name,'mimeType':'application/vnd.google-apps.folder','parents':[currentFolderId]};
    createFolderAPI({
        resource: fileMetadata,
    }).then(function (response) {
        if(response){
            var file = response;
            if(isModule){
                currentFolderId = file.id;
                getContactFolder(file.id);
            }
            else if(isRecord){
                currentFolderId = file.id;
                getFilesByParent(file.id,recordId);
            }
            else{
                getFilesByParent(currentFolderId);
            }
            console.log('Created Folder Id: ', file.id);
        }
        else{    
            console.log('Error creating the folder, ' + response);
        }
    });
    $('#addnewfolderdiv').hide();
    $('#newfolder').val('');
}


// Client ID and API key from the Developer Console
// var CLIENT_ID = '832636563044-kt8tl5vac2dds38tdl5j65gjlh57tpri.apps.googleusercontent.com';
// var API_KEY = 'AIzaSyDiuXgRN-SgkYB802x3d011C9N0SB-wtAY';

// // Array of API discovery doc URLs for APIs used by the quickstart
// var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"];

// // Authorization scopes required by the API; multiple scopes can be
// // included, separated by spaces.
// var SCOPES = 'https://www.googleapis.com/auth/drive';

// var authorizeButton = document.getElementById('authorize_button');
// var signoutButton = document.getElementById('signout_button');

// /**
//  *  On load, called to load the auth2 library and API client library.
//  */
// function handleClientLoad() {
//   gapi.load('client:auth2', initClient);
// }

// /**
//  *  Initializes the API client library and sets up sign-in state
//  *  listeners.
//  */
// function initClient() {
//   gapi.client.init({
//     apiKey: API_KEY,
//     clientId: CLIENT_ID,
//     discoveryDocs: DISCOVERY_DOCS,
//     scope: SCOPES
//   }).then(function () {
//     // Listen for sign-in state changes.
//     gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);


//     // Handle the initial sign-in state.
//     updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
//     authorizeButton.onclick = handleAuthClick;
//     signoutButton.onclick = handleSignoutClick;
//   }, function(error) {
//     appendPre(JSON.stringify(error, null, 2));
//   });
// }

/**
 *  Called when the signed in status changes, to update the UI
 *  appropriately. After a sign-in, the API is called.
 */

// function updateSigninStatus(isSignedIn) {
//   if (isSignedIn) {
//     $('.signInAccountHeadDiv').hide();
//     $('.signedAccountHeadDiv').show();
//     authorizeButton.style.display = 'none';
//     signoutButton.style.display = 'flex';
//     var currentUserDetailObj = gapi.auth2.getAuthInstance().currentUser.get().Is ? gapi.auth2.getAuthInstance().currentUser.get().Is : gapi.auth2.getAuthInstance().currentUser.get().Hs;
//     $('.signedAccountName').text(currentUserDetailObj.sd+ ' (Google Drive)');
//     userEmail = currentUserDetailObj.ot ? currentUserDetailObj.ot : currentUserDetailObj.nt;
//     $('.signedAccountEmail').text(userEmail);
//     $('.signedAccountImg').html(`<img src="${currentUserDetailObj.AI ? currentUserDetailObj.AI : currentUserDetailObj.jI}" style="width:100%;">`);          
//     $('.contentBodyOuter').show();
//     if(rootFolderIdMap && rootFolderIdMap[userEmail]){
//       rootFolderId = rootFolderIdMap[userEmail];
//       currentFolderId = rootFolderId;
//     }
//     if(rootFolderId){
//       $('.contentBodyOuter').show();
//        $("#rootfolderSection").hide();
//       getContactFolder();
//     }
//     else{
//       $('.contentBodyOuter').hide();
//        $("#rootfolderSection").show();
//       getRootFolders();
//     }
//   } else {
//     $('.signInAccountHeadDiv').show();
//     $('.signedAccountHeadDiv').hide();
//     authorizeButton.style.display = 'block';
//     signoutButton.style.display = 'none';
//     $('.contentBodyOuter').hide();
//   }
// }

/**
 *  Sign in the user upon button click.
 */
function handleAuthClick(event) {
    gapi.auth2.getAuthInstance().signIn();
}

/**
 *  Sign out the user upon button click.
 */
function handleSignoutClick(event) {
    gapi.auth2.getAuthInstance().signOut();
}

/**
 * Append a pre element to the body containing the given message
 * as its text node. Used to display the results of the API call.
 *
 * @param {string} message Text to be placed in pre element.
 */
function appendPre(message) {
    var pre = document.getElementById('content');
    var textContent = document.createTextNode(message + '\n');
    pre.appendChild(textContent);
}
$(function () {

    // preventing page from redirecting
    $("html").on("dragover", function (e) {
        e.preventDefault();
        e.stopPropagation();
        $("h1").text("Drag here");
    });

    $("html").on("drop", function (e) {
        e.preventDefault();
        e.stopPropagation();
    });

    // Drag enter
    $('.upload-area').on('dragenter', function (e) {
        e.stopPropagation();
        e.preventDefault();
        $("h1").text("Drop");
    });

    // Drag over
    $('.upload-area').on('dragover', function (e) {
        e.stopPropagation();
        e.preventDefault();
        $("h1").text("Drop");
    });

    // Drop
    $('.upload-area').on('drop', function (e) {
        e.stopPropagation();
        e.preventDefault();

        var files = e.originalEvent.dataTransfer.files;
        for (let i = 0; i < files.length; i++) {
            files[i]['uploadedTime'] = Date.now();
            addThumbnail(files[i]);
            uploadFiles.push(files[i]);
        }
    });

    // Open file selector on div click
    $("#uploadfile").click(function (e) {
        if ($(e.target).hasClass('RemoveUploadFile')) {
            return false;
        }
        $("#file").click();
    });

    // file selected
    $("#file").change(function () {
        var files = $('#file')[0].files;
        for (let i = 0; i < files.length; i++) {
            files[i]['uploadedTime'] = Date.now();
            addThumbnail(files[i]);
            uploadFiles.push(files[i]);
        }
    });

    $("#newfolder").on('keyup', function (e) {
            var folderVal = $('#newfolder').val().trim();
            if(folderVal!==''){
                $('.newFolderNameCreateButt').show();
            }
            else{
                $('.newFolderNameCreateButt').hide();
            } 
            if (e.keyCode === 13) {
                createFolder(folderVal);
            }
         });

});

var loaderDiv = `<div class="loadingAnimation">
    <div id="floatingCirclesG">
    <div class="f_circleG" id="frotateG_01"></div>
    <div class="f_circleG" id="frotateG_02"></div>
    <div class="f_circleG" id="frotateG_03"></div>
    <div class="f_circleG" id="frotateG_04"></div>
    <div class="f_circleG" id="frotateG_05"></div>
    <div class="f_circleG" id="frotateG_06"></div>
    <div class="f_circleG" id="frotateG_07"></div>
    <div class="f_circleG" id="frotateG_08"></div>
  </div>
</div>`;

var loaderDivFullView = `<div class="loadingAnimationFullView">
    <div id="floatingCirclesG">
    <div class="f_circleGfull" id="frotateG_01"></div>
    <div class="f_circleGfull" id="frotateG_02"></div>
    <div class="f_circleGfull" id="frotateG_03"></div>
    <div class="f_circleGfull" id="frotateG_04"></div>
    <div class="f_circleGfull" id="frotateG_05"></div>
    <div class="f_circleGfull" id="frotateG_06"></div>
    <div class="f_circleGfull" id="frotateG_07"></div>
    <div class="f_circleGfull" id="frotateG_08"></div>
  </div>
</div>`;


// Added thumbnail
function addThumbnail(data) {
    $("#uploadfile h3").remove();
    var len = $("#uploadfile div.thumbnail").length;

    var num = Number(len);
    num = num + 1;

    var name = data.name;
    var size = convertSize(data.size);
    var src = URL.createObjectURL(data);
    let fileType = getFileType(data.type);
    // Creating an thumbnail
    let appendHtml = `<div id="thumbnail_${num}" class="thumbnail" onmouseover="$(this).find('.RemoveUploadFile').show();" onmouseout="$(this).find('.RemoveUploadFile').hide();">
              <div class="uploadFileViewDiv">`;

    if (fileType == 'image')
        appendHtml = appendHtml + `<img src="${src}" width="100%" height="100%">`;
    else if (fileType == 'videocam')
        appendHtml = appendHtml + `<span class="material-icons" style="color:#2768c3;">play_circle_filled</span>`;
    else if (fileType == 'audiotrack')
        appendHtml = appendHtml + `<span class="material-icons" style="color:#c32747;">headset</span>`;
    else if (fileType == 'picture_as_pdf')
        appendHtml = appendHtml + `<span class="material-icons" style="color:#a96d08;">picture_as_pdf</span>`;
    else
        appendHtml = appendHtml + `<span class="material-icons" style="color:#08a99f;">description</span>`;

    $("#uploadfile").append(appendHtml + `</div>
      <div class="uploadFileViewSize"><span class="size">${size}</span></div>
      <span class="RemoveUploadFile" style="display:none;" onclick="removeUploadFile(this, '${data.uploadedTime}')">×</span>
      </div>`);
    $('.uploadButtonDiv').show();
}

function removeUploadFile(selected, key) {
    $(selected).parent().remove();
    uploadFiles = $.grep(uploadFiles, function (e) {
        return e.uploadedTime != key;
    });
    if (!$("#uploadfile div.thumbnail").length) {
        $("#uploadfile").html(`<h3>Drag and Drop file here<br/>Or<br/>Click to select file</h3>`);
        uploadFiles = [];
        $('.uploadButtonDiv').hide();
    }
}

// Bytes conversion
function convertSize(size) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (size == 0) return '0 Byte';
    var i = parseInt(Math.floor(Math.log(size) / Math.log(1024)));
    return Math.round(size / Math.pow(1024, i), 2) + ' ' + sizes[i];
}
