try 
{
	responseMap = crmAPIRequest.toMap().get("body").toMap();
	eventType = responseMap.get("event");
	payload = responseMap.get("payload").toMap();
	questions = payload.get("questions_and_answers").toList();
	customerMap = payload.get("invitee").toMap();
	email = customerMap.get("email");
	eventMap = payload.get("event").toMap();
	ownerEmail = eventMap.get("extended_assigned_to").toList().get(0).toMap().get("email");
	userTypeMap = Map();
	userTypeMap.put("type","ActiveUsers");
	response = zoho.crm.invokeConnector("crm.getusers",userTypeMap);
	respMap = response.get("response").toMap();
	users = respMap.get("users");
	userList = users.toJSONList();
	for each  user in userList
	{
		eachUser = user.toMap();
		if(eachUser.get("email") == ownerEmail)
		{
			ownerId = eachUser.get("id");
		}
	}
	for each  quest in questions
	{
		quest = quest.toMap();
		if(quest.get("question") == "Flexmls Username")
		{
			ans = quest.get("answer");
		}
		else if(quest.get("question") == "Phone" || quest.get("question") == "phone")
		{
			phoneNumber = quest.get("answer");
		}
	}
	module = zoho.crm.getOrgVariable("calendlyforzohocrm__Module");
	contactslist = List();
	optionalMap = Map();
	optionalMap.put("trigger",{"workflow","approval","blueprint"});
	if(email != null && email + "" != "")
	{
		contactslist = zoho.crm.searchRecords("Contacts","(Email:equals:" + email + ")");
		leadlist = zoho.crm.searchRecords("Leads","(Email:equals:" + email + ")");
	}
	if(contactslist.size() > 0 || leadlist.size() > 0)
	{
		if(contactslist.size() > 0)
		{
			module = "Contacts";
			contactLeadMap = contactslist.toMap();
		}
		else
		{
			module = "Leads";
			contactLeadMap = leadlist.toMap();
		}
		contactId = contactLeadMap.get("id");
		if(contactLeadMap.get("Full_Name") != customerMap.get("name"))
		{
			updateContactMap = Map();
			if(zoho.adminuserid == "mcarter@fbsdata.com")
			{
				if(ans != null && ans != "")
				{
					updateContactMap.put("FlexMLS_Username",ans);
				}
				if(phoneNumber != null && phoneNumber != "")
				{
					updateContactMap.put("Phone",phoneNumber);
				}
			}
			updateContactMap.put("First_Name",customerMap.get("first_name"));
			updateContactMap.put("Last_Name",customerMap.get("last_name"));
			updateContactMap.put("Lead_Source","Calendly");
			if(customerMap.get("first_name") == null && customerMap.get("last_name") == null)
			{
				updateContactMap.put("Last_Name",customerMap.get("name"));
			}
			zoho.crm.updateRecord(module,contactId,updateContactMap,optionalMap);
		}
	}
	else
	{
		createContactMap = Map();
		if(zoho.adminuserid == "mcarter@fbsdata.com")
		{
			if(ans != null && ans != "")
			{
				createContactMap.put("FlexMLS_Username",ans);
			}
			if(phoneNumber != null && phoneNumber != "")
			{
				createContactMap.put("Phone",phoneNumber);
			}
		}
		if(ownerId != null)
		{
			createContactMap.put("Owner",ownerId);
		}
		if(customerMap.get("first_name") == null && customerMap.get("last_name") == null)
		{
			createContactMap.put("Last_Name",customerMap.get("name"));
		}
		else
		{
			createContactMap.put("First_Name",customerMap.get("first_name"));
			createContactMap.put("Last_Name",customerMap.get("last_name"));
		}
		createContactMap.put("Email",email);
		createContactMap.put("Lead_Source","Calendly");
		contactResp = zoho.crm.createRecord(module,createContactMap,optionalMap);
		info "contactResp" + contactResp;
		contactId = contactResp.get("id");
	}
	info "contactId" + contactId;
	event_type = payload.get("event_type").toMap();
	activityModule = zoho.crm.getOrgVariable("calendlyforzohocrm__Activity");
	if(activityModule == "Events")
	{
		CalendlyId = "calendlyforzohocrm__CalendlyId";
		createEventMap = {"Event_Title":event_type.get("name") + " with " + customerMap.get("name"),"All_day":false,"Start_DateTime":eventMap.get("start_time"),"End_DateTime":eventMap.get("end_time"),CalendlyId:eventMap.get("uuid")};
	}
	else if(activityModule == "Tasks")
	{
		CalendlyId = "calendlyforzohocrm__Calendly_Id";
		createEventMap = {"Subject":event_type.get("name") + " with " + customerMap.get("name"),"Due_Date":eventMap.get("start_time").toDate(),CalendlyId:eventMap.get("uuid")};
	}
	if(ownerId != null)
	{
		createEventMap.put("Owner",ownerId);
	}
	if(eventMap == null || eventMap + "" == "")
	{
		return contactId + "";
	}
	if(eventType == "invitee.created")
	{
		if(module == "Contacts")
		{
			contactInfo = {"name":"Arthur","id":contactId};
			createEventMap.put("Who_Id",contactInfo);
			createEventMap.put("Participants",{{"type":"contact","participant":contactId}});
		}
		else if(module == "Leads")
		{
			createEventMap.put("hostId",contactId);
			createEventMap.put("$se_module","Leads");
			createEventMap.put("What_Id",contactId);
			createEventMap.put("Participants",{{"type":"lead","participant":contactId}});
		}
		info "createEventMap" + createEventMap;
		EventResp = zoho.crm.createRecord(activityModule,createEventMap,optionalMap);
		isShowSignal = zoho.crm.getOrgVariable("calendlyforzohocrm__SalesSignal");
		if(EventResp.get("id") != null && isShowSignal == "true")
		{
			EventId = EventResp.get("id");
			signalMap = Map();
			signalMap.put("signal_namespace","calendlyforzohocrm.calendlynewappointment");
			signalMap.put("email",email);
			signalMap.put("subject",event_type.get("name") + " to " + customerMap.get("name"));
			signalMap.put("message",event_type.get("name") + " to " + customerMap.get("name"));
			actionsList = List();
			actionMap = Map();
			actionMap.put("type","link");
			actionMap.put("display_name","View " + activityModule.subString(0,activityModule.length() - 1));
			actionMap.put("url","/crm/EntityInfo.do?module=" + activityModule + "&id=" + EventId);
			actionsList.add(actionMap);
			signalMap.put("actions",actionsList);
			result = zoho.crm.invokeConnector("raisesignal",signalMap);
			info "result" + result;
		}
	}
	else if(eventType == "invitee.canceled")
	{
		crmEvent = zoho.crm.searchRecords("Events","(calendlyforzohocrm__CalendlyId:equals:" + eventMap.get("uuid") + ")");
		crmTask = zoho.crm.searchRecords("Tasks","(calendlyforzohocrm__Calendly_Id:equals:" + eventMap.get("uuid") + ")");
		if(crmEvent + "" != "")
		{
			crmEventId = crmEvent.toMap().get("id");
			mapp = Map();
			mapp.put("module","Events");
			mapp.put("id",crmEventId);
			mapp.put("calendlyforzohocrm__CalendlyId",eventMap.get("uuid"));
			EventResp = zoho.crm.invokeConnector("crm.delete",mapp);
		}
		else if(crmTask + "" != "")
		{
			crmTaskId = crmTask.toMap().get("id");
			mapp = Map();
			mapp.put("module","Tasks");
			mapp.put("id",crmTaskId);
			mapp.put("calendlyforzohocrm__Calendly_Id",eventMap.get("uuid"));
			EventResp = zoho.crm.invokeConnector("crm.delete",mapp);
		}
	}
	if(EventResp != null)
	{
		return EventResp + "";
	}
}
 catch (e)
{	
info e;
}
return "";