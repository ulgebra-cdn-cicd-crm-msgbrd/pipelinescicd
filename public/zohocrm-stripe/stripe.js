
var stripe_apikey ;

document.addEventListener("DOMContentLoaded", function(event) {

		ZOHO.embeddedApp.on("PageLoad", async function(record) { 
						await resizeWidget("65%", "100%");    
						await getCredential();   		
						var recordId = record.EntityId;
		               	var recordModule = record.Entity;
		               	// console.log(record); 
		               	await getRecordDetails(recordModule,recordId);   
		               	

		});

		ZOHO.embeddedApp.init().then();

});

async function resizeWidget(height,width)
{
	await ZOHO.CRM.UI.Resize({height:height,width:width}).then(function(data){
		// console.log(data);
	});
}

async function  getRecordDetails(currentModule,recordId)
{
	await ZOHO.CRM.API.getRecord({Entity:currentModule,RecordID:recordId})
	.then(async function(response){
		// console.log(response);
		await getCustomers(response.data[0].Email);
	});
}

async function getCredential()
{
	ZOHO.CRM.API.getOrgVariable("ulgstripe__credentials").then(function(apiKeyData){
        if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content && apiKeyData.Success.Content != "{}")
        {
        	var credentials = JSON.parse(apiKeyData.Success.Content);
        	if(credentials.apikey)
        	{
        		stripe_apikey = credentials.apikey;
        	}
        }      	
	});	
}

async function getCustomers(email)
{
	var request ={
		url : "https://api.stripe.com/v1/customers?email="+email,
		headers:{
		 	Authorization:'Bearer '+stripe_apikey,
		}
	}
	await ZOHO.CRM.HTTP.get(request)
	.then(async function(response){
		response = JSON.parse(response);
		// console.log(response.data);
		var datas = response.data;
		if(datas)
		{
			$("#select_invoice_cusomer_opt_count_div").text(`${datas.length} Customers found by email`);
			await datas.forEach(async(customer)=>{
				$("#select_invoice_cusomer_opt_div").append(`<div class="select_customer_options" onclick="getCustomer_invoices('${customer.id}','${customer.name}')">${customer.name}</div>`);
			});

			// await getCustomer_invoices(datas[0].id,datas[0].name);
		}
		else
		{
			$("#select_invoice_cusomer_opt_div").append(`<div class="select_customer_options">No Customers</div>`);
			$("#select_customer_name_div").text("No Customers");
			return  true;
		}
		setTimeout(function(){$("#stripe_loader_full_div").attr("style","display:none;");},1500);
	    
	});
}

async function getCustomer_invoices(customer_id,customer_name)
{
	$("#stripe_loader_full_div").attr("style","display:block;");
	$("#stripe_customer_invoice_title_txt").attr("style","display:block;");
	$("#select_invoice_cusomer_opt_div").hide();
	if(!customer_id && !customer_name)
	{
		return true;
	}
	$("#select_stripe_customer_name_div").text(customer_name);
	$("#select_stripe_customer_update_div").attr("onclick",`updateCustomer_popupDiv('${customer_id}')`);
	var request ={
		url : "https://api.stripe.com/v1/invoices?customer="+customer_id,
		headers:{
		 	Authorization:'Bearer '+stripe_apikey,
		}
	}
	$("#stripe_invoices_contains_div").empty();
	await ZOHO.CRM.HTTP.get(request)
	.then(async function(data){
	    // console.log(JSON.parse(data).data);
	    data = JSON.parse(data).data;
	    if(data.length > 0)
	    {
		    var invoices = data;
		    await invoices.forEach(async(invoice)=>{
		    	await show_invoice(invoice);
		    });
		}
		if(data.length == 0)
		{
			// console.log(data.length);
			await $("#stripe_invoices_contains_div").append(`<div id="no_invoices">No Invoice</div>`);
		}
	    setTimeout(function(){$("#stripe_loader_full_div").attr("style","display:none;");},1500);
	});
}

async function show_invoice(invoice)
{
	await $("#stripe_invoices_contains_div").append(`<div class="customer_invoice_item_div" id="customer_invoice_item_div_${invoice.id}">
												  <div class="customer_invoice_status_info_div">
												  	<div class="customer_invoice_status_txt_div">Status</div>
												  	<div class="customer_invoice_status_val_div" id="customer_invoice_status_val_div_${invoice.id}" style="color: ${await status_color(invoice.status)}">${invoice.status}</div>
												  	<div class="customer_invoice_update_div" id="customer_invoice_update_div_${invoice.id}" style="display:none;${(invoice.status != "draft" && invoice.status != "void" && invoice.status !="uncollectible") ? "width:100%;left:0;" : ""}">${await getUpdateInvoiceFields(invoice)}</div>
												  </div>
												  <div class="customer_invoice_details_div">
												  	<div class="customer_inv&rec_num_div">
												  	   <div class="customer_invoice_num" id="customer_invoice_num_${invoice.id}">Invoice No : ${invoice.number? invoice.number : "No Number"}</div>
												  	   <div class="customer_invoice_rec_num" style="display: ${invoice.receipt_number? "block" : "none"}">Receipt No : ${invoice.receipt_number}</div>
												  	   <div class="customer_invoice_date_details_div" id="customer_invoice_date_details_div_${invoice.id}">${await getInvoiceStatusDate(invoice)}</div>
												  	</div>
												  	<div class="customer_invoice_otherDetails_div">
												  		<div class="customer_invoice_period" style="display: ${invoice.subscription != null ? "block":"none"}">Period : ${await getInvoiceDate(invoice.period_start)+" - "+await getInvoiceDate(invoice.period_end)}</div>
												  		<div class="customer_invoice_pdf" style="display: ${invoice.invoice_pdf != null ? "block" : "none"}"><a href="${invoice.invoice_pdf}" class="customer_invoice_pdf_a"><div class="customer_invoice_pdf_btn_div">Invoice pdf <span class="material-icons invoice_pdf_icon">download</span></div></a></div>
												  		<div class="customer_invoice_hosted_invUrl" style="display: ${invoice.hosted_invoice_url != null ? "block" : "none"}"><span class="material-icons inv_hosted_url_icon"  onclick="open_inv_hosted_inUrl('${invoice.hosted_invoice_url}')">launch</span></div>
												  	</div>
												  	<div class="customer_invoice_list_items" id="customer_invoice_list_items_${invoice.id}" style="display:none">
												  		<div class="customer_invoice_list_item_title_div">
												  			<div class="ci_description_div">Description</div>
												  			<div class="ci_qty_div">Qty</div>
												  			<div class="ci_price_div">Unit price</div>
												  			<div class="ci_amount_div">Amount</div>
												  			<div class="ci_discount_div">Discount</div>
												  		</div>
												  	</div>
												  	<div class="customer_invoice_calc_div" id="customer_invoice_calc_div_${invoice.id}" style="display:none">
												  		<div class="customer_invoice_subtotal_div">
												  			<div class="customer_invoice_subtotal_txt_div">Subtotal</div>
												  			<div class="customer_invoice_subtotal_val_div">${await changeAmount(invoice.currency,invoice.subtotal)}</div>
												  		</div>
												  		<div class="customer_invoice_coupon_div" style="visible:${invoice.discounts.length>0 ? "true":"false"}">
												  			<div class="customer_invoice_coupon_txt_div">${invoice.discounts.length>0 ? (invoice.discount.coupon["name"] + invoice.discount.coupon["percent_off"] ? (invoice.discount.coupon["name"]+" ("+ invoice.discount.coupon["percent_off"]+"% off)"):""+invoice.discount.coupon["name"]):""}</div>
												  			<div class="customer_invoice_coupon_val_div">${invoice.discounts.length>0 ? (invoice.discount.coupon["percent_off"] ? (" - "+ await getDisPerAmt(invoice.currency,invoice.discounts[0],invoice.total_discount_amounts)) : (" - "+ await changeAmount(invoice.currency,invoice.discount.coupon["amount_off"]))) : ""}</div>
												  		</div>
												  		<div class="customer_invoice_tax_div" style="visible:${invoice.tax ? "true":"false"}">
												  			<div class="customer_invoice_tax_txt_div">${invoice.default_tax_rates.length>0 ? invoice.default_tax_rates[0].display_name : ""}</div>
												  			<div class="customer_invoice_tax_val_div">${invoice.default_tax_rates.length>0 ? (" + "+ await changeAmount(invoice.currency,invoice.tax)) : ""}</div>
												  		</div>
												  		<div class="customer_invoice_total_div">
												  			<div class="customer_invoice_amount_txt_div">${invoice.paid ? "Amount paid" : "Amount due"}</div>
												  			<div class="customer_invoice_amount_val_div">${await changeAmount(invoice.currency,invoice.total)}</div>
												  		</div>
												  	</div>
												  	<div class="expand_invoice_item" id="expand_invoice_item_${invoice.id}"><span class="material-icons expand_icon" id="expand_icon_${invoice.id}" onclick="expand_invoice_item('${invoice.id}')">expand_more</span></div>
												  </div>
											   </div>`);

	var items = invoice.lines['data'];
	for(var item_index in items)
	{
		var item = items[item_index];
		await $(`#customer_invoice_list_items_${invoice.id}`).append(`<div class="customer_invoice_list_item">
																		<div class="ci_li_description_div">${item.description}</div>
															  			<div class="ci_li_qty_div">${item.quantity}</div>
															  			<div class="ci_li_price_div">${await changeAmount(item.currency,item.price['unit_amount'])}</div>
															  			<div class="ci_li_amount_div">${await changeAmount(item.currency,item["amount"])}</div>
															  			<div class="ci_li_discount_div">${item.discounts.length>0 ? (await changeAmount(item.currency,item.discount_amounts[0].amount)) : (await changeAmount(item.currency,"000"))}</div>
																	  </div>`);
	}
			


}

async function updateCustomer_popupDiv(customer_id)
{
	// console.log(customer_id);
	// $("body").append(`<div>
	// 					<div></div>
	// 				  </div>`);

}
async function status_color(status)
{
	if(status == "paid")
	{
		return "#29d37a";
	}
	else if(status == "open")
	{
		return "#29ced3";
	}  
	else if(status == "draft")
	{
	    return "#7087a8";
	}
	else if(status == "void")
	{
		return "#c15e68";
	}
	else if(status == "uncollectible")
	{
		return "#c5b7cd";
	}
	else
	{
		return "#7087a8";
	}
}

async function getInvoiceStatusDate(invoice)
{
	if(invoice.status == "paid")
	{
		return "Paid Date : "+await getInvoiceDate(invoice.status_transitions["paid_at"]);
	}
	else if(invoice.status == "open")
	{
		return "Due Date: "+await getInvoiceDate(invoice.due_date);
	}  
	else if(invoice.status == "draft")
	{
	    return "Date of issue : "+await getInvoiceDate(invoice.created);
	}
	else if(invoice.status == "void")
	{
		return "Void Date : "+await getInvoiceDate(invoice.status_transitions["voided_at"]);
	}
	else if(invoice.status == "uncollectible")
	{
		return "Uncollectible Date : "+await getInvoiceDate(invoice.status_transitions["marked_uncollectible_at"]);
	}
	else
	{
		return "Date of issue : "+await getInvoiceDate(invoice.created);
	}
}

async function getUpdateInvoiceFields(invoice)
{
	// if(invoice.status == "draft")
	// {
	// 	return `<div class="customer_invoice_delete_btn_div" id="customer_invoice_delete_btn_div_${invoice.id}">
	// 				<button class="customer_invoice_delete_btn" onclick="deleteInvoice('${invoice.id}')">Delete</button>
	// 			</div>
	// 			<div class="customer_invoice_send_btn_div" id="customer_invoice_send_btn_div_${invoice.id}">
	// 				<button class="customer_invoice_send_btn" onclick="sendInvoice('${invoice.id}')">Send</button>
	// 			</div>`;
	// }
	// if(invoice.status != "draft" && invoice.status != "void" && invoice.status !="uncollectible")
	// {
	// 	return `<div class="customer_invoice_changeOfVoid_btn_div" id="customer_invoice_changeOfVoid_btn_div_${invoice.id}">
	// 				<button class="customer_invoice_changeOfVoid_btn" onclick="changeVoidOfInvoice('${invoice.id}')">Void</button>
	// 			</div>
	// 			<div class="customer_invoice_changeOfUncollectible_btn_div" id="customer_invoice_changeOfUncollectible_btn_div_${invoice.id}">
	// 				<button class="customer_invoice_changeOfUncollectible_btn" onclick="changeUncollectibleOfInvoice('${invoice.id}')">Uncollectible</button>
	// 			</div>`;
	// }
	// if(invoice.status == "uncollectible")
	// {
	// 	return `<div class="customer_invoice_changeOfVoid_btn_div" id="customer_invoice_changeOfVoid_btn_div_${invoice.id}">
	// 				<button class="customer_invoice_changeOfVoid_btn" onclick="changeVoidOfInvoice('${invoice.id}')">Void</button>
	// 			</div>`;
	// }
	return;
}

async function deleteInvoice(invoice_id)
{
	await getConformation("to delete",async function callback(res){
		if(res == true)
		{
			var request ={
				url : "https://api.stripe.com/v1/invoices/"+invoice_id,
				headers:{
				 	Authorization:'Bearer '+stripe_apikey,
				}
			};
			await ZOHO.CRM.HTTP.delete(request)
			.then(async function(data){
			    // console.log("deleted invoice "+invoice_id);
			    $(`#customer_invoice_item_div_${invoice_id}`).remove();
			});
		}
		else
		{
			// console.log(res);
			return;
		}
	});
	
}

async function sendInvoice(invoice_id)
{
	await getConformation("to send",async function callback(res){
		if(res == true)
		{
			var request ={
				url : "https://api.stripe.com/v1/invoices/"+invoice_id+"/send",
				headers:{
				 	Authorization:'Bearer '+stripe_apikey,
				}
			};
			await ZOHO.CRM.HTTP.post(request)
			.then(async function(data){
				data = JSON.parse(data);
			    $(`#customer_invoice_update_div_${invoice_id}`).empty();
			    $(`#customer_invoice_update_div_${invoice_id}`).append(`<div class="customer_invoice_changeOfVoid_btn_div" id="customer_invoice_changeOfVoid_btn_div_${invoice_id}">
								<button class="customer_invoice_changeOfVoid_btn" onclick="changeVoidOfInvoice('${invoice_id}')">Void</button>
							</div>`);
			    $(`#customer_invoice_num_${invoice_id}`).text("Invoice No : "+data.number);
			    $(`#customer_invoice_status_val_div_${invoice_id}`).text("open");
			});
		}
		else
		{
			// console.log(res);
			return;
		}
	});
}

async function changeVoidOfInvoice(invoice_id)
{
	await getConformation("change to void",async function callback(res){
		if(res == true)
		{
			var request ={
				url : "https://api.stripe.com/v1/invoices/"+invoice_id+"/void",
				headers:{
				 	Authorization:'Bearer '+stripe_apikey,
				}
			};
			await ZOHO.CRM.HTTP.post(request)
			.then(async function(data){
				data = JSON.parse(data);
				var voided_at = "Date of voided : "+await getInvoiceDate(data.status_transitions["voided_at"]);
				$(`#customer_invoice_changeOfVoid_btn_div_${invoice_id}`).remove();
				$(`#customer_invoice_date_details_div_${invoice_id}`).text(voided_at);
			    $(`#customer_invoice_status_val_div_${invoice_id}`).text("void");
			});
		}
		else
		{
			return;
		}
	});
	
}

async function changeUncollectibleOfInvoice(invoice_id)
{
	await getConformation("change to void",async function callback(res){
		if(res == true)
		{
			var request ={
				url : "https://api.stripe.com/v1/invoices/"+invoice_id+"/mark_uncollectible ",
				headers:{
				 	Authorization:'Bearer '+stripe_apikey,
				}
			};
			await ZOHO.CRM.HTTP.post(request)
			.then(async function(data){
				data = JSON.parse(data);
				var uncollectible_at = "Date of uncollectibled : "+await getInvoiceDate(data.status_transitions["marked_uncollectible_at"]);
				$(`#customer_invoice_changeOfUncollectible_btn_div_${invoice_id}`).remove();
				$(`#customer_invoice_date_details_div_${invoice_id}`).text(uncollectible_at);
			    $(`#customer_invoice_status_val_div_${invoice_id}`).text("uncollectible");
			});
		}
		else
		{
			return;
		}
	});
}

async function getConformation(confirm_name,callback)
{
	$("body").append(`<div id="confirm_box_full_div">
						<div id="confirm_box_div">
							<div id="confirm_box_text_div">Do you want ${confirm_name} a invoice?</div>
							<div id="confirm_box_inp_div">
								<div id="confirm_yes_btn">Yes</div>
								<div id="confirm_no_btn">No</div>
							</div>
						</div>
					  </div>`);

	$("#confirm_yes_btn").on('click',function(){
		$("#confirm_box_full_div").remove();
		callback(true);
		return;
	});
	$("#confirm_no_btn").on('click',function(){
		$("#confirm_box_full_div").remove();
		callback(false);
		return;
	});
	return;
}

async function expand_invoice_item(invoice_id)
{
	if($(`#customer_invoice_list_items_${invoice_id}`).is(":visible") == true)
	{
		$(`#customer_invoice_update_div_${invoice_id}`).delay(400).fadeOut();
		$(`#customer_invoice_list_items_${invoice_id}`).delay(400).fadeOut();
		$(`#customer_invoice_calc_div_${invoice_id}`).delay(400).fadeOut();
		$(`#expand_icon_${invoice_id}`).text("expand_more");
		$(`#expand_invoice_item_${invoice_id}`).attr("style","bottom: 0%;");
		$(`#customer_invoice_item_div_${invoice_id}`).attr("style","padding: 2px 2px 20px 2px;");
	}
	else
	{
		$(`#customer_invoice_update_div_${invoice_id}`).fadeIn();
		$(`#customer_invoice_list_items_${invoice_id}`).fadeIn();
		$(`#customer_invoice_calc_div_${invoice_id}`).fadeIn();
		$(`#expand_icon_${invoice_id}`).text("expand_less");
		$(`#expand_invoice_item_${invoice_id}`).attr("style","bottom: -10%;");
		$(`#customer_invoice_item_div_${invoice_id}`).attr("style","padding: 10px 20px 40px 20px;");
	}

}

async function select_customer_opt()
{
	if($("#select_invoice_cusomer_opt_div").is(":visible") == true)
	{
		$("#select_invoice_cusomer_opt_div").hide();
	}
	else
	{
		$("#select_invoice_cusomer_opt_div").show();
	}
}

async function getInvoiceDate(time)
{
	var inv_date = new Date(time*1000);
	let date = inv_date.getDate();
	let month = inv_date.getMonth();
	let year = inv_date.getFullYear();

	switch(month)
	{
		case 0 : { month = "Jan"; break;}
		case 1 : { month = "Feb"; break;}
		case 2 : { month = "Mar"; break;}
		case 3 : { month = "Apr"; break;}
		case 4 : { month = "May"; break;}
		case 5 : { month = "Jun"; break;}
		case 6 : { month = "Jul"; break;}
		case 7 : { month = "Aug"; break;}
		case 8 : { month = "Sep"; break;}
		case 9 : { month = "Oct"; break;}
		case 10 : { month = "Nov"; break;}
		case 11 : { month = "Dec"; break;}
	}

	return ""+month+" "+date+" ,"+year;
}

async function changeAmount(currency,amount)
{
	var amt = amount.toString();
	var amount_str = amt.substring(0,amt.length-2)+"."+amt.substring(amt.length-2,amt.length);
	var amount_str = country_symbols[currency.toUpperCase()].symbol+" "+amount_str;
	return amount_str;
}

async function getDisPerAmt(currency,dis_id,all_discounts)
{
	var discount_amount ;
	await all_discounts.forEach(async(discount)=>{
		if(discount.discount == dis_id)
		{
			discount_amount  = await changeAmount(currency,discount.amount);
			return true;
		}
	});
	return discount_amount;
}

async function open_inv_hosted_inUrl(url)
{
	// console.log(url);
	window.open(url,"_blank");
	return;
}

var country_symbols = {
	"USD": {
		"symbol": "$",
		"name": "US Dollar",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "USD",
		"name_plural": "US dollars"
	},
	"CAD": {
		"symbol": "CA$",
		"name": "Canadian Dollar",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "CAD",
		"name_plural": "Canadian dollars"
	},
	"EUR": {
		"symbol": "€",
		"name": "Euro",
		"symbol_native": "€",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "EUR",
		"name_plural": "euros"
	},
	"AED": {
		"symbol": "AED",
		"name": "United Arab Emirates Dirham",
		"symbol_native": "د.إ.‏",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "AED",
		"name_plural": "UAE dirhams"
	},
	"AFN": {
		"symbol": "Af",
		"name": "Afghan Afghani",
		"symbol_native": "؋",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "AFN",
		"name_plural": "Afghan Afghanis"
	},
	"ALL": {
		"symbol": "ALL",
		"name": "Albanian Lek",
		"symbol_native": "Lek",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "ALL",
		"name_plural": "Albanian lekë"
	},
	"AMD": {
		"symbol": "AMD",
		"name": "Armenian Dram",
		"symbol_native": "դր.",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "AMD",
		"name_plural": "Armenian drams"
	},
	"ARS": {
		"symbol": "AR$",
		"name": "Argentine Peso",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "ARS",
		"name_plural": "Argentine pesos"
	},
	"AUD": {
		"symbol": "AU$",
		"name": "Australian Dollar",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "AUD",
		"name_plural": "Australian dollars"
	},
	"AZN": {
		"symbol": "man.",
		"name": "Azerbaijani Manat",
		"symbol_native": "ман.",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "AZN",
		"name_plural": "Azerbaijani manats"
	},
	"BAM": {
		"symbol": "KM",
		"name": "Bosnia-Herzegovina Convertible Mark",
		"symbol_native": "KM",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "BAM",
		"name_plural": "Bosnia-Herzegovina convertible marks"
	},
	"BDT": {
		"symbol": "Tk",
		"name": "Bangladeshi Taka",
		"symbol_native": "৳",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "BDT",
		"name_plural": "Bangladeshi takas"
	},
	"BGN": {
		"symbol": "BGN",
		"name": "Bulgarian Lev",
		"symbol_native": "лв.",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "BGN",
		"name_plural": "Bulgarian leva"
	},
	"BHD": {
		"symbol": "BD",
		"name": "Bahraini Dinar",
		"symbol_native": "د.ب.‏",
		"decimal_digits": 3,
		"rounding": 0,
		"code": "BHD",
		"name_plural": "Bahraini dinars"
	},
	"BIF": {
		"symbol": "FBu",
		"name": "Burundian Franc",
		"symbol_native": "FBu",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "BIF",
		"name_plural": "Burundian francs"
	},
	"BND": {
		"symbol": "BN$",
		"name": "Brunei Dollar",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "BND",
		"name_plural": "Brunei dollars"
	},
	"BOB": {
		"symbol": "Bs",
		"name": "Bolivian Boliviano",
		"symbol_native": "Bs",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "BOB",
		"name_plural": "Bolivian bolivianos"
	},
	"BRL": {
		"symbol": "R$",
		"name": "Brazilian Real",
		"symbol_native": "R$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "BRL",
		"name_plural": "Brazilian reals"
	},
	"BWP": {
		"symbol": "BWP",
		"name": "Botswanan Pula",
		"symbol_native": "P",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "BWP",
		"name_plural": "Botswanan pulas"
	},
	"BYN": {
		"symbol": "Br",
		"name": "Belarusian Ruble",
		"symbol_native": "руб.",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "BYN",
		"name_plural": "Belarusian rubles"
	},
	"BZD": {
		"symbol": "BZ$",
		"name": "Belize Dollar",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "BZD",
		"name_plural": "Belize dollars"
	},
	"CDF": {
		"symbol": "CDF",
		"name": "Congolese Franc",
		"symbol_native": "FrCD",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "CDF",
		"name_plural": "Congolese francs"
	},
	"CHF": {
		"symbol": "CHF",
		"name": "Swiss Franc",
		"symbol_native": "CHF",
		"decimal_digits": 2,
		"rounding": 0.05,
		"code": "CHF",
		"name_plural": "Swiss francs"
	},
	"CLP": {
		"symbol": "CL$",
		"name": "Chilean Peso",
		"symbol_native": "$",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "CLP",
		"name_plural": "Chilean pesos"
	},
	"CNY": {
		"symbol": "CN¥",
		"name": "Chinese Yuan",
		"symbol_native": "CN¥",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "CNY",
		"name_plural": "Chinese yuan"
	},
	"COP": {
		"symbol": "CO$",
		"name": "Colombian Peso",
		"symbol_native": "$",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "COP",
		"name_plural": "Colombian pesos"
	},
	"CRC": {
		"symbol": "₡",
		"name": "Costa Rican Colón",
		"symbol_native": "₡",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "CRC",
		"name_plural": "Costa Rican colóns"
	},
	"CVE": {
		"symbol": "CV$",
		"name": "Cape Verdean Escudo",
		"symbol_native": "CV$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "CVE",
		"name_plural": "Cape Verdean escudos"
	},
	"CZK": {
		"symbol": "Kč",
		"name": "Czech Republic Koruna",
		"symbol_native": "Kč",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "CZK",
		"name_plural": "Czech Republic korunas"
	},
	"DJF": {
		"symbol": "Fdj",
		"name": "Djiboutian Franc",
		"symbol_native": "Fdj",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "DJF",
		"name_plural": "Djiboutian francs"
	},
	"DKK": {
		"symbol": "Dkr",
		"name": "Danish Krone",
		"symbol_native": "kr",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "DKK",
		"name_plural": "Danish kroner"
	},
	"DOP": {
		"symbol": "RD$",
		"name": "Dominican Peso",
		"symbol_native": "RD$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "DOP",
		"name_plural": "Dominican pesos"
	},
	"DZD": {
		"symbol": "DA",
		"name": "Algerian Dinar",
		"symbol_native": "د.ج.‏",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "DZD",
		"name_plural": "Algerian dinars"
	},
	"EEK": {
		"symbol": "Ekr",
		"name": "Estonian Kroon",
		"symbol_native": "kr",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "EEK",
		"name_plural": "Estonian kroons"
	},
	"EGP": {
		"symbol": "EGP",
		"name": "Egyptian Pound",
		"symbol_native": "ج.م.‏",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "EGP",
		"name_plural": "Egyptian pounds"
	},
	"ERN": {
		"symbol": "Nfk",
		"name": "Eritrean Nakfa",
		"symbol_native": "Nfk",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "ERN",
		"name_plural": "Eritrean nakfas"
	},
	"ETB": {
		"symbol": "Br",
		"name": "Ethiopian Birr",
		"symbol_native": "Br",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "ETB",
		"name_plural": "Ethiopian birrs"
	},
	"GBP": {
		"symbol": "£",
		"name": "British Pound Sterling",
		"symbol_native": "£",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "GBP",
		"name_plural": "British pounds sterling"
	},
	"GEL": {
		"symbol": "GEL",
		"name": "Georgian Lari",
		"symbol_native": "GEL",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "GEL",
		"name_plural": "Georgian laris"
	},
	"GHS": {
		"symbol": "GH₵",
		"name": "Ghanaian Cedi",
		"symbol_native": "GH₵",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "GHS",
		"name_plural": "Ghanaian cedis"
	},
	"GNF": {
		"symbol": "FG",
		"name": "Guinean Franc",
		"symbol_native": "FG",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "GNF",
		"name_plural": "Guinean francs"
	},
	"GTQ": {
		"symbol": "GTQ",
		"name": "Guatemalan Quetzal",
		"symbol_native": "Q",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "GTQ",
		"name_plural": "Guatemalan quetzals"
	},
	"HKD": {
		"symbol": "HK$",
		"name": "Hong Kong Dollar",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "HKD",
		"name_plural": "Hong Kong dollars"
	},
	"HNL": {
		"symbol": "HNL",
		"name": "Honduran Lempira",
		"symbol_native": "L",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "HNL",
		"name_plural": "Honduran lempiras"
	},
	"HRK": {
		"symbol": "kn",
		"name": "Croatian Kuna",
		"symbol_native": "kn",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "HRK",
		"name_plural": "Croatian kunas"
	},
	"HUF": {
		"symbol": "Ft",
		"name": "Hungarian Forint",
		"symbol_native": "Ft",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "HUF",
		"name_plural": "Hungarian forints"
	},
	"IDR": {
		"symbol": "Rp",
		"name": "Indonesian Rupiah",
		"symbol_native": "Rp",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "IDR",
		"name_plural": "Indonesian rupiahs"
	},
	"ILS": {
		"symbol": "₪",
		"name": "Israeli New Sheqel",
		"symbol_native": "₪",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "ILS",
		"name_plural": "Israeli new sheqels"
	},
	"INR": {
		"symbol": "Rs",
		"name": "Indian Rupee",
		"symbol_native": "টকা",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "INR",
		"name_plural": "Indian rupees"
	},
	"IQD": {
		"symbol": "IQD",
		"name": "Iraqi Dinar",
		"symbol_native": "د.ع.‏",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "IQD",
		"name_plural": "Iraqi dinars"
	},
	"IRR": {
		"symbol": "IRR",
		"name": "Iranian Rial",
		"symbol_native": "﷼",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "IRR",
		"name_plural": "Iranian rials"
	},
	"ISK": {
		"symbol": "Ikr",
		"name": "Icelandic Króna",
		"symbol_native": "kr",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "ISK",
		"name_plural": "Icelandic krónur"
	},
	"JMD": {
		"symbol": "J$",
		"name": "Jamaican Dollar",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "JMD",
		"name_plural": "Jamaican dollars"
	},
	"JOD": {
		"symbol": "JD",
		"name": "Jordanian Dinar",
		"symbol_native": "د.أ.‏",
		"decimal_digits": 3,
		"rounding": 0,
		"code": "JOD",
		"name_plural": "Jordanian dinars"
	},
	"JPY": {
		"symbol": "¥",
		"name": "Japanese Yen",
		"symbol_native": "￥",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "JPY",
		"name_plural": "Japanese yen"
	},
	"KES": {
		"symbol": "Ksh",
		"name": "Kenyan Shilling",
		"symbol_native": "Ksh",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "KES",
		"name_plural": "Kenyan shillings"
	},
	"KHR": {
		"symbol": "KHR",
		"name": "Cambodian Riel",
		"symbol_native": "៛",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "KHR",
		"name_plural": "Cambodian riels"
	},
	"KMF": {
		"symbol": "CF",
		"name": "Comorian Franc",
		"symbol_native": "FC",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "KMF",
		"name_plural": "Comorian francs"
	},
	"KRW": {
		"symbol": "₩",
		"name": "South Korean Won",
		"symbol_native": "₩",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "KRW",
		"name_plural": "South Korean won"
	},
	"KWD": {
		"symbol": "KD",
		"name": "Kuwaiti Dinar",
		"symbol_native": "د.ك.‏",
		"decimal_digits": 3,
		"rounding": 0,
		"code": "KWD",
		"name_plural": "Kuwaiti dinars"
	},
	"KZT": {
		"symbol": "KZT",
		"name": "Kazakhstani Tenge",
		"symbol_native": "тңг.",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "KZT",
		"name_plural": "Kazakhstani tenges"
	},
	"LBP": {
		"symbol": "L.L.",
		"name": "Lebanese Pound",
		"symbol_native": "ل.ل.‏",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "LBP",
		"name_plural": "Lebanese pounds"
	},
	"LKR": {
		"symbol": "SLRs",
		"name": "Sri Lankan Rupee",
		"symbol_native": "SL Re",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "LKR",
		"name_plural": "Sri Lankan rupees"
	},
	"LTL": {
		"symbol": "Lt",
		"name": "Lithuanian Litas",
		"symbol_native": "Lt",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "LTL",
		"name_plural": "Lithuanian litai"
	},
	"LVL": {
		"symbol": "Ls",
		"name": "Latvian Lats",
		"symbol_native": "Ls",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "LVL",
		"name_plural": "Latvian lati"
	},
	"LYD": {
		"symbol": "LD",
		"name": "Libyan Dinar",
		"symbol_native": "د.ل.‏",
		"decimal_digits": 3,
		"rounding": 0,
		"code": "LYD",
		"name_plural": "Libyan dinars"
	},
	"MAD": {
		"symbol": "MAD",
		"name": "Moroccan Dirham",
		"symbol_native": "د.م.‏",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "MAD",
		"name_plural": "Moroccan dirhams"
	},
	"MDL": {
		"symbol": "MDL",
		"name": "Moldovan Leu",
		"symbol_native": "MDL",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "MDL",
		"name_plural": "Moldovan lei"
	},
	"MGA": {
		"symbol": "MGA",
		"name": "Malagasy Ariary",
		"symbol_native": "MGA",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "MGA",
		"name_plural": "Malagasy Ariaries"
	},
	"MKD": {
		"symbol": "MKD",
		"name": "Macedonian Denar",
		"symbol_native": "MKD",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "MKD",
		"name_plural": "Macedonian denari"
	},
	"MMK": {
		"symbol": "MMK",
		"name": "Myanma Kyat",
		"symbol_native": "K",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "MMK",
		"name_plural": "Myanma kyats"
	},
	"MOP": {
		"symbol": "MOP$",
		"name": "Macanese Pataca",
		"symbol_native": "MOP$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "MOP",
		"name_plural": "Macanese patacas"
	},
	"MUR": {
		"symbol": "MURs",
		"name": "Mauritian Rupee",
		"symbol_native": "MURs",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "MUR",
		"name_plural": "Mauritian rupees"
	},
	"MXN": {
		"symbol": "MX$",
		"name": "Mexican Peso",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "MXN",
		"name_plural": "Mexican pesos"
	},
	"MYR": {
		"symbol": "RM",
		"name": "Malaysian Ringgit",
		"symbol_native": "RM",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "MYR",
		"name_plural": "Malaysian ringgits"
	},
	"MZN": {
		"symbol": "MTn",
		"name": "Mozambican Metical",
		"symbol_native": "MTn",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "MZN",
		"name_plural": "Mozambican meticals"
	},
	"NAD": {
		"symbol": "N$",
		"name": "Namibian Dollar",
		"symbol_native": "N$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "NAD",
		"name_plural": "Namibian dollars"
	},
	"NGN": {
		"symbol": "₦",
		"name": "Nigerian Naira",
		"symbol_native": "₦",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "NGN",
		"name_plural": "Nigerian nairas"
	},
	"NIO": {
		"symbol": "C$",
		"name": "Nicaraguan Córdoba",
		"symbol_native": "C$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "NIO",
		"name_plural": "Nicaraguan córdobas"
	},
	"NOK": {
		"symbol": "Nkr",
		"name": "Norwegian Krone",
		"symbol_native": "kr",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "NOK",
		"name_plural": "Norwegian kroner"
	},
	"NPR": {
		"symbol": "NPRs",
		"name": "Nepalese Rupee",
		"symbol_native": "नेरू",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "NPR",
		"name_plural": "Nepalese rupees"
	},
	"NZD": {
		"symbol": "NZ$",
		"name": "New Zealand Dollar",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "NZD",
		"name_plural": "New Zealand dollars"
	},
	"OMR": {
		"symbol": "OMR",
		"name": "Omani Rial",
		"symbol_native": "ر.ع.‏",
		"decimal_digits": 3,
		"rounding": 0,
		"code": "OMR",
		"name_plural": "Omani rials"
	},
	"PAB": {
		"symbol": "B/.",
		"name": "Panamanian Balboa",
		"symbol_native": "B/.",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "PAB",
		"name_plural": "Panamanian balboas"
	},
	"PEN": {
		"symbol": "S/.",
		"name": "Peruvian Nuevo Sol",
		"symbol_native": "S/.",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "PEN",
		"name_plural": "Peruvian nuevos soles"
	},
	"PHP": {
		"symbol": "₱",
		"name": "Philippine Peso",
		"symbol_native": "₱",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "PHP",
		"name_plural": "Philippine pesos"
	},
	"PKR": {
		"symbol": "PKRs",
		"name": "Pakistani Rupee",
		"symbol_native": "₨",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "PKR",
		"name_plural": "Pakistani rupees"
	},
	"PLN": {
		"symbol": "zł",
		"name": "Polish Zloty",
		"symbol_native": "zł",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "PLN",
		"name_plural": "Polish zlotys"
	},
	"PYG": {
		"symbol": "₲",
		"name": "Paraguayan Guarani",
		"symbol_native": "₲",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "PYG",
		"name_plural": "Paraguayan guaranis"
	},
	"QAR": {
		"symbol": "QR",
		"name": "Qatari Rial",
		"symbol_native": "ر.ق.‏",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "QAR",
		"name_plural": "Qatari rials"
	},
	"RON": {
		"symbol": "RON",
		"name": "Romanian Leu",
		"symbol_native": "RON",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "RON",
		"name_plural": "Romanian lei"
	},
	"RSD": {
		"symbol": "din.",
		"name": "Serbian Dinar",
		"symbol_native": "дин.",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "RSD",
		"name_plural": "Serbian dinars"
	},
	"RUB": {
		"symbol": "RUB",
		"name": "Russian Ruble",
		"symbol_native": "₽.",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "RUB",
		"name_plural": "Russian rubles"
	},
	"RWF": {
		"symbol": "RWF",
		"name": "Rwandan Franc",
		"symbol_native": "FR",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "RWF",
		"name_plural": "Rwandan francs"
	},
	"SAR": {
		"symbol": "SR",
		"name": "Saudi Riyal",
		"symbol_native": "ر.س.‏",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "SAR",
		"name_plural": "Saudi riyals"
	},
	"SDG": {
		"symbol": "SDG",
		"name": "Sudanese Pound",
		"symbol_native": "SDG",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "SDG",
		"name_plural": "Sudanese pounds"
	},
	"SEK": {
		"symbol": "Skr",
		"name": "Swedish Krona",
		"symbol_native": "kr",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "SEK",
		"name_plural": "Swedish kronor"
	},
	"SGD": {
		"symbol": "S$",
		"name": "Singapore Dollar",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "SGD",
		"name_plural": "Singapore dollars"
	},
	"SOS": {
		"symbol": "Ssh",
		"name": "Somali Shilling",
		"symbol_native": "Ssh",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "SOS",
		"name_plural": "Somali shillings"
	},
	"SYP": {
		"symbol": "SY£",
		"name": "Syrian Pound",
		"symbol_native": "ل.س.‏",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "SYP",
		"name_plural": "Syrian pounds"
	},
	"THB": {
		"symbol": "฿",
		"name": "Thai Baht",
		"symbol_native": "฿",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "THB",
		"name_plural": "Thai baht"
	},
	"TND": {
		"symbol": "DT",
		"name": "Tunisian Dinar",
		"symbol_native": "د.ت.‏",
		"decimal_digits": 3,
		"rounding": 0,
		"code": "TND",
		"name_plural": "Tunisian dinars"
	},
	"TOP": {
		"symbol": "T$",
		"name": "Tongan Paʻanga",
		"symbol_native": "T$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "TOP",
		"name_plural": "Tongan paʻanga"
	},
	"TRY": {
		"symbol": "TL",
		"name": "Turkish Lira",
		"symbol_native": "TL",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "TRY",
		"name_plural": "Turkish Lira"
	},
	"TTD": {
		"symbol": "TT$",
		"name": "Trinidad and Tobago Dollar",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "TTD",
		"name_plural": "Trinidad and Tobago dollars"
	},
	"TWD": {
		"symbol": "NT$",
		"name": "New Taiwan Dollar",
		"symbol_native": "NT$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "TWD",
		"name_plural": "New Taiwan dollars"
	},
	"TZS": {
		"symbol": "TSh",
		"name": "Tanzanian Shilling",
		"symbol_native": "TSh",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "TZS",
		"name_plural": "Tanzanian shillings"
	},
	"UAH": {
		"symbol": "₴",
		"name": "Ukrainian Hryvnia",
		"symbol_native": "₴",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "UAH",
		"name_plural": "Ukrainian hryvnias"
	},
	"UGX": {
		"symbol": "USh",
		"name": "Ugandan Shilling",
		"symbol_native": "USh",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "UGX",
		"name_plural": "Ugandan shillings"
	},
	"UYU": {
		"symbol": "$U",
		"name": "Uruguayan Peso",
		"symbol_native": "$",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "UYU",
		"name_plural": "Uruguayan pesos"
	},
	"UZS": {
		"symbol": "UZS",
		"name": "Uzbekistan Som",
		"symbol_native": "UZS",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "UZS",
		"name_plural": "Uzbekistan som"
	},
	"VEF": {
		"symbol": "Bs.F.",
		"name": "Venezuelan Bolívar",
		"symbol_native": "Bs.F.",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "VEF",
		"name_plural": "Venezuelan bolívars"
	},
	"VND": {
		"symbol": "₫",
		"name": "Vietnamese Dong",
		"symbol_native": "₫",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "VND",
		"name_plural": "Vietnamese dong"
	},
	"XAF": {
		"symbol": "FCFA",
		"name": "CFA Franc BEAC",
		"symbol_native": "FCFA",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "XAF",
		"name_plural": "CFA francs BEAC"
	},
	"XOF": {
		"symbol": "CFA",
		"name": "CFA Franc BCEAO",
		"symbol_native": "CFA",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "XOF",
		"name_plural": "CFA francs BCEAO"
	},
	"YER": {
		"symbol": "YR",
		"name": "Yemeni Rial",
		"symbol_native": "ر.ي.‏",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "YER",
		"name_plural": "Yemeni rials"
	},
	"ZAR": {
		"symbol": "R",
		"name": "South African Rand",
		"symbol_native": "R",
		"decimal_digits": 2,
		"rounding": 0,
		"code": "ZAR",
		"name_plural": "South African rand"
	},
	"ZMK": {
		"symbol": "ZK",
		"name": "Zambian Kwacha",
		"symbol_native": "ZK",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "ZMK",
		"name_plural": "Zambian kwachas"
	},
	"ZWL": {
		"symbol": "ZWL$",
		"name": "Zimbabwean Dollar",
		"symbol_native": "ZWL$",
		"decimal_digits": 0,
		"rounding": 0,
		"code": "ZWL",
		"name_plural": "Zimbabwean Dollar"
	}
}

