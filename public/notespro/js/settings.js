var replaceNotesField;
var settingMap;
var loadingId = 0;

document.addEventListener("DOMContentLoaded", function(event) {

	document.getElementById("loader").style.display = "none";
	document.getElementById("contentDiv").style.display = "block";
	document.getElementById("google_translate_element").style.display = "block";

	ZOHO.embeddedApp.init().then(function(){
		initialise();
	})
});

function showProcess(text, id)
{
    $(".process-window-outer").show();
    $("#process-window-items").append(`<div id="process-item-${id}" class="process-window-item">${text}</div>`);
}

function processCompleted(id)
{
    $(`#process-item-${id}`).remove();
    if(($("#process-window-items").children().length) === 0){
        $(".process-window-outer").hide();
    }
}

function initialise()
{
	var currentLoadingId1 = loadingId++;
	showProcess("Fetching configurations",currentLoadingId1)
	ZOHO.CRM.API.getOrgVariable("zohocrmrichtextfield__settingMap").then(function(response){
		// var response = {"Success":{"Content":"{\"replaceNotesField\":true}"}};
		processCompleted(currentLoadingId1);
		settingMap = JSON.parse(response.Success.Content);
		replaceNotesField = settingMap.replaceNotesField;
		if(replaceNotesField)
		{
			$("#replace-notes-field-toggle")[0].checked = true;
		}
		else
		{
			$("#replace-notes-field-toggle")[0].checked = false;
		}
	});
}

function replaceNotesFieldToggle()
{
	if($("#replace-notes-field-toggle")[0].checked)
	{
		replaceNotesField = true;
		settingMap.replaceNotesField = true;
	}
	else
	{
		replaceNotesField = false;
		settingMap.replaceNotesField = false;
	}
	var currentLoadingId2 = loadingId++;
	showProcess("Updating configurations",currentLoadingId2)
	ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "zohocrmrichtextfield__settingMap","value": JSON.stringify(settingMap)}).then(function(response){
		processCompleted(currentLoadingId2);
	})
}

function save()
{
	
}