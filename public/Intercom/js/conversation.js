// var emailContentText = "Hi ,\n\nHope you're well. I'm writing to know how our business can help you.\n\nPlease choose a event to schedule an appointment:\n\n${eventsUrls} See you soon!\n\n %ownerName% \n\n %companyName%";
    var recordId;
    var recordModule;
    var ButtonPosition;
    var accessKey;
    var channelId;
    var existingConversations = [];
    var phoneFields =[];
    var platform = "";
    var convId;
    var intercomId;
    var adminId;
    var currentRecord;
     var RTCs = {
                    
                };
    document.addEventListener("DOMContentLoaded", function(event) {
      $('#existingConvListWindow').append(`<div class="processingFull"> Fetching conversations...</div>`);
        ZOHO.embeddedApp.on("PageLoad", function(record) {
            addWebhook();
            recordId = record.EntityId;
            recordModule = record.Entity;
            document.getElementById("intercomupdate").innerText = "Update "+ recordModule.substring(0,recordModule.length-1)+" from Intercom";
            ZOHO.CRM.CONNECTOR.invokeAPI("intercomforzohocrm.intercomconnector.getadmins",{}).then(function(resp){

              if(resp.status_code == 200){
                 var admins = JSON.parse(resp.response).admins;
                 adminId = admins[0].id;
              }
            });
            ZOHO.CRM.API.getRecord({Entity:record.Entity,RecordID:record.EntityId}).then(function(data){
               console.log(data.data[0].intercomforzohocrm__IntercomId);
               currentRecord = data.data[0];
               if(data.data[0].intercomforzohocrm__IntercomId != null){
                  intercomId = data.data[0].intercomforzohocrm__IntercomId;
                  var dynamicMap = {"query":{"query":{"field":"contact_ids","operator":"=","value":data.data[0].intercomforzohocrm__IntercomId}}};
                  ZOHO.CRM.CONNECTOR.invokeAPI("intercomforzohocrm.intercomconnector.getallconversations",dynamicMap)
                  .then(function(resp){
                       console.log(resp);
                        if(resp.status_code == 200){
                            if(JSON.parse(resp.response).total_count > 0){
                              var conversations = JSON.parse(resp.response).conversations;
                              existingConversations =conversations;
                              $(".processingFull").remove();
                              renderConversationList();
                                      // for(let i=0;i<conversations.length;i++){
                                      //   if(conversations[i].source.delivered_as == "customer_initiated"){
                                      //      messages =messages+ '<div class="conversation" onclick="showConversation(this)" id="'+conversations[i].id+'"> <span class="incoming" id="'+conversations[i].source.id+'" ></span></div>'
                                      //   }
                                      //   else{
                                      //     messages = messages+'<div class="conversation" onclick="showConversation(this)" id="'+conversations[i].id+'"><span class="outgoing" id="'+conversations[i].source.id+'" ></span>'
                                      //   }
                                      // }
                                      // $('#conversations').append(messages);
                                      // if(messages == ""){
                                      //   $('#conversations').append('<li style="text-align:center;">No Conversations</li>');
                                      // }
                                      // else{
                                      //   for(let i=0;i <conversations.length;i++){
                                      //     document.getElementById(conversations[i].source.id).innerHTML = conversations[i].source.body;
                                      //   }
                                      // }
                              
                            }
                            else{
                                 $(".processingFull").remove();
                            }

                        }
                        else{
                             $(".processingFull").remove();
                        }
                  });
                } 
                else{
                    $(".processingFull").remove();

                         
                } 
            });
            
      });
      ZOHO.embeddedApp.init();
    });
    const urlParams = new URLSearchParams(window.location.search);
    var serviceOrigin = urlParams.get('serviceOrigin');
    async function addWebhook(){
        try{
            if(localStorage.getItem("intercomWebHookAdded") == "true"){
                return;
            }    
            var domain="";
            var getmap = {"nameSpace":"<portal_name.extension_namespace>"};
            var resp = await ZOHO.CRM.CONNECTOR.invokeAPI("crm.zapikey",getmap);
            var zapikey = JSON.parse(resp).response;
            var domain = "com";
            if(serviceOrigin.indexOf(".zoho.") != -1){
                domain = serviceOrigin.substring(serviceOrigin.indexOf(".zoho.")+6)
            }
            var webHookurl = "https://platform.zoho."+domain+"/crm/v2/functions/intercomforzohocrm__intercomwebhook/actions/execute?auth_type=apikey&zapikey="+zapikey
            if(serviceOrigin.indexOf(".zohosandbox.") != -1){
                webHookurl = "https://plugin-intercomforzohocrm.zohosandbox.com/crm/v2/functions/intercomforzohocrm__intercomwebhook/actions/execute?auth_type=apikey&zapikey=1001.7a90efe1ea0445bc84ca7d4a9df54700.78606eb968a8fbaf2410f4bd262a09cc";
            }
            var orgInfo = await ZOHO.CRM.CONFIG.getOrgInfo();
            var orgId = orgInfo.org[0].zgid;
            var adminResp = await ZOHO.CRM.CONNECTOR.invokeAPI("intercomforzohocrm.intercomconnector.getme",{})

            if(adminResp.status_code == 200){
                var orgDetails = JSON.parse(adminResp.response);
                var appId = orgDetails.app.id_code;
                var request = {
                    "url":"https://us-central1-ulgebra-license.cloudfunctions.net/intercomWebhook",
                    "body":{
                        "action":"addWebhook",
                        "CRM_ORG_ID":orgId,
                        "ACCOUNT_UNIQUE_ID":appId,
                        "CRM_API_URL":webHookurl
                    },
                    "headers":{"Content-Type":"application/json"}
                }
                var response = await ZOHO.CRM.HTTP.post(request);
                if(response == "ok"){
                    localStorage.setItem("intercomWebHookAdded",true)
                }
                console.log(response);

            } 
        }
        catch(e){
            console.log(e);
        }       
    }
    window.onload = function() {
        addScript('https://cdn.firebase.com/js/client/2.2.1/firebase.js');
        addScript('https://www.gstatic.com/firebasejs/7.14.1/firebase-app.js');
        addScript('https://www.gstatic.com/firebasejs/7.14.1/firebase-auth.js');
        addScript('https://www.gstatic.com/firebasejs/7.14.1/firebase-firestore.js');
        addScript('https://www.gstatic.com/firebasejs/7.14.1/firebase-database.js');
        $(".chatmessages-inner").on('click', (function(){
            $('.chatmessage-item.unread').removeClass('unread');
        }));
    };
          
    function addScript( src ) {
        var s = document.createElement( 'script' );
        s.setAttribute( 'src', src );
        document.body.appendChild( s );
    }


    function initiateRTSForConvs(){
        console.log("Trying to initialize initiateRTSForConvs");
        if(typeof firebase === undefined){
          console.log("FIREBASE not yet present..");
          setTimeout(initiateRTSForConvs, 2000);
        }
        else{
            console.log("FIREBASE loaded..");
            for(var item in existingConversations){
                var obj = existingConversations[item];
                initiateRTListeners(obj.id);
            }
        }
    }

    function initiateRTListeners(CID){
        if(Object.keys(RTCs).length === 0){
            var firebaseConfig = {
                apiKey: "AIzaSyAKy6MMaRs72wJvZ05DrPJv85lfY3ZE5no",
                authDomain: "intercomcrmapp.firebaseapp.com",
                databaseURL: "https://intercomcrmapp.firebaseio.com",
                projectId: "intercomcrmapp",
                storageBucket: "intercomcrmapp.appspot.com",
                messagingSenderId: "667227807773",
                appId: "1:667227807773:web:5e08c5de47a6c201ec66f4",
                measurementId: "G-G9SHLTD45L"
            };
            // Initialize Firebase
            firebase.initializeApp(firebaseConfig);
        }
        if(RTCs.hasOwnProperty(CID)){
              console.log("RTC already inited "+RTCs[CID]);
              return;
        }
        RTCs[CID] = new Date().getTime();
        var starCountRef = firebase.database().ref('cursors/'+CID+'/ts');
          starCountRef.on('value', function(snapshot) {
            newRTEvent(CID, snapshot.val());
        });
      }
    function newRTEvent(CID, MID){
       // addNewMessageToWindow(CID, MID);
        if(convId == CID){
            getConvs(0,true);
        }
    }      


    function showConversation(conversation){
      var dynamic_map = {"id":conversation.id};
      ZOHO.CRM.CONNECTOR.invokeAPI("intercomforzohocrm.intercomconnector.getsingleconversation",dynamic_map).then(function(resp){
        console.log(resp);
        var messages = "";
        if(resp.status_code == 200){
            var conversations = JSON.parse(resp.response).conversation_parts.conversation_parts;
            for(let i=0;i<conversations.length;i++){
              if(conversations[i].author.type == "user"){
                 messages =messages+ '<span class="incoming" id="'+conversations[i].id+'" onclick="showConversation(this)"></span>'
              }
              else{
                messages = messages+'<span class="outgoing" id="'+conversations[i].id+'" onclick="showConversation(this)"></span>'
              }
            }
            messages = messages+'<input type="text" class="outgoing"> </input><input type="submit" class="outgoing" onclick="reply('+conversation.id+')"> </input>';
             $('#'+conversation.id).append(messages);
            if(messages == ""){
              $('#'+conversation.id).append('<li style="text-align:center;">No Conversations</li>');
            }
            else{
              for(let i=0;i <conversations.length;i++){
                document.getElementById(conversations[i].id).innerHTML = conversations[i].body;
              }
            }
        }
        else{
          var ss = "try after sometime";
        }
      });  

    }
    function reply(conversationId){
      
      var dynamic_map = {"messageId":conversationId,"body":{"message_type":"comment","type":"admin","admin_id":adminId,"body":"reply for crm"}};
      ZOHO.CRM.CONNECTOR.invokeAPI("intercomforzohocrm.intercomconnector.replytoconversation",dynamic_map).then(function(resp){
             console.log(resp);
      });
    }

    function valueExists(val) {
        return val !== null && val !== undefined && val.length > 1 && val!=="null";
    }
            
    function getTimeString(time){
          let msgTime = Date.parse(time);
          let timeSuffix = "am";
          if(isNaN(msgTime) || time.indexOf('Z')<0){
            return time;
          }
          let msgHour = new Date(msgTime).getHours();
          timeSuffix = msgHour>12 ? "pm" : "am";
          msgHour = msgHour>12 ? msgHour-12 : msgHour;
          let msgMins = new Date(msgTime).getMinutes();
          msgMins = msgMins<10 ? "0"+msgMins : msgMins;
          return msgHour+":"+msgMins+" "+timeSuffix;
    }
          
    function renderConversationList(){
        initiateRTSForConvs();
        $("#prevConversations").html("");    
        for(var item in existingConversations){
            var obj = existingConversations[item];
            $("#prevConversations").prepend(`<div class="conv-list-item" id="list-${obj.id}" onclick="openConversation('${obj.id}')">
                    <div class="conv-channel">
                        ${obj.source.body}
                    </div>
                    <div class="conv-number">
                        ${obj.source.delivered_as}
                    </div>
                </div>`);
        }
    }
          
    function initiateMessage(){
        if(intercomId){
            var text = $("#input-sms-content").val().trim();
            if(text){
                $('#sms-form').append(`<div class="processingFull"> Sending message...</div>`);
                var dynamic_map = {"body":{"from": {"type":"contact","id":intercomId},"body":text}};
                ZOHO.CRM.CONNECTOR.invokeAPI("intercomforzohocrm.intercomconnector.createconversation",dynamic_map).then(function(resp){
                    console.log(resp);
                    $("#input-sms-content").val("");
                    if(resp.status_code == 200){
                        setTimeout((()=>{getConversations(JSON.parse(resp.response).id);}), 2000);
                    }  
                });  
            }  
            else{
              $('.init-box').append(`<div style="background-color:red;" class="loadingdiv"> Message cannot be empty</div>`);
              setTimeout((()=>{ $(".loadingdiv").remove();}), 1500);
             
            }    
        }  
        else{
            addContactToIntercom(true);
        }  

    }
    function updateFromIntercom(){
        if(intercomId){
            var dynamicMap = {"id":intercomId};
            document.getElementById("intercomupdate").innerText = "updating...";
            document.getElementById("intercomupdate").style.cursor="none";
            ZOHO.CRM.CONNECTOR.invokeAPI("intercomforzohocrm.intercomconnector.getcontact",dynamicMap).then(function(resp){
                if(resp.status_code == 200){
                    var intercomData = JSON.parse(resp.response);
                    if(intercomData.companies != null && intercomData.companies.data.length){
                        var companyMap={"companyId":intercomData.companies.data[0].id};
                        var companyApi = ZOHO.CRM.CONNECTOR.invokeAPI("intercomforzohocrm.intercomconnector.getcompany",companyMap);
                    }
                    Promise.all([ZOHO.CRM.API.searchRecord({Entity:"intercomforzohocrm__Intercom_Apps",Type:"criteria",Query:"(intercomforzohocrm__Trigger:equals:" + intercomData.type
     + ".created)",delay:false}),companyApi]).then(function(resp2){
                        if(resp2[0].data && resp2[0].data.length){
                            var req_data= {"id":recordId,"intercomforzohocrm__IntercomId":intercomId};
                            settings = JSON.parse(resp2[0].data[0].intercomforzohocrm__Settings);
                            fieldsMap = settings.fieldsMap;
                            if(resp2[1] && resp2[1].status_code == 200){
                                var company= JSON.parse(resp2[1].response);
                            }
                            Object.keys(fieldsMap).forEach(function(key){
                                var intercomKey = fieldsMap[key];
                                var value;
                                if(intercomKey.indexOf(".") != -1){
                                    var mapkey = intercomKey.substring(0,intercomKey.indexOf("."));
                                    if(mapkey  == "company" && company){
                                        value = company[intercomKey.substring(intercomKey.indexOf(".")+1)];
                                    }
                                    else if(intercomData[mapkey] != null){
                                        var keyMap =  intercomData[mapkey];
                                        value = keyMap[intercomKey.substring(intercomKey.indexOf(".")+1)];
                                    }
                                    else if(intercomKey.indexOf("_") != -1){
                                        mapkey = mapkey.substring(0,mapkey.indexOf("_"));
                                        if(intercomData[mapkey] != null){
                                            var keyMap =intercomData[mapkey];
                                            var valuekey = intercomKey.substring(intercomKey.indexOf(".")+1);
                                            valuekey = valuekey.substring(0,valuekey.indexOf("_"));
                                            value = keyMap[valuekey];
                                        }    
                                    }
                                }
                                else{
                                   value = intercomData[intercomKey];
                                }
                                if(value != null){
                                    req_data[key]=value;
                                }
                            });
                            ZOHO.CRM.API.updateRecord({Entity:recordModule,APIData:req_data,Trigger:["workflow"]}).then(function(response){
                                 document.getElementById("intercomupdate").innerText = "Updated";
                                 document.getElementById("intercomupdate").style.cursor="pointer";
                                 ZOHO.CRM.UI.Record.open({Entity:recordModule,RecordID:recordId});
                            });  
                        }
                        else{
                            document.getElementById("intercomupdate").innerText = "Update Lead from Intercom";
                            document.getElementById("intercomupdate").style.cursor="pointer";
                        }        
                    });    
                } 
                else{
                    document.getElementById("intercomupdate").innerText = "Update Lead from Intercom";
                    document.getElementById("intercomupdate").style.cursor="pointer";
                }    
            });
        }        
    }
          
    function addContactToIntercom(isInitiateMessage){
        if(currentRecord.Email != null){
            // var dynamicMap = {"body":{"query":{"operator":"OR","value":[{"field":"email","operator":"=","value":currentRecord.Email},{"field":"phone","operator":"=","value":currentRecord.Phone}]}}};
            var dynamicMap = {"body":{"query":{"field":"email","operator":"=","value":currentRecord.Email}}};
            ZOHO.CRM.CONNECTOR.invokeAPI("intercomforzohocrm.intercomconnector.searchcontacts",dynamicMap).then(function(resp){
               console.log(resp);
                if(resp.status_code == 200){
                    if(JSON.parse(resp.response).total_count == 0 && currentRecord.Email != null && currentRecord.Email != ""){
                        var dynamic_map2 = {"body":{"role":"lead","email":currentRecord.Email,"phone":currentRecord.Phone,"name":currentRecord.Full_Name}}
                        ZOHO.CRM.CONNECTOR.invokeAPI("intercomforzohocrm.intercomconnector.createcontact",dynamic_map2).then(function(contactResp){
                            if(contactResp.status_code == 200){
                                if(JSON.parse(contactResp.response) && JSON.parse(contactResp.response).id){
                                    intercomId = JSON.parse(contactResp.response).id;
                                    var req_data= {"id":recordId,"intercomforzohocrm__IntercomId":intercomId};
                                    ZOHO.CRM.API.updateRecord({Entity:recordModule,APIData:req_data,Trigger:["workflow"]}).then(function(response){
                                        if(isInitiateMessage){
                                            initiateMessage();
                                        }    
                                    });   
                                }
                            }        
                        });
                    } 
                    else if(JSON.parse(resp.response).total_count > 0){
                        intercomId = JSON.parse(resp.response).data[0].id;
                        var req_data= {"id":recordId,"intercomforzohocrm__IntercomId":intercomId};
                        ZOHO.CRM.API.updateRecord({Entity:recordModule,APIData:req_data,Trigger:["workflow"]}).then(function(response){
                            if(isInitiateMessage){
                                initiateMessage();
                            }    
                        });   
                    } 
                }
            });     
        }    
    }
    function getConversations(sourceId){
          var dynamicMap = {"query":{"query":{"field":"contact_ids","operator":"=","value":intercomId}}};
          ZOHO.CRM.CONNECTOR.invokeAPI("intercomforzohocrm.intercomconnector.getallconversations",dynamicMap).then(function(resp){
              if(resp.status_code == 200){
                if(JSON.parse(resp.response).total_count > 0){
                  existingConversations =JSON.parse(resp.response).conversations;
                  renderConversationList();
                  if(sourceId == existingConversations[0].source.id){
                      openConversation(existingConversations[0].id);
                       $(".processingFull").remove();
                  }
                  else{
                     setTimeout((()=>{getConversations(JSON.parse(resp.response).id);}), 2000);
                  }
                 
                }
              }
          });        
    } 
    function getSafeString(htmlString){
      return $('<div>').text(htmlString).html();
    }        
    function sendMessage(){
          var text = $("#main-message-text").val().trim();
          text = getSafeString(text);
          if(text){
              $('.chatbox-outer').append(`<div class="loadingdiv"> Sending message...</div>`);
              var dynamic_map = {"messageId":convId,"body":{"message_type":"comment","type":"admin","admin_id":adminId,"body":text}};
              ZOHO.CRM.CONNECTOR.invokeAPI("intercomforzohocrm.intercomconnector.replytoconversation",dynamic_map).then(function(resp){
                  $("#main-message-text").val("");
                  $(".loadingdiv").text('Message Sent, Checking Status...');
                  renderMessages(resp);
              });
            }
            else{
              $('.chatbox-outer').append(`<div style="background-color:red;" class="loadingdiv"> Message cannot be empty</div>`);
              setTimeout((()=>{ $(".loadingdiv").remove();}), 1500);
             
            }    
          
    }
          
    function escapeHTMLS(rawStr){
        return $('<textarea/>').text(rawStr).html();
    }


          //      var dynamic_map = {"id":conversation.id};
          // ZOHO.CRM.CONNECTOR.invokeAPI("intercomforzohocrm.intercomconnector.getsingleconversation",dynamic_map).then(function(resp){
          //   console.log(resp);
          //   var messages = "";
          //   if(resp.status_code == 200){
          //       var conversations = JSON.parse(resp.response).conversation_parts.conversation_parts;
          //       for(let i=0;i<conversations.length;i++){
          //         if(conversations[i].author.type == "user"){
          //            messages =messages+ '<span class="incoming" id="'+conversations[i].id+'" onclick="showConversation(this)"></span>'
          //         }
          //         else{
          //           messages = messages+'<span class="outgoing" id="'+conversations[i].id+'" onclick="showConversation(this)"></span>'
          //         }
          //       }
          //       messages = messages+'<input type="text" class="outgoing"> </input><input type="submit" class="outgoing" onclick="reply('+conversation.id+')"> </input>';
          //        $('#'+conversation.id).append(messages);
          //       if(messages == ""){
          //         $('#'+conversation.id).append('<li style="text-align:center;">No Conversations</li>');
          //       }
          //       else{
          //         for(let i=0;i <conversations.length;i++){
          //           document.getElementById(conversations[i].id).innerHTML = conversations[i].body;
          //         }
          //       }
          //   }
          //   else{
          //     var ss = "try after sometime";
          //   }
          // });  
          function getConvs(offset,isNotLoading){
              $("#initiateConvWindow").hide();
//              $('#existingConvListWindow').show();
              $("#existingConvWindow").show();
              var limit = 20;
              if(offset!==0){
                  $(`#loadmore-off-${offset}`).html('Loading...');
              }
              if(!isNotLoading && offset === 0){
                $('.chatbox-outer').append(`<div class="loadingdiv"> Loading Messages...</div>`);
              }
              if(!isNotLoading){
                 $('.chatmessages-inner').html("");
              }   
              var hasMoreMsgs = true;
              var dynamic_map = {"id":convId};
              ZOHO.CRM.CONNECTOR.invokeAPI("intercomforzohocrm.intercomconnector.getsingleconversation",dynamic_map).then(function(resp){
                renderMessages(resp);
              });
          }
          function renderMessages(resp){
            $('.chatmessages-inner').html("");
            $(".loadingdiv").remove();
            if(resp.status_code == 200){
              var response=JSON.parse(resp.response);
              var conversations = response.conversation_parts.conversation_parts;
              for(var i=conversations.length-1; i>=0;i--){
                  var msgObj = conversations[i];
                  // var msgType = msgObj.type;
                  var content = escapeHTMLS(msgObj.body ? msgObj.body : "");
                  var mediaHTML = "";
                  // if(msgType!=="text"){
                  //     if(msgType==="location"){
                  //         content = "http://maps.google.com/?ll="+msgObj.content[msgType].latitude+","+msgObj.content[msgType].longitude;
                  //     }else{
                  //         if(msgType === "image"){
                  //             mediaHTML = `<img src="${msgObj.content[msgType].url}"/>`;
                  //         }
                  //         else if(msgType === "video"){
                  //             mediaHTML = `<video controls src="${msgObj.content[msgType].url}"/>`;
                  //         }
                  //         else if(msgType === "audio"){
                  //             mediaHTML = `<audio controls src="${msgObj.content[msgType].url}"/>`;
                  //         }
                  //         else if(msgType === "file"){
                  //             mediaHTML = `<div class="msg-attch"><span class="material-icons">attachment</span> <a target="_blank" href="${msgObj.content[msgType].url}">Click to download file</a></div>`;
                  //         }
                  //     }
                  // }
                  var inComing = msgObj.author.type == "user";
                  if(content){
                    $('.chatmessages-inner').prepend(`<div class="chatmessage-item ${inComing ? 'direction-in' :'direction-out' }">
                      <div class="chatmessage-inner">
                          <div class="chatmessage-content" id="${msgObj.id}">
                          </div>
                          <div class="chatmessage-time" title="${new Date(msgObj.updated_at).toLocaleString()}">
                              <span class="smallTime">${new Date(msgObj.updated_at).toLocaleString()}</span>
                          </div>
                      </div>
                    </div>`);
                  }  
              }
              for(var i=conversations.length-1; i>=0;i--){
                 if(conversations[i].body && document.getElementById(conversations[i].id)){
                    document.getElementById(conversations[i].id).innerHTML = conversations[i].body;
                 }   
              }  
              var inComing = response.source.delivered_as === "customer_initiated";
              $('.chatmessages-inner').prepend(`<div class="chatmessage-item ${inComing ? 'direction-in' :'direction-out' }">
                <div class="chatmessage-inner">
                    <div class="chatmessage-content" id="${response.source.id}">
                    </div>
                    <div class="chatmessage-time" title="${new Date(response.created_at).toLocaleString()}">
                        <span class="smallTime">${new Date(response.created_at).toLocaleString()}</span>
                    </div>
                </div>
              </div>`);
              document.getElementById(response.source.id).innerHTML = response.source.body;  
              $(".chatmessages-inner").scrollTop($(".chatmessages-inner")[0].scrollHeight);
            }
          }
           function onTestChange(e,ele) {
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13) { //Enter keycode
                       // if(ele.id == "main-message-text"){
                       //   initiateMessage();
                       // }
                       // else{
                          sendMessage();
                       // }
                    return false;
                }
                else {
                    return true;
                }
            }
          function openConversation(conversationId){
              convId = conversationId;
              $(".conv-list-item").removeClass('selected');
              $("#list-"+conversationId).addClass('selected');
              $('#existingConvListWindow').hide();
              getConvs(0);
          }
