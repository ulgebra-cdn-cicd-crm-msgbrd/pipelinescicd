    // var intercomFieldsMap ={"id":"Intercom Id","name":"Lead Name","app_id":"App Id","email":"Lead Email","phone":"Lead Phone","unsubscribed_from_emails":"Lead Unsubscribed From Emails","marked_email_as_spam":"Lead Marked Email As Spam","last_seen_ip":"Lead Last Seen IP Address","location_data.city_name":"Lead City","location_data.continent_code":"Lead Continent Code","location_data.country_code":"Lead Country Code","location_data.country_name":"Lead Country Name","location_data.latitude":"Lead Latitude","location_data.longitude":"Lead Longitude","location_data.postal_code":"Lead Postal Code","location_data.region_name":"Lead Region","location_data.timezone":"Lead TimeZone","avatar.type":"Avatar Type","avatar.image_url":"Avatar Image Url","pseudonym":"Lead Pseudonym","anonymous":"Lead Anonymous","delivery_attempts":"Delivery Attempts","delivery_status":"Delivery Status","first_sent_at":"First Seen At","delivered_at":"Delivered At"};
    var intercomFieldsMap ={"name":"Lead Name","email":"Lead Email","phone":"Lead Phone","unsubscribed_from_emails":"Lead Unsubscribed From Emails","marked_email_as_spam":"Lead Marked Email As Spam","last_seen_ip":"Lead Last Seen IP Address","location_data.city_name":"Lead City","location_data.continent_code":"Lead Continent Code","location_data.country_code":"Lead Country Code","location_data.country_name":"Lead Country Name","location_data.latitude":"Lead Latitude","location_data.longitude":"Lead Longitude","location_data.postal_code":"Lead Postal Code","location_data.region_name":"Lead Region","location_data.timezone":"Lead TimeZone","avatar.type":"Avatar Type","avatar.image_url":"Avatar Image Url","pseudonym":"Lead Pseudonym","anonymous":"Lead Anonymous","delivery_attempts":"Delivery Attempts","delivery_status":"Delivery Status","company.name":"Company Name","company.size":"Company Size","company.website":"Company Website","company.industry":"Company Industry"};
    var ContactFields=[];
    var LeadFields=[];
    var settings =[];
    var editableSettings=[];
    document.addEventListener("DOMContentLoaded", function(event) {
        ZOHO.embeddedApp.init().then(function(){
            addWebhook();
            ZOHO.CRM.CONFIG.getCurrentUser().then(function(user){
                if(user.users[0].profile.name != "Administrator"){
                    document.getElementById("ErrorText").innerText = "You do not have permissions for this webtab";
                    document.getElementById("Error").style.display= "block";
                    return;
                }
                ZOHO.CRM.API.getAllRecords({Entity:"intercomforzohocrm__Intercom_Apps",sort_order:"asc"}).then(function(data){
                    Promise.all([ZOHO.CRM.META.getFields({"Entity":"Contacts"}),ZOHO.CRM.META.getFields({"Entity":"Leads"})]).then(function(metaData){
                        ContactFields = metaData[0].fields;
                        LeadFields = metaData[1].fields;
                        if(data.data && data.data.length > 0){
                            settings = data.data;
                            editableSettings = JSON.parse(JSON.stringify(data.data));
                            $("#settingsContainer").append(`<div class="appContainer">
                                                                <div class="triggersetting"><h3>Tigger</h3></div>
                                                                <div class="triggersetting"><h3>Enable/Disable</h3></div>
                                                                <div class="triggersetting"><h3>SalesSignal Notification</h3></div>
                                                                <div class="triggersetting" style="width:35%"><h3>Fields Mapping</h3></div>
                                                            </div>`);
                            // settingsList='<div class="appContainer" ><div class="triggersetting"><h3>Tigger</h3></div><div class="triggersetting"><h3>Enable/Disable</h3></div><div class="triggersetting"><h3>SalesSignal Notification</h3></div><div class="triggersetting" style="width:35%"><h3>Fields Mapping</h3></div></div>';
                            for (let i =0; i<settings.length ; i++) {
                                var triggerSetting =JSON.parse(settings[i].intercomforzohocrm__Settings);
                                var fieldsMap=triggerSetting.fieldsMap?triggerSetting.fieldsMap:{};
                                var firstField='';
                                var fieldsMapUi='';
                                var fieldsMapUi=`<div class="eventFieldMap"><div class="eventname collapsible" id="${settings[i].id}_name" onclick="toggleFieldMap('${settings[i].id}')">Configure Fields Mapping for ${settings[i].Name} trigger</div><div class="fieldMapperContainer" id="${settings[i].id}_fieldsMap"></div></div>`;
                               
                                $("#settingsContainer").append(`<div class="appContainer" id="${settings[i].id}">
                                                                    <div class="triggersetting" >${settings[i].Name}</div>
                                                                    <div class="triggersetting">
                                                                        <label class="switch"><input type="checkbox" onchange="updateOrgVariables('isEnable',this)"><span class="slider round"></span></label>
                                                                    </div>
                                                                    <div class="triggersetting">
                                                                        <label class="switch"><input type="checkbox" onchange="updateOrgVariables('salesSignal',this)" ><span class="slider round"></span></label>
                                                                    </div>
                                                                    <div class="triggersetting" style="width:35%">
                                                                        ${fieldsMapUi}
                                                                    </div>  
                                                                </div>`);
                                // loadSettings(settings[i]);
                            }
                            for (let i =0; i<settings.length ; i++) {
                                var setting = JSON.parse(settings[i].intercomforzohocrm__Settings);
                                // if(document.getElementById(setting.module+"_"+settings[i].id)){
                                //     // $("#"+setting.module+"_"+settings[i].id).prop("checked", true);
                                //     document.getElementById(setting.module+"_"+settings[i].id).checked = true;
                                // }
                                document.getElementById(settings[i].id).children[1].children[0].children[0].checked = setting.isEnable;
                                document.getElementById(settings[i].id).children[2].children[0].children[0].checked = setting.salesSignal;
                                if(settings[i].intercomforzohocrm__Trigger == "user.deleted"){
                                    document.getElementById(settings[i].id).children[2].children[0].children[0].checked = false; 
                                    document.getElementById(settings[i].id).children[2].children[0].children[0].disabled = true;
                                }
                            }
                        }
                    });    
                });
            });
        });
        
    });

    const urlParams = new URLSearchParams(window.location.search);
    var serviceOrigin = urlParams.get('serviceOrigin');
    async function addWebhook(){
        try{
            if(localStorage.getItem("intercomWebHookAdded") == "true"){
                return;
            }    
            var domain="";
            var getmap = {"nameSpace":"<portal_name.extension_namespace>"};
            var resp = await ZOHO.CRM.CONNECTOR.invokeAPI("crm.zapikey",getmap);
            var zapikey = JSON.parse(resp).response;
            var domain = "com";
            if(serviceOrigin.indexOf(".zoho.") != -1){
                domain = serviceOrigin.substring(serviceOrigin.indexOf(".zoho.")+6)
            }
            var webHookurl = "https://platform.zoho."+domain+"/crm/v2/functions/intercomforzohocrm__intercomwebhook/actions/execute?auth_type=apikey&zapikey="+zapikey
            if(serviceOrigin.indexOf(".zohosandbox.") != -1){
                webHookurl = "https://plugin-intercomforzohocrm.zohosandbox.com/crm/v2/functions/intercomforzohocrm__intercomwebhook/actions/execute?auth_type=apikey&zapikey=1001.7a90efe1ea0445bc84ca7d4a9df54700.78606eb968a8fbaf2410f4bd262a09cc";
            }
            var orgInfo = await ZOHO.CRM.CONFIG.getOrgInfo();
            var orgId = orgInfo.org[0].zgid;
            var adminResp = await ZOHO.CRM.CONNECTOR.invokeAPI("intercomforzohocrm.intercomconnector.getme",{})

            if(adminResp.status_code == 200){
                var orgDetails = JSON.parse(adminResp.response);
                var appId = orgDetails.app.id_code;
                var request = {
                    "url":"https://us-central1-ulgebra-license.cloudfunctions.net/intercomWebhook",
                    "body":{
                        "action":"addWebhook",
                        "CRM_ORG_ID":orgId,
                        "ACCOUNT_UNIQUE_ID":appId,
                        "CRM_API_URL":webHookurl
                    },
                    "headers":{"Content-Type":"application/json"}
                }
                var response = await ZOHO.CRM.HTTP.post(request);
                if(response == "ok"){
                    localStorage.setItem("intercomWebHookAdded",true)
                }
                console.log(response);

            } 
        }
        catch(e){
            console.log(e);
        }       
    }
    function saveAppSetting(settingId){
        var selectedApp;
        editableSettings.forEach(function(setting){
            if(setting.id == settingId){
                selectedApp= setting;
            }
        });
        document.getElementById("ErrorText").innerText = "Saving...";
        document.getElementById("Error").style.display= "block";
        ZOHO.CRM.API.updateRecord({Entity:"intercomforzohocrm__Intercom_Apps",APIData:selectedApp,Trigger:["workflow"]}).then(function(response){
            settings =JSON.parse(JSON.stringify(editableSettings));
            document.getElementById("ErrorText").innerText = "Saved";
            document.getElementById("Error").style.display = "none";
        }); 
    } 
    function updateOrgVariables(fieldName,element){
        var triggerSetting ="";
        var settingId = element.parentElement.parentElement.parentElement;
        settingId = settingId.id? settingId.id:settingId.parentElement.id.slice(0,-10);
        var selectedApp;
        if(fieldName == "isEnable" || fieldName == "salesSignal"){
            settings.forEach(function(setting){
                if(setting.id == settingId){
                    selectedApp= setting;
                    triggerSetting =JSON.parse(setting.intercomforzohocrm__Settings);
                }
            });
            triggerSetting[fieldName] = element.checked;
            selectedApp.intercomforzohocrm__Settings=JSON.stringify(triggerSetting);
            ZOHO.CRM.API.updateRecord({Entity:"intercomforzohocrm__Intercom_Apps",APIData:selectedApp,Trigger:["workflow"]}).then(function(response){
            });
        }
        else{
            editableSettings.forEach(function(setting){
                if(setting.id == settingId){
                    selectedApp= setting;
                    triggerSetting =JSON.parse(setting.intercomforzohocrm__Settings);
                }
            });
            if(fieldName == "module"){
                triggerSetting[fieldName] = element.value;
                selectedApp.intercomforzohocrm__Settings=JSON.stringify(triggerSetting);
                loadSettings(selectedApp);
            }
            else if(fieldName == "fieldsMap"){
                var fieldMapElement = element.parentElement.parentElement.parentElement;
                var key =fieldMapElement.children[0].children[0].children[0].value;
                var value = fieldMapElement.children[2].children[0].children[0].value;
                if(key != "Select" && value != "Select"){ 
                     delete triggerSetting.fieldsMap[$(element).data('pre')];
                    triggerSetting.fieldsMap[key] =value;
                    selectedApp.intercomforzohocrm__Settings=JSON.stringify(triggerSetting);
                }   
            }
        }    
    }

    function getFieldsOptions(fieldMap,module,key){
        var options='';
        if(module == ""){
            if(!key){
                  options='<option value="Select">Select</option>';
            }
            Object.keys(intercomFieldsMap).forEach(function(field){
                if(field != key){
                    options = `${options}<option value="${field}">${intercomFieldsMap[field]}</option>`;
                }
            });
        }
        else 
        {
            var fields =[];
            if(module == "Contacts"){
                fields = ContactFields;
            }
            else if(module == "Leads"){
                fields = LeadFields;
            } 
            var currentField ="";
            fields.forEach(function(field){
                if(field.api_name == key){
                    currentField =`<option value="${field.api_name}">${field.field_label}</option>`;
                }
                if(Object.keys(fieldMap).indexOf(field.api_name) == -1){
                    options = `${options}<option value="${field.api_name}">${field.field_label}</option>`;
                }
            });
            if(currentField){
                options=currentField+options;
            }
            else{
                options='<option value="Select">Select</option>'+options;
            }
        }
        return options;
    }
    function loadSettings(setting){
        var settingId = setting.id;
        triggerSetting =JSON.parse(setting.intercomforzohocrm__Settings);
        var fieldsMap=triggerSetting.fieldsMap?triggerSetting.fieldsMap:{};
        var firstField='';
        var fieldsMapUi='';
        Object.keys(fieldsMap).forEach(function(key){
            var options =getFieldsOptions(fieldsMap,triggerSetting.module,key);
            var intercomFields =getFieldsOptions(intercomFieldsMap,"",fieldsMap[key]);
            if(key == "Last_Name"){
                firstField=`<div class="fieldMapp">
                                <div class="fromField">
                                    <div class="styled-select slate fieldMap">
                                        <select onchange="updateOrgVariables('fieldsMap',this)" disabled>${options}
                                        </select>
                                    </div>
                                </div>
                                <div class="arrowIcon"> <i class="material-icons ">arrow_back</i></div>
                                <div class="fromField">
                                    <div class="styled-select slate fieldMap">
                                        <select onchange="updateOrgVariables('fieldsMap',this)"><option value="${fieldsMap[key]}">${intercomFieldsMap[fieldsMap[key]]}</option>${intercomFields}
                                        </select>
                                    </div>
                                </div>
                                <div class="arrowIcon" > <i class="material-icons pt" onclick="addFieldMap(this)">add</i> </div>
                            </div>`;
            }
            else{
                fieldsMapUi =`${fieldsMapUi}<div class="fieldMapp">
                                            <div class="fromField">
                                                <div class="styled-select slate fieldMap" >
                                                    <select onchange="updateOrgVariables('fieldsMap',this)">${options}
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="arrowIcon"> <i class="material-icons ">arrow_back</i></div>
                                            <div class="fromField">
                                                <div class="styled-select slate fieldMap" >
                                                    <select onchange="updateOrgVariables('fieldsMap',this)"><option value="${fieldsMap[key]}">${intercomFieldsMap[fieldsMap[key]]}</option>${intercomFields}
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="arrowIcon" > <i class="material-icons pt" onclick="deleteFieldMap(this)">delete</i> </div>
                                        </div>`;
            }                            
        });
        if(Object.keys(fieldsMap).length){
            var moduleSettingsUi = `<div class="btns">
                                    <div class="cancel btn" onclick="toggleFieldMap('${settingId}')">Cancel</div>
                                    <div class="save btn" onclick="saveAppSetting('${settingId}')">Save</div>
                                </div>
                                <div class="moduleSettings">
                                   <fieldset>
                                      <div class="modules">
                                        <label class="radiolabel" style="float:left;padding-right:50px;padding-left:0px;">Select module</label>
                                        <input class="mdlslect" type="radio"  name="modules_${settingId}" value="Contacts" id="Contacts_${settingId}" onchange="updateOrgVariables('module',this)" >
                                        <label class="radiolabel" for="Contacts_${settingId}">Contacts</label>
                                        <input class="mdlslect" type="radio"  name="modules_${settingId}" value="Leads" id="Leads_${settingId}" onchange="updateOrgVariables('module',this)">
                                        <label class="radiolabel" for="Leads_${settingId}">Leads</label>
                                      </div>
                                    </fieldset>
                                </div>`;
            fieldsMapUi =`${moduleSettingsUi}${firstField}${fieldsMapUi}`;
        }
        else{
            var triggername = setting.intercomforzohocrm__Trigger;
            if(triggername == "contact.added_email" || triggername == "user.email.updated"){
                fieldsMapUi='<div><h4>If this Intercom Lead/User is not available in Zoho CRM ,New Lead/Contact will be Created in Zoho CRM using Lead/User Created trigger settings. Zoho CRM Lead/Contact Email field will be updated.</h4> </div>'
            }
            else if(triggername == "user.deleted"){
                fieldsMapUi='<div><h4>Lead/Contact in Zoho CRM with same email Id will be deleted.</h4> </div>'
            }
            else if(triggername == "user.unsubscribed"){
                fieldsMapUi='<div><h4>If this Intercom Lead/User is not available in Zoho CRM ,New Lead/Contact will be Created in Zoho CRM using Lead/User Created trigger settings. Zoho CRM Lead/Contact Email_Opt_Out field will be updated.</h4> </div>'
            }
            else{
                fieldsMapUi='<div><h4>If this Intercom Lead/User is not available in Zoho CRM ,New Lead/Contact will be Created in Zoho CRM using Lead/User Created trigger settings. You can chat with the Lead/Contact in Intercom Conversations related list.</h4> </div>'
            }
        }                    
        $("#"+settingId+"_fieldsMap").html(`${fieldsMapUi}`);
        if(document.getElementById(triggerSetting.module+"_"+settingId)){
            // $("#"+setting.module+"_"+settings[i].id).prop("checked", true);
            document.getElementById(triggerSetting.module+"_"+settingId).checked = true;
        }
    }
    function toggleFieldMap(settingId){
        editableSettings =JSON.parse(JSON.stringify(settings));
        $('#'+settingId+'_name').toggleClass('active');
        settings.forEach(function(setting){
            if(setting.id == settingId){
                 $("#"+settingId+"_fieldsMap").html("");
                loadSettings(setting);
                $('#'+settingId+'_fieldsMap').toggle(500);

                $("select").on('focus', function () {
                    // Store the current value on focus and on change
                   $(this).data('pre', $(this).val());
                });
            }
        });
       
    }
    function addFieldMap(addElement){
        var triggerSetting ="";
        editableSettings.forEach(function(setting){
            if(setting.id == addElement.parentElement.parentElement.parentElement.id.slice(0,-10)){
                triggerSetting =JSON.parse(setting.intercomforzohocrm__Settings);
            }
        });
        var fieldsMap=triggerSetting.fieldsMap?triggerSetting.fieldsMap:{};
        var options =getFieldsOptions(fieldsMap,triggerSetting.module,"");
        var intercomFields =getFieldsOptions(intercomFieldsMap,"","");
        var addField=`<div class="fieldMapp">
                        <div class="fromField">
                            <div class="styled-select slate fieldMap" >
                                <select onchange="updateOrgVariables('fieldsMap',this)">${options}
                                </select>
                            </div>
                        </div>
                        <div class="arrowIcon"> <i class="material-icons ">arrow_back</i></div>
                        <div class="fromField">
                            <div class="styled-select slate fieldMap">
                                <select onchange="updateOrgVariables('fieldsMap',this)">${intercomFields}
                                </select>
                            </div>
                        </div>
                        <div class="arrowIcon" > <i class="material-icons pt" onclick="deleteFieldMap(this)">delete</i> </div>
                    </div>`;
        $(addField).insertAfter(addElement.parentElement.parentElement)
    }
    function deleteFieldMap(deleteIcon){
        var triggerSetting ="";
        var deleteElement = deleteIcon.parentElement.parentElement;
        var selectedApp;
        editableSettings.forEach(function(setting){
            if(setting.id == deleteElement.parentElement.id.slice(0,-10)){
                selectedApp = setting;
                triggerSetting =JSON.parse(setting.intercomforzohocrm__Settings);
            }
        })
        var key =deleteElement.children[0].children[0].children[0].value;
        var value = deleteElement.children[2].children[0].children[0].value;
        if(key != "Select" && value != "Select"){ 
            delete triggerSetting.fieldsMap[key];
            selectedApp.intercomforzohocrm__Settings=JSON.stringify(triggerSetting);
        }   
        deleteElement.parentElement.removeChild(deleteElement);
    }
