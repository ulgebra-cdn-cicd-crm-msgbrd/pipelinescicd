// var emailContentText = "Hi ,\n\nHope you're well. I'm writing to know how our business can help you.\n\nPlease choose a event to schedule an appointment:\n\n${eventsUrls} See you soon!\n\n %ownerName% \n\n %companyName%";
		var emailContentForContacts = [];
		var emailContentForLeads = [];
		var emailContentText = "";
		var customVariable = "";
		var startIndex =0;
		var endIndex = 0;
		var currentEditor="";
		var subject="";
		var name="";
		var sel;
		var range;
		var smsTemplates = [];
		var templateSettings;
		var phoneFieldsSetting={};
		var serviceOrigin="";
		var zapikey;
		var apikey=null;
        document.addEventListener("DOMContentLoaded", function(event) {
        	const urlParams = new URLSearchParams(window.location.search);
			serviceOrigin = urlParams.get('serviceOrigin'); 
        	ZOHO.embeddedApp.init().then(function(){
        		var getmap = {"nameSpace":"<portal_name.extension_namespace>"};
				
        		ZOHO.CRM.API.getOrgVariable("masivossmsforzohocrm__apikey").then(function(apiKeyData){
	        		if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content && apiKeyData.Success.Content != "0"){
	        			apikey = apiKeyData.Success.Content;
	        			document.getElementById("apikey").value=apiKeyData.Success.Content;
	        		}
	        		ZOHO.CRM.CONNECTOR.invokeAPI("crm.zapikey",getmap).then(function(resp){
						zapikey = JSON.parse(resp).response;
						if(apikey){
							addWebHook();
						}
					});	
	        	});	
	        	var oldmodules =["Leads","Contacts"];
	        	var DealsPhoneList ='';
	        	var modules=[];
	        	ZOHO.CRM.META.getModules().then(function(userModules){
	        		var fetchList =[];
	        		userModules.modules.forEach(function(module){
	        			if(oldmodules.indexOf(module.api_name)!= -1 && module.api_supported && module.visible){
	        				fetchList.push(ZOHO.CRM.META.getFields({"Entity":module.api_name}));
	        				modules.push(module.api_name);
	        			}
	        		});
		        	Promise.all(fetchList).then(function(data){ 
						for(let i=0;i<data.length;i++){
							var phoneList='';
							if(modules[i] == "Contacts" || modules[i] == "Accounts"){
								data[i].fields.forEach(function(field){
									if(field.data_type == "phone"){
										DealsPhoneList =DealsPhoneList+ `<li class="templateItem" onclick="savePhoneFields('Deals','${modules[i]}.${field.api_name}')">${modules[i]} ${field.field_label}</li>`;
									}	
								});	
							}
							data[i].fields.forEach(function(field){
								if(field.data_type == "phone"){
									phoneList =phoneList+ `<li class="templateItem" onclick="savePhoneFields('${modules[i]}','${field.api_name}')">${field.field_label}</li>`;
								}	
							});	
							if($(`#${modules[i]}PhoneList`)){
								$(`#${modules[i]}PhoneList`).append(phoneList);
							}
						}
						$(`#DealsPhoneList`).append(DealsPhoneList);
						ZOHO.CRM.API.getOrgVariable("masivossmsforzohocrm__settings").then(function(phoneFieldsData){
							if(phoneFieldsData && phoneFieldsData.Success && phoneFieldsData.Success.Content && phoneFieldsData.Success.Content != "0"){
		        				phoneFieldsSetting =JSON.parse(phoneFieldsData.Success.Content);
		        				if(phoneFieldsSetting["phoneField"]){
			        				Object.keys(phoneFieldsSetting["phoneField"]).forEach(function(key){
			        					document.getElementById("selected"+key+"Phone").innerText = phoneFieldsSetting["phoneField"][key].replace(/\./g," ").replace(/\_/g," ");;
			        				});
			        			}	
		        				document.getElementById("countryCode").value = phoneFieldsSetting.countryCode?phoneFieldsSetting.countryCode:"";
		        				document.getElementById("modules").value = phoneFieldsSetting.module;
		        			}
		        			document.getElementById("loader").style.display= "none";
        					document.getElementById("contentDiv").style.display= "block";
						});
					});
				}); 	
	    //     	ZOHO.CRM.API.getOrgVariable("masivossmsforzohocrm__settings").then(function(salesSignalData){
	    //     		document.getElementById("loader").style.display= "none";
     //    			document.getElementById("contentDiv").style.display= "block";
	    //     		if(salesSignalData && salesSignalData.Success && salesSignalData.Success.Content && salesSignalData.Success.Content != "0"){
	    //     			templateSettings =JSON.parse(salesSignalData.Success.Content);
	    //     			document.getElementById("salesSignal").checked = (templateSettings.enable == true);
	    //     		}	
		   //      	ZOHO.CRM.API.searchRecord({Entity:"masivossmsforzohocrm__SMS_Templates",Type:"criteria",Query:"(masivossmsforzohocrm__Module_Name:equals:Events)"})
					// .then(function(data){
					// 	smsTemplates = data.data;
					// 	if(data.data){
					// 		var templateList ="";
					// 		for(let i=0;i <data.data.length;i++){ 
					// 			if(templateSettings.templateId == data.data[i].id){
					// 				document.getElementById("selectedTemplate").innerText = data.data[i].Name;
					// 				document.getElementById("tooltiptext").innerText = data.data[i].Name;
					// 			}
					// 			templateList =templateList+ '<li class="templateItem" id="'+data.data[i].id+'" onclick="saveTemplateVariable(this)"></li>';
					// 			// templateList =templateList+ '<option  value="'+data.data[i].id+'"">'+data.data[i].Name+'</option>';
					// 		}
					// 		$('#templateList').append(templateList);
					// 		if(templateList == ""){
					// 			$('#templateList').append('<li style="text-align:center;">No Templates</li>');
					// 		}
					// 		else{
					// 			for(let i=0;i <data.data.length;i++){ 
					// 				document.getElementById(data.data[i].id).innerText = data.data[i].Name;
					// 			}
					// 		}	
					// 	}
					// 	else{
					// 		$('#templateList').append('<li style="text-align:center;">No Templates</li>');
					// 	}
					// });	
	    //     	});	
	        });    	
        });
    	
    	function checkWebHook(){
        	url = "https://api.smsmasivos.com.mx/webhook/get";
			var request = {
                url: url,
                headers: {
                	"Content-Type": "application/x-www-form-urlencoded",
                	"apikey" : apikey
                }
            };
			ZOHO.CRM.HTTP.post(request).then(function(resp){
				console.log(resp);
			});	
        }
        function addWebHook(){
        	if(serviceOrigin.indexOf(".zoho.") != -1){
        		serviceOrigin="https://platform.zoho."+serviceOrigin.substring(serviceOrigin.indexOf(".zoho.")+6);
        	}
        	var webhookurl = "https://us-central1-ulgebra-license.cloudfunctions.net/masivoswebhook?domain="+serviceOrigin+"&zapikey="+zapikey;
        	document.getElementById("webHookurl").innerText=webhookurl;
        	var url = "https://api.smsmasivos.com.mx/webhook/add";
			var request = {
                url,
                headers: {
                	"Content-Type": "application/x-www-form-urlencoded",
                	"apikey" : apikey
                },
                body:{
                	"lang":"en",
                	"url":webhookurl,
                	"status":1,
                }
            };
			ZOHO.CRM.HTTP.post(request).then(function(resp){
				console.log(resp);
			});	
        }
        function saveCountryCode(countryCode){
        	phoneFieldsSetting["countryCode"]=countryCode;
        	updateOrgVariables("masivossmsforzohocrm__settings",phoneFieldsSetting);
        }
        function saveModule(module){
        	phoneFieldsSetting["module"]=module;
        	updateOrgVariables("masivossmsforzohocrm__settings",phoneFieldsSetting);
        }
        function savePhoneFields(model,phoneField){
        	if(!phoneFieldsSetting["phoneField"]){
        		phoneFieldsSetting["phoneField"] ={};
        	}
        	phoneFieldsSetting["phoneField"][model]=phoneField;
        	document.getElementById("selected"+model+"Phone").innerText = phoneField.replace(/\./g," ").replace(/\_/g," ");;;
        	updateOrgVariables("masivossmsforzohocrm__settings",phoneFieldsSetting);
        }
        function saveTemplateVariable(editor){
        	for(var i=0; i<smsTemplates.length;i++){
				if(smsTemplates[i].id == editor.id){
					templateId = smsTemplates[i].id;
					document.getElementById("selectedTemplate").innerText = smsTemplates[i].Name;
					document.getElementById("tooltiptext").innerText = smsTemplates[i].Name;
					updateOrgVariables("masivossmsforzohocrm__EventReminderSettings",editor.id,"templateId");
					break;
				}
			}
        }
        function updateOrgVariables(apiname,value,key){
    		if(apiname == "masivossmsforzohocrm__apikey"){
    			value = document.getElementById("apikey").value;
    			if(value){
	    			apikey = value;
	    			addWebHook();
	    	    }		
    		}
    // 		else if(apiname == "masivossmsforzohocrm__EventReminderSettings"){
				// templateSettings[key]=value;
				// value =templateSettings;
    // 		}
			document.getElementById("ErrorText").innerText = "Saving...";
			document.getElementById("Error").style.display= "block";
    		ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": apiname,"value": value}).then(function(res){
    			document.getElementById("ErrorText").innerText = "Saved";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 500);
    		});
        }
