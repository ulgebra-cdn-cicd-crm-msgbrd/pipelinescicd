
var sel = window.getSelection();
var module;
var ButtonPosition;
var scheduledTime;
var isBulk;

var userFields;
var moduleFields;
var historyFields;
var selectedUser;
var currentRecords=[];

var credential = null;

var dotCircleLoader = `<div id="floatingCirclesG">
                    <div class="f_circleG" id="frotateG_01"></div>
                    <div class="f_circleG" id="frotateG_02"></div>
                    <div class="f_circleG" id="frotateG_03"></div>
                    <div class="f_circleG" id="frotateG_04"></div>
                    <div class="f_circleG" id="frotateG_05"></div>
                    <div class="f_circleG" id="frotateG_06"></div>
                    <div class="f_circleG" id="frotateG_07"></div>
                    <div class="f_circleG" id="frotateG_08"></div>
                </div>`;

var sendingLoader = `<div class="cssload-contain">
                <div class="cssload-dot"></div>
                <div class="cssload-dot"></div>
                <div class="cssload-dot"></div>
                <div class="cssload-dot"></div>
                <div class="cssload-dot"></div>
            </div>`;

var verifiedTickSVG = `<svg class="sendingMsgSuccessSpan" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000"><g><rect fill="none" height="24" width="24"/></g><g><path d="M23,12l-2.44-2.79l0.34-3.69l-3.61-0.82L15.4,1.5L12,2.96L8.6,1.5L6.71,4.69L3.1,5.5L3.44,9.2L1,12l2.44,2.79l-0.34,3.7 l3.61,0.82L8.6,22.5l3.4-1.47l3.4,1.46l1.89-3.19l3.61-0.82l-0.34-3.69L23,12z M10.09,16.72l-3.8-3.81l1.48-1.48l2.32,2.33 l5.85-5.87l1.48,1.48L10.09,16.72z"/></g></svg>`;

var loader = `<div class="wcMsgLoadingInner" title="loading…"><svg class="wcMsgLoadingSVG" width="17" height="17" viewBox="0 0 46 46" role="status"><circle class="wcMsgLoadingSvgCircle" cx="23" cy="23" r="20" fill="none" stroke-width="6" style="stroke: rgb(57 82 234);"></circle></svg></div>`;


// Extension Details
var phoneRecord;
var SMScredential;
var msgTextMaxLength;
var selectedTemplateCreate;
var mTalkzSenderId;

var extensionName = 'mTalkz';
var extensionAPI = 'mtalkzindiasmsforzohocrm__';

var extensionFieldName = "Name";
var extensionFieldMessage = extensionAPI + "Message";
var extensionFieldContactNumber = extensionAPI + "Customer_Number";
var extensionFieldModule = extensionAPI + "Module";
var extensionFieldDeal = extensionAPI + "Deal";
var extensionFieldContact = extensionAPI + "Contact";
var extensionFieldLead = extensionAPI + "Lead";
var extensionFieldAccount = extensionAPI + "Account";
var extensionFieldSchedule = extensionAPI + "Scheduled_Time";
var extensionFieldStatus = extensionAPI + "Status";
var extensionFieldSenderId = extensionAPI + "sender_Id";

var extensionCredential = extensionAPI + "credentials";
var extensionTemplate = extensionAPI + "mTalkz_Templates";
var extensionHistory = extensionAPI + "mTalkz_History";


document.addEventListener("DOMContentLoaded", function(event) {       	
        	
	ZOHO.embeddedApp.on("PageLoad", function(record) {        		

       	recordIds = record.EntityId;
       	module = record.Entity;
       	ButtonPosition = record.ButtonPosition;               	

		if(module != extensionTemplate && !module.includes('CustomModule')) {
			popupSizeSet(720, 520);
			if(ButtonPosition == "DetailView" || ButtonPosition == "ListViewEachRecord")
			isBulk = false;
			else
			isBulk = true;
			credentialDetailsGet(recordIds);			
			$('.templateFieldDiv').hide();
		}
		else {

			popupSizeSet(720, 520);
			messageContentBoxSet();

			$('.createTemplateOuterDiv').show();
			$('#editNumberui').hide();
			$('#smsTypeSetting').hide();
			$('.templateFieldDiv').hide();
			$('.footer').html(`<div class="send" id="send" onClick="saveTemplate()">Save Template</div>`);

			getFields('Users').then(function(fields) {
				fields.forEach(function(field){
					addListItem("dropdown-menu-user",field.field_label,"dropdown-item","Users."+field.field_label);
				});
		    });

		    getFields(extensionHistory).then(function(fields) {
				let templateModules ="";
				fields.forEach(function(field){
					if(field.data_type == "lookup" && field.field_label != 'Account'){
						templateModules = templateModules +`<li class="templateItem"  onclick="selectModuleTemp('${field.lookup.module.api_name}')">${field.lookup.module.api_name}</li>`;
					}
				});
				$('#createTemplateList').append(templateModules);
				$('#createTemplateList li').first().click();
		    });


			thisModuleTemplateCreate().then(function() {

				if(recordIds){
	               	tempRecordId = recordIds[0];
	               	if(ButtonPosition == "DetailView" || ButtonPosition == "ListViewEachRecord" || ButtonPosition == "EditView"){
	               		getRecord(module, tempRecordId).then(function(data){

	               			data = data.data[0];
						  	$(".selectedTemplateCreate").text(data.Name);
						  	let initialText = $("."+data.Name.replaceAll(' ', '__').replaceAll('-', '___')).attr('value').split('{#var#}');
						  	let changedText = data[extensionFieldMessage];
						  	selectedTemplateCreate = changedText;

						  	for (let i=0; i < initialText.length; i++) {

						  		if(initialText[i] != '.' && initialText[i] != '' && initialText[i] != ' ') {
						  			changedText = changedText.replace(initialText[i], '||replace||');
						  		}
						  		if(i==initialText.length-1) {
							  		changedText = changedText.split('||replace||');
						  			changedText = changedText.filter( function( item, index, inputArray ) { return inputArray.indexOf(item) == index; });
						  			changedText.forEach(function(val) {
						  				if(val!='')
								  		selectedTemplateCreate = selectedTemplateCreate.replaceAll(val, `<span contenteditable="true" class="selectInsertTextField" onclick="selectInsertText(this)">${val}</span>`);
								  	});
						  		}

						  	}

						  	document.getElementById("emailContentEmail").innerHTML = selectedTemplateCreate;
						  	selectedTemplateCreate = $("."+data.Name.replaceAll(' ', '__').replaceAll('-', '___')).attr('value').replaceAll('{#var#}', `<span contenteditable="true" class="selectInsertTextField" onclick="selectInsertText(this)">{#var#}</span>`);
							selectModuleTemp(data[extensionFieldModule]);
							$('#templateNotSelect').hide();
							$('#emailContentEmail').css({'background-color': '#fffffc'});	
							document.getElementById("loader").style.display= "none";

						});
					}
				}	
				else{
					document.getElementById("loader").style.display= "none";
				}	

			});
			
		}

		
    });

    ZOHO.embeddedApp.init().then();

});


function messageContentBoxSet() {
	
	let selectedDiv = 'emailContentEmail';

	msgTextMaxLength = 2000;

	editDivPasteToTextFunc(selectedDiv, msgTextMaxLength);
	ontypingToSizeCheck(selectedDiv, msgTextMaxLength);
	editDivPasteToNumberFunc('editNumber');
	  
}

function ontypingToSizeCheck(selected, max) {
	$('#'+selected).on('keyup, keydown', (e) => {
		check_charcount(selected, max, e);
	});
}

function editDivPasteToTextFunc(selected, max) {

	$('#'+selected).on('paste', (e) => {

		if(e.originalEvent.clipboardData) {

			let data = e.originalEvent.clipboardData.getData('text/html') || e.originalEvent.clipboardData.getData('text/plain');

			let regex = /<(?!(\/\s*)?()[>,\s])([^>])*>/g;
			data = data.replace(regex, '');

			let size = Number($('#'+selected).text().length) + Number(data.length);

			if(size > max) {
				wcConfirm(`Message should be within ${max} characters.`,'','Okay',true,false);
				e.preventDefault();
			}
			else {
				document.execCommand('insertHTML', false, data);
				e.preventDefault();
			}
			
		}	  

	});

	$('#'+selected).on('drop', (e) => {    

        if(e.originalEvent.dataTransfer) {

            let data = e.originalEvent.dataTransfer.getData('text/html') || e.originalEvent.dataTransfer.getData('text/plain');

            let regex = /<(?!(\/\s*)?()[>,\s])([^>])*>/g;
            data = data.replace(regex, '');

            let size = Number($('#'+selected).text().length) + Number(data.length);

			if(size > max) {
				wcConfirm(`Message should be within ${max} characters.`,'','Okay',true,false);
				e.preventDefault();
			}
			else {
				$('#'+selected).text($('#'+selected).text()+data);
				e.preventDefault();
			}
            
        } 

    });

}

function editDivPasteToNumberFunc(selected) {

	$('#'+selected).on('paste', (e) => {

		if(e.originalEvent.clipboardData) {

			let data = e.originalEvent.clipboardData.getData('text/html') || e.originalEvent.clipboardData.getData('text/plain');

			let regex = /<(?!(\/\s*)?()[>,\s])([^>])*>/g;
			data = data.replace(regex, '').replace(/\D/g,'');

			let size = Number($('#'+selected).val().length) + Number(data.length);

			if(size > 15) {
				wcConfirm('Invalid Number.','','Okay',true,false);
				e.preventDefault();
			}
			else {
				document.execCommand('insertHTML', false, data);
				e.preventDefault();
			}
			
		}	  

	});


	$('#'+selected).on('drop', (e) => {    

        if(e.originalEvent.dataTransfer) {

            let data = e.originalEvent.dataTransfer.getData('text/html') || e.originalEvent.dataTransfer.getData('text/plain');

            let regex = /<(?!(\/\s*)?()[>,\s])([^>])*>/g;
            data = data.replace(regex, '').replace(/\D/g,'');

            let size = Number($('#'+selected).val().length) + Number(data.length);

			if(size > 15) {
				wcConfirm(`Invalid Number.`,'','Okay',true,false);
				e.preventDefault();
			}
			else {
				$('#'+selected).val($('#'+selected).val()+data);
				e.preventDefault();
			}
            
        } 

    });


}

function check_charcount(content_id, max, e) {   
	if(e.which != 46 && e.which != 8 && $('#'+content_id).text().length > max) {
		wcConfirm(`Message should be within ${max} characters.`,'','Okay',true,false);
		e.preventDefault();
	}
}

function onKeypressNumberField(selected, e) {
    if (isNaN(String.fromCharCode(e.keyCode))) {
        if(!Number(String(e.key)))
        e.preventDefault();
    }
    else if (e.keyCode == 13) {
        e.preventDefault();
        return false;
    }
    else if (e.keyCode == 32) {
        e.preventDefault();
        return false;
    }
    else if (e.keyCode == 45) {
        e.preventDefault();
        return false;
    }
    else if (e.keyCode == 46) {
        e.preventDefault();
        return false;
    }
    else if($(selected).val().length > 14) {
        e.preventDefault();
        return false;
    }
    else if(true){

        return true;

    }

    if($(selected).val().length==15) {
        e.preventDefault();
        return false;
    }

}

function onKeyupNumberField(e) {

    if (e.which == 8 || e.which == 46) {
        $('.countryCodeHintShowBulk').text('* with Country Code');
    }

}



function popupSizeSet(width, height) {
	ZOHO.CRM.UI.Resize({height:height,width:width}).then(function(data){
		// console.log(data);
	});
}

async function getFields(entity) {
	return await ZOHO.CRM.META.getFields({"Entity":entity}).then(function(fields){
		return fields.fields;
	});
}

async function getRecord(entity, recordIds) {

	return await ZOHO.CRM.API.getRecord({Entity:entity,RecordID:recordIds}).then(function(data){
		return data;
	});	

}

async function createRecord(entity, req_data) {

	return await ZOHO.CRM.API.insertRecord({Entity:entity,APIData:req_data,Trigger:["workflow"]}).then(function(response) {
		let responseInfo	= response.data[0];
		let resCode			= responseInfo.code;
		if(resCode == 'SUCCESS'){
			return responseInfo.details.id;
		}
		else{
			return false;
		}
	});

}

async function updateRecord(entity, req_data) {

	return await ZOHO.CRM.API.updateRecord({Entity:entity,APIData:req_data,Trigger:["workflow"]}).then(function(response) {
		let responseInfo	= response.data[0];
		let resCode			= responseInfo.code;
		if(resCode == 'SUCCESS'){
			return responseInfo.details.id;
		}
		else{
			return false;
		}
	});

}


async function credentialValuesSet(credential) {
	if(credential != {} && credential.apikey) {
		let SMScredential_url = {url:`http://msg.mtalkz.com/V2/http-balance-api.php?apikey=${credential.apikey}&format=json`};
		await zohoHttpRequest('get', SMScredential_url).then(function(SMScredts){

			if(SMScredts.data && SMScredts.data.balance) {
				SMScredential = Number(SMScredts.data.balance);
				$('.creditsOfextension').text(SMScredential);
				$('.sms_credentialHead').text(SMScredts.data.gateway);
				$('.sms_userDetaildivision').remove();
				$('.creditsOfextensionDiv').show();					
			}
			else {
				$('.extensionCredentialMain').remove();
			}
							
		});    
	}
	else {
		$('.extensionCredentialMain').remove();
	}
}

async function credentialDetailsGet(recordIds) {
	await ZOHO.CRM.API.getOrgVariable(extensionCredential).then(async function(apiKeyData){
    		
		if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content && apiKeyData.Success.Content != "0" && apiKeyData.Success.Content != "null"){
			credential = apiKeyData.Success.Content;
			credential = JSON.parse(credential);
				
        	if(credential == {} || !credential.apikey){
        		document.getElementById("loader").style.display= "none";
				wcConfirm(`Please enter your ${extensionName} credential in extension settings page.`,'','Okay',true,true);
			}
			else {

				await messageContentBoxSet();
				await credentialValuesSet(credential);
				await smsChannelChoose();
				await scheduleTimeSet();
				await getAllUsers();
				await historyFieldsGet();
               	await thisModuleTemplateSet();
               	await singleBulkMsgSelect(recordIds);
               	await customersModifyCustomisation();
               	
			}
		}
		else{
			document.getElementById("loader").style.display= "none";		
        	wcConfirm(`Please enter your ${extensionName} credential in extension settings page.`,'','Okay',true,true);
		}
	});
}

async function smsChannelChoose() {
	let senderRequest = {
			url : "http://msg.mtalkz.com/V2/http-senderid-api.php?apikey="+credential.apikey
		}
	await ZOHO.CRM.HTTP.get(senderRequest).then(function(resp){

		resp = JSON.parse(resp);
		let senderList = '';

		if(resp && resp.data && resp.data.length >= 1 && resp.message && resp.message == "Success") {
			resp.data.forEach(function(sender) {
				senderList =senderList+ '<li class="templateItem senderItem" onclick="selectedSenderId(this)" value="'+sender.senderId+'">'+sender.senderId+'</li>';
			});
			$('#senderList').append(senderList);
		}
		else {
			$('#senderList').append('<li style="padding: 4px 15px;box-sizing: border-box;cursor:default;">No SenderId</li>');
		}

	});
}

function scheduleTimeSet() {

	let timeList=["12:00 AM","12:15 AM","12:30 AM","12:45 AM","01:00 AM","01:15 AM","01:30 AM","01:45 AM","02:00 AM","02:15 AM","02:30 AM","02:45 AM","03:00 AM","03:15 AM","03:30 AM","03:45 AM","04:00 AM","04:15 AM","04:30 AM","04:45 AM","05:00 AM","05:15 AM","05:30 AM","05:45 AM","06:00 AM","06:15 AM","06:30 AM","06:45 AM","07:00 AM","07:15 AM","07:30 AM","07:45 AM","08:00 AM","08:15 AM","08:30 AM","08:45 AM","09:00 AM","09:15 AM","09:30 AM","09:45 AM","10:00 AM","10:15 AM","10:30 AM","10:45 AM","11:00 AM","11:15 AM","11:30 AM","11:45 AM","12:00 PM","12:15 PM","12:30 PM","12:45 PM","01:00 PM","01:15 PM","01:30 PM","01:45 PM","02:00 PM","02:15 PM","02:30 PM","02:45 PM","03:00 PM","03:15 PM","03:30 PM","03:45 PM","04:00 PM","04:15 PM","04:30 PM","04:45 PM","05:00 PM","05:15 PM","05:30 PM","05:45 PM","06:00 PM","06:15 PM","06:30 PM","06:45 PM","07:00 PM","07:15 PM","07:30 PM","07:45 PM","08:00 PM","08:15 PM","08:30 PM","08:45 PM","09:00 PM","09:15 PM","09:30 PM","09:45 PM","10:00 PM","10:15 PM","10:30 PM","10:45 PM","11:00 PM","11:15 PM","11:30 PM","11:45 PM"];
	let timeOptions ="";
	timeList.forEach(function(time){
		timeOptions = timeOptions +"<option  value='"+time+"'>"+time+"</option>"
	});

	$('#timeList').append(timeOptions);
	$('#datepicker').datepicker().datepicker('setDate',new Date());
	let date = document.getElementById("datepicker").value;
	let time = document.getElementById("timeList").value;
	document.getElementById("scheduledDateTime").innerText=new Date(date).toDateString()+" at "+time +" ("+Intl.DateTimeFormat().resolvedOptions().timeZone+")";
    	
}

async function currentUserGet() {
	return await ZOHO.CRM.CONFIG.getCurrentUser().then(function(data){
		return data.users[0].id;
	});

}

async function historyFieldsGet() {
	historyFields = await getFields(extensionHistory);
}

async function getAllUsers() {

	ZOHO.CRM.API.getAllUsers({Type:"AllUsers"}).then(async function(data){

	    let users = data.users;

	    await currentUserGet().then(async function(currentLoginUser) {

	    	if(users.length) {
		    	$('#dropdown-menu-user').append(`<li class="userListHead">Users</li>`);
		    	let userCount = 0;
		    	users.forEach(async function(user) {

		    		if(user.status == "active")
			    	$('#dropdown-menu-user').append(`<li class="selectedUserList${user.role.name == 'CEO' ? ' selectedOwnerUser': ''}" userId="${user.id}" onclick="userSelectFunction(this);"><span class="selectedUserTick" style="display:none;">&#10003;</span>${user.role.name == 'CEO' ? currentLoginUser == user.id ? 'Owner/Current User' : 'Owner' : currentLoginUser == user.id ? 'Current User' : user.full_name}</li>`);
	
			    	if(userCount++ == users.length-1) {
			    		await getFields('Users').then(function(fields) {
			    			userFields = fields;
					    	$('.selectedOwnerUser').click();
					    	$('#dropdown-menu-user').append(`<li class="userListHead">User Fields</li>`);
							userFields.forEach(function(field){
								addListItem("dropdown-menu-user",field.field_label,"dropdown-item","Users."+field.field_label);
							});
							$('#dropdown-menu-user').find('.dropdown_Butt').css({'padding-left': '20px'});
					    });
			    	}

			    });
		    }
		    else {
		    	$('.userFieldDiv').remove();
		    }	

	    });	

	});
}

function userSelectFunction(selected) {
	$('.selectedUserList').find('.selectedUserTick').hide();
	$('.selectedUserList').removeClass('userSelectedHover');
	$(selected).find('.selectedUserTick').show();
	$(selected).addClass('userSelectedHover');
	getRecord('users', $(selected).attr('userId')).then(function(data) {
		selectedUser = data.users[0];
	});
}

function addListItem(id,text,className,value){

	let linode = '<li class="'+className+'"><button class="dropdown_Butt" onclick="insert(this)">'+text+'<input type="hidden" value="'+value+'"></button></li>';
	$('#'+id).append(linode);

}

async function searchRecord(entity, query) {

	return await ZOHO.CRM.API.searchRecord({Entity:entity,Type:"criteria",Query:query,delay:"false"}).then(function(response) {
		return response.data;
	});

}

async function thisModuleTemplateCreate() {

	await ZOHO.CRM.API.getOrgVariable(extensionCredential).then(async function(apiKeyData){
    		
		if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content && apiKeyData.Success.Content != "0" && apiKeyData.Success.Content != "null"){
			credential = apiKeyData.Success.Content;
			credential = JSON.parse(credential);
				
        	if(credential == {} || !credential.apikey){
        		document.getElementById("loader").style.display= "none";
				wcConfirm(`Please enter your ${extensionName} credential in extension settings page.`,'','Okay',true,true);
			}
			else {
				let request = {
							url : "http://msg.mtalkz.com/V2/http-template-api.php?apikey="+credential.apikey
						}

				await ZOHO.CRM.HTTP.get(request).then(function(resp){

					resp = JSON.parse(resp);
					let templateList = '';

					if(resp && resp.data && resp.data.length >= 1 && resp.message && resp.message == "Success") {
						resp.data.forEach(function(temp) {
							templateList =templateList+ `<li class="templateItem ${temp.templateName.replaceAll(' ', '__').replaceAll('-', '___')}" onclick="showsmsCreate(this)" value="${temp.templateData}">${temp.templateName}</li>`;
						});
						$('.createTemplateList').append(templateList);
					}
					else {
						$('.createTemplateList').append('<li style="padding: 4px 15px;box-sizing: border-box;cursor:default;">No Templates</li>');
					}

				});
			}
		}
		else{
			document.getElementById("loader").style.display= "none";		
        	wcConfirm(`Please enter your ${extensionName} credential in extension settings page.`,'','Okay',true,true);
		}
	});

	return true;

}

function thisModuleTemplateSet() {
	
	let request = {
					url : "http://msg.mtalkz.com/V2/http-template-api.php?apikey="+credential.apikey
				}
	ZOHO.CRM.HTTP.get(request).then(function(resp){

		resp = JSON.parse(resp);
		let templateList = '';

		if(resp && resp.data && resp.data.length >= 1 && resp.message && resp.message == "Success") {
			resp.data.forEach(function(temp) {
				templateList =templateList+ '<li class="templateItem" onclick="showsms(this)" value="'+temp.templateData+'">'+temp.templateName+'</li>';
			});
			$('#templateList').append(templateList);
		}
		else {
			$('#templateList').append('<li style="padding: 4px 15px;box-sizing: border-box;cursor:default;">No Templates</li>');
		}

	});

}


async function singleBulkMsgSelect(recordIds) {
	await getRecord(module, recordIds).then(async function(record){	
		currentRecords = record.data;
		if(!isBulk){
			document.getElementById("loader").style.display= "none";
			await selectModule(currentRecords[0]);
		}
		else{
			$('#editNumberui').hide();
			$('#bulksettings').show();
			document.getElementById("loader").style.display= "none";
			await selectModuleforBulk(currentRecords);
		}	
	});
}

function templateModuleChangeCheck(templateModule, smsContent) {

	let othermodule=false;
	if(templateModule == "Leads" && (smsContent.indexOf("${Contacts.") != -1 || smsContent.indexOf("${Accounts.") != -1 || smsContent.indexOf("${Deals.") != -1 )){
		othermodule=true;
	}
	else if(templateModule == "Contacts" && (smsContent.indexOf("${Leads.") != -1 || smsContent.indexOf("${Accounts.") != -1 || smsContent.indexOf("${Deals.") != -1 )){
		othermodule=true;
	}
	else if(templateModule == "Accounts" && (smsContent.indexOf("${Contacts.") != -1 || smsContent.indexOf("${Leads.") != -1 || smsContent.indexOf("${Deals.") != -1 )){
		othermodule=true;
	}
	else if(templateModule == "Deals" && (smsContent.indexOf("${Contacts.") != -1 || smsContent.indexOf("${Accounts.") != -1 || smsContent.indexOf("${Leads.") != -1 )){
		othermodule=true;
	}

	return othermodule;

}

async function selectModuleTemp(selectModule) {

	document.getElementById("selectedmodule").innerText = selectModule;
	document.getElementById("thisModuleName").innerText = "Insert "+selectModule+" Fields";

	let smsContent = document.getElementById("emailContentEmail").innerText;
	
	if(templateModuleChangeCheck(selectModule, smsContent))
	document.getElementById("emailContentEmail").innerHTML = selectedTemplateCreate;

	document.getElementById("dropdown-menu-email").innerHTML="";
	getFields(selectModule).then(function(fields){
		fields.forEach(function(field){
			addListItem("dropdown-menu-email",field.field_label,"dropdown-item",selectModule+"."+field.field_label);
		})
	});	

	$('details').removeAttr("open"); 

}

function saveTemplate() {

	let name = $(".selectedTemplateCreate").text();
	let templateModule = document.getElementById("selectedmodule").innerText;
	let smsContent = document.getElementById("emailContentEmail").innerText.trim().replaceAll(/\s\s+/g, ' ').replace(/\n/g,"").replace(/\t/g,"");
	
	if(name == ""){
		wcConfirm('Choose a Template to continue..','','Okay',true,false);
		return ;
	}
	else if(templateModule == ""){
		wcConfirm('Please Choose Module.','','Okay',true,false);
		return ;
	}
	else if( smsContent == ""){
		wcConfirm('Message cannot be empty.','','Okay',true,false);
		return;
	}
	else if(templateModuleChangeCheck(templateModule, smsContent)){
		wcConfirm('Message Contains Other Modules Merge Fields.Please change it.','','Okay',true,false);
		return ;
	}
	else{

    	wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingLoaderInner">${loader}</div><div class="sendingLoaderHint">SMS Template is saving...</div></div>`,'','Okay',true,true);
    	
    	if(ButtonPosition != 'CreateOrCloneView' && ButtonPosition != 'ListView') {

			let req_data = {"id":tempRecordId};
			req_data[extensionFieldName] = name;
			req_data[extensionFieldMessage] = smsContent;
			req_data[extensionFieldModule] = templateModule;
			updateRecord(extensionTemplate, req_data).then(function(resp){

				let tempStatus = "Your SMS Template is updated successfully.";
				if(resp) {
					wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingMsgSuccess">${verifiedTickSVG}</div><div class="sendingMsgSuccessHint">${tempStatus}</div></div>`,'','Okay',true,true);
					setTimeout(function() {	popupCloseFunc(); }, 1500);	
				}
				else
				setTimeout(function() {	wcConfirm('Opps! Something went wrong from server side. Please try after sometimes!!!','','Okay',true,false); }, 1500); 

			});	

    	}
    	else {

    		let req_data = {};
			req_data[extensionFieldName] = name;
			req_data[extensionFieldMessage] = smsContent;
			req_data[extensionFieldModule] = templateModule;
			createRecord(extensionTemplate, req_data).then(function(resp){

				let tempStatus = "Your SMS Template is saved successfully.";
				if(resp) {

					wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingMsgSuccess">${verifiedTickSVG}</div><div class="sendingMsgSuccessHint">${tempStatus}</div></div>`,'','Okay',true,true);
					ZOHO.CRM.UI.Record.open({Entity:extensionTemplate,RecordID:resp}).then(function(data){
							popupCloseFunc();
					});	
				}
				else
				setTimeout(function() {	wcConfirm('Opps! Something went wrong from server side. Please try after sometimes!!!','','Okay',true,false); }, 1500); 

			});	

		}	
	}		
}



async function selectModuleforBulk(){
	
	document.getElementById("thisModuleName").innerText = "Insert "+module+" Fields";
	document.getElementById("dropdown-menu-email").innerHTML="";
	document.getElementById("bulkNumberList").innerHTML="";
	await getFields(module).then(async function(fields){			
		
		moduleFields = fields;
		
		let NumberList= '';
        moduleFields.forEach(function(field) {
            if(field.data_type == "phone"){
                NumberList = NumberList+ `<li class="templateItem" onclick="savePhoneFields(this)" entity="${module}" api_name="${field.api_name}">${field.field_label}</li>`;
            }
            addListItem("dropdown-menu-email",field.field_label,"dropdown-item",module+"."+field.field_label);
        });
        if(module == "Deals"){
            let contactphoneList = ["Mobile","Phone","Home Phone","Other Phone","Ass Phone"];
            contactphoneList.forEach(function(field){
                NumberList = NumberList+ `<li class="templateItem" onclick="savePhoneFields(this)" entity="${'Contacts'}" api_name="${field.replace(" ","_")}">Contact - ${field}</li>`;
            })
            NumberList = NumberList+ `<li class="templateItem" onclick="savePhoneFields(this)" entity="${'Accounts'}" api_name="Phone">Account - Phone</li>`;
        }
        $('#bulkNumberList').append(NumberList); 
        $('#bulkNumberList li').first().click();



	});	

}

async function selectModule(record){

	document.getElementById("thisModuleName").innerText = "Insert "+module+" Fields";
	document.getElementById("dropdown-menu-email").innerHTML="";
	document.getElementById("NumberList").innerHTML="";

	await getFields(module).then(async function(fields){
		
		moduleFields = fields;
		
		let NumberList = "";
		let lookupModules = [];
		let errText = "";
		moduleFields.forEach(function(field){
			
			if(field.data_type == "phone") {
				errText = errText + field.field_label + '/';
				if(record[field.api_name] != null)
				NumberList = NumberList+ `<li class="templateItem" onclick="setNumber(this)" entity="${module}" api_name="${field.api_name}" num="${record[field.api_name]}">${field.field_label}</li>`;
			}
			else if(field.data_type == "lookup"){
				if(record[field.api_name] != null)
				lookupModules.push(field);
			}	

			addListItem("dropdown-menu-email",field.field_label,"dropdown-item",module+"."+field.field_label);
			
		});	

		if(lookupModules.length == 0) {
			
			if(NumberList == "") {
				$('#NumberList').append('<li class="noNumberListStyle">No Numbers</li>');
				wcConfirm(`<div><span style="font-weight: 600;font-size: 14px;">${errText.slice(0, -1)}</span> fields is empty.</div>`,'','Okay',true,false);
			}
			else{
				$('#NumberList').append(NumberList);
				$('#NumberList li').first().click();
			}
			
			document.getElementById("loader").style.display= "none";
		}	
		else{

			for (let i = 0; i < lookupModules.length; i++) {
				let lookupModule = lookupModules[i].lookup.module.api_name;
				let lookupId = record[lookupModules[i].api_name].id;
				await getFields(lookupModule).then(async function(respFields) {
					await getRecord(lookupModule, lookupId).then(async function(datarecord) {
						
						datarecord = datarecord.data[0];
						respFields.forEach(function(field){

							if(field.data_type == "phone") {
								errText = errText + lookupModule + ' - ' +field.field_label + '/';
								if(datarecord[field.api_name] != null)
								NumberList = NumberList + `<li class="templateItem" onclick="setNumber(this)" entity="${lookupModule}" api_name="${field.api_name}" num="${datarecord[field.api_name]}">${lookupModules[i].lookup.module.api_name} - ${field.field_label}</li>`;
							}		

						});

						if(i == lookupModules.length-1) {
							if(NumberList == "") {
								$('#NumberList').append('<li class="noNumberListStyle">No Numbers</li>');
								wcConfirm(`<div><span style="font-weight: 600;font-size: 14px;">${errText.slice(0, -1)}</span> fields is empty.</div>`,'','Okay',true,false);
							}
							else {
								$('#NumberList').append(NumberList);
								$('#NumberList li').first().click();
							}
							
							document.getElementById("loader").style.display= "none";
						}

					});

				});
			}

		}	

	});	

}

async function setNumber(selected){

	document.getElementById("editNumber").value = "";
	$('#numberLoader').html(dotCircleLoader).show();

    phoneRecord = { entity: $(selected).attr('entity'), api_name: $(selected).attr('api_name') };

	let selectedNumber = $(selected).attr('num').replace(/\D/g,'');
	if(selectedNumber) {
		await checkPhoneNumber(selectedNumber).then(function(resp) {

			if(resp && resp.countryPrefix != null) {
	    		let phoneNumber = resp.phoneNumber.toString();
	    		countryCode = resp.countryPrefix.toString();
	    		no = phoneNumber.substring(phoneNumber.indexOf(countryCode) + countryCode.length);

	    		
	    		$('#countryCode option[value="91"]').prop('selected', 'selected').change();
	    		

	    		document.getElementById("editNumber").value = no;
				$('#numberLoader').html("").hide();
	    	}
	    	else {
	    		document.getElementById("editNumber").value = selectedNumber;
				$('#numberLoader').html("").hide();
	    	}	

		});
	}
			
	document.getElementById("selectedNumber").innerText =  $(selected).text();			
	$('details').removeAttr("open");

}

function selectCountryCode(selected) {
	if(!isBulk)
	$('.selectedCountryCodeShow').text($(selected).val());
}

async function checkPhoneNumber(no) {

	let request ={
		url : "https://rest.messagebird.com/lookup/" + no.replace(/\D/g,''),
		headers:{
			Authorization:"AccessKey 7NMPor0R8DofSHH61SpViNNqQ",
		}
	}
	return await zohoHttpRequest('get', request).then(function(resp) {

		if(resp.error)
		return null;
		else
		return resp;
		
	}); 

}

function savePhoneFields(selected) {
    phoneRecord = { 'entity': $(selected).attr('entity'), api_name: $(selected).attr('api_name') };
    document.getElementById("bulkSelectedNumber").innerText = $(selected).text();
    $('details').removeAttr("open");  
}
        
function showsms(editor){
			
	document.getElementById("selectedTemplate").innerText = $(editor).text();			
	document.getElementById("emailContentEmail").innerHTML = $(editor).attr('value').replaceAll('{#var#}', `<span contenteditable="true" class="selectInsertTextField" onclick="selectInsertText(this)">{#var#}</span>`);
	$('#templateNotSelect').hide();
	$('#emailContentEmail').css({'background-color': '#fffffc'});
	$('details').removeAttr("open");

}
function showsmsCreate(editor){
	
	$(".selectedTemplateCreate").text($(editor).text());	
	selectedTemplateCreate = $(editor).attr('value').replaceAll('{#var#}', `<span contenteditable="true" class="selectInsertTextField" onclick="selectInsertText(this)">{#var#}</span>`);		
	document.getElementById("emailContentEmail").innerHTML = selectedTemplateCreate;
	$('details').removeAttr("open");
	$('#templateNotSelect').hide();
	$('#emailContentEmail').css({'background-color': '#fffffc'});	

}
function selectedSenderId(editor){
	
	document.getElementById("selectedSender").innerText = $(editor).attr('value');
	mTalkzSenderId = $(editor).attr('value');

	$('details').removeAttr("open");

}		

async function sendSMS(){
	
	let country_code = $('#countryCode').find(":selected").val();
	let bulkCountry_code = $('#bulkCountryCode').find(":selected").val();
	let MobileNumber = document.getElementById("editNumber").value;
	let message = document.getElementById("emailContentEmail").innerText.trim();
	let date = document.getElementById("datepicker").value;
	let time = document.getElementById("timeList").value;

	if(message == "") {
		await wcConfirm('Message cannot be empty.','','Okay',true,false);
		return false;
	}  
	else if(country_code == "" && !isBulk) {
		await wcConfirm('Select countryCode.','','Okay',true,false);
		return false;
	}    	
	else if(bulkCountry_code == "" && isBulk) {
		wcConfirm('Select countryCode.','','Okay',true,false);
		return false;
	}
	else if(message.length > msgTextMaxLength){
		await wcConfirm(`Message should be within ${msgTextMaxLength} characters.`,'','Okay',true,false);
		return false;
	}
	else if(scheduledTime && new Date(date+" "+time).getTime() < new Date().getTime()){
		await wcConfirm('Schedule time should be in future.','','Okay',true,false);
		return false;
	}
	else if(!isBulk && (MobileNumber == null || MobileNumber == "")){
        await wcConfirm('Mobile field is empty.','','Okay',true,false);
        return false;
	}
	else if(mTalkzSenderId == "" || mTalkzSenderId == null || mTalkzSenderId == undefined) {
		await wcConfirm('Select Sender Id.','','Okay',true,false);
        return false;
	}
	else{

		let countryCode;
		if(isBulk) {
			countryCode = bulkCountry_code;
			await wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingLoaderInner">${sendingLoader}</div><div class="sendingLoaderHint">Sending...<br>Messages will be sent only for valid numbers.</div></div>`,'','Okay',true,true);
		}
		else {
			countryCode = country_code;	
			await wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingLoaderInner">${sendingLoader}</div><div class="sendingLoaderHint">Sending...</div></div>`,'','Okay',true,true);
		}

	    for(let i=0;i<currentRecords.length;i++) {

	    	if(SMScredential <= 0) {
	    		await wcConfirm('<div style="width:100%;"><div>You do not have Transactional Credits.</div><div class="wcConfirmButt"><a style=" text-decoration: auto; position: relative; left: 23px; top: 16px; " class="wcConfirmYes" href="https://dashboard.nexmo.com/payments/new" target="_blank">Buy Credits</a></div></div>','','Okay',true,true);
				return false;
	    	}

	    	let currentRecord = currentRecords[i];

	    	await getMobileNumber(currentRecord);

	    	if(!isBulk) {
	    		
	    		if(MobileNumber != phoneRecord.Mobile.replace(/\D/g,'') && countryCode+MobileNumber != phoneRecord.Mobile.replace(/\D/g,'')) {
	    			phoneRecord['Mobile'] = MobileNumber;
					phoneRecord['recipientName'] = MobileNumber;
					phoneRecord['id'] = '';
	    		}

	    	}

	    	if(phoneRecord['Mobile'] && phoneRecord['Mobile'].replace(/\D/g,'')) {

	    		let filledMessage = await getMessageWithFields(message, currentRecord);

				if(filledMessage.length > msgTextMaxLength && !isBulk)
				{
					await wcConfirm('Message is Too Large.','','Okay',true,false);
					return;
				}
				else if(filledMessage.length < 1)
				{
					if(!isBulk) {
						await wcConfirm('Merge Fields value is empty.','','Okay',true,false);
						return;
					}
					else {
						filledMessage = " ";
					}
				}
		        		
				let to = await checkMobileNumber(phoneRecord.Mobile, countryCode);

				if(to != phoneRecord.Mobile.replace(/\D/g,'')) {
					if(phoneRecord['recipientName'] == phoneRecord['Mobile'])				
					phoneRecord['recipientName'] = to;
	    			phoneRecord['Mobile'] = to;
	    		}
	    		
				let req_data = {};
				req_data[extensionFieldName] = `SMS to ${phoneRecord.recipientName}`;
				req_data[extensionFieldMessage] = filledMessage;
				req_data[extensionFieldContactNumber] = phoneRecord.Mobile;
				req_data[extensionFieldModule] = module;
				req_data[extensionFieldSenderId] = mTalkzSenderId;
				
				historyFields.forEach(function(field){
					if(field.data_type == "lookup" && phoneRecord.entity && phoneRecord.entity.includes(field.lookup.module.api_name)){
						req_data[field.api_name] = phoneRecord.id;
					}
					if(field.data_type == "lookup" && field.lookup.module.api_name == module)
					req_data[field.api_name] = currentRecord.id;
				});


				if(scheduledTime)
				{
					let time = scheduledTime.substring(0,19) + "+00:00";
					req_data[extensionFieldSchedule]=time.toString();
					req_data[extensionFieldStatus]="Scheduled";

					let requestMap = {status:"Your SMS has been scheduled successfully.", count: i};
					await smsResponseToHistory(true, requestMap, req_data);

				}
		        else {

					let requestMap = { to: to, msg: filledMessage, count: i };

					await sendSMS_Request(requestMap, req_data).then(async function(response) {
						if(!response.resp) {
							await wcConfirm('Message sent is failed.','','Okay',true,false);
							return false;
						}					
						else
						await sendSMS_Response(response.resp, response.requestMap, response.req_data);
					}); 

				}

	    	}
	    	else {

	    		if(!isBulk) {
		        	await wcConfirm('Mobile field is empty.','','Okay',true,false);
		        	return false;
		        }
		        else {

		   //      	let filledMessage = await getMessageWithFields(message, currentRecord);

		   //      	let req_data = {};
					// req_data[extensionFieldName] = `SMS to No Number`;
					// req_data[extensionFieldMessage] = filledMessage;
					// req_data[extensionFieldContactNumber] = '';
					// req_data[extensionFieldModule] = module;

					// historyFields.forEach(function(field){
					// 	if(field.data_type == "lookup" && phoneRecord.entity && phoneRecord.entity.includes(field.lookup.module.api_name)){
					// 		req_data[field.api_name] = phoneRecord.id;
					// 	}
					// 	if(field.data_type == "lookup" && field.lookup.module.api_name == module)
					// 	req_data[field.api_name] = currentRecord.id;
					// });

					// req_data[extensionFieldStatus]="Mobile field is empty.";
		        	let requestMap = {status:"SMS to No Number.", count: i};
					//await smsResponseToHistory(false, requestMap, req_data);

					await popupSccuessExit(false, requestMap);

		        }

	    	}


	    }  
	    		
	}	
}

async function smsResponseToHistory(Success, requestMap, req_data) {

	await createHistory(requestMap, req_data).then(async function(response) {
		if(response.resp)
		await popupSccuessExit(Success, response.requestMap);
		else
		await setTimeout(async function() {	await wcConfirm('Opps! Something went wrong from server side. Please try after sometimes!!!','','Okay',true,false); }, 1500); 
	});

}

async function zohoHttpRequest(method, request) {


	if(method == 'get')
	return await ZOHO.CRM.HTTP.get(request).then(function(resp) {
     	resp = JSON.parse(resp);
     	return resp;
     	
    },function(err) {
		console.log(err);
		return null;
	});
	
	if(method == 'post')
	return await ZOHO.CRM.HTTP.post(request).then(function(resp) {
     	resp = JSON.parse(resp);
     	return resp;
     	
    },function(err) {
		console.log(err);
		return null;
	});
	
}

async function sendSMS_Request(requestMap, req_data) {
	
	let request = {
	    url: "https://msg.mtalkz.com/V2/http-api-post.php",
	    headers: {
	        "Content-Type": "application/json"
	    },
	    body:{
	     	"apikey": credential.apikey,
		    "senderid": mTalkzSenderId,
		    "number": requestMap.to,
		    "message": requestMap.msg,
		    "format": "json"
	    }
	};

	return await zohoHttpRequest('post', request).then(function(resp) {
     	return {resp: resp, requestMap: requestMap, req_data: req_data};     	
    });	

}	

async function popupCloseFunc() {
	await ZOHO.CRM.UI.Popup.closeReload();
}

async function popupSccuessExit(Success, requestMap) {

	if(!requestMap['status']) {
		if(!isBulk) {
			requestMap['status'] = 'Your SMS has been sent successfully.';
		}
		else {
			requestMap['status'] = 'Your Bulk SMS has been sent successfully.';
		}
	}

	if(!isBulk) {
		if(!Success)
		await setTimeout(async function() {	await wcConfirm(requestMap.status,'','Okay',true,false); }, 1500); 
		else {
			await wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingMsgSuccess">${verifiedTickSVG}</div><div class="sendingMsgSuccessHint">${requestMap.status}</div></div>`,'','Okay',true,true);
			await setTimeout(async function(){ await popupCloseFunc(); }, 1500);
		}
	}
	else {

		let currentRecordPosition = requestMap.count + 1;
		let sendingposition = ((100/currentRecords.length)*currentRecordPosition);

		if(!Success)
		await wcConfirm(`<div class="sendingLoaderOuter" style="height:180px;"><div class="sendingLoaderInner">${sendingLoader}</div>

			<div class="loadingStatusShowDiv">
			    <div class="loadingStatusShowDivStart"><div class="loadingStatusShowDivStartThis">${currentRecordPosition}</div><div class="loadingStatusShowDivStartDiv">/</div><div class="loadingStatusShowDivStartTot">${currentRecords.length}</div></div>
			        <div class="loadingStatusShowDivLoader"><div id="pageUploaderOuter">
			            <div value="0" max="100" id="pageUploader" style="width:${sendingposition}%;">
			                <div class="progress-shadow"></div>
			            </div>
			        </div>
			</div>
			    <div class="loadingStatusShowDivEnd">${Number(sendingposition).toFixed()}%</div>
			    </div>

    <div class="sendingLoaderHint">Sending... <br> Message ${scheduledTime? 'scheduled ':'sent '} ${'is failed for this '+phoneRecord.recipientName+'.'}</div></div>`,'','Okay',true,true); 
		else
		await wcConfirm(`<div class="sendingLoaderOuter" style="height:180px;"><div class="sendingLoaderInner">${sendingLoader}</div>

				<div class="loadingStatusShowDiv">
				    <div class="loadingStatusShowDivStart"><div class="loadingStatusShowDivStartThis">${currentRecordPosition}</div><div class="loadingStatusShowDivStartDiv">/</div><div class="loadingStatusShowDivStartTot">${currentRecords.length}</div></div>
				        <div class="loadingStatusShowDivLoader"><div id="pageUploaderOuter">
				            <div value="0" max="100" id="pageUploader" style="width:${sendingposition}%;">
				                <div class="progress-shadow"></div>
				            </div>
				        </div>
				</div>
				    <div class="loadingStatusShowDivEnd">${Number(sendingposition).toFixed()}%</div>
				    </div>

			<div class="sendingLoaderHint">Sending... <br> Message ${scheduledTime? 'scheduled ':'sent ' } ${'for this '+phoneRecord.recipientName+'.'}</div></div>`,'','Okay',true,true);

		if(currentRecords.length-1 == requestMap.count) {
			await setTimeout(async function() { 
				await wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingMsgSuccess">${verifiedTickSVG}</div><div class="sendingMsgSuccessHint">Your Bulk SMS has been ${scheduledTime? 'scheduled ':'sent ' }successfully.</div></div>`,'','Okay',true,true); 
				await setTimeout(async function() {	await popupCloseFunc(); }, 1500);
			}, 1500);	
		
		}
		
	}

}

async function sendSMS_Response(resp, requestMap, req_data) {
	
	await sendSMS_Response_smsChannel(resp, requestMap, req_data);

}

async function sendSMS_Response_smsChannel(resp, requestMap, req_data) {
	
	if(resp && resp.message) {

		if(resp.message == "message Submitted successfully") {

			req_data[extensionFieldStatus] = "Sent";
			req_data[extensionFieldName] = resp.msgid+"";
			requestMap['status'] = "Your SMS has been sent successfully.";
			await smsResponseToHistory(true, requestMap, req_data);
			
		}
		else if(resp.message == 'Invalid Api Key') {

			req_data[extensionFieldStatus] = resp.message+"";
			requestMap['status'] = resp.message+"";
			if(!isBulk) {
				await popupSccuessExit(false, requestMap);
			}
			else {
				await smsResponseToHistory(false, requestMap, req_data)
			}

		}
		else if(resp.message == 'Invalid senderid') {
			
			req_data[extensionFieldStatus] = resp.message+"";
			requestMap['status'] = resp.message+"";
			if(!isBulk) {
				await popupSccuessExit(false, requestMap);
			}
			else {
				await smsResponseToHistory(false, requestMap, req_data)
			}

		}							
		else {		

			req_data[extensionFieldStatus] = resp.message+"";
			requestMap['status'] = resp.message+"";
			if(!isBulk) {
				await popupSccuessExit(false, requestMap);
			}
			else {
				await smsResponseToHistory(false, requestMap, req_data)
			}	
			
		}

	}
	else {

		req_data[extensionFieldStatus]="failed";
		requestMap['status'] = 'Message sent is failed.';
		if(!isBulk) {
			await popupSccuessExit(false, requestMap);
		}
		else {
			await smsResponseToHistory(false, requestMap, req_data)
		}

	}

}

async function createHistory(requestMap, req_data) {

	return await createRecord(extensionHistory, req_data).then(function(resp) {
		return { resp: resp, requestMap: requestMap };
	});

}

function getMessageWithFields(message, currentRecord){
	
	moduleFields.forEach(function(field) {
		let replace = "${"+module+"."+field.field_label+"}";
		if(currentRecord[field.api_name] != null)
		{
			let value = currentRecord[field.api_name];
			if(field.data_type == "datetime")
			{
				value = new Date(value);
				toTimeSet = value.toLocaleTimeString().split(':')[0]+':'+value.toLocaleTimeString().split(':')[1]+' '+value.toLocaleTimeString().split(':')[2].slice(3);
				value = value.toDateString()+', '+toTimeSet;
			}
			if(value.name)
			{
				value = value.name;
			}
			
			message = message.replaceAll(replace,value);
		}
		else
		{
			message = message.replaceAll(replace,"");
		}
	});	
		
	if(selectedUser) {
		userFields.forEach(function(field){
			let replace = "${Users."+field.field_label+"}";
			if(selectedUser[field.api_name] != null)
			{
				let value = selectedUser[field.api_name];
				if(field.data_type == "datetime")
				{
					value = new Date(value);
					toTimeSet = value.toLocaleTimeString().split(':')[0]+':'+value.toLocaleTimeString().split(':')[1]+' '+value.toLocaleTimeString().split(':')[2].slice(3);
					value = value.toDateString()+', '+toTimeSet;
				}
				if(value.name)
				{
					value = value.name;
				}
				
				message = message.replaceAll(replace,value);
			}
			else
			{
				message = message.replaceAll(replace,"");
			}
		});
	}

	return message.replace(/ /g, ' ').trim();
	
}

async function checkMobileNumber(no, countryCode) {

	return await checkPhoneNumber(no).then(function(resp) {

			if(resp && resp.countryPrefix != null) {
	    		let phoneNumber = resp.phoneNumber.toString();
	    		countryCode = resp.countryPrefix.toString();
	    		no = phoneNumber.substring(phoneNumber.indexOf(countryCode) + countryCode.length);
				return countryCode+no;
	    	}
	    	else {
	    		return countryCode+no.replace(/\D/g,'');
	    	}	

	});
		
}

function setphoneRecordUpdate(updatePhoneRecord) {

	let recipientName = updatePhoneRecord[phoneRecord.api_name];

	if(phoneRecord.entity == "Contacts" || phoneRecord.entity == "Leads" ) {
		recipientName = updatePhoneRecord.Full_Name;
	}
	else if(phoneRecord.entity == "Accounts") {
		recipientName = updatePhoneRecord.Account_Name;
	}	
	phoneRecord['Mobile'] = updatePhoneRecord[phoneRecord.api_name];
	phoneRecord['recipientName'] = recipientName;
	phoneRecord['id'] = updatePhoneRecord.id;
	return;

}

async function getMobileNumber(currentRecord) {

	if(!phoneRecord || !phoneRecord.entity) {

		phoneRecord = {'Mobile': '', 'recipientName': '', 'id': ''};

	}
	else if(module == phoneRecord.entity) {
		setphoneRecordUpdate(currentRecord);
		return;
	}
	else {

		let moduleEntity = phoneRecord.entity;
		if(module == "Deals") {
			moduleEntity = phoneRecord.entity.slice(0, -1)+'_Name';
		}

		await getRecord(phoneRecord.entity, currentRecord[moduleEntity].id).then(function(contactData) {

			setphoneRecordUpdate(contactData.data[0]);
			return;

		});
	}
	
}    
		
		
function insert(bookingLink){

	if (sel && sel.rangeCount && isDescendant(sel.focusNode)){
		window.getSelection().deleteFromDocument();
        let range = sel.getRangeAt(0);
        range.collapse(true);
	    let span = document.createElement("span");
	    span.appendChild( document.createTextNode('${'+bookingLink.children[0].value+'}') );
		range.insertNode(span);
		range.setStartAfter(span);
        range.collapse(true);
        sel.removeAllRanges();
        sel.addRange(range);
    }   

    $('details').removeAttr("open"); 
    
}

// function isDescendant(child) {
// 	let parent = document.getElementById("emailContentEmail");
// 	let node = child.parentNode;
// 	while (node != null) {
// 	 if (node == parent || child == parent) {
// 	     return true;
// 	 }
// 	 node = node.parentNode;
// 	}
// 	return false;
// }

function isDescendant(child) {

	if($(sel.getRangeAt(0).commonAncestorContainer).hasClass('selectInsertTextField') || $(sel.getRangeAt(0).commonAncestorContainer.parentNode).hasClass('selectInsertTextField'))
	return true;
	else
	return false;

}

function enableSchedule(element){
	if(element.checked == true){
		document.getElementById("send").innerText="Schedule";
		let date = document.getElementById("datepicker").value;
		let time = document.getElementById("timeList").value;
		scheduledTime = new Date(date+" "+time).toISOString();
	}
	else{
		document.getElementById("send").innerText="Send";
		scheduledTime = undefined;
	}
}
		
function openDatePicker(){
	document.getElementById("dateTime").style.display= "flex";
}	

function scheduleClose(){
	let date = document.getElementById("datepicker").value;
	let time = document.getElementById("timeList").value;
	if(new Date(date+" "+time).getTime() < new Date().getTime()){
		wcConfirm('Schedule time should be greater than current time.','','Okay',true,false);
	}
	else{
		document.getElementById("dateTime").style.display = "none";
		document.getElementById("scheduleCheck").checked = true;
		document.getElementById("send").innerText = "Schedule";
		document.getElementById("scheduledDateTime").innerText = new Date(date).toDateString()+" at "+time +" ("+Intl.DateTimeFormat().resolvedOptions().timeZone+")";
		scheduledTime = new Date(date+" "+time).toISOString();
	}	
}

function cancel(){
	document.getElementById("dateTime").style.display= "none";
}

function msgBoxInsertFieldButtFunc(selected) {
	$(selected).parent().click();
}

function mainBodyClickFunction(e) {

    if($(e.target).hasClass('templateItem') || $(e.target).hasClass('dropdown-menu') || $(e.target).hasClass('dropListButt') || $(e.target).hasClass('choose') || $(e.target).hasClass('arrowIcon')){

    }
    else{
        $('.dropUlOut').removeAttr("open");
    }

    if($(e.target).hasClass('dropdown-item') || $(e.target).hasClass('dropdown-menu') || $(e.target).hasClass('dropdown_Butt') || $(e.target).hasClass('userFieldDivButt') || $(e.target).hasClass('msgBoxInsertFieldButt') || $(e.target).hasClass('dp_arrowIcon') || $(e.target).hasClass('selectedUserTick') || $(e.target).hasClass('selectedUserList') || $(e.target).hasClass('userListHead')){

    }
    else{
        $('.userFieldDiv').removeAttr("open");
    }

    if($(e.target).hasClass('dropdown-item') || $(e.target).hasClass('dropdown-menu') || $(e.target).hasClass('dropdown_Butt') || $(e.target).hasClass('moduleFieldDivButt') || $(e.target).hasClass('msgBoxInsertFieldButt') || $(e.target).hasClass('dp_arrowIcon') || $(e.target).hasClass('thisModuleName')){

    }
    else{
        $('.moduleFieldDiv').removeAttr("open");
    }

    if($(e.target).hasClass('templateItem') || $(e.target).hasClass('dropdown-menu') || $(e.target).hasClass('tempChoose') || $(e.target).hasClass('templateFieldDivButt') || $(e.target).hasClass('msgBoxInsertFieldButt') || $(e.target).hasClass('dp_arrowIcon')){

    }
    else{
        $('.templateFieldDiv').removeAttr("open");
    }

    if($(e.target).hasClass('templateItem') || $(e.target).hasClass('dropdown-menu') || $(e.target).hasClass('templateChoose') || $(e.target).hasClass('templateFieldDivButt') || $(e.target).hasClass('arrowIcon')){

    }
    else{
        $('.template_FieldDiv').removeAttr("open");
    }

    if($(e.target).hasClass('templateItem') || $(e.target).hasClass('dropdown-menu') || $(e.target).hasClass('senderChoose') || $(e.target).hasClass('senderFieldDivButt') || $(e.target).hasClass('arrowIcon')){

    }
    else{
        $('.sender_FieldDiv').removeAttr("open");
    }

}

function wcConfirm(htmlText, runFunc, confirmTitle, alart, buttonHide) {

	$('.wcConfirmOuter').remove();
    $('body').append(`<div class="wcConfirmOuter">
        <div class="wcConfirmInner divPopUpShow">
            <div class="wcConfirmBody">
                ${htmlText}
            </div>
            <div class="wcConfirmButt" style="display:${buttonHide?'none':'flex'};">
                <div class="wcConfirmCancel" style="display:${alart?'none':'block'};" onclick="wcConfirmHide()">Cancel</div>
                <div class="wcConfirmYes" onclick="wcConfirmHide();${runFunc}">${confirmTitle}</div>
            </div>
        </div>
    </div>`);

}

function wcConfirmHide() {

    $('.wcConfirmInner').removeClass('divPopUpHide').removeClass('divPopUpShow').addClass('divPopUpHide').hide();
    $('.wcConfirmOuter').fadeOut(0);
    setTimeout(function() { $('.wcConfirmInner').removeClass('divPopUpHide').removeClass('divPopUpShow').html('').hide().fadeOut(); $('.wcConfirmOuter').remove(); }, 300);

}

$(document).keypress(function(e) {

    if($('.wcConfirmYes').is(':visible'))
    if(e.which == 13) {
        $('.wcConfirmYes').click();
    }

});

async function customersModifyCustomisation() {

	if(credential.apikey  == 'B2vmVgOo3Vih14rT') {
		//$('.templateFieldDiv').show();
		$('#templateNotSelect').hide();
		$('#emailContentEmail').attr('contenteditable', 'true').attr('placeholder', 'Type your message here...');
		$('#senderList li').first().click();
		$('#countryCode option[value="91"]').prop('selected', 'selected').change();
	}
	
}