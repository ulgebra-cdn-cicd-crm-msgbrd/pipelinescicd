var appsConfig = {
    "AUTH_ID": undefined,
    "AUTH_TOKEN": undefined,
    "PHONE_NUMBER": undefined,
    "APPLICATION_ID": undefined,
    "APP_UNIQUE_ID": undefined
};
var initialAppsConfig = {
    "AUTH_ID": undefined,
    "AUTH_TOKEN": undefined,
    "PHONE_NUMBER": undefined,
    "APPLICATION_ID": undefined,
    "APP_UNIQUE_ID": undefined
};
var curId = 1000;
var errorId = 1000;
var initTries = 0;
var initProcessId = 1;
function resolveCurrentProcessView() {
    if(valueExists(appsConfig.PHONE_NUMBER)){
        $('#input-from-phone').val(appsConfig.PHONE_NUMBER);
    }
    else{
        var prefurl = `https://desk.zoho.com/support/${appsConfig.UA_DESK_PORTAL_NAME}/ShowHomePage.do#setup/marketplace/installed-extensions/${appsConfig.EXTENSION_ID}/preference`;
        showErroWindow("App is not configured yet", `Configure <b>SMS From phone number</b> in extension preference page to start sending SMS. <br><br><a href="${prefurl}" nofollow noopener><div class="help-step-btn bg-green">
                              <i class="material-icons">settings</i>  Go to preference page
                            </div></a>`);
    }
}
function resetPhoneNumerVal() {
    $('#phone-select-options').html("");
    $('#phone-select-label').text('Select a phone number');
    $('#phone-select-label').attr({'data-selectedval': 'null'});
}
function selectDropDownItem(elemId, val) {
    $('.dropdown-holder').hide();
    $('#' + elemId).text(val);
    $('#' + elemId).attr({'data-selectedval': val});
}

function showDropDown(elemId) {
    if($('#' + elemId).is(":visible")){
        $('#' + elemId).hide();
    }
    else{
        $('#' + elemId).show();
        if(elemId === 'phone-select-options'){
            fetchPhoneNumbersAndShow();
        }
    }
    
}
function valueExists(val) {
    return val !== null && val !== undefined && val.length > 1 && val!=="null";
}
function showTryAgainError(){
    showErroMessage('Try again later');
}
function showInvalidCredsError(){
    showErroMessage('Given Plivo Auth ID or Authtoken is Invalid <br><br> Try again with proper Plivo credentials from <a href="https://console.plivo.com/dashboard/" title="Click to go to plivo dashboard" target="_blank" noopener nofollow>Plivo dashboard</a>.');
}
function showContactsSearchPopUp(){
    $('.item-list-popup').show();
}
function searchContact(){
    searchContactsAndRender($('#contact-seach-name').val());
}
function searchContactsAndRender(searchTerm){
    if(!valueExists(searchTerm)){
        showErroWindow("Error","Enter search term");
        return;
    }
    var contactProcess = curId++;
    showProcess(`Searching contacts having phone numbers ...`, contactProcess);
                    var reqObj = {
                        url : `https://desk.zoho.com/api/v1/contacts/search?_all=*${encodeURI(searchTerm)}*&limit=99&phone=`+encodeURI('${notEmpty}'),
                        connectionLinkName : "readreceiptconnection",
                        type : "GET", 
                        headers:{
                            "orgId" : appsConfig.UA_DESK_ORG_ID
                        },
                        postBody:{}
                     };
                     ZOHODESK.request(reqObj).then(function(response){
                         $("#contact-search-items").html(" ");
                         processCompleted(contactProcess);
                         var responseJSON = JSON.parse(response);
                         if(responseJSON["statusCode"]===200 || responseJSON["statusCode"]===201){
                             var data = JSON.parse(responseJSON["response"]).statusMessage;
                             if(data === ""){
                                 $("#contact-search-items").html("No contacts found. <br><br> <span class=\"c-silver\"> This search will include <br> only contacts having phone numbers</span>");
                                 return;
                             }
                             if(!(data instanceof Object)){
                                 data = JSON.parse(data);
                             }
                             
                             var contactObjArray = data.data;
                             for(var i in contactObjArray){
                                 var obj = contactObjArray[i];
                                 var fullName = (valueExists(obj.firstName) ? obj.firstName : "") +" " + (valueExists(obj.lastName) ? obj.lastName : "");
                                 var itemHTML = `<div class="item-list-popup-item" onclick="selectPhoneNumber('${obj.phone}', '${fullName}')">
                    <span class="c-name"> ${fullName} </span> <span class="c-phone"> ${obj.phone} </span>
                </div>`;
                                 $("#contact-search-items").append(itemHTML);
                             }
                         }
                     }).catch(function(err){
                         processCompleted(contactProcess);
                         console.log(err);
                     });
                }

function selectPhoneNumber(number, name){
    $('#input-to-phone').val(number);
    hideElem('.item-list-popup');
}
function sendSMS(){
    var phoneNumber = ""+$('#input-to-phone').val().trim().replace(' ','').replace('(', '').replace('-', '');
    if(!valueExists(phoneNumber)){
        showTryAgainError("Error","Enter phone number");
        return;
    }
    var reqObj = {
                url: `https://api.plivo.com/v1/Account/${appsConfig.AUTH_ID}/Message/`,
                type: "POST",
                headers: {
                    "Authorization": 'Basic ' + btoa(appsConfig.AUTH_ID + ":" + appsConfig.AUTH_TOKEN),
                    "Content-Type" : "application/json"
                },
                postBody: {
                    "dst" : phoneNumber,
                    "text": $("#input-sms-content").val(),
                    "src" : appsConfig.PHONE_NUMBER
                }
            };
            var smsProcess = curId++;
            showProcess(`Sending SMS...`, smsProcess);
            ZOHODESK.request(reqObj).then(function (response) {
                processCompleted(smsProcess);
                var responseJSON = JSON.parse(response);
                if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201 || responseJSON["statusCode"] === 202) {
                    showErroWindow("SMS has been sent successfully", "SMS Sent to <b>"+phoneNumber+"</b>");
                }
                else if(responseJSON["statusCode"] === 401){
                     showInvalidCredsError();
                }
                else{
                    showErroWindow("Error Message from Plivo", JSON.parse(responseJSON['response']).error);
                }
            }).catch(function (err) {
                processCompleted(smsProcess);
                showTryAgainError();
                console.log(err);
            });
}
function fetchPhoneNumbersAndShow(adviseManualConfiguration) {
    if(!valueExists(appsConfig.AUTH_ID) || !valueExists(appsConfig.AUTH_TOKEN)){
        showErroMessage('Provide valid Plivo AUTH ID and AUTHTOKEN');
        return;
    }
    var reqObj = {
        url: `https://api.plivo.com/v1/Account/${appsConfig.AUTH_ID}/Number/`,
        type: "GET",
        headers: {
            "Authorization": 'Basic ' + btoa(appsConfig.AUTH_ID + ":" + appsConfig.AUTH_TOKEN)
        },
        postBody: {}
    };
    $('#phone-select-options').html(`<div class="statusMsg">Loading...</div>`);
    ZOHODESK.request(reqObj).then(function (response) {
        var responseJSON = JSON.parse(response);
        if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201) {
            var data = JSON.parse(responseJSON["response"]);
            if (!(data instanceof Object)) {
                data = JSON.parse(data);
            }
            $('#phone-select-options').html("");
            var phoneNumberArray = data.objects;
            var configuredIncomingNumbers = "";
            if(phoneNumberArray.length === 0){
                showErroWindow('No phone numbers has been purchased in Plivo', `Kindly purchase phone numbers in <a href="https://console.plivo.com/phone-numbers/search/" title="Click to go to purchase SMS number" target="_blank" noopener nofollow>Plivo phone numbers configuration</a> and try again<br><br><div class="help-step-btn bg-green" onclick="closeAllErrorWindows();fetchPhoneNumbersAndShow(true)">
                              <i class="material-icons">refresh</i>  Purchased Numbers, Check Again
                            </div>`);
                return;
            }
            else{
                for (var i in phoneNumberArray) {
                    var obj = phoneNumberArray[i];
                    if(valueExists(obj.application) && obj.application.indexOf(appsConfig.APPLICATION_ID)>1){
                        configuredIncomingNumbers += obj.number;
                    }
                    if(obj.sms_enabled){
                        $('#phone-select-options').append(`<div class="dropdown-select-item" onclick="selectDropDownItem('phone-select-label','${obj.number}')">${obj.country} - ${obj.number}</div>`);
                    }
                }
            }
            if(valueExists(configuredIncomingNumbers)){
                    $("#incoming-integ-status").removeClass('c-silver').addClass('c-green').html(`<i class="material-icons">check_circle</i> SMS Sent to ${configuredIncomingNumbers} will be converted as ticket`);
                }
                else{
                    if(adviseManualConfiguration ===  true){
                        showErroWindow('Associate phone numbers to Desk App in Plivo', `Kindly associate phone numbers to Zoho Desk application manually in <a href="https://console.plivo.com/phone-numbers/" title="Click to go to plivo dashboard" target="_blank" noopener nofollow>Plivo phone numbers configuration</a>.<br><br><div class="help-step-btn bg-green" onclick="closeAllErrorWindows();fetchPhoneNumbersAndShow(true)">
                              <i class="material-icons">refresh</i>  Done, Check Status
                            </div>`);
                    }
                    $("#incoming-integ-status").removeClass('c-green').addClass('c-silver').html('<i class="material-icons">error</i> None of Plivo phone numbers configured for this application');
                }
        }else if(responseJSON["statusCode"] === 401){
                     showInvalidCredsError();
                }
                else{
                    showErroWindow('Unable to Plivo fetch mobile numbers', 'Ensure you have purchased Plivo mobile numbers from <a href="https://console.plivo.com/phone-numbers/search/" title="Click to go to purchase SMS number" target="_blank" noopener nofollow>Plivo phone numbers configuration</a> or Try again later.');
                }
    }).catch(function (err) {
        showErroWindow('Unable to fetch Plivo mobile numbers', 'Ensure you have provided correct AUTH ID & AUTHTOKEN. <br> <br> Ensure you have purchased Plivo mobile numbers from <a href="https://console.plivo.com/phone-numbers/search/" title="Click to go to purchase SMS number" target="_blank" noopener nofollow>Plivo phone numbers configuration</a> or Try again later.');
        console.log(err);
    });
}
function closeAllErrorWindows(){
    $('.error-window-outer').remove();
}
function showProcess(text, id){
    $(".process-window-outer").show();
    $("#process-window-items").append(`<div id="process-item-${id}" class="process-window-item">${text}</div>`);
}

function showErroMessage(html){
    showErroWindow('Unable to process your request', html);
}
function showErroWindow(title, html){
    var id = errorId++;
   $('body').append(`<div class="error-window-outer" id="error-window-${id}">
            <div class="error-window-inner">
                <div class="error-window-title">
                    ${title}
                </div>
                <div class="error-window-detail">
                    ${html}
                </div>
                <div class="error-window-close" onclick="removeElem('#error-window-${id}')">
                   <i class="material-icons">close</i> Close
                </div>
            </div>
        </div>`);
}
function removeElem(sel){
    $(sel).remove();
}
function hideElem(sel){
    $(sel).hide();
}
function showHelpItem(helpId){
    $('#helpWindow').show();
    $('.help-window-item').hide();
    $("#"+helpId).show();
}
function processCompleted(id){
    $(`#process-item-${id}`).remove();
    if(($("#process-window-items").children().length) === 0){
        $(".process-window-outer").hide();
    }
}
function initializeFromConfigParams(){
    initTries++;
    if(initTries > 3){
        showErroMessage('Kindly refresh the page to start the configuration.');
        return;
    }
    var applicationProcess = curId++;
    if(initTries === 1){
        showProcess(`Initializing app ...`, initProcessId);
    }
        showProcess(`Fetching application configuration ...`, applicationProcess);
        ZOHODESK.get("extension.config").then(function (response) {
            processCompleted(applicationProcess);
            var data = response['extension.config'];
            for (var item in data) {
                var configname = data[item]['name'];
                var configValue = data[item]['value'];
                if(!valueExists(configValue)){
                    continue;
                }
                if (configname === 'AUTH_ID') {
                    appsConfig.AUTH_ID = configValue;
                    initialAppsConfig.AUTH_ID = configValue;
                }
                if (configname === 'AUTH_TOKEN') {
                    appsConfig.AUTH_TOKEN = configValue;
                    initialAppsConfig.AUTH_TOKEN = configValue;
                }
                if (configname === 'PHONE_NUMBER') {
                    appsConfig.PHONE_NUMBER = configValue;
                    initialAppsConfig.PHONE_NUMBER = configValue;
                    fetchPhoneNumbersAndShow();
                }
                if (configname === 'APPLICATION_ID') {
                    appsConfig.APPLICATION_ID = configValue;
                    initialAppsConfig.APPLICATION_ID = configValue;
                }
                if (configname === 'APP_UNIQUE_ID') {
                    appsConfig.APP_UNIQUE_ID = configValue;
                    initialAppsConfig.APPLICATION_ID = configValue;
                }
            }
            if(valueExists(appsConfig.APP_UNIQUE_ID)){
                processCompleted(initProcessId);
            }
            else{
                setTimeout(initializeFromConfigParams, 5000);
            }
            resolveCurrentProcessView();
        }).catch(function (err) {
            showTryAgainError();
            processCompleted(applicationProcess);
            console.log(err);
        });

}
window.onload = function () {
    ZOHODESK.extension.onload().then(function (App) {
        console.log(App);
        appsConfig.EXTENSION_ID = App.extensionID;
        initializeFromConfigParams();

        ZOHODESK.get("portal.id").then(function (response) {
            appsConfig.UA_DESK_ORG_ID = response['portal.id'];
        }).catch(function (err) {
            console.log(err);
        });
        ZOHODESK.get("portal.name").then(function (response) {
            appsConfig.UA_DESK_PORTAL_NAME = response['portal.name'];
        }).catch(function (err) {
            console.log(err);
        });
        ZOHODESK.get("portal.customDomainName").then(function(response){
            appsConfig.UA_DESK_CUSTOMDOMAIN_NAME = response['portal.customDomainName'];
        }).catch(function(err){
            console.log(err);
        });

    });
};