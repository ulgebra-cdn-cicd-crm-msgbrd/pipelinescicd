var appsConfig = {
    "ACCESS_KEY": undefined,
    "APP_UNIQUE_ID": undefined
};
var initialAppsConfig = {
    "ACCESS_KEY": undefined,
    "APP_UNIQUE_ID": undefined
};
var curId = 1000;
var errorId = 1000;
var initTries = 0;
var initProcessId = 1;
var serverURL = "https://sms.ulgebra.com";
function syncInputValues() {

}
function resolveCurrentProcessView() {
    $('#input-api-key').val(appsConfig.ACCESS_KEY);
    if (valueExists(appsConfig.ACCESS_KEY)) {
        $('#input-api-key').attr({'readonly':true}).val(appsConfig.ACCESS_KEY.substr(0,5)+"xxxxxxxxxxxxx");
        
        fetchWebhookInfoAndExecute((function(hookId){
            if(hookId === null){
                $("#incoming-integ-status").removeClass('c-orange').removeClass('c-green').removeClass('c-crimson').addClass('c-silver').html('<i class="material-icons">error</i> Reset configurate and save Access Key ');
            }else{
                $("#incoming-integ-status").removeClass('c-crimson').removeClass('c-orange').removeClass('c-silver').addClass('c-green').html('<i class="material-icons">error</i> MessageBird - Integration enabled');
            }
        }));
    }
    else {
        $('#input-api-key').removeAttr('readonly');
        $("#incoming-integ-status").removeClass('c-silver').removeClass('c-orange').removeClass('c-green').addClass('c-crimson').html('<i class="material-icons">error</i> Provide Access Key to enable integration');
    }
}
function showHelpWinow(manual){
    if(!valueExists(appsConfig.ACCESS_KEY)){
        showHelpItem('step-1');
    }
    else{
        if(manual){
            showHelpItem('step-youtube');
        }
    }
}
function resetPhoneNumerVal() {
    $('#phone-select-options').html("");
    $('#phone-select-label').text('Select a phone number');
    $('#phone-select-label').attr({'data-selectedval': 'null'});
}
function selectDropDownItem(elemId, val) {
    $('.dropdown-holder').hide();
    $('#' + elemId).text(val);
    $('#' + elemId).attr({'data-selectedval': val});
}

function showDropDown(elemId) {
    if($('#' + elemId).is(":visible")){
        $('#' + elemId).hide();
    }
    else{
        $('#' + elemId).show();
        if(elemId === 'phone-select-options'){
            fetchPhoneNumbersAndShow();
        }
    }
    
}
function valueExists(val) {
    return val !== null && val !== undefined && val.length > 1 && val!=="null";
}
function saveAPIKey() {
    
    var accessKey = $('#input-api-key').val();
    
    if(!valueExists(accessKey)){
        showErroWindow('Access Key is missing','Kindly provide AUTH ID, then proceed to save.');
        return;
    }
    
    var authtokenChange = !valueExists(appsConfig.ACCESS_KEY);
    if(authtokenChange){
        
        var reqObj = {
                url: `https://rest.messagebird.com/balance`,
                type: "GET",
                headers: {
                    "Authorization": "AccessKey "+accessKey
                },
                postBody: {}
            };
            var phoneNumberProcess = curId++;
            showProcess(`Validating given configuration...`, phoneNumberProcess);
            ZOHODESK.request(reqObj).then(function (response) {
                var responseJSON = JSON.parse(response);
                processCompleted(phoneNumberProcess);
                if (responseJSON["statusCode"] === 200) {
                    var authIdProcess = curId++;
                    showProcess('Saving MessageBird Access Key ...', authIdProcess);
                    appsConfig.ACCESS_KEY = accessKey;
                    ZOHODESK.set('extension.config', {name: 'ACCESS_KEY', value: accessKey}).then(function (res) {
                        console.log(res);
                        processCompleted(authIdProcess);
                        createApplicationInMessageBird();
                        resolveCurrentProcessView();
                    }).catch(function (err) {
                        processCompleted(authIdProcess);
                        console.log(err);
                    });
                }
                else if(responseJSON["statusCode"] === 401){
                     showInvalidCredsError();
                }
                else{
                    showTryAgainError();
                }
            }).catch(function (err) {
                processCompleted(phoneNumberProcess);
                showTryAgainError();
                console.log(err);
            });
        }
}
function showTryAgainError(){
    showErroMessage('Try again later');
}
function showInvalidCredsError(){
    showErroMessage('Given MessageBird is Invalid <br><br> Try again with proper MessageBird credentials from <a href="https://dashboard.messagebird.com/en/developers/access" title="Click to go to MessageBird dashboard" target="_blank" noopener nofollow>MessageBird dashboard</a>.');
}


function resetAllFields(){
    if(valueExists(appsConfig.ACCESS_KEY)){
    ZOHODESK.set('extension.config', {name: 'ACCESS_KEY', value: "null"}).then(function (res) {
        fetchWebhookInfoAndExecute(deleteApplicationInMessageBird);
       // appsConfig.ACCESS_KEY = null;
                        console.log(res);
                        resolveCurrentProcessView();
                    }).catch(function (err) {
                        showTryAgainError();
                        console.log(err);
                    });
                }
}

function fetchWebhookInfoAndExecute(callback){
    var reqObj = {
        url: `https://conversations.messagebird.com/v1/webhooks`,
        type: "GET",
        headers: {
            "Authorization": "AccessKey "+(appsConfig.ACCESS_KEY === null ? initialAppsConfig.ACCESS_KEY : appsConfig.ACCESS_KEY)
        },
        postBody: {}
    };
    var applicationProcess = curId++;
    showProcess(`Checking desk application in MessageBird ...`, applicationProcess);
    ZOHODESK.request(reqObj).then(function (response) {
        processCompleted(applicationProcess);
        var responseJSON = JSON.parse(response);
        if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201 || responseJSON["statusCode"] === 204 || responseJSON["statusCode"] === 404) {
            var data = JSON.parse(responseJSON["response"]);
            if (!(data instanceof Object)) {
                data = JSON.parse(data);
            }
            if(data.count === 0){
                return callback(null);
            }
            else{
              for(var i in data.items){
                  var webhookJSON = data.items[i];
                  if(webhookJSON.url === getWebhookURL()){
                      callback(webhookJSON.id);
                      return;
                  }
              }  
            }
        }
        else if(responseJSON["statusCode"] === 401){
                     showInvalidCredsError();
                }
                else{
                    showTryAgainError();
                }
    }).catch(function (err) {
        showTryAgainError();
        processCompleted(applicationProcess);
        console.log(err);
    });
}

function getWebhookURL(){
    return `${serverURL}/public/callbacks/messagebird/zohodesk?appOrgId=${appsConfig.UA_DESK_ORG_ID}&appSecurityContext=${appsConfig.APP_UNIQUE_ID}`;
}

function deleteApplicationInMessageBird(webhookId){
    if(!valueExists(webhookId)){
        return;
    }
    var reqObj = {
        url: `https://conversations.messagebird.com/v1/webhooks/${webhookId}/`,
        type: "DELETE",
        headers: {
            "Authorization": "AccessKey "+(appsConfig.ACCESS_KEY === null ? initialAppsConfig.ACCESS_KEY : appsConfig.ACCESS_KEY)
        },
        postBody: {}
    };
    var applicationProcess = curId++;
    showProcess(`Removing existing desk application in MessageBird ...`, applicationProcess);
    ZOHODESK.request(reqObj).then(function (response) {
        processCompleted(applicationProcess);
        var responseJSON = JSON.parse(response);
        if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201 || responseJSON["statusCode"] === 204 || responseJSON["statusCode"] === 404) {
            resolveCurrentProcessView();
            window.location.reload();
        }
    }).catch(function (err) {
        showTryAgainError();
        processCompleted(applicationProcess);
        console.log(err);
    });
}

function closeAllErrorWindows(){
    $('.error-window-outer').remove();
}

function createApplicationInMessageBird(){
    var callbackURL = getWebhookURL();
    var reqObj = {
        url: `https://conversations.messagebird.com/v1/webhooks`,
        type: "POST",
        headers: {
            "Authorization": "AccessKey "+appsConfig.ACCESS_KEY,
            "Content-Type": "application/json"
        },
        postBody: {
            "url" : callbackURL,
            "events": ["message.created", "message.updated"]
        }
    };
    var applicationProcess = curId++;
    showProcess(`Configuring desk application in MessageBird ...`, applicationProcess);
    ZOHODESK.request(reqObj).then(function (response) {
        processCompleted(applicationProcess);
        var responseJSON = JSON.parse(response);
        if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201) {
            resolveCurrentProcessView(); 
        }
        else if (responseJSON["statusCode"] === 401){
            showInvalidCredsError();
        }
        else{
            showTryAgainError();
        }
    }).catch(function (err) {
        showTryAgainError();
        processCompleted(applicationProcess);
        console.log(err);
    });
}

function showProcess(text, id){
    $(".process-window-outer").show();
    $("#process-window-items").append(`<div id="process-item-${id}" class="process-window-item">${text}</div>`);
}

function showErroMessage(html){
    showErroWindow('Unable to process your request', html);
}
function showErroWindow(title, html){
    var id = errorId++;
   $('body').append(`<div class="error-window-outer" id="error-window-${id}">
            <div class="error-window-inner">
                <div class="error-window-title">
                    ${title}
                </div>
                <div class="error-window-detail">
                    ${html}
                </div>
                <div class="error-window-close" onclick="removeElem('#error-window-${id}')">
                   <i class="material-icons">close</i> Close
                </div>
            </div>
        </div>`);
}
function removeElem(sel){
    $(sel).remove();
}
function hideElem(sel){
    $(sel).hide();
}
function showHelpItem(helpId){
    $('#helpWindow').show();
    $('.help-window-item').hide();
    $("#"+helpId).show();
}
function processCompleted(id){
    $(`#process-item-${id}`).remove();
    if(($("#process-window-items").children().length) === 0){
        $(".process-window-outer").hide();
    }
}
function initializeFromConfigParams(){
    initTries++;
    if(initTries > 3){
    	showErroMessage('Kindly refresh the page to start the configuration. If this error continues, kindly re - authorize the extension and try preference tab again. <br> <a target="_blank" title="Click to View Image" href="https://puplicfiles.s3.us-east-2.amazonaws.com/uhoioe8y28g79e/inconsistent_app_init.png"><img id="inconst_error_img" src="https://puplicfiles.s3.us-east-2.amazonaws.com/uhoioe8y28g79e/inconsistent_app_init.png" style="max-width:80%;max-height:300px"/></a>');
        return;
    }
    var applicationProcess = curId++;
    if(initTries === 1){
        showProcess(`Initializing app ...`, initProcessId);
    }
        showProcess(`Fetching application configuration ...`, applicationProcess);
        ZOHODESK.get("extension.config").then(function (response) {
            processCompleted(applicationProcess);
            var data = response['extension.config'];
            for (var item in data) {
                var configname = data[item]['name'];
                var configValue = data[item]['value'];
                if(!valueExists(configValue)){
                    continue;
                }
                if (configname === 'ACCESS_KEY') {
                    appsConfig.ACCESS_KEY = configValue;
                    initialAppsConfig.ACCESS_KEY = configValue;
                }
                if (configname === 'APP_UNIQUE_ID') {
                    appsConfig.APP_UNIQUE_ID = configValue;
                    initialAppsConfig.APP_UNIQUE_ID = configValue;
                }
            }
            showHelpWinow(false);
            if(valueExists(appsConfig.APP_UNIQUE_ID)){
                processCompleted(initProcessId);
            }
            else{
                setTimeout(initializeFromConfigParams, 5000);
            }
            resolveCurrentProcessView();
        }).catch(function (err) {
            showTryAgainError();
            processCompleted(applicationProcess);
            console.log(err);
        });

}
window.onload = function () {
    ZOHODESK.extension.onload().then(function (App) {
        
        initializeFromConfigParams();

        ZOHODESK.get("portal.id").then(function (response) {
            appsConfig.UA_DESK_ORG_ID = response['portal.id'];
        }).catch(function (err) {
            console.log(err);
        });
        
        var imported = document.createElement('script');
        imported.src = 'https://ulgebra-cdn-cicd-plv.gitlab.io/pipelinescicd/ext/js/misc.js';
        document.head.appendChild(imported);

    });
};