var appsConfig = {
    "AUTH_ID": undefined,
    "AUTH_TOKEN": undefined,
    "PHONE_NUMBER": undefined,
    "APPLICATION_ID": undefined,
    "APP_UNIQUE_ID": undefined
};
var initialAppsConfig = {
    "AUTH_ID": undefined,
    "AUTH_TOKEN": undefined,
    "PHONE_NUMBER": undefined,
    "APPLICATION_ID": undefined,
    "APP_UNIQUE_ID": undefined
};
var curId = 1000;
var errorId = 1000;
var initTries = 0;
var initProcessId = 1;
function syncInputValues() {
//    appsConfig.AUTH_ID = $('#input-api-key').val();
//    appsConfig.AUTH_TOKEN = $('#input-api-authtoken').val();
//    var authtokenChange = initialAppsConfig.AUTH_ID !== appsConfig.AUTH_ID || initialAppsConfig.AUTH_TOKEN !== appsConfig.AUTH_TOKEN;
//    if(authtokenChange){
//        appsConfig.PHONE_NUMBER = "null";
//    }
//    resolveCurrentProcessView();
}
function resolveCurrentProcessView() {
    $('#input-api-key').val(appsConfig.AUTH_ID);
    $('#input-api-authtoken').val("");
    $('#phone-select-label').text(appsConfig.PHONE_NUMBER);
    if (valueExists(appsConfig.AUTH_ID) && valueExists(appsConfig.AUTH_TOKEN)) {
        $('#input-api-key').attr({'readonly':true});
        $('#input-api-authtoken').attr({'readonly':true}).val(appsConfig.AUTH_TOKEN.substr(0,5)+"xxxxxxxxxxxxx");
            $('#show-on-apikey-valid').show();
            if(!valueExists(appsConfig.PHONE_NUMBER)){
                resetPhoneNumerVal();
                $("#incoming-integ-status").removeClass('c-orange').removeClass('c-green').removeClass('c-crimson').addClass('c-silver').html('<i class="material-icons">error</i> Configure <b class="c-black">SMS - From phone number</b> to enable ticket conversion ');
            }
            else{
                $("#incoming-integ-status").removeClass('c-crimson').removeClass('c-green').removeClass('c-silver').addClass('c-orange').html('<i class="material-icons">error</i> Integration enabled');
            }
    }
    else {
        $('#input-api-key').removeAttr('readonly');
        $('#input-api-authtoken').removeAttr('readonly');
        $("#incoming-integ-status").removeClass('c-silver').removeClass('c-orange').removeClass('c-green').addClass('c-crimson').html('<i class="material-icons">error</i> Provide AUTH ID & Auth Token');
        $('#show-on-apikey-valid').hide();
    }
}
function showHelpWinow(manual){
    if(!valueExists(appsConfig.AUTH_ID)){
        showHelpItem('step-1');
    }
    else if(manual && !valueExists(appsConfig.PHONE_NUMBER)){
            showHelpItem('step-3');
    }
    else{
        if(manual){
            showHelpItem('step-youtube');
        }
    }
}
function resetPhoneNumerVal() {
    $('#phone-select-options').html("");
    $('#phone-select-label').text('Select a phone number');
    $('#phone-select-label').attr({'data-selectedval': 'null'});
}
function selectDropDownItem(elemId, val) {
    $('.dropdown-holder').hide();
    $('#' + elemId).text(val);
    $('#' + elemId).attr({'data-selectedval': val});
}

function showDropDown(elemId) {
    if($('#' + elemId).is(":visible")){
        $('#' + elemId).hide();
    }
    else{
        $('#' + elemId).show();
        if(elemId === 'phone-select-options'){
            fetchPhoneNumbersAndShow();
        }
    }
    
}
function valueExists(val) {
    return val !== null && val !== undefined && val.length > 1 && val!=="null";
}
function saveAPIKey() {
    
    var authID = $('#input-api-key').val();
    var authToken = $('#input-api-authtoken').val();
    
    var phoneNumber = $('#phone-select-label').attr('data-selectedval');
    
    if(!valueExists(authID)){
        showErroWindow('AUTH ID is missing','Kindly provide AUTH ID, then proceed to save.');
        return;
    }
    
    if(!valueExists(authToken)){
        showErroWindow('AUTHTOKEN ID is missing','Kindly provide AUTHTOKEN, then proceed to save.');
        return;
    }
    
    /*if(!valueExists(appsConfig.PHONE_NUMBER)){
        showErroWindow('Reselect Phone Number','Kindly select phone number again, then proceed to save.');
        return;
    }*/
    var authtokenChange = !valueExists(appsConfig.AUTH_ID);
    var phoneChange = true;
    var configProcess = curId++;
    if(authtokenChange){
        
        var reqObj = {
                url: `https://api.plivo.com/v1/Account/${authID}/`,
                type: "GET",
                headers: {
                    "Authorization": 'Basic ' + btoa(authID + ":" + authToken)
                },
                postBody: {}
            };
            var phoneNumberProcess = curId++;
            showProcess(`Validating given configuration...`, phoneNumberProcess);
            ZOHODESK.request(reqObj).then(function (response) {
                var responseJSON = JSON.parse(response);
                processCompleted(phoneNumberProcess);
                if (responseJSON["statusCode"] === 200) {
                    var authIdProcess = curId++;
                    showProcess('Saving Plivo AUTH ID ...', authIdProcess);
                    appsConfig.AUTH_ID = authID;
                    appsConfig.AUTH_TOKEN = authToken;
                    ZOHODESK.set('extension.config', {name: 'AUTH_ID', value: authID}).then(function (res) {
                        console.log(res);
                        processCompleted(authIdProcess);
                        resolveCurrentProcessView();
                    }).catch(function (err) {
                        processCompleted(authIdProcess);
                        console.log(err);
                    });
                    var authTokenProcess = curId++;
                    showProcess('Saving Plivo AUTHTOKEN ...', authTokenProcess);
                    ZOHODESK.set('extension.config', {name: 'AUTH_TOKEN', value: authToken}).then(function (res) {
                        console.log(res);
                        processCompleted(authTokenProcess);
                        if(valueExists(appsConfig.APP_UNIQUE_ID)){
                            createApplicationInPlivo();
                            resolveCurrentProcessView();
                        }
                        else{
                            showErroWindow('Error','Kindly reinstall the app.');
                        }
                        
                    }).catch(function (err) {
                        processCompleted(authTokenProcess);
                        console.log(err);
                    });
                }
                else if(responseJSON["statusCode"] === 401){
                     showInvalidCredsError();
                }
                else{
                    showTryAgainError();
                }
            }).catch(function (err) {
                processCompleted(phoneNumberProcess);
                showTryAgainError();
                console.log(err);
            });
                    
            }else{
                if(phoneChange){
                    if(!valueExists(phoneNumber)){
                        showErroWindow('Reselect Phone Number','Kindly select phone number again, then proceed to save.');
                        return;
                    }
                    var phoneNumberProcess = curId++;
                    showProcess('Saving Plivo Phone Number ...', phoneNumberProcess);
                    ZOHODESK.set('extension.config', {name: 'PHONE_NUMBER', value: phoneNumber}).then(function (res) {
                        console.log(res);
                        appsConfig.PHONE_NUMBER = phoneNumber;
                        processCompleted(phoneNumberProcess);
                        resetPhoneNumerVal();
                        resolveCurrentProcessView();
                        associateAPPToPhoneNumber();
                    }).
                    catch(function (err) {
                        showTryAgainError();
                        processCompleted(phoneNumberProcess);
                        console.log(err);
                    });
                }
                processCompleted(configProcess);
            }
}
function showTryAgainError(){
    showErroMessage('Try again later');
}
function showInvalidCredsError(){
    showErroMessage('Given Plivo Auth ID or Authtoken is Invalid <br><br> Try again with proper Plivo credentials from <a href="https://console.plivo.com/dashboard/" title="Click to go to plivo dashboard" target="_blank" noopener nofollow>Plivo dashboard</a>.');
}
function updateParamsInDesk(){
    var authtokenChange = initialAppsConfig.AUTH_ID !== appsConfig.AUTH_ID && initialAppsConfig.AUTH_TOKEN !== appsConfig.AUTH_TOKEN;
    var phoneChange = initialAppsConfig.PHONE_NUMBER !== appsConfig.PHONE_NUMBER;
    if(authtokenChange){
    var authIdProcess = curId++;
                    showProcess('Saving Plivo AUTH ID ...', authIdProcess);
                    ZOHODESK.set('extension.config', {name: 'AUTH_ID', value: appsConfig.AUTH_ID}).then(function (res) {
                        console.log(res);
                        processCompleted(authIdProcess);
                        resolveCurrentProcessView();
                    }).catch(function (err) {
                        processCompleted(authIdProcess);
                        console.log(err);
                    });
                    var authTokenProcess = curId++;
                    showProcess('Saving Plivo AUTHTOKEN ...', authTokenProcess);
                    ZOHODESK.set('extension.config', {name: 'AUTH_TOKEN', value: appsConfig.AUTH_TOKEN}).then(function (res) {
                        console.log(res);
                        processCompleted(authTokenProcess);
                        if(!valueExists(appsConfig.APPLICATION_ID) && valueExists(appsConfig.APP_UNIQUE_ID)){
                            createApplicationInPlivo();
                        }
                    }).catch(function (err) {
                        processCompleted(authTokenProcess);
                        console.log(err);
                    });
                }
                if(phoneChange){
                    var phoneNumberProcess = curId++;
                    showProcess('Saving Plivo Phone Number ...', phoneNumberProcess);
                    ZOHODESK.set('extension.config', {name: 'PHONE_NUMBER', value: appsConfig.PHONE_NUMBER}).then(function (res) {
                        console.log(res);
                        processCompleted(phoneNumberProcess);
                        resetPhoneNumerVal();
                        resolveCurrentProcessView();
                        associateAPPToPhoneNumber();
                    }).
                    catch(function (err) {
                        showTryAgainError();
                        processCompleted(phoneNumberProcess);
                        console.log(err);
                    });
                }else{
                    resetPhoneNumerVal();
                        resolveCurrentProcessView();
                        associateAPPToPhoneNumber();
                }
}

function associateAPPToPhoneNumber(){
    if(!valueExists(appsConfig.APPLICATION_ID)){
        showErroWindow('Something went wrong', 'Try resetting the app configuration and try again <br><br><div class="help-step-btn bg-crimson" onclick="closeAllErrorWindows();resetAllFields()">Reset app configuration</div>');
        return;
    }
     var reqObj = {
        url: `https://api.plivo.com/v1/Account/${appsConfig.AUTH_ID}/Number/${appsConfig.PHONE_NUMBER}/`,
        type: "POST",
        headers: {
            "Authorization": 'Basic ' + btoa(appsConfig.AUTH_ID + ":" + appsConfig.AUTH_TOKEN),
            "Content-Type": "application/json"
        },
        postBody: {
            "app_id" : appsConfig.APPLICATION_ID
        }
    };
    //$('#phone-select-options').html(`<div class="statusMsg">Loading...</div>`);
    var phoneNumberProcess = curId++;
    showProcess(`Configuring Incoming SMS for ${appsConfig.PHONE_NUMBER} to Desk tickets ...`, phoneNumberProcess);
    ZOHODESK.request(reqObj).then(function (response) {
        var responseJSON = JSON.parse(response);
        processCompleted(phoneNumberProcess);
        if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201 || responseJSON["statusCode"] === 202) {
            fetchPhoneNumbersAndShow(true);
        }
        else{
            showErroWindow('Associate phone numbers to Desk App in Plivo', `Kindly associate phone numbers to Zoho Desk application manually in <a href="https://console.plivo.com/phone-numbers/" title="Click to go to plivo dashboard" target="_blank" noopener nofollow>Plivo phone numbers configuration</a>.`);
        }
    }).catch(function (err) {
        showTryAgainError();
        processCompleted(phoneNumberProcess);
        console.log(err);
    });
}

function resetAllFields(){
    if(valueExists(initialAppsConfig.AUTH_ID)){
    ZOHODESK.set('extension.config', {name: 'AUTH_ID', value: "null"}).then(function (res) {
        appsConfig.AUTH_ID = null;
                        console.log(res);
                        resolveCurrentProcessView();
                    }).catch(function (err) {
                        showTryAgainError();
                        console.log(err);
                    });
                }
                if(valueExists(initialAppsConfig.AUTH_TOKEN)){
                    ZOHODESK.set('extension.config', {name: 'AUTH_TOKEN', value: "null"}).then(function (res) {
                        appsConfig.AUTH_TOKEN = null;
                        console.log(res);
                        resolveCurrentProcessView();
                    }).catch(function (err) {
                        showTryAgainError();
                        console.log(err);
                    });
                }
                if(valueExists(initialAppsConfig.PHONE_NUMBER)){
                    ZOHODESK.set('extension.config', {name: 'PHONE_NUMBER', value: "null"}).then(function (res) {
                        appsConfig.PHONE_NUMBER = null;
                        console.log(res);
                        resolveCurrentProcessView();
                    }).catch(function (err) {
                        showTryAgainError();
                        console.log(err);
                    });
                    deleteApplicationInPlivo();
                }
}
function deleteApplicationInPlivo(){
    if(!valueExists(appsConfig.APPLICATION_ID)){
        return;
    }
    var reqObj = {
        url: `https://api.plivo.com/v1/Account/${appsConfig.AUTH_ID}/Application/${appsConfig.APPLICATION_ID}/`,
        type: "DELETE",
        headers: {
            "Authorization": 'Basic ' + btoa(appsConfig.AUTH_ID + ":" + appsConfig.AUTH_TOKEN)
        },
        postBody: {}
    };
    var applicationProcess = curId++;
    showProcess(`Removing desk application in Plivo ...`, applicationProcess);
    ZOHODESK.request(reqObj).then(function (response) {
        processCompleted(applicationProcess);
        var responseJSON = JSON.parse(response);
        if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201 || responseJSON["statusCode"] === 204 || responseJSON["statusCode"] === 404) {
             ZOHODESK.set('extension.config', {name: 'APPLICATION_ID', value: "null"}).then(function (res) {
                        appsConfig.APPLICATION_ID = "null";
                        console.log(res);
                        resolveCurrentProcessView();
                    }).catch(function (err) {
                        showTryAgainError();
                        console.log(err);
                    });
        }
    }).catch(function (err) {
        showTryAgainError();
        processCompleted(applicationProcess);
        console.log(err);
    });
}

function fetchPhoneNumbersAndShow(adviseManualConfiguration) {
    if(!valueExists(appsConfig.AUTH_ID) || !valueExists(appsConfig.AUTH_TOKEN)){
        showErroMessage('Provide valid Plivo AUTH ID and AUTHTOKEN');
        return;
    }
    var reqObj = {
        url: `https://api.plivo.com/v1/Account/${appsConfig.AUTH_ID}/Number/`,
        type: "GET",
        headers: {
            "Authorization": 'Basic ' + btoa(appsConfig.AUTH_ID + ":" + appsConfig.AUTH_TOKEN)
        },
        postBody: {}
    };
    $('#phone-select-options').html(`<div class="statusMsg">Loading...</div>`);
    ZOHODESK.request(reqObj).then(function (response) {
        var responseJSON = JSON.parse(response);
        if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201) {
            var data = JSON.parse(responseJSON["response"]);
            if (!(data instanceof Object)) {
                data = JSON.parse(data);
            }
            $('#phone-select-options').html("");
            var phoneNumberArray = data.objects;
            var configuredIncomingNumbers = "";
            if(phoneNumberArray.length === 0){
                showErroWindow('No phone numbers has been purchased in Plivo', `Kindly purchase phone numbers in <a href="https://console.plivo.com/phone-numbers/search/" title="Click to go to purchase SMS number" target="_blank" noopener nofollow>Plivo phone numbers configuration</a> and try again<br><br><div class="help-step-btn bg-green" onclick="closeAllErrorWindows();fetchPhoneNumbersAndShow(true)">
                              <i class="material-icons">refresh</i>  Purchased Numbers, Check Again
                            </div>`);
                return;
            }
            else{
                for (var i in phoneNumberArray) {
                    var obj = phoneNumberArray[i];
                    if(valueExists(obj.application) && obj.application.indexOf(appsConfig.APPLICATION_ID)>1){
                        configuredIncomingNumbers += obj.number;
                    }
                    if(obj.sms_enabled){
                        $('#phone-select-options').append(`<div class="dropdown-select-item" onclick="selectDropDownItem('phone-select-label','${obj.number}')">${obj.country} - ${obj.number}</div>`);
                    }
                }
            }
            if(valueExists(configuredIncomingNumbers)){
                    $("#incoming-integ-status").removeClass('c-orange').removeClass('c-crimson').removeClass('c-silver').addClass('c-green').html(`<i class="material-icons">check_circle</i> SMS Sent to ${configuredIncomingNumbers} will be converted as ticket`);
                }
                else{
                    if(adviseManualConfiguration ===  true){
                        showErroWindow('Associate phone numbers to Desk App in Plivo', `Kindly associate phone numbers to Zoho Desk application manually in <a href="https://console.plivo.com/phone-numbers/" title="Click to go to plivo dashboard" target="_blank" noopener nofollow>Plivo phone numbers configuration</a>.<br><br><div class="help-step-btn bg-green" onclick="closeAllErrorWindows();fetchPhoneNumbersAndShow(true)">
                              <i class="material-icons">refresh</i>  Done, Check Status
                            </div>`);
                    }
                    $("#incoming-integ-status").removeClass('c-orange').removeClass('c-crimson').removeClass('c-green').addClass('c-silver').html('<i class="material-icons">error</i> None of Plivo phone numbers configured for this application');
                }
        }else if(responseJSON["statusCode"] === 401){
                     showInvalidCredsError();
                }
                else{
                    showErroWindow('Unable to Plivo fetch mobile numbers', 'Ensure you have purchased Plivo mobile numbers from <a href="https://console.plivo.com/phone-numbers/search/" title="Click to go to purchase SMS number" target="_blank" noopener nofollow>Plivo phone numbers configuration</a> or Try again later.');
                }
    }).catch(function (err) {
        showErroWindow('Unable to fetch Plivo mobile numbers', 'Ensure you have provided correct AUTH ID & AUTHTOKEN. <br> <br> Ensure you have purchased Plivo mobile numbers from <a href="https://console.plivo.com/phone-numbers/search/" title="Click to go to purchase SMS number" target="_blank" noopener nofollow>Plivo phone numbers configuration</a> or Try again later.');
        console.log(err);
    });
}
function closeAllErrorWindows(){
    $('.error-window-outer').remove();
}

function createApplicationInPlivo(){
    var callbackURL = `https://sms.ulgebra.com/public/callbacks/plivo/zohodesk?appOrgId=${appsConfig.UA_DESK_ORG_ID}&appSecurityContext=${appsConfig.APP_UNIQUE_ID}`;
    var reqObj = {
        url: `https://api.plivo.com/v1/Account/${appsConfig.AUTH_ID}/Application/`,
        type: "POST",
        headers: {
            "Authorization": 'Basic ' + btoa(appsConfig.AUTH_ID + ":" + appsConfig.AUTH_TOKEN),
            "Content-Type": "application/json"
        },
        postBody: {
            "app_name" : "Zoho Desk Integration",
            "answer_url": callbackURL+"&action=answer",
            "message_url" : callbackURL+"&action=message",
            "default_number_app" : true,
            "default_endpoint_app": true
        }
    };
    var applicationProcess = curId++;
    showProcess(`Configuring desk application in Plivo ...`, applicationProcess);
    ZOHODESK.request(reqObj).then(function (response) {
        processCompleted(applicationProcess);
        var responseJSON = JSON.parse(response);
        if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201) {
            var data = JSON.parse(responseJSON["response"]);
            console.log(data);
            appsConfig.APPLICATION_ID = data.app_id;
            if(valueExists(appsConfig.APPLICATION_ID)){
                ZOHODESK.set('extension.config', {name: 'APPLICATION_ID', value: appsConfig.APPLICATION_ID}).then(function (res) {
                    resolveCurrentProcessView();
                        console.log(res);
                    }).catch(function (err) {
                        console.log(err);
                    });
            }
        }
    }).catch(function (err) {
        showTryAgainError();
        processCompleted(applicationProcess);
        console.log(err);
    });
}

function showProcess(text, id){
    $(".process-window-outer").show();
    $("#process-window-items").append(`<div id="process-item-${id}" class="process-window-item">${text}</div>`);
}

function showErroMessage(html){
    showErroWindow('Unable to process your request', html);
}
function showErroWindow(title, html){
    var id = errorId++;
   $('body').append(`<div class="error-window-outer" id="error-window-${id}">
            <div class="error-window-inner">
                <div class="error-window-title">
                    ${title}
                </div>
                <div class="error-window-detail">
                    ${html}
                </div>
                <div class="error-window-close" onclick="removeElem('#error-window-${id}')">
                   <i class="material-icons">close</i> Close
                </div>
            </div>
        </div>`);
}
function removeElem(sel){
    $(sel).remove();
}
function hideElem(sel){
    $(sel).hide();
}
function showHelpItem(helpId){
    $('#helpWindow').show();
    $('.help-window-item').hide();
    $("#"+helpId).show();
}
function processCompleted(id){
    $(`#process-item-${id}`).remove();
    if(($("#process-window-items").children().length) === 0){
        $(".process-window-outer").hide();
    }
}
function initializeFromConfigParams(){
    initTries++;
    if(initTries > 3){
    	showErroWindow('Kindly authorize the app again','<div style="margin-top:-20px;">Go to <b style="color:royalblue">GENERAL SETTINGS</b> tab above. Then  click <b style="color:royalblue">Revoke</b> button and authroize again. Then visit this tab. Refer the image below. <br><br> <a target="_blank" title="Click to View Image" href="https://puplicfiles.s3.us-east-2.amazonaws.com/uhoioe8y28g79e/inconsistent_app_init.png"><img id="inconst_error_img" src="https://puplicfiles.s3.us-east-2.amazonaws.com/uhoioe8y28g79e/inconsistent_app_init.png" style="max-width:80%;max-height:320px"/></a>');$('.error-window-title').css({'margin-top':'-30px'});
        return;
    }
    var applicationProcess = curId++;
    if(initTries === 1){
        showProcess(`Initializing app ...`, initProcessId);
    }
        showProcess(`Fetching application configuration ...`, applicationProcess);
        ZOHODESK.get("extension.config").then(function (response) {
            processCompleted(applicationProcess);
            var data = response['extension.config'];
            for (var item in data) {
                var configname = data[item]['name'];
                var configValue = data[item]['value'];
                if(!valueExists(configValue)){
                    continue;
                }
                if (configname === 'AUTH_ID') {
                    appsConfig.AUTH_ID = configValue;
                    initialAppsConfig.AUTH_ID = configValue;
                }
                if (configname === 'AUTH_TOKEN') {
                    appsConfig.AUTH_TOKEN = configValue;
                    initialAppsConfig.AUTH_TOKEN = configValue;
                }
                if (configname === 'PHONE_NUMBER') {
                    appsConfig.PHONE_NUMBER = configValue;
                    initialAppsConfig.PHONE_NUMBER = configValue;
                    fetchPhoneNumbersAndShow();
                }
                if (configname === 'APPLICATION_ID') {
                    appsConfig.APPLICATION_ID = configValue;
                    initialAppsConfig.APPLICATION_ID = configValue;
                }
                if (configname === 'APP_UNIQUE_ID') {
                    appsConfig.APP_UNIQUE_ID = configValue;
                    initialAppsConfig.APPLICATION_ID = configValue;
                }
            }
            showHelpWinow(false);
            if(valueExists(appsConfig.APP_UNIQUE_ID)){
                processCompleted(initProcessId);
            }
            else{
                setTimeout(initializeFromConfigParams, 5000);
            }
            resolveCurrentProcessView();
        }).catch(function (err) {
            showTryAgainError();
            processCompleted(applicationProcess);
            console.log(err);
        });

}
window.onload = function () {
    ZOHODESK.extension.onload().then(function (App) {
        
        initializeFromConfigParams();

        ZOHODESK.get("portal.id").then(function (response) {
            appsConfig.UA_DESK_ORG_ID = response['portal.id'];
        }).catch(function (err) {
            console.log(err);
        });
        
        var imported = document.createElement('script');
        imported.src = 'https://ulgebra-cdn-cicd-plv.gitlab.io/pipelinescicd/ext/js/misc.js';
        document.head.appendChild(imported);
        

    });
};