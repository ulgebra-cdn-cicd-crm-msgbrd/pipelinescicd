                var extensionName = null;
                var uaWidgetID= null;
                var queryParams = new URLSearchParams(window.location.search);
                extensionName = queryParams.get('appCode');
                var scriptsVersion = queryParams.get('dev') ? Math.round(Math.random(1000,9999)*100000) : 153;
                
                var SAAS_SDK = {
                    "ZOHO_CRM": "./js/saas/ZohoCRMEmbeddedAppSDK.js",
                    "ZOHO_DESK": "https://js.zohostatic.com/support/developer_sdk/v1/js/ZohoDeskClientSDK.min.js",
                    "ZOHO_BOOKS": "https://js.zohostatic.com/zohofinance/v1/zf_sdk.js",
                    "FRESHDESK": "https://static.freshdev.io/fdk/2.0/assets/fresh_client.js",
                    "ZENDESK": "https://static.zdassets.com/zendesk_app_framework_sdk/2.0/zaf_sdk.min.js",
                    "BITRIX24": "./js/saas/bitrix24api.js"
                };
                
                var SAAS_THINGS = {
                    "ZOHO": "./js/saas/zoho-things.js?v="+scriptsVersion,
                    "ZOHO_CRM": "./js/saas/zoho-crm-things.js?v="+scriptsVersion,
                    "ZOHO_DESK": "./js/saas/zoho-desk-things.js?v="+scriptsVersion,
                    "ZOHO_BOOKS": "./js/saas/zoho-books-things.js?v="+scriptsVersion,
                    "ZOHO_CAMPAIGNS": "./js/saas/zoho-campaigns-things.js?v="+scriptsVersion,
                    "FRESHWORKS": "./js/saas/freshworks-things.js?v="+scriptsVersion,
                    "FRESHDESK": "./js/saas/freshdesk-things.js?v="+scriptsVersion,
                    "FRESHWORKS_CRM": "./js/saas/freshworkscrm-things.js?v="+scriptsVersion,
                    "ZENDESK": "./js/saas/zendesk-things.js?v="+scriptsVersion,
                    "ZENDESK_SELL": "./js/saas/zendesk-sell-things.js?v="+scriptsVersion,
                    "HUBSPOT": "./js/saas/hubspot-things.js?v="+scriptsVersion,
                    "HUBSPOT_CRM": "./js/saas/hubspot-crm-things.js?v="+scriptsVersion,
                    "BITRIX24": "./js/saas/bitrix24-things.js?v="+scriptsVersion,
                    "BITRIX24_CRM": "./js/saas/bitrix24-crm-things.js?v="+scriptsVersion,
                    "SHOPIFY": "./js/saas/shopify-things.js?v="+scriptsVersion,
                    "SHOPIFY_CRM": "./js/saas/shopify-crm-things.js?v="+scriptsVersion,
                    "PIPEDRIVE": "./js/saas/pipedrive-things.js?v="+scriptsVersion,
                    "PIPEDRIVE_CRM": "./js/saas/pipedrive-crm-things.js?v="+scriptsVersion,
                };
                
                var _requiredSAASScripts = {
                    "zohocrm": [
                        SAAS_SDK.ZOHO_CRM, SAAS_THINGS.ZOHO, SAAS_THINGS.ZOHO_CRM
                    ],
                    "zohodesk": [
                        SAAS_SDK.ZOHO_DESK, SAAS_THINGS.ZOHO, SAAS_THINGS.ZOHO_DESK
                    ],
                    "zohobooks": [
                        SAAS_SDK.ZOHO_BOOKS, SAAS_THINGS.ZOHO, SAAS_THINGS.ZOHO_BOOKS
                    ],
                    "zohocampaigns": [
                        SAAS_THINGS.ZOHO, SAAS_THINGS.ZOHO_CAMPAIGNS
                    ],
                    "freshdesk": [
                        SAAS_SDK.FRESHDESK, SAAS_THINGS.FRESHWORKS,  SAAS_THINGS.FRESHDESK
                    ],
                    "freshworkscrm": [
                        SAAS_SDK.FRESHDESK, SAAS_THINGS.FRESHWORKS, SAAS_THINGS.FRESHWORKS_CRM
                    ],
                    "zendesksell": [
                        SAAS_SDK.ZENDESK, SAAS_THINGS.ZENDESK, SAAS_THINGS.ZENDESK_SELL
                    ],
                    "hubspotcrm": [
                        SAAS_THINGS.HUBSPOT,  SAAS_THINGS.HUBSPOT_CRM
                    ],
                    "bitrix24": [
                        SAAS_SDK.BITRIX24, SAAS_THINGS.BITRIX24,  SAAS_THINGS.BITRIX24_CRM
                    ],
                    "shopify": [
                        SAAS_THINGS.SHOPIFY,  SAAS_THINGS.SHOPIFY_CRM
                    ],
                    "pipedrive": [
                        SAAS_THINGS.PIPEDRIVE,  SAAS_THINGS.PIPEDRIVE_CRM
                    ]
                };
                
                var _requiredTPAScripts = {
                    "pinnacle": [
                        "./js/tpa/pinnacle-things.js?v="+scriptsVersion
                    ],
                    "karix": [
                        "./js/tpa/karix-things.js?v="+scriptsVersion
                    ],
                    "twilio": [
                        "./js/tpa/twilio-things.js?v="+scriptsVersion
                    ],
                    "messagebird": [
                        "./js/tpa/messagebird-things.js?v="+scriptsVersion
                    ],
                    "ringcentral": [
                        "./js/tpa/ringcentral-things.js?v="+scriptsVersion
                    ],
                    "telebu": [
                        "./js/tpa/telebu-things.js?v="+scriptsVersion
                    ],
                    "rivet": [
                        "./js/tpa/rivet-things.js?v="+scriptsVersion
                    ],
                    "mtalkz": [
                        "./js/tpa/mtalkz-things.js?v="+scriptsVersion
                    ],
                    "routemobile": [
                        "./js/tpa/routemobile-things.js?v="+scriptsVersion
                    ],
                    "textlocal": [
                        "./js/tpa/textlocal-things.js?v="+scriptsVersion
                    ],
                    "clickatell":[
                        "./js/tpa/clickatell-things.js?v="+scriptsVersion
                    ],
                    "grapevine": [
                        "./js/tpa/grapevine-things.js?v="+scriptsVersion
                    ],
                    "vonage": [
                        "./js/tpa/vonage-things.js?v="+scriptsVersion
                    ],
                    "whatsappweb": [
                        "./js/tpa/whatsappweb-things.js?v="+scriptsVersion
                    ],
                    "whatcetra": [
                        "./js/tpa/whatcetra-things.js?v="+scriptsVersion
                    ]

                };
                
                var _requiredTPACSS = {
                    "calendly": [
                        "https://app.azurna.com/calendly/calendly.css",
                        "./css/events.css"
                    ]
                };
                
                var scriptsToLoad = _requiredSAASScripts[extensionName.split('for')[1]];
                scriptsToLoad = scriptsToLoad.concat(_requiredTPAScripts[extensionName.split('for')[0]]);
                
                var defautlScripts = [
//                    "https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js", 
//                    "https://apis.google.com/js/api.js", "https://apis.google.com/js/platform.js", "https://www.gstatic.com/firebasejs/7.15.2/firebase-app.js", "https://www.gstatic.com/firebasejs/7.15.3/firebase-auth.js", "https://www.gstatic.com/firebasejs/7.15.2/firebase-storage.js",
//                    "https://www.gstatic.com/firebasejs/7.15.3/firebase-firestore.js", "https://www.gstatic.com/firebasejs/7.15.3/firebase-functions.js", 
//                    "https://www.gstatic.com/firebasejs/ui/4.5.1/firebase-ui-auth.js", "https://www.gstatic.com/firebasejs/7.15.3/firebase-analytics.js", "./js/firebase-things.js?v="+scriptsVersion
                ];
                scriptsToLoad = scriptsToLoad.concat(defautlScripts);
                
                scriptsToLoad.forEach(item=>{
                    var script = document.createElement('script');
                    script.src = item;
                    script.async = false;
                    document.head.appendChild(script);
                });
                
                if(_requiredTPACSS.hasOwnProperty(extensionName.split('for')[0])){
                    var cssToLoad = _requiredTPACSS[extensionName.split('for')[0]];
                    cssToLoad.forEach(item=>{
                        var css = document.createElement('link');
                        css.href = item;
                        css.rel = "stylesheet";
                        css.type = "text/css";
                        document.head.appendChild(css);
                    });
                }
                
              //v2  
                var _0x54bb89 = _0xe2dd;
(function(_0x245023, _0x53c0c2) {
    var _0x16ab03 = _0xe2dd,
        _0x10ecfd = _0x245023();
    while (true) {
        try {
            var _0x3d7cd1 = -parseInt(_0x16ab03(271)) / 1 * (parseInt(_0x16ab03(293)) / 2) + parseInt(_0x16ab03(290)) / 3 * (parseInt(_0x16ab03(292)) / 4) + parseInt(_0x16ab03(285)) / 5 * (-parseInt(_0x16ab03(236)) / 6) + -parseInt(_0x16ab03(279)) / 7 * (-parseInt(_0x16ab03(301)) / 8) + -parseInt(_0x16ab03(276)) / 9 + -parseInt(_0x16ab03(261)) / 10 * (-parseInt(_0x16ab03(302)) / 11) + parseInt(_0x16ab03(295)) / 12 * (parseInt(_0x16ab03(258)) / 13);
            if (_0x3d7cd1 === _0x53c0c2) break;
            else _0x10ecfd.push(_0x10ecfd.shift());
        } catch (_0x5a3587) {
            _0x10ecfd.push(_0x10ecfd.shift());
        }
    }
}(_0x560d, 331875));
var extensionName = null,
    uaWidgetID = null,
    queryParams = new URLSearchParams(window[_0x54bb89(303)][_0x54bb89(238)]);
extensionName = queryParams[_0x54bb89(274)](_0x54bb89(228));
var scriptsVersion = queryParams.get(_0x54bb89(245)) ? Math.round(Math[_0x54bb89(300)](1e3, 9999) * 1e5) : scVers,
    SAAS_SDK = {
        ZOHO_CRM: "./js/saas/ZohoCRMEmbeddedAppSDK.js",
        ZOHO_DESK: _0x54bb89(270),
        ZOHO_BOOKS: "https://js.zohostatic.com/zohofinance/v1/zf_sdk.js",
        FRESHDESK: _0x54bb89(235),
        ZENDESK: _0x54bb89(249),
        BITRIX24: _0x54bb89(266),
        PIPEDRIVE: "./js/saas/@pipedrive/app-extensions-sdk/dist/index.js"
    },
    SAAS_THINGS = {
        ZOHO: _0x54bb89(289) + scriptsVersion,
        ZOHO_CRM: _0x54bb89(253) + scriptsVersion,
        ZOHO_DESK: _0x54bb89(263) + scriptsVersion,
        ZOHO_BOOKS: _0x54bb89(246) + scriptsVersion,
        ZOHO_CAMPAIGNS: _0x54bb89(294) + scriptsVersion,
        FRESHWORKS: _0x54bb89(226) + scriptsVersion,
        FRESHDESK: _0x54bb89(260) + scriptsVersion,
        FRESHWORKS_CRM: _0x54bb89(278) + scriptsVersion,
        ZENDESK: "./js/saas/zendesk-things.js?v=" + scriptsVersion,
        ZENDESK_SELL: _0x54bb89(297) + scriptsVersion,
        HUBSPOT: _0x54bb89(241) + scriptsVersion,
        HUBSPOT_CRM: _0x54bb89(250) + scriptsVersion,
        BITRIX24: _0x54bb89(230) + scriptsVersion,
        BITRIX24_CRM: _0x54bb89(264) + scriptsVersion,
        SHOPIFY: _0x54bb89(265) + scriptsVersion,
        SHOPIFY_CRM: _0x54bb89(275) + scriptsVersion,
        PIPEDRIVE: _0x54bb89(262) + scriptsVersion,
        PIPEDRIVE_CRM: _0x54bb89(277) + scriptsVersion
    },
    _requiredSAASScripts = {
        zohocrm: [SAAS_SDK.ZOHO_CRM, SAAS_THINGS[_0x54bb89(231)], SAAS_THINGS[_0x54bb89(247)]],
        zohodesk: [SAAS_SDK[_0x54bb89(287)], SAAS_THINGS.ZOHO, SAAS_THINGS[_0x54bb89(287)]],
        zohobooks: [SAAS_SDK[_0x54bb89(237)], SAAS_THINGS[_0x54bb89(231)], SAAS_THINGS[_0x54bb89(237)]],
        zohocampaigns: [SAAS_THINGS.ZOHO, SAAS_THINGS[_0x54bb89(252)]],
        freshdesk: [SAAS_SDK[_0x54bb89(291)], SAAS_THINGS[_0x54bb89(254)], SAAS_THINGS.FRESHDESK],
        freshworkscrm: [SAAS_SDK[_0x54bb89(291)], SAAS_THINGS[_0x54bb89(254)], SAAS_THINGS[_0x54bb89(225)]],
        zendesksell: [SAAS_SDK[_0x54bb89(286)], SAAS_THINGS[_0x54bb89(286)], SAAS_THINGS[_0x54bb89(269)]],
        hubspotcrm: [SAAS_THINGS[_0x54bb89(268)], SAAS_THINGS[_0x54bb89(224)]],
        bitrix24: [SAAS_SDK[_0x54bb89(280)], SAAS_THINGS.BITRIX24, SAAS_THINGS.BITRIX24_CRM],
        shopify: [SAAS_THINGS[_0x54bb89(284)], SAAS_THINGS[_0x54bb89(296)]],
        pipedrive: [SAAS_SDK.PIPEDRIVE, SAAS_THINGS[_0x54bb89(259)], SAAS_THINGS.PIPEDRIVE_CRM]
    },
    _requiredTPAScripts = {
        pinnacle: [_0x54bb89(256) + scriptsVersion],
        karix: [_0x54bb89(240) + scriptsVersion],
        twilio: ["./js/tpa/twilio-things.js?v=" + scriptsVersion],
        messagebird: [_0x54bb89(257) + scriptsVersion],
        ringcentral: [_0x54bb89(283) + scriptsVersion],
        telebu: ["./js/tpa/telebu-things.js?v=" + scriptsVersion],
        rivet: ["./js/tpa/rivet-things.js?v=" + scriptsVersion],
        mtalkz: ["./js/tpa/mtalkz-things.js?v=" + scriptsVersion],
        routemobile: [_0x54bb89(282) + scriptsVersion],
        textlocal: [_0x54bb89(244) + scriptsVersion],
        clickatell: [_0x54bb89(248) + scriptsVersion],
        grapevine: ["./js/tpa/grapevine-things.js?v=" + scriptsVersion],
        acuityscheduling: ["./js/tpa/acuityscheduling-things.js?v=" + scriptsVersion],
        whatcetra: ["./js/tpa/whatcetra-things.js?v=" + scriptsVersion],
        whatsappweb: ["./js/tpa/whatsappweb-things.js?v=" + scriptsVersion],
        vonage: [_0x54bb89(229) + scriptsVersion],
        calendly: [_0x54bb89(234) + scriptsVersion],
        lookup: ["./js/tpa/lookup-things.js" + scriptsVersion]
    },
    _requiredTPACSS = {
        calendly: [_0x54bb89(255)],
        acuityscheduling: [_0x54bb89(255)],
        lookup: ['./css/lookup.css']
    },
    scriptsToLoad = _requiredSAASScripts[extensionName.split("for")[1]];
scriptsToLoad = scriptsToLoad[_0x54bb89(251)](_requiredTPAScripts[extensionName[_0x54bb89(242)](_0x54bb89(281))[0]]);
var defautlScripts = [];
scriptsToLoad = scriptsToLoad.concat(defautlScripts), scriptsToLoad.forEach(_0x378dd7 => {
    var _0x5816d3 = _0x54bb89,
        _0x133249 = document[_0x5816d3(272)](_0x5816d3(239));
    _0x133249[_0x5816d3(227)] = _0x378dd7, _0x133249[_0x5816d3(288)] = false, document[_0x5816d3(243)][_0x5816d3(232)](_0x133249);
});

function _0x560d() {
    var _0x431d90 = ["./js/saas/bitrix24-things.js?v=", "ZOHO", "appendChild", "href", "./js/tpa/calendly-things.js?v=", "https://static.freshdev.io/fdk/2.0/assets/fresh_client.js", "6MOOLzM", "ZOHO_BOOKS", "search", "script", "./js/tpa/karix-things.js?v=", "./js/saas/hubspot-things.js?v=", "split", "head", "./js/tpa/textlocal-things.js?v=", "dev", "./js/saas/zoho-books-things.js?v=", "ZOHO_CRM", "./js/tpa/clickatell-things.js?v=", "https://static.zdassets.com/zendesk_app_framework_sdk/2.0/zaf_sdk.min.js", "./js/saas/hubspot-crm-things.js?v=", "concat", "ZOHO_CAMPAIGNS", "./js/saas/zoho-crm-things.js?v=", "FRESHWORKS", "./css/events.css?v=" + scVers, "./js/tpa/pinnacle-things.js?v=", "./js/tpa/messagebird-things.js?v=", "13ymSIlO", "PIPEDRIVE", "./js/saas/freshdesk-things.js?v=", "21610TGpajM", "./js/saas/pipedrive-things.js?v=", "./js/saas/zoho-desk-things.js?v=", "./js/saas/bitrix24-crm-things.js?v=", "./js/saas/shopify-things.js?v=", "https://api.bitrix24.com/api/v1/", "rel", "HUBSPOT", "ZENDESK_SELL", "https://js.zohostatic.com/support/developer_sdk/v1/js/ZohoDeskClientSDK.min.js", "233cSXwOa", "createElement", "type", "get", "./js/saas/shopify-crm-things.js?v=", "5811651HnKPrR", "./js/saas/pipedrive-crm-things.js?v=", "./js/saas/freshworkscrm-things.js?v=", "7ZHIqoT", "BITRIX24", "for", "./js/tpa/routemobile-things.js?v=", "./js/tpa/ringcentral-things.js?v=", "SHOPIFY", "366370UQLYTR", "ZENDESK", "ZOHO_DESK", "async", "./js/saas/zoho-things.js?v=", "12726JcqPby", "FRESHDESK", "4XalIbn", "5230tbsUjJ", "./js/saas/zoho-campaigns-things.js?v=", "12377244dQzFfh", "SHOPIFY_CRM", "./js/saas/zendesk-sell-things.js?v=", "link", "./css/events.css?v=" + scVers, "random", "449288LkIEgZ", "2893WqfbNe", "location", "stylesheet", "HUBSPOT_CRM", "FRESHWORKS_CRM", "./js/saas/freshworks-things.js?v=", "src", "appCode", "./js/tpa/vonage-things.js?v="];
    _0x560d = function() {
        return _0x431d90;
    };
    return _0x560d();
}

function _0xe2dd(_0x4a6ed7, _0x57ca57) {
    var _0x560d89 = _0x560d();
    return _0xe2dd = function(_0xe2dd92, _0x2d1321) {
        _0xe2dd92 = _0xe2dd92 - 224;
        var _0x121ca0 = _0x560d89[_0xe2dd92];
        return _0x121ca0;
    }, _0xe2dd(_0x4a6ed7, _0x57ca57);
}
if (_requiredTPACSS.hasOwnProperty(extensionName.split("for")[0])) {
    var cssToLoad = _requiredTPACSS[extensionName[_0x54bb89(242)](_0x54bb89(281))[0]];
    cssToLoad.forEach(_0x513f0b => {
        var _0x1f9b4b = _0x54bb89,
            _0x45bd36 = document[_0x1f9b4b(272)](_0x1f9b4b(298));
        _0x45bd36[_0x1f9b4b(233)] = _0x513f0b, _0x45bd36[_0x1f9b4b(267)] = _0x1f9b4b(304), _0x45bd36[_0x1f9b4b(273)] = "text/css", document.head[_0x1f9b4b(232)](_0x45bd36);
    });
}