var UA_SAAS_SERVICE_APP = {
    
    CURRENT_USER_INFO : null,
    CURRENT_ORG_INFO : null,
    CURRENT_MODULE_FIELDS_DROPDOWN : [],
    CURRENT_OAUTH_SCOPE_NEEDED : ["read_customers","read_users"],
    FETCHED_RECORD_DETAILS: {},
    widgetContext: {},
    APP_API_NAME: "pinnaclesms__",
    SELECTED_RECEIPS: {},
    MODULE_FIELDS_MAP: {},
    SEARCH_RECORD_MODULE : "Customers",
    SEARCH_MODULE_RECORD_MAP: {},
    fetchedList : {},
    getAPIBaseUrl : function(){
        return `https://${UA_SAAS_SERVICE_APP.widgetContext.shop}`;
    },
    initiateAPPFlow: async function(){
        $("#ac_name_label_saas .anl_servicename").text('Shopify');

        let module = queryParams.get("module");
        UA_SAAS_SERVICE_APP.widgetContext.module = module;
        if(queryParams.get("action") && queryParams.get("action") == "single")
        {
            UA_SAAS_SERVICE_APP.widgetContext.mode = "single";
            UA_SAAS_SERVICE_APP.widgetContext.entityId = [queryParams.get("id")];
        }
        else if(queryParams.get("action") && queryParams.get("action") == "bulk")
        {
            UA_SAAS_SERVICE_APP.widgetContext.mode = "bulk";
            let ids = await UA_SAAS_SERVICE_APP.getIds();
            UA_SAAS_SERVICE_APP.widgetContext.entityId = ids?ids.split(','):[];
        }
        UA_SAAS_SERVICE_APP.widgetContext.shop = queryParams.get("shop");
        UA_SAAS_SERVICE_APP.initiateAPPFlow_SA();
    },
    initiateAPPFlow_SA: async function(){
        $("#ac_name_label_saas .anl_servicename").text('Shopify');
        if(queryParams.get("disableSAASSdk") && !UA_SAAS_SERVICE_APP.widgetContext.module && queryParams.get("module")){
            UA_SAAS_SERVICE_APP.widgetContext.module = queryParams.get("module");
            UA_SAAS_SERVICE_APP.widgetContext.entityId = queryParams.get("entityId");
            UA_SAAS_SERVICE_APP.widgetContext.entityId = UA_SAAS_SERVICE_APP.widgetContext.entityId.split(",");
            UA_SAAS_SERVICE_APP.injectShowSearchContact(); 
        }
        
        // UA_SAAS_SERVICE_APP.injectShowSearchContact();
        // $('[onclick="UA_APP_UTILITY.showWorkFlowInstructionDialog()"]').attr('onclick',"UA_SAAS_SERVICE_APP.showWorkFlowInstructionDialog()");
        $("#suo-item-workflow-nav").hide()
        if(document.querySelectorAll("#signedProfileUserLink")[0].getElementsByTagName("span")[0].innerText == "payments") {
            document.querySelectorAll("#signedProfileUserLink")[0].remove();
        }
                
        UA_SAAS_SERVICE_APP.getCurrentOrgInfo(function(response){
            UA_SAAS_SERVICE_APP.widgetContext.shop = response.api_name;
            UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO = response.data.shop;
            appsConfig.APP_UNIQUE_ID = currentUser.uid;
            appsConfig.UA_DESK_ORG_ID = UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO.id;
            UA_SAAS_SERVICE_APP.widgetContext.shop = UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO.domain;
            $("#saasAuthIDName").text(UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO.name).attr('title', `Authorized Shopify Account: ${UA_SAAS_SERVICE_APP.widgetContext.shop}`);
            setTimeout((function(){
                $('#ac_name_label_saas .ac_name_id').text(UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO.domain);
            }), 2000);
            UA_SAAS_SERVICE_APP.proceedToAppInitializationIfAPPConfigResolved();
        });
    },
    
    addSAASUsersToLICUtility: async function(){
        let query = {
            "query":`query {
                        shop { 
                            id 
                            staffMembers( first: 100 ) {
                                edges { 
                                    cursor 
                                    node { 
                                        id 
                                        active 
                                        isShopOwner 
                                        email 
                                        firstName 
                                        lastName 
                                        name 
                                    }
                                }
                                pageInfo { 
                                    hasNextPage 
                                    hasPreviousPage 
                                }
                            }
                        }
                    }`
        };
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/admin/api/2022-07/graphql.json`, "POST", query);
        response.data.users.sort(function(a, b) {
            return a.full_name.localeCompare(b.full_name);
        });
        response.data.users.forEach(item=>{
            UA_LIC_UTILITY.addFetchedSAASUser(item.id, item.full_name, item.email, null, item.profile.name === "Administrator", item.phone = null, url = null);
        });
        UA_LIC_UTILITY.saasUserListFetchCompleted();
    },
        
    proceedToAppInitializationIfAPPConfigResolved: function(){
        if(appsConfig.APP_UNIQUE_ID && appsConfig.UA_DESK_ORG_ID){
            UA_SAAS_SERVICE_APP.renderInitialElements(UA_SAAS_SERVICE_APP.widgetContext.module, UA_SAAS_SERVICE_APP.widgetContext.entityId);
            UA_APP_UTILITY.appsConfigHasBeenResolved();
            if(UA_SAAS_SERVICE_APP.widgetContext.module)
            {
                UA_SAAS_SERVICE_APP.renderInitialElementsForShopifyList();
            }
        }
    },
    
    renderInitialElements: async function(module, entityId){
        if(!UA_SAAS_SERVICE_APP.widgetContext.module){
            UA_SAAS_SERVICE_APP.entityDetailFetched(null);
            console.log('Module does not exist, rendering skipped');
            $("#recip-count-holder").text("Add recipients to proceed");
            return;
        }
//        UA_APP_UTILITY.renderSavedTemplatesInDropdowns();
        UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(module, entityId, function(contactList){
            UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
            UA_SAAS_SERVICE_APP.addRecordsAsRecipients(contactList);
        });
    },

    renderInitialElementsForShopifyList: async function(){
        $(".ssf-new-recip-form").prepend(`<div id="ssf-recip-list-add-holdr" class="ssf-recip-choice-btn" style="margin-top: -5px;background: none;border: none;box-shadow: none;"></div>`);
        UA_SAAS_SERVICE_APP.renderContactList();
    },
    
    renderContactList: async function(){
        // let departmentId = await ZOHODESK.get('department.id');
        // departmentId = departmentId['department.id'];
        let query = {
            "query":"query { segments(first: 25) { edges { cursor node { id name query } } pageInfo { hasNextPage hasPreviousPage } } }"
        };
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/admin/api/2022-07/graphql.json`, "POST", query);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('renderList',response);
        let listDropdownItems = [];
        if(response && response.data && response.data.data && response.data.data.segments && response.data.data.segments.edges && response.data.data.segments.edges.length > 0){
            response.data.data.segments.edges.forEach(item=>{
                UA_SAAS_SERVICE_APP.fetchedList[item.node.id.split('/')[4]] = item.node;
                listDropdownItems.push({
                    'label': item.node.name.replace("'",""),
                    'value': item.node.id.split('/')[4]
                });
            });
        }

        UA_APP_UTILITY.renderSelectableDropdown('#ssf-recip-list-add-holdr', 'Select a filter', listDropdownItems, 'UA_SAAS_SERVICE_APP.insertManageListContacts', false, false);
    
    },

    insertManageListContacts: async function(listId){
        let filter_query = this.fetchedList[listId].query;
        let query = { 
            "query":`query { customerSegmentMembers(first: 100,, query: "${filter_query}") { edges { node { id }}}}`
        };
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/admin/api/2022-07/graphql.json`, "POST", query);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('renderList',response);
        if(response && response.data && response.data.data && response.data.data.customerSegmentMembers && response.data.data.customerSegmentMembers.edges && response.data.data.customerSegmentMembers.edges.length > 0) {
            response.data.data.customerSegmentMembers.edges.forEach(item=>{
                let id = item.node.id.split('/')[4];
                UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails("customers", id, async function(contactList){return;});
            });
            UA_SAAS_SERVICE_APP.addRecordsAsRecipients(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
            UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
        }
        else{
            alert(`We can't found records in list or list is empty.`);
        }
            
    },
    
    sendPhoneFieldsForRecipientType: function(fieldsArray){
        fieldsArray.forEach(item=>{
            if(item.data_type === "phone"){
                UA_APP_UTILITY.addRecipientPhoneFieldType(item.api_name, item.display_label);
            }
        });
    },
    
    populateModuleItemFieldsInDropDown: async function (module, target, fieldsCallback=(()=>{})){
        if(!module || UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.includes(module) == true){
            return;
        }
        UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.push(module);
        var moduleToLoad = module === "users" ? UA_SAAS_SERVICE_APP.CURRENT_USER_INFO : UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[UA_SAAS_SERVICE_APP.widgetContext.entityId[0]];
        var moduleFields = Object.keys(moduleToLoad);
        var fieldsArray = [];
        var dropDownValues = [];
        moduleFields.forEach(item=>{
            let itemVal = moduleToLoad[item];
            if(typeof itemVal !== "object" && itemVal !== null && typeof itemVal!=="boolean"){
                fieldsArray.push({
                    "api_name": item,
                    "display_label": item
                });
                dropDownValues.push({
                    "label": item,
                    "value": (module.toUpperCase() === "USERS" ? "CURRENT_USER": module.toUpperCase())+'.'+item
                });
            }
        });
        fieldsCallback(fieldsArray);
        if(typeof target === "string"){
            target = [target];
        }
        target.forEach(item=>{
            UA_APP_UTILITY.renderSelectableDropdown(item, `Insert ${module} fields`, dropDownValues, 'UA_APP_UTILITY.templateMergeFieldSelected', false, true);
        });
        return true;
    },

    getIds : async function(){
        var ids="";
        if(window.location.search){
            var params =decodeURIComponent(window.location.search).split("&");
            params.forEach(function(param){
                if(param.indexOf("ids[]=") != -1)
                    if(ids == ""){
                        ids = param.substring(param.indexOf("ids[]=")+6);
                    }
                    else{
                        ids = ids+","+param.substring(param.indexOf("ids[]=")+6);
                    }
            })
        }
        return ids;
    },
    
    injectShowSearchContact :async function(){
        $(".ssf-new-recip-form").prepend(`<div id="searchAndAddFromContactBtn" class="ssf-recip-choice-btn" onclick="$('.item-list-popup').show()">
                                            <i class="material-icons">search</i> Search from ${UA_SAAS_SERVICE_APP.widgetContext.module}
                                      </div>`);
        $('.pageContentHolder').append(`<div class="item-list-popup" style="display:none">
                        <div class="item-list-popup-title">
                            Select a contact <div class="pop-win-close" onclick="$('.item-list-popup').hide();">x</div>
                        </div>
                        <div class="item-search-box">
                            <input onkeyup="proceedForSearchIfEnter()" class="input-form" id="contact-seach-name" type="text" autocomplete="off" placeholder="Name of the contact"/><br>
                            <div class="btn-save" onclick="UA_SAAS_SERVICE_APP.searchRecordsAndRender($('#contact-seach-name').val(),UA_SAAS_SERVICE_APP.widgetContext.module)">
                                Search ${UA_SAAS_SERVICE_APP.widgetContext.module}
                            </div>
                            <div class="btn-reset" onclick="UA_SAAS_SERVICE_APP.resetsearchContacts()">
                                Reset
                            </div>
                        </div>
                        <div class="item-list-popup-content" id="contact-search-items">

                        </div>
                    </div>`);
    },

    resetsearchContacts: function(){
        $("#contact-seach-name").val("");
        $("#contact-search-items").html("");
    },

    searchRecordsAndRender: async function(searchTerm,module){
        if(!valueExists(searchTerm)){
            return;
        }
            await UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/admin/api/2022-04/'+ module +'/search.json?query='+searchTerm , function(response){
                console.log(response);
                $("#contact-search-items").html(" ");
                if(response.data[module].length>0)
                {
                    var search_items = response.data[module];
                    if(search_items && search_items.length > 50){
                        search_items.splice(0,49);
                    }
                    UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module] = {};
                    search_items.forEach((obj)=>{
                        UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][obj.id] = obj;
                        if(module == 'customers')
                        {
                            var fullName = obj.first_name ? obj.first_name:"";
                            if(obj.last_name){
                                fullName = fullName+" "+ obj.last_name;
                            }
                        }
                        if(module && obj.phone)
                        {
                            let itemHTML = `<div class="item-list-popup-item"><span class="c-name"> ${fullName} </span>`;
                            itemHTML =  itemHTML + `${'<span class="c-phone '+(UA_SAAS_SERVICE_APP.SELECTED_RECEIPS["customers_"+obj.id] ? 'alreadyadded': '')+'" onclick="UA_SAAS_SERVICE_APP.selectPhoneNumber(\''+obj.id+'\', \''+"customers"+'\', \''+fullName+'\',\''+module+'\',\''+true+'\')"> <i class="material-icons">phone_iphone</i>( Customer ) </span>'}`;
                            itemHTML =  itemHTML + `</div>`;
                            $("#contact-search-items").append(itemHTML);
                        }    
                        
                    });
                }
                else
                {
                    $("#contact-search-items").html("No records found. <br><br> <span class=\"c-silver\"> This search will include <br> only customers having phone numbers</span>");
                    return;
                }
            }).catch(err=>{
                console.log(err);
            });
    },

    selectPhoneNumber: function(id,number,name,module,isSelected){
        var receipNumberID = module + "_" + id;
        if(!UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] && !UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id]){
            UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] = {
                'id': id,
                'name': name,
                'module': module,
                'isSelected': isSelected
            };
            $.extend(UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id],UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID]) ;
            UA_SAAS_SERVICE_APP.renderInitialElements(module,id);
            // UA_SAAS_SERVICE_APP.addRecordsAsRecipients([UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id]]);
        }
        else {
            console.log(module+' already added');
            alert(`This ${module} is already added.`);
            return;
        }
    },

    getCurrentUserInfo : async function(callback){
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/admin/api/2022-04/users/current.json', callback);
    },
    
    getUserInfoFromID : async function(userEmail, callback){
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/crm/v3/owners?email='+userEmail, callback);
    },
    
    getCurrentOrgInfo : async function(callback){
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/admin/api/2022-04/shop.json', callback);
    },
    
    getAPIResponseAndCallback: async function(url, callback){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${url}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        callback(response);
    },
    
    showErrorIfInAPICall : function(response){
        if(response.code === 401){
            UA_OAUTH_PROCESSOR.showReAuthorizationError(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED);
        }
    },
    
    initiateAuthFlow: function(){
        // UA_OAUTH_PROCESSOR.initiateAuth(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED);
        UA_OAUTH_PROCESSOR.showReAuthorizationError(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED);
    },
    
    fetchStoreExecuteOnRecordDetails: async function(module, recordIds, callback){
        if(!recordIds || recordIds.length < 1 || !module){
            return;
        }
        if(typeof recordIds !== "object"){
            var recordId = recordIds;
            recordIds = [recordId];
        }
        
        $("#recip-count-holder").text(`fetching ${module.toLocaleLowerCase()} information...`);
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/admin/api/2022-01/${module}.json?ids=${recordIds.join(',')}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('fetchStoreExecuteOnRecordDetails',response);
        response.data.customers.forEach(recordItem=>{
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordItem.id] = recordItem;
        });
        $("#recip-count-holder").text(`Selected ${recordIds.length} ${module.toLocaleLowerCase()}`);
        
        callback(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
        UA_SAAS_SERVICE_APP.entityDetailFetched();
    },
    
    addRecordsAsRecipients: function(contactsList){
        for(var item in contactsList)
        {
            if($(`[data-recip-id=${contactsList[item]['id']}]`).length === 0)
            {
                if(contactsList[item]['phone']){
                    UA_APP_UTILITY.addRecipientPhoneFieldType("phone", "phone");
                }
                if(!contactsList[item].name)
                {
                    contactsList[item].name = "";
                    if(contactsList[item].first_name){
                        contactsList[item].name = contactsList[item].name +" "+ contactsList[item].first_name;
                    }
                    if(contactsList[item].last_name){
                        contactsList[item].name = contactsList[item].name +" "+ contactsList[item].last_name;
                    }
                    contactsList[item].Full_Name = contactsList[item].name;
                }
                UA_APP_UTILITY.addInStoredRecipientInventory(contactsList[item]); 
            }       
        }
    },
    
    addSentSMSAsRecordInHistory: async function(sentMessages){
        // var historyDataArray = [];
        // for(var sentMessageId in sentMessages){
        //     var sentMessage = sentMessages[sentMessageId];
        //     var recModule = sentMessage.module; /*sentMessage.moduleId.split("_")[0];*/
        //     var recId = sentMessage.moduleId /*sentMessage.moduleId.split("_")[1];*/
        //     historyDataArray = {
        //         "comment":{
        //             "body":"I like comments\nAnd I like posting them *RESTfully*.",
        //             "author":"Pinnacle",
        //             "email":currentUser.email
        //         }
        //         // fields: {
        //         //     "ENTITY_ID": recId,
        //         //     "ENTITY_TYPE": recModule,
        //         //     "COMMENT": `${sentMessage.tpa?sentMessage.tpa:"SMS"} - ${sentMessage.channel} message status: ${sentMessage.status} from ${sentMessage.from} to ${sentMessage.to}  
                                
        //         //                 Platform  : ${sentMessage.channel}
                                
        //         //                 Sender    : ${sentMessage.from}
                                
        //         //                 Receiver  : ${sentMessage.to}
                                
        //         //                 Direction : Outbound
                                
        //         //                 Status    :  ${sentMessage.status}
                                
        //         //                 Message   : ${sentMessage.message}`
        //         // }  
        //     };
        // }
        // var credAdProcess3 = curId++;
        // showProcess(`Adding ${Object.keys(sentMessages).length} messages to history...`, credAdProcess3);
        // var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/admin/api/2022-04/comments.json`, "POST", historyDataArray);
        // processCompleted(credAdProcess3);
        // UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        // console.log('addSentSMSAsRecordInHistory',response);
        // //alert('All Messages has been added to Pinnacle SMS History');
        
    },

    getModuleFields: async function(module,callback){
        await UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/rest/crm.'+module+'.fields', function(response){
            UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
            console.log(response);
            callback(response);
        });
    },

    showWorkFlowInstructionDialog: function(){
        UA_SAAS_SERVICE_APP.getPersonalWebhookAuthtoken();
        $("#workflowInstructionDialog").show();
    },

    getPersonalWebhookAuthtoken: async function(){
        await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, action:"GET_WEBHOOK_AUTH_TOKEN"}).then((response) => {
            console.log('getPersonalWebhookAuthtoken response recd', response);
            var authtoken = response.data.authtoken['saas'];
            if(authtoken){
                UA_APP_UTILITY.PERSONAL_WEBHOOK_TOKEN = authtoken;
                // UA_SAAS_SERVICE_APP.renderWorkflowBodyCode(authtoken);
                var saasWithTpaWebhookUrlMap = {
                    "pinnacleforbitrix24":`https://us-central1-ulgebra-license.cloudfunctions.net/sms_workflowWebhookHandler?authtoken=${authtoken}&c=sms&t={{templateName}}&s={{senderId}}&p=WORK`,
                    "ringcentralforbitrix24":`https://us-central1-ulgebra-license.cloudfunctions.net/sms_workflowWebhookHandler?authtoken=${authtoken}&t={{templateName}}&f={{fromNumber}}&p=WORK`
                }
                $('#wi-webhook-url textarea').val(saasWithTpaWebhookUrlMap[extensionName]);
                $(".ssf-fitem.messageInputHolder")[2].remove();
            }
            return true;
        }).catch(err => {
            console.log(err);
            return false;
        });
    },
    
    renderWorkflowBodyCode :function(accessToken){
        
        UA_TPA_FEATURES.workflowCode.SMS.ulgebra_webhook_authtoken = accessToken;
        UA_TPA_FEATURES.workflowCode.WhatsApp.ulgebra_webhook_authtoken = accessToken;
        
        UA_SAAS_SERVICE_APP.addWorkflowBodyCode('sms', 'For Sending SMS', UA_TPA_FEATURES.workflowCode.SMS);
        
        UA_SAAS_SERVICE_APP.addWorkflowBodyCode('whatsapp', 'For Sending WhatsApp', UA_TPA_FEATURES.workflowCode.WhatsApp);
    },

    addWorkflowBodyCode: function(id, title, object){
        if($(`#wiparams-body-code-${id}`).length > 0){
            return;
        }
        $("#wiparams-holder").append(`<div class="wiparams-body-code-ttl">${title}</div><pre class="wiparams-pre-code" id="wiparams-body-code-${id}">`+syntaxHighlight(JSON.stringify(object, undefined, 4)));
    },
    entityDetailFetched: function(data){
        setTimeout(()=>{UA_APP_UTILITY.fetchedAllModuleRecords();}, 2000);
        if(typeof UA_TPA_FEATURES.callOnEntityDetailFetched === "function"){
            UA_TPA_FEATURES.callOnEntityDetailFetched(data);
        }
    }
};