var UA_SAAS_SERVICE_APP = {
    API_BASE_URL : "",
    CURRENT_USER_INFO : null,
    CURRENT_ORG_INFO : null,
    CURRENT_OAUTH_SCOPE_NEEDED : [],
    FETCHED_RECORD_DETAILS: {},
    CURRENT_MODULE_FIELDS_DROPDOWN : [],
    SELECTED_RECEIPS: {},
    SEARCH_MODULE_RECORD_MAP: {},
    fetchedList : {},
    widgetContext: {},
    appsConfig: {},
    APP_API_NAME: "pinnaclesms__",
    isModelOpened: false,
    APP_CLIENT: null,
    receivedParentData : null,
    appClient : ZAFClient.init(),
    supportedIncomingModules: [
        {
            "selected": true,
            "value": "contact",
            "label": "Contacts"
        },
        {
            "value": "lead",
            "label": "Leads"
        }
    ],
    
    getAPIBaseUrl : function(){
        return `https://api.getbase.com/v2`;
    },

    provideSuggestedCurrentEnvLoginEmailID: async function(){
        var client =UA_SAAS_SERVICE_APP.appClient;
        if(client && client._context && client._context.currentUser && client._context.currentUser.email) {
            UA_APP_UTILITY.receivedSuggestedCurrentEnvLoginEmailID(client._context.currentUser.email);
        }
    },
    
    initiateAPPFlow: async function(){
        var client = ZAFClient.init();
        $("#ac_name_label_saas .anl_servicename").text('Zendesk Sell');
        UA_SAAS_SERVICE_APP.APP_CLIENT = client;
        console.log(UA_SAAS_SERVICE_APP.APP_CLIENT);
//        var domainNameQuery = UA_SAAS_SERVICE_APP.APP_CLIENT._context.account.domain;
//        UA_OAUTH_PROCESSOR.currentSAASDomain = domainNameQuery;
        
        UA_SAAS_SERVICE_APP.initiateAPPFlow_SA(true);
        
    },
    
    initiateAPPFlow_SA: function(isSMS = false){

        $("#ac_name_label_saas .anl_servicename").text('Zendesk Sell');

        if(!UA_SAAS_SERVICE_APP.widgetContext.module && queryParams.get("module")){
            UA_SAAS_SERVICE_APP.widgetContext.module = queryParams.get("module");
            UA_SAAS_SERVICE_APP.widgetContext.entityId = queryParams.get("entityId");
            UA_SAAS_SERVICE_APP.injectShowSearchContact(); 
        }
        
        UA_SAAS_SERVICE_APP.getCurrentUserInfo(function(response){
            UA_SAAS_SERVICE_APP.CURRENT_USER_INFO = response.data.data;
            appsConfig.APP_UNIQUE_ID = UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.id;
            $("#saasAuthIDName").text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.name).attr('title', `Authorized Zendesk Sell Account: (${UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.email})`);
            setTimeout((function(){
                $('#ac_name_label_saas .ac_name_id').text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.email);
            }), 2000);
            UA_SAAS_SERVICE_APP.proceedToAppInitializationIfAPPConfigResolved();
        });
        
        UA_SAAS_SERVICE_APP.getCurrentOrgInfo(function(response){
            UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO = response.data.data;
            appsConfig.UA_DESK_ORG_ID = UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO.id+"";
            //$("#saasAuthIDName").text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.full_name).attr('title', `Authorized Zoho Account: (${UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.email})`);
            UA_SAAS_SERVICE_APP.proceedToAppInitializationIfAPPConfigResolved();
        });
    },
    
    addSAASUsersToLICUtility: async function(url){
        url = url?url:`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/users?status=active`;
        $('.saasServiceName').text("Zendesk Sell");
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(url, "GET", null);
        response.data.items.sort(function(a, b) {
            return a.data.name.localeCompare(b.data.name);
        });
        response.data.items.forEach(item=>{
            UA_LIC_UTILITY.addFetchedSAASUser(item.data.id, item.data.name, item.data.email, null, item.data.role === "admin", item.data.phone_number, url = null);
        });
        if(response.data && response.data.meta && response.data.meta.links && response.data.meta.links.next){
            UA_SAAS_SERVICE_APP.addSAASUsersToLICUtility(response.data.meta.links.next);
            return;
        }
        UA_LIC_UTILITY.saasUserListFetchCompleted();
        return;
    },
    
    proceedToAppInitializationIfAPPConfigResolved: function(){
        if(appsConfig.APP_UNIQUE_ID && appsConfig.UA_DESK_ORG_ID){
            UA_SAAS_SERVICE_APP.renderInitialElements();
            UA_SAAS_SERVICE_APP.addSAASUsersToLICUtility();
            UA_APP_UTILITY.appsConfigHasBeenResolved();
        }
    },
    
    renderInitialElements: async function(){
        UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown('users', ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder']);
//        UA_APP_UTILITY.renderSavedTemplatesInDropdowns();
        if(!UA_SAAS_SERVICE_APP.widgetContext.module){
            UA_SAAS_SERVICE_APP.entityDetailFetched(null);
            console.log('Module does not exist, rendering skipped');
            $("#recip-count-holder").text("Add recipients to proceed");
            return;
        }
        
        
        /*["phone", "mobile"].forEach(item=>{
            if(contactData[item]){
                UA_APP_UTILITY.addRecipientPhoneFieldType(item, item);
            }
        });
        
        UA_SAAS_SERVICE_APP.addRecordsAsRecipients([
            contactData
        ]);
        */
        UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(UA_SAAS_SERVICE_APP.widgetContext.module, UA_SAAS_SERVICE_APP.widgetContext.entityId, function(resp){
            UA_SAAS_SERVICE_APP.addRecordsAsRecipients(resp);
            UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], console.log);
        });
    },
    
    sendPhoneFieldsForRecipientType: function(fieldsArray){
        fieldsArray.forEach(item=>{
            if(item.display_label === "phone" || item.display_label === "mobile"){
                UA_APP_UTILITY.addRecipientPhoneFieldType(item.api_name, item.display_label);
            }
        });
    },
    
    populateModuleItemFieldsInDropDown: async function (module, target, fieldsCallback=(()=>{})){
        if(!module || UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.includes(module) == true){
            return;
        }
        UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.push(module);
        var moduleToLoad = module === "users" ? UA_SAAS_SERVICE_APP.CURRENT_USER_INFO : UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[UA_SAAS_SERVICE_APP.widgetContext.entityId];
        var moduleFields = Object.keys(moduleToLoad);
        var fieldsArray = [];
        var dropDownValues = [];
        moduleFields.forEach(item=>{
            let itemVal = moduleToLoad[item];
            if(typeof itemVal !== "object" && itemVal !== null && typeof itemVal!=="boolean"){
                fieldsArray.push({
                    "api_name": item,
                    "display_label": item
                });
                dropDownValues.push({
                    "label": item,
                    "value": (module.toUpperCase() === "USERS" ? "CURRENT_USER": module.toUpperCase())+'.'+item
                });
            }
        });
        fieldsCallback(fieldsArray);
        if(typeof target === "string"){
            target = [target];
        }
        target.forEach(item=>{
            UA_APP_UTILITY.renderSelectableDropdown(item, `${module} fields`, dropDownValues, 'UA_APP_UTILITY.templateMergeFieldSelected', false, true);
        });
        return true;
    },
    
    injectShowSearchContact :async function(){
        $(".ssf-new-recip-form").prepend(`<div id="searchAndAddFromContactBtn" class="ssf-recip-choice-btn" onclick="$('.item-list-popup').show()">
                                            <i class="material-icons">search</i> Search from ${UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()}
                                      </div>`);
        $('.pageContentHolder').append(`<div class="item-list-popup" style="display:none">
                        <div class="item-list-popup-title">
                            Select a ${UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()} <div class="pop-win-close" onclick="$('.item-list-popup').hide();">x</div>
                        </div>
                        <div class="item-search-box">
                            <input class="input-form" id="contact-seach-name" type="text" autocomplete="off" placeholder="name of the ${UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()}"/><br>
                            <div class="btn-save" onclick="UA_SAAS_SERVICE_APP.searchRecordsAndRender($('#contact-seach-name').val(),UA_SAAS_SERVICE_APP.widgetContext.module)">
                                Search ${UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()}
                            </div>
                            <div class="btn-reset" onclick="UA_SAAS_SERVICE_APP.resetsearchContacts()">
                                Reset
                            </div>
                        </div>
                        <div class="item-list-popup-content" id="contact-search-items">

                        </div>
                    </div>`);
    },

    resetsearchContacts: function(){
        $("#contact-seach-name").val("");
        $("#contact-search-items").html("");
    },

    searchRecordsAndRender: async function(searchTerm,module){
        if(!valueExists(searchTerm)){
            return;
        }
        let reqData =   {
                            "items": [
                                {
                                    "data": {
                                        "query": {
                                            "filter": {
                                                "filter": {
                                                    "attribute": {
                                                        "name": "name"
                                                    },
                                                    "parameter": {
                                                        "starts_with": searchTerm
                                                    }
                                                }
                                            },
                                            "projection": [
                                                {
                                                    "name": "name"
                                                }
                                            ]
                                        }
                                    },
                                    "per_page": 50
                                }
                            ]
                        };
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`https://api.getbase.com/v3/${module}s/search`,"POST",reqData)
            if(response && response.data && response.status == 200)
            {
                $("#contact-search-items").html(" ");
                if(response.data.items && response.data.items.length>0 && response.data.items[0].successful && response.data.items[0].items.length > 0)
                {
                    var search_items = response.data.items[0].items;
                    if(search_items && search_items.length > 50){
                        search_items.splice(0,49);
                    }
                    UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module] = {};
                    search_items.forEach((obj)=>{
                        UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][obj.data.id] = obj.data;
                        var fullName = "";
                        if(module)
                        {
                            fullName = obj.data.name;
                            fullName = fullName?fullName: module+"#"+obj.data.id;
                        }
                        let itemHTML = `<div class="item-list-popup-item" style="cursor:pointer;" onclick="UA_SAAS_SERVICE_APP.selectPhoneNumber('${obj.data.id}', '${module}', '${fullName}','${module}','${true}')"><span class="c-name"> ${fullName} </span>`;
                        // itemHTML =  (obj.item.phones && obj.item.phones.length > 0 )? itemHTML + `${'<span class="c-phone"> <i class="material-icons">phone_iphone</i> phone </span>'}` : itemHTML;
                        itemHTML =  itemHTML + `</div>`;
                        $("#contact-search-items").append(itemHTML);
                    });
                }
                else
                {
                    $("#contact-search-items").html("No records found. <br><br> <span class=\"c-silver\"> This search will include <br> only customers having phone numbers</span>");
                    return;
                }
            }
    },

    selectPhoneNumber: async function(id,number,name,module,isSelected){
        var receipNumberID = id;
        if(!UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] && !UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id]) {
            UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] = {
                'id': id,
                'name': name,
                'module': module,
                'isSelected': isSelected
            };
            $.extend(UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id],UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID]) ;
            UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(module,id,UA_SAAS_SERVICE_APP.addRecordsAsRecipients);
        }
        else {
            // console.log(module+' already added');
            showErroWindow("Alert Message",`This ${module.toLocaleLowerCase()} is already added.`)
            // alert(`This ${module.toLocaleLowerCase()} is already added.`);
            return;
        }
    },
    
    getCurrentUserInfo : async function(callback){
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/users/self', callback);
    },
    
    getCurrentOrgInfo : async function(callback){
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/accounts/self', callback);
    },
    
    getAPIResponseAndCallback: async function(url, callback){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}${url}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        callback(response);
    },
    
    showErrorIfInAPICall : async function(response){
        if(response.code === 401 || response.code === 403){
            UA_OAUTH_PROCESSOR.showReAuthorizationError("Zendesk Sell", `<a href="https://app.futuresimple.com/settings/oauth/tokens" target="_blank">Click to get Access Token</a>`, false, false);
        }
    },
    
    initiateAuthFlow: function(){
        UA_OAUTH_PROCESSOR.initiateAuth();
    },
    
    fetchStoreExecuteOnRecordDetails: async function(module, recordIds, callback){

        /*if(typeof recordIds !== "object"){
            var recordId = recordIds;
            recordIds = [recordId];
        }
//        recordIds = ["5318123000000377412"]; */
        $("#recip-count-holder").text(`fetching ${module.toLocaleLowerCase()} information...`);
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${module}s/${recordIds}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('fetchStoreExecuteOnRecordDetails',response);
        var responseData = response.data.data;
        if(UA_SAAS_SERVICE_APP.widgetContext.module === "deal"){
                // var q_contactPhone = queryParams.get('cpn');
                // var q_contactMobile = queryParams.get('cmn');
                // if(q_contactPhone && q_contactPhone.length>0){
                //     UA_APP_UTILITY.addRecipientPhoneFieldType('phone', 'phone');
                //     responseData.phone = q_contactPhone;
                // }
                // if(q_contactMobile && q_contactMobile.length>0){
                //     UA_APP_UTILITY.addRecipientPhoneFieldType('mobile', 'mobile');
                //     responseData.mobile = q_contactMobile;
                // }
                if(responseData.contact_id) {
                    var dealContResponse = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/contacts/${responseData.contact_id}`, "GET", null);
                    if(dealContResponse && dealContResponse.data && dealContResponse.data.data && dealContResponse.data.data.phone){
                        UA_APP_UTILITY.addRecipientPhoneFieldType('phone', 'phone');
                        responseData.phone = dealContResponse.data.data.phone;
                        responseData.email = dealContResponse.data.data.email;
                    }
                    if(dealContResponse && dealContResponse.data && dealContResponse.data.data && dealContResponse.data.data.mobile){
                        UA_APP_UTILITY.addRecipientPhoneFieldType('mobile', 'mobile');
                        responseData.mobile = dealContResponse.data.data.mobile;
                        responseData.email = dealContResponse.data.data.email;
                    }
                }
        }
        UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds] = responseData;
        $("#recip-count-holder").text(`Selected ${recordIds.length} ${module.toLocaleLowerCase()}`);
        callback(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
        UA_SAAS_SERVICE_APP.entityDetailFetched();
    },
    
    entityDetailFetched: function(data){
        setTimeout(()=>{UA_APP_UTILITY.fetchedAllModuleRecords();}, 2000);
        if(typeof UA_TPA_FEATURES.callOnEntityDetailFetched === "function"){
            UA_TPA_FEATURES.callOnEntityDetailFetched(data);
        }
    },
    
    addRecordsAsRecipients: function(contactsList){
        for(var item in contactsList){
            if($(`[data-recip-id=${contactsList[item]['id']}]`).length === 0) {
                ["phone", "mobile"].forEach(mobileFieldItem => {
                    if (contactsList[item][mobileFieldItem]) {
                        UA_APP_UTILITY.addRecipientPhoneFieldType(mobileFieldItem, mobileFieldItem);
                    }
                });
                if(!contactsList[item].name){
                    contactsList[item].name = "";
                    if(contactsList[item].first_name){
                    contactsList[item].name = contactsList[item].first_name;
                    }
                    if(contactsList[item].last_name){
                        contactsList[item].name = contactsList[item].name+" "+ contactsList[item].last_name;
                    }
                }
                UA_APP_UTILITY.addInStoredRecipientInventory(contactsList[item]);
            }
        }
    },
    
    addSentSMSAsRecordInHistory: async function(sentMessages){
        if(!UA_SAAS_SERVICE_APP.widgetContext.module){
            return;
        }
//        var historyDataArray = [];
        var commentContent = `${appPrettyName} Extension - `;
        
        var credAdProcess3 = curId++;
        showProcess(`Adding ${Object.keys(sentMessages).length} messages to ticket comments...`, credAdProcess3);

        for(var sentMessageId in sentMessages){
            let sentMessage = sentMessages[sentMessageId];
            let recModule = valueExists(sentMessage.module)?sentMessage.module:UA_SAAS_SERVICE_APP.widgetContext.module;
            let recId = valueExists(sentMessage.moduleId)?(sentMessage.moduleId.indexOf("newmanual")!==0?sentMessage.moduleId:UA_SAAS_SERVICE_APP.widgetContext.entityId):UA_SAAS_SERVICE_APP.widgetContext.entityId;

            commentContent=`${appPrettyName} Extension - ${sentMessage.channel} ${sentMessage.status} from ${sentMessage.from} to ${sentMessage.to}: \n
                             ${sentMessage.message}`;
                            
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/notes`, "POST", {"data":{
                resource_type: recModule,
                resource_id: parseInt(recId),
                content: commentContent,
                tags: [appPrettyName+sentMessage.channel.toLowerCase()]
            }});
        }
        
        processCompleted(credAdProcess3);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('addSentSMSAsRecordInHistory',response);
       
        
    }
};
