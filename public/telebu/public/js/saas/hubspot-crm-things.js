var UA_SAAS_SERVICE_APP = {
    
    CURRENT_USER_INFO : null,
    CURRENT_ORG_INFO : null,
    CURRENT_OAUTH_SCOPE_NEEDED : ["automation","timeline","tickets","crm.objects.contacts.read","crm.objects.contacts.write","crm.objects.companies.write","crm.schemas.contacts.read","crm.objects.companies.read","crm.objects.deals.read","crm.objects.deals.write","crm.schemas.companies.read","crm.schemas.companies.write","crm.schemas.contacts.write","crm.schemas.deals.read","crm.schemas.deals.write","crm.objects.owners.read", "settings.users.read", "crm.lists.read", "crm.lists.write"],
    FETCHED_RECORD_DETAILS: {},
    CURRENT_MODULE_FIELDS_DROPDOWN : [],
    SELECTED_RECEIPS: {},
    SEARCH_MODULE_RECORD_MAP: {},
    fetchedList : {},
    widgetContext: {},
    moduleVsTimeLineTemplateID : {
            "pinnacleforhubspotcrm":{
                "TICKET": "1149397",
                "CONTACT": "1149389",
                "DEAL": "1149396",
                "COMPANY": "1149395"
            },
            "karixforhubspotcrm":{
                "TICKET": "1152578",
                "CONTACT": "1152575",
                "DEAL": "1152577",
                "COMPANY": "1152576"
            },
            "grapevineforhubspotcrm":{
                "TICKET": "1152977",
                "CONTACT": "1097572",
                "DEAL": "1152976",
                "COMPANY": "1152975"
            },
            "routemobileforhubspotcrm":{
                "TICKET": "1152964",
                "CONTACT": "1054764",
                "DEAL": "1152963",
                "COMPANY": "1152962"
            },
            "rivetforhubspotcrm":{
                "TICKET": "1152971",
                "CONTACT": "1055489",
                "DEAL": "1152970",
                "COMPANY": "1152969"
            },
            "vonageforhubspotcrm":{
                "TICKET": "1152974",
                "CONTACT": "1061819",
                "DEAL": "1152973",
                "COMPANY": "1152972"
            },
            "twilioforhubspotcrm":{
                "TICKET": "1152946",
                "CONTACT": "1012133",
                "DEAL": "1152945",
                "COMPANY": "1152944"
            },
            "ringcentralforhubspotcrm": {
                "TICKET": "1156291",
                "CONTACT": "1155557",
                "DEAL": "1156290",
                "COMPANY": "1155759"
            },
            "messagebirdforhubspotcrm": {
                "TICKET": "1152961",
                "CONTACT": "1052134",
                "DEAL": "1152960",
                "COMPANY": "1152956"
            },
            "whatcetraforhubspotcrm": {
                "TICKET": "1152949",
                "CONTACT": "1009605",
                "DEAL": "1152948",
                "COMPANY": "1152947"
            }
    },
    
    supportedIncomingModules: [
        {
            "selected": true,
            "value": "Contacts",
            "label": "Contacts"
        }
    ],
    
    getAPIBaseUrl : function(){
        return `https://api.hubapi.com`;
    },
    
    initiateAPPFlow: async function(){
        UA_SAAS_SERVICE_APP.initiateAPPFlow_SA(true);
    },
    
    initiateAPPFlow_SA: function(isSMS = false){

        if(!UA_SAAS_SERVICE_APP.widgetContext.module && queryParams.get("module")){
            UA_SAAS_SERVICE_APP.widgetContext.module = queryParams.get("module").toUpperCase();
            UA_SAAS_SERVICE_APP.widgetContext.entityId = queryParams.get("entityId");
            UA_SAAS_SERVICE_APP.injectShowSearchContact(); 
        }
            if(UA_TPA_FEATURES.workflowCode){
                try{
                    Object.keys(UA_TPA_FEATURES.workflowCode).forEach(msgType=>{
                        delete UA_TPA_FEATURES.workflowCode[msgType].module;
                        delete UA_TPA_FEATURES.workflowCode[msgType].recordId;
                    });
                }
                catch(ex){
                    console.log(ex);
                }
            }
            $("#ac_name_label_saas .anl_servicename").text('Hubspot');
            UA_SAAS_SERVICE_APP.getCurrentUserInfo(function(response){
                UA_SAAS_SERVICE_APP.CURRENT_USER_INFO = response.data;
                appsConfig.APP_UNIQUE_ID = UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.user_id+"";
                appsConfig.UA_DESK_ORG_ID = UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.hub_id+"";
                $("#saasAuthIDName").text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.user).attr('title', `Authorized HubSpot Account: ${UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.hub_domain}`);
                setTimeout((function(){
                    $('#ac_name_label_saas .ac_name_id').text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.email);
                }), 2000);
                UA_SAAS_SERVICE_APP.proceedToAppInitializationIfAPPConfigResolved();
                UA_SAAS_SERVICE_APP.getUserInfoFromID(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.user, function(response){
                    UA_SAAS_SERVICE_APP.CURRENT_USER_INFO = response.data.results[0];
                    UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown('users', ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder']);
                });
            });
    },
        
    proceedToAppInitializationIfAPPConfigResolved: function(){
        if(appsConfig.APP_UNIQUE_ID && appsConfig.UA_DESK_ORG_ID){
            UA_SAAS_SERVICE_APP.renderInitialElements();
            UA_SAAS_SERVICE_APP.addSAASUsersToLICUtility();
            UA_APP_UTILITY.appsConfigHasBeenResolved();
            if(UA_SAAS_SERVICE_APP.widgetContext.module == "CONTACT")
            {
                UA_SAAS_SERVICE_APP.renderInitialElementsForHubList();
            }
            UA_SAAS_SERVICE_APP.retrieveModuleFieldsForLookUp(console.log);
        }
    },
    
    renderInitialElements: async function(){
        if(!UA_SAAS_SERVICE_APP.widgetContext.module){
            UA_SAAS_SERVICE_APP.entityDetailFetched(null);
            console.log('Module does not exist, rendering skipped');
            $("#recip-count-holder").text("Add recipients to proceed");
            return;
        }
        if(UA_SAAS_SERVICE_APP.widgetContext.module !== "CONTACT"){
            await UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown("CONTACT", ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
        }
        await UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
//        UA_APP_UTILITY.renderSavedTemplatesInDropdowns();
        let entityIdArr = UA_SAAS_SERVICE_APP.widgetContext.entityId.split(',');
        for(let i=0; i < entityIdArr.length; i++) {
            
            UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(UA_SAAS_SERVICE_APP.widgetContext.module, entityIdArr[i], function(contactList){
                //UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
                UA_SAAS_SERVICE_APP.addRecordsAsRecipients(contactList);
                UA_APP_UTILITY.fetchedAllModuleRecords();
            });

        }
    },

    renderInitialElementsForHubList: async function(){
        $(".ssf-new-recip-form").prepend(`<div id="ssf-recip-list-add-holdr" class="ssf-recip-choice-btn" style="margin-top: -5px;background: none;border: none;box-shadow: none;"></div>`);
        UA_SAAS_SERVICE_APP.renderContactList();
    },
    
    renderContactList: async function(){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/contacts/v1/lists?count=50`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('renderList',response);
        let listDropdownItems = [];
        response.data.lists.forEach(item=>{
            UA_SAAS_SERVICE_APP.fetchedList[item.listId] = item;
            listDropdownItems.push({
                'label': item.name,
                'value': item.listId
            });
        });

        UA_APP_UTILITY.renderSelectableDropdown('#ssf-recip-list-add-holdr', 'Select a List', listDropdownItems, 'UA_SAAS_SERVICE_APP.insertManageListContacts', false, false);
    
    },

    insertManageListContacts: async function(listId){

        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/contacts/v1/lists/${listId}/contacts/all`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('renderList',response);
        if(response.data && response.data.contacts && response.data.contacts.length > 0) {
            response.data.contacts.forEach(item=>{
                UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails("CONTACT",item.vid, async function(contactList){
                    UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
                    UA_SAAS_SERVICE_APP.addRecordsAsRecipients(contactList);
                });
            });
        }
            
    },
    
    addSAASUsersToLICUtility: async function(){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/owners?archived=false`, "GET", null);
        response.data.results.sort(function(a, b) {
            return a.lastName.localeCompare(b.lastName);
        });
        response.data.results.forEach(item=>{
            UA_LIC_UTILITY.addFetchedSAASUser(item.id, item.lastName+ ' '+item.firstName, item.email, null, true, null, url = null);
        });
        UA_LIC_UTILITY.saasUserListFetchCompleted();
    },
    
    sendPhoneFieldsForRecipientType: function(fieldsArray){
        fieldsArray.forEach(item=>{
            if(item.data_type === "phone" || item.data_type === "mobilephone" || item.fieldType === "phonenumber"){
                UA_APP_UTILITY.addRecipientPhoneFieldType(item.api_name, item.display_label);
            }
        });
    },
    FETCHED_MODULE_FIELDS: {},
    populateModuleItemFieldsInDropDown: async function (module, target, fieldsCallback=(()=>{})){
        if(!module || UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.includes(module) === true){
            return false;
        }
        UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.push(module);
        UA_SAAS_SERVICE_APP.FETCHED_MODULE_FIELDS[module] = {};
        var moduleToLoad = module === "users" ? UA_SAAS_SERVICE_APP.CURRENT_USER_INFO : UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[UA_SAAS_SERVICE_APP.widgetContext.entityId.split(',')[0]];
        var moduleFields = [];
        if(module ==="users"){
            moduleFields = Object.keys(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO);
        }
        else{
            var modulePluralWord = module === "COMPANY" ? "companies": module.toLowerCase()+"s";
            var moduleFieldsResponse = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/properties/${modulePluralWord}?archived=false`, 'GET', null);
            if(!moduleFieldsResponse.error){
                moduleFieldsResponse.data.results.forEach(item=>{
                    if(item.hidden || (!["string" , "number"].includes(item.type)) || item.calculated || item.modificationMetadata.readOnlyValue || item.displayOrder === -1 || !item.name){
                        return;
                    }
                    moduleFields.push(item);
                });
                moduleFields.sort(function(a, b) {
                    return a.label.localeCompare(b.label);
                });
            }
        }
        var fieldsArray = [];
        var dropDownValues = [];
        moduleFields.forEach(item=>{
            if(module ==="users"){
                let itemVal = moduleToLoad[item];
                if(typeof itemVal !== "object" && itemVal !== null && typeof itemVal!=="boolean"){
                    UA_SAAS_SERVICE_APP.FETCHED_MODULE_FIELDS[module][item.name] = item;
                    fieldsArray.push({
                        "api_name": item,
                        "display_label": item
                    });
                    dropDownValues.push({
                        "label": item,
                        "value": (module.toUpperCase() === "USERS" ? "CURRENT_USER": module.toUpperCase())+'.'+item
                    });
                }
            }
            else{
                    UA_SAAS_SERVICE_APP.FETCHED_MODULE_FIELDS[module][item.name] = item;
                    if(UA_SAAS_SERVICE_APP.widgetContext.module !== module){
                        item.name = module.toLowerCase() + "_"+ item.name;
                    }
                    fieldsArray.push({
                        "api_name": item.name,
                        "display_label": item.label
                    });
                    dropDownValues.push({
                        "label": item.label,
                        "value": (UA_SAAS_SERVICE_APP.widgetContext.module ? UA_SAAS_SERVICE_APP.widgetContext.module : module.toUpperCase())+'.'+item.name
                    });
            }
        });
        fieldsCallback(fieldsArray);
        if(typeof target === "string"){
            target = [target];
        }
        target.forEach(item=>{
            UA_APP_UTILITY.renderSelectableDropdown(item, `Insert ${module} fields`, dropDownValues, 'UA_APP_UTILITY.templateMergeFieldSelected', false, true);
        });
        return true;
    },
    
    injectShowSearchContact :async function(){
        $(".ssf-new-recip-form").prepend(`<div id="searchAndAddFromContactBtn" class="ssf-recip-choice-btn" onclick="$('.item-list-popup').show()">
                                            <i class="material-icons">search</i> Search from ${UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()}
                                      </div>`);
        $('.pageContentHolder').append(`<div class="item-list-popup" style="display:none">
                        <div class="item-list-popup-title">
                            Select ${UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()} <div class="pop-win-close" onclick="$('.item-list-popup').hide();">x</div>
                        </div>
                        <div class="item-search-box">
                            <input class="input-form" id="contact-seach-name" type="text" autocomplete="off" placeholder="name of the ${UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()}"/><br>
                            <div class="btn-save" onclick="UA_SAAS_SERVICE_APP.searchRecordsAndRender($('#contact-seach-name').val(),UA_SAAS_SERVICE_APP.widgetContext.module)">
                                Search ${UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()}
                            </div>
                            <div class="btn-reset" onclick="UA_SAAS_SERVICE_APP.resetsearchContacts()">
                                Reset
                            </div>
                        </div>
                        <div class="item-list-popup-content" id="contact-search-items">

                        </div>
                    </div>`);
    },

    resetsearchContacts: function(){
        $("#contact-seach-name").val("");
        $("#contact-search-items").html("");
    },

    searchRecordsAndRender: async function(searchTerm,module){
        if(!valueExists(searchTerm)){
            return;
        }
        var modulePluralWord = module === "COMPANY" ? "companies": module.toLowerCase()+"s";
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/${modulePluralWord}/search`,"POST",JSON.stringify({"query":searchTerm}))
            if(response)
            {
                console.log(response);
                UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
                $("#contact-search-items").html(" ");
                if(response.data["results"].length>0)
                {
                    var search_items = response.data["results"];
                    if(search_items && search_items.length > 50){
                        search_items.splice(0,49);
                    }
                    UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module] = {};
                    search_items.forEach((obj)=>{
                        UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][obj.id] = obj;
                        let fullName = "";
                        if(module)
                        {
                            switch(module){
                                case("CONTACT"): {
                                    fullName = obj['properties'].firstname ? obj['properties'].firstname:"";
                                    if(obj['properties'].lastname){
                                        fullName = fullName+" "+ obj['properties'].lastname;
                                    } 
                                    fullName = fullName?fullName:"Contact#"+obj.id;
                                    break;
                                }
                                case("COMPANY"):{
                                    fullName = obj['properties'].name ? obj['properties'].name:"Company#"+obj.id;
                                    break;
                                }
                                case("DEAL"):{
                                    fullName = obj['properties'].dealname ? obj['properties'].dealname:"Deal#"+obj.id;
                                    break;
                                }
                                case("TICKET"):{
                                    fullName = obj['properties'].subject ? obj['properties'].subject:"Ticket#"+obj.id;
                                    break;
                                }
                            }
                        }
                        let itemHTML = `<div class="item-list-popup-item" style="cursor:pointer;" onclick="UA_SAAS_SERVICE_APP.selectPhoneNumber('${obj.id}', '${module}', '${fullName}','${module}','${true}')"><span class="c-name"> ${fullName} </span>`;
                        // itemHTML =  itemHTML + `${'<span class="c-phone '+(UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[module+"_"+obj.id] ? 'alreadyadded': '')+'"></span>'}`;
                        itemHTML =  itemHTML + `</div>`;
                        $("#contact-search-items").append(itemHTML);
                    });
                }
                else
                {
                    $("#contact-search-items").html("No records found. <br><br> <span class=\"c-silver\"> This search will include <br> only customers having phone numbers</span>");
                    return;
                }
            }
    },

    selectPhoneNumber:async function(id,number,name,module,isSelected){
        var receipNumberID = module + "_" + id;
        if(!UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] && !UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id]){
            UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] = {
                'id': id,
                'name': name,
                'module': module,
                'isSelected': isSelected
            };
            $.extend(UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id],UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID]) ;
            // UA_SAAS_SERVICE_APP.renderInitialElements(module,id);
            UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(module,id,UA_SAAS_SERVICE_APP.addRecordsAsRecipients);
            await UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
            // UA_SAAS_SERVICE_APP.addRecordsAsRecipients([UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id]]);
        }
        else {
            console.log(module+' already added');
            showErroWindow("Alert Message",`This ${module.toLocaleLowerCase()} is already added.`)
            // alert(`This ${module.toLocaleLowerCase()} is already added.`);
            return;
        }
    },
    
    getCurrentUserInfo : async function(callback){
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/oauth/v1/access-tokens', callback);
    },
    
    getUserInfoFromID : async function(userEmail, callback){
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/crm/v3/owners?email='+userEmail, callback);
    },
    
    getCurrentOrgInfo : async function(callback){
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/org', callback);
    },
    
    getAPIResponseAndCallback: async function(url, callback){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}${url}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        callback(response);
    },
    
    showErrorIfInAPICall : function(response){
        if(response.code === 401){
            UA_OAUTH_PROCESSOR.showReAuthorizationError(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED);
        }
    },
    
    initiateAuthFlow: function(){
        UA_OAUTH_PROCESSOR.initiateAuth(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED);
    },
    
    fetchStoreExecuteOnRecordDetails: async function(module, recordIds, callback){
        if(!module){
            return;
        }
//        if(typeof recordIds !== "object"){
//            var recordId = recordIds;
//            recordIds = [recordId];
//        }
//        recordIds = ["5318123000000377412"];
        $("#recip-count-holder").text(`fetching ${module.toLocaleLowerCase()} information...`);
        var modulePluralWord = module === "COMPANY" ? "companies": module.toLowerCase()+"s";
        let fieldsToFetch = ["phone","email","firstname","lastname","mobilephone"];
        if(UA_SAAS_SERVICE_APP.FETCHED_MODULE_FIELDS && UA_SAAS_SERVICE_APP.FETCHED_MODULE_FIELDS[module]){
            fieldsToFetch = Object.keys(UA_SAAS_SERVICE_APP.FETCHED_MODULE_FIELDS[module]);
        }
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/${modulePluralWord}/${recordIds}?properties=${fieldsToFetch.join(',')}&associations=contact`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('fetchStoreExecuteOnRecordDetails',response);
        UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds] = response.data.properties;
        UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds].id = recordIds;
        if(module === "CONTACT" || module === "COMPANY"){
            $("#recip-count-holder").text(`Selected ${recordIds.length} ${module.toLocaleLowerCase()}`);
            UA_SAAS_SERVICE_APP.entityDetailFetched(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds]);
            callback(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
        }
        else if(response.data && response.data.associations && response.data.associations.contacts) {
            var fetchedAssociatedContactsList = response.data.associations.contacts.results;
            var contactIdInputPayload = [];
            fetchedAssociatedContactsList.forEach(contactItem=>{
                contactIdInputPayload.push({
                    'id': contactItem.id
                });
            });
            $("#recip-count-holder").text(`fetching ${contactIdInputPayload.length} ${module.toLocaleLowerCase()} information...`);
            let contactFieldsToFetch = ["phone","email","firstname","lastname","mobilephone"];
            if(UA_SAAS_SERVICE_APP.FETCHED_MODULE_FIELDS && UA_SAAS_SERVICE_APP.FETCHED_MODULE_FIELDS['CONTACT']){
                contactFieldsToFetch = Object.keys(UA_SAAS_SERVICE_APP.FETCHED_MODULE_FIELDS['CONTACT']);
            }
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/contacts/batch/read`, "POST", {
                properties: contactFieldsToFetch,
                inputs: contactIdInputPayload
            });
            UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
            console.log('fetchStoreExecuteOnRecordDetails',response);
            response.data.results.forEach(item=>{
                UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds+"-"+item.id] = UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds];
                UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds+"-"+item.id][`contact_id`] = item.id;
                for (const [key, value] of Object.entries(item.properties)) {
                    UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds+"-"+item.id][`contact_${key}`] = value;
                }
                // UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id] = item.properties;
                // UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id].id = item.id;
            });
            if(Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS).length > 1){
                UA_APP_UTILITY.changeCurrentView('MESSAGE_FORM');
            }
           
            callback(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
            UA_SAAS_SERVICE_APP.entityDetailFetched(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds]);
        }
        else {
            showErroWindow("Alert Message",`This ${module.toLocaleLowerCase()} haven't any associated Contacts.`);
        }
    },
    
     addRecordsAsRecipients: function(contactsList){
        for(var item in contactsList){
            if($(`[data-recip-id=${contactsList[item]['id']}]`).length == 0)
            {
                ["phone", "mobilephone","contact_phone","contact_mobilephone"].forEach(mobileFieldItem => {
                    if (contactsList[item][mobileFieldItem]) {
                        UA_APP_UTILITY.addRecipientPhoneFieldType(mobileFieldItem, mobileFieldItem);
                    }
                });
                if(!contactsList[item].name){
                    contactsList[item].name = "";
                    let contactName = "";
                    if(contactsList[item].firstname){
                        contactsList[item].name = contactsList[item].firstname;
                    }
                    if(contactsList[item].lastname){
                        contactsList[item].name = contactsList[item].name+" "+ contactsList[item].lastname;
                    }
                    if(contactsList[item]['contact_firstname'] || contactsList[item]['contact_lastname'])
                    {
                        contactName = (contactsList[item]['contact_firstname']?contactsList[item]['contact_firstname']:"") + " "+ (contactsList[item]['contact_lastname']?contactsList[item]['contact_lastname'] :"") + " (contact)";
                    }
                    if(UA_SAAS_SERVICE_APP.widgetContext.module == "DEAL"){
                        contactsList[item].name = (contactsList[item].dealname?contactsList[item].dealname:"") + "(deal) - "+ (contactName?contactName:"");
                    }
                    if(UA_SAAS_SERVICE_APP.widgetContext.module == "TICKET"){
                        contactsList[item].name = (contactsList[item].subject?contactsList[item].subject:"") +"(ticket) - "+ `${contactName?contactName:""}`;
                    }
                }
                UA_APP_UTILITY.addInStoredRecipientInventory(contactsList[item]);
            }
        }
    },
    
    addSentSMSAsRecordInHistory: async function(sentMessages){
        var historyDataArray = [];
        for(var sentMessageId in sentMessages){
            let sentMessage = sentMessages[sentMessageId];
            let recModule = valueExists(sentMessage.module)?sentMessage.module:UA_SAAS_SERVICE_APP.widgetContext.module;
            let recId = valueExists(sentMessage.moduleId)?(sentMessage.moduleId.indexOf("newmanual")!==0?sentMessage.moduleId:UA_SAAS_SERVICE_APP.widgetContext.entityId.split(',')[0]):UA_SAAS_SERVICE_APP.widgetContext.entityId.split(',')[0];
                recId = recId.includes('-') == true ? recId.split('-')[0] : recId;
            historyDataArray.push({
                "eventTemplateId": UA_SAAS_SERVICE_APP.moduleVsTimeLineTemplateID[extensionName][recModule],
                "objectId": recId,
                "tokens": {
                    "platform": sentMessage.channel,
                    "messageId": sentMessageId,
                    "direction": "Outbound",
                    "sender_phone": sentMessage.from,
                    "receiver_phone": sentMessage.to ? (sentMessage.to.indexOf("+") === 0 ? sentMessage.to : "+" + sentMessage.to) : null,
                    "status": sentMessage.status,
                    "message": sentMessage.message
                }
            });
        }
        let credAdProcess3 = curId++;
        showProcess(`Adding ${Object.keys(sentMessages).length} messages to history...`, credAdProcess3);
        let response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/timeline/events/batch/create`, "POST", {
            inputs: historyDataArray
        });
        processCompleted(credAdProcess3);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('addSentSMSAsRecordInHistory',response);
        //alert('All Messages has been added to Pinnacle SMS History');
        
    },
    entityDetailFetched: function(data){
        if(typeof UA_TPA_FEATURES.callOnEntityDetailFetched === "function"){
            UA_TPA_FEATURES.callOnEntityDetailFetched(data);
        }
    },

// LookUp functions
    LOOKUP_SUPPORTED_MODULES: {
        "Contacts": {
            "autoLookup": true,
            "apiName": "Contacts"
        },
        "Companies": {
            "autoLookup": true,
            "apiName": "Companies"
        }
        // "Deals": {},
        // "Tickets": {}
    },

    allModuleFieldsFetched: function(){
        let fieldsPresentModuleCount = 0;
        Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).forEach(item=>{
            if(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[item].fields && Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[item].fields).length > 0){
                fieldsPresentModuleCount++;
            }
        });
        return Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).length === fieldsPresentModuleCount;
    },

    retrieveModuleFieldsForLookUp: function(callback){
        $('#ua-loo-search-field-ddholder').hide();
        if (UA_SAAS_SERVICE_APP.allModuleFieldsFetched()) {
            callback();
        }
        var primaryFieldsToCreate = [];
        let primaryFieldsToViewForModule = ["id"];

        Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).forEach(async moduleId=>{
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/properties/${moduleId.toLocaleLowerCase()}?archived=false`, "GET", null);
            let fieldAPINameMap = {};
            // await response.data.results.forEach(fieldItem=>{
            for(var i=0; i < response.data.results.length; i++){
                let fieldItem = response.data.results[i];
                if((fieldItem.type != "enumeration" || fieldItem.options.length > 0)) {
                    if(["lookup"].includes(fieldItem.fieldType)){
                        fieldItem.field_read_only = true;
                    }

                    fieldItem['display_label'] = fieldItem.label;
                    fieldItem['api_name'] = fieldItem.name;
                    fieldItem['data_type'] = fieldItem.fieldType;
                    if(fieldItem['type'] == "enumeration"){
                        fieldItem['data_type'] = "picklist";
                    }
                    if(fieldItem['data_type'] == "date"){
                        fieldItem['data_type'] = "datetime";
                    }
                    if(fieldItem.options && fieldItem.options.length > 0){
                        fieldItem["pick_list_values"] = [];
                        for(var optInd=0; optInd < fieldItem.options.length; optInd++){
                            let option = fieldItem.options[optInd];
                            fieldItem["pick_list_values"][optInd] = {
                                "actual_value": option.value,
                                "display_value": option.label
                            }
                        }
                    }
                    fieldAPINameMap[fieldItem.name] = fieldItem;
                }
            }
            fieldAPINameMap.id = {
              'display_label': 'ID',
              'data_type': 'number',
              'api_name': 'id',
              'field_read_only': true
            };
            
            switch(moduleId){
                case("Contacts"): {
                    primaryFieldsToCreate = ["lastname", "firstname","phone","mobilephone","email"];
                    primaryFieldsToViewForModule = ["lastname","firstname","mobilephone","phone","email","description"];
                    break;
                }
                case("Companies"):{
                    primaryFieldsToCreate = ["name", "domain","phone", "type","description"];
                    primaryFieldsToViewForModule = ["name","domain","createdate","hs_lastmodifieddate"];
                    break;
                }
                case("Deals"):{
                    primaryFieldsToCreate = ["dealname", "description", "dealtype", "amount"];
                    primaryFieldsToViewForModule = ["dealname","amount","dealstage","hs_lastmodifieddate","closedate"];
                    break;
                }
                case("Tickets"):{
                    primaryFieldsToCreate = ["subject", "content", "source_type"];
                    primaryFieldsToViewForModule = ["subject","content","createdate","hs_lastmodifieddate"];
                    break;
                }
            }

            if(UA_TPA_FEATURES.setCreateModuleRecordFields) UA_TPA_FEATURES.setCreateModuleRecordFields(moduleId, primaryFieldsToCreate);
            if(UA_TPA_FEATURES.setViewModuleRecordFields) UA_TPA_FEATURES.setViewModuleRecordFields(moduleId, primaryFieldsToViewForModule);
            UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[moduleId].fields = fieldAPINameMap;
            if(UA_SAAS_SERVICE_APP.allModuleFieldsFetched()){
                callback();
            }
        });
    },

    getSearchedResultItems: async function(searchTerm, module, field="phone"){
        UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS = {};
        if(searchTerm.startsWith('+')){
            searchTerm = searchTerm.replace('+', '*');
        }
        var apiReqData = {
            "filters": [
              {
                "propertyName": field.toLocaleLowerCase(),
                "operator": "EQ",
                "value": searchTerm
              }
            ],
            "properties": Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[module].fields)
          };
        apiReqData.properties.push("hubspot_owner_id");
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/${module.toLocaleLowerCase()!="companies"?"contact":"companies"}/search`,"POST",JSON.stringify(apiReqData));
        if(response.error && response.error.category == "RATE_LIMITS"){
            return {status: "retry_request"};
        }
        // await response.data.results.forEach(async (item)=>{
        if(response && response.data && response.data.results){
            for(var j=0; j < response.data.results.length; j++){
                var item = response.data.results[j];
                if(module.toLocaleLowerCase() == "deals" || module.toLocaleLowerCase() == "tickets"){
                    var asso_response_list = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/contacts/${item.id}/associations/${module.toLocaleLowerCase()}`,"GET", null);
                    // await asso_response_list.data.results.forEach(async (asslistitem)=>{
                    for(var i=0; i < asso_response_list.data.results.length; i++){
                        var asslistitem = asso_response_list.data.results[i];

                        asslistitem = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/${module.toLocaleLowerCase()}/${asslistitem.id}?properties=${Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[module].fields).join(",")}`,"GET", null);
                        asslistitem = asslistitem.data;

                        var fullName = await UA_SAAS_SERVICE_APP.getRecordName(module,asslistitem);
                        asslistitem = {...asslistitem,...asslistitem.properties};
                        delete asslistitem.properties;
                        asslistitem.name = fullName;
                        asslistitem.webURL = `https://app.hubspot.com/${module.toLocaleLowerCase()}/${appsConfig.UA_DESK_ORG_ID}/${module.toLocaleLowerCase()}/${asslistitem.id}`;
                        UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[asslistitem.id] = asslistitem;
                    }
                }
                else{
                    var fullName = await UA_SAAS_SERVICE_APP.getRecordName(module,item);
                    item = {...item,...item.properties};
                    delete item.properties;
                    item.name = fullName;
                    item.webURL = `https://app.hubspot.com/${module.toLocaleLowerCase()}/${appsConfig.UA_DESK_ORG_ID}/${module.toLocaleLowerCase()}/${item.id}`;
                    item.owner_name = (item.hubspot_owner_id && UA_LIC_UTILITY.SAAS_FETCHED_USERS_IDS_LIST[item.hubspot_owner_id]) ? UA_LIC_UTILITY.SAAS_FETCHED_USERS_IDS_LIST[item.hubspot_owner_id].name :"No Owner";
                    UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id] = item;
                } 
            }
            return response.data.results;
        }
        else{
            return [];
        }
        
    },
    
    getNotesOfRecord: async function(module, entityId){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/${module.toLocaleLowerCase()}/${entityId}/associations/notes?limit=10`, "GET", null);
            let notesArray = [];
            if(response && response.data && response.data.results && response.data.results.length > 0){
                // response.data.results.forEach(async(noteItem)=>{
                for(var i=0;i < response.data.results.length; i++){
                    noteItem = response.data.results[i];
                    var noteResponse = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/notes/${noteItem.id}?properties=hs_note_body`, "GET", null);
                    if(noteResponse && noteResponse.data && noteResponse.data.id){
                        notesArray.push({
                            'id': noteResponse.data.id,
                            'content': noteResponse.data.properties.hs_note_body,
                            'title': "",
                            'time': noteResponse.data.createdAt
                        });
                    }
                }
            }
        return notesArray;
    },
    
    addModuleRecordNote: async function(module, entityId, noteContent){
        var noteResponse = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/notes`, "POST", {
            "properties": {
              "hs_timestamp": new Date().toISOString(),
              "hs_note_body": noteContent
              
            }
        });
        if(noteResponse.data && noteResponse.status && noteResponse.data.id){
            var associationsLableResponse = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v4/associations/notes/${module.toLocaleLowerCase()}/labels`, "GET", {});
            if(associationsLableResponse.data && associationsLableResponse.status && associationsLableResponse.data.results && associationsLableResponse.data.results.length > 0){
                var associationResponse = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/notes/${noteResponse.data.id}/associations/${module.toLocaleLowerCase()}/${entityId}/${associationsLableResponse.data.results[0].typeId}`, "PUT", {});
                if(associationResponse && associationResponse.data && associationResponse.data.id){
                    return associationResponse;
                }
            }
        }
        else{
            return {
                error: true
            }
        }
        return false;
    },
    
    updateRecordByField: async function(module, entityId, fieldName, fieldValue){
        let updateObjPayload = {};
            updateObjPayload[fieldName] = fieldValue;
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl(3)}/crm/v3/objects/${module.toLocaleLowerCase()}/${entityId}`, "PATCH", {"properties": updateObjPayload});
        if(response.data || response.status == 200){
            return response;
        }
        else{
            return {
                error: {
                    'message': 'Error, Try again'
                }
            }
        }
    },
    
    createRecordInModule: async function(module, fieldMap){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/${module.toLocaleLowerCase()}`, "POST", {"properties": fieldMap});
        if(response.data){
            let resultItemId = response.data.id;
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/${module.toLocaleLowerCase()}/${resultItemId}?properties=${Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[module].fields).join(",")}`, "GET", null);
            let recordItem = response.data;

            recordItem.name = await UA_SAAS_SERVICE_APP.getRecordName(module,recordItem);
            recordItem = {...recordItem,...recordItem.properties};
            delete recordItem.properties;

            recordItem.webURL = `https://app.hubspot.com/${module.toLocaleLowerCase()}/${appsConfig.UA_DESK_ORG_ID}/${module.toLocaleLowerCase()}/${recordItem.id}`;
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[resultItemId] = recordItem;
            return recordItem;
        }
        else if(response.error){
            showErroWindow('Unable to create '+module, response.error.message);
            return {
                error: {
                    'message': response.error.message
                }
            };
        }
        else{
            return {
                error: {
                    'message': 'Error, Try again'
                }
            };
        }
    },
    
    getNewRecordInSAASWebURL: function(module){
        let modules = ["", "Contacts","Companies","Deals","","Tickets"]
        return `https://app.hubspot.com/contacts/${appsConfig.UA_DESK_ORG_ID}/objects/0-${modules.indexOf(module)}/views/all/list`;
    },

    getRecordName: async function(module,item){
        var fullName = "";
            switch(module){
                case("Contacts"): {
                    fullName = item['properties'].firstname ? item['properties'].firstname:"";
                    if(item['properties'].lastname){
                        fullName = fullName+" "+ item['properties'].lastname;
                    } 
                    fullName = fullName?fullName:"Contact#"+item.id;
                    break;
                }
                case("Companies"):{
                    fullName = item['properties'].name ? item['properties'].name:"Company#"+item.id;
                    break;
                }
                case("Deals"):{
                    fullName = item['properties'].dealname ? item['properties'].dealname:"Deal#"+item.id;
                    break;
                }
                case("Tickets"):{
                    fullName = item['properties'].subject ? item['properties'].subject:"Ticket#"+item.id;
                    break;
                }
            }
        return fullName;
    },

    searchRelatedRecordsToAllModules: async function(searchTerm, field="phone"){
        try{
            var searchData = [];
            if(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES && Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).length > 0){
                for(var i=0; i < Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).length; i++){
                    let module = Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES)[i];
                    let data = await UA_SAAS_SERVICE_APP.getSearchedResultItems(searchTerm, module, field);
                    if(data && data.status && data.status == "retry_request"){
                        return [{status: "retry_request"}];
                    }
                    if(data && data.length > 0){
                        // Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS).forEach((id)=>{
                        for(var j=0; j < Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS).length; j++){
                            let id = Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS)[j];                        
                            let item = UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id]
                            searchData.push({module: module, record: item});
                        }
                    }
                }
            }  
            return searchData;
        }catch(e){ console.log(e); return []; }
    }
};