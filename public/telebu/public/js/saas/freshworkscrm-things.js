var UA_SAAS_SERVICE_APP = {
    API_BASE_URL : "",
    CURRENT_USER_INFO : null,
    CURRENT_ORG_INFO : null,
    CURRENT_OAUTH_SCOPE_NEEDED : [],
    FETCHED_RECORD_DETAILS: {},
    CURRENT_MODULE_FIELDS_DROPDOWN : [],
    SELECTED_RECEIPS: {},
    SEARCH_MODULE_RECORD_MAP: {},
    widgetContext: {},
    appsConfig: {},
    APP_API_NAME: "pinnaclesms__",
    isModelOpened: false,
    APP_CLIENT: null,
    fetchedList : {},
    supportedIncomingModules: [
        {
            "selected": true,
            "value": "contact",
            "label": "Contacts"
        }
    ],
    
    getAPIBaseUrl : function(){
        return `https://${UA_SAAS_SERVICE_APP.API_BASE_URL}/crm/sales/api`;
    },

    provideSuggestedCurrentEnvLoginEmailID: async function(){
        var client = await app.initialized();
        client.data.get("loggedInUser").then (
            function(response) {
                if(response && response.loggedInUser && response.loggedInUser.email) {
                    UA_APP_UTILITY.receivedSuggestedCurrentEnvLoginEmailID(response.loggedInUser.email);
                }
            },
            function(error) {
              // failure operation
            });
    },
    
    initiateAPPFlow: async function(){
        $("#ac_name_label_saas .anl_servicename").text('Freshworks CRM');
        var client = await app.initialized();
        UA_SAAS_SERVICE_APP.APP_CLIENT = client;
        var domainNameQuery = await UA_SAAS_SERVICE_APP.APP_CLIENT.data.get("domainName");
        UA_SAAS_SERVICE_APP.API_BASE_URL = domainNameQuery.domainName;
        UA_OAUTH_PROCESSOR.currentSAASDomain = UA_SAAS_SERVICE_APP.API_BASE_URL;
        
        if(UA_SAAS_SERVICE_APP.APP_CLIENT.context.location === "full_page_app"){
            UA_SAAS_SERVICE_APP.widgetContext.module = null;
        }
        else{
            var entityData = (await UA_SAAS_SERVICE_APP.APP_CLIENT.data.get('currentEntityInfo')).currentEntityInfo;
            UA_SAAS_SERVICE_APP.widgetContext.entity = entityData[UA_SAAS_SERVICE_APP.widgetContext.module];
            UA_SAAS_SERVICE_APP.widgetContext.entityId = entityData.currentEntityId;
            UA_SAAS_SERVICE_APP.widgetContext.module = entityData.currentEntityType;
//            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[UA_SAAS_SERVICE_APP.widgetContext.entityId] = UA_SAAS_SERVICE_APP.widgetContext.entity;
        }

        UA_SAAS_SERVICE_APP.injectShowSearchContact();

        UA_SAAS_SERVICE_APP.initiateAPPFlow_SA();
        
        UA_SAAS_SERVICE_APP.getCurrentUserInfo(function(response){
            UA_SAAS_SERVICE_APP.CURRENT_USER_INFO = response;
            appsConfig.APP_UNIQUE_ID = UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.id;
            appsConfig.UA_DESK_ORG_ID = UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.account_id;
            $("#saasAuthIDName").text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.display_name);//.attr('title', `Authorized Zoho Account: (${UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.contact.email})`);
            setTimeout((function(){
                $('#ac_name_label_saas .ac_name_id').text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.email);
            }), 2000);
            UA_SAAS_SERVICE_APP.proceedToAppInitializationIfAPPConfigResolved();
        });
        
    },
    
    initiateAPPFlow_SA: async function(){
        let params = new URLSearchParams(location.search);
        if(!UA_SAAS_SERVICE_APP.widgetContext.module && queryParams.get("module")){
            UA_SAAS_SERVICE_APP.widgetContext.module = queryParams.get("module");
            UA_SAAS_SERVICE_APP.widgetContext.entityId = queryParams.get("entityId");
            UA_SAAS_SERVICE_APP.injectShowSearchContact(); 
        }

        $("#ac_name_label_saas .anl_servicename").text('Freshworks CRM');
        if(!UA_OAUTH_PROCESSOR.currentSAASDomain){
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/crm/sales/api/search?q=${encodeURIComponent(currentUser.email)}&include=user`, "GET", null);
            UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
            if(response.error && response.error.errors && response.error.errors.message){
                showErroWindow('Error : ', response.error.errors.message[0]);
            }
            if(response && response.api_domain){
                UA_OAUTH_PROCESSOR.currentSAASDomain = response.api_domain;
                UA_SAAS_SERVICE_APP.API_BASE_URL = response.api_domain;
                $('#ac_name_label_saas .ac_name_id').text(response.api_domain);
            }
            if(response && response.data && response.data.length > 0){
                UA_SAAS_SERVICE_APP.CURRENT_USER_INFO = response.data[0];
                var userResponse = await UA_OAUTH_PROCESSOR.getAPIResponse(`/crm/sales/settings/users/${response.data[0].id}`, "GET", null);
                UA_SAAS_SERVICE_APP.CURRENT_USER_INFO = userResponse.data["user"];
                appsConfig.APP_UNIQUE_ID = UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.id;
                appsConfig.UA_DESK_ORG_ID = UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.deal_pipeline_id;
                $("#saasAuthIDName").text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.display_name);//.attr('title', `Authorized Zoho Account: (${UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.contact.email})`);
                setTimeout((function(){
                    $('#ac_name_label_saas .ac_name_id').text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.email);
                }), 2000);
            }
            UA_SAAS_SERVICE_APP.proceedToAppInitializationIfAPPConfigResolved();
        }
    },
    
    addSAASUsersToLICUtility: async function(){
        let url = `/crm/sales/api/selector/owners`;
        let admin_role_id = '';
        $('.saasServiceName').text("Freshworks CRM");
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(url, "GET", null);
        if(!response || !response.data || !response.data.users){
            return;
        }
        // response.data.roles.forEach(item=>{
        //     if(item.name === "Account Admin"){
        //         admin_role_id = item.id;
        //     }
        // });
        response.data.users.sort(function(a, b) {
            return a.display_name.localeCompare(b.display_name);
        });
        response.data.users.forEach(item=>{
            UA_LIC_UTILITY.addFetchedSAASUser(item.id, item.display_name, item.email, null, true, item.mobile_number || item.work_number, url = null);
        });
        UA_LIC_UTILITY.saasUserListFetchCompleted();
    },
    
    proceedToAppInitializationIfAPPConfigResolved: function(){
        if(appsConfig.APP_UNIQUE_ID && appsConfig.UA_DESK_ORG_ID){
            UA_SAAS_SERVICE_APP.renderInitialElements();
            UA_SAAS_SERVICE_APP.addSAASUsersToLICUtility();
            UA_APP_UTILITY.appsConfigHasBeenResolved();
            if(UA_SAAS_SERVICE_APP.widgetContext.module)
            {
                UA_SAAS_SERVICE_APP.renderInitialElementsForFreshsalesList();
            }
        }
    },
    
    renderInitialElements: async function(){
        UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown('users', ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder']);
//        UA_APP_UTILITY.renderSavedTemplatesInDropdowns();
        if(!UA_SAAS_SERVICE_APP.widgetContext.module){
            UA_SAAS_SERVICE_APP.entityDetailFetched(null);
            console.log('Module does not exist, rendering skipped');
            $("#recip-count-holder").text("Add recipients to proceed");
            return;
        }
        
        
        /*
        var contactData = await UA_SAAS_SERVICE_APP.APP_CLIENT.data.get("contact");
        contactData = contactData.contact;
        
        
        UA_SAAS_SERVICE_APP.addRecordsAsRecipients([
            contactData
        ]); */
        
        UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(UA_SAAS_SERVICE_APP.widgetContext.module, UA_SAAS_SERVICE_APP.widgetContext.entityId, function(resp){
            UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], console.log);
            UA_SAAS_SERVICE_APP.addRecordsAsRecipients(resp);
        });
    },

    renderInitialElementsForFreshsalesList: async function(isList){
        $(".ssf-new-recip-form").prepend(`<div id="ssf-recip-list-add-holdr${isList?'-is':''}" class="ssf-recip-choice-btn" style="margin-top: -5px;background: none;border: none;box-shadow: none;"></div>`);
        UA_SAAS_SERVICE_APP.renderContactList(isList);
        (UA_SAAS_SERVICE_APP.widgetContext.module == 'contact' && isList != true)?UA_SAAS_SERVICE_APP.renderInitialElementsForFreshsalesList(true):"";
    },
    
    renderContactList: async function(isList){
        try{
            let type = isList?'lists':'filters';
            let url = `/crm/sales/api/${UA_SAAS_SERVICE_APP.widgetContext.module}s/filters`;
            url = isList == true ? `/crm/sales/lists?page=1&perPage=25`:url;
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(url, "GET", null);
    //        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
            console.log('renderList',response);
            let listDropdownItems = [];
            response.data[type].forEach(item=>{
                UA_SAAS_SERVICE_APP.fetchedList[item.id] = item;
                listDropdownItems.push({
                    'label': item.name,
                    'value': isList? 'List-'+item.id : item.id
                });
            });

            UA_APP_UTILITY.renderSelectableDropdown(`#ssf-recip-list-add-holdr${isList?'-is':''}`, isList==true?'Select a List':'Select a Filter', listDropdownItems, 'UA_SAAS_SERVICE_APP.insertManageListContacts', false, false);
        }
        catch(ex){
            console.log(ex);
        }
    
    },

    insertManageListContacts: async function(listId){
        let url = `/crm/sales/api/${UA_SAAS_SERVICE_APP.widgetContext.module}s/view/${listId}`;
        url = listId.includes('List-') == true ? `/crm/sales/contacts/lists/${listId.split('-')[1]}?sort_type=desc&page=1&per_page=100`:url;

        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(url, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('renderList',response);
        if(response.data && response.data[`${UA_SAAS_SERVICE_APP.widgetContext.module}s`] && response.data[`${UA_SAAS_SERVICE_APP.widgetContext.module}s`].length > 0) {
            response.data[`${UA_SAAS_SERVICE_APP.widgetContext.module}s`].forEach(item=>{
                UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(UA_SAAS_SERVICE_APP.widgetContext.module,item.id, async function(contactList){
                    UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
                    UA_SAAS_SERVICE_APP.addRecordsAsRecipients(contactList);
                });
            });
        }
            
    },
    
    sendPhoneFieldsForRecipientType: function(fieldsArray){
        fieldsArray.forEach(item=>{
            if(item.display_label === "phone"){
                UA_APP_UTILITY.addRecipientPhoneFieldType(item.api_name, item.display_label);
            }
        });
    },
    
    populateModuleItemFieldsInDropDown: async function (module, target, fieldsCallback=(()=>{})){
        /*var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.API_BASE_URL}/settings/fields?module=${module}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log(response);
        response.data.fields.forEach(item=>{
            dropDownValues.push({
                "label": item.display_label,
                "value": (module.toUpperCase() === "USERS" ? "CURRENT_USER": module.toUpperCase())+'.'+item.api_name
            });
        }); */
        if(!module || UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.includes(module) == true){
            return;
        }
        UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.push(module);
        var moduleToLoad = module === "users" ? UA_SAAS_SERVICE_APP.CURRENT_USER_INFO : UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[UA_SAAS_SERVICE_APP.widgetContext.entityId?UA_SAAS_SERVICE_APP.widgetContext.entityId:Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS)[0]];
        var moduleFields = Object.keys(moduleToLoad);
        var fieldsArray = [];
        var dropDownValues = [];
        moduleFields.forEach(item=>{
            let itemVal = moduleToLoad[item];
            if(typeof itemVal !== "object" && itemVal !== null && typeof itemVal!=="boolean"){
                fieldsArray.push({
                    "api_name": item,
                    "display_label": item
                });
                dropDownValues.push({
                    "label": item,
                    "value": (module.toUpperCase() === "USERS" ? "CURRENT_USER": module.toUpperCase())+'.'+item
                });
            }
        });
        fieldsCallback(fieldsArray);
        if(typeof target === "string"){
            target = [target];
        }
        target.forEach(item=>{
            UA_APP_UTILITY.renderSelectableDropdown(item, `${module} fields`, dropDownValues, 'UA_APP_UTILITY.templateMergeFieldSelected', false, true);
        });
        return true;
    },
    
    injectShowSearchContact :async function(){
        $(".ssf-new-recip-form").prepend(`<div id="searchAndAddFromContactBtn" class="ssf-recip-choice-btn" onclick="$('.item-list-popup').show()">
                                            <i class="material-icons">search</i> Search from ${UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase().replace("_"," "):"contact"}
                                      </div>`);
        $('.pageContentHolder').append(`<div class="item-list-popup" style="display:none">
                        <div class="item-list-popup-title">
                            Search ${(UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase().replace("_"," "):"contact").capitalize()} <div class="pop-win-close" onclick="$('.item-list-popup').hide();">x</div>
                        </div>
                        <div class="item-search-box">
                            <input class="input-form" id="contact-seach-name" type="text" autocomplete="off" placeholder="Name of the ${UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase():"contact"}"/><br>
                            <div class="btn-save" onclick="UA_SAAS_SERVICE_APP.searchRecordsAndRender($('#contact-seach-name').val(),UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase():'contact')">
                                Search ${(UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase().replace("_"," "):"contact").capitalize()}
                            </div>
                            <div class="btn-reset" onclick="UA_SAAS_SERVICE_APP.resetsearchContacts()">
                                Reset
                            </div>
                        </div>
                        <div class="item-list-popup-content" id="contact-search-items">

                        </div>
                    </div>`);
    },

    resetsearchContacts: function(){
        $("#contact-seach-name").val("");
        $("#contact-search-items").html("");
    },

    searchRecordsAndRender: async function(searchTerm,module){
        if(!valueExists(searchTerm)){
            return;
        }
        UA_SAAS_SERVICE_APP.widgetContext.module = UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module:"contact";
            await UA_SAAS_SERVICE_APP.getAPIResponseAndCallback(`/crm/sales/api/search?q=${searchTerm}&include=${module}&per_page=50` , function(response){
                console.log(response);
                $("#contact-search-items").html(" ");
                if(response && response.data && response.data.length>0)
                {
                    var search_items = response.data;
                    if(search_items && search_items.length > 50){
                        search_items.splice(0,49);
                    }
                    UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module] = {};
                    search_items.forEach((obj)=>{
                        UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][obj.id] = obj;
                        let fullName = obj.name ? obj.name:module+"#"+obj.id;
                        if(module)
                        {
                            let itemHTML = `<div class="item-list-popup-item"><span class="c-name" style="cursor:pointer;" onclick="UA_SAAS_SERVICE_APP.selectPhoneNumber('${obj.id}', '${module.trim()}', '${fullName}','${module}','${true}')"> ${fullName} </span>`;
                            // itemHTML =  itemHTML + `${'<span class="c-phone '+(UA_SAAS_SERVICE_APP.SELECTED_RECEIPS["customers_"+obj.id] ? 'alreadyadded': '')+'" onclick="UA_SAAS_SERVICE_APP.selectPhoneNumber(\''+obj.id+'\', \''+"customers"+'\', \''+fullName+'\',\''+module+'\',\''+true+'\')"> <i class="material-icons">phone_iphone</i>( Customer ) </span>'}`;
                            itemHTML =  itemHTML + `</div>`;
                            $("#contact-search-items").append(itemHTML);
                        }    
                        
                    });
                }
                else
                {
                    $("#contact-search-items").html("No records found. <br><br> <span class=\"c-silver\"> This search will include <br> only customers having phone numbers</span>");
                    return;
                }
            }).catch(err=>{
                console.log(err);
            });
    },

    selectPhoneNumber: async function(id,number,name,module,isSelected){
        var receipNumberID = module + "_" + id;
        if(!UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] && !UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id]){
            // if(true){
            UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] = {
                'id': id,
                'name': name,
                'module': module,
                'isSelected': isSelected
            };
            $.extend(UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id],UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID]) ;
            // UA_SAAS_SERVICE_APP.renderInitialElements(module,id);

            await UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(module, id, UA_SAAS_SERVICE_APP.addRecordsAsRecipients);
            await UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);

            // UA_SAAS_SERVICE_APP.addRecordsAsRecipients([UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id]]);
        }
        else {
            console.log(module+' already added');
            alert(`This ${module} is already added.`);
            return;
        }
    },
    
    getCurrentUserInfo : async function(callback){
        var loggedInUser = (await UA_SAAS_SERVICE_APP.APP_CLIENT.data.get("loggedInUser")).loggedInUser;
        callback(loggedInUser);
    },
    
    getCurrentOrgInfo : async function(callback){
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/crm/sales/api/account', callback);
    },
    
    getAPIResponseAndCallback: async function(url, callback){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${url}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        callback(response);
    },
    
    showErrorIfInAPICall : async function(response){
        if(response.code === 401 || response.code === 403){
            if(UA_SAAS_SERVICE_APP.APP_CLIENT && UA_SAAS_SERVICE_APP.APP_CLIENT.data){
                var loggedInUser = await UA_SAAS_SERVICE_APP.APP_CLIENT.data.get("loggedInUser");
                loggedInUser = loggedInUser.loggedInUser;
            }
            UA_OAUTH_PROCESSOR.showReAuthorizationError("Freshworks CRM", `<a href="https://${UA_SAAS_SERVICE_APP.API_BASE_URL}/crm/sales/personal-settings/api-settings" target="_blank">Click to get API key</a>`, false, false);
        }
    },
    
    initiateAuthFlow: function(){
        UA_OAUTH_PROCESSOR.initiateAuth();
    },
    
    fetchStoreExecuteOnRecordDetails: async function(module, recordIds, callback){
        if(typeof recordIds == 'string' || typeof recordIds == "number") {
            recordIds = recordIds.toString().split(",");
        }
        $("#recip-count-holder").text(`fetching ${module.toLocaleLowerCase()} information...`);
        var params = '';
        if(module === "deal"){
            params+='&include=contacts';
        }
        for(var i=0; i < recordIds.length; i++){
            let id = recordIds[i];
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/crm/sales/api/${module}s/${id}?${params}`, "GET");
            if(!queryParams.get("disableSAASSdk") || queryParams.get("disableSAASSdk") !== 'true'){
                UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
            }
            console.log('fetchStoreExecuteOnRecordDetails',response);
            var contactData = response.data[UA_SAAS_SERVICE_APP.widgetContext.module];
            UA_SAAS_SERVICE_APP.widgetContext.entity = contactData;
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[contactData.id] = contactData;
            if(module === "deal"){
                response.data.contacts.forEach(item=>{
                    item.id = contactData.id;
                });
                UA_SAAS_SERVICE_APP.addRecordsAsRecipients(response.data.contacts);
            }
        }
        $("#recip-count-holder").text(`Selected ${recordIds.length} ${module.toLocaleLowerCase()}`);
        
        callback(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
        UA_SAAS_SERVICE_APP.entityDetailFetched();
    },
    
    addRecordsAsRecipients: function(contactsList){
        for (var item in contactsList) {
            if($(`[data-recip-id=${contactsList[item]['id']}]`).length === 0) {
                ["work_number", "mobile_number", "phone"].forEach(mobileFieldItem => {
                    if (contactsList[item][mobileFieldItem]) {
                        UA_APP_UTILITY.addRecipientPhoneFieldType(mobileFieldItem, mobileFieldItem);
                    }
                });
                if(!contactsList[item].name){
                    contactsList[item].name = contactsList[item].display_name;
                }
                UA_APP_UTILITY.addInStoredRecipientInventory(contactsList[item]);
            }
        }
    },
    
    addSentSMSAsRecordInHistory: async function(sentMessages){
        if(!UA_SAAS_SERVICE_APP.widgetContext.module){
            return;
        }
//        var historyDataArray = [];

        var credAdProcess3 = curId++;
        showProcess(`Adding ${Object.keys(sentMessages).length} messages to ticket comments...`, credAdProcess3);

        
        for(var sentMessageId in sentMessages){
            let commentContent = appPrettyName+` Extension: `;
            let sentMessage = sentMessages[sentMessageId];
            let recModule = sentMessage.module?sentMessage.module:UA_SAAS_SERVICE_APP.widgetContext.module; 
            let recId = sentMessage.moduleId?(sentMessage.moduleId.indexOf("newmanual")!==0?sentMessage.moduleId:UA_SAAS_SERVICE_APP.widgetContext.entityId):UA_SAAS_SERVICE_APP.widgetContext.entityId;
            
            commentContent+=`${sentMessage.channel} ${sentMessage.status} from ${sentMessage.from} to ${sentMessage.to}:
                             ${sentMessage.message}`;
            var noteTargetTypeVsEntityMap = {
                "lead": "Lead",
                "contact": "Contact",
                "deal": "Deal",
                "sales_account":"SalesAccount"
                };
                var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/crm/sales/api/notes`, "POST", {
                    note: {
                        "description" : commentContent,
                        "targetable_type": noteTargetTypeVsEntityMap[recModule],
                        "targetable_id": recId
                    },
                    private: true
                });
            UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        }
        
        processCompleted(credAdProcess3);
        console.log('addSentSMSAsRecordInHistory',response);
        UA_SAAS_SERVICE_APP.APP_CLIENT.interface.trigger("showNotify", {
            type: "success",
            message: `All Messages has been added to ${UA_SAAS_SERVICE_APP.widgetContext.module} notes`
          /* The "message" should be plain text */
          }).then(function(data) {
          // data - success message
          }).catch(function(error) {
          // error - error object
          });
        
    },
    entityDetailFetched: function(data){
        setTimeout(()=>{UA_APP_UTILITY.fetchedAllModuleRecords();}, 2000);
        if(typeof UA_TPA_FEATURES.callOnEntityDetailFetched === "function"){
            UA_TPA_FEATURES.callOnEntityDetailFetched(data);
        }
    },

// LookUp functions
    LOOKUP_SUPPORTED_MODULES: {
        "Contacts": {
            "autoLookup": true,
            "apiName": "contact"
        },
        "Accounts": {
            "autoLookup": true,
            "apiName": "sales_account"
        },
        "Leads": {
            "hideDisplay": true,
            "autoLookup": false,
            "apiName": "lead"
        }
    },
    
    moduleIDs: {
        "Contacts": "contact",
        "Accounts": "sales_account",
        "Leads": "lead"
    },

    allModuleFieldsFetched: function(){
        let fieldsPresentModuleCount = 0;
        Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).forEach(item=>{
            if(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[item].fields && Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[item].fields).length > 0){
                fieldsPresentModuleCount++;
            }
        });
        return Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).length === fieldsPresentModuleCount;
    },

    retrieveModuleFieldsForLookUp: function(callback){
        $('#ua-loo-search-field-ddholder').hide();
        if (UA_SAAS_SERVICE_APP.allModuleFieldsFetched()) {
            callback();
        }
        var primaryFieldsToCreate = [];
        let primaryFieldsToViewForModule = ["id"];

        Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).forEach(async (moduleId)=>{
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/crm/sales/api/settings/${UA_SAAS_SERVICE_APP.moduleIDs[moduleId]}s/fields`, "GET", null);
            let fieldAPINameMap = {};
            if(!response.data || !response.data.fields || response.data.fields.length < 1){
                return;
            }
            var fields = response.data.fields;
            for(var i=0; i < fields.length; i++){
                let fieldItem = fields[i];
                if(fieldItem.type != "group_field" && fieldItem.type != "auto_complete") {
                    if(["lookup"].includes(fieldItem.fieldType)){
                        fieldItem.field_read_only = true;
                    }

                    fieldItem['display_label'] = fieldItem.label;
                    fieldItem['api_name'] = fieldItem.name;
                    fieldItem["system_mandatory"] = fieldItem.required;
                    fieldItem['data_type'] = fieldItem.type;

                    if(fieldItem['data_type'] == "url"){
                        fieldItem['data_type'] = "text";
                    }
                    if(fieldItem['data_type'] == "date"){
                        fieldItem['data_type'] = "datetime";
                    }
                    if(fieldItem['data_type'] == "dropdown" || fieldItem['data_type'] == "group_field"){
                        fieldItem['data_type'] = "picklist";
                    }
                    if(fieldItem.choices && fieldItem.choices.length > 0){
                        fieldItem["pick_list_values"] = [];
                        for(var optInd=0; optInd < fieldItem.choices.length; optInd++){
                            let option = fieldItem.choices[optInd];
                            fieldItem["pick_list_values"][optInd] = {
                                "actual_value": option.id,
                                "display_value": option.value
                            }
                        }
                    }
                    fieldAPINameMap[fieldItem.name] = fieldItem;
                }
            }
            fieldAPINameMap.id = {
                'display_label': 'ID',
                'data_type': 'number',
                'api_name': 'id',
                'field_read_only': true
            };            
            
            switch(moduleId){
                case("Contacts"): {
                    primaryFieldsToCreate = ["first_name", "last_name","email","mobile_number","owner_id"];
                    primaryFieldsToViewForModule = ["first_name", "last_name","email","mobile_number"];
                    fieldAPINameMap.email = {
                        'display_label': 'Email',
                        'data_type': 'text',
                        'api_name': 'email',
                        'field_read_only': false
                    };
                    break;
                }
                case("Accounts"):{
                    primaryFieldsToCreate = ["name", "website","phone","owner_id"];
                    primaryFieldsToViewForModule = ["name", "website","phone"];
                    break;
                }
            }

            UA_TPA_FEATURES.setCreateModuleRecordFields(moduleId, primaryFieldsToCreate);
            UA_TPA_FEATURES.setViewModuleRecordFields(moduleId, primaryFieldsToViewForModule);
            UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[moduleId].fields = fieldAPINameMap;
            if(UA_SAAS_SERVICE_APP.allModuleFieldsFetched()){
                callback();
            }
        });
    },

    getSearchedResultItems: async function(searchTerm, module, field){
        UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS = {};
        if(searchTerm.startsWith('+')){
            searchTerm = searchTerm.replace('+', '%2B');
        }
        if(field && field == "Phone" && module == "Contacts"){
            field = "mobile_number";
        }
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/crm/sales/api/lookup?q=${searchTerm}&f=${field.toLocaleLowerCase()}&entities=${UA_SAAS_SERVICE_APP.moduleIDs[module]}`,"GET",{});
        if(response && response.data && response.data[`${UA_SAAS_SERVICE_APP.moduleIDs[module]}s`] && response.data[`${UA_SAAS_SERVICE_APP.moduleIDs[module]}s`][`${UA_SAAS_SERVICE_APP.moduleIDs[module]}s`]){ 
            var responseData = response.data[`${UA_SAAS_SERVICE_APP.moduleIDs[module]}s`][`${UA_SAAS_SERVICE_APP.moduleIDs[module]}s`];
            for(var j=0; j < responseData.length; j++){
                var item = responseData[j];
                var fullName = module=="Contacts"?item.display_name:item.name;
                item.name = fullName;
                item.id = item.id.toString();
                item.webURL = `https://${UA_SAAS_SERVICE_APP.API_BASE_URL}/crm/sales/${module.toLocaleLowerCase()}/${item.id}`;
                UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id] = item;     
            }
        }
        return response.data;
    },
    
    getNotesOfRecord: async function(module, entityId){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/crm/sales/api/${UA_SAAS_SERVICE_APP.moduleIDs[module]}s/${entityId}/notes`, "GET", null);
            let notesArray = [];
            if(response && response.data && response.data.notes && response.data.notes.length > 0){
                for(var i=0;i < response.data.notes.length; i++){
                    noteItem = response.data.notes[i];
                    notesArray.push({
                        'id': noteItem.id,
                        'content': noteItem.description,
                        'title': "",
                        'time': noteItem.updated_at
                    });
                }
            }
        return notesArray;
    },
    
    addModuleRecordNote: async function(module, entityId, noteContent){
        var noteTargetTypeVsEntityMap = {
            "Contacts": "Contact",
            "Accounts": "SalesAccount"
        };
        var noteResponse = await UA_OAUTH_PROCESSOR.getAPIResponse(`/crm/sales/api/notes`, "POST", {
                note: {
                    "description" : noteContent,
                    "targetable_type": noteTargetTypeVsEntityMap[module],
                    "targetable_id": entityId
                },
                private: true
        });
        if(noteResponse.data && noteResponse.data.note){
            return noteResponse.data;
        }
        else{
            return {
                error: true
            }
        }
        return false;
    },
    
    updateRecordByField: async function(module, entityId, fieldName, fieldValue){
        let updateObjPayload = {};
        updateObjPayload[`${UA_SAAS_SERVICE_APP.moduleIDs[module]}`] = {};
        if(fieldName.startsWith("cf")){
            updateObjPayload[`${UA_SAAS_SERVICE_APP.moduleIDs[module]}`]["custom_field"] = {};
            updateObjPayload[`${UA_SAAS_SERVICE_APP.moduleIDs[module]}`]["custom_field"][fieldName] = fieldValue;
        }
        else{
            updateObjPayload[`${UA_SAAS_SERVICE_APP.moduleIDs[module]}`][fieldName] = fieldValue;
        }
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/crm/sales/api/${UA_SAAS_SERVICE_APP.moduleIDs[module]}s/${entityId}`, "PUT", updateObjPayload);
        if(response.data && response.status == 200){
            return response;
        }
        else if(response.error && response.error.message){
            showErroWindow('Unable to create '+module, response.error.message);
            return {
                error: {
                    'message': response.error.message
                }
            }
        }
        else{
            return {
                error: {
                    'message': 'Error, Try again'
                }
            }
        }
    },
    
    createRecordInModule: async function(module, fieldMap){
        if(!fieldMap){
            return;
        }
        let updateObjPayload = {};
        updateObjPayload[`${UA_SAAS_SERVICE_APP.moduleIDs[module]}`] = {};
        Object.keys(fieldMap).forEach((fieldName)=>{
            let fieldValue = fieldMap[fieldName];
            if(fieldName.startsWith("cf")){
                if(!updateObjPayload[`${UA_SAAS_SERVICE_APP.moduleIDs[module]}`]["custom_field"]){
                    updateObjPayload[`${UA_SAAS_SERVICE_APP.moduleIDs[module]}`]["custom_field"] = {};
                }
                updateObjPayload[`${UA_SAAS_SERVICE_APP.moduleIDs[module]}`]["custom_field"][fieldName] = fieldValue;
            }
            else{
                updateObjPayload[`${UA_SAAS_SERVICE_APP.moduleIDs[module]}`][fieldName] = fieldValue;
            }
        });
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/crm/sales/api/${UA_SAAS_SERVICE_APP.moduleIDs[module]}s`, "POST", updateObjPayload);
        if(response.data && response.data[`${UA_SAAS_SERVICE_APP.moduleIDs[module]}`]){
            let resultItemId = response.data[`${UA_SAAS_SERVICE_APP.moduleIDs[module]}`].id;
            let recordItem = response.data[`${UA_SAAS_SERVICE_APP.moduleIDs[module]}`];

            recordItem.name = module=="Contacts"?recordItem.display_name:recordItem.name;
            recordItem.webURL = `https://${UA_SAAS_SERVICE_APP.API_BASE_URL}/crm/sales/${module.toLocaleLowerCase()}/${recordItem.id}`;
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[resultItemId] = recordItem;
            return recordItem;
        }
        else if(response.error){
            var errMsg = "Error, Try again";
            if(response.error && response.error.message){
                errMsg = response.error.message;
            }
            else if(response.error && response.error.errors){
                errMsg = response.error.errors.message[0];
            }
            showErroWindow('Unable to create '+module, errMsg);
            return {
                error: {
                    'message': errMsg
                }
            };
        }
        else{
            return {
                error: {
                    'message': 'Error, Try again'
                }
            };
        }
    },
    
    getNewRecordInSAASWebURL: function(module){
        return `https://${UA_SAAS_SERVICE_APP.API_BASE_URL}/crm/sales/${module.toLocaleLowerCase()}/`;
    }
};