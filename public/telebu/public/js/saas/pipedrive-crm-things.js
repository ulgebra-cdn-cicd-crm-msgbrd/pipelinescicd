var UA_SAAS_SERVICE_APP = {
    
    CURRENT_USER_INFO : null,
    CURRENT_ORG_INFO : null,
    CURRENT_OAUTH_SCOPE_NEEDED : ["automation","timeline","tickets","crm.objects.contacts.read","crm.objects.contacts.write","crm.objects.companies.write","crm.schemas.contacts.read","crm.objects.companies.read","crm.objects.deals.read","crm.objects.deals.write","crm.schemas.companies.read","crm.schemas.companies.write","crm.schemas.contacts.write","crm.schemas.deals.read","crm.schemas.deals.write","crm.objects.owners.read", "settings.users.read"],
    FETCHED_RECORD_DETAILS: {},
    CURRENT_MODULE_FIELDS_DROPDOWN : [],
    SELECTED_RECEIPS: {},
    SEARCH_MODULE_RECORD_MAP: {},
    fetchedList : {},
    widgetContext: {},
    APP_API_NAME: "pinnaclesms__",
    DESK_APP: null,
    supportedIncomingModules: [
        {
            "selected": true,
            "value": "person_only",
            "label": "Sync events to person"
        },
        {
            "value": "lead_latest_or_create",
            "label": "Sync Events to latest lead or create new lead"
        },
        {
            "value": "deal_latest_or_create",
            "label": "Sync Events to latest deal or create new deal"
        }
    ],
    
    getAPIBaseUrl : function(){
        return ``;
    },
    
    initiateAPPFlow: async function(){
        $("#ac_name_label_saas .anl_servicename").text('Pipedrive');
        if(eventApps.includes(extensionName.split("for")[0]) == true){
            UA_SAAS_SERVICE_APP.supportedIncomingModules.push({
                "value": "activity_only",
                "label": "Only activity"
            });
        }
        UA_SAAS_SERVICE_APP.insertIncomingConfigItems();
        if (UA_SAAS_SERVICE_APP.DESK_APP) {
            if (queryParams.get("openMode") === "modal") {
                await UA_SAAS_SERVICE_APP.DESK_APP.execute(Command.RESIZE, {height: 600, width: 800});
                $('body').removeClass('sidePanelView');
            }
        }
//        ZOHO.embeddedApp.on("PageLoad", async function(widgetContext) {
            
//            console.log('embeddd', widgetContext); 
//            
//            var crmEnv = await ZOHO.CRM.CONFIG.GetCurrentEnvironment();
//            UA_OAUTH_PROCESSOR.currentSAASDomain = UA_OAUTH_PROCESSOR.domainExtnMap[crmEnv.deployment];
//            
//            UA_SAAS_SERVICE_APP.widgetContext.module = widgetContext.Entity;
//            UA_SAAS_SERVICE_APP.widgetContext.entityId = widgetContext.EntityId;
            if(!queryParams.get("selectedIds") && queryParams.get("entityId")){
                queryParams.set("selectedIds", queryParams.get("entityId"));
            }
            if(!queryParams.get("resource") && queryParams.get("module")){
                queryParams.set("resource", queryParams.get("module"));
            }
            UA_SAAS_SERVICE_APP.widgetContext.module = queryParams.get("resource");
            UA_SAAS_SERVICE_APP.widgetContext.entityId = queryParams.get("selectedIds");
            if(UA_SAAS_SERVICE_APP.widgetContext.module)
            {
                UA_SAAS_SERVICE_APP.injectShowSearchContact(); 
            }
            UA_SAAS_SERVICE_APP.getCurrentUserInfo(function(response){
                UA_OAUTH_PROCESSOR.currentSAASDomain = response.api_domain;
                UA_SAAS_SERVICE_APP.CURRENT_USER_INFO = response.data.data;
                appsConfig.APP_UNIQUE_ID = UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.id+"";
                appsConfig.UA_DESK_ORG_ID = UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.company_id+"";
                if((queryParams.get("companyId") && appsConfig.UA_DESK_ORG_ID !== queryParams.get("companyId")) || (queryParams.get("userId") && appsConfig.APP_UNIQUE_ID !== queryParams.get("userId"))){
                    UA_OAUTH_PROCESSOR.showReAuthorizationError(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED);
                }
                $("#saasAuthIDName").text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.name).attr('title', `Authorized Pipedrive Account: ${UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.company_domain}.pipedrive.com`);
                setTimeout((function(){
                    $('#ac_name_label_saas .ac_name_id').text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.company_domain + ` (${UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.email})`).attr('title', UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.company_domain+'.pipedrive.com');
                }), 2000);
                UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown('users', ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder']);
                UA_SAAS_SERVICE_APP.proceedToAppInitializationIfAPPConfigResolved();
            });

            if(extensionName === "whatcetraforpipedrive"){
                $('#ssf-manual-workflow-btn-holder').append(`<div class="ua-hd" style="position: relative;width: 1px;background: #aaa;margin: 0px 10px 0px 10px;"></div><button style="padding:3px 10px;border-bottom:1px dotted;border-radius: 0px" id="show-old-pp-templates" onclick="UA_SAAS_SERVICE_APP.openOldWhatCetraTemplatesPage()" class="ssf-action-btn primary-form-btn hideOnChatView">Old WhatCetra Templates</button>`);
            }
            
//            UA_SAAS_SERVICE_APP.getCurrentOrgInfo(function(response){
//                UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO = response.data.org[0];
//                appsConfig.UA_DESK_ORG_ID = UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO.id;
//                //$("#saasAuthIDName").text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.full_name).attr('title', `Authorized Zoho Account: (${UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.email})`);
//                UA_SAAS_SERVICE_APP.proceedToAppInitializationIfAPPConfigResolved();
//            });
            
//        });
        
//        ZOHO.embeddedApp.init().then();
        
        
    },
    
    initiateAPPFlow_SA: function(){
        UA_SAAS_SERVICE_APP.initiateAPPFlow();
    },

    addSAASUsersToLICUtility: async function(){
        $('.saasServiceName').text("PipeDrive CRM");
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/users`, "GET", null);
        response.data.data.sort(function(a, b) {
            return a.name.localeCompare(b.name);
        });
        response.data.data.forEach(item=>{
            if(item.active_flag){
                UA_LIC_UTILITY.addFetchedSAASUser(item.id, item.name, item.email, null, item.is_admin === 1, item.phone, url = null);
            }
        });
        UA_LIC_UTILITY.saasUserListFetchCompleted();
    },
        
    proceedToAppInitializationIfAPPConfigResolved: function(){
        if(appsConfig.APP_UNIQUE_ID && appsConfig.UA_DESK_ORG_ID){
            UA_SAAS_SERVICE_APP.renderInitialElements();
            UA_SAAS_SERVICE_APP.addSAASUsersToLICUtility();
            UA_APP_UTILITY.appsConfigHasBeenResolved();
            if(UA_SAAS_SERVICE_APP.widgetContext.module) {
                UA_SAAS_SERVICE_APP.renderInitialElementsForPipedriveList();
            }
        }
    },
    
    renderInitialElements: async function(){
        if(!UA_SAAS_SERVICE_APP.widgetContext.module){
            UA_SAAS_SERVICE_APP.entityDetailFetched(null);
            console.log('Module does not exist, rendering skipped');
            $("#recip-count-holder").text("Add recipients to proceed");
            return;
        }
//        UA_APP_UTILITY.renderSavedTemplatesInDropdowns();
        UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(UA_SAAS_SERVICE_APP.widgetContext.module, UA_SAAS_SERVICE_APP.widgetContext.entityId, function(contactList){
            UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
            var actualContactsList = [];
            for(var item in contactList){
                let entityItem = contactList[item];
                if(UA_SAAS_SERVICE_APP.widgetContext.module === "deal"){
                    entityItem.person_id.id = entityItem.id;
                    actualContactsList.push(entityItem.person_id);
                }
                if(UA_SAAS_SERVICE_APP.widgetContext.module === "person"){
                    actualContactsList.push(entityItem);
                }
            }
            UA_SAAS_SERVICE_APP.addRecordsAsRecipients(actualContactsList);
        });
    },

    renderInitialElementsForPipedriveList: async function(){
        $(".ssf-new-recip-form").prepend(`<div id="ssf-recip-list-add-holdr" class="ssf-recip-choice-btn" style="margin-top: -5px;background: none;border: none;box-shadow: none;"></div>`);
        UA_SAAS_SERVICE_APP.renderContactList();
    },
    
    renderContactList: async function(){
        try{
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/v1/filters?type=${UA_SAAS_SERVICE_APP.widgetContext.module==='person'?'people':UA_SAAS_SERVICE_APP.widgetContext.module+'s'}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('renderList',response);
        let listDropdownItems = [];
        if(response && response.data && response.data.data.length > 0){
            response.data.data.forEach(item=>{
                UA_SAAS_SERVICE_APP.fetchedList[item.id] = item;
                listDropdownItems.push({
                    'label': item.name,
                    'value': item.id
                });
            });
        }

        UA_APP_UTILITY.renderSelectableDropdown('#ssf-recip-list-add-holdr', 'Select a filter', listDropdownItems, 'UA_SAAS_SERVICE_APP.insertManageListContacts', false, false);
        }catch(ex){console.log(ex);}
    },

    insertManageListContacts: async function(listId){

        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${UA_SAAS_SERVICE_APP.widgetContext.module}s?filter_id=${listId}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('renderList',response);
        if(response && response.data && response.data.data && response.data.data.length > 0) {
            response.data.data.forEach(item=>{
                UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id] = item;
                // UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(UA_SAAS_SERVICE_APP.widgetContext.module, item.id, UA_SAAS_SERVICE_APP.addRecordsAsRecipients);
                // UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
            });

            UA_SAAS_SERVICE_APP.addRecordsAsRecipients(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
            UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
        }
        else{
            alert(`We can't found records in list or list is empty.`);
        }
            
    },
    
    sendPhoneFieldsForRecipientType: function(fieldsArray){
        fieldsArray.forEach(item=>{
            if(item.data_type === "phone"){
                UA_APP_UTILITY.addRecipientPhoneFieldType(item.api_name, item.display_label);
            }
        });
    },
    
    populateModuleItemFieldsInDropDown: async function (module, target, fieldsCallback=(()=>{})){
        if(!module || UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.includes(module) == true){
            return;
        }
        UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.push(module);
        var moduleToLoad = module === "users" ? UA_SAAS_SERVICE_APP.CURRENT_USER_INFO : UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS)[0]];
        var moduleFields = Object.keys(moduleToLoad);
        var fieldsArray = [];
        var dropDownValues = [];
        moduleFields.forEach(item=>{
            let itemVal = moduleToLoad[item];
            if(typeof itemVal !== "object" && itemVal !== null && typeof itemVal!=="boolean"){
                fieldsArray.push({
                    "api_name": item,
                    "display_label": item
                });
                dropDownValues.push({
                    "label": item,
                    "value": (module.toUpperCase() === "USERS" ? "CURRENT_USER": module.toUpperCase())+'.'+item
                });
            }
        });
        fieldsCallback(fieldsArray);
        if(typeof target === "string"){
            target = [target];
        }
        target.forEach(item=>{
            UA_APP_UTILITY.renderSelectableDropdown(item, `Insert ${module} fields`, dropDownValues, 'UA_APP_UTILITY.templateMergeFieldSelected', false, true);
        });
        return true;
    },
    
    injectShowSearchContact :async function(){
        $(".ssf-new-recip-form").prepend(`<div id="searchAndAddFromContactBtn" class="ssf-recip-choice-btn" onclick="$('.item-list-popup').show()">
                                            <i class="material-icons">search</i> Search from ${UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()}
                                      </div>`);
        $('.pageContentHolder').append(`<div class="item-list-popup" style="display:none">
                        <div class="item-list-popup-title">
                            Select ${UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()} <div class="pop-win-close" onclick="$('.item-list-popup').hide();">x</div>
                        </div>
                        <div class="item-search-box">
                            <input class="input-form" id="contact-seach-name" type="text" autocomplete="off" placeholder="name of the ${UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()}"/><br>
                            <div class="btn-save" onclick="UA_SAAS_SERVICE_APP.searchRecordsAndRender($('#contact-seach-name').val(),UA_SAAS_SERVICE_APP.widgetContext.module)">
                                Search ${UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()}
                            </div>
                            <div class="btn-reset" onclick="UA_SAAS_SERVICE_APP.resetsearchContacts()">
                                Reset
                            </div>
                        </div>
                        <div class="item-list-popup-content" id="contact-search-items">

                        </div>
                    </div>`);
    },

    resetsearchContacts: function(){
        $("#contact-seach-name").val("");
        $("#contact-search-items").html("");
    },

    expandPageToNormalFullView: async function(){
        $("#page-min-max-btn").hide();
        setTimeout(UA_APP_UTILITY.reCalcluateChatBoxSize, 4000);
        await UA_SAAS_SERVICE_APP.DESK_APP.execute(Command.RESIZE, {height: 600});
        setTimeout(UA_APP_UTILITY.reCalcluateChatBoxSize, 4000);
    },
    
    searchRecordsAndRender: async function(searchTerm,module){
        if(!valueExists(searchTerm)){
            return;
        }
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/v1/itemSearch?term=${searchTerm}&item_types=${module}`,"GET",null)
            if(response && response.data && response.data.success)
            {
                console.log(response);
                UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
                $("#contact-search-items").html(" ");
                if(response.data["data"].items.length>0)
                {
                    var search_items = response.data["data"].items;
                    if(search_items && search_items.length > 50){
                        search_items.splice(0,49);
                    }
                    UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module] = {};
                    search_items.forEach((obj)=>{
                        UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][obj.item.id] = obj.item;
                        var fullName = "";
                        if(module)
                        {
                            if(module == "person"){
                                fullName = obj.item.name;
                                fullName = fullName?fullName:"person#"+obj.item.id;
                            }
                            else if(module == "deal" || module == "lead") {
                                fullName = obj.item.title;
                                fullName = fullName?fullName: module+"#"+obj.item.id;
                            }
                        }
                        let itemHTML = `<div class="item-list-popup-item" style="cursor:pointer;" onclick="UA_SAAS_SERVICE_APP.selectPhoneNumber('${obj.item.id}', '${module}', '${fullName}','${module}','${true}')"><span class="c-name"> ${fullName} </span>`;
                        itemHTML =  (obj.item.phones && obj.item.phones.length > 0 )? itemHTML + `${'<span class="c-phone"> <i class="material-icons">phone_iphone</i> phone </span>'}` : itemHTML;
                        itemHTML =  itemHTML + `</div>`;
                        $("#contact-search-items").append(itemHTML);
                    });
                }
                else
                {
                    $("#contact-search-items").html("No records found. <br><br> <span class=\"c-silver\"> This search will include <br> only customers having phone numbers</span>");
                    return;
                }
            }
    },

    selectPhoneNumber: async function(id,number,name,module,isSelected){
        var receipNumberID = id;
        if(!UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] && !UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id]){
            UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] = {
                'id': id,
                'name': name,
                'module': module,
                'isSelected': isSelected
            };
            $.extend(UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id],UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID]) ;
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${module}s/${id}`, "GET", null); 
            if(response && response.data && response.data.data)
            {
                UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[response.data.data.id] = response.data.data;
                UA_SAAS_SERVICE_APP.addRecordsAsRecipients(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
                await UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
            }
        }
        else {
            console.log(module+' already added');
            alert(`This ${module.toLocaleLowerCase()} is already added.`);
            return;
        }
    },
    
    getCurrentUserInfo : async function(callback){
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/v1/users/me', callback);
    },
    
    getUserInfoFromID : async function(userEmail, callback){
//        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/crm/v3/owners?email='+userEmail, callback);
    },
    
    getCurrentOrgInfo : async function(callback){
//        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/org', callback);
    },
    
    getAPIResponseAndCallback: async function(url, callback){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}${url}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        callback(response);
    },
    
    showErrorIfInAPICall : function(response){
        if(response.code === 401){
            UA_OAUTH_PROCESSOR.showReAuthorizationError(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED);
        }
    },
    
    initiateAuthFlow: function(){
        UA_OAUTH_PROCESSOR.initiateAuth(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED);
    },
    
    fetchStoreExecuteOnRecordDetails: async function(module, recordIds, callback){
        if(!module){
            return;
        }
//        if(typeof recordIds !== "object"){
//            var recordId = recordIds;
//            recordIds = [recordId];
//        }
//        recordIds = ["5318123000000377412"];
        var apiURLToFetch = "";
        var filterId = null;
        var modulePluralWord = module.toLowerCase()+"s";
        var selectedIds = null;
        if(queryParams.get('selectedIds') && queryParams.get('selectedIds')!==""){
            if(queryParams.get('selectedIds').split(',').length > 0){
                selectedIds = queryParams.get('selectedIds').split(',');
            }
        }
        if(queryParams.get("filter") && queryParams.get("filter")!=="" && (!selectedIds || selectedIds.length !== 1)){
            var queryParamVal = "";
            filterId = JSON.parse(queryParams.get("filter"));
            for(var item in filterId){
                if(item === "everyone"){
                    if(parseInt(filterId[item]) !== 1){
                        queryParamVal += `&filter_id=${filterId[item]}`;
                    }
                }
                else{
                    queryParamVal+="&"+item+"="+filterId[item];
                }
            }
            $("#recip-count-holder").text(`fetching ${module.toLocaleLowerCase()}s information...`);
            let queryLimit = 500;
            if(selectedIds){
                selectedIds = selectedIds.sort();
                if(selectedIds[0]){
                    if(!queryParamVal || queryParamVal.length === 0){
                        queryParamVal = `&start=${selectedIds[0]-500}`;
                    }
                }
            }
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${modulePluralWord}?limit=${queryLimit}`+(queryParamVal), "GET", null);
            UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
            console.log('fetchStoreExecuteOnRecordDetails',response);
            var excludedIds = [];
            if(queryParams.get(`excludedIds`)){
                excludedIds = queryParams.get(`excludedIds`).split(',');
            }
            try{
                response.data.data.forEach(item => {
                    item.id += ''; 
                    if(excludedIds && excludedIds.indexOf(item.id) > -1){
                        return;
                    }
                    if(selectedIds && selectedIds.length>0){
                       if(selectedIds.indexOf(item.id) === -1){
                            return;
                        } 
                    }
                    UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id] = item;
                });
            }catch (ex){
                $("#recip-count-holder").text(`Unable to fetch ${module.toLocaleLowerCase()}s information`);
            }
            if(Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS).length > 0){
                UA_SAAS_SERVICE_APP.entityDetailFetched();
                callback(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
            }
            else{
                $("#recip-count-holder").text(`Add recipients to proceed`);
            }
        }
        else{
            $("#recip-count-holder").text(`fetching ${module.toLocaleLowerCase()} information...`);
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${modulePluralWord}/${queryParams.get("selectedIds").split(',')[0]}`, "GET", null);
            UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
            console.log('fetchStoreExecuteOnRecordDetails',response);
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[response.data.data.id] = response.data.data;
            callback(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
            UA_SAAS_SERVICE_APP.entityDetailFetched();
        }
        if(Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS).length > 1){
            UA_APP_UTILITY.changeCurrentView('MESSAGE_FORM');
        }
    },
    
     addRecordsAsRecipients: function(contactsList){
        for(var item in contactsList){
            try{
                if($(`[data-recip-id=${contactsList[item]['id']}]`).length == 0) { 
                    ["phone", "mobile"].forEach(mobileFieldItem => {
                        let mobileFieldLabels = {};
                        if (contactsList[item][mobileFieldItem]) {
                            contactsList[item][mobileFieldItem].forEach(mobileFieldItemValueItem => {
                                if(mobileFieldItemValueItem.value && mobileFieldItemValueItem.value!==""){
                                    let fieldLabel = mobileFieldItemValueItem.label ? mobileFieldItemValueItem.label : mobileFieldItem;
                                    let propName = ('prop_'+mobileFieldItem+'_'+fieldLabel).replace(/[\W_]+/g,"_");
                                    
                                    if(!mobileFieldLabels.hasOwnProperty(fieldLabel)){
                                        mobileFieldLabels[fieldLabel] = 1;
                                    }
                                    else{
                                        let currentIndex = ++mobileFieldLabels[fieldLabel];
                                        fieldLabel = fieldLabel+"_"+currentIndex;
                                        propName = propName+"_"+currentIndex;
                                    }
                                    contactsList[item][propName] = mobileFieldItemValueItem.value;
                                    UA_APP_UTILITY.addRecipientPhoneFieldType(propName, fieldLabel);
                                }
                            });
                        }
                    });
                    if(!contactsList[item].name){
                        contactsList[item].name = "";
                        if(contactsList[item].firstname){
                        contactsList[item].name = contactsList[item].firstname;
                        }
                        if(contactsList[item].lastname){
                            contactsList[item].name = contactsList[item].name+" "+ contactsList[item].lastname;
                        }
                    }
                    UA_APP_UTILITY.addInStoredRecipientInventory(contactsList[item]);
                }
            }
            catch(ex){
                console.log(ex);
            }
        }
    },
    
    addSentSMSAsRecordInHistory: async function(sentMessages){
        for(var sentMessageId in sentMessages){
            let commentContent = `<u><i>${appPrettyName} Extension</i></u> - `;
            let sentMessage = sentMessages[sentMessageId];
            commentContent+=`<b>${sentMessage.channel} ${sentMessage.status} from ${sentMessage.from} to ${sentMessage.to}</b>:<br>
                             ${sentMessage.message}<br>
                                `;
            let credAdProcess3 = curId++;
            showProcess(`Adding message to notes...`, credAdProcess3);
            let notesObj = {
                'content': commentContent,
                'user_id': UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.id
            };
            notesObj[UA_SAAS_SERVICE_APP.widgetContext.module+'_id'] = valueExists(sentMessage.moduleId)?(sentMessage.moduleId.indexOf("newmanual")!==0?sentMessage.moduleId:UA_SAAS_SERVICE_APP.widgetContext.entityId):UA_SAAS_SERVICE_APP.widgetContext.entityId;
            let response = UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/notes`, "POST", notesObj).then(response => {
                processCompleted(credAdProcess3);
                return true;
                //UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
                //console.log('addSentSMSAsRecordInHistory',response);
            });
        }
        
    },
    entityDetailFetched: function(data){
        setTimeout(()=>{UA_APP_UTILITY.fetchedAllModuleRecords();}, 2000);
        if(typeof UA_TPA_FEATURES.callOnEntityDetailFetched === "function"){
            UA_TPA_FEATURES.callOnEntityDetailFetched(data);
        }
    },
    changedCurrentView: async function(){
        if (queryParams.get("openMode") === "modal") {
            if(UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CHAT){
                await UA_SAAS_SERVICE_APP.DESK_APP.execute(Command.RESIZE, {height: 575, width: 770});
            }
            if(UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CONVERSATIONS){
                await UA_SAAS_SERVICE_APP.DESK_APP.execute(Command.RESIZE, {height: 600, width: 1300});
            }
            if(UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.MESSAGE_FORM){
                await UA_SAAS_SERVICE_APP.DESK_APP.execute(Command.RESIZE, {height: 600, width: 850});
            }
        }
    },
    insertIncomingConfigItems: function(){
        $('#incoming-module-config-item').append(`
            <div class="tpaichan-item" style="display:none">
                <label class="switch">
                    <input disabled readonly checked onchange="UA_TPA_FEATURES.updateIncomingActivitiesToggle()" type="checkbox" id="tpa-switch-incoming-enable-capture">
                    <div class="slider round"></div>
                </label> Create persons if not exists for new ${appPrettyName} <span class="incoming-config-tpa-entity"></span>
            </div>`);
    },
    
    searchRecord: async function(module, query) {
        return await ZOHO.CRM.API.searchRecord({Entity:module,Type:"criteria",Query:query,delay:"false"}).then(function(response) {
            return response.data;
        });
    },
    LOOKUP_SUPPORTED_MODULES: {
        "persons": {
            "displayLabel": "Contacts",
            "apiName": "person",
            "autoLookup": true,
            "relatedModules": ["deals"]
        },
        "organizations": {
            "apiName": "organization",
            "relatedModules": ["deals"],
            "hideDisplay": true
        },
        "deals": {
            "apiName": "deal",
            "displayLabel": "Deals",
            "hideDisplay": true
        }
    },
    allModuleFieldsFetched: function(){
        let fieldsPresentModuleCount = 0;
        Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).forEach(item=>{
            if(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[item].fields && Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[item].fields).length > 0){
                fieldsPresentModuleCount++;
            }
        });
        return Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).length === fieldsPresentModuleCount;
    },
    retrieveModuleFieldsForLookUp: function(callback){
        $('#ua-loo-search-field-ddholder').hide();
        if (UA_SAAS_SERVICE_APP.allModuleFieldsFetched()) {
            callback();
        }
        let primaryFieldsToCreate = ["name", "phone"];
        let primaryFieldsToViewForModule = [];
        Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).forEach(async moduleId=>{
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${moduleId.substring(0, moduleId.length-1)}Fields`, "GET", null);
            let fieldAPINameMap = {};
            response.data.data.forEach(fieldItem=>{
                try{
                    primaryFieldsToCreate = [];
                    primaryFieldsToViewForModule = [];
                    if(["ownerlookup", "lookup"].includes(fieldItem.data_type)){
                        fieldItem.field_read_only = true;
                    }
                    let isSystemMandatory = fieldItem.add_visible_flag && fieldItem.mandatory_flag && (!["org", "people", "user"].includes(fieldItem.field_type));
                    if(["varchar", "varchar_auto", "time", "monetary", "double"].includes(fieldItem.field_type)){
                        fieldItem.field_type = "text";
                    }
                    if(fieldItem.field_type === "time"){
                        fieldItem.field_type = "datetime";
                    }
                    if(fieldItem.field_type === "date"){
                        fieldItem.field_type = "datetime";
                    }
                    if(fieldItem.field_type === "address"){
                        fieldItem.field_type = "textarea";
                    }
                    if(fieldItem.field_type === "set" || fieldItem.field_type === "enum"){
                        fieldItem.field_type = "picklist";
                        fieldItem.pick_list_values = [];
                        if(!fieldItem.options){
                            return true;
                        }
                        fieldItem.options.forEach(optItem=>{
                            fieldItem.pick_list_values.push({
                                'actual_value': optItem.id,
                                'display_value': optItem.label
                            });
                        });
                    }
                    if(fieldItem.field_type === "stage"){
                        fieldItem.field_type = "picklist";  
                        fieldItem.pick_list_values = [];
                    }
                    if(moduleId === "deals" && fieldItem.key === "pipeline"){
                        fieldItem.field_type = "picklist";  
                        fieldItem.pick_list_values = [];
                    }
                    if(moduleId === "deals" && fieldItem.key === "status"){
                        fieldItem.field_type = "picklist";
                        fieldItem.pick_list_values = [
                            {
                                'actual_value': "open",
                                'display_value': "Open"
                            },
                            {
                                'actual_value': "won",
                                'display_value': "Won"
                            },
                            {
                                'actual_value': "lost",
                                'display_value': "Lost"
                            },
                            {
                                'actual_value': "deleted",
                                'display_value': "Deleted"
                            }
                        ];
                    }
                    if(moduleId === "deals" && fieldItem.key === "title"){
                        isSystemMandatory = true;
                    }
                    if(["persons", "orgranizations"].includes(moduleId) && fieldItem.key === "name"){
                        isSystemMandatory = true;
                    }
                    fieldAPINameMap[fieldItem.key] = {
                        'display_label': fieldItem.name,
                        'data_type': fieldItem.field_type,
                        'api_name': fieldItem.key,
                        'field_read_only': !fieldItem.bulk_edit_allowed,
                        'system_mandatory': isSystemMandatory,
                        'pick_list_values' : fieldItem.pick_list_values
                    };
                    if(fieldItem.add_visible_flag || fieldItem.mandatory_flag){
                        primaryFieldsToCreate.push(fieldItem.name);
                    }
                    if(fieldItem.details_visible_flag || fieldItem.important_flag){
                        primaryFieldsToViewForModule.push(fieldItem.name);
                    }
                }
                catch(ex){
                    console.log(ex);
                }
            });
            fieldAPINameMap.id = {
              'display_label': 'ID',
              'data_type': 'number',
              'api_name': 'id',
              'field_read_only': true
            };
            if(moduleId === "deals"){
                primaryFieldsToCreate.push('title');
                primaryFieldsToCreate.push('status');
                primaryFieldsToCreate.push('stage');
            }
            else{
                primaryFieldsToCreate.push('name');
            }
            UA_TPA_FEATURES.setCreateModuleRecordFields(moduleId, primaryFieldsToCreate);
            UA_TPA_FEATURES.setViewModuleRecordFields(moduleId, primaryFieldsToViewForModule);
            UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[moduleId].fields = fieldAPINameMap;
            if(UA_SAAS_SERVICE_APP.allModuleFieldsFetched()){
                callback();
            }
        });
    },
    callbackOnMaximizeModuleRecordItem: function(module, recordId){
        let moduleAllFieldsFetched = (UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordId].hasOwnProperty('add_time'));
        if(!moduleAllFieldsFetched){
            UA_SAAS_SERVICE_APP.refreshRecordItem(module, recordId);
        }
    },
    getSearchedResultItems: async function(searchTerm, module, field="phone"){
        UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS = {};
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/itemSearch?term=${searchTerm}&item_types=${module.substring(0, module.length-1)}&fields=${field.toLocaleLowerCase()}`,"GET", {});
        if(response.status === 204){
            response.data = {
                'data': []
            };
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS = {};
        }
        let itemsArray = [];
        if(response && response.data && response.data.data && response.data.data.items && response.data.data.items.length > 0){
            response.data.data.items.forEach(item=>{
                item.item.minimizeOnDefault = true;
                itemsArray.push(item.item);
            });
            itemsArray = itemsArray.sort((a, b) => {
                try{
                    return a.id > b.id ? 1 : -1;
                }
                catch(ex){console.log(ex);}
            });
            itemsArray[0].minimizeOnDefault = false;
            itemsArray.forEach(item=>{
                item.id +='';
                item.phone = item.phones ? item.phones.join(',') : '';
                item.email = item.emails ? item.emails.join(',') : '';
                item.webURL = "https://"+UA_OAUTH_PROCESSOR.currentSAASDomain+"/"+module.substring(0, module.length-1)+'/'+item.id;
                item.owner_name = (item.owner && item.owner.id && UA_LIC_UTILITY.SAAS_FETCHED_USERS_IDS_LIST[item.owner.id]) ? UA_LIC_UTILITY.SAAS_FETCHED_USERS_IDS_LIST[item.owner.id].name :"No Owner";
                UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id] = item;
            });
            if(UA_SAAS_SERVICE_APP.refreshRecordItem) UA_SAAS_SERVICE_APP.refreshRecordItem(module, itemsArray[0].id);
        }
        return response.data.data;
    },
    
    getRecordByID: async function(module, entityId){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${module}/${entityId}`, "GET", null);
        return response.data.data;
    },
    
    getNotesOfRecord: async function(module, entityId){
        if(entityId.indexOf("_") > 0){
            entityId = entityId.split('_')[1];
        }
        if(module === "organization"){
            module = "orgs";
        }
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/notes?${module.substring(0, module.length-1)}_id=${entityId}&limit=25&sort=add_time%20DESC`, "GET", null);
            let notesArray = [];
            response.data.data.forEach(noteItem=>{
                notesArray.push({
                    'id': noteItem.id,
                    'content': noteItem.content,
                    'title': noteItem.user.name+":",
                    'time': noteItem.update_time
                });
            });
            return notesArray;
    },
    
    addModuleRecordNote: async function(module, entityId, noteContent){
        if(entityId.indexOf("_") > 0){
            entityId = entityId.split('_')[1];
        }
        if(module === "organization"){
            module = "orgs";
        }
        var noteObj = {
            'content': noteContent
        };
        noteObj[module.substring(0, module.length-1)+'_id'] = entityId;
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/notes`, "POST", noteObj);
        if(response.data.success){
            return response.data;
        }
        else{
            return {
                error: true
            };
        }
    },
    
    refreshRecordItem: async function(module, entityId){
        if(entityId.indexOf("_") > 0){
            entityId = entityId.split('_')[1];
        }
        var recordData = await UA_SAAS_SERVICE_APP.getRecordByID(module, entityId);
        if(recordData){
            recordData.id +='';
            recordData.phones = [];
            recordData.emails = [];
            recordData.phone.forEach(item=>{
                recordData.phones.push(item.value);
            });
            recordData.email.forEach(item=>{
                recordData.emails.push(item.value);
            });
            recordData.name = recordData.name || recordData.title;
            recordData.phone = recordData.phones ? recordData.phones.join(',') : '';
            recordData.email = recordData.emails ? recordData.emails.join(',') : '';
            recordData.webURL = "https://"+UA_OAUTH_PROCESSOR.currentSAASDomain+"/"+module.substring(0, module.length-1)+'/'+recordData.id;
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[entityId] = recordData;
        }
        if(UA_TPA_FEATURES.refreshRecordItemInUI) UA_TPA_FEATURES.refreshRecordItemInUI(entityId);
        if(UA_TPA_FEATURES.fetchAndRenderRecordRelatedModule && UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[module].relatedModules){
            UA_TPA_FEATURES.fetchAndRenderRecordRelatedModule(module, UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[module].relatedModules[0], entityId);
        }
    },
    
    getAndStoreExtraDropdownFieldDetails: async function(){
        UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/stages`, "GET", null).then(response=>{
            let responseData = response.data.data;
            let ddValues = [];
            responseData.forEach(item=>{
                ddValues.push({
                    "display_value": item.name,
                    "actual_value": item.id
                });
            });
            UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES.deals.fields.stage_id.pick_list_values = ddValues;
        });
        UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/pipelines`, "GET", null).then(response=>{
            let responseData = response.data.data;
            let ddValues = [];
            responseData.forEach(item=>{
                ddValues.push({
                    "display_value": item.name,
                    "actual_value": item.id
                });
            });
            UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES.deals.fields.pipeline.pick_list_values = ddValues;
        });
    },
    
    updateRecordByField: async function(module, entityId, fieldName, fieldValue){
        if(entityId.indexOf("_") > 0){
            entityId = entityId.split('_')[1];
        }
        var createObj = {};
        createObj[fieldName] = fieldValue;
        if(fieldName === "email"){
            let givenEmails = fieldValue.split(',');
            createObj.email = [];
            givenEmails.forEach(item=>{
                createObj.email.push({
                    'value': item
                });
            });
        }
        if(fieldName === "phone"){
            let givenPhones = fieldValue.split(',');
            createObj.phone = [];
            givenPhones.forEach(item=>{
                createObj.phone.push({
                    'label': 'whatsapp',
                    'value': item
                });
            });
        }
        createObj[fieldName] = fieldValue;
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl(3)}/${module}/${entityId}`, "PUT", createObj);
        if(response.data.data){
            return response;
        }
        else{
            return {
                error: {
                    'message': 'Error, Try again'
                }
            };
        }
    },
    
    createRecordInModule: async function(module, fieldMap, parentRecordId = null){
        let parentModule = null;
        if(parentRecordId !== null && UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[parentRecordId]){
            parentModule = UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[parentRecordId].__RecordModuleID;
        }
        var createObj = fieldMap;
        if(createObj.hasOwnProperty('email')){
            let givenEmails = createObj.email.split(',');
            createObj.email = [];
            givenEmails.forEach(item=>{
                createObj.email.push({
                    'value': item
                });
            });
        }
        if(createObj.hasOwnProperty('phone')){
            let givenPhones = createObj.phone.split(',');
            createObj.phone = [];
            givenPhones.forEach(item=>{
                createObj.phone.push({
                    'label': 'whatsapp',
                    'value': item
                });
            });
        }
        if(parentModule){
            if(module === "organization"){
                parentModule = "orgs";
            }
            createObj[parentModule.substring(0, parentModule.length-1)+'_id'] = parentRecordId;
        }
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${module}`, "POST", createObj);
        if(response.data.data){
            let recordItem = response.data.data;
            recordItem.id+='';
            recordItem.phones = [];
            recordItem.emails = [];
            if(recordItem.phone){
                recordItem.phone.forEach(item=>{
                    recordItem.phones.push(item.value);
                });
            }
            if(recordItem.email){
                recordItem.email.forEach(item=>{
                    recordItem.emails.push(item.value);
                });
            }
            recordItem.name = recordItem.name || recordItem.title;
            recordItem.phone = recordItem.phones ? recordItem.phones.join(',') : '';
            recordItem.email = recordItem.emails ? recordItem.emails.join(',') : '';
            
            if(parentModule){
                recordItem.__RelatedRecordOriginalID = recordItem.id;
                recordItem.id = module+"_"+recordItem.id;
                recordItem.__isRelatedModuleItem = true;
                recordItem.__RelatedModuleID = parentModule;
                recordItem.__RelatedRecordID = parentRecordId;
                recordItem.__RecordModuleID = module;
            }
            recordItem.minimizeOnDefault = false;
            if(module === "deals"){
                recordItem.__labels = [{
                    'label': recordItem.status,
                    'color': UA_SAAS_SERVICE_APP.dealLabelColorMap[recordItem.status] ? UA_SAAS_SERVICE_APP.dealLabelColorMap[recordItem.status]  : null
                }];
            }
            recordItem.webURL = "https://"+UA_OAUTH_PROCESSOR.currentSAASDomain+"/"+module.substring(0, module.length-1)+'/'+recordItem.__RelatedRecordOriginalID;
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordItem.id] = recordItem;
            return recordItem;
        }
        else{
            return {
                error: {
                    'message': 'Error, Try again'
                }
            };
        }
    },
    dealLabelColorMap: {
        "won": "green",
        "lost": "red"
    },
    fetchRecordRelatedModuleItems: async function(module, relatedModule, recordId){
        UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordId].__relatedModule = {};
        UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordId].__relatedModule[relatedModule] = {};
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${module}/${recordId}/${relatedModule}?sort=update_time%20DESC&limit=25`,"GET", {});
        if(response.status === 204){
            response.data = {
                'data': []
            };
        }
        let itemsArray = [];
        response.data.data.forEach(item=>{
            itemsArray.push(item);
        });
        itemsArray = itemsArray.sort((a, b) => {
            try{
                return a.id > b.id ? 1 : -1;
            }
            catch(ex){console.log(ex);}
        });
        itemsArray.forEach(item=>{
            item.__RelatedRecordOriginalID = item.id;
            item.id = relatedModule+"_"+item.id;
            item.name = item.name || item.title;
            item.__RelatedModuleID = module;
            item.__isRelatedModuleItem = true;
            item.__RelatedRecordID = recordId;
            item.__RecordModuleID = relatedModule;
            item.minimizeOnDefault = false;
            item.__labels = [{
                'label': item.status,
                'color': UA_SAAS_SERVICE_APP.dealLabelColorMap[item.status] ? UA_SAAS_SERVICE_APP.dealLabelColorMap[item.status]  : null
            }];
            item.webURL = "https://"+UA_OAUTH_PROCESSOR.currentSAASDomain+"/"+relatedModule.substring(0, relatedModule.length-1)+'/'+item.__RelatedRecordOriginalID;
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordId].__relatedModule[relatedModule][item.__RelatedRecordOriginalID] = item;
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id] = item;
        });
        return itemsArray;
    },
    
    getNewRecordInSAASWebURL: function(module){
        return "https://"+UA_OAUTH_PROCESSOR.currentSAASDomain+"/"+module;
    },

    searchRelatedRecordsToAllModules: async function(searchTerm, field="phone"){
        try{
            var searchData = [];
            if(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES && Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).length > 0){
                for(var i=0; i < Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).length; i++){
                    let module = Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES)[i];
                    let data = await UA_SAAS_SERVICE_APP.getSearchedResultItems(searchTerm, module, field);
                    if(data && data.items && data.items.length > 0){
                        for(var j=0; j < Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS).length; j++){
                            let id = Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS)[j];                        
                            let item = UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id]
                            searchData.push({module: module, record: item});
                        }
                    }
                }
            }
            return searchData;
        }
        catch(e){ console.log(e); return []; }
    },

    openOldWhatCetraTemplatesPage: function(){
        window.open(`https://app.whatcetra.com/pipedrive/whatcetra/template?resource=${queryParams.get('resource')}&userId=${queryParams.get('userId')}&companyId=${queryParams.get('companyId')}`, '_blank');
    }
};

window.addEventListener('load', async function(){
    try{
        if (AppExtensionsSDK) {
            UA_SAAS_SERVICE_APP.DESK_APP = await new AppExtensionsSDK().initialize();
            if (queryParams.get("openMode") !== "modal") {
                await UA_SAAS_SERVICE_APP.DESK_APP.execute(Command.RESIZE, {height: 100, width: 800});
                $('body').append(`<div id="page-min-max-btn" onclick="UA_SAAS_SERVICE_APP.expandPageToNormalFullView()">Click to expand <i class="material-icons">aspect_ratio</i></div>`);
            }
            else{
                await UA_SAAS_SERVICE_APP.DESK_APP.execute(Command.RESIZE, {height: 600, width: 800});
            }
        }
    }catch(ex){ console.log(ex); }
});