const UA_OAUTH_PROCESSOR = {
    
    currentSAASDomain: null,
    
    initiateAuth: function (scope) {
        if(!extensionName){
            showErroWindow('Application error!', 'Unable to load app type. Kindly <a target="_blank" href="https://apps.ulgebra.com/contact">contact developer</a>');
            return;
        }
        if(extensionName.endsWith('zendesksell')){
            window.open(`https://api.getbase.com/oauth2/authorize?response_type=code&client_id=a6dbc8c36f1d1e5474426ff3b7701d6a213211dddf1531bff30a159153e8906f&redirect_uri=https%3A%2F%2Fsms.ulgebra.com%2Foauth-home.html&state=${currentUser.uid}:::${extensionName}`, 'Authorize', 'popup');
        }
        if(extensionName.endsWith('zendesksupport')){
            if(!valueExists($('#inp_saas_api_domain').val())){
                showErroWindow('Zendesk Domain is Empty!', 'Please enter your zendesk support domain');
                return false;
            }
            let subdomain = null;
            let fullDomainValue = $('#inp_saas_api_domain').val().trim();
            if(fullDomainValue.startsWith('https://')){
                fullDomainValue = fullDomainValue.replace('https://', '');
            }
        subdomain = fullDomainValue.split('.')[0];
            window.open(`https://${subdomain}.zendesk.com/oauth/authorizations/new?response_type=code&client_id=zdg-ulgebra_zendesk_integrations&redirect_uri=https%3A%2F%2Fus-central1-ulgebra-license.cloudfunctions.net%2FoauthCallbackZendesk&state=${currentUser.uid}:::${extensionName}:::${subdomain}&scope=${encodeURIComponent(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED.join(' '))}`, 'Authorize', 'popup');
        }
    },
    
    getAPIResponse: async function (url, method, data) {
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var returnResponse = await UA_APP_UTILITY.makeUAServiceAuthorizedHTTPCall(url, method, data, {"Authorization": "Basic {{AUTHTOKEN}}", "Content-Type": "application/json"});
        removeTopProgressBar(credAdProcess3);
        return returnResponse;
    },
    
    proceedToSaveSaaSAPIkey : async function(){
        let authtoken = $("#inp_saas_api_key").val();
        if(!valueExists(authtoken)){
            showErroMessage("Please fill API Key");
            return;
        }
        await UA_APP_UTILITY.saveAPIKeyInCredentials('saas', 'authtoken', authtoken, UA_APP_UTILITY.reloadWindow);
    },
    
    showReAuthorizationError: function(serviceName = "Zendesk Sell", getAPIKeyHelpTip){
        if(extensionName.endsWith('zendesksell')){
            if($('#inp_saas_api_key').length > 0){
                console.log('SaaS auth window already showing.. skipping.');
                return;
            }
            showErroWindow("Authentication needed!", `You need to authorize your ${serviceName} Account to proceed. <br><br> <input id="inp_saas_api_key" style="display:none"/> <div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
                <button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_OAUTH_PROCESSOR.initiateAuth()">Authorize Now</button>
            </div>`, true, false, 1000);
            $('#inp_saas_api_key').focus();
        }
        if(extensionName.endsWith('zendesksupport')){
            if($('#inp_saas_api_domain').length > 0){
                console.log('SaaS auth window already showing.. skipping.');
                return;
            }
            showErroWindow("Authentication needed!", `You need to authorize your ${serviceName} Account to proceed. <br><br> <div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
            <div style="font-size: 18px;margin-bottom: 5px;margin-top: 20px;">${serviceName} Domain</div>
                <input id="inp_saas_api_domain" value="${UA_OAUTH_PROCESSOR.currentSAASDomain ? UA_OAUTH_PROCESSOR.currentSAASDomain : '' }" type="text" placeholder="eg: mycompany.zendesk.com" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
                <br>
                <button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_OAUTH_PROCESSOR.initiateAuth()">Authorize Now</button>
            </div>`, true, false, 1000);
            $('#inp_saas_api_key').focus();
        }
    }
    
};