var UA_SAAS_SERVICE_APP = {
    
    CURRENT_USER_INFO : null,
    CURRENT_ORG_INFO : null,
    CURRENT_OAUTH_SCOPE_NEEDED : ["Desk.basic.ALL","Desk.extensions.ALL","Desk.settings.ALL","Desk.search.READ","Desk.contacts.ALL","Desk.tickets.ALL", "Desk.events.ALL", "Desk.articles.READ"],
    FETCHED_RECORD_DETAILS: {},
    CURRENT_MODULE_FIELDS_DROPDOWN : [],
    SELECTED_RECEIPS: {},
    SEARCH_MODULE_RECORD_MAP: {},
    fetchedList : {},
    widgetContext: {},
    appsConfig: {},
    APP_API_NAME: "pinnaclesms__",
    isModelOpened: false,
    DESK_APP: null,
    FETCHED_DEPARTMENTS: {},
    CURRENT_DEPARTMENT_ID: null,
    supportedIncomingModules: [
        {
            "selected": true,
            "value": "tickets",
            "label": "tickets"
        }
    ],
    getAPIBaseUrl : function(){
        return `https://desk.zoho${UA_OAUTH_PROCESSOR.currentSAASDomain}/api/v1`;
    },
    
    provideSuggestedCurrentEnvLoginEmailID: async function(){
            await ZOHODESK.get('user').then(function (response) {
                if(response && response.user && response.user.email) {
                    UA_APP_UTILITY.receivedSuggestedCurrentEnvLoginEmailID(response['user'].email);
                }
            }).catch(function (err) {
                console.log(err);
            });
    },
    
    initiateAPPFlow: async function(){
        if (queryParams.get("openMode") === "modal") {
            ZOHODESK.invoke('RESIZE', {width: '60%', height: '80%'});
        }
        UA_SAAS_SERVICE_APP.injectShowSearchContact();
        UA_SAAS_SERVICE_APP.initiateAPPFlow_SA();
    },

    fetchAndStoreSuggestedConversationForCurrentEntity: async function(){
        if(!extensionName.startsWith("twilio") && !extensionName.startsWith("messagebird") && !extensionName.startsWith("ringcentral")){
            return false;
        }
        if(UA_SAAS_SERVICE_APP.widgetContext.module === "ticket" && UA_SAAS_SERVICE_APP.widgetContext.entityId){
            let currentTicketSAASTpaMap = await UA_APP_UTILITY.getServiceSAASTPAConversationMapIfExists("ZOHODESK", "tickets", "saas_ticket_"+UA_SAAS_SERVICE_APP.widgetContext.entityId);
            if(currentTicketSAASTpaMap){
                UA_APP_UTILITY.suggestConversationSAASTPAMapFound(currentTicketSAASTpaMap);
            }
        }
    },

    initiateAPPFlow_SA: async function(){
        if(!UA_SAAS_SERVICE_APP.widgetContext.module && queryParams.get("module")){
            UA_SAAS_SERVICE_APP.widgetContext.module = queryParams.get("module").endsWith('s') ? queryParams.get("module").substr(0, queryParams.get("module").length-1) : queryParams.get("module");
            UA_SAAS_SERVICE_APP.widgetContext.entityId = queryParams.get("entityId");
            UA_SAAS_SERVICE_APP.injectShowSearchContact(); 
        }
        $("#ac_name_label_saas .anl_servicename").text('Zoho Desk');
        this.insertIncomingConfigItems();
        UA_SAAS_SERVICE_APP.getCurrentUserInfo(function(response){
            if(extensionName.startsWith('lookupfor') || queryParams.get("disableSAASSdk")){
                if(response.api_domain){
                    let domainSplits = response.api_domain.split('.');
                    let apiDomainDC = domainSplits[domainSplits.length - 1];
                    if (apiDomainDC === "cn") {
                        apiDomainDC = "com.cn";
                    }
                    if (apiDomainDC === "au") {
                        apiDomainDC = "com.au";
                    }
                    UA_OAUTH_PROCESSOR.currentSAASDomain = "."+apiDomainDC;
                }
            }
            UA_SAAS_SERVICE_APP.CURRENT_USER_INFO = response.data;
            appsConfig.APP_UNIQUE_ID = UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.id;
            $("#saasAuthIDName").text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.name).attr('title', `Authorized Zoho Account: (${UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.emailId})`);
            setTimeout((function(){
                $('#ac_name_label_saas .ac_name_id').text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.emailId);
            }), 2000);
            UA_SAAS_SERVICE_APP.proceedToAppInitializationIfAPPConfigResolved();
        });
        UA_SAAS_SERVICE_APP.getCurrentOrgInfo(function(response){
            UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO = response.data.data[0];
            appsConfig.UA_DESK_ORG_ID = UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO.id+"";
            //$("#saasAuthIDName").text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.full_name).attr('title', `Authorized Zoho Account: (${UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.email})`);
            UA_SAAS_SERVICE_APP.proceedToAppInitializationIfAPPConfigResolved();
        });
    },
    
    addSAASUsersToLICUtility: async function(){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/agents?status=ACTIVE&limit=200`, "GET", null);
        response.data.data.sort(function(a, b) {
            return a.name.localeCompare(b.name);
        });
        response.data.data.forEach(item=>{
            UA_LIC_UTILITY.addFetchedSAASUser(item.id, item.name, item.emailId, item.photoURL, item.rolePermissionType === "Admin", item.phone = null, null);
        });
        UA_LIC_UTILITY.saasUserListFetchCompleted();
    },
    
    proceedToAppInitializationIfAPPConfigResolved: function(){
        if(appsConfig.APP_UNIQUE_ID && appsConfig.UA_DESK_ORG_ID){
            UA_SAAS_SERVICE_APP.fetchAndStoreSuggestedConversationForCurrentEntity();
            UA_SAAS_SERVICE_APP.renderInitialElements();
            UA_SAAS_SERVICE_APP.addSAASUsersToLICUtility();
            UA_APP_UTILITY.appsConfigHasBeenResolved();
            if(UA_SAAS_SERVICE_APP.widgetContext.module)
            {
                UA_SAAS_SERVICE_APP.renderInitialElementsForZohoDeskList();
            }
            this.fetchAndStoreDefaultDepartments(function(response){
                response.data.data.forEach(item=>{
                    UA_SAAS_SERVICE_APP.FETCHED_DEPARTMENTS[item.id] = item;
                });
                UA_SAAS_SERVICE_APP.renderDefaultDepartmentsInIncomingSelect();
            });
            if(isUASMSApp){
                UA_SAAS_SERVICE_APP.addNewWebhook();
            }
        }
    },
    
    renderInitialElements: async function(){
        UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown('users', ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder']);
//        UA_APP_UTILITY.renderSavedTemplatesInDropdowns();
        // if(UA_SAAS_SERVICE_APP.widgetContext.module !== "ticket" && $("#inp_chk_create_new_ticket_on_msg").length < 1){
        //     //$('.ssf-win-actions-holder').prepend(`<div style="padding: 0px 0px 10px 0px;"><input id="inp_chk_create_new_ticket_on_msg" name="inp_chk_create_new_ticket_on_msg" type="checkbox"> <label for="inp_chk_create_new_ticket_on_msg">Create a new ticket </label></div>`);
        // }
        if(!UA_SAAS_SERVICE_APP.widgetContext.module){
            UA_SAAS_SERVICE_APP.entityDetailFetched(null);
            console.log('Module does not exist, rendering skipped');
            $("#recip-count-holder").text("Add recipients to proceed");
            return;
        }
        UA_SAAS_SERVICE_APP.addSuggestedArtcilesAsTemplateDropdown();
        await UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
        UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(UA_SAAS_SERVICE_APP.widgetContext.module, UA_SAAS_SERVICE_APP.widgetContext.entityId, UA_SAAS_SERVICE_APP.addRecordsAsRecipients);
    },

    renderInitialElementsForZohoDeskList: async function(){
        $(".ssf-new-recip-form").prepend(`<div id="ssf-recip-list-add-holdr" class="ssf-recip-choice-btn" style="margin-top: -5px;background: none;border: none;box-shadow: none;"></div>`);
        UA_SAAS_SERVICE_APP.renderContactList();
    },
    
    renderContactList: async function(){
        // let departmentId = await ZOHODESK.get('department.id');
        // departmentId = departmentId['department.id'];
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/views?module=${UA_SAAS_SERVICE_APP.widgetContext.module}s`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('renderList',response);
        let listDropdownItems = [];
        if(response && response.data && response.data.data.length > 0){
            response.data.data.forEach(item=>{
                UA_SAAS_SERVICE_APP.fetchedList[item.id] = item;
                listDropdownItems.push({
                    'label': item.name,
                    'value': item.id
                });
            });
        }

        UA_APP_UTILITY.renderSelectableDropdown('#ssf-recip-list-add-holdr', 'Select a filter', listDropdownItems, 'UA_SAAS_SERVICE_APP.insertManageListContacts', false, false);
    
    },

    insertManageListContacts: async function(listId){

        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${UA_SAAS_SERVICE_APP.widgetContext.module}s?viewId=${listId}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('renderList',response);
        if(response && response.data && response.data.data && response.data.data.length > 0) {
            response.data.data.forEach(item=>{
                UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id] = item;
                UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(UA_SAAS_SERVICE_APP.widgetContext.module, item.id, UA_SAAS_SERVICE_APP.addRecordsAsRecipients);
                UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
            });
        }
        else{
            alert(`We can't found records in list or list is empty.`);
        }
            
    },
    
    sendPhoneFieldsForRecipientType: function(fieldsArray){
        fieldsArray.forEach(item=>{
            if(item.display_label === "phone" || item.display_label === "mobile"){
                UA_APP_UTILITY.addRecipientPhoneFieldType(item.api_name, item.display_label);
            }
        });
    },
    
    populateModuleItemFieldsInDropDown: async function (module, target, fieldsCallback=(()=>{})){
        /*var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.API_BASE_URL}/settings/fields?module=${module}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log(response);
        response.data.fields.forEach(item=>{
            dropDownValues.push({
                "label": item.display_label,
                "value": (module.toUpperCase() === "USERS" ? "CURRENT_USER": module.toUpperCase())+'.'+item.api_name
            });
        }); */
        try{
        if(!module || UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.includes(module) == true){
            return;
        }
        UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.push(module);
        var moduleToLoad = module === "users" ? UA_SAAS_SERVICE_APP.CURRENT_USER_INFO : UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[UA_SAAS_SERVICE_APP.widgetContext.entityId?UA_SAAS_SERVICE_APP.widgetContext.entityId:Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS)[0]];
        var moduleFields = Object.keys(moduleToLoad);
        var fieldsArray = [];
        var dropDownValues = [];
        moduleFields.forEach(item=>{
            let itemVal = moduleToLoad[item];
            if(typeof itemVal !== "object" && itemVal !== null && typeof itemVal!=="boolean"){
                fieldsArray.push({
                    "api_name": item,
                    "display_label": item
                });
                dropDownValues.push({
                    "label": item,
                    "value": (module.toUpperCase() === "USERS" ? "CURRENT_USER": module.toUpperCase())+'.'+item
                });
            }
        });
        fieldsCallback(fieldsArray);
        if(typeof target === "string"){
            target = [target];
        }
        target.forEach(item=>{
            UA_APP_UTILITY.renderSelectableDropdown(item, `${module} fields`, dropDownValues, 'UA_APP_UTILITY.templateMergeFieldSelected', false, true);
        });
        return true;
    }catch(e){ console.log(e); return false}
    },
    
    injectShowSearchContact :async function(){
        // UA_SAAS_SERVICE_APP.widgetContext.module = UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module:"contact";
        $(".ssf-new-recip-form").prepend(`<div id="searchAndAddFromContactBtn" class="ssf-recip-choice-btn" onclick="$('.item-list-popup').show()">
                                            <i class="material-icons">search</i> Search from ${UA_SAAS_SERVICE_APP.widgetContext.module ? UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase():"contact"}
                                      </div>`);
        $('.pageContentHolder').append(`<div class="item-list-popup" style="display:none">
                        <div class="item-list-popup-title">
                            Select a ${UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase():"contact"} <div class="pop-win-close" onclick="$('.item-list-popup').hide();">x</div>
                        </div>
                        <div class="item-search-box">
                            <input class="input-form" id="contact-seach-name" type="text" autocomplete="off" placeholder="name of the ${UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase():"contact"}"/><br>
                            <div class="btn-save" onclick="UA_SAAS_SERVICE_APP.searchRecordsAndRender($('#contact-seach-name').val(),UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase():'contact')">
                                Search ${UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase():"contact"}
                            </div>
                            <div class="btn-reset" onclick="UA_SAAS_SERVICE_APP.resetsearchContacts()">
                                Reset
                            </div>
                        </div>
                        <div class="item-list-popup-content" id="contact-search-items">

                        </div>
                    </div>`);
    },

    resetsearchContacts: function(){
        $("#contact-seach-name").val("");
        $("#contact-search-items").html("");
    },

    searchRecordsAndRender: async function(searchTerm,module){
        if(!valueExists(searchTerm)){
            return;
        }
        UA_SAAS_SERVICE_APP.widgetContext.module = UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module:"contact";
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${module}s/search?_all=${searchTerm}*`,"GET",{});
            if(response)
            {    
                console.log(response);
                UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
                $("#contact-search-items").html(" ");
                if(response.data && response.data["data"].length>0)
                {
                    var search_items = response.data["data"];
                    if(search_items && search_items.length > 50){
                        search_items.splice(0,49);
                    }
                    UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module] = {};
                    search_items.forEach((obj)=>{
                        UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][obj.id] = obj;
                        let fullName = "";
                        if(module)
                        {
                            switch(module){
                                case("contact"): {
                                    fullName = obj.firstName ? obj.firstName:"";
                                    if(obj.lastName){
                                        fullName = fullName+" "+ obj.lastName;
                                    } 
                                    fullName = fullName?fullName:"Contact#"+obj.id;
                                    break;
                                }
                                case("ticket"):{
                                    fullName = "Ticket#"+obj.ticketNumber;
                                    break;
                                }
                                case("account"):{
                                    fullName = obj.accountName ? obj.accountName:"Account#"+obj.code;
                                    break;
                                }
                            }
                        }
                        let itemHTML = `<div class="item-list-popup-item" style="cursor:pointer;" onclick="UA_SAAS_SERVICE_APP.selectPhoneNumber('${obj.id}', '${module.trim()}', '${fullName}','${module}','${true}')"><span class="c-name"> ${fullName} </span>`;
                        itemHTML =  itemHTML + `${'<span class="c-phone '+(UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[module+"_"+obj.id] ? 'alreadyadded': '')+'"><i class="material-icons">phone_iphone</i>(phone)</span>'}`;
                        itemHTML =  itemHTML + `</div>`;
                        $("#contact-search-items").append(itemHTML);
                    });
                }
                else
                {
                    $("#contact-search-items").html("No records found. <br><br> <span class=\"c-silver\"> This search will include <br> only customers having phone numbers</span>");
                    return;
                }
            }
    },

    selectPhoneNumber: async function(id,number,name,module,isSelected){
        var receipNumberID = module + "_" + id;
        if(!UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] && !UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id]){
            UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] = {
                'id': id,
                'name': name,
                'module': module,
                'isSelected': isSelected
            };
            $.extend(UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id],UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID]) ;
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id] = UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id];
            // UA_SAAS_SERVICE_APP.renderInitialElements(module,id);
            
            if(UA_SAAS_SERVICE_APP.widgetContext.module !== "ticket" && $("#inp_chk_create_new_ticket_on_msg").length < 1){
                $('.ssf-win-actions-holder').prepend(`<div style="padding: 0px 0px 10px 0px;"><input id="inp_chk_create_new_ticket_on_msg" name="inp_chk_create_new_ticket_on_msg" type="checkbox"> <label for="inp_chk_create_new_ticket_on_msg">Create a new ticket </label></div>`);
            }
            await UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
            UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(module, id, UA_SAAS_SERVICE_APP.addRecordsAsRecipients);
            // UA_SAAS_SERVICE_APP.addRecordsAsRecipients([UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id]]);
        }
        else {
            console.log(module+' already added');
            // showErroWindow("Alert Message",`This ${module.toLocaleLowerCase()} is already added.`)
            alert(`This ${module.toLocaleLowerCase()} is already added.`);
            return;
        }
    },

    getCurrentUserInfo : async function(callback){
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/myinfo', callback);
    },
    
    getCurrentOrgInfo : async function(callback){
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/accessibleOrganizations', callback);
    },
    
    getAPIResponseAndCallback: async function(url, callback){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}${url}${UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO ? '?orgId='+UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO.id : ''}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        callback(response);
    },
    
    showErrorIfInAPICall : function(response){
        if(response.code === 401 || response.code === 403){
            UA_OAUTH_PROCESSOR.showReAuthorizationError(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED);
        }
    },
    
    initiateAuthFlow: function(){
        UA_OAUTH_PROCESSOR.initiateAuth(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED);
    },
    
    fetchStoreExecuteOnRecordDetails: async function(module, recordIds, callback){

        /*if(typeof recordIds !== "object"){
            var recordId = recordIds;
            recordIds = [recordId];
        }
//        recordIds = ["5318123000000377412"];
        $("#recip-count-holder").text(`fetching ${recordIds.length} ${module.toLocaleLowerCase()} information...`);
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.API_BASE_URL}/${module}?ids=${recordIds.join(',')}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('fetchStoreExecuteOnRecordDetails',response);
        response.data.data.forEach(recordItem=>{
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordItem.id] = recordItem;
        });
        $("#recip-count-holder").text(`Selected ${recordIds.length} ${module.toLocaleLowerCase()}`);*/
        
        callback(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
        UA_SAAS_SERVICE_APP.entityDetailFetched();
    },

    fetchAndStoreDefaultDepartments: async function(callback) { 
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/departments', callback);
    },
    
    addRecordsAsRecipients: function(contactsList){
        for(var item in contactsList){
            let contactItem = contactsList[item];
            if($(`[data-recip-id=${contactsList[item]['id']}]`).length === 0) {
                if(!contactItem.name){
                    if(UA_SAAS_SERVICE_APP.widgetContext.module === "contact") {
                        contactItem.name = contactItem.lastName;
                        if(contactItem.firstName){
                            contactItem.name = contactItem.firstName+" "+ contactItem.lastName;
                        }
                    }
                    else if(UA_SAAS_SERVICE_APP.widgetContext.module === "ticket") {
                        contactItem.name = contactItem.contactName?contactItem.contactName : "";
                    }
                }
                UA_APP_UTILITY.addInStoredRecipientInventory(contactItem);
            }
        }
    },
    showExpandedModalView: function(){
        UA_SAAS_SERVICE_APP.DESK_APP.instance.modal({
                                        url: window.location.href+"&openMode=modal&module="+UA_SAAS_SERVICE_APP.widgetContext.module+"&entityId="+UA_SAAS_SERVICE_APP.widgetContext.entityId,
                                        title: appPrettyName+" SMS"
                                        }).then(function(modalInfo) {
                                        var modalInstance = UA_SAAS_SERVICE_APP.DESK_APP.instance.getWidgetInstance(modalInfo.widgetID);
                                        modalInstance.on('modal.opened', function(data) {
                                        // console.log('modal opened ++++++++++++++++++', data);
                                        
                                            modalInstance.emit('event', {
                                                "dataType": "initialContextModules",
                                                'UA_SAAS_SERVICE_APP.widgetContext.module': UA_SAAS_SERVICE_APP.widgetContext.module,
                                                'UA_SAAS_SERVICE_APP.widgetContext.entity' : UA_SAAS_SERVICE_APP.widgetContext.entity
                                            });
                                        });
                                        }).catch(function(err) {
                                        console.log(err, "Modal error");
                                    });
    },
    
    addSentSMSAsRecordInHistory: async function(sentMessages){
//        var historyDataArray = [];
        if(!UA_SAAS_SERVICE_APP.widgetContext.module){
            return;
        }
        var credAdProcess3 = curId++;
        showProcess(`Adding ${Object.keys(sentMessages).length} messages to ${UA_SAAS_SERVICE_APP.widgetContext.module} comments...`, credAdProcess3);

        var commentContent = `<u><i>${appPrettyName} Extension</i></u> - `;
        for(var sentMessageId in sentMessages){
            let sentMessage = sentMessages[sentMessageId];
            UA_SAAS_SERVICE_APP.addTimeEntryInSAASForSentMessage(sentMessage);
            let recModule = sentMessage.module?sentMessage.module:UA_SAAS_SERVICE_APP.widgetContext.module; 
            let recId = sentMessage.moduleId?(sentMessage.moduleId.indexOf("newmanual")!==0?sentMessage.moduleId:UA_SAAS_SERVICE_APP.widgetContext.entityId):UA_SAAS_SERVICE_APP.widgetContext.entityId;
//            let historyData = {};
//            historyData['Name'] = 'SMS to '+(sentMessage.contact.name ? sentMessage.contact.name : sentMessage.number);
//            historyData[UA_SAAS_SERVICE_APP.APP_API_NAME+'Message'] = sentMessage.message;
//            historyData[UA_SAAS_SERVICE_APP.APP_API_NAME+'Customer_Number'] = sentMessage.to;
//            historyData[UA_SAAS_SERVICE_APP.APP_API_NAME+'From'] = sentMessage.from;
//            historyData[UA_SAAS_SERVICE_APP.APP_API_NAME+'Direction'] = "Outbound";
//            historyData[UA_SAAS_SERVICE_APP.APP_API_NAME+'Status'] = sentMessage.status;
//            historyDataArray.push(historyData);
            let commentContent = `<u><i>${appPrettyName} Extension</i></u> - `;
            commentContent+=`<b>${sentMessage.channel} ${sentMessage.status} from ${sentMessage.from} to ${sentMessage.to}</b>:<br>
                             ${sentMessage.message}<br>
                                `;
                    if(document.getElementById('inp_chk_create_new_ticket_on_msg') && document.getElementById('inp_chk_create_new_ticket_on_msg').checked){
                        let departmentId = await ZOHODESK.get('department.id');
                        departmentId = departmentId['department.id'];
                        let contactId =  null;
                        if(UA_SAAS_SERVICE_APP.widgetContext.module === "contact"){
                            contactId = recId;
                        }
                        if(UA_SAAS_SERVICE_APP.widgetContext.module === "ticket"){
                            contactId = UA_SAAS_SERVICE_APP.widgetContext.entity.contactId;
                        }
                        UA_SAAS_SERVICE_APP.createTicketForContact(departmentId, contactId, appPrettyName+" Message Sent to "+sentMessage.to, commentContent, sentMessage.to);
                    }
            
            let response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${recModule.toLocaleLowerCase()}s/${recId}/comments`, "POST", {
                content: commentContent,
                contentType: 'html'
            });
            
            UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
            // console.log('addSentSMSAsRecordInHistory',response);
        }
        processCompleted(credAdProcess3);
        if(!UA_APP_UTILITY.isConvChatView()){
            ZOHODESK.notify({
                title : appPrettyName+" Messages Sent Successfully",
                content : `All Messages has been added to ${UA_SAAS_SERVICE_APP.widgetContext.module} comments`, 
                icon:"success", 
                autoClose:"true"
            });
        }
    },

    async createTicketForContact(departmentId, contactId, subject, description, phone){
        var credAdProcess3 = curId++;
        showProcess(`Creating ticket...`, credAdProcess3);
        var ticketCreateObj = {
            subject: subject,
            description: description,
            departmentId: departmentId,
            phone: phone
        };
        if(contactId){
            ticketCreateObj.contactId = contactId;
        }
        else{
            ticketCreateObj.contact = {
                'phone': phone,
                'name': phone
            };
        }
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/tickets`, "POST", ticketCreateObj);
        processCompleted(credAdProcess3);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        // console.log('addSentSMSAsRecordInHistory',response);
        ZOHODESK.notify({
            title : appPrettyName+" - Ticket Created Successfully",
            content : `Messages has been added to new ticket`, 
            icon:"success", 
            autoClose:"true"
        });
    },
    entityDetailFetched: function(data){
        setTimeout(()=>{UA_APP_UTILITY.fetchedAllModuleRecords();}, 2000);
        if(typeof UA_TPA_FEATURES.callOnEntityDetailFetched === "function"){
            UA_TPA_FEATURES.callOnEntityDetailFetched(data);
        }
    },
    renderDefaultDepartmentsInIncomingSelect: function(){
        $('#inc-sel-incoming-def-dept').html('<option value="-" selected>Select a department</option>');
        for(let item in this.FETCHED_DEPARTMENTS){
            let department = this.FETCHED_DEPARTMENTS[item];
            $('#inc-sel-incoming-def-dept').append(`<option value="${department.id}">${department.name}</option>`);
        }
        UA_SAAS_SERVICE_APP.setSelectedIncomingDepartmentIdInDD();
    },
    setSelectedIncomingDepartmentIdInDD: function(){
        if(!UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS){
            UA_APP_UTILITY.CALL_AFTER_PERSONAL_COMMON_SETTING_RESOLVED.push(UA_SAAS_SERVICE_APP.setSelectedIncomingDepartmentIdInDD);
            return false;
        }
        if(UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS['ua_incoming_saas_default_department_id']){
            $('#inc-sel-incoming-def-dept').val(UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS['ua_incoming_saas_default_department_id']);
        }
    },
    insertIncomingConfigItems: function(){
        $('#incoming-module-config-item').append(`
            <div class="tpaichan-item">
                Default department for ticket creation <select onchange="UA_SAAS_SERVICE_APP.updateDefaultDepartmentIdForTicket()" id="inc-sel-incoming-def-dept"><option>Loading...</option></select>
            </div>`);
        if(isUASMSApp){
            $('#incoming-module-config-item').append(`
            <div class="tpaichan-item">
                <label class="switch">
                <input onchange="UA_APP_UTILITY.toggleSAASTicketReplyCaptureConfig()" type="checkbox" id="saas-switch-incoming-comments-enable-capture">
                <div class="slider round"></div>
                </label> Listen for ticket public comments to send replies
            </div>`);
        }
    },
    updateDefaultDepartmentIdForTicket: function(){
        UA_APP_UTILITY.saveSettingInCredDoc('ua_incoming_saas_default_department_id', $('#inc-sel-incoming-def-dept').val());
    },
    EXISTING_WEBHOOK_ID: null,
    serviceSID: null,

    isWebhookAlreadyExists: async function(){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/webhooks?orgId=${appsConfig.UA_DESK_ORG_ID}`, "GET", {}, {'Accept' :"application/json"});
        if(UA_SAAS_SERVICE_APP.showIfErrorInAPIResult(response)){
            return;
        }
        let existingServiceID = null;
        if(!response || !response.data){
            return false;
        }
        response.data.data.forEach((item)=>{
            let url = item.url;
            if(url && url.startsWith(UA_APP_UTILITY.getSAASWebhookBaseURL())){
                existingServiceID = item.id;
            }
        });
        UA_SAAS_SERVICE_APP.EXISTING_WEBHOOK_ID = existingServiceID;
        UA_SAAS_SERVICE_APP.incomingCaptureStatusChanged();
        UA_SAAS_SERVICE_APP.serviceSID = existingServiceID;
        return existingServiceID !== null;
    },

    addNewWebhook: async function(){
        if(!UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS){
            UA_APP_UTILITY.CALL_AFTER_PERSONAL_COMMON_SETTING_RESOLVED.push(UA_SAAS_SERVICE_APP.addNewWebhook);
            return false;
        }
        if(UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.hasOwnProperty('ua_incoming_saas_ticket_reply_enable_capture_'+currentUser.uid) && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS['ua_incoming_saas_ticket_reply_enable_capture_'+currentUser.uid] === false){
            return;
        }
        if(UA_APP_UTILITY.PERSONAL_WEBHOOK_TOKEN && appsConfig.UA_DESK_ORG_ID){
            var webhookExists = await UA_SAAS_SERVICE_APP.isWebhookAlreadyExists();
            if(!webhookExists){
                var webhookData = {
                        "subscriptions": {
                            "Ticket_Comment_Add": null
                        },
                        "includeEventsFrom" : [ "AUTOMATION" ],
                        "url": UA_APP_UTILITY.getAuthorizedSAASWebhookURL()
                    };
                var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/webhooks`, "POST", webhookData);
                if(UA_SAAS_SERVICE_APP.showIfErrorInAPIResult(response)){
                    return false;
                }
                UA_SAAS_SERVICE_APP.serviceSID = response.data.id;
                UA_SAAS_SERVICE_APP.EXISTING_WEBHOOK_ID = UA_SAAS_SERVICE_APP.serviceSID;
                UA_SAAS_SERVICE_APP.incomingCaptureStatusChanged();
                return response.data && (response.data.id !== null);
            }
            return true;
        }
        return false;
    },
    incomingCaptureStatusChanged: function(){
        if(UA_SAAS_SERVICE_APP.EXISTING_WEBHOOK_ID){
            document.getElementById('saas-switch-incoming-comments-enable-capture').checked = true;
        }
        else{
            document.getElementById('saas-switch-incoming-comments-enable-capture').checked = false;
        }
    },

    showIfErrorInAPIResult: function(response){
        console.log(response);
        if((response.code === 401 || response.message === "Authentication failed") || (response.error && response.error.code === 401) || (response.data && (response.data.code === 401 || response.data.message === "Authentication failed"))){
            UA_OAUTH_PROCESSOR.showReAuthorizationError(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED);
            return true;
        }
        return false;
    },
    deleteExistingWebhook: async function(){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/webhooks/${UA_SAAS_SERVICE_APP.EXISTING_WEBHOOK_ID}`, "DELETE", {});
        if (UA_SAAS_SERVICE_APP.showIfErrorInAPIResult(response)){
            return false;
        }
        UA_SAAS_SERVICE_APP.EXISTING_WEBHOOK_ID = null;
        UA_SAAS_SERVICE_APP.incomingCaptureStatusChanged();
        return true;
    },
    FETCHED_SUGGESTED_ARTICLES: {},
    addSuggestedArtcilesAsTemplateDropdown: async function(){
        if(!isUASMSApp){
            return;
        }
        if(UA_SAAS_SERVICE_APP.widgetContext.module !== "ticket" || !UA_SAAS_SERVICE_APP.widgetContext.entityId){
            return;
        }
        let apiURL = `${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/tickets/${UA_SAAS_SERVICE_APP.widgetContext.entityId}/articleSuggestion?departmentId=${UA_SAAS_SERVICE_APP.CURRENT_DEPARTMENT_ID ? UA_SAAS_SERVICE_APP.CURRENT_DEPARTMENT_ID : 0}&orgId=${appsConfig.UA_DESK_ORG_ID}`;
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(apiURL, "GET", {});
        if (UA_SAAS_SERVICE_APP.showIfErrorInAPIResult(response)){
            return false;
        }
        let templatesArray = [];
        if(response.data && response.data.data){
            response.data.data.forEach(item=>{
                UA_APP_UTILITY.fetchedTemplates[item.id] = {
                    'message': item.summary,
                    'templateType': 'saas_api_need_fetch',
                    'tempid': item.id
                };
                UA_SAAS_SERVICE_APP.FETCHED_SUGGESTED_ARTICLES[item.id] = item;
                templatesArray.push({
                    'label': item.title,
                    'value': item.id
                });
            });
        }
        if(templatesArray.length > 0){
            UA_APP_UTILITY.renderSelectableDropdown('#ssf-fitem-template-var-holder', 'Suggested Articles', templatesArray, 'UA_SAAS_SERVICE_APP.fetchArticleAndCall_insertTemplateContentInMessageInput', false, false, null);
        }
    },
    fetchArticleAndCall_insertTemplateContentInMessageInput: async function(articleID){
        UA_APP_UTILITY.currentAttachedExtraFiles = {};
        UA_APP_UTILITY.removeCurrentUploadedAttachment();
        UA_APP_UTILITY.removeAllExtraUploadedAttachment();
        if(UA_APP_UTILITY.fetchedTemplates[articleID].templateType === 'saas_api_need_fetch'){
            await UA_SAAS_SERVICE_APP.getDetailedArticleMessageDetailAndStore(articleID);
        }
        let i =0;
        UA_APP_UTILITY.fetchedTemplates[articleID].attachments.forEach(async attachmentObj=>{
            i++;
            UA_APP_UTILITY.fetchAndUploadAndGetDownloadURLOfAttachment(attachmentObj.downloadUrl, attachmentObj.name, null, true, true, i===1, true);
        });
        UA_TPA_FEATURES.insertTemplateContentInMessageInput(articleID);
        UA_APP_UTILITY.autoGrowTextArea(document.getElementById('inp-ssf-main-message'));
    },
    getDetailedArticleMessageDetailAndStore: async function(articleID){
        var mediaAPIResp = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/articles/${articleID}`, "GET", {});
        if(mediaAPIResp.status === 200 && mediaAPIResp.data){
            UA_APP_UTILITY.fetchedTemplates[articleID] = mediaAPIResp.data;
            UA_APP_UTILITY.fetchedTemplates[articleID].message = UA_APP_UTILITY.stripHtml(mediaAPIResp.data.answer);
            UA_APP_UTILITY.fetchedTemplates[articleID].templateType = 'content_only_template';
            UA_APP_UTILITY.fetchedTemplates[articleID].tempid = articleID;
        }
        return UA_APP_UTILITY.fetchedTemplates[articleID];
    },
    addTimeEntryInSAASForSentMessage: async function(message){
        if(!isUASMSApp){
            return;
        }
        if(UA_SAAS_SERVICE_APP.widgetContext.module !== "ticket" || !UA_SAAS_SERVICE_APP.widgetContext.entityId){
            return;
        }
        let ticketTimeEntryObj = {
            "secondsSpent": Math.ceil((new Date().getTime() - UA_APP_UTILITY.TIME_ENTRY_TICKET_START_TIME) * 0.001),
            "executedTime" : new Date().toISOString(),
            "description": `${message.channel} message sent via ${appPrettyName}`,
            "ownerId": UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.id
        };
        var mediaAPIResp = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/tickets/${UA_SAAS_SERVICE_APP.widgetContext.entityId}/timeEntry`, "POST", ticketTimeEntryObj);
        UA_APP_UTILITY.TIME_ENTRY_TICKET_START_TIME = new Date().getTime();
        console.log('time entry added response', mediaAPIResp);
    },

    searchRelatedRecordsToAllModules: async function(convId){
        try{
            var data = await UA_APP_UTILITY.getServiceSAASTPAConversationMapIfExists("ZOHODESK", "tickets", "tpa_conv_"+convId.replace(/[^a-z0-9_&]+/g, "_").replace(/[&]+/g, "-_-"));
            var searchData = [];
            if(data && data.ticket_id){
                let ticket_res = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/tickets/${data.ticket_id}`, "GET", null);
                if(ticket_res && ticket_res.data.subject){
                    let owner_name = ticket_res.data.assigneeId?UA_LIC_UTILITY.SAAS_FETCHED_USERS_IDS_LIST[ticket_res.data.assigneeId].name: "Unassigned";
                    owner_name = owner_name? owner_name:"No Name";
                    searchData.push({
                        module: "Ticket",
                        record: {
                            name: ticket_res.data.subject,
                            webURL: ticket_res.data.webUrl,
                            owner_name: owner_name
                        }
                    });
                }
            }
            return searchData;
        }
        catch(err){ console.log(err); return []}
    },
    RENDER_RAW_FIELD_ID_IN_FIELDMAPPING: true,
    LOOKUP_SUPPORTED_MODULES: {
        "tickets": {
            "displayLabel": "Tickets",
            "apiName": "tickets",
            "autoLookup": true,
            "relatedModules": ["deals"]
        },
        "contacts": {
            "apiName": "contacts",
            "relatedModules": ["contacts"],
            "hideDisplay": true
        }
    },
    allModuleFieldsFetched: function(){
        let fieldsPresentModuleCount = 0;
        Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).forEach(item=>{
            if(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[item].fields && Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[item].fields).length > 0){
                fieldsPresentModuleCount++;
            }
        });
        return Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).length === fieldsPresentModuleCount;
    },
    retrieveModuleFieldsForLookUp: function(callback){
        $('#ua-loo-search-field-ddholder').hide();
        if (UA_SAAS_SERVICE_APP.allModuleFieldsFetched()) {
            callback();
        }
        let primaryFieldsToCreate = ["subject"];
        let primaryFieldsToViewForModule = [];
        Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).forEach(async moduleId=>{
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/layouts/standardLayoutFormat?module=${moduleId}`, "GET", null);
            if(response.error && response.error.errorCode === "LICENSE_ACCESS_LIMITED"){
                response = UA_SAAS_SERVICE_APP.getSampleMockData("layoutfields_tickets");
            }
            let fieldAPINameMap = {};
            console.log(response);
            response.data.data.forEach(fieldSecItem=>{
                fieldSecItem.fields.forEach(fieldItem=>{
                try{
                    primaryFieldsToCreate = [];
                    primaryFieldsToViewForModule = [];
                    if(["varchar", "varchar_auto", "time", "monetary", "double"].includes(fieldItem.field_type)){
                        fieldItem.field_type = "text";
                    }
                    if(fieldItem.field_type === "date"){
                        fieldItem.field_type = "datetime";
                    }
                    if(fieldItem.field_type === "time"){
                        fieldItem.field_type = "datetime";
                    }
                    if(fieldItem.field_type === "address"){
                        fieldItem.field_type = "textarea";
                    }
                    if(fieldItem.field_type === "Picklist"){
                        fieldItem.field_type = "picklist";
                        fieldItem.pick_list_values = [];
                        if(!fieldItem.allowedValues){
                            return true;
                        }
                        fieldItem.allowedValues.forEach(optItem=>{
                            fieldItem.pick_list_values.push({
                                'actual_value': optItem,
                                'display_value': optItem
                            });
                        });
                    }
                    fieldAPINameMap[fieldItem.apiName] = {
                        'display_label': fieldItem.displayLabel,
                        'data_type': fieldItem.type,
                        'api_name': fieldItem.apiName,
                        'field_read_only': false,
                        'system_mandatory': fieldItem.isSystemMandatory,
                        'pick_list_values' : fieldItem.pick_list_values
                    };
                    if(fieldItem.isSystemMandatory){
                        primaryFieldsToCreate.push(fieldItem.name);
                    }
                }
                catch(ex){
                    console.log(ex);
                }
            });
        });
            fieldAPINameMap.id = {
              'display_label': 'ID',
              'data_type': 'number',
              'api_name': 'id',
              'field_read_only': true
            };
            UA_TPA_FEATURES.setCreateModuleRecordFields(moduleId, primaryFieldsToCreate);
            UA_TPA_FEATURES.setViewModuleRecordFields(moduleId, primaryFieldsToViewForModule);
            UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[moduleId].fields = fieldAPINameMap;
            if(UA_SAAS_SERVICE_APP.allModuleFieldsFetched()){
                callback();
            }
        });
    },

    getSampleMockData: function(type){
        if(type === "layoutfields_tickets"){
            return JSON.parse(`{"data":{"data":[{"name":"Case Information","id":1,"fields":[{"displayLabel":"Department","apiName":"departmentId","isCustomField":false,"i18NLabel":"Department","name":"departmentId","isEncryptedField":false,"id":"484134000000000389","isSystemMandatory":false,"isRemovable":false,"type":"LookUp","maxLength":50,"isMandatory":true},{"displayLabel":"Layout","apiName":"layoutId","isCustomField":false,"i18NLabel":"Layout","name":"layoutId","isEncryptedField":false,"id":"484134000004139017","isSystemMandatory":false,"isRemovable":false,"type":"LookUp","maxLength":50,"isMandatory":true},{"displayLabel":"Contact Name","apiName":"contactId","isCustomField":false,"i18NLabel":"Contact Name","name":"contactId","isEncryptedField":false,"id":"484134000000000391","isSystemMandatory":true,"isRemovable":false,"type":"LookUp","maxLength":300,"isMandatory":true},{"displayLabel":"Account Name","apiName":"accountId","isCustomField":false,"i18NLabel":"Account Name","name":"accountId","isEncryptedField":false,"id":"484134000000000433","isSystemMandatory":false,"isRemovable":true,"type":"LookUp","maxLength":300,"isMandatory":false},{"displayLabel":"Email","apiName":"email","isCustomField":false,"i18NLabel":"Email","name":"email","isEncryptedField":false,"id":"484134000000000393","isSystemMandatory":false,"isRemovable":false,"type":"Email","maxLength":120,"isMandatory":false},{"displayLabel":"Phone","apiName":"phone","isCustomField":false,"i18NLabel":"Phone","name":"phone","isEncryptedField":false,"id":"484134000000000395","isSystemMandatory":false,"isRemovable":true,"type":"Phone","maxLength":120,"isMandatory":false},{"displayLabel":"Subject","apiName":"subject","isCustomField":false,"i18NLabel":"Subject","name":"subject","isEncryptedField":false,"id":"484134000000000397","isSystemMandatory":true,"isRemovable":false,"type":"Text","maxLength":255,"isMandatory":true},{"displayLabel":"Description","apiName":"description","isCustomField":false,"i18NLabel":"Description","name":"description","isEncryptedField":false,"id":"484134000000000399","isSystemMandatory":false,"isRemovable":false,"type":"Textarea","maxLength":65535,"isMandatory":false},{"displayLabel":"Status","allowedValues":["Open","On Hold","Escalated","Closed"],"apiName":"status","isCustomField":false,"defaultValue":"Open","i18NLabel":"Status","isSystemMandatory":true,"type":"Picklist","restoreOnReplyValues":["Open","On Hold","Escalated","Closed"],"name":"status","isEncryptedField":false,"sortBy":"userDefined","id":"484134000000000401","isRemovable":false,"maxLength":120,"isMandatory":true},{"displayLabel":"Ticket Owner","apiName":"assigneeId","isCustomField":false,"i18NLabel":"Ticket Owner","name":"assigneeId","isEncryptedField":false,"id":"484134000000000405","isSystemMandatory":false,"isRemovable":false,"type":"LookUp","maxLength":120,"isMandatory":false},{"displayLabel":"Product Name","apiName":"productId","isCustomField":false,"i18NLabel":"Product Name","name":"productId","isEncryptedField":false,"id":"484134000000000403","isSystemMandatory":false,"isRemovable":true,"type":"LookUp","maxLength":120,"isMandatory":false},{"displayLabel":"Skills","apiName":"entitySkills","isCustomField":false,"i18NLabel":"Skills","name":"Skills","isEncryptedField":false,"id":"484134000005353004","isSystemMandatory":false,"isRemovable":true,"type":"LookUp","maxLength":300,"isMandatory":false}]},{"name":"Additional Information","id":2,"fields":[{"displayLabel":"Language","allowedValues":["-None-","Abkhazian","Afar","Afrikaans","Akan","Albanian","Amharic","Arabic","Aragonese","Armenian","Assamese","Asturian","Avaric","Avestan","Aymara","Azerbaijani","Bambara","Bashkir","Basque","Belarusian","Bengali","Bihari","Bislama","Bosnian","Breton","Bulgarian","Burmese","Catalan","Cebuano","Chamorro","Chechen","Chichewa","Chinese (Simplified)","Chinese (Traditional)","Church Slavic","Chuvash","Cornish","Corsican","Cree","Croatian","Czech","Danish","Divehi","Dutch","Dzongkha","English","Esperanto","Estonian","Ewe","Faroese","Fijian","Filipino","Finnish","French","Fulah","Gaelic","Galician","Ganda","Georgian","German","Greek","Guarani","Gujarati","Haitian","Hausa","Hawaiian","Hebrew","Herero","Hindi","Hiri Motu","Hmong","Hungarian","Icelandic","Ido","Igbo","Indonesian","Interlingua","Interlingue","Inuktitut","Inupiaq","Irish","Italian","Japanese","Javanese","Kalaallisut","Kannada","Kanuri","Kashmiri","Kazakh","Khmer","Kikuyu","Kinyarwanda","Kirghiz","Komi","Kongo","Korean","Kuanyama","Kurdish","Lao","Latin","Latvian","Limburgan","Lingala","Lithuanian","Luba-Katanga","Luxembourgish","Macedonian","Malagasy","Malay","Malayalam","Maltese","Manx","Maori","Marathi","Marshallese","Mongolian","Nauru","Navajo","Ndonga","Nepali","North Ndebele","Northern Sami","Norwegian","Norwegian Bokmal","Norwegian Nynorsk","Occitan","Ojibwa","Oriya","Oromo","Ossetian","Pali","Pashto","Persian","Polish","Portuguese","Punjabi","Quechua","Romanian","Romansh","Rundi","Russian","Samoan","Sango","Sanskrit","Sardinian","Serbian","Shona","Sichuan Yi","Sindhi","Sinhala","Slovak","Slovenian","Somali","South Ndebele","Southern Sotho","Spanish","Sundanese","Swahili","Swati","Swedish","Tagalog","Tahitian","Tajik","Tamil","Tatar","Telugu","Thai","Tibetan","Tigrinya","Tonga","Tsonga","Tswana","Turkish","Turkmen","Twi","Uighur","Ukrainian","Urdu","Uzbek","Venda","Vietnamese","Volapük","Walloon","Welsh","Western Frisian","Wolof","Xhosa","Yiddish","Yoruba","Zhuang","Zulu"],"apiName":"language","isCustomField":false,"defaultValue":"-None-","i18NLabel":"Language","isSystemMandatory":false,"type":"Picklist","name":"language","isEncryptedField":false,"sortBy":"userDefined","id":"484134000007657001","isRemovable":true,"maxLength":255,"isMandatory":false},{"displayLabel":"Due Date","apiName":"dueDate","isCustomField":false,"i18NLabel":"Due Date","name":"dueDate","isEncryptedField":false,"id":"484134000000000435","isSystemMandatory":false,"isRemovable":false,"type":"DateTime","maxLength":300,"isMandatory":false},{"displayLabel":"Priority","allowedValues":["-None-","High","Medium","Low"],"apiName":"priority","isCustomField":false,"defaultValue":"-None-","i18NLabel":"Priority","isSystemMandatory":false,"type":"Picklist","name":"priority","isEncryptedField":false,"sortBy":"userDefined","id":"484134000000000437","isRemovable":false,"maxLength":120,"isMandatory":false},{"displayLabel":"Channel","allowedValues":["Phone","Email","Web","Twitter","Facebook","Chat","Forums","Feedback Widget"],"apiName":"channel","isCustomField":false,"defaultValue":"Phone","i18NLabel":"Channel","isSystemMandatory":false,"type":"Picklist","name":"channel","isEncryptedField":false,"sortBy":"userDefined","id":"484134000000000439","isRemovable":false,"maxLength":120,"isMandatory":false},{"displayLabel":"Classification","allowedValues":["-None-","Question","Problem","Feature","Others"],"apiName":"classification","isCustomField":false,"defaultValue":"-None-","i18NLabel":"Classifications","isSystemMandatory":false,"type":"Picklist","name":"classification","isEncryptedField":false,"sortBy":"userDefined","id":"484134000000022001","isRemovable":true,"maxLength":120,"isMandatory":false},{"displayLabel":"Resolution","apiName":"resolution","isCustomField":false,"i18NLabel":"Resolution","name":"resolution","isEncryptedField":false,"id":"484134000000000425","isSystemMandatory":false,"isRemovable":false,"type":"Textarea","maxLength":65535,"isMandatory":false}]}]},"api_domain":"desk.zoho.com","status":200}`);
        }
    }
};



window.addEventListener('load', function () {
    ZOHODESK.extension.onload().then(async function (App) {
            // console.log(App);
            UA_OAUTH_PROCESSOR.currentSAASDomain = UA_OAUTH_PROCESSOR.domainExtnMap[App.meta.dcType];
            UA_SAAS_SERVICE_APP.DESK_APP = App;
            UA_SAAS_SERVICE_APP.appsConfig.EXTENSION_ID = App.extensionID;
            let departmentId = await ZOHODESK.get('department.id');
            departmentId = departmentId['department.id'];
            UA_SAAS_SERVICE_APP.CURRENT_DEPARTMENT_ID = departmentId;
            ZOHODESK.get("portal.id").then(function (response) {
                UA_SAAS_SERVICE_APP.appsConfig.UA_DESK_ORG_ID = response['portal.id'];
            }).catch(function (err) {
                console.log(err);
            });
            ZOHODESK.get("portal.name").then(function (response) {
                UA_SAAS_SERVICE_APP.appsConfig.UA_DESK_PORTAL_NAME = response['portal.name'];
            }).catch(function (err) {
                console.log(err);
            });
            ZOHODESK.get("portal.customDomainName").then(function(response){
                UA_SAAS_SERVICE_APP.appsConfig.UA_DESK_CUSTOMDOMAIN_NAME = response['portal.customDomainName'];
            }).catch(function(err){
                console.log(err);
            });
                if(queryParams.get("openMode") !== "modal" && App.location !== "desk.topband"){
                    $('.siteName').hide();
                    $('.siteName').html(`<span onclick="UA_SAAS_SERVICE_APP.showExpandedModalView()" class="material-icons topModalExpandBtn">aspect_ratio</span>`).show();
                    $("#showExpandModalBtn").show();
                }
            var supportedModules = ["ticket", "contact", "account"];
            
            supportedModules.forEach(async item=>{
                if(App.location.indexOf(item) > 0){
                    UA_SAAS_SERVICE_APP.widgetContext.module = item;
                    await ZOHODESK.get(item).then(function(response){
                        UA_SAAS_SERVICE_APP.widgetContext.entity = response[item];
			UA_SAAS_SERVICE_APP.widgetContext.entityId = response[item].id;
                        UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[response[item].id] = response[item];
                    });
                    if(item){
                            if(queryParams.get("openMode") === "modal"){
                                setTimeout(function(){
                                    ZOHODESK.invoke('RESIZE', { width: '80%', height: '60%' });
                                }, 3000);
                            }
                            else{
                               /* window.addEventListener('click', function(){
                                    if(UA_SAAS_SERVICE_APP.isModelOpened){
                                        return;
                                    }
                                    UA_SAAS_SERVICE_APP.isModelOpened = true;
                                    UA_SAAS_SERVICE_APP.showExpandedModalView();
                                }); */
                            }
                        }
                }
            });
            
            App.instance.on('event', function(data){
                // console.log('daataa received', data);
                if(data.dataType === "initialContextModules"){
                    UA_SAAS_SERVICE_APP.widgetContext.module = data['UA_SAAS_SERVICE_APP.widgetContext.module'];
                    UA_SAAS_SERVICE_APP.widgetContext.entity = data['UA_SAAS_SERVICE_APP.widgetContext.entity'];
                    UA_SAAS_SERVICE_APP.widgetContext.entityId = UA_SAAS_SERVICE_APP.widgetContext.entity.id;
                    UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[UA_SAAS_SERVICE_APP.widgetContext.entityId] = UA_SAAS_SERVICE_APP.widgetContext.entity;
                }
            });

        });
});