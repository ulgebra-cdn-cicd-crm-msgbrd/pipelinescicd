var UA_SAAS_SERVICE_APP = {
    
    CURRENT_USER_INFO : null,
    CURRENT_ORG_INFO : null,
    CURRENT_MODULE_FIELDS_DROPDOWN : [],
    CURRENT_OAUTH_SCOPE_NEEDED : ['ZohoCampaigns.contact.CREATE','ZohoCampaigns.contact.READ','ZohoCampaigns.contact.UPDATE','ZohoCampaigns.campaign.READ'],
    FETCHED_RECORD_DETAILS: {},
    widgetContext: {},
    APP_API_NAME: "pinnaclesms__",
    SELECTED_RECEIPS: {},
    MODULE_FIELDS_MAP: {},
    SEARCH_RECORD_MODULE : "contacts",
    SEARCH_MODULE_RECORD_MAP: {},
    fetchedList : {},
    getAPIBaseUrl : function(){
        return `https://campaigns.zoho${UA_OAUTH_PROCESSOR.currentSAASDomain}/api/v1.1`;
    },
    initiateAPPFlow: async function(){
        $("#ac_name_label_saas .anl_servicename").text('Zoho Campaigns');

            // $('#ac_name_label_tpa').after(`<div id="ac_name_label_saas" class="suo-item suo-item-ac-labels" onclick="UA_SAAS_SERVICE_APP.showAddManageListKeyUI()">
            //                                     <span class="material-icons">restart_alt</span> Manage List Keys
            //                                </div>`);

            // UA_SAAS_SERVICE_APP.widgetContext.module = queryParams.get("module");
            UA_SAAS_SERVICE_APP.widgetContext.module = "contacts";

            // UA_SAAS_SERVICE_APP.widgetContext.mode = "bulk";

            UA_SAAS_SERVICE_APP.widgetContext.domain = "."+queryParams.get("server_domain");
            // UA_SAAS_SERVICE_APP.widgetContext.domain = ".com";

            UA_SAAS_SERVICE_APP.widgetContext.orgId = queryParams.get("orgId");
            // UA_SAAS_SERVICE_APP.widgetContext.orgId = "org703141937"; 

            if(UA_SAAS_SERVICE_APP.widgetContext.domain)
            {
                UA_OAUTH_PROCESSOR.currentSAASDomain = UA_SAAS_SERVICE_APP.widgetContext.domain;
            }

            if(queryParams.get("list"))
            {
                UA_SAAS_SERVICE_APP.widgetContext.list = queryParams.get("list");
                UA_SAAS_SERVICE_APP.insertManageListContacts(UA_SAAS_SERVICE_APP.widgetContext.list);
            }
            $("#suo-item-workflow-nav").hide()
            
            UA_SAAS_SERVICE_APP.getCurrentOrgInfo(function(response){
                UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO = response.data;
                appsConfig.APP_UNIQUE_ID = currentUser.uid;
                appsConfig.UA_DESK_ORG_ID = UA_SAAS_SERVICE_APP.widgetContext.orgId;
                if(UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO.recent_campaigns.length > 0) {
                    // appsConfig.UA_DESK_ORG_ID = UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO.recent_campaigns[0].zuid;
                    $("#saasAuthIDName").text(UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO.recent_campaigns[0].campaign_name).attr('title', `Recent Campaigns for Authorized Zoho Account: ${UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO.recent_campaigns[0].campaign_name}`);
                    $('#ac_name_label_saas .ac_name_id').text(`Recent Campaign of Authorized Account: ${UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO.recent_campaigns[0].campaign_name}`);
                }
                else {
                    $('#ac_name_label_saas .ac_name_id').text(`Authorized`);
                }
                UA_SAAS_SERVICE_APP.proceedToAppInitializationIfAPPConfigResolved();
            });
    },
    
    // addSAASUsersToLICUtility: async function(){
    //     var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/users?type=ActiveUsers`, "GET", null);
    //     response.data.users.sort(function(a, b) {
    //         return a.full_name.localeCompare(b.full_name);
    //     });
    //     response.data.users.forEach(item=>{
    //         UA_LIC_UTILITY.addFetchedSAASUser(item.id, item.full_name, item.email, null, item.profile.name === "Administrator", item.phone = null, url = null);
    //     });
    // },
        
    proceedToAppInitializationIfAPPConfigResolved: function(){
        if(appsConfig.APP_UNIQUE_ID && appsConfig.UA_DESK_ORG_ID){
            UA_SAAS_SERVICE_APP.renderInitialElementsForZohoCamp();
            // UA_SAAS_SERVICE_APP.renderInitialElements(UA_SAAS_SERVICE_APP.widgetContext.module, UA_SAAS_SERVICE_APP.widgetContext.entityId);
            UA_APP_UTILITY.appsConfigHasBeenResolved();
        }
    },

    renderInitialElementsForZohoCamp: async function(){
        $(".ssf-new-recip-form").prepend(`<div id="ssf-recip-list-add-holdr" class="ssf-recip-choice-btn" style="margin-top: -5px;background: none;border: none;box-shadow: none;"></div>`);
        UA_SAAS_SERVICE_APP.renderCampaignsList();
        $('#recip-count-holder').text('Please select any contacts list in dropdown!');
    },
    
    renderCampaignsList: async function(){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/getmailinglists?resfmt=JSON`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('renderCampaignsList',response);
        let listDropdownItems = [];
        if(response && response.data && response.data.list_of_details && response.data.list_of_details.length >0) {
            response.data.list_of_details.forEach(item=>{
                UA_SAAS_SERVICE_APP.fetchedList[item.listkey] = item;
                listDropdownItems.push({
                    'label': item.listname,
                    'value': item.listkey
                });
            });
        }
        UA_APP_UTILITY.renderSelectableDropdown('#ssf-recip-list-add-holdr', 'Select a List', listDropdownItems, 'UA_SAAS_SERVICE_APP.insertManageListContacts', false, false);
    
    },

    insertManageListContacts: async function(listkey){

        UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(true,listkey, async function(contactList){
            UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
            UA_SAAS_SERVICE_APP.addRecordsAsRecipients(contactList);
        });
    },

    renderInitialElements: async function(selectContacts){
        if(!UA_SAAS_SERVICE_APP.widgetContext.module){
            UA_SAAS_SERVICE_APP.entityDetailFetched(null);
            console.log('Module does not exist, rendering skipped');
            $("#recip-count-holder").text("Add recipients to proceed");
            return;
        }
        UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(false,selectContacts,async function(contactList){
            UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
            UA_SAAS_SERVICE_APP.addRecordsAsRecipients(contactList);
        });
    },

    sendPhoneFieldsForRecipientType: function(fieldsArray){
        fieldsArray.forEach(item=>{
            if(item.data_type === "phone"){
                UA_APP_UTILITY.addRecipientPhoneFieldType(item.api_name, item.display_label);
            }
        });
    },
    
    populateModuleItemFieldsInDropDown: async function (module, target, fieldsCallback=(()=>{})){
        if(!module || UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.includes(module) == true){
            return;
        }
        UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.push(module);
        var moduleToLoad = module === "users" ? UA_SAAS_SERVICE_APP.CURRENT_USER_INFO : UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS)[0]];
        var moduleFields = Object.keys(moduleToLoad);
        var fieldsArray = [];
        var dropDownValues = [];
        moduleFields.forEach(item=>{
            var itemVal = moduleToLoad[item];
            if(typeof itemVal !== "object" && itemVal !== null && typeof itemVal!=="boolean"){
                fieldsArray.push({
                    "api_name": item,
                    "display_label": item
                });
                dropDownValues.push({
                    "label": item,
                    "value": (module.toUpperCase() === "USERS" ? "CURRENT_USER": module.toUpperCase())+'.'+item
                });
            }
        });
        fieldsCallback(fieldsArray);
        if(typeof target === "string"){
            target = [target];
        }
        target.forEach(item=>{
            UA_APP_UTILITY.renderSelectableDropdown(item, `Insert ${module} fields`, dropDownValues, 'UA_APP_UTILITY.templateMergeFieldSelected', false, true);
        });
        return true;
    },
    
    getCurrentOrgInfo : async function(callback){
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/recentcampaigns?resfmt=JSON', callback);
    },
    
    getAPIResponseAndCallback: async function(url, callback){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}${url}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        callback(response);
    },
    
    showErrorIfInAPICall : function(response){
        if((response.code && response.code === 401) || (response.status && response.status === 401) || (response.data.status == "error" && response.data.Code && response.data.Code == "1007") || (response.data.status == "error" && response.data.Code && response.data.Code == "401")){
            UA_OAUTH_PROCESSOR.showReAuthorizationError(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED);
        }
    },
    
    initiateAuthFlow: function(){
        UA_OAUTH_PROCESSOR.initiateAuth(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED);
    },
    
    fetchStoreExecuteOnRecordDetails: async function(isKey,value, callback){

        $("#recip-count-holder").text(`fetching list contacts...`);
        let contactsList = [];
        if(isKey){
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/getlistsubscribers?resfmt=JSON&listkey=${value}`, "GET", null);
            UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
            console.log('fetchStoreExecuteOnRecordDetails',response);
            if(response.data && response.data.list_of_details){
                contactsList = response.data.list_of_details;
            }
        }
        else{
            contactsList = value;
        }
        if(contactsList.length > 0){
            contactsList.forEach((recordItem,index)=>{
                let id = isKey==true?value.substring(value.length-10,value.length)+"ULGEBRA"+index : "selectContact" +"-"+index;
                recordItem["id"] = id;
                UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id] = recordItem;
            });
        }
        $("#recip-count-holder").text(`Selected ${Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS).length} Contacts`);
        
        callback(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
        UA_SAAS_SERVICE_APP.entityDetailFetched();
    },
    
    addRecordsAsRecipients: function(contactsList){
        for(var item in contactsList)
        {
            if($(`[data-recip-id=${contactsList[item]['id']}]`).length == 0)
            {
                if(contactsList[item]['phone']){
                    UA_APP_UTILITY.addRecipientPhoneFieldType("phone", "phone");
                }
                if(!contactsList[item].name)
                {
                    contactsList[item].name = "";
                    if(contactsList[item].firstname){
                        contactsList[item].name = contactsList[item].name +" "+ contactsList[item].firstname;
                    }
                    if(contactsList[item].lastname){
                        contactsList[item].name = contactsList[item].name +" "+ contactsList[item].lastname;
                    }
                    contactsList[item].Full_Name = contactsList[item].name;
                }
                UA_APP_UTILITY.addInStoredRecipientInventory(contactsList[item]); 
            }       
        }
    },
    
    addSentSMSAsRecordInHistory: async function(sentMessages){
        // var historyDataArray = [];
        // for(var sentMessageId in sentMessages){
        //     var sentMessage = sentMessages[sentMessageId];
        //     var recModule = sentMessage.module; /*sentMessage.moduleId.split("_")[0];*/
        //     var recId = sentMessage.moduleId /*sentMessage.moduleId.split("_")[1];*/
        //     historyDataArray = {
        //         "comment":{
        //             "body":"I like comments\nAnd I like posting them *RESTfully*.",
        //             "author":"Pinnacle",
        //             "email":currentUser.email
        //         }
        //         // fields: {
        //         //     "ENTITY_ID": recId,
        //         //     "ENTITY_TYPE": recModule,
        //         //     "COMMENT": `${sentMessage.tpa?sentMessage.tpa:"SMS"} - ${sentMessage.channel} message status: ${sentMessage.status} from ${sentMessage.from} to ${sentMessage.to}  
                                
        //         //                 Platform  : ${sentMessage.channel}
                                
        //         //                 Sender    : ${sentMessage.from}
                                
        //         //                 Receiver  : ${sentMessage.to}
                                
        //         //                 Direction : Outbound
                                
        //         //                 Status    :  ${sentMessage.status}
                                
        //         //                 Message   : ${sentMessage.message}`
        //         // }  
        //     };
        // }
        // var credAdProcess3 = curId++;
        // showProcess(`Adding ${Object.keys(sentMessages).length} messages to history...`, credAdProcess3);
        // var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/admin/api/2022-04/comments.json`, "POST", historyDataArray);
        // processCompleted(credAdProcess3);
        // UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        // console.log('addSentSMSAsRecordInHistory',response);
        // //alert('All Messages has been added to Pinnacle SMS History');
        
    },
    entityDetailFetched: function(data){
        setTimeout(()=>{UA_APP_UTILITY.fetchedAllModuleRecords();}, 2000);
        if(typeof UA_TPA_FEATURES.callOnEntityDetailFetched === "function"){
            UA_TPA_FEATURES.callOnEntityDetailFetched(data);
        }
    }

};