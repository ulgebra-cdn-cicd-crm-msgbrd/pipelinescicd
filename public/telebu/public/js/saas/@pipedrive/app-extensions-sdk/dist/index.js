"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
let exports = {};
Object.defineProperty(exports, "__esModule", { value: true });
function detectIframeFocus(cb) {
    let isFocused = false;
    window === null || window === void 0 ? void 0 : window.addEventListener('focus', () => {
        if (!isFocused) {
            cb();
        }
        isFocused = true;
    });
    window === null || window === void 0 ? void 0 : window.addEventListener('blur', () => {
        isFocused = false;
    });
}
function detectIdentifier() {
    const params = new URLSearchParams(window.location.search);
    return params.get('id');
}
var Command;
(function (Command) {
    Command["SHOW_SNACKBAR"] = "show_snackbar";
    Command["SHOW_CONFIRMATION"] = "show_confirmation";
    Command["RESIZE"] = "resize";
    Command["INITIALIZE"] = "initialize";
    Command["OPEN_MODAL"] = "open_modal";
    Command["CLOSE_MODAL"] = "close_modal";
    Command["GET_SIGNED_TOKEN"] = "get_signed_token";
})(Command = exports.Command || (exports.Command = {}));
var Event;
(function (Event) {
    Event["VISIBILITY"] = "visibility";
    Event["CLOSE_CUSTOM_MODAL"] = "close_custom_modal";
})(Event = exports.Event || (exports.Event = {}));
var MessageType;
(function (MessageType) {
    MessageType["COMMAND"] = "command";
    MessageType["LISTENER"] = "listener";
    MessageType["TRACK"] = "track";
})(MessageType = exports.MessageType || (exports.MessageType = {}));
var Color;
(function (Color) {
    Color["PRIMARY"] = "primary";
    Color["SECONDARY"] = "secondary";
    Color["NEGATIVE"] = "negative";
})(Color = exports.Color || (exports.Color = {}));
var Modal;
(function (Modal) {
    Modal["DEAL"] = "deal";
    Modal["ORGANIZATION"] = "organization";
    Modal["PERSON"] = "person";
    Modal["JSON_MODAL"] = "json_modal";
    Modal["CUSTOM_MODAL"] = "custom_modal";
})(Modal = exports.Modal || (exports.Modal = {}));
var ModalStatus;
(function (ModalStatus) {
    ModalStatus["CLOSED"] = "closed";
    ModalStatus["SUBMITTED"] = "submitted";
})(ModalStatus = exports.ModalStatus || (exports.ModalStatus = {}));
var TrackingEvent;
(function (TrackingEvent) {
    TrackingEvent["FOCUSED"] = "focused";
})(TrackingEvent = exports.TrackingEvent || (exports.TrackingEvent = {}));

const commandKeys = Object.values(Command);
const eventKeys = Object.values(Event);

class AppExtensionsSDK {
    constructor(options = {}) {
        const { identifier, targetWindow } = options;
        this.initialized = false;
        this.window = targetWindow !== null && targetWindow !== void 0 ? targetWindow : window.parent;
        this.identifier = identifier !== null && identifier !== void 0 ? identifier : (0, detectIdentifier)();
        if (!this.identifier) {
            throw new Error('Missing custom UI identifier');
        }
        (0, detectIframeFocus)(() => {
            this.track(TrackingEvent.FOCUSED);
        });
    }
    postMessage(payload, targetOrigin = '*') {
        return new Promise((resolve, reject) => {
            const channel = new MessageChannel();
            const message = {
                payload,
                id: this.identifier,
            };
            channel.port1.onmessage = ({ data: response }) => {
                channel.port1.close();
                const { error, data } = response;
                if (error) {
                    reject(new Error(error));
                }
                else {
                    resolve(data);
                }
            };
            this.window.postMessage(message, targetOrigin, [channel.port2]);
        });
    }
    execute(command, ...args) {
        if (!this.initialized) {
            throw new Error('SDK is not initialized');
        }
        if (!commandKeys.includes(command)) {
            throw new Error('Invalid command');
        }
        return this.postMessage({
            command,
            args: args[0],
            type: MessageType.COMMAND,
        });
    }
    track(event, targetOrigin = '*') {
        const message = {
            payload: {
                type: MessageType.TRACK,
                event,
            },
            id: this.identifier,
        };
        this.window.postMessage(message, targetOrigin);
    }
    listen(event, onEventReceived) {
        if (!eventKeys.includes(event)) {
            throw new Error('Invalid event');
        }
        const channel = new MessageChannel();
        const message = {
            payload: {
                type: MessageType.LISTENER,
                event,
            },
            id: this.identifier,
        };
        channel.port1.onmessage = ({ data }) => {
            if (data.error) {
                channel.port1.close();
            }
            onEventReceived(data);
        };
        this.window.postMessage(message, '*', [channel.port2]);
        return () => {
            channel.port1.close();
        };
    }
    setWindow(window) {
        this.window = window;
    }
    initialize(options = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.postMessage({
                command: Command.INITIALIZE,
                args: options,
                type: MessageType.COMMAND,
            });
            this.initialized = true;
            return this;
        });
    }
}

