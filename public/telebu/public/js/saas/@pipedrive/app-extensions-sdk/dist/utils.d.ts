declare type Callback = () => void;
export declare function detectIframeFocus(cb: Callback): void;
export declare function detectIdentifier(): string;
export {};
