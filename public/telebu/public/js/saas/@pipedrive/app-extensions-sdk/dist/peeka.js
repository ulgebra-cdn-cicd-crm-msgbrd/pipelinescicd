(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageType = exports.TrackingEvent = exports.Color = exports.ModalStatus = exports.Modal = exports.Event = exports.Command = void 0;
const utils_1 = require("./utils");
const types_1 = require("./types");
const commandKeys = Object.values(types_1.Command);
const eventKeys = Object.values(types_1.Event);
class AppExtensionsSDK {
    constructor(options = {}) {
        const { identifier, targetWindow } = options;
        this.initialized = false;
        this.window = targetWindow !== null && targetWindow !== void 0 ? targetWindow : window.parent;
        this.identifier = identifier !== null && identifier !== void 0 ? identifier : (0, utils_1.detectIdentifier)();
        if (!this.identifier) {
            throw new Error('Missing custom UI identifier');
        }
        (0, utils_1.detectIframeFocus)(() => {
            this.track(types_1.TrackingEvent.FOCUSED);
        });
    }
    postMessage(payload, targetOrigin = '*') {
        return new Promise((resolve, reject) => {
            const channel = new MessageChannel();
            const message = {
                payload,
                id: this.identifier,
            };
            channel.port1.onmessage = ({ data: response }) => {
                channel.port1.close();
                const { error, data } = response;
                if (error) {
                    reject(new Error(error));
                }
                else {
                    resolve(data);
                }
            };
            this.window.postMessage(message, targetOrigin, [channel.port2]);
        });
    }
    execute(command, ...args) {
        if (!this.initialized) {
            throw new Error('SDK is not initialized');
        }
        if (!commandKeys.includes(command)) {
            throw new Error('Invalid command');
        }
        return this.postMessage({
            command,
            args: args[0],
            type: types_1.MessageType.COMMAND,
        });
    }
    track(event, targetOrigin = '*') {
        const message = {
            payload: {
                type: types_1.MessageType.TRACK,
                event,
            },
            id: this.identifier,
        };
        this.window.postMessage(message, targetOrigin);
    }
    listen(event, onEventReceived) {
        if (!eventKeys.includes(event)) {
            throw new Error('Invalid event');
        }
        const channel = new MessageChannel();
        const message = {
            payload: {
                type: types_1.MessageType.LISTENER,
                event,
            },
            id: this.identifier,
        };
        channel.port1.onmessage = ({ data }) => {
            if (data.error) {
                channel.port1.close();
            }
            onEventReceived(data);
        };
        this.window.postMessage(message, '*', [channel.port2]);
        return () => {
            channel.port1.close();
        };
    }
    setWindow(window) {
        this.window = window;
    }
    initialize(options = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.postMessage({
                command: types_1.Command.INITIALIZE,
                args: options,
                type: types_1.MessageType.COMMAND,
            });
            this.initialized = true;
            return this;
        });
    }
}
var types_2 = require("./types");
Object.defineProperty(exports, "Command", { enumerable: true, get: function () { return types_2.Command; } });
Object.defineProperty(exports, "Event", { enumerable: true, get: function () { return types_2.Event; } });
Object.defineProperty(exports, "Modal", { enumerable: true, get: function () { return types_2.Modal; } });
Object.defineProperty(exports, "ModalStatus", { enumerable: true, get: function () { return types_2.ModalStatus; } });
Object.defineProperty(exports, "Color", { enumerable: true, get: function () { return types_2.Color; } });
Object.defineProperty(exports, "TrackingEvent", { enumerable: true, get: function () { return types_2.TrackingEvent; } });
Object.defineProperty(exports, "MessageType", { enumerable: true, get: function () { return types_2.MessageType; } });
exports.default = AppExtensionsSDK;

},{"./types":2,"./utils":3}],2:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrackingEvent = exports.ModalStatus = exports.Modal = exports.Color = exports.MessageType = exports.Event = exports.Command = void 0;
var Command;
(function (Command) {
    Command["SHOW_SNACKBAR"] = "show_snackbar";
    Command["SHOW_CONFIRMATION"] = "show_confirmation";
    Command["RESIZE"] = "resize";
    Command["INITIALIZE"] = "initialize";
    Command["OPEN_MODAL"] = "open_modal";
    Command["CLOSE_MODAL"] = "close_modal";
    Command["GET_SIGNED_TOKEN"] = "get_signed_token";
})(Command = exports.Command || (exports.Command = {}));
var Event;
(function (Event) {
    Event["VISIBILITY"] = "visibility";
    Event["CLOSE_CUSTOM_MODAL"] = "close_custom_modal";
})(Event = exports.Event || (exports.Event = {}));
var MessageType;
(function (MessageType) {
    MessageType["COMMAND"] = "command";
    MessageType["LISTENER"] = "listener";
    MessageType["TRACK"] = "track";
})(MessageType = exports.MessageType || (exports.MessageType = {}));
var Color;
(function (Color) {
    Color["PRIMARY"] = "primary";
    Color["SECONDARY"] = "secondary";
    Color["NEGATIVE"] = "negative";
})(Color = exports.Color || (exports.Color = {}));
var Modal;
(function (Modal) {
    Modal["DEAL"] = "deal";
    Modal["ORGANIZATION"] = "organization";
    Modal["PERSON"] = "person";
    Modal["JSON_MODAL"] = "json_modal";
    Modal["CUSTOM_MODAL"] = "custom_modal";
})(Modal = exports.Modal || (exports.Modal = {}));
var ModalStatus;
(function (ModalStatus) {
    ModalStatus["CLOSED"] = "closed";
    ModalStatus["SUBMITTED"] = "submitted";
})(ModalStatus = exports.ModalStatus || (exports.ModalStatus = {}));
var TrackingEvent;
(function (TrackingEvent) {
    TrackingEvent["FOCUSED"] = "focused";
})(TrackingEvent = exports.TrackingEvent || (exports.TrackingEvent = {}));

},{}],3:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.detectIdentifier = exports.detectIframeFocus = void 0;
function detectIframeFocus(cb) {
    let isFocused = false;
    window === null || window === void 0 ? void 0 : window.addEventListener('focus', () => {
        if (!isFocused) {
            cb();
        }
        isFocused = true;
    });
    window === null || window === void 0 ? void 0 : window.addEventListener('blur', () => {
        isFocused = false;
    });
}
exports.detectIframeFocus = detectIframeFocus;
function detectIdentifier() {
    const params = new URLSearchParams(window.location.search);
    return params.get('id');
}
exports.detectIdentifier = detectIdentifier;

},{}]},{},[1]);
