var UA_SAAS_SERVICE_APP = {

    CURRENT_USER_INFO: null,
    CURRENT_ORG_INFO: null,
    CURRENT_OAUTH_SCOPE_NEEDED: [],
    FETCHED_RECORD_DETAILS: {},
    CURRENT_MODULE_FIELDS_DROPDOWN: [],
    SELECTED_RECEIPS: {},
    SEARCH_MODULE_RECORD_MAP: {},
    fetchedList: {},
    widgetContext: { 'module': 'WEBUI', 'entityId': 'webui' },
    DEALS_CONTACTS_LIST: {},
    APP_API_NAME: {},
    supportedIncomingModules: [],
    fetchedSaas: {},
    saasDropdownID: null,
    initiateAPPFlow: async function () {

        UA_SAAS_SERVICE_APP.CURRENT_USER_INFO = { 'id': currentUser.uid, 'full_name': currentUser.displayName, 'email': currentUser.email };
        UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO = { 'id': currentUser.uid };
        $("#ac_name_label_saas").html(`<span class="material-icons">restart_alt</span><span class="anl_servicename">Authorized Web UI</span><span class="ac_name_id">${UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.email}</span>`).css({ 'cursor': 'default' });
        $("#recip-count-holder").text(`Selected ${'0'} ${'numbers'}`);
        $('#suo-item-incoming-nav').remove();
        $('#suo-item-fieldmapping-nav').parent().remove();
        $('#suo-item-workflow-nav').remove();

        let authorizedSaasDoc = await db.collection("ulgebraUsers").doc(currentUser.uid).collection("extensions").get();
        let authorizedSaasObj = [];
        authorizedSaasDoc.docs.forEach(function (doc, key) {
            if (doc.id.startsWith(extensionName.split('for')[0])) {
                authorizedSaasObj.push(doc.id);
            }
        });
        console.log(authorizedSaasObj);
        if (authorizedSaasObj.length > 1 || (authorizedSaasObj.length === 1 && authorizedSaasObj.indexOf(extensionName) > -1)) {
            await this.addSaasList(authorizedSaasObj);
        }
        else {
            authorizedSaasObj.remove_by_value(extensionName);
            authorizedSaasObj.push(extensionName);
            await this.addSaasList(authorizedSaasObj);
        }

        $('#authorizedSaas .dropdownDisplayText').text(extensionName);

    },

    addSaasList: async function (authorizedSaasObj) {

        let uaWebSearchLink = new URLSearchParams(decodeURIComponent(window.location.search));
        let chooseSaasOptionSubDiv = "";
        authorizedSaasObj.forEach(async function (val, key) {

            let id = (key + 1) + '';
            uaWebSearchLink.set('appCode', val);
            let saasObj = {
                'label': val,
                'value': id,
                'appLink': window.location.origin + window.location.pathname + '?' + decodeURIComponent(uaWebSearchLink.toString()),
                'selected': false //(id === localStorage.getItem("choseDefaultUASavedPreferenceItem_Saas"))
            };
            if (val === extensionName) {
                saasObj['appLink'] = '';
                saasObj['selected'] = false;
            }
            UA_SAAS_SERVICE_APP.fetchedSaas[id] = saasObj;
            chooseSaasOptionSubDiv = chooseSaasOptionSubDiv + `<div onclick="window.location.href = '${saasObj.appLink}';" style="display: inline-block;margin: 0px 5px 5px 0px; background-color: white; padding: 20px 20px; box-sizing: border-box; text-align: center; position: relative; border-radius: 3px; box-shadow: 0px 1px 3px rgb(0 0 0 / 30%); cursor: pointer;">${val}</div>`;

        });

        showErroWindow("Authorized SAAS!", `<div style="display: inline-block;">${chooseSaasOptionSubDiv}</div>`, false, false, 500);

        if (!($('#sms-form-item-item-sender #authorizedSaas').is(':visible'))) {
            $('#sms-form-item-item-sender').prepend(`<div class="ssf-fitem-label">
                                                            <span class="material-icons ssf-fitem-ttl-icon" title="Saas">business</span>
                                                            <span class="ssf-fitem-ttl-label">Saas</span> 
                                                        </div>
                                                        <div id="authorizedSaas" style=" height: 40px;padding-bottom: 10px; display: flex; align-items: center; "></div>`);
        }

        let selectedDefaultSaas = [];
        // selectedSaas = [{
        //                 'ddActionMapType': UA_APP_UTILITY.ddActionMap.select_by_default,
        //                 'callback': 'UA_SAAS_SERVICE_APP.choseDefaultUASavedPreferenceItem_Saas'
        //             }];
        let saasList = Object.values(UA_SAAS_SERVICE_APP.fetchedSaas);
        UA_SAAS_SERVICE_APP.saasDropdownID = UA_APP_UTILITY.renderSelectableDropdown('#authorizedSaas', 'Choose Saas', saasList, `UA_SAAS_SERVICE_APP.newUrlToLoad`, false, false, UA_SAAS_SERVICE_APP.saasDropdownID, selectedDefaultSaas);

    },

    newUrlToLoad: function (value) {
        if (UA_SAAS_SERVICE_APP.fetchedSaas[value].appLink) {
            window.location.href = UA_APP_UTILITY.fetchedSaas[value].appLink;
        }
    },

    choseDefaultUASavedPreferenceItem_Saas: function (val) {
        if (localStorage.getItem('choseDefaultUASavedPreferenceItem_Saas') === val) {
            localStorage.removeItem('choseDefaultUASavedPreferenceItem_Saas');
        }
        else {
            localStorage.setItem('choseDefaultUASavedPreferenceItem_Saas', val);
        }
        window.location.reload();
    },

    initiateAuthFlow: function () {
        // UA_OAUTH_PROCESSOR.initiateAuth(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED);
    },

    addSentSMSAsRecordInHistory: async function (sentMessages) {

        var credAdProcess3 = curId++;
        processCompleted(credAdProcess3);

    }

};