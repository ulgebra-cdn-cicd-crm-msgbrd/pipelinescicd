const UA_OAUTH_PROCESSOR = {
     domainExtnMap: {
        "IN": ".in",
        "EU": ".eu",
        "CN": ".com.cn",
        "AU": ".com.au",
        "US": ".com"
    },
    currentSAASDomain: null,
    initiateAuth: function (scope) {
        var scopeText = typeof scope === "object" ? scope.join(',') : scope;
//        $('.ua_service_login_btn').text('Processing...');
        if(!extensionName){
            showErroWindow('Application error!', 'Unable to load app type. Kindly <a target="_blank" href="https://apps.ulgebra.com/contact">contact developer</a>');
            return;
        }
        window.open(`https://login.salesforce.com/services/oauth2/authorize?response_type=code&client_id=3MVG9fe4g9fhX0E4uq40a93_a6DU9vpAoyQlEjOJYavAfv_hwVSkMYbJVmCtXKcl3KElHh_8phLy21fJJpSbO&scope=${scopeText}&redirect_uri=${APP_ENV_CLIENT_URL}/oauth-home&state=${currentUser.uid}:::${extensionName}&access_type=offline&prompt=consent`, 'Authorize', 'popup');
    },
    
    getAPIResponse: async function (url, method, data, headers = null) {
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        if(!headers){
            headers = {};
            headers["Content-Type"] = "application/json";
        }
        headers.Authorization = "Bearer {{AUTHTOKEN}}";
        headers._uaClientRequestID = new Date().getTime()+"-"+Math.random(100000,99999);
        var returnResponse = await UA_APP_UTILITY.makeUAServiceAuthorizedHTTPCall(url, method, data, headers);
        removeTopProgressBar(credAdProcess3);
        return returnResponse;
    },
    
    showReAuthorizationError: function(scope= [], serviceName = "SalesForce"){
        if($('#UA_SAAS_AUTH_BTN').length > 0){
            console.log('SaaS auth window already showing.. skipping.');
            return;
        }
       showErroWindow("SalesForce Authentication Needed!", `
        You need to authorize your ${serviceName} Account to proceed
        <div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;display:none"> 
            <div style="font-size: 18px;margin-bottom: 5px;margin-top: 20px;">Your ${serviceName} Domain</div>
            <input id="inp_saas_api_domain" value="${UA_OAUTH_PROCESSOR.currentSAASDomain}" type="text" placeholder="Enter ${serviceName} domain" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
            </div></div>
             <br><br> <button id="UA_SAAS_AUTH_BTN" class="ua_service_login_btn ua_primary_action_btn" onclick="UA_OAUTH_PROCESSOR.initiateAuth('${scope.join(',')}')">Authorize Now</button>`, false, false, 1000);
    }
    
};