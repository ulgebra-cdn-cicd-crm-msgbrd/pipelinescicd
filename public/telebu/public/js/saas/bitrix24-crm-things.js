var UA_SAAS_SERVICE_APP = {
    
    CURRENT_USER_INFO : null,
    CURRENT_ORG_INFO : null,
    CURRENT_MODULE_FIELDS_DROPDOWN : [],
    CURRENT_OAUTH_SCOPE_NEEDED : ["crm","task","im","user_brief","user_basic","user.userfield","lists","messageservice","user","log","placement"],
    FETCHED_RECORD_DETAILS: {},
    widgetContext: {},
    APP_API_NAME: "pinnaclesms__",
    SELECTED_RECEIPS: {},
    MODULE_FIELDS_MAP: {},
    SEARCH_RECORD_MODULE : "contact",
    SEARCH_MODULE_RECORD_MAP: {},
    isEventApp: ["calendlyforbitrix24","acuityforbitrix24"],
    supportedIncomingModules: [
        {
            "selected": true,
            "value": "lead",
            "label": "Leads"
        },
        {
            "value": "contact",
            "label": "Contacts"
        },
        {
            "value": "deal",
            "label": "Deals"
        }
    ],
    
    getAPIBaseUrl : function(){
        return UA_SAAS_SERVICE_APP.widgetContext.bitrix24_domain?`https://${UA_SAAS_SERVICE_APP.widgetContext.bitrix24_domain}`:"";
    },
    
    provideSuggestedCurrentEnvLoginEmailID: async function(){
        await BX24.callMethod('user.current', {}, function(response){
            console.log(response);
            if(response && response.data() && response.data().EMAIL) {
                UA_APP_UTILITY.receivedSuggestedCurrentEnvLoginEmailID(response.data().EMAIL);
            }
        });
        
    },

    initiateAPPFlow: async function(){
        $("#ac_name_label_saas .anl_servicename").text('Bitrix24');

            if(queryParams.get("action") && queryParams.get("action") == "sendsms")
            {
                let module = queryParams.get("module");
                module = module.split("_")[1].toLowerCase();

                UA_SAAS_SERVICE_APP.widgetContext.module = module;
                UA_SAAS_SERVICE_APP.widgetContext.entityId = queryParams.get("entityId");
            }
            UA_SAAS_SERVICE_APP.widgetContext.bitrix24_domain = queryParams.get("domain")?queryParams.get("domain"):queryParams.get("DOMAIN");

            UA_SAAS_SERVICE_APP.checkPlacementBind();

            UA_SAAS_SERVICE_APP.initiateAPPFlow_SA(true);

           
    },

    initiateAPPFlow_SA: async function(isSMS = false){
        if(!UA_SAAS_SERVICE_APP.widgetContext.module && queryParams.get("module")){
            UA_SAAS_SERVICE_APP.widgetContext.module = queryParams.get("module");
            UA_SAAS_SERVICE_APP.widgetContext.entityId = queryParams.get("entityId");
            UA_SAAS_SERVICE_APP.injectShowSearchContact(); 
        }
        if(!isSMS || queryParams.get("disableSAASSdk")){
            try{
                $('[onclick="UA_APP_UTILITY.showWorkFlowInstructionDialog()"]').attr('onclick',"UA_SAAS_SERVICE_APP.showWorkFlowInstructionDialog()");
            }
            catch(ex){
                console.log(ex);
            }
        }
        
        $("#ac_name_label_saas .anl_servicename").text('Bitrix24 CRM');
        UA_SAAS_SERVICE_APP.getCurrentUserInfo(function(response){
            UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
            if(response.error && response.error.error){
                showErroWindow('Error : ', response.error.error + " - " + response.error.error_description);
            }
            if(response && response.api_domain){
                UA_SAAS_SERVICE_APP.widgetContext.bitrix24_domain = response.api_domain;
            }
            UA_SAAS_SERVICE_APP.CURRENT_USER_INFO = response.data.result;
            appsConfig.APP_UNIQUE_ID = UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.ID;
            appsConfig.UA_DESK_ORG_ID = UA_SAAS_SERVICE_APP.widgetContext.bitrix24_domain;
            $("#saasAuthIDName").text(appsConfig.UA_DESK_ORG_ID).attr('title', `Authorized Bitrix24 Account: ${UA_SAAS_SERVICE_APP.widgetContext.bitrix24_domain}`);
            setTimeout((function(){
                $('#ac_name_label_saas .ac_name_id').text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.EMAIL?UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.EMAIL:UA_SAAS_SERVICE_APP.widgetContext.bitrix24_domain);
            }), 2000);
            UA_SAAS_SERVICE_APP.proceedToAppInitializationIfAPPConfigResolved();
            UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown('users', ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder']);
        });
    },
    
    addSAASUsersToLICUtility: async function(){
        $('.saasServiceName').text("Bitrix24 CRM");
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/rest/user.get.json?filter[ACTIVE]=true`, "GET", null);
        response.data.result.sort(function(a, b) {
            return a.NAME.localeCompare(b.NAME);
        });
        response.data.result.forEach(item=>{
            UA_LIC_UTILITY.addFetchedSAASUser(item.ID, item.NAME, item.EMAIL, null, item.USER_TYPE === "extranet", item.PERSONAL_PHONE, url = null);
        });
        UA_LIC_UTILITY.saasUserListFetchCompleted();
    },
        
    proceedToAppInitializationIfAPPConfigResolved: function(){
        if(appsConfig.APP_UNIQUE_ID && appsConfig.UA_DESK_ORG_ID){
            UA_SAAS_SERVICE_APP.renderInitialElements(UA_SAAS_SERVICE_APP.widgetContext.module, UA_SAAS_SERVICE_APP.widgetContext.entityId);
            UA_SAAS_SERVICE_APP.addSAASUsersToLICUtility();
            UA_APP_UTILITY.appsConfigHasBeenResolved();
        }
    },
    
    renderInitialElements: async function(module, entityId){
        $("#peanea-main-new-meeting-iframe").css("margin","0px")
        if(!module){
            UA_SAAS_SERVICE_APP.entityDetailFetched(null);
            console.log('Module does not exist, rendering skipped');
            $("#recip-count-holder").text("Add recipients to proceed");
            return;
        }
//        UA_APP_UTILITY.renderSavedTemplatesInDropdowns();
        UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(module, entityId, function(contactList){
            UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
            UA_SAAS_SERVICE_APP.addRecordsAsRecipients(contactList);
        });
    },
    
    sendPhoneFieldsForRecipientType: function(fieldsArray){
        fieldsArray.forEach(item=>{
            if(item.data_type === "phone"){
                UA_APP_UTILITY.addRecipientPhoneFieldType(item.api_name, item.display_label);
            }
        });
    },
    
    populateModuleItemFieldsInDropDown: async function (module, target, fieldsCallback=(()=>{})){
        if(!module || UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.includes(module) == true){
            return;
        }
        UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.push(module);
        var moduleToLoad = module === "users" ? UA_SAAS_SERVICE_APP.CURRENT_USER_INFO : UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[UA_SAAS_SERVICE_APP.widgetContext.entityId];
        var moduleFields = Object.keys(moduleToLoad);
        var fieldsArray = [];
        var dropDownValues = [];
        moduleFields.forEach(item=>{
            var itemVal = moduleToLoad[item];
            if(typeof itemVal !== "object" && itemVal !== null && typeof itemVal!=="boolean"){
                fieldsArray.push({
                    "api_name": item,
                    "display_label": item
                });
                dropDownValues.push({
                    "label": item,
                    "value": (module.toUpperCase() === "USERS" ? "CURRENT_USER": module.toUpperCase())+'.'+item
                });
            }
        });
        fieldsCallback(fieldsArray);
        if(typeof target === "string"){
            target = [target];
        }
        target.forEach(item=>{
            UA_APP_UTILITY.renderSelectableDropdown(item, `Insert ${module} fields`, dropDownValues, 'UA_APP_UTILITY.templateMergeFieldSelected', false, true);
        });
        return true;
    },
    
    injectShowSearchContact :async function(){
        $(".ssf-new-recip-form").prepend(`<div id="searchAndAddFromContactBtn" class="ssf-recip-choice-btn" onclick="$('.item-list-popup').show()">
                                            <i class="material-icons">search</i> Search from ${UA_SAAS_SERVICE_APP.widgetContext.module}
                                      </div>`);
        $('.pageContentHolder').append(`<div class="item-list-popup" style="display:none">
                        <div class="item-list-popup-title">
                            Select a contact <div class="pop-win-close" onclick="$('.item-list-popup').hide();">x</div>
                        </div>
                        <div class="item-search-box">
                            <input onkeyup="proceedForSearchIfEnter()" class="input-form" id="contact-seach-name" type="text" autocomplete="off" placeholder="Name of the contact"/><br>
                            <div class="btn-save" onclick="UA_SAAS_SERVICE_APP.searchRecordsAndRender($('#contact-seach-name').val(),UA_SAAS_SERVICE_APP.widgetContext.module)">
                                Search ${UA_SAAS_SERVICE_APP.widgetContext.module}
                            </div>
                            <div class="btn-reset" onclick="UA_SAAS_SERVICE_APP.resetsearchContacts()">
                                Reset
                            </div>
                        </div>
                        <div class="item-list-popup-content" id="contact-search-items">

                        </div>
                    </div>`);
    },

    resetsearchContacts: function(){
        $("#contact-seach-name").val("");
        $("#contact-search-items").html("");
    },

    searchRecordsAndRender: async function(searchTerm,module){
        if(!valueExists(searchTerm)){
            return;
        }
        await UA_SAAS_SERVICE_APP.getModuleFields(module,async function(res){
            UA_SAAS_SERVICE_APP.MODULE_FIELDS_MAP[module] = res.data.result;
            let contactModuleFieldsList = Object.keys(UA_SAAS_SERVICE_APP.MODULE_FIELDS_MAP[module]);
            let fieldsParams = $.param( {select:contactModuleFieldsList}, true ).replaceAll('select',"select[]");

            await UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/rest/crm.'+module+'.list?'+`${module =='contact'?'filter[NAME]=':'filter[TITLE]='}`+searchTerm+"&"+fieldsParams , function(response){
                console.log(response);
                $("#contact-search-items").html(" ");
                if(response.data["result"].length>0)
                {
                    var search_items = response.data["result"];
                    if(search_items && search_items.length > 50){
                        search_items.splice(0,49);
                    }
                    UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module] = {};
                    search_items.forEach((obj)=>{
                        UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][obj.ID] = obj;
                        if(module != 'contact')
                        {
                            var fullName = obj.TITLE ? obj.TITLE:"";
                        }
                        else
                        {
                            var fullName = obj.NAME ? obj.NAME:"";
                            if(obj.LAST_NAME){
                                fullName = fullName+" "+ obj.LAST_NAME;
                            }
                        }
                        if((module != "deal" && obj.HAS_PHONE == "Y") || (module == "deal" && obj.CONTACT_ID) || (module != "deal" && obj.CONTACT_ID))
                        {
                            let itemHTML = `<div class="item-list-popup-item"><span class="c-name"> ${fullName} </span>`;
                            if(module != "deal" && obj.HAS_PHONE == "Y")
                            {
                                obj.PHONE.forEach((item)=>{
                                    itemHTML =  itemHTML + `${valueExists(item.VALUE) ? '<span class="c-phone '+(UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[obj.ID+'-'+item.VALUE] ? 'alreadyadded': '')+'" onclick="UA_SAAS_SERVICE_APP.selectPhoneNumber(\''+obj.ID+'\', \''+item.VALUE+'\', \''+fullName+'\',\''+module+'\',\''+true+'\')"> <i class="material-icons">phone_iphone</i>('+ item.VALUE_TYPE.toLocaleLowerCase() +') '+item.VALUE+'</span>' : ''}`;  
                                });
                            }
                            else if(obj.CONTACT_ID)
                            {
                                itemHTML =  itemHTML + `${'<span class="c-phone '+(UA_SAAS_SERVICE_APP.SELECTED_RECEIPS["contact_"+obj.ID] ? 'alreadyadded': '')+'" onclick="UA_SAAS_SERVICE_APP.selectPhoneNumber(\''+obj.ID+'\', \''+"contact"+'\', \''+fullName+'\',\''+module+'\',\''+true+'\')"> <i class="material-icons">phone_iphone</i>( Contact ) </span>'}`;
                            }
                            itemHTML =  itemHTML + `</div>`;
                            $("#contact-search-items").append(itemHTML);
                        }    
                        
                    });
                }
                else
                {
                    $("#contact-search-items").html("No records found. <br><br> <span class=\"c-silver\"> This search will include <br> only contacts having phone numbers</span>");
                    return;
                }
            }).catch(err=>{
                console.log(err);
            });
        });
    },

    selectPhoneNumber: function(id,number,name,module,isSelected){
        var receipNumberID = module + "_" + id;
        if(!UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] && !UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id]){
            UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] = {
                'id': id,
                'name': name,
                'module': module,
                'isSelected': isSelected
            };
            $.extend(UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id],UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID]) ;
            UA_SAAS_SERVICE_APP.renderInitialElements(module,id);
            // UA_SAAS_SERVICE_APP.addRecordsAsRecipients([UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id]]);
        }
        else {
            console.log(module+' already added');
            alert(`This ${module} is already added.`);
            return;
        }
    },

    getCurrentUserInfo : async function(callback){
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/rest/user.current.json', callback);
    },
    
    getUserInfoFromID : async function(userEmail, callback){
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/crm/v3/owners?email='+userEmail, callback);
    },
    
    getCurrentOrgInfo : async function(callback){
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/org', callback);
    },
    
    getAPIResponseAndCallback: async function(url, callback){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}${url}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        callback(response);
    },
    
    showErrorIfInAPICall : function(response){
        if(response.code === 401){
            UA_OAUTH_PROCESSOR.showReAuthorizationError(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED);
        }
    },
    
    initiateAuthFlow: function(){
        // UA_OAUTH_PROCESSOR.initiateAuth(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED);
        UA_OAUTH_PROCESSOR.showReAuthorizationError(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED);
    },
    
    fetchStoreExecuteOnRecordDetails: async function(module, recordIds, callback){
        if(!module){
            return;
        }
        
        $("#recip-count-holder").text(`fetching ${module.toLocaleLowerCase()} information...`);
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/rest/crm.${module}.get?id=${recordIds}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('fetchStoreExecuteOnRecordDetails',response);
        // recordIds = module+'_'+recordIds;
        UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds] = response.data.result;
        UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds]["module"] = module;
        if((module === "contact" || module === "lead") && response.data.result["HAS_PHONE"] === "Y"){
            $("#recip-count-holder").text(`Selected ${recordIds.length} ${module.toLocaleLowerCase()}`);
        }
        else
        {
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds]["PHONE"] = [];
        }
        if((module === "contact" || module === "lead") && response.data.result["HAS_EMAIL"] === "Y"){
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds]["email"] = response.data.result["EMAIL"][0].VALUE;
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds]["EMAIL"] = response.data.result["EMAIL"][0].VALUE;
        }
        if(response.data.result.CONTACT_ID && (module === "deal" || module === "lead"))
        {
            var fetchAssociatedContactResponse = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/rest/crm.contact.get?id=${response.data.result.CONTACT_ID}`, "GET", null);
            UA_SAAS_SERVICE_APP.showErrorIfInAPICall(fetchAssociatedContactResponse);
            if(fetchAssociatedContactResponse.data.result.HAS_PHONE == "Y")
            {
                fetchAssociatedContactResponse.data.result.PHONE.forEach((item,i)=>{
                    fetchAssociatedContactResponse.data.result.PHONE[i].VALUE_TYPE = "CONTACT-" + fetchAssociatedContactResponse.data.result.PHONE[i].VALUE_TYPE;
                });
                for (const [key, value] of Object.entries(fetchAssociatedContactResponse.data.result)) {
                    // console.log(`${key}: ${value}`);
                    UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds][`CONTACT_${key}`] = value;
                }
                if(module !== 'contact')
                {   
                    UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds]['name'] = UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds]['TITLE'];
                }

                UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds]["HAS_PHONE"] = "Y",
                UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds]["PHONE"] = UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds]["PHONE"].concat(fetchAssociatedContactResponse.data.result.PHONE);
            }
            if(UA_SAAS_SERVICE_APP.isEventApp.includes(extensionName) && fetchAssociatedContactResponse.data.result.HAS_EMAIL == "Y") {
                UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds]["EMAIL"] = fetchAssociatedContactResponse.data.result["EMAIL"][0].VALUE;
                if(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds]["HAS_PHONE"] == "Y"){
                    UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds]["MOBILE"] = UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds]["PHONE"][0].VALUE;
                    UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds]["NAME"] = UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds]["name"];
                }
            }
        }
        
        callback(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
        UA_SAAS_SERVICE_APP.entityDetailFetched();
    },
    
    addRecordsAsRecipients: function(contactsList){
        for(var item in contactsList)
        {
            if($(`[data-recip-id=${contactsList[item]['ID']}]`).length === 0)
            {
                if(contactsList[item]['HAS_PHONE'] === 'Y' ){
                    contactsList[item]["PHONE"].forEach(mobileFieldItem => {
                         if (mobileFieldItem.VALUE) {
                             UA_APP_UTILITY.addRecipientPhoneFieldType(mobileFieldItem.VALUE_TYPE, mobileFieldItem.VALUE_TYPE);
                             contactsList[item][mobileFieldItem.VALUE_TYPE] = mobileFieldItem.VALUE;
                         }
                     });
                }
                contactsList[item].id = contactsList[item].ID;
                if(!contactsList[item].name)
                {
                    contactsList[item].name = "";
                    if(contactsList[item].TITLE){
                        contactsList[item].name = contactsList[item].TITLE;
                    }
                    if(contactsList[item].NAME){
                        contactsList[item].name = contactsList[item].name +" "+ contactsList[item].NAME;
                    }
                    if(contactsList[item].LAST_NAME){
                        contactsList[item].name = contactsList[item].name +" "+ contactsList[item].LAST_NAME;
                    }
                    contactsList[item].Full_Name = contactsList[item].name;
                }
                UA_APP_UTILITY.addInStoredRecipientInventory(contactsList[item]); 
            }       
        }
    },
    
    addSentSMSAsRecordInHistory: async function(sentMessages){
        var historyDataArray = [];

        let credAdProcess3 = curId++;
        showProcess(`Adding ${Object.keys(sentMessages).length} messages to history...`, credAdProcess3);

        for(var sentMessageId in sentMessages){
            let sentMessage = sentMessages[sentMessageId];
            let recModule = sentMessage.module; /*sentMessage.moduleId.split("_")[0];*/
            let recId = sentMessage.moduleId?(sentMessage.moduleId.indexOf("newmanual")!==0?sentMessage.moduleId:UA_SAAS_SERVICE_APP.widgetContext.entityId):UA_SAAS_SERVICE_APP.widgetContext.entityId;
            historyDataArray = {
                fields: {
                    "ENTITY_ID": recId,
                    "ENTITY_TYPE": recModule,
                    "COMMENT": `${sentMessage.tpa?sentMessage.tpa:"SMS"} - ${sentMessage.channel} message status: ${sentMessage.status} from ${sentMessage.from} to ${sentMessage.to}  
                                
                                Platform  : ${sentMessage.channel}
                                
                                Sender    : ${sentMessage.from}
                                
                                Receiver  : ${sentMessage.to}
                                
                                Direction : Outbound
                                
                                Status    :  ${sentMessage.status}
                                
                                Message   : ${sentMessage.message}`
                }  
            };
            let response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/rest/crm.timeline.comment.add`, "POST", historyDataArray);
            console.log('addSentSMSAsRecordInHistory',response);
        }
        
        processCompleted(credAdProcess3);
        //alert('All Messages has been added to Pinnacle SMS History');
        
    },

    getModuleFields: async function(module,callback){
        await UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/rest/crm.'+module+'.fields', function(response){
            UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
            console.log(response);
            callback(response);
        });
    },

    showWorkFlowInstructionDialog: function(){
        UA_SAAS_SERVICE_APP.getPersonalWebhookAuthtoken();
        $("#workflowInstructionDialog").show();
    },

    getPersonalWebhookAuthtoken: async function(){
        await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, action:"GET_WEBHOOK_AUTH_TOKEN"}).then((response) => {
            console.log('getPersonalWebhookAuthtoken response recd', response);
            var authtoken = response.data.authtoken['saas'];
            if(authtoken){
                UA_APP_UTILITY.PERSONAL_WEBHOOK_TOKEN = authtoken;
                // UA_SAAS_SERVICE_APP.renderWorkflowBodyCode(authtoken);
                var saasWithTpaWebhookUrlMap = {
                    "pinnacleforbitrix24":`https://us-central1-ulgebra-license.cloudfunctions.net/sms_workflowWebhookHandler?authtoken=${authtoken}&c=sms&t={{templateName}}&s={{senderId}}&p=WORK`,
                    "ringcentralforbitrix24":`https://us-central1-ulgebra-license.cloudfunctions.net/sms_workflowWebhookHandler?authtoken=${authtoken}&c=sms&t={{templateName}}&s={{fromNumber}}&p=WORK`,
                    "clickatellforbitrix24":`https://us-central1-ulgebra-license.cloudfunctions.net/sms_workflowWebhookHandler?authtoken=${authtoken}&c=sms&t={{templateName}}&s={{fromNumber}}&p=WORK`,
                    "vonageforbitrix24":`https://us-central1-ulgebra-license.cloudfunctions.net/sms_workflowWebhookHandler?authtoken=${authtoken}&c={{sms/whatsapp/messenger}}&t={{templateName}}&s={{senderId/channelId}}&p=WORK`,
                    "twilioforbitrix24":`https://us-central1-ulgebra-license.cloudfunctions.net/sms_workflowWebhookHandler?authtoken=${authtoken}&c={{sms/mms/whatsapp}}&t={{templateName}}&s={{fromNumber}}&p=WORK`,
                    "whatcetraforbitrix24":`https://us-central1-ulgebra-license.cloudfunctions.net/sms_workflowWebhookHandler?authtoken=${authtoken}&c={{sms/mms}}&t={{templateName}}&p=WORK`
                }
                $('#wi-webhook-url textarea').val(saasWithTpaWebhookUrlMap[extensionName]);
                $(".ssf-fitem.messageInputHolder")[2].remove();
            }
            return true;
        }).catch(err => {
            console.log(err);
            return false;
        });
    },
    
    renderWorkflowBodyCode :function(accessToken){
        
        UA_TPA_FEATURES.workflowCode.SMS.ulgebra_webhook_authtoken = accessToken;
        UA_TPA_FEATURES.workflowCode.WhatsApp.ulgebra_webhook_authtoken = accessToken;
        
        UA_SAAS_SERVICE_APP.addWorkflowBodyCode('sms', 'For Sending SMS', UA_TPA_FEATURES.workflowCode.SMS);
        
        UA_SAAS_SERVICE_APP.addWorkflowBodyCode('whatsapp', 'For Sending WhatsApp', UA_TPA_FEATURES.workflowCode.WhatsApp);
    },

    addWorkflowBodyCode: function(id, title, object){
        if($(`#wiparams-body-code-${id}`).length > 0){
            return;
        }
        $("#wiparams-holder").append(`<div class="wiparams-body-code-ttl">${title}</div><pre class="wiparams-pre-code" id="wiparams-body-code-${id}">`+syntaxHighlight(JSON.stringify(object, undefined, 4)));
    },
    
    entityDetailFetched: function(data){
        setTimeout(()=>{UA_APP_UTILITY.fetchedAllModuleRecords();}, 2000);
        if(typeof UA_TPA_FEATURES.callOnEntityDetailFetched === "function"){
            UA_TPA_FEATURES.callOnEntityDetailFetched(data);
        }
    },

    checkPlacementBind : async function() {
       var placementRes = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/rest/placement.get.json`, "GET", null);
       UA_SAAS_SERVICE_APP.showErrorIfInAPICall(placementRes);
       if(placementRes && placementRes.data.result && placementRes.data.result.length < 1) {
            var queryParams = new URLSearchParams(location.search);
            let appCode = queryParams.get("appCode");
            let app = appCode.split("forbitrix24")[0];
            let action = "sendsms";
            UA_SAAS_SERVICE_APP.setHandlers(app,action);
       }
    },

    appNameLables : {
        "pinnacle":"Pinnacle SMS",
        "vonage": "Vonage",
        "msg91": "MSG91",
        "ringcentral": "RingCentral",
        "clickatell": "Clickatell",
        "twilio": "Twilio",
        "whatcetra": "WhatCetra",
        "messagebird": "MessageBird",
        "calendly": "Calendly",
        "textlocal": "TextLocal"
    },
    
    setHandlers: async function(app,action) {
        var handlers = [
            "CRM_LEAD_LIST_MENU",
            "CRM_DEAL_LIST_MENU",
            "CRM_CONTACT_LIST_MENU",
            "CRM_LEAD_DETAIL_TAB",
            "CRM_DEAL_DETAIL_TAB",
            "CRM_CONTACT_DETAIL_TAB",
            "CRM_LEAD_DETAIL_ACTIVITY",
            "CRM_DEAL_DETAIL_ACTIVITY",
            "CRM_CONTACT_DETAIL_ACTIVITY"
        ];
        handlers.forEach(async (item) => {
            var placementBindApiObj = {
                "PLACEMENT": item, 
                "HANDLER": `https://us-central1-ulgebra-license.cloudfunctions.net/new_bitrix24AppHandler?app=${app}&action=${action}`,
                "TITLE": `${UA_SAAS_SERVICE_APP.appNameLables[app]}`
            };
            var placementBindRes = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/rest/placement.bind`, "POST", placementBindApiObj);
        });
    },

    

// LookUp functions
LOOKUP_SUPPORTED_MODULES: {
    "contacts": {
        "autoLookup": true,
        "apiName": "contact"
    },
    "leads": {
        "autoLookup": true,
        "apiName": "lead"
    },
    "companies": {
        "autoLookup": true,
        "apiName": "company"
    },
    "deals": {
        "autoLookup": false,
        "apiName": "deal"
    }
},
    
moduleIDs: {
    "contacts": "contact",
    "leads": "lead",
    "companies": "company",
    "deals": "deal"
},

allModuleFieldsFetched: function(){
    let fieldsPresentModuleCount = 0;
    Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).forEach(item=>{
        if(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[item].fields && Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[item].fields).length > 0){
            fieldsPresentModuleCount++;
        }
    });
    return Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).length === fieldsPresentModuleCount;
},

retrieveModuleFieldsForLookUp: function(callback){
    $('#ua-loo-search-field-ddholder').hide();
    if (UA_SAAS_SERVICE_APP.allModuleFieldsFetched()) {
        callback();
    }
    var primaryFieldsToCreate = [];
    let primaryFieldsToViewForModule = ["id"];

    Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).forEach(async (moduleId)=>{
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/rest/crm.${UA_SAAS_SERVICE_APP.moduleIDs[moduleId]}.fields`, "GET", null);
        let fieldAPINameMap = {};
        if(!response.data || !response.data.result){
            return;
        }
        var fields = response.data.result;
        for(var i=0; i < Object.keys(fields).length; i++){
            let fieldItemId = Object.keys(fields)[i];
            let fieldItem = fields[fieldItemId];
            if(fieldItem.type != "file") {
                if(["lookup"].includes(fieldItem.fieldType)){
                    fieldItem.field_read_only = true;
                }

                fieldItem['display_label'] = fieldItem.title;
                fieldItem['api_name'] = fieldItem.fieldItemId;
                fieldItem["system_mandatory"] = fieldItem.isRequired;
                fieldItem["field_read_only"] = fieldItem.isReadOnly;
                fieldItem['data_type'] = fieldItem.type;

                if(["user","integer","char","string"].includes(fieldItem['data_type'])){
                    fieldItem['data_type'] = "text";
                }
                if(fieldItem['data_type'].includes("crm_") == true){
                    fieldItem['data_type'] = "text";
                    if(fieldItemId != "PHONE" && fieldItemId != "EMAIL"){
                        fieldItem["field_read_only"] = true;
                    }
                }
                if(fieldItem['data_type'] == "date"){
                    fieldItem['data_type'] = "datetime";
                }
                if(fieldItem['data_type'] == "dropdown" || fieldItem['data_type'] == "select"){
                    fieldItem['data_type'] = "picklist";
                }
                if(fieldItem.options && fieldItem.options.length > 0){
                    fieldItem["pick_list_values"] = [];
                    for(var optInd=0; optInd < fieldItem.options.length; optInd++){
                        let option = fieldItem.options[optInd];
                        fieldItem["pick_list_values"][optInd] = {
                            "actual_value": option.id,
                            "display_value": option.value
                        }
                    }
                }
                fieldAPINameMap[fieldItemId] = fieldItem;
            }
        }
        fieldAPINameMap.id = {
            'display_label': 'ID',
            'data_type': 'number',
            'api_name': 'id',
            'field_read_only': true
        };            
        
        switch(moduleId){
            case("contacts"): {
                primaryFieldsToCreate = ["NAME","EMAIL","PHONE"];
                primaryFieldsToViewForModule = ["ID","NAME","EMAIL","PHONE"];
                break;
            }
            case("leads"):{
                primaryFieldsToCreate = ["NAME","TITLE","PHONE","EMAIL"];
                primaryFieldsToViewForModule = ["ID","NAME","TITLE","PHONE","EMAIL"];
                break;
            }
            case("companies"):{
                primaryFieldsToCreate = ["TITLE","PHONE","EMAIL"];
                primaryFieldsToViewForModule = ["ID","TITLE","PHONE","EMAIL"];
                break;
            }
            case("deals"):{
                primaryFieldsToCreate = ["TITLE","COMMENTS"];
                primaryFieldsToViewForModule = ["ID","TITLE","TYPE_ID","COMMENTS"];
                break;
            }
        }

        UA_TPA_FEATURES.setCreateModuleRecordFields(moduleId, primaryFieldsToCreate);
        UA_TPA_FEATURES.setViewModuleRecordFields(moduleId, primaryFieldsToViewForModule);
        UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[moduleId].fields = fieldAPINameMap;
        if(UA_SAAS_SERVICE_APP.allModuleFieldsFetched()){
            callback();
        }
    });
},

getSearchedResultItems: async function(searchTerm, module, field){
    UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS = {};
    if(searchTerm.startsWith('+')){
        searchTerm = searchTerm.replace('+', '%2B');
    }
    let moduleFieldsList = Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[module].fields);
    let fieldsParams = $.param( {select:moduleFieldsList}, true ).replaceAll('select',"select[]");

    var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/rest/crm.${UA_SAAS_SERVICE_APP.moduleIDs[module]}.list?filter[${field.toUpperCase()}]=${searchTerm}&${fieldsParams}`,"GET",{});
    if(response && response.data && response.data.result && response.data.result.length > 0){ 
        var responseData = response.data.result;
        for(var j=0; j < responseData.length; j++){
            var item = responseData[j];
            item = await UA_SAAS_SERVICE_APP.changeNeedRecFormate(module,item);
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id] = item;     
        }
    }
    return response.data;
},

getNotesOfRecord: async function(module, entityId){
    var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/rest/crm.timeline.comment.list?filter[ENTITY_TYPE]=${UA_SAAS_SERVICE_APP.moduleIDs[module]}&filter[ENTITY_ID]=${entityId}`, "GET", null);
        let notesArray = [];
        if(response && response.data && response.data.result && response.data.result.length > 0){
            for(var i=0;i < response.data.result.length; i++){
                noteItem = response.data.result[i];
                notesArray.push({
                    'id': noteItem.ID,
                    'content': noteItem.COMMENT,
                    'title': "",
                    'time': noteItem.CREATED
                });
            }
        }
    return notesArray;
},

addModuleRecordNote: async function(module, entityId, noteContent){
    var noteResponse = await UA_OAUTH_PROCESSOR.getAPIResponse(`/rest/crm.timeline.comment.add`, "POST", {
            fields: {
                "COMMENT" : noteContent,
                "ENTITY_TYPE": UA_SAAS_SERVICE_APP.moduleIDs[module],
                "ENTITY_ID": entityId
            }
    });
    if(noteResponse.data && noteResponse.data.result){
        return noteResponse.data;
    }
    else{
        return {
            error: true
        }
    }
},

updateRecordByField: async function(module, entityId, fieldName, fieldValue){
    let updateObjPayloadFields = {};
    if(fieldName == "PHONE" || fieldName == "EMAIL"){
        let fieldId = UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[entityId][fieldName+"S"][0].ID;
        updateObjPayloadFields[fieldName] = [{"VALUE":fieldValue,"ID":fieldId}];
    }
    else{
        updateObjPayloadFields[fieldName] = fieldValue;
    }
    var updateObjPayload = {fields:updateObjPayloadFields};

    var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/rest/crm.${UA_SAAS_SERVICE_APP.moduleIDs[module]}.update?id=${entityId}`, "POST", updateObjPayload);
    if(response.data && response.status == 200){
        return response;
    }
    else if(response.error && response.error.message){
        showErroWindow('Unable to create '+module, response.error.message);
        return {
            error: {
                'message': response.error.message
            }
        }
    }
    else{
        return {
            error: {
                'message': 'Error, Try again'
            }
        }
    }
},

createRecordInModule: async function(module, fieldMap){
    if(!fieldMap){
        return;
    }
    let updateObjPayloadFields = {};
    Object.keys(fieldMap).forEach((fieldName)=>{
        let fieldValue = fieldMap[fieldName];
        if(fieldName == "PHONE" || fieldName == "EMAIL"){
            updateObjPayloadFields[fieldName] = [{"VALUE":fieldValue,"VALUE_TYPE":"WORK"}];
        }
        else{
            updateObjPayloadFields[fieldName] = fieldValue;
        }
    });
    var updateObjPayload = {fields:updateObjPayloadFields};

    var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/rest/crm.${UA_SAAS_SERVICE_APP.moduleIDs[module]}.add`, "POST", updateObjPayload);
    if(response.data && response.data.result){
        let resultItemId = response.data.result;
        let recordItem = response.data.result;
        location.reload();
        recordItem = await UA_SAAS_SERVICE_APP.changeNeedRecFormate(module,recordItem);
        UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[resultItemId] = recordItem;
        return recordItem;
    }
    else if(response.error){
        var errMsg = "Error, Try again";
        if(response.error && response.error.message){
            errMsg = response.error.message;
        }
        showErroWindow('Unable to create '+module, errMsg);
        return {
            error: {
                'message': errMsg
            }
        };
    }
    else{
        return {
            error: {
                'message': 'Error, Try again'
            }
        };
    }
},

getNewRecordInSAASWebURL: function(module){
    return `https://${appsConfig.UA_DESK_ORG_ID}/crm/${UA_SAAS_SERVICE_APP.moduleIDs[module]}/details/0/`;
},

changeNeedRecFormate: async function(module, item){
    var fullName = "";
    if(module=="Contacts"){
        fullName = item.NAME?item.NAME:(item.LAST_NAME?item.LAST_NAME:"Contact - #"+item.ID);
    }
    if(module=="Leads"){
        fullName = item.NAME?item.NAME:(item.TITLE?item.TITLE:"Lead - #"+item.ID);
    } 
    if(module=="Companies"){
        fullName = item.TITLE?item.TITLE:"Company - #"+item.ID;
    }

    if(item.HAS_PHONE == 'Y'){
        item.PHONES = item.PHONE;
        item.PHONE = item.PHONE[0].VALUE;
    }
    if(item.HAS_EMAIL == 'Y'){
        item.EMAILS = item.EMAIL;
        item.EMAIL = item.EMAIL[0].VALUE;
    }

    item.name = fullName;
    item.id = item.ID.toString();
    item.webURL = `https://${appsConfig.UA_DESK_ORG_ID}/crm/${UA_SAAS_SERVICE_APP.moduleIDs[module]}/details/${item.id}/`;

    return item;
}

};