var UA_SAAS_SERVICE_APP = {

    CURRENT_USER_INFO: null,
    CURRENT_ORG_INFO: null,
    CURRENT_OAUTH_SCOPE_NEEDED: ["automation", "timeline", "tickets", "crm.objects.contacts.read", "crm.objects.contacts.write", "crm.objects.companies.write", "crm.schemas.contacts.read", "crm.objects.companies.read", "crm.objects.deals.read", "crm.objects.deals.write", "crm.schemas.companies.read", "crm.schemas.companies.write", "crm.schemas.contacts.write", "crm.schemas.deals.read", "crm.schemas.deals.write", "crm.objects.owners.read", "settings.users.read", "crm.lists.read", "crm.lists.write"],
    FETCHED_RECORD_DETAILS: {},
    CURRENT_MODULE_FIELDS_DROPDOWN: [],
    SELECTED_RECEIPS: {},
    SEARCH_MODULE_RECORD_MAP: {},
    fetchedList: {},
    widgetContext: {},
   

    supportedIncomingModules: [
        {
            "selected": true,
            "value": "Leads",
            "label": "Leads"
        }
    ],

    getAPIBaseUrl: function () {
        return ``;
    },

    initiateAPPFlow: async function () {
        UA_SAAS_SERVICE_APP.initiateAPPFlow_SA(true);
    },

    initiateAPPFlow_SA: function (isSMS = false) {

        if (!UA_SAAS_SERVICE_APP.widgetContext.module && queryParams.get("module")) {
            UA_SAAS_SERVICE_APP.widgetContext.module = queryParams.get("module");
            UA_SAAS_SERVICE_APP.widgetContext.entityId = queryParams.get("entityId");
            UA_SAAS_SERVICE_APP.injectShowSearchContact();
        }

        try {
            Object.keys(UA_TPA_FEATURES.workflowCode).forEach(msgType => {
                delete UA_TPA_FEATURES.workflowCode[msgType].module;
                delete UA_TPA_FEATURES.workflowCode[msgType].recordId;
            });
        }
        catch (ex) {
            console.log(ex);
        }

        $("#ac_name_label_saas .anl_servicename").text('Leadsquared');

        UA_SAAS_SERVICE_APP.getCurrentUserInfo(function (response) {
            UA_SAAS_SERVICE_APP.CURRENT_USER_INFO = response.data;
            appsConfig.APP_UNIQUE_ID = UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.Id;
            appsConfig.UA_DESK_ORG_ID = UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.OrgId;
            $("#saasAuthIDName").text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.user).attr('title', `Authorized Leadsuared Account: ${UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.OrgDisplayName}`);
            setTimeout((function () {
                $('#ac_name_label_saas .ac_name_id').text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.PassPhrase + `(${UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.OrgDisplayName})`);
            }), 2000);
            UA_SAAS_SERVICE_APP.proceedToAppInitializationIfAPPConfigResolved();
        });
    },

    proceedToAppInitializationIfAPPConfigResolved: function () {
        if (appsConfig.APP_UNIQUE_ID && appsConfig.UA_DESK_ORG_ID) {
            UA_SAAS_SERVICE_APP.renderInitialElements();
            UA_SAAS_SERVICE_APP.addSAASUsersToLICUtility();
            UA_APP_UTILITY.appsConfigHasBeenResolved();
            if (UA_SAAS_SERVICE_APP.widgetContext.module == "CONTACT") {
                UA_SAAS_SERVICE_APP.renderInitialElementsForHubList();
            }
        }
    },

    renderInitialElements: async function () {
        UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown('users', ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder']);
        if (!UA_SAAS_SERVICE_APP.widgetContext.module) {
            UA_SAAS_SERVICE_APP.entityDetailFetched(null);
            console.log('Module does not exist, rendering skipped');
            $("#recip-count-holder").text("Add recipients to proceed");
            return;
        }
        //        UA_APP_UTILITY.renderSavedTemplatesInDropdowns();
        let entityIdArr = UA_SAAS_SERVICE_APP.widgetContext.entityId.split(',');
        
        UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(UA_SAAS_SERVICE_APP.widgetContext.module, entityIdArr, function (contactList) {
            console.log(contactList);
            UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
            UA_SAAS_SERVICE_APP.addRecordsAsRecipients(contactList);
            UA_APP_UTILITY.fetchedAllModuleRecords();
            
        });
    },

    renderInitialElementsForHubList: async function () {
        $(".ssf-new-recip-form").prepend(`<div id="ssf-recip-list-add-holdr" class="ssf-recip-choice-btn" style="margin-top: -5px;background: none;border: none;box-shadow: none;"></div>`);
        UA_SAAS_SERVICE_APP.renderContactList();
    },

    renderContactList: async function () {
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/contacts/v1/lists?count=50`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('renderList', response);
        let listDropdownItems = [];
        response.data.lists.forEach(item => {
            UA_SAAS_SERVICE_APP.fetchedList[item.listId] = item;
            listDropdownItems.push({
                'label': item.name,
                'value': item.listId
            });
        });

        UA_APP_UTILITY.renderSelectableDropdown('#ssf-recip-list-add-holdr', 'Select a List', listDropdownItems, 'UA_SAAS_SERVICE_APP.insertManageListContacts', false, false);

    },

    insertManageListContacts: async function (listId) {

        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/contacts/v1/lists/${listId}/contacts/all`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('renderList', response);
        if (response.data && response.data.contacts && response.data.contacts.length > 0) {
            response.data.contacts.forEach(item => {
                UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails("CONTACT", item.vid, async function (contactList) {
                    UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
                    UA_SAAS_SERVICE_APP.addRecordsAsRecipients(contactList);
                });
            });
        }

    },

    addSAASUsersToLICUtility: async function () {
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/v2/UserManagement.svc/Users.Get`, "GET", null);
        response.data.sort(function (a, b) {
            return a.LastName.localeCompare(b.LastName);
        });

        response.data.forEach(item => {
            UA_LIC_UTILITY.addFetchedSAASUser(item.ID, item.LastName + ' ' + item.FirstName, item.EmailAddress, null, true, null, url = null);
        });
        UA_LIC_UTILITY.saasUserListFetchCompleted();
    },

    sendPhoneFieldsForRecipientType: function (fieldsArray) {
        fieldsArray.forEach(item => {
            if (item.data_type === "Phone" || item.data_type === "mobilephone" || item.fieldType === "phonenumber") {
                UA_APP_UTILITY.addRecipientPhoneFieldType(item.api_name, item.display_label);
            }
        });
    },
    FETCHED_MODULE_FIELDS: {},
    populateModuleItemFieldsInDropDown: async function (module, target, fieldsCallback = (() => { })) {
        try{
            if(!module || UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.includes(module) == true){
                return;
            }
            UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.push(module);
            var moduleToLoad = module === "users" ? UA_SAAS_SERVICE_APP.CURRENT_USER_INFO : null;
            if(module !== "users"){
                moduleToLoad = UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS)[0]];
                if(UA_SAAS_SERVICE_APP.widgetContext.entityId){
                    moduleToLoad = UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[UA_SAAS_SERVICE_APP.widgetContext.entityId.split(',')[0]+''];
                }
            }
            var moduleFields = Object.keys(moduleToLoad);
            var fieldsArray = [];
            var dropDownValues = [];
            moduleFields.forEach(item=>{
                let itemVal = moduleToLoad[item];
                if(typeof itemVal !== "object" && itemVal !== null && typeof itemVal!=="boolean"){
                    fieldsArray.push({
                        "api_name": item,
                        "display_label": item
                    });
                    dropDownValues.push({
                        "label": item,
                        "value": (module.toUpperCase() === "USERS" ? "CURRENT_USER": module.toUpperCase())+'.'+item
                    });
                }
            });
            fieldsCallback(fieldsArray);
            if(typeof target === "string"){
                target = [target];
            }
            target.forEach(item=>{
                UA_APP_UTILITY.renderSelectableDropdown(item, `${module} fields`, dropDownValues, 'UA_APP_UTILITY.templateMergeFieldSelected', false, true);
            });
            return true;
        }catch(e){ console.log(e); return false}
    },

    injectShowSearchContact: async function () {
        $(".ssf-new-recip-form").prepend(`<div id="searchAndAddFromContactBtn" class="ssf-recip-choice-btn" onclick="$('.item-list-popup').show()">
                                            <i class="material-icons">search</i> Search from ${UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()}
                                      </div>`);
        $('.pageContentHolder').append(`<div class="item-list-popup" style="display:none">
                        <div class="item-list-popup-title">
                            Select ${UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()} <div class="pop-win-close" onclick="$('.item-list-popup').hide();">x</div>
                        </div>
                        <div class="item-search-box">
                            <input class="input-form" id="contact-seach-name" type="text" autocomplete="off" placeholder="name of the ${UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()}"/><br>
                            <div class="btn-save" onclick="UA_SAAS_SERVICE_APP.searchRecordsAndRender($('#contact-seach-name').val(),UA_SAAS_SERVICE_APP.widgetContext.module)">
                                Search ${UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()}
                            </div>
                            <div class="btn-reset" onclick="UA_SAAS_SERVICE_APP.resetsearchContacts()">
                                Reset
                            </div>
                        </div>
                        <div class="item-list-popup-content" id="contact-search-items">

                        </div>
                    </div>`);
    },

    resetsearchContacts: function () {
        $("#contact-seach-name").val("");
        $("#contact-search-items").html("");
    },

    searchRecordsAndRender: async function (searchTerm, module) {
        if (!valueExists(searchTerm)) {
            return;
        }
        var modulePluralWord = module === "COMPANY" ? "companies" : module.toLowerCase() + "s";
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/${modulePluralWord}/search`, "POST", JSON.stringify({ "query": searchTerm }))
        if (response) {
            console.log(response);
            UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
            $("#contact-search-items").html(" ");
            if (response.data["results"].length > 0) {
                var search_items = response.data["results"];
                if (search_items && search_items.length > 50) {
                    search_items.splice(0, 49);
                }
                UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module] = {};
                search_items.forEach((obj) => {
                    UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][obj.id] = obj;
                    let fullName = "";
                    if (module) {
                        switch (module) {
                            case ("CONTACT"): {
                                fullName = obj['properties'].firstname ? obj['properties'].firstname : "";
                                if (obj['properties'].lastname) {
                                    fullName = fullName + " " + obj['properties'].lastname;
                                }
                                fullName = fullName ? fullName : "Contact#" + obj.id;
                                break;
                            }
                            case ("COMPANY"): {
                                fullName = obj['properties'].name ? obj['properties'].name : "Company#" + obj.id;
                                break;
                            }
                            case ("DEAL"): {
                                fullName = obj['properties'].dealname ? obj['properties'].dealname : "Deal#" + obj.id;
                                break;
                            }
                            case ("TICKET"): {
                                fullName = obj['properties'].subject ? obj['properties'].subject : "Ticket#" + obj.id;
                                break;
                            }
                        }
                    }
                    let itemHTML = `<div class="item-list-popup-item" style="cursor:pointer;" onclick="UA_SAAS_SERVICE_APP.selectPhoneNumber('${obj.id}', '${module}', '${fullName}','${module}','${true}')"><span class="c-name"> ${fullName} </span>`;
                    // itemHTML =  itemHTML + `${'<span class="c-phone '+(UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[module+"_"+obj.id] ? 'alreadyadded': '')+'"></span>'}`;
                    itemHTML = itemHTML + `</div>`;
                    $("#contact-search-items").append(itemHTML);
                });
            }
            else {
                $("#contact-search-items").html("No records found. <br><br> <span class=\"c-silver\"> This search will include <br> only customers having phone numbers</span>");
                return;
            }
        }
    },

    selectPhoneNumber: async function (id, number, name, module, isSelected) {
        var receipNumberID = module + "_" + id;
        if (!UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] && !UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id]) {
            UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] = {
                'id': id,
                'name': name,
                'module': module,
                'isSelected': isSelected
            };
            $.extend(UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id], UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID]);
            // UA_SAAS_SERVICE_APP.renderInitialElements(module,id);
            UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(module, id, UA_SAAS_SERVICE_APP.addRecordsAsRecipients);
            await UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
            // UA_SAAS_SERVICE_APP.addRecordsAsRecipients([UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id]]);
        }
        else {
            console.log(module + ' already added');
            showErroWindow("Alert Message", `This ${module.toLocaleLowerCase()} is already added.`)
            // alert(`This ${module.toLocaleLowerCase()} is already added.`);
            return;
        }
    },

    getCurrentUserInfo: async function (callback) {
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/v2/Authentication.svc/UserByAccessKey.Get?', callback);
    },

    getUserInfoFromID: async function (userEmail, callback) {
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/crm/v3/owners?email=' + userEmail, callback);
    },

    getCurrentOrgInfo: async function (callback) {
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/org', callback);
    },

    getAPIResponseAndCallback: async function (url, callback) {
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}${url}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        callback(response);
    },

    showErrorIfInAPICall: function (response) {
        if (response.code === 401) {
            UA_OAUTH_PROCESSOR.showReAuthorizationError();
        }
    },

    initiateAuthFlow: function () {
        UA_OAUTH_PROCESSOR.initiateAuth();
    },

    fetchStoreExecuteOnRecordDetails: async function (module, recordIds, callback) {
        if (!module) {
            return;
        }
        //        if(typeof recordIds !== "object"){
        //            var recordId = recordIds;
        //            recordIds = [recordId];
        //        }
        //        recordIds = ["5318123000000377412"];
        let leadIds = recordIds;
        $("#recip-count-holder").text(`fetching ${module.toLocaleLowerCase()} information...`);

        let moduleUrl = module === "leads" ? "/v2/LeadManagement.svc/Leads/Retrieve/ByIds" : false;
        if (!moduleUrl) {
            return;
        }
        let metaDataUrl = '/v2/LeadManagement.svc/LeadsMetaData.Get';
        let metaData = await UA_OAUTH_PROCESSOR.getAPIResponse(metaDataUrl, "GET", null);
        debugger;
        let schemaNames = metaData.data && metaData.data.length ? metaData.data.map(o => o.SchemaName) : [];
        let columns = schemaNames.join(', ');
        let requestPayload = {
            "SearchParameters": {
                "LeadIds": leadIds
            },
            "Columns": {
                "Include_CSV": columns
            }
        };

        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(moduleUrl, "POST", requestPayload);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('fetchStoreExecuteOnRecordDetails', response);
        
        
        if (module === "leads") {
            $("#recip-count-holder").text(`Selected ${leadIds.length} ${module.toLocaleLowerCase()}`);
            UA_SAAS_SERVICE_APP.entityDetailFetched();
            if(response.data.Leads){
                response.data.Leads.forEach(leaditem => {
                    UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[leaditem.ProspectID] = leaditem;
                    UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[leaditem.ProspectID].id = leaditem.ProspectID;    
                    UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[leaditem.ProspectID].email = leaditem.EmailAddress;    
                })
            }
        }
        
        // else if (response.data && response.data) {
        //     var fetchedAssociatedContactsList = response.data.Leads;
        //     var contactIdInputPayload = [];
        //     fetchedAssociatedContactsList.forEach(contactItem => {
        //         contactIdInputPayload.push({
        //             'id': contactItem.id
        //         });
        //     });
        //     $("#recip-count-holder").text(`fetching ${contactIdInputPayload.length} ${module.toLocaleLowerCase()} information...`);
        //     let contactFieldsToFetch = ["phone", "email", "firstname", "lastname", "mobilephone"];
        //     if (UA_SAAS_SERVICE_APP.FETCHED_MODULE_FIELDS && UA_SAAS_SERVICE_APP.FETCHED_MODULE_FIELDS['CONTACT']) {
        //         contactFieldsToFetch = Object.keys(UA_SAAS_SERVICE_APP.FETCHED_MODULE_FIELDS['CONTACT']);
        //     }


        //     var response = await UA_OAUTH_PROCESSOR.getAPIResponse(moduleUrl, "POST", {
        //         properties: contactFieldsToFetch,
        //         inputs: contactIdInputPayload
        //     });
        //     UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        //     console.log('fetchStoreExecuteOnRecordDetails', response);
        //     response.data.results.forEach(item => {
        //         UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds + "-" + item.id] = UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds];
        //         UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds + "-" + item.id][`contact_id`] = item.id;
        //         for (const [key, value] of Object.entries(item.properties)) {
        //             UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordIds + "-" + item.id][`contact_${key}`] = value;
        //         }
        //         // UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id] = item.properties;
        //         // UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id].id = item.id;
        //     });
        //     if (Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS).length > 1) {
        //         UA_APP_UTILITY.changeCurrentView('MESSAGE_FORM');
        //     }

        //     callback(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
        //     UA_SAAS_SERVICE_APP.entityDetailFetched();
        // }
        else {
            showErroWindow("Alert Message", `This ${module.toLocaleLowerCase()} haven't any associated Contacts.`);
        }
        callback(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
        UA_SAAS_SERVICE_APP.entityDetailFetched();
    },

    addRecordsAsRecipients: function (contactsList) {
        for (var item in contactsList) {
            if ($(`[data-recip-id=${contactsList[item]['id']}]`).length == 0) {
                ["Phone"].forEach(mobileFieldItem => {
                    if (contactsList[item][mobileFieldItem]) {
                        UA_APP_UTILITY.addRecipientPhoneFieldType(mobileFieldItem, mobileFieldItem);
                    }
                });

                contactsList[item].name = "";
                if (!contactsList[item].FirstName || !contactsList[item].LastName) {
                    if (contactsList[item].FirstName) {
                        contactsList[item].name = contactsList[item].FirstName;
                    }
                    if (contactsList[item].LastName) {
                        contactsList[item].name = contactsList[item].LastName;
                    }
                }else if (contactsList[item].FirstName && contactsList[item].LastName){
                    contactsList[item].name = contactsList[item].FirstName && contactsList[item].LastName;
                }
                UA_APP_UTILITY.addInStoredRecipientInventory(contactsList[item]);
            }
        }
    },

    addSentSMSAsRecordInHistory: async function(sentMessages){
        //        var historyDataArray = [];
                if(!UA_SAAS_SERVICE_APP.widgetContext.module){
                    return;
                }
                var credAdProcess3 = curId++;
                showProcess(`Adding ${Object.keys(sentMessages).length} messages to ${UA_SAAS_SERVICE_APP.widgetContext.module} comments...`, credAdProcess3);
        
                var commentContent = `<u><i>${appPrettyName} Extension</i></u> - `;
                for(var sentMessageId in sentMessages){
                    let sentMessage = sentMessages[sentMessageId];
                    let recModule = sentMessage.module?sentMessage.module:UA_SAAS_SERVICE_APP.widgetContext.module; 
                    let recId = sentMessage.moduleId?(sentMessage.moduleId.indexOf("newmanual")!==0?sentMessage.moduleId:UA_SAAS_SERVICE_APP.widgetContext.entityId):UA_SAAS_SERVICE_APP.widgetContext.entityId;
        //            let historyData = {};
        //            historyData['Name'] = 'SMS to '+(sentMessage.contact.name ? sentMessage.contact.name : sentMessage.number);
        //            historyData[UA_SAAS_SERVICE_APP.APP_API_NAME+'Message'] = sentMessage.message;
        //            historyData[UA_SAAS_SERVICE_APP.APP_API_NAME+'Customer_Number'] = sentMessage.to;
        //            historyData[UA_SAAS_SERVICE_APP.APP_API_NAME+'From'] = sentMessage.from;
        //            historyData[UA_SAAS_SERVICE_APP.APP_API_NAME+'Direction'] = "Outbound";
        //            historyData[UA_SAAS_SERVICE_APP.APP_API_NAME+'Status'] = sentMessage.status;
        //            historyDataArray.push(historyData);
                    
                let commentContent = `<u><i>${appPrettyName} Extension</i></u> - `;
                    commentContent+=`<b>${sentMessage.channel} ${sentMessage.status} from ${sentMessage.from} to ${sentMessage.to}</b>:<br>
                                     ${sentMessage.message}<br>`;
                    
                    let response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/v2/LeadManagement.svc/CreateNote`, "POST", {
                        Note: commentContent,
                        RelatedProspectId: recId
                    });
                    
                    UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
                    // console.log('addSentSMSAsRecordInHistory',response);
                }
                processCompleted(credAdProcess3);
    },
    entityDetailFetched: function (data) {
        if (typeof UA_TPA_FEATURES.callOnEntityDetailFetched === "function") {
            UA_TPA_FEATURES.callOnEntityDetailFetched(data);
        }
    },

    // LookUp functions
    LOOKUP_SUPPORTED_MODULES: {
        "Leads": {
            "autoLookup": true,
            "apiName": "Leads"
        },
      
        // "Deals": {},
        // "Tickets": {}
    },

    allModuleFieldsFetched: function () {
        let fieldsPresentModuleCount = 0;
        Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).forEach(item => {
            if (UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[item].fields && Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[item].fields).length > 0) {
                fieldsPresentModuleCount++;
            }
        });
        return Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).length === fieldsPresentModuleCount;
    },

    retrieveModuleFieldsForLookUp: function (callback) {
        $('#ua-loo-search-field-ddholder').hide();
        if (UA_SAAS_SERVICE_APP.allModuleFieldsFetched()) {
            callback();
        }
        var primaryFieldsToCreate = [];
        let primaryFieldsToViewForModule = ["id"];

        Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).forEach(async moduleId => {
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/v2/LeadManagement.svc/LeadsMetaData.Get`, "GET", null);
            let fieldAPINameMap = {};
            // await response.data.results.forEach(fieldItem=>{
            for (var i = 0; i < response.data.length; i++) {
                let fieldItem = response.data[i];
                if ((fieldItem.type != "enumeration" || fieldItem.options.length > 0)) {
                

                    if(fieldItem.field_type === "Text"){
                        fieldItem.field_type = "text";
                    }
                    let fieldDataTypeMap = {
                        Text: 'text',
                        Number: 'int',
                        Website: 'website',
                        Email: 'email',
                        Phone:  "tel",
                        Date: 'datetime',
                        Select: 'picklist'
                    }

                    // {
                    //     "website": "url",
                    //     "email" : "email",
                    //     "phone": "tel",
                    //     "integer": "number",
                    //     "currency": "number",
                    //     "datetime": "datetime-local",
                    //     "double" : "number",
                    //     "textarea": "textarea",
                    //     "address": "textarea",
                    //     "int": "number"
                    // }


                    fieldItem['display_label'] = fieldItem.DisplayName;
                    fieldItem['api_name'] = fieldItem.Name;
                    fieldItem['data_type'] = fieldDataTypeMap[fieldItem.DataType] ? fieldDataTypeMap[fieldItem.DataType] : fieldItem.DataType;
                    fieldItem['field_read_only'] = fieldItem.IsReadOnly;
                    fieldItem['system_mandatory'] = fieldItem.SchemaName === 'LastName';
                    
                    if (fieldItem.Options && fieldItem.Options.length > 0) {
                        fieldItem["pick_list_values"] = [];
                        for (var optInd = 0; optInd < fieldItem.Options.length; optInd++) {
                            let option = fieldItem.Options[optInd];
                            fieldItem["pick_list_values"][optInd] = {
                                "actual_value": option.Value,
                                "display_value": option.Value
                            }
                        }
                    }

                    fieldAPINameMap[fieldItem.Name] = fieldItem;
                   
                }
                
            }
            fieldAPINameMap.id = {
                'display_label': 'ID',
                'data_type': 'number',
                'api_name': 'id',
                'field_read_only': true
            };

            switch (moduleId) {
                case ("Contacts"): {
                    primaryFieldsToCreate = ["LastName", "FirstName", "Phone", "Mobile", "EmailAddress"];
                    primaryFieldsToViewForModule = ["lastname", "firstname", "mobilephone", "phone", "email", "description"];
                    break;
                }
                case ("Leads"): {
                    primaryFieldsToCreate = ["Name  ", "Company", "Phone", "type", "description"];
                    primaryFieldsToViewForModule = ["name", "domain", "createdate", "hs_lastmodifieddate"];
                    break;
                }
            }

            UA_TPA_FEATURES.setCreateModuleRecordFields(moduleId, primaryFieldsToCreate);
            UA_TPA_FEATURES.setViewModuleRecordFields(moduleId, primaryFieldsToViewForModule);
            UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[moduleId].fields = fieldAPINameMap;
            if (UA_SAAS_SERVICE_APP.allModuleFieldsFetched()) {
                callback();
            }
        });
    },

    getSearchedResultItems: async function (searchTerm, module, field) {
        UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS = {};
        if (searchTerm.startsWith('+')) {
            searchTerm = searchTerm.replace('+', '*');
        }
        var apiReqData = {
            "filters": [
                {
                    "propertyName": field.toLocaleLowerCase(),
                    "operator": "EQ",
                    "value": searchTerm
                }
            ],
            "properties": Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[module].fields)
        };
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/${module.toLocaleLowerCase() != "companies" ? "contact" : "companies"}/search`, "POST", JSON.stringify(apiReqData));
        // await response.data.results.forEach(async (item)=>{
        for (var j = 0; j < response.data.results.length; j++) {
            var item = response.data.results[j];
            if (module.toLocaleLowerCase() == "deals" || module.toLocaleLowerCase() == "tickets") {
                var asso_response_list = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/contacts/${item.id}/associations/${module.toLocaleLowerCase()}`, "GET", null);
                // await asso_response_list.data.results.forEach(async (asslistitem)=>{
                for (var i = 0; i < asso_response_list.data.results.length; i++) {
                    var asslistitem = asso_response_list.data.results[i];

                    asslistitem = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/${module.toLocaleLowerCase()}/${asslistitem.id}?properties=${Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[module].fields).join(",")}`, "GET", null);
                    asslistitem = asslistitem.data;

                    var fullName = await UA_SAAS_SERVICE_APP.getRecordName(module, asslistitem);
                    asslistitem = { ...asslistitem, ...asslistitem.properties };
                    delete asslistitem.properties;
                    asslistitem.name = fullName;
                    asslistitem.webURL = `https://app.hubspot.com/${module.toLocaleLowerCase()}/${appsConfig.UA_DESK_ORG_ID}/${module.toLocaleLowerCase()}/${asslistitem.id}`;
                    UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[asslistitem.id] = asslistitem;
                }
            }
            else {
                var fullName = await UA_SAAS_SERVICE_APP.getRecordName(module, item);
                item = { ...item, ...item.properties };
                delete item.properties;
                item.name = fullName;
                item.webURL = `https://app.hubspot.com/${module.toLocaleLowerCase()}/${appsConfig.UA_DESK_ORG_ID}/${module.toLocaleLowerCase()}/${item.id}`;
                UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id] = item;
            }
        }
        return response.data.results;
    },

    getNotesOfRecord: async function (module, entityId) {
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVpickuICE_APP.getAPIBaseUrl()}/crm/v3/objects/${module.toLocaleLowerCase()}/${entityId}/associations/notes?limit=10`, "GET", null);
        let notesArray = [];
        if (response && response.data && response.data.results && response.data.results.length > 0) {
            // response.data.results.forEach(async(noteItem)=>{
            for (var i = 0; i < response.data.results.length; i++) {
                noteItem = response.data.results[i];
                var noteResponse = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/notes/${noteItem.id}?properties=hs_note_body`, "GET", null);
                if (noteResponse && noteResponse.data && noteResponse.data.id) {
                    notesArray.push({
                        'id': noteResponse.data.id,
                        'content': noteResponse.data.properties.hs_note_body,
                        'title': "",
                        'time': noteResponse.data.createdAt
                    });
                }
            }
        }
        return notesArray;
    },

    addModuleRecordNote: async function (module, entityId, noteContent) {
        var noteResponse = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/notes`, "POST", {
            "properties": {
                "hs_timestamp": new Date().toISOString(),
                "hs_note_body": noteContent

            }
        });
        if (noteResponse.data && noteResponse.status && noteResponse.data.id) {
            var associationsLableResponse = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v4/associations/notes/${module.toLocaleLowerCase()}/labels`, "GET", {});
            if (associationsLableResponse.data && associationsLableResponse.status && associationsLableResponse.data.results && associationsLableResponse.data.results.length > 0) {
                var associationResponse = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/notes/${noteResponse.data.id}/associations/${module.toLocaleLowerCase()}/${entityId}/${associationsLableResponse.data.results[0].typeId}`, "PUT", {});
                if (associationResponse && associationResponse.data && associationResponse.data.id) {
                    return associationResponse;
                }
            }
        }
        else {
            return {
                error: true
            }
        }
        return false;
    },

    updateRecordByField: async function (module, entityId, fieldName, fieldValue) {
        let updateObjPayload = {};
        updateObjPayload[fieldName] = fieldValue;
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl(3)}/crm/v3/objects/${module.toLocaleLowerCase()}/${entityId}`, "PATCH", { "properties": updateObjPayload });
        if (response.data || response.status == 200) {
            return response;
        }
        else {
            return {
                error: {
                    'message': 'Error, Try again'
                }
            }
        }
    },

    createRecordInModule: async function (module, fieldMap) {
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/${module.toLocaleLowerCase()}`, "POST", { "properties": fieldMap });
        if (response.data) {
            let resultItemId = response.data.id;
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/${module.toLocaleLowerCase()}/${resultItemId}?properties=${Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[module].fields).join(",")}`, "GET", null);
            let recordItem = response.data;

            recordItem.name = await UA_SAAS_SERVICE_APP.getRecordName(module, recordItem);
            recordItem = { ...recordItem, ...recordItem.properties };
            delete recordItem.properties;

            recordItem.webURL = `https://app.hubspot.com/${module.toLocaleLowerCase()}/${appsConfig.UA_DESK_ORG_ID}/${module.toLocaleLowerCase()}/${recordItem.id}`;
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[resultItemId] = recordItem;
            return recordItem;
        }
        else if (response.error) {
            showErroWindow('Unable to create ' + module, response.error.message);
            return {
                error: {
                    'message': response.error.message
                }
            };
        }
        else {
            return {
                error: {
                    'message': 'Error, Try again'
                }
            };
        }
    },

    getNewRecordInSAASWebURL: function (module) {
        let modules = ["", "Contacts", "Companies", "Deals", "", "Tickets"]
        return `https://app.hubspot.com/contacts/${appsConfig.UA_DESK_ORG_ID}/objects/0-${modules.indexOf(module)}/views/all/list`;
    },

    getRecordName: async function (module, item) {
        var fullName = "";
        switch (module) {
            case ("Contacts"): {
                fullName = item['properties'].firstname ? item['properties'].firstname : "";
                if (item['properties'].lastname) {
                    fullName = fullName + " " + item['properties'].lastname;
                }
                fullName = fullName ? fullName : "Contact#" + item.id;
                break;
            }
            case ("Companies"): {
                fullName = item['properties'].name ? item['properties'].name : "Company#" + item.id;
                break;
            }
            case ("Deals"): {
                fullName = item['properties'].dealname ? item['properties'].dealname : "Deal#" + item.id;
                break;
            }
            case ("Tickets"): {
                fullName = item['properties'].subject ? item['properties'].subject : "Ticket#" + item.id;
                break;
            }
        }
        return fullName;
    }
};