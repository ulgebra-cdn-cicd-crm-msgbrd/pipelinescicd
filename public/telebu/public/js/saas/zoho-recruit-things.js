
var UA_SAAS_SERVICE_APP = {


    CURRENT_USER_INFO: null,
    CURRENT_ORG_INFO: null,
    CURRENT_OAUTH_SCOPE_NEEDED: ['ZohoRecruit.modules.READ','ZohoRecruit.modules.CREATE','ZohoRecruit.modules.UPDATE','ZohoRecruit.settings.fields.read', 'ZohoRecruit.users.READ', 'ZohoRecruit.org.READ', 'ZohoRecruit.setup.operation.READ'],
    FETCHED_RECORD_DETAILS: {},
    CURRENT_MODULE_FIELDS_DROPDOWN: [],
    SELECTED_RECEIPS: {},
    SEARCH_MODULE_RECORD_MAP: {},
    fetchedList: {},
    widgetContext: {},
    DEALS_CONTACTS_LIST: {},
  
    supportedIncomingModules: [
        {
            "value": "Contacts",
            "label": "Contacts"
        },
        {
            "selected": true,
            "value": "Candidates",
            "label": "Candidates"
        }
    ],

    getAPIBaseUrl: function (version = 2) {
        return `https://recruit.zoho${UA_OAUTH_PROCESSOR.currentSAASDomain}/recruit/v2`;
    },

    provideSuggestedCurrentEnvLoginEmailID: function () {
        /* ZOHO.embeddedApp.on("PageLoad", async function(widgetContext) {
             var crmEnv = await ZOHO.CRM.CONFIG.getCurrentUser();
             UA_APP_UTILITY.receivedSuggestedCurrentEnvLoginEmailID(crmEnv.users[0].email);
         });
         
         ZOHO.embeddedApp.init().then();
 
         */
    },
    URL: {
        users: {
            fetch: {
                url: "/users?type=AllUsers",
                method: "GET"
            }
        },
        Candidates: {
            fetch: {
                url: "/Candidates/{ID}",
                method: "GET"
            }
        },
        Contacts: {
            fetch: {
                url: "/Contacts/{ID}",
                method: "GET"
            }
        },
        currentuser: {
            fetch: {
                url: "/users?type=CurrentUser",
                method: "GET"
            }
        }
    },
    initiateAPPFlow: async function () {
        ZOHO.embeddedApp.on("PageLoad", async function (widgetContext) {

            // console.log('embeddd', widgetContext); 

            var crmEnv = await ZOHO.CRM.CONFIG.GetCurrentEnvironment();
            UA_OAUTH_PROCESSOR.currentSAASDomain = UA_OAUTH_PROCESSOR.domainExtnMap[crmEnv.deployment];

            UA_SAAS_SERVICE_APP.widgetContext.module = widgetContext.Entity;
            UA_SAAS_SERVICE_APP.widgetContext.entityId = widgetContext.EntityId;

            // UA_SAAS_SERVICE_APP.injectShowSearchContact();
            // UA_SAAS_SERVICE_APP.initiateAPPFlow_SA();

        });

        ZOHO.embeddedApp.init().then();
    },

    initiateAPPFlow_SA: function () {
        if (extensionName.startsWith('lookupfor')) {
            // UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED.remove_by_value('ZohoCRM.modules.ALL');
            // UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED.push('ZohoCRM.modules.READ', 'ZohoCRM.modules.UPDATE', 'ZohoCRM.modules.CREATE');
        }
        let params = new URLSearchParams(location.search);
        if (!UA_SAAS_SERVICE_APP.widgetContext.module && queryParams.get("module")) {
            UA_SAAS_SERVICE_APP.widgetContext.module = queryParams.get("module");
            UA_SAAS_SERVICE_APP.widgetContext.entityId = queryParams.get("entityId");
            UA_SAAS_SERVICE_APP.injectShowSearchContact();
        }
        $("#ac_name_label_saas .anl_servicename").text('Zoho');
        UA_SAAS_SERVICE_APP.getCurrentUserInfo(function (response) {

            if (extensionName.startsWith('lookupfor') || queryParams.get("disableSAASSdk")) {
                if (response.api_domain) {
                    let domainSplits = response.api_domain.split('.');
                    let apiDomainDC = domainSplits[domainSplits.length - 1];
                    if (apiDomainDC === "cn") {
                        apiDomainDC = "com.cn";
                    }
                    if (apiDomainDC === "au") {
                        apiDomainDC = "com.au";
                    }
                    UA_OAUTH_PROCESSOR.currentSAASDomain = "." + apiDomainDC;
                }
            }

            UA_SAAS_SERVICE_APP.CURRENT_USER_INFO = response.data.users[0];
            appsConfig.APP_UNIQUE_ID = UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.id;
            appsConfig.UA_DESK_ORG_ID = UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.zuid;
            $("#saasAuthIDName").text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.full_name).attr('title', `Authorized Zoho Account: (${UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.email})`);
            setTimeout((function () {
                $('#ac_name_label_saas .ac_name_id').text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.email);
            }), 2000);
            UA_SAAS_SERVICE_APP.proceedToAppInitializationIfAPPConfigResolved();
        });


    },

    addSAASUsersToLICUtility: async function () {
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/users?type=ActiveUsers`, "GET", null);
        response.data.users.sort(function (a, b) {
            return a.full_name.localeCompare(b.full_name);
        });
        response.data.users.forEach(item => {
            UA_LIC_UTILITY.addFetchedSAASUser(item.id, item.full_name, item.email, null, item.profile.name === "Administrator", item.phone = null, url = null);
        });
        UA_LIC_UTILITY.saasUserListFetchCompleted();
    },

    proceedToAppInitializationIfAPPConfigResolved: function () {

        if (appsConfig.APP_UNIQUE_ID && appsConfig.UA_DESK_ORG_ID) {
            UA_SAAS_SERVICE_APP.renderInitialElements();
            UA_SAAS_SERVICE_APP.addSAASUsersToLICUtility();
            UA_APP_UTILITY.appsConfigHasBeenResolved();
            if (UA_SAAS_SERVICE_APP.widgetContext.module) {
                UA_SAAS_SERVICE_APP.renderInitialElementsForZohoCRMList();
            }
        }
    },

    renderInitialElements: async function () {
        // UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown('users', ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder']);
        //        UA_APP_UTILITY.renderSavedTemplatesInDropdowns();
        if (!UA_SAAS_SERVICE_APP.widgetContext.module) {
            UA_SAAS_SERVICE_APP.entityDetailFetched(null);
            console.log('Module does not exist, rendering skipped');
            $("#recip-count-holder").text("Add recipients to proceed");
            return;
        }
        await UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
        UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(UA_SAAS_SERVICE_APP.widgetContext.module, UA_SAAS_SERVICE_APP.widgetContext.entityId, UA_SAAS_SERVICE_APP.addRecordsAsRecipients);
    },

    renderInitialElementsForZohoCRMList: async function () {
        $(".ssf-new-recip-form").prepend(`<div id="ssf-recip-list-add-holdr" class="ssf-recip-choice-btn" style="margin-top: -5px;background: none;border: none;box-shadow: none;"></div>`);
        UA_SAAS_SERVICE_APP.renderContactList();
    },

    renderContactList: async function () {
        // let departmentId = await ZOHODESK.get('department.id');
        // departmentId = departmentId['department.id'];
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(this.getURI(this.URL.users.fetch.url), this.URL.users.fetch.method, null);
        // UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);

        let listDropdownItems = [];

        if (response && response.data && response.data.users && response.data.users.length > 0) {
            response.data.users.forEach(item => {
                UA_SAAS_SERVICE_APP.fetchedList[item.id] = item;
                listDropdownItems.push({
                    'label': item.full_name,
                    'value': item.id
                });
            });
        }

        UA_APP_UTILITY.renderSelectableDropdown('#ssf-recip-list-add-holdr', 'Select a filter', listDropdownItems, 'UA_SAAS_SERVICE_APP.insertManageListContacts', false, false);

    },

    insertManageListContacts: async function (listId, page = 1, page_token = null) {
        let page_param = '&per_page=100';
        if (listId && (page || page_token)) {
            if (page) {
                // let page_prompt = prompt("Please enter list page number", `${page}`);
                let page_prompt = page;
                if (!page_prompt) {
                    return;
                }
                page = page_prompt;
            }
            page_param = page ? `&page=${page}` : `&page_token=${page_token}`;
        }
        else {
            return;
        }
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${UA_SAAS_SERVICE_APP.widgetContext.module}?cvid=${listId}` + page_param, "GET", null);
        // UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('renderList', response);
        if (response && response.data && response.data.data && response.data.data.length > 0) {
            for (var i = 0; i < response.data.data.length; i++) {
                let item = response.data.data[i];
                UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id] = item;
            }
            UA_SAAS_SERVICE_APP.addRecordsAsRecipients(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
            UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);

            if (response.data.info.more_records) {
                let next_page = response.data.info.page;
                if (response.data.info.page) {
                    next_page = response.data.info.page + 1;
                    if (!next_page) {
                        return;
                    }
                }
                if (response.data.info.next_page_token && !confirm("Go to next page in this list")) {
                    return;
                }
                UA_SAAS_SERVICE_APP.insertManageListContacts(listId, response.data.info.page ? next_page : null, response.data.info.next_page_token ? response.data.info.next_page_token : null);
                return;
            }
        }
        else {
            alert(`We can't found records in list or list is empty.`);
        }

    },

    sendPhoneFieldsForRecipientType: function (fieldsArray) {
        fieldsArray.forEach(item => {
            if (item.data_type === "phone") {
                UA_APP_UTILITY.addRecipientPhoneFieldType(item.api_name, item.display_label);
            }
        });
    },

    generateSchemaMap: async function () {
        var modulesListResponse = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/settings/modules?module=${module}`, "GET", null);
    },
    assignNestedObjectasproperty: function(object_data){
        let obj = Object.assign({},object_data);;
        let objectKeyMaps = {
            Created_By: 'Created_By.name',
            Modified_By: 'Modified_By.name',
        }
        for(let i in objectKeyMaps){
            let nestedObj = objectKeyMaps[i].split('.');
            obj[i] = obj[nestedObj[0]][nestedObj[1]]; 
          
        }
        return obj;
    },
    populateModuleItemFieldsInDropDown: async function (module, target, fieldsCallback = (() => { })) {
        if (!module || UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.includes(module) == true) {
            return;
        }
        UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.push(module);
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/settings/fields?module=${module}`, "GET", null);
        // UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        // console.log(response);

        var dropDownValues = [];
        response.data.fields.forEach(item => {
            dropDownValues.push({
                "label": item.display_label,
                "value": (module.toUpperCase() === "USERS" ? "CURRENT_USER" : module.toUpperCase()) + '.' + item.api_name
            });
        });
        fieldsCallback(response.data.fields);
        if (typeof target === "string") {
            target = [target];
        }
        target.forEach(item => {
            UA_APP_UTILITY.renderSelectableDropdown(item, `Insert ${module} fields`, dropDownValues, 'UA_APP_UTILITY.templateMergeFieldSelected', false, true);
        });
        return true;
    },

    injectShowSearchContact: async function () {
        // UA_SAAS_SERVICE_APP.widgetContext.module = UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module:"Contacts";
        $(".ssf-new-recip-form").prepend(`<div id="searchAndAddFromContactBtn" class="ssf-recip-choice-btn" onclick="$('.item-list-popup').show()">
                                         <i class="material-icons">search</i> Search from ${UA_SAAS_SERVICE_APP.widgetContext.module ? UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase() : "Contacts"}
                                      </div>`);
        $('.pageContentHolder').append(`<div class="item-list-popup" style="display:none">
                        <div class="item-list-popup-title">
                            Select a ${UA_SAAS_SERVICE_APP.widgetContext.module ? UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase() : "Contacts"} <div class="pop-win-close" onclick="$('.item-list-popup').hide();">x</div>
                        </div>
                        <div class="item-search-box">
                            <input class="input-form" id="contact-seach-name" type="text" autocomplete="off" placeholder="name of the ${UA_SAAS_SERVICE_APP.widgetContext.module ? UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase() : "Contacts"}"/><br>
                            <div class="btn-save" onclick="UA_SAAS_SERVICE_APP.searchRecordsAndRender($('#contact-seach-name').val(),UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module:'Contacts')">
                                Search ${UA_SAAS_SERVICE_APP.widgetContext.module ? UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase() : "Contacts"}
                            </div>
                            <div class="btn-reset" onclick="UA_SAAS_SERVICE_APP.resetsearchContacts()">
                                Reset
                            </div>
                        </div>
                        <div class="item-list-popup-content" id="contact-search-items">

                        </div>
                    </div>`);
    },

    resetsearchContacts: function () {
        $("#contact-seach-name").val("");
        $("#contact-search-items").html("");
    },

    searchRecordsAndRender: async function (searchTerm, module) {
        if (!valueExists(searchTerm) || searchTerm.trim().length < 2) {
            return;
        }
        UA_SAAS_SERVICE_APP.widgetContext.module = UA_SAAS_SERVICE_APP.widgetContext.module ? UA_SAAS_SERVICE_APP.widgetContext.module : "Contacts";
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${module}/search?word=${searchTerm}`, "GET", {});
        if (response) {
            console.log(response);
            // UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
            $("#contact-search-items").html(" ");
            if (response.data && response.data["data"].length > 0) {
                var search_items = response.data["data"];
                if (search_items && search_items.length > 50) {
                    search_items.splice(0, 49);
                }
                UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module] = {};
                search_items.forEach((obj) => {
                    UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][obj.id] = obj;
                    var fullName = "";
                    if (module) {
                        switch (module) {
                            case ("Contacts"): {
                                fullName = obj.Full_Name ? obj.Full_Name : "";
                                fullName = fullName ? fullName : "Contact#" + obj.id;
                                break;
                            }
                            case ("Deals"): {
                                fullName = obj.Deal_Name ? obj.Deal_Name : obj.Contact_Name ? obj.Contact_Name.name : "Deal#" + obj.id;
                                break;
                            }
                            case ("Leads"): {
                                fullName = obj.Full_Name ? obj.Full_Name : "";
                                fullName = fullName ? fullName : "Lead#" + obj.id;
                                break;
                            }
                        }
                    }
                    let itemHTML = `<div class="item-list-popup-item" style="cursor:pointer;" onclick="UA_SAAS_SERVICE_APP.selectPhoneNumber('${obj.id}', '', '${fullName}','${module}','${true}')"><span class="c-name"> ${fullName} </span>`;
                    // itemHTML =  itemHTML + `${'<span class="c-phone '+(UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[module+"_"+obj.id] ? 'alreadyadded': '')+'"><i class="material-icons">phone_iphone</i>(phone)</span>'}`;
                    itemHTML = itemHTML + `</div>`;
                    $("#contact-search-items").append(itemHTML);
                });
            }
            else {
                $("#contact-search-items").html("No records found. <br><br> <span class=\"c-silver\"> This search will include <br> only customers having phone numbers</span>");
                return;
            }
        }
    },

    selectPhoneNumber: async function (id, number, name, module, isSelected) {
        var receipNumberID = module + "_" + id;
        if (!UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] && !UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id]) {
            UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] = {
                'id': id,
                'name': name,
                'module': module,
                'isSelected': isSelected
            };
            $.extend(UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id], UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID]);
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id] = UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id];
            // UA_SAAS_SERVICE_APP.renderInitialElements(module,id);

            await UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
            // UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(module, id, UA_SAAS_SERVICE_APP.addRecordsAsRecipients);
            // UA_SAAS_SERVICE_APP.addRecordsAsRecipients([UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id]]);
        }
        else {
            console.log(module + ' already added');
            // showErroWindow("Alert Message",`This ${module.toLocaleLowerCase()} is already added.`)
            alert(`This ${module.toLocaleLowerCase()} is already added.`);
            return;
        }
    },

    getCurrentUserInfo: async function (callback) {
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback(this.getURI(this.URL.currentuser.fetch.url), callback);
    },

    getCurrentOrgInfo: async function (callback) {

        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/org`, "GET", null);
        if (response.ok) {
            callback(response);
        }

    },

    getAPIResponseAndCallback: async function (url, callback) {
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${url}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        callback(response);
    },

    showErrorIfInAPICall: function (response) {
        if (response.code === 401) {
            UA_OAUTH_PROCESSOR.showReAuthorizationError(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED);
        }
    },

    initiateAuthFlow: function () {
        UA_OAUTH_PROCESSOR.initiateAuth(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED);
    },

    getURI: function (url, ...args) {
        let a = 0;
        let urlRelaced = args && args.length ? url.split('{').map(i => {
            if(!i.includes('}')) return i;
            let spl = i.split('}').map((j, pos) => {
                return pos == 0 ? args[pos] : j;
            }).join('');
            return spl;
        }).join('') : url;
        console.log('urlRelaced', urlRelaced)
        return this.getAPIBaseUrl() + urlRelaced;
    },

    fetchStoreExecuteOnRecordDetails: async function (module, entityId, callback) {
        var recordIds = entityId.includes(',') ? entityId.split(',') : [entityId];
       
        if (!module) {
            UA_APP_UTILITY.fetchedAllModuleRecords();
            return;
        }

        recordIds.forEach(async recordId => {
            let apiUrl = this.URL[module].fetch.url;
            $("#recip-count-holder").text(`fetching ${recordIds.length} ${module.toLocaleLowerCase()} information...`);
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(this.getURI(apiUrl, recordId), "GET", null);

            if (response && response.data.data.length > 0) {
                let fetchedDetails = response.data.data[0];
                UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordId] = this.assignNestedObjectasproperty(fetchedDetails);
                UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordId].Mobile = fetchedDetails.Mobile;
                UA_APP_UTILITY.addRecipientPhoneFieldType("Mobile", "Mobile");

                UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordId].Phone = fetchedDetails.Phone;
                UA_APP_UTILITY.addRecipientPhoneFieldType("Phone", "Phone");

            }
            if (Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS).length > 1) {
                UA_APP_UTILITY.changeCurrentView('MESSAGE_FORM');
            }
            $("#recip-count-holder").text(`Selected ${recordIds.length} ${module.toLocaleLowerCase()}`);
            callback(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
            UA_SAAS_SERVICE_APP.entityDetailFetched();
        });

    },

    addRecordsAsRecipients: function (contactsList) {
        for (var item in contactsList) {
            let contactItem = contactsList[item];
            if ($(`[data-recip-id=${contactsList[item]['id']}]`).length === 0) {
                contactItem.name = contactItem.Full_Name;
                UA_APP_UTILITY.addInStoredRecipientInventory(contactItem);
            }
        }
    },

    addSentSMSAsRecordInHistory: async function (sentMessages) {
        var historyDataArray = [];
        var credAdProcess3 = curId++;

        showProcess(`Adding ${Object.keys(sentMessages).length} messages to history...`, credAdProcess3);

        for (var sentMessageId in sentMessages) {
            let sentMessage = sentMessages[sentMessageId];
            let historyData = {};

            let message = `ZOHO Recruit Extension 
                           ${sentMessage.channel} ${sentMessage.status} from ${sentMessage.from} to ${sentMessage.to}:
                           
                           ${sentMessage.message}`;
            
            historyData['Note_Content'] = message;
            historyData['Parent_Id'] = sentMessage.moduleId;
            historyData['se_module'] = sentMessage.module;
            historyData['Note_Title'] = "Note";
          
            historyDataArray.push(historyData);
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/Notes`, "POST", {
                data: [historyData]
            });
            // UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
            if (response && response.data && response.data.data && response.data.data.length) {
                let responseData = response.data.data;
                await UA_SAAS_SERVICE_APP.storeSAASServiceTPAEntityMap(extensionName, appsConfig.UA_DESK_ORG_ID, sentMessage.id, { 'saas_history_entity_id': responseData[0].details.id });
            }
        }
        // var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${apiNameConfigObject.API_NAME+apiNameConfigObject.HISTORY_MODULE_NAME}`, "POST", {
        //     data: historyDataArray
        // });
        processCompleted(credAdProcess3);
        // console.log('addSentSMSAsRecordInHistory',response);
        //alert('All Messages has been added to Pinnacle SMS History');

    },

    storeSAASServiceTPAEntityMap: async function (extension, oid, tpaEntityID, properties) {
        try {
            if (!tpaEntityID || !properties || !oid || !extension) {
                return false;
            }
            properties.t = Date.now();
            await firebase.firestore().collection('serviceEntityMapsForTPA').doc(`apps/${extension}/${oid}/entities/${tpaEntityID}`).set(properties, { merge: true });
        }
        catch (ex) {
            console.log('error_while_adding_storeSAASServiceTPAEntityMap', JSON.stringify(ex));
        }
    },

    changedCurrentView: async function () {
        if (UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CHAT) {
            await ZOHO.CRM.UI.Resize({ height: '575', width: '400' });
        }
        if (UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CONVERSATIONS) {
            await ZOHO.CRM.UI.Resize({ height: '575', width: '1200' });
        }
        if (UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.MESSAGE_FORM) {
            await ZOHO.CRM.UI.Resize({ height: '600', width: '700' });
        }
    },

    entityDetailFetched: function (data) {
        setTimeout(() => { UA_APP_UTILITY.fetchedAllModuleRecords(); }, 2000);
        if (typeof UA_TPA_FEATURES.callOnEntityDetailFetched === "function") {
            UA_TPA_FEATURES.callOnEntityDetailFetched(data);
        }
    },
    searchRecord: async function (module, query) {
        return await ZOHO.CRM.API.searchRecord({ Entity: module, Type: "criteria", Query: query, delay: "false" }).then(function (response) {
            return response.data;
        });
    },
    LOOKUP_SUPPORTED_MODULES: {
        "Contacts": {
            "autoLookup": true,
            "apiName": "Contacts"
        },
        "Candidates": {
            "autoLookup": true,
            "apiName": "Candidates"
        }
    },
    allModuleFieldsFetched: function () {
        let fieldsPresentModuleCount = 0;
        Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).forEach(item => {
            if (UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[item].fields && Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[item].fields).length > 0) {
                fieldsPresentModuleCount++;
            }
        });
        return Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).length === fieldsPresentModuleCount;
    },
    retrieveModuleFieldsForLookUp: function (callback) {
        $('#ua-loo-search-field-ddholder').hide();
        if (UA_SAAS_SERVICE_APP.allModuleFieldsFetched()) {
            callback();
        }
        var primaryFieldsToCreate = ["Last_Name", "First_Name", "Phone", "Mobile", "Email", "Description", "Account_Name"];
        var primaryFieldsToView = ["Phone", "Mobile", "Email", "Description", "Modified_Time", "id"];
        Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).forEach(async moduleId => {
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/settings/fields?module=${moduleId}`, "GET", null);
            let fieldAPINameMap = {};
            response.data.fields.forEach(fieldItem => {
                if (["ownerlookup", "lookup"].includes(fieldItem.data_type)) {
                    fieldItem.field_read_only = true;
                }
                fieldAPINameMap[fieldItem.api_name] = fieldItem;
            });
            fieldAPINameMap.id = {
                'display_label': 'ID',
                'data_type': 'number',
                'api_name': 'id',
                'field_read_only': true
            };
            let primaryFieldsToViewForModule = ["Phone", "Mobile", "Email", "Description", "Modified_Time", "id"];
            if (moduleId === "Leads" || moduleId === "Contacts") {
                primaryFieldsToViewForModule.push("Last_Name");
                primaryFieldsToViewForModule.push("First_Name");
            }
            if (moduleId === "Accounts") {
                primaryFieldsToViewForModule.push("Account_Name");
            }
            UA_TPA_FEATURES.setCreateModuleRecordFields(moduleId, primaryFieldsToCreate);
            UA_TPA_FEATURES.setViewModuleRecordFields(moduleId, primaryFieldsToViewForModule);
            UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[moduleId].fields = fieldAPINameMap;
            if (UA_SAAS_SERVICE_APP.allModuleFieldsFetched()) {
                callback();
            }
        });
    },
    getSearchedResultItems: async function (searchTerm, module, field) {
        UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS = {};
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${module}/search?phone=${searchTerm}`, "GET", {});
        if (response.status === 204) {
            response.data = {
                'data': []
            };
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS = {};
        }
        response.data.data.forEach(item => {
            item.name = item.Full_Name || item.Deal_Name || item.Contact_Name || item.Account_Name || item.Deal_Name;
            item.webURL = "https://crm.zoho" + UA_OAUTH_PROCESSOR.currentSAASDomain + "/crm/tab/" + module + '/' + item.id;
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id] = item;
        });
        return response.data.data;
    },

    getNotesOfRecord: async function (module, entityId) {
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/Notes`, "GET", null);
        let notesArray = [];
        if (response.status === 204) {
            return notesArray;
        }
        response.data.data.forEach(noteItem => {
            notesArray.push({
                'id': noteItem.id,
                'content': noteItem.Note_Content,
                'title': noteItem.Note_Title,
                'time': noteItem.Modified_Time
            });
        });
        return notesArray;
    },

    addModuleRecordNote: async function (module, entityId, noteContent) {

        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/Notes`, "POST", { "data": [{ 'message': noteContent, 'Parent_Id': entityId, 'se_module': module }] });
        if (response.data.data.length > 0) {
            return response;
        }
        else {
            return {
                error: true
            }
        }
    },

    updateRecordByField: async function (module, entityId, fieldName, fieldValue) {
        let updateObjPayload = {
            id: entityId
        };
        updateObjPayload[fieldName] = fieldValue;
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl(3)}/${module}/${entityId}`, "PUT", { "data": [updateObjPayload] });
        if (response.data.data) {
            return response;
        }
        else {
            return {
                error: {
                    'message': 'Error, Try again'
                }
            }
        }
    },

    createRecordInModule: async function (module, fieldMap) {
        if (module === "Leads") {
            if (!valueExists(fieldMap.Lead_Source)) {
                fieldMap.Lead_Source = "WhatsApp";
            }
        }
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${module}`, "POST", { "data": [fieldMap] });
        if (response.data.data) {
            let resultItemId = response.data.data[0].details.id;
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${module}/${resultItemId}`, "GET", null);
            let recordItem = response.data.data[0];
            recordItem.name = recordItem.Full_Name || recordItem.Deal_Name || recordItem.Contact_Name;
            recordItem.webURL = "https://crm.zoho" + UA_OAUTH_PROCESSOR.currentSAASDomain + "/crm/tab/" + module + '/' + recordItem.id;
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[resultItemId] = recordItem;
            return recordItem;
        }
        else {
            return {
                error: {
                    'message': 'Error, Try again'
                }
            };
        }
    },

    getNewRecordInSAASWebURL: function (module) {
        return "https://crm.zoho" + UA_OAUTH_PROCESSOR.currentSAASDomain + "/crm/tab/" + module + "/create";
    }
};