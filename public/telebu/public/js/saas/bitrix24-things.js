const UA_OAUTH_PROCESSOR = {
     domainExtnMap: {
        "IN": ".in",
        "EU": ".eu",
        "CN": ".com.cn",
        "AU": ".com.au",
        "US": ".com"
    },
    currentSAASDomain: ".com",
    initiateAuth: function (scope) {
        var scopeText = typeof scope === "object" ? scope.join(' ') : scope;
//        $('.ua_service_login_btn').text('Processing...');
        if(!extensionName){
            showErroWindow('Application error!', 'Unable to load app type. Kindly <a target="_blank" href="https://apps.ulgebra.com/contact">contact developer</a>');
            return;
        }
        window.open(`http://${UA_SAAS_SERVICE_APP.widgetContext.bitrix24_domain}/oauth/authorize/?client_id=${UA_TPA_FEATURES.clientIds[extensionName]}&redirect_uri=${APP_ENV_CLIENT_URL}/oauth-home&response_type=code&state=${currentUser.uid}:::${extensionName}`, 'Authorize', 'popup');
    },
    
    getAPIResponse: async function (url, method, data) {
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var returnResponse = await UA_APP_UTILITY.makeUAServiceAuthorizedHTTPCall(url, method, data, {"Authorization": "Bearer {{AUTHTOKEN}}", "Content-Type": "application/json"});
        removeTopProgressBar(credAdProcess3);
        return returnResponse;
    },
    
    showReAuthorizationError: function(scope){
        if($('#UA_SAAS_AUTH_BTN').length > 0){
            console.log('SaaS auth window already showing.. skipping.');
            return;
        }
        var authHtmlText = `You need to authorize your Bitrix24 Account to proceed <br><br> <button id="UA_SAAS_AUTH_BTN" class="ua_service_login_btn ua_primary_action_btn" onclick="UA_OAUTH_PROCESSOR.initiateAuth('${scope.join(' ')}')">Authorize Now</button>`;
        if(!UA_SAAS_SERVICE_APP.widgetContext.bitrix24_domain){
            authHtmlText = `You need to authorize your Bitrix24 Account to proceed <br><br>
                <div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
                    <div style="font-size: 18px;margin-bottom: 5px;margin-top: 20px;">Bitrix24 Domain</div>
                    <input id="inp_saas_api_domain" type="text" placeholder="Enter Bitrix24 domain" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;" onkeyup="UA_SAAS_SERVICE_APP.widgetContext.bitrix24_domain = $('#inp_saas_api_domain').val().replace('https://','')">
                </div>
                <button id="UA_SAAS_AUTH_BTN" class="ua_service_login_btn ua_primary_action_btn" onclick="UA_OAUTH_PROCESSOR.initiateAuth('${scope.join(' ')}')">Authorize Now</button>`;
        }
       showErroWindow("Bitrix24 Authentication Needed!", authHtmlText, false, false, 1000);
    }
    
};