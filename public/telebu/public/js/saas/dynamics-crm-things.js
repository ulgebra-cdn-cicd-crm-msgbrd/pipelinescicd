var UA_SAAS_SERVICE_APP = {
    
    CURRENT_USER_INFO : null,
    CURRENT_ORG_INFO : null,
    CURRENT_OAUTH_SCOPE_NEEDED : [],
    FETCHED_RECORD_DETAILS: {},
    CURRENT_MODULE_FIELDS_DROPDOWN : [],
    SELECTED_RECEIPS: {},
    SEARCH_MODULE_RECORD_MAP: {},
    fetchedList : {},
    widgetContext: {},
    
    supportedIncomingModules: [
        {
            "selected": true,
            "value": "contact",
            "label": "Contacts"
        },
        {
            "value": "account",
            "label": "Accounts"
        },
        {
            "value": "lead",
            "label": "Leads"
        }
    ],
    
    getAPIBaseUrl : function(){
        return `https://${UA_OAUTH_PROCESSOR.currentSAASDomain}`;
    },
    
    initiateAPPFlow: async function(){
        UA_SAAS_SERVICE_APP.initiateAPPFlow_SA();
    },
    
    initiateAPPFlow_SA: function(){

        if(!UA_SAAS_SERVICE_APP.widgetContext.module && queryParams.get("module")){
            UA_SAAS_SERVICE_APP.widgetContext.module = queryParams.get("module");
            UA_SAAS_SERVICE_APP.widgetContext.entityId = queryParams.get("entityId");
            UA_SAAS_SERVICE_APP.injectShowSearchContact(); 
        }
        $("#ac_name_label_saas .anl_servicename").text('Microsoft Dynamics CRM');
        UA_SAAS_SERVICE_APP.getCurrentUserInfo(async function(response){
            UA_OAUTH_PROCESSOR.currentSAASDomain = response.api_domain;
            UA_SAAS_SERVICE_APP.CURRENT_USER_INFO = response.data;
            appsConfig.APP_UNIQUE_ID = UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.UserId+"";
            appsConfig.UA_DESK_ORG_ID = UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.OrganizationId+"";

            let userDataRes = await UA_OAUTH_PROCESSOR.getAPIResponse(`/api/data/v9.2/systemusers(${appsConfig.APP_UNIQUE_ID})`, "GET", null);
            UA_SAAS_SERVICE_APP.CURRENT_USER_INFO = userDataRes.data;
            $("#saasAuthIDName").text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.fullname).attr('title', `Authorized Dynamics CRM Account: ${UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.domainname}`);
            setTimeout((function(){
                $('#ac_name_label_saas .ac_name_id').text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.domainname);
            }), 2000);
            UA_SAAS_SERVICE_APP.proceedToAppInitializationIfAPPConfigResolved();
            UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown('users', ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder']);
        });
    },
        
    proceedToAppInitializationIfAPPConfigResolved: function(){
        if(appsConfig.APP_UNIQUE_ID && appsConfig.UA_DESK_ORG_ID){
            UA_SAAS_SERVICE_APP.renderInitialElements();
            UA_SAAS_SERVICE_APP.addSAASUsersToLICUtility();
            UA_APP_UTILITY.appsConfigHasBeenResolved();
            if(UA_SAAS_SERVICE_APP.widgetContext.module)
            {
                UA_SAAS_SERVICE_APP.renderInitialElementsForHubList();
            }
            // UA_SAAS_SERVICE_APP.retrieveModuleFieldsForLookUp(console.log);
        }
    },
    
    renderInitialElements: async function(){
        if(!UA_SAAS_SERVICE_APP.widgetContext.module){
            UA_SAAS_SERVICE_APP.entityDetailFetched(null);
            console.log('Module does not exist, rendering skipped');
            $("#recip-count-holder").text("Add recipients to proceed");
            return;
        }
        UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(UA_SAAS_SERVICE_APP.widgetContext.module, UA_SAAS_SERVICE_APP.widgetContext.entityId, function(contactList){
            UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
            UA_SAAS_SERVICE_APP.addRecordsAsRecipients(contactList);
        });
    },

    renderInitialElementsForHubList: async function(){
        $(".ssf-new-recip-form").prepend(`<div id="ssf-recip-list-add-holdr" class="ssf-recip-choice-btn" style="margin-top: -5px;background: none;border: none;box-shadow: none;"></div>`);
        UA_SAAS_SERVICE_APP.renderContactList();
    },
    
    renderContactList: async function(){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/api/data/v9.2/savedqueries?$select=name,savedqueryid&$filter=returnedtypecode eq '${UA_SAAS_SERVICE_APP.widgetContext.module}'`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('renderList',response);
        if(!response && !response.data && response.data.value < 1){
            return;
        }
        let listDropdownItems = [];
        response.data.value.forEach(item=>{
            item.id = item.savedqueryid;
            UA_SAAS_SERVICE_APP.fetchedList[item.id] = item;
            listDropdownItems.push({
                'label': item.name,
                'value': item.id
            });
        });

        UA_APP_UTILITY.renderSelectableDropdown('#ssf-recip-list-add-holdr', 'Select a List', listDropdownItems, 'UA_SAAS_SERVICE_APP.insertManageListContacts', false, false);
    
    },

    insertManageListContacts: async function(listId){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/api/data/v9.2/${UA_SAAS_SERVICE_APP.widgetContext.module}s?savedQuery=${listId}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('renderList',response);
        if(response.data && response.data.value && response.data.value.length > 0) {
            let recIds = [];
            response.data.value.forEach(item=>{
                let id = item[`${UA_SAAS_SERVICE_APP.widgetContext.module}id`];
                recIds.push(id);
            });
            UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(UA_SAAS_SERVICE_APP.widgetContext.module, recIds, async function(contactList){
                UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
                UA_SAAS_SERVICE_APP.addRecordsAsRecipients(contactList);
            });
        }
            
    },
    
    addSAASUsersToLICUtility: async function(){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/api/data/v9.2/systemusers?$filter=isdisabled eq false and issyncwithdirectory eq true and islicensed eq true`, "GET", null);
        response.data.value.sort(function(a, b) {
            return a.fullname.localeCompare(b.fullname);
        });
        response.data.value.forEach(item=>{
            UA_LIC_UTILITY.addFetchedSAASUser(item.systemuserid, item.fullname, item.internalemailaddress, null, item.isemailaddressapprovedbyo365admin === true, item.address1_telephone1, url = null);
        });
        UA_LIC_UTILITY.saasUserListFetchCompleted();
    },
    
    sendPhoneFieldsForRecipientType: function(fieldsArray){
        fieldsArray.forEach(item=>{
            if(item.data_type === "phone" || item.data_type === "mobilephone" || item.fieldType === "phonenumber"){
                UA_APP_UTILITY.addRecipientPhoneFieldType(item.api_name, item.display_label);
            }
        });
    },
    FETCHED_MODULE_FIELDS: {},
    populateModuleItemFieldsInDropDown: async function (module, target, fieldsCallback=(()=>{})){
        if(!module || UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.includes(module) === true){
            return false;
        }
        UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.push(module);
        UA_SAAS_SERVICE_APP.FETCHED_MODULE_FIELDS[module] = {};
        var moduleToLoad = module === "users" ? UA_SAAS_SERVICE_APP.CURRENT_USER_INFO : UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[UA_SAAS_SERVICE_APP.widgetContext.entityId.split(',')[0]];
        var moduleFields = [];
        if(module ==="users"){
            moduleFields = Object.keys(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO);
        }
        else{
            // var modulePluralWord = module === "COMPANY" ? "companies": module.toLowerCase()+"s";
            var moduleFieldsResponse = await UA_OAUTH_PROCESSOR.getAPIResponse(`/api/data/v9.2/EntityDefinitions(LogicalName='${module}')/Attributes?$filter=(AttributeType ne 'Virtual' and IsValidODataAttribute eq true)`, 'GET', null);
            if(moduleFieldsResponse.data && moduleFieldsResponse.data.value){
                moduleFieldsResponse.data.value.forEach(item=>{
                    if((["None", "Url"].includes(item.Format)) || (!["String", "Integer", "Memo"].includes(item.AttributeType))){
                        return;
                    }
                    moduleFields.push(item);
                });
                moduleFields.sort(function(a, b) {
                    return a.LogicalName.localeCompare(b.LogicalName);
                });
            }
        }
        var fieldsArray = [];
        var dropDownValues = [];
        moduleFields.forEach(item=>{
            if(module ==="users"){
                let itemVal = moduleToLoad[item];
                if(typeof itemVal !== "object" && itemVal !== null && typeof itemVal!=="boolean"){
                    UA_SAAS_SERVICE_APP.FETCHED_MODULE_FIELDS[module][item] = item;
                    fieldsArray.push({
                        "api_name": item,
                        "display_label": item
                    });
                    dropDownValues.push({
                        "label": item,
                        "value": (module.toUpperCase() === "USERS" ? "CURRENT_USER": module.toUpperCase())+'.'+item
                    });
                }
            }
            else{
                UA_SAAS_SERVICE_APP.FETCHED_MODULE_FIELDS[module][item.LogicalName] = item;
                if(UA_SAAS_SERVICE_APP.widgetContext.module !== module){
                    item.name = module.toLowerCase() + "_"+ item.LogicalName;
                }
                if(!item.DisplayName || !item.DisplayName.UserLocalizedLabel || !item.DisplayName.UserLocalizedLabel.Label){
                    console.log(item);
                }
                fieldsArray.push({
                    "api_name": item.LogicalName,
                    "display_label": item.DisplayName.UserLocalizedLabel.Label
                });
                dropDownValues.push({
                    "label": item.DisplayName.UserLocalizedLabel.Label,
                    "value": (UA_SAAS_SERVICE_APP.widgetContext.module ? UA_SAAS_SERVICE_APP.widgetContext.module.toUpperCase() : module.toUpperCase())+'.'+item.LogicalName
                });
            }
        });
        fieldsCallback(fieldsArray);
        if(typeof target === "string"){
            target = [target];
        }
        target.forEach(item=>{
            UA_APP_UTILITY.renderSelectableDropdown(item, `Insert ${module} fields`, dropDownValues, 'UA_APP_UTILITY.templateMergeFieldSelected', false, true);
        });
        return true;
    },
    
    injectShowSearchContact :async function(){
        $(".ssf-new-recip-form").prepend(`<div id="searchAndAddFromContactBtn" class="ssf-recip-choice-btn" onclick="$('.item-list-popup').show()">
                                            <i class="material-icons">search</i> Search from ${UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()}
                                      </div>`);
        $('.pageContentHolder').append(`<div class="item-list-popup" style="display:none">
                        <div class="item-list-popup-title">
                            Select ${UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()} <div class="pop-win-close" onclick="$('.item-list-popup').hide();">x</div>
                        </div>
                        <div class="item-search-box">
                            <input class="input-form" id="contact-seach-name" type="text" autocomplete="off" placeholder="name of the ${UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()}"/><br>
                            <div class="btn-save" onclick="UA_SAAS_SERVICE_APP.searchRecordsAndRender($('#contact-seach-name').val(),UA_SAAS_SERVICE_APP.widgetContext.module)">
                                Search ${UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()}
                            </div>
                            <div class="btn-reset" onclick="UA_SAAS_SERVICE_APP.resetsearchContacts()">
                                Reset
                            </div>
                        </div>
                        <div class="item-list-popup-content" id="contact-search-items">

                        </div>
                    </div>`);
    },

    resetsearchContacts: function(){
        $("#contact-seach-name").val("");
        $("#contact-search-items").html("");
    },

    searchRecordsAndRender: async function(searchTerm,module){
        if(!valueExists(searchTerm)){
            return;
        }
        let seachField = module == "account"?"name":"fullname";
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/api/data/v9.2/${module}s?$filter=startswith(${seachField},'${searchTerm}')`, "GET", null);
            if(response && response.data && response.data.value && response.data.value.length > 0) {
                console.log(response);
                UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
                $("#contact-search-items").html(" ");
                if(response.data["value"].length>0)
                {
                    var search_items = response.data["value"];
                    if(search_items && search_items.length > 50){
                        search_items.splice(0,49);
                    }
                    UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module] = {};
                    search_items.forEach((obj)=>{
                        let objId = obj[`${module}id`];
                        UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][objId] = obj;
                        let fullName = module == "account"?obj.name:obj.fullname;
                        let itemHTML = `<div class="item-list-popup-item" style="cursor:pointer;" onclick="UA_SAAS_SERVICE_APP.selectPhoneNumber('${objId}', '${module}', '${fullName}','${module}','${true}')"><span class="c-name"> ${fullName} </span>`;
                        // itemHTML =  itemHTML + `${'<span class="c-phone '+(UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[module+"_"+obj.id] ? 'alreadyadded': '')+'"></span>'}`;
                        itemHTML =  itemHTML + `</div>`;
                        $("#contact-search-items").append(itemHTML);
                    });
                }
                else
                {
                    $("#contact-search-items").html("No records found. <br><br> <span class=\"c-silver\"> This search will include <br> only customers having phone numbers</span>");
                    return;
                }
            }
    },

    selectPhoneNumber:async function(id,number,name,module,isSelected){
        var receipNumberID = module + "_" + id;
        if(!UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] && !UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id]){
            UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] = {
                'id': id,
                'name': name,
                'module': module,
                'isSelected': isSelected
            };
            $.extend(UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id],UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID]) ;
            // UA_SAAS_SERVICE_APP.renderInitialElements(module,id);
            UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(module,id,UA_SAAS_SERVICE_APP.addRecordsAsRecipients);
            await UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
            // UA_SAAS_SERVICE_APP.addRecordsAsRecipients([UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id]]);
        }
        else {
            console.log(module+' already added');
            showErroWindow("Alert Message",`This ${module.toLocaleLowerCase()} is already added.`)
            // alert(`This ${module.toLocaleLowerCase()} is already added.`);
            return;
        }
    },
    
    getCurrentUserInfo : async function(callback){
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/api/data/v9.2/WhoAmI', callback);
    },
    
    getAPIResponseAndCallback: async function(url, callback){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(url, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        callback(response);
    },
    
    showErrorIfInAPICall : function(response){
        if(response.code === 401){
            UA_OAUTH_PROCESSOR.showReAuthorizationError(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED);
        }
    },
    
    initiateAuthFlow: function(){
        UA_OAUTH_PROCESSOR.showReAuthorizationError(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED);
    },
    
    fetchStoreExecuteOnRecordDetails: async function(module, recordIds, callback){
        if(!module){
            return;
        }
        if(typeof recordIds == 'string' || typeof recordIds == "number") {
            recordIds = recordIds.toString().split(",");
        }
        $("#recip-count-holder").text(`fetching ${module.toLocaleLowerCase()} information...`);
        
        for(var i=0; i < recordIds.length; i++){
            let id = recordIds[i];
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/api/data/v9.2/${module}s(${id})`, "GET", null);
            UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
            // console.log('fetchStoreExecuteOnRecordDetails',response);
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id] = response.data;
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id].id = id;
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id].email = response.data.emailaddress1;
            if(Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS).length > 1){
                UA_APP_UTILITY.changeCurrentView('MESSAGE_FORM');
            }
            
        }
        $("#recip-count-holder").text(`Selected ${recordIds.length} ${module}`);
        callback(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
        UA_SAAS_SERVICE_APP.entityDetailFetched();
    },
    
     addRecordsAsRecipients: function(contactsList){
        for(var item in contactsList){
            if($(`[data-recip-id=${contactsList[item]['id']}]`).length == 0)
            {
                ["mobilephone", "telephone1","telephone2","telephone3"].forEach(mobileFieldItem => {
                    if (contactsList[item][mobileFieldItem]) {
                        UA_APP_UTILITY.addRecipientPhoneFieldType(mobileFieldItem, mobileFieldItem);
                    }
                });
                if(!contactsList[item].name){
                    contactsList[item].name = "";
                    if(contactsList[item].fullname){
                        contactsList[item].name = contactsList[item].fullname;
                    }
                }
                UA_APP_UTILITY.addInStoredRecipientInventory(contactsList[item]);
            }
        }
    },
    
    addSentSMSAsRecordInHistory: async function(sentMessages){
        let credAdProcess3 = curId++;
        showProcess(`Adding ${Object.keys(sentMessages).length} messages to history...`, credAdProcess3);
        for(var sentMessageId in sentMessages){
            let sentMessage = sentMessages[sentMessageId];
            let recModule = valueExists(sentMessage.module)?sentMessage.module:UA_SAAS_SERVICE_APP.widgetContext.module;
            let recId = valueExists(sentMessage.moduleId)?(sentMessage.moduleId.indexOf("newmanual")!==0?sentMessage.moduleId:UA_SAAS_SERVICE_APP.widgetContext.entityId.split(',')[0]):UA_SAAS_SERVICE_APP.widgetContext.entityId.split(',')[0];
            let commentContent =`${sentMessage.channel} ${sentMessage.status} from ${sentMessage.from} to ${sentMessage.to}:
                             ${sentMessage.message}`;

            var entity = {};
            entity.subject = `${appPrettyName} Outgoing :`;
            entity.notetext = commentContent;
            entity.isdocument = false;
            entity[`objectid_${recModule}@odata.bind`] = `/${recModule}s(${recId})`;
            let response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/api/data/v9.2/annotations`, "POST", entity);
            console.log(response);
            UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        }
        processCompleted(credAdProcess3);
        //alert('All Messages has been added to Pinnacle SMS History');
        
    },
    entityDetailFetched: function(data){
        if(typeof UA_TPA_FEATURES.callOnEntityDetailFetched === "function"){
            UA_TPA_FEATURES.callOnEntityDetailFetched(data);
        }
    },

// LookUp functions
    LOOKUP_SUPPORTED_MODULES: {
        "contact": {
            "autoLookup": true,
            "apiName": "contact"
        },
        "account": {
            "autoLookup": true,
            "apiName": "account"
        },
        "lead": {
            "autoLookup": true,
            "apiName": "lead"
        }
    },

    allModuleFieldsFetched: function(){
        let fieldsPresentModuleCount = 0;
        Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).forEach(item=>{
            if(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[item].fields && Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[item].fields).length > 0){
                fieldsPresentModuleCount++;
            }
        });
        return Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).length === fieldsPresentModuleCount;
    },

    retrieveModuleFieldsForLookUp: function(callback){
        $('#ua-loo-search-field-ddholder').hide();
        if (UA_SAAS_SERVICE_APP.allModuleFieldsFetched()) {
            callback();
        }
        var primaryFieldsToCreate = [];
        let primaryFieldsToViewForModule = ["id"];

        Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).forEach(async moduleId=>{
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/api/data/v9.2/EntityDefinitions(LogicalName='${moduleId}')/Attributes?$filter=(AttributeType ne 'Virtual' and IsValidODataAttribute eq true)`, "GET", null);
            let fieldAPINameMap = {};
            // await response.data.results.forEach(fieldItem=>{
            response.data.value.forEach((fieldItem)=>{
                // let fieldItem = response.data.value[i];
                if((["None", "Url"].includes(fieldItem.Format)) || (!["String", "Integer", "Memo"].includes(fieldItem.AttributeType))){
                    return;
                }
                else {
                    fieldItem['display_label'] = fieldItem.DisplayName.UserLocalizedLabel.Label;
                    fieldItem['api_name'] = fieldItem.LogicalName;
                    fieldItem['data_type'] = fieldItem['AttributeType'];
                    if(fieldItem['data_type']){
                        fieldItem['data_type'] = "text";
                    }
                    fieldItem['system_mandatory'] = (fieldItem.RequiredLevel.Value === "ApplicationRequired")?true:false,
                    // if(fieldItem.options && fieldItem.options.length > 0){
                    //     fieldItem["pick_list_values"] = [];
                    //     for(var optInd=0; optInd < fieldItem.options.length; optInd++){
                    //         let option = fieldItem.options[optInd];
                    //         fieldItem["pick_list_values"][optInd] = {
                    //             "actual_value": option.value,
                    //             "display_value": option.label
                    //         }
                    //     }
                    // }
                    fieldAPINameMap[fieldItem.LogicalName] = fieldItem;
                }
            });
            fieldAPINameMap.id = {
              'display_label': 'ID',
              'data_type': 'number',
              'api_name': 'id',
              'field_read_only': true
            };
            
            switch(moduleId){
                case("contact"): {
                    primaryFieldsToCreate = ["lastname", "firstname","telephone1","emailaddress1"];
                    primaryFieldsToViewForModule = ["lastname","firstname","telephone1","emailaddress1","description"];
                    break;
                }
                case("account"):{
                    primaryFieldsToCreate = ["name","telephone1"];
                    primaryFieldsToViewForModule = ["name","telephone1", "modifiedon"];
                    break;
                }
                case("lead"):{
                    primaryFieldsToCreate = ["subject", "lastname", "firstname", "telephone1", "emailaddress1"];
                    primaryFieldsToViewForModule = ["subject", "lastname", "firstname", "telephone1", "emailaddress1", "modifiedon"];
                    break;
                }
            }

            if(UA_TPA_FEATURES.setCreateModuleRecordFields) UA_TPA_FEATURES.setCreateModuleRecordFields(moduleId, primaryFieldsToCreate);
            if(UA_TPA_FEATURES.setViewModuleRecordFields) UA_TPA_FEATURES.setViewModuleRecordFields(moduleId, primaryFieldsToViewForModule);
            UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[moduleId].fields = fieldAPINameMap;
            if(UA_SAAS_SERVICE_APP.allModuleFieldsFetched()){
                callback();
            }
        });
    },

    getSearchedResultItems: async function(searchTerm, module, field="phone"){
        UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS = {};
        if(searchTerm.startsWith('+')){
            searchTerm = searchTerm.replace('+', '*');
        }
        var apiReqData = {
            "filters": [
              {
                "propertyName": field.toLocaleLowerCase(),
                "operator": "EQ",
                "value": searchTerm
              }
            ],
            "properties": Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[module].fields)
          };
        apiReqData.properties.push("hubspot_owner_id");
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/${module.toLocaleLowerCase()!="companies"?"contact":"companies"}/search`,"POST",JSON.stringify(apiReqData));
        if(response.error && response.error.category == "RATE_LIMITS"){
            return {status: "retry_request"};
        }
        // await response.data.results.forEach(async (item)=>{
        if(response && response.data && response.data.results){
            for(var j=0; j < response.data.results.length; j++){
                var item = response.data.results[j];
                if(module.toLocaleLowerCase() == "deals" || module.toLocaleLowerCase() == "tickets"){
                    var asso_response_list = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/contacts/${item.id}/associations/${module.toLocaleLowerCase()}`,"GET", null);
                    // await asso_response_list.data.results.forEach(async (asslistitem)=>{
                    for(var i=0; i < asso_response_list.data.results.length; i++){
                        var asslistitem = asso_response_list.data.results[i];

                        asslistitem = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/${module.toLocaleLowerCase()}/${asslistitem.id}?properties=${Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[module].fields).join(",")}`,"GET", null);
                        asslistitem = asslistitem.data;

                        var fullName = await UA_SAAS_SERVICE_APP.getRecordName(module,asslistitem);
                        asslistitem = {...asslistitem,...asslistitem.properties};
                        delete asslistitem.properties;
                        asslistitem.name = fullName;
                        asslistitem.webURL = `https://app.hubspot.com/${module.toLocaleLowerCase()}/${appsConfig.UA_DESK_ORG_ID}/${module.toLocaleLowerCase()}/${asslistitem.id}`;
                        UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[asslistitem.id] = asslistitem;
                    }
                }
                else{
                    var fullName = await UA_SAAS_SERVICE_APP.getRecordName(module,item);
                    item = {...item,...item.properties};
                    delete item.properties;
                    item.name = fullName;
                    item.webURL = `https://app.hubspot.com/${module.toLocaleLowerCase()}/${appsConfig.UA_DESK_ORG_ID}/${module.toLocaleLowerCase()}/${item.id}`;
                    item.owner_name = (item.hubspot_owner_id && UA_LIC_UTILITY.SAAS_FETCHED_USERS_IDS_LIST[item.hubspot_owner_id]) ? UA_LIC_UTILITY.SAAS_FETCHED_USERS_IDS_LIST[item.hubspot_owner_id].name :"No Owner";
                    UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id] = item;
                } 
            }
            return response.data.results;
        }
        else{
            return [];
        }
        
    },
    
    getNotesOfRecord: async function(module, entityId){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/${module.toLocaleLowerCase()}/${entityId}/associations/notes?limit=10`, "GET", null);
            let notesArray = [];
            if(response && response.data && response.data.results && response.data.results.length > 0){
                // response.data.results.forEach(async(noteItem)=>{
                for(var i=0;i < response.data.results.length; i++){
                    noteItem = response.data.results[i];
                    var noteResponse = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/notes/${noteItem.id}?properties=hs_note_body`, "GET", null);
                    if(noteResponse && noteResponse.data && noteResponse.data.id){
                        notesArray.push({
                            'id': noteResponse.data.id,
                            'content': noteResponse.data.properties.hs_note_body,
                            'title': "",
                            'time': noteResponse.data.createdAt
                        });
                    }
                }
            }
        return notesArray;
    },
    
    addModuleRecordNote: async function(module, entityId, noteContent){
        var noteResponse = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/notes`, "POST", {
            "properties": {
              "hs_timestamp": new Date().toISOString(),
              "hs_note_body": noteContent
              
            }
        });
        if(noteResponse.data && noteResponse.status && noteResponse.data.id){
            var associationsLableResponse = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v4/associations/notes/${module.toLocaleLowerCase()}/labels`, "GET", {});
            if(associationsLableResponse.data && associationsLableResponse.status && associationsLableResponse.data.results && associationsLableResponse.data.results.length > 0){
                var associationResponse = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/notes/${noteResponse.data.id}/associations/${module.toLocaleLowerCase()}/${entityId}/${associationsLableResponse.data.results[0].typeId}`, "PUT", {});
                if(associationResponse && associationResponse.data && associationResponse.data.id){
                    return associationResponse;
                }
            }
        }
        else{
            return {
                error: true
            }
        }
        return false;
    },
    
    updateRecordByField: async function(module, entityId, fieldName, fieldValue){
        let updateObjPayload = {};
            updateObjPayload[fieldName] = fieldValue;
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl(3)}/crm/v3/objects/${module.toLocaleLowerCase()}/${entityId}`, "PATCH", {"properties": updateObjPayload});
        if(response.data || response.status == 200){
            return response;
        }
        else{
            return {
                error: {
                    'message': 'Error, Try again'
                }
            }
        }
    },
    
    createRecordInModule: async function(module, fieldMap){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/${module.toLocaleLowerCase()}`, "POST", {"properties": fieldMap});
        if(response.data){
            let resultItemId = response.data.id;
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/crm/v3/objects/${module.toLocaleLowerCase()}/${resultItemId}?properties=${Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[module].fields).join(",")}`, "GET", null);
            let recordItem = response.data;

            recordItem.name = await UA_SAAS_SERVICE_APP.getRecordName(module,recordItem);
            recordItem = {...recordItem,...recordItem.properties};
            delete recordItem.properties;

            recordItem.webURL = `https://app.hubspot.com/${module.toLocaleLowerCase()}/${appsConfig.UA_DESK_ORG_ID}/${module.toLocaleLowerCase()}/${recordItem.id}`;
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[resultItemId] = recordItem;
            return recordItem;
        }
        else if(response.error){
            showErroWindow('Unable to create '+module, response.error.message);
            return {
                error: {
                    'message': response.error.message
                }
            };
        }
        else{
            return {
                error: {
                    'message': 'Error, Try again'
                }
            };
        }
    },
    
    getNewRecordInSAASWebURL: function(module){
        return;
        let modules = ["", "Contacts","Companies","Deals","","Tickets"]
        return `https://app.hubspot.com/contacts/${appsConfig.UA_DESK_ORG_ID}/objects/0-${modules.indexOf(module)}/views/all/list`;
    },

    getRecordName: async function(module,item){
        var fullName = "";
            switch(module){
                case("Contacts"): {
                    fullName = item['properties'].firstname ? item['properties'].firstname:"";
                    if(item['properties'].lastname){
                        fullName = fullName+" "+ item['properties'].lastname;
                    } 
                    fullName = fullName?fullName:"Contact#"+item.id;
                    break;
                }
                case("Companies"):{
                    fullName = item['properties'].name ? item['properties'].name:"Company#"+item.id;
                    break;
                }
                case("Deals"):{
                    fullName = item['properties'].dealname ? item['properties'].dealname:"Deal#"+item.id;
                    break;
                }
                case("Tickets"):{
                    fullName = item['properties'].subject ? item['properties'].subject:"Ticket#"+item.id;
                    break;
                }
            }
        return fullName;
    },

    searchRelatedRecordsToAllModules: async function(searchTerm, field="phone"){
        try{
            var searchData = [];
            if(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES && Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).length > 0){
                for(var i=0; i < Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).length; i++){
                    let module = Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES)[i];
                    let data = await UA_SAAS_SERVICE_APP.getSearchedResultItems(searchTerm, module, field);
                    if(data && data.status && data.status == "retry_request"){
                        return [{status: "retry_request"}];
                    }
                    if(data && data.length > 0){
                        // Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS).forEach((id)=>{
                        for(var j=0; j < Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS).length; j++){
                            let id = Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS)[j];                        
                            let item = UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id]
                            searchData.push({module: module, record: item});
                        }
                    }
                }
            }  
            return searchData;
        }catch(e){ console.log(e); return []; }
    }
};