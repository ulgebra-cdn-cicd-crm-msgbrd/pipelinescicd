const UA_OAUTH_PROCESSOR = {
     domainExtnMap: {
        "IN": ".in",
        "EU": ".eu",
        "JP": ".jp",
        "AU": ".com.au",
        "US": ".com"
    },
    currentSAASDomain: ".com",
    initiateAuth: function (scope) {
        var scopeText = typeof scope === "object" ? scope.join(',') : scope;
//        $('.ua_service_login_btn').text('Processing...');
        if(!extensionName){
            showErroWindow('Application error!', 'Unable to load app type. Kindly <a target="_blank" href="https://apps.ulgebra.com/contact">contact developer</a>');
            return;
        }
        if(!UA_OAUTH_PROCESSOR.currentSAASDomain || UA_OAUTH_PROCESSOR.currentSAASDomain === "undefined"){
            UA_OAUTH_PROCESSOR.currentSAASDomain = ".com";
        }
        window.open(`https://accounts.zoho${UA_OAUTH_PROCESSOR.currentSAASDomain}/oauth/v2/auth?response_type=code&client_id=1000.BEJADNTOF3QA6RR0FD0MQ61VMNZLBK&scope=${scopeText}&redirect_uri=${APP_ENV_CLIENT_URL}/oauth-home&state=${currentUser.uid}:::${extensionName}&access_type=offline&prompt=consent`, 'Authorize', 'popup');
    },
    
    getAPIResponse: async function (url, method, data, headers = null) {
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        if(!headers){
            headers = {};
            headers["Content-Type"] = "application/json";
        }
        headers.Authorization = "Zoho-oauthtoken {{AUTHTOKEN}}";
        headers._uaClientRequestID = new Date().getTime()+"-"+Math.random(100000,99999);
        var returnResponse = await UA_APP_UTILITY.makeUAServiceAuthorizedHTTPCall(url, method, data, headers);
        removeTopProgressBar(credAdProcess3);
        return returnResponse;
    },
    
    showReAuthorizationError: function(scope){
        if($('#UA_SAAS_AUTH_BTN').length > 0){
            console.log('SaaS auth window already showing.. skipping.');
            return;
        }
       showErroWindow("Zoho Authentication Needed!", `<b style="font-size:17px">You need to authorize your Zoho Account to proceed </b><br><br>
            <div style="font-size:13px"> Select Your Zoho Account DC:  
                <select onchange="UA_OAUTH_PROCESSOR.currentSAASDomain=this.value">
                    <option value=".com" selected>US (accounts.zoho.com)</option>
                    <option value=".in">India (accounts.zoho.in)</option>
                    <option value=".eu">Europe (accounts.zoho.eu)</option>
                    <option value=".jp">Japan (accounts.zoho.jp)</option>
                    <option value=".com.au">Australia (accounts.zoho.com.au)</option>
                </select> 
            </div>
       <br>  <button id="UA_SAAS_AUTH_BTN" class="ua_service_login_btn ua_primary_action_btn" onclick="UA_OAUTH_PROCESSOR.initiateAuth('${scope.join(',')}')">Authorize Now</button>`, false, false, 1000);
    }
    
};