var UA_SAAS_SERVICE_APP = {
    
    CURRENT_USER_INFO : null,
    CURRENT_ORG_INFO : null,
    CURRENT_OAUTH_SCOPE_NEEDED : ['ZohoBooks.settings.READ', 'ZohoBooks.contacts.READ', 'ZohoBooks.contacts.CREATE', 'ZohoBooks.invoices.CREATE', 'ZohoBooks.invoices.READ'],
    FETCHED_RECORD_DETAILS: {},
    CURRENT_MODULE_FIELDS_DROPDOWN : [],
    SELECTED_RECEIPS: {},
    SEARCH_MODULE_RECORD_MAP: {},
    fetchedList : {},
    widgetContext: {},
    APP_API_NAME: "pinnaclesms__",
    DESK_APP: null,
    moduleVsDataProperty : {
        "invoice": "invoice",
        "customer": "contact",
        "dashboard": "dashboard",
        "vendor": "contact"
    },
    getAPIBaseUrl : function(){
        return `https://books.zoho.com/api/v3`;
    },
    
    provideSuggestedCurrentEnvLoginEmailID: async function(){
        ZFAPPS.get('user').then (
            function(response) {
                if(response && response.user && response.user.email) {
                    UA_APP_UTILITY.receivedSuggestedCurrentEnvLoginEmailID(response.user.email);
                }
            },
            function(error) {
              // failure operation
            });
    },
    
    initiateAPPFlow: async function(){
        //console.log('loading app...books');
        if (queryParams.get("openMode") === "modal") {
            ZFAPPS.invoke('RESIZE', { width: '1000px', height: '550px'})
                    .then(() => { 
                        //console.log('Resized successfully');
            });
        }
        $("#ac_name_label_saas .anl_servicename").text('Zoho Books');
        //ZOHO.embeddedApp.on("PageLoad", async function(widgetContext) {
            
//            console.log('embeddd', widgetContext); 
//            ZFAPPS.invoke('RESIZE', { width: '1000px', height: '550px'})
//                    .then(() => { console.log('Resized successfully');
//            });
            
            var orgInfo = await ZFAPPS.get('organization');
            orgInfo = orgInfo.organization;
            UA_OAUTH_PROCESSOR.currentSAASDomain = orgInfo.data_center_extension;
            UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO = orgInfo;
            appsConfig.UA_DESK_ORG_ID = orgInfo.organization_id;
            UA_SAAS_SERVICE_APP.proceedToAppInitializationIfAPPConfigResolved();
            
            UA_SAAS_SERVICE_APP.widgetContext.module = UA_SAAS_SERVICE_APP.moduleVsDataProperty[UA_SAAS_SERVICE_APP.DESK_APP.location.split('.')[0]];
            if(UA_SAAS_SERVICE_APP.widgetContext.module === "dashboard"){
                ZFAPPS.invoke('RESIZE', {height: '550px'})
                    .then(() => { 
                        //console.log('Resized successfully');
                });
                UA_SAAS_SERVICE_APP.widgetContext.module = null;
                $('.siteName').hide();
                $('.siteName').html(`<span onclick="UA_SAAS_SERVICE_APP.showExpandedModalView()" class="material-icons topModalExpandBtn">aspect_ratio</span>`).show();
                $("#showExpandModalBtn").show();
            }
            else{
                if (queryParams.get("openMode") !== "modal") {
                    var entityInfo = await ZFAPPS.get(UA_SAAS_SERVICE_APP.widgetContext.module);
                    entityInfo = entityInfo[UA_SAAS_SERVICE_APP.widgetContext.module];
                    UA_SAAS_SERVICE_APP.widgetContext.entity = entityInfo;
                    UA_SAAS_SERVICE_APP.widgetContext.entityId = entityInfo[UA_SAAS_SERVICE_APP.widgetContext.module+"_id"];
                    UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[UA_SAAS_SERVICE_APP.widgetContext.entityId] = UA_SAAS_SERVICE_APP.widgetContext.entity;
                    $('.siteName').hide();
                    $('.siteName').html(`<span onclick="UA_SAAS_SERVICE_APP.showExpandedModalView()" class="material-icons topModalExpandBtn">aspect_ratio</span>`).show();
                    $("#showExpandModalBtn").show();
                }else{
                    UA_SAAS_SERVICE_APP.widgetContext.module = queryParams.get("module");
                    UA_SAAS_SERVICE_APP.widgetContext.entityId = queryParams.get("entityId");
                }
            }
            
            var userInfo = await ZFAPPS.get('user');
            userInfo = userInfo.user;
            UA_SAAS_SERVICE_APP.CURRENT_USER_INFO = userInfo;
            appsConfig.APP_UNIQUE_ID = UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.user_id;
            $("#saasAuthIDName").text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.name).attr('title', `Authorized Zoho Account: (${UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.email})`);
            setTimeout((function () {
                $('#ac_name_label_saas .ac_name_id').text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.email);
            }), 2000);
            if(UA_SAAS_SERVICE_APP.widgetContext.module){    
                UA_SAAS_SERVICE_APP.injectShowSearchContact();
            }
            UA_SAAS_SERVICE_APP.proceedToAppInitializationIfAPPConfigResolved();
            
    },
    
    addSAASUsersToLICUtility: async function(url){
        url = url?url:`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/users?filter_by=Status.Active&organization_id=${appsConfig.UA_DESK_ORG_ID}`;
        $('.saasServiceName').text("Zoho Books");
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(url, "GET", null);
        response.data.users.sort(function(a, b) {
            return a.name.localeCompare(b.name);
        });
        response.data.users.forEach(item=>{
            UA_LIC_UTILITY.addFetchedSAASUser(item.user_id, item.name, item.email, null, item.user_role === "admin", item.phone = null, item.photo_url);
        });
        if(response.data && response.data.page_context && response.data.page_context.has_more_page){
            UA_SAAS_SERVICE_APP.addSAASUsersToLICUtility(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/users?page=${response.data.page_context.page+1}&filter_by=Status.Active&organization_id=${appsConfig.UA_DESK_ORG_ID}`);
            return;
        }
        UA_LIC_UTILITY.saasUserListFetchCompleted();
        return;
    },
    
    proceedToAppInitializationIfAPPConfigResolved: function(){
        if(appsConfig.APP_UNIQUE_ID && appsConfig.UA_DESK_ORG_ID){
            UA_SAAS_SERVICE_APP.renderInitialElements();
            UA_SAAS_SERVICE_APP.addSAASUsersToLICUtility();
            UA_APP_UTILITY.appsConfigHasBeenResolved();
            UA_SAAS_SERVICE_APP.getCurrentUserInfo(function(resp){
                if(resp.error){
                    if(resp.error.code == 6041){
                        UA_OAUTH_PROCESSOR.showReAuthorizationError(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED);
                        return;
                    }else{
                        showErroWindow("Alert", resp.error.message);
                        return;
                    }
                }
            });
            if(UA_SAAS_SERVICE_APP.widgetContext.module && UA_SAAS_SERVICE_APP.widgetContext.module != 'dashboard')
            {
                UA_SAAS_SERVICE_APP.renderInitialElementsForZohoBooksList();
            }
        }
    },
    
    renderInitialElements: async function(){
        UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown('users', ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder']);
        if(!UA_SAAS_SERVICE_APP.widgetContext.module){
            UA_SAAS_SERVICE_APP.entityDetailFetched(null);
            //console.log('Module does not exist, rendering skipped');
            $("#recip-count-holder").text("Add recipients to proceed");
            return;
        }
//        UA_APP_UTILITY.renderSavedTemplatesInDropdowns();
        await UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
        if (queryParams.get("openMode") === "modal") {
            UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(UA_SAAS_SERVICE_APP.widgetContext.module, UA_SAAS_SERVICE_APP.widgetContext.entityId, UA_SAAS_SERVICE_APP.addRecordsAsRecipients);
        }
        else{
            UA_SAAS_SERVICE_APP.addRecordsAsRecipients(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
        }
    },

    renderInitialElementsForZohoBooksList: async function(){
        $(".ssf-new-recip-form").prepend(`<div id="ssf-recip-list-add-holdr" class="ssf-recip-choice-btn" style="margin-top: -5px;background: none;border: none;box-shadow: none;"></div>`);
        UA_SAAS_SERVICE_APP.renderContactList();
    },
    
    renderContactList: async function(){
        // let departmentId = await ZOHODESK.get('department.id');
        // departmentId = departmentId['department.id'];
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/customviews?entity_type=${UA_SAAS_SERVICE_APP.DESK_APP.location.split('.')[0]}&organization_id=${appsConfig.UA_DESK_ORG_ID}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        //console.log('renderList',response);
        let listDropdownItems = [];
        if(response && response.data && response.data.data && response.data.data.default_filters){
            response.data.data.default_filters.forEach(item=>{
                UA_SAAS_SERVICE_APP.fetchedList[item.value] = item;
                listDropdownItems.push({
                    'label': item.title,
                    'value': item.value
                });
            });
            response.data.data.created_by_me.forEach(item=>{
                UA_SAAS_SERVICE_APP.fetchedList[item.customview_id] = item;
                listDropdownItems.push({
                    'label': item.title,
                    'value': item.customview_id
                });
            });
        }

        UA_APP_UTILITY.renderSelectableDropdown('#ssf-recip-list-add-holdr', 'Select a filter', listDropdownItems, 'UA_SAAS_SERVICE_APP.insertManageListContacts', false, false);
    
    },

    insertManageListContacts: async function(listId){

        let url = `${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${UA_SAAS_SERVICE_APP.DESK_APP.location.split('.')[0]}s?page=1&per_page=100&usestate=true&organization_id=${appsConfig.UA_DESK_ORG_ID}`;
        url = listId.includes('.') == true ? url+`&filter_by=${listId}`:url+`&customview_id=${listId}`;
        let propertyName = UA_SAAS_SERVICE_APP.widgetContext.module+'s';

        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(url, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        //console.log('renderList',response);
        if(response && response.data && response.data[propertyName] && response.data[propertyName].length > 0) {
            response.data[propertyName].forEach(item=>{
                let id = item[`${UA_SAAS_SERVICE_APP.widgetContext.module}_id`];
                // UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id] = item;
                UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(UA_SAAS_SERVICE_APP.widgetContext.module, id, UA_SAAS_SERVICE_APP.addRecordsAsRecipients);
                UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
            });
        }
        else{
            alert(`We can't find records in this list or list is empty.`);
        }
            
    },
    
    sendPhoneFieldsForRecipientType: function(fieldsArray){
        fieldsArray.forEach(item=>{
            if(item.display_label === "phone" || item.display_label === "mobile"){
                UA_APP_UTILITY.addRecipientPhoneFieldType(item.api_name, item.display_label);
            }
        });
    },
    
    populateModuleItemFieldsInDropDown: async function (module, target, fieldsCallback=(()=>{})){
        if(!module || UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.includes(module) == true){
            return;
        }
        UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.push(module);
        var moduleToLoad = module === "users" ? UA_SAAS_SERVICE_APP.CURRENT_USER_INFO : UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[UA_SAAS_SERVICE_APP.widgetContext.entityId];
        if(!moduleToLoad){
            return;
        }
        var moduleFields = Object.keys(moduleToLoad);
        var fieldsArray = [];
        var dropDownValues = [];
        moduleFields.forEach(item=>{
            var itemVal = moduleToLoad[item];
            if(typeof itemVal !== "object" && itemVal !== null && typeof itemVal!=="boolean"){
                fieldsArray.push({
                    "api_name": item,
                    "display_label": item
                });
                dropDownValues.push({
                    "label": item,
                    "value": (module.toUpperCase() === "USERS" ? "CURRENT_USER": module.toUpperCase())+'.'+item
                });
            }
        });
        fieldsCallback(fieldsArray);
        if(typeof target === "string"){
            target = [target];
        }
        target.forEach(item=>{
            UA_APP_UTILITY.renderSelectableDropdown(item, `${module} fields`, dropDownValues, 'UA_APP_UTILITY.templateMergeFieldSelected', false, true);
        });
        return true;
    },
    
    injectShowSearchContact :async function(){
        $(".ssf-new-recip-form").prepend(`<div id="searchAndAddFromContactBtn" class="ssf-recip-choice-btn" onclick="$('.item-list-popup').show()">
                                            <i class="material-icons">search</i> Search from ${UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()}
                                      </div>`);
        $('.pageContentHolder').append(`<div class="item-list-popup" style="display:none">
                        <div class="item-list-popup-title">
                            Select a ${UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()} <div class="pop-win-close" onclick="$('.item-list-popup').hide();">x</div>
                        </div>
                        <div class="item-search-box">
                            <input class="input-form" id="contact-seach-name" type="text" autocomplete="off" placeholder="name of the ${UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()}"/><br>
                            <div class="btn-save" onclick="UA_SAAS_SERVICE_APP.searchRecordsAndRender($('#contact-seach-name').val(),UA_SAAS_SERVICE_APP.widgetContext.module)">
                                Search ${UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()}
                            </div>
                            <div class="btn-reset" onclick="UA_SAAS_SERVICE_APP.resetsearchContacts()">
                                Reset
                            </div>
                        </div>
                        <div class="item-list-popup-content" id="contact-search-items">

                        </div>
                    </div>`);
    },

    resetsearchContacts: function(){
        $("#contact-seach-name").val("");
        $("#contact-search-items").html("");
    },

    searchRecordsAndRender: async function(searchTerm,module){
        if(!valueExists(searchTerm)){
            return;
        }
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${module}s?organization_id=${appsConfig.UA_DESK_ORG_ID}&search_text=${searchTerm}`, "GET", null);
            if(response)
            {
                //console.log(response);
                UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
                $("#contact-search-items").html(" ");
                if(response.data && response.data[`${module}s`] && response.data[`${module}s`].length>0)
                {
                    var search_items = response.data[`${module}s`];
                    if(search_items && search_items.length > 50){
                        search_items.splice(0,49);
                    }
                    UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module] = {};
                    search_items.forEach((obj)=>{
                        obj.id = obj[`${module}_id`];
                        UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][obj.id] = obj;
                        var fullName = "";
                        if(module)
                        {
                            switch(module){
                                case("contact"): {
                                    fullName = obj.contact_name ? obj.contact_name:`Contact#${obj.id}`;
                                    break;
                                }
                                case("invoice"):{
                                    fullName = obj.invoice_number;
                                    break;
                                }
                            }
                        }
                        let itemHTML = `<div class="item-list-popup-item" style="cursor:pointer;" onclick="UA_SAAS_SERVICE_APP.selectPhoneNumber('${obj.id}', '${module}', '${fullName}','${module}','${true}')"><span class="c-name"> ${fullName} </span>`;
                        // itemHTML =  itemHTML + `${'<span class="c-phone '+(UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[module+"_"+obj.id] ? 'alreadyadded': '')+'"></span>'}`;
                        itemHTML =  itemHTML + `</div>`;
                        $("#contact-search-items").append(itemHTML);
                    });
                }
                else
                {
                    $("#contact-search-items").html("No records found. <br><br> <span class=\"c-silver\"> This search will include <br> only customers having phone numbers</span>");
                    return;
                }
            }
    },

    selectPhoneNumber: async function(id,number,name,module,isSelected){
        var receipNumberID = module + "_" + id;
        if(!UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] && !UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id]){
            UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] = {
                'id': id,
                'name': name,
                'module': module,
                'isSelected': isSelected
            };
            $.extend(UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id],UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID]) ;

            await UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(module, id, UA_SAAS_SERVICE_APP.addRecordsAsRecipients);
            await UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
        }
        else {
            //console.log(module+' already added');
            showErroWindow("Alert Message",`This ${module.toLocaleLowerCase()} is already added.`)
            // alert(`This ${module.toLocaleLowerCase()} is already added.`);
            return;
        }
    },

    getCurrentUserInfo : async function(callback){
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/users/me?organization_id='+appsConfig.UA_DESK_ORG_ID, callback);
    },
    
    getCurrentOrgInfo : async function(callback){
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/org', callback);
    },
    
    getAPIResponseAndCallback: async function(url, callback){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}${url}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        callback(response);
    },
    
    showErrorIfInAPICall : function(response){
        if(response.code === 401){
            UA_OAUTH_PROCESSOR.showReAuthorizationError(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED);
        }
    },
    
    initiateAuthFlow: function(){
        UA_OAUTH_PROCESSOR.initiateAuth(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED);
    },
    
    fetchStoreExecuteOnRecordDetails: async function(module, recordIds, callback){
//        if(typeof recordIds !== "object"){
//            var recordId = recordIds;
//            recordIds = [recordId];
//        }
////        recordIds = ["5318123000000377412"];
        if(!module){
            return;
        }
        $("#recip-count-holder").text(`fetching ${module.toLocaleLowerCase()} information...`);
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${module}s/${recordIds}?organization_id=${appsConfig.UA_DESK_ORG_ID}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        //console.log('fetchStoreExecuteOnRecordDetails',response);
        UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[response.data[module][`${module}_id`]] = response.data[module];
//        $("#recip-count-holder").text(`Selected ${recordIds.length} ${module.toLocaleLowerCase()}`);
        
        callback(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
        UA_SAAS_SERVICE_APP.entityDetailFetched();
    },
    
    added_invoice_contact_person_ids: [],
    addRecordsAsRecipients: function(contactsListParam){
        var contactsList = [];
        if(UA_SAAS_SERVICE_APP.widgetContext.module === "invoice"){
            Object.keys(contactsListParam).forEach(contactId=>{
                var contactItem = contactsListParam[contactId];
                try{
                    contactItem.contact_persons_details = contactItem.contact_persons_details.concat(contactItem.contact_persons_associated);
                }
                catch(ex){
                    console.log(ex);
                };
                contactItem.contact_persons_details.forEach((contactPersonDetailItem, i)=>{
                    contactPersonDetailItem.id = contactItem[UA_SAAS_SERVICE_APP.widgetContext.module+"_id"];
                    if(contactPersonDetailItem.contact_person_id && UA_SAAS_SERVICE_APP.added_invoice_contact_person_ids.includes(contactPersonDetailItem.contact_person_id) === false) { 
                        UA_SAAS_SERVICE_APP.added_invoice_contact_person_ids.push(contactPersonDetailItem.contact_person_id);
                    }
                    if(contactPersonDetailItem.contact_person_id && UA_SAAS_SERVICE_APP.added_invoice_contact_person_ids.includes(contactPersonDetailItem.contact_person_id) === true) { 
                        contactItem.contact_persons_details.splice(i,1);
                    }
                });
                contactsList = contactsList.concat(contactItem.contact_persons_details);
            });
        }
        if(UA_SAAS_SERVICE_APP.widgetContext.module === "contact"){
            contactsList = contactsListParam;
        }
        for(var item in contactsList){
            if(UA_SAAS_SERVICE_APP.widgetContext.module === "contact"){
                contactsList[item].id = contactsList[item].contact_id;
            }
            if($(`[data-recip-id=${contactsList[item]['id']}]`).length === 0)
            {
                ["phone", "mobile"].forEach(mobileFieldItem => {
                    if (contactsList[item][mobileFieldItem]) {
                        UA_APP_UTILITY.addRecipientPhoneFieldType(mobileFieldItem, mobileFieldItem);
                    }
                });
                if(!contactsList[item].name){
                    contactsList[item].name = "";
                    if(contactsList[item].first_name){
                        contactsList[item].name = contactsList[item].first_name;
                    }
                    if(contactsList[item].last_name){
                        contactsList[item].name = contactsList[item].name+" "+ contactsList[item].last_name;
                    }
                    if(contactsList[item].name === ""){
                        contactsList[item].name = contactsList[item].contact_name;
                    }
                }
                UA_APP_UTILITY.addInStoredRecipientInventory(contactsList[item]);
            }
        }
    },
    
    showExpandedModalView: function(){
        ZFAPPS.showModal({
            url: window.location.href+"&openMode=modal&module="+UA_SAAS_SERVICE_APP.widgetContext.module+"&entityId="+UA_SAAS_SERVICE_APP.widgetContext.entityId
       });
    },
    
    addSentSMSAsRecordInHistory: async function(sentMessages){
        if(UA_SAAS_SERVICE_APP.widgetContext.module === null){
            return;
        }
        for(var sentMessageId in sentMessages){
            let commentContent = `<u><i>${appPrettyName} Extension</i></u> - `;
            let sentMessage = sentMessages[sentMessageId];
            commentContent+=`<b>${sentMessage.channel} ${sentMessage.status} from ${sentMessage.from} to ${sentMessage.to}</b>:\n
                             ${sentMessage.message}\n`;
      
            let credAdProcess3 = curId++;
            showProcess(`Adding SMS to ${sentMessage.to} in comments...`, credAdProcess3);
            if(sentMessage.moduleId.startsWith("newmanual")){
                sentMessage.moduleId = UA_SAAS_SERVICE_APP.widgetContext.entityId;
            }
            let response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${UA_SAAS_SERVICE_APP.widgetContext.module}s/${sentMessage.moduleId}/comments`, "POST", {
                "JSONString": JSON.stringify({
                    "description": commentContent
                }),
                "organization_id": appsConfig.UA_DESK_ORG_ID
            }, {
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": "Zoho-oauthtoken {{AUTHTOKEN}}"
            });
            processCompleted(credAdProcess3);
            UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
            //console.log('addSentSMSAsRecordInHistory',response);
        }
        
        //alert('All Messages has been added to Pinnacle SMS History');
        
    },
    entityDetailFetched: function(data){
        setTimeout(()=>{UA_APP_UTILITY.fetchedAllModuleRecords();}, 2000);
        if(typeof UA_TPA_FEATURES.callOnEntityDetailFetched === "function"){
            UA_TPA_FEATURES.callOnEntityDetailFetched(data);
        }
    }
};

window.addEventListener('load', function(){
        var ZbooksClient =  ZFAPPS.extension.init();
        ZbooksClient.then(function(app) {  
           UA_SAAS_SERVICE_APP.DESK_APP = app; 
            let supportedTransitions = ['ON_INVOICE_PREVIEW', 'ON_ESTIMATE_PREVIEW', 'ON_SALESORDER_PREVIEW',
                'ON_CUSTOMER_PREVIEW', 'ON_VENDOR_PREVIEW', 'ON_CREDITNOTE_PREVIEW', 'ON_RETAINERINVOICE_PREVIEW'];
            supportedTransitions.forEach(item=>{
                app.instance.on(item, function() {
                    if(UA_SAAS_SERVICE_APP.widgetContext.entityId){
                        window.location.reload();
                    }
                }); 
            });
        });
});