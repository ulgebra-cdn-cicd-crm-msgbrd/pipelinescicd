const UA_OAUTH_PROCESSOR = {
     domainExtnMap: {
        "IN": ".in",
        "EU": ".eu",
        "CN": ".com.cn",
        "AU": ".com.au",
        "US": ".com"
    },
    currentSAASDomain: ".com",
    initiateAuth: function (scope) {
        var scopeText = typeof scope === "object" ? scope.join(' ') : scope;
//        $('.ua_service_login_btn').text('Processing...');
        if(!extensionName){
            showErroWindow('Application error!', 'Unable to load app type. Kindly <a target="_blank" href="https://apps.ulgebra.com/contact">contact developer</a>');
            return;
        }
        var redirectURI = encodeURIComponent(APP_ENV_CLIENT_URL+`/oauth-home?client_id=${UA_TPA_FEATURES.clientIds[extensionName]}&appCode=${extensionName}`);
        window.open(`https://oauth.pipedrive.com/oauth/authorize?client_id=${UA_TPA_FEATURES.clientIds[extensionName]}&state=${currentUser.uid}:::${extensionName}&redirect_uri=${redirectURI}`, 'Authorize', 'popup');
    },
    
    getAPIResponse: async function (url, method, data) {
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var returnResponse = await UA_APP_UTILITY.makeUAServiceAuthorizedHTTPCall(url, method, data, {"Authorization": "Bearer {{AUTHTOKEN}}", "Content-Type": "application/json"});
        removeTopProgressBar(credAdProcess3);
        return returnResponse;
    },
    
    showReAuthorizationError: function(scope){
        if($('#UA_SAAS_AUTH_BTN').length > 0){
            console.log('SaaS auth window already showing.. skipping.');
            return;
        }
       showErroWindow("Pipedrive Authentication Needed!", `You need to authorize your Pipedrive Account to proceed <br><br> <button id="UA_SAAS_AUTH_BTN" class="ua_service_login_btn ua_primary_action_btn" onclick="UA_OAUTH_PROCESSOR.initiateAuth('${scope.join(' ')}')">Authorize Now</button>`, false, false, 1000);
    }
    
};