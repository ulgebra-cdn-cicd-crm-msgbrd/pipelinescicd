const UA_OAUTH_PROCESSOR = {
    domainExtnMap: {
       "IN": ".in",
       "EU": ".eu",
       "CN": ".com.cn",
       "AU": ".com.au",
       "US": ".com"
   },
   currentSAASDomain: ".com",
   initiateAuth: function (scope) {
       var scopeText = typeof scope === "object" ? scope.join(' ') : scope;
//        $('.ua_service_login_btn').text('Processing...');
       if(!extensionName){
           showErroWindow('Application error!', 'Unable to load app type. Kindly <a target="_blank" href="https://apps.ulgebra.com/contact">contact developer</a>');
           return;
       }
       let api_domain = $("#inp_saas_api_domain")?$("#inp_saas_api_domain").val().replace("https://","").replace("/",""):UA_SAAS_SERVICE_APP.widgetContext.shop;
       if(!valueExists(api_domain)){
            showErroMessage("Please fill shopify domain");
            return;
        }
       window.open(`https://${api_domain}/admin/oauth/authorize?client_id=${UA_TPA_FEATURES.clientIds[extensionName]}&scope=read_customers&grant_options[]=offline&state=${currentUser.uid}:::${extensionName}&redirect_uri=https://sms.ulgebra.com/oauth-home.html?appCode=${extensionName}`, 'Authorize', 'popup');
   },
   
   getAPIResponse: async function (url, method, data) {
       var credAdProcess3 = curId++;
       showTopProgressBar(credAdProcess3);
       var returnResponse = await UA_APP_UTILITY.makeUAServiceAuthorizedHTTPCall(url, method, data, {"Authorization": "Bearer {{AUTHTOKEN}}", "Content-Type": "application/json"});
       removeTopProgressBar(credAdProcess3);
       return returnResponse;
   },
   
   showReAuthorizationError: function(scope){
       if($('#UA_SAAS_AUTH_BTN').length > 0){
           console.log('SaaS auth window already showing.. skipping.');
           return;
       }
       let sampleDomain = '.myshopify.com';
      showErroWindow("Shopify Authentication Needed!", `You need to authorize your Shopify Account to proceed <br><br> 
        <div style="font-size: 18px;margin-bottom: 5px;margin-top: 20px;">Shopify Domain</div>
        <input id="inp_saas_api_domain" value="${UA_SAAS_SERVICE_APP.widgetContext.shop?UA_SAAS_SERVICE_APP.widgetContext.shop:""}" type="text" placeholder="e.g mydomain${sampleDomain}" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
        <button id="UA_SAAS_AUTH_BTN" class="ua_service_login_btn ua_primary_action_btn" onclick="UA_OAUTH_PROCESSOR.initiateAuth('${scope.join(' ')}')">Authorize Now</button>`, false, false, 1000);
   }
   
};