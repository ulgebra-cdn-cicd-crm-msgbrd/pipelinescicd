var UA_SAAS_SERVICE_APP = {
    API_BASE_URL : "",
    CURRENT_USER_INFO : null,
    CURRENT_ORG_INFO : null,
    CURRENT_OAUTH_SCOPE_NEEDED : [],
    FETCHED_RECORD_DETAILS: {},
    CURRENT_MODULE_FIELDS_DROPDOWN : [],
    SELECTED_RECEIPS: {},
    SEARCH_MODULE_RECORD_MAP: {},
    fetchedList : {},
    widgetContext: {},
    appsConfig: {},
    APP_API_NAME: "pinnaclesms__",
    isModelOpened: false,
    APP_CLIENT: null,
    supportedIncomingModules: [
        {
            "selected": true,
            "value": "tickets",
            "label": "Tickets"
        },
        {
            "selected": false,
            "value": "--no-tickets",
            "label": "No tickets"
        }
    ],
    
    getAPIBaseUrl : function(){
        return `https://${UA_SAAS_SERVICE_APP.API_BASE_URL}/api/v2`;
    },

    provideSuggestedCurrentEnvLoginEmailID: async function(){
        var client = await app.initialized();
        client.data.get("loggedInUser").then (
            function(response) {
                if(response && response.loggedInUser && response.loggedInUser.contact && response.loggedInUser.contact.email) {
                    UA_APP_UTILITY.receivedSuggestedCurrentEnvLoginEmailID(response.loggedInUser.contact.email);
                }
            },
            function(error) {
              // failure operation
            });
    },
    
    initiateAPPFlow: async function(){
        $("#ac_name_label_saas .anl_servicename").text('Freshdesk');
        var client = await app.initialized();
        UA_SAAS_SERVICE_APP.APP_CLIENT = client;
        var domainNameQuery = await UA_SAAS_SERVICE_APP.APP_CLIENT.data.get("domainName");
        UA_SAAS_SERVICE_APP.API_BASE_URL = domainNameQuery.domainName;
        UA_OAUTH_PROCESSOR.currentSAASDomain = UA_SAAS_SERVICE_APP.API_BASE_URL;
        UA_SAAS_SERVICE_APP.widgetContext.module = UA_SAAS_SERVICE_APP.APP_CLIENT.context.page;
        
        if(UA_SAAS_SERVICE_APP.widgetContext.module === "full_page"){
            UA_SAAS_SERVICE_APP.widgetContext.module = null;
        }
        else{
            var entityData = await UA_SAAS_SERVICE_APP.APP_CLIENT.data.get(UA_SAAS_SERVICE_APP.widgetContext.module);
            UA_SAAS_SERVICE_APP.widgetContext.entity = entityData[UA_SAAS_SERVICE_APP.widgetContext.module];
            UA_SAAS_SERVICE_APP.widgetContext.entityId = UA_SAAS_SERVICE_APP.widgetContext.entity.id;
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[UA_SAAS_SERVICE_APP.widgetContext.entityId] = UA_SAAS_SERVICE_APP.widgetContext.entity;
        }
        
        await UA_SAAS_SERVICE_APP.initiateAPPFlow_SA();
    },
    
    initiateAPPFlow_SA: async function(){
        if(extensionName == "twilioforfreshdesk" && queryParams.get('force-ui-view') && queryParams.get('force-ui-view') == "SETTINGS"){
            sendAlteredConfigParamToFreshdesk();
        }
        let params = new URLSearchParams(location.search);
        if(!UA_SAAS_SERVICE_APP.widgetContext.module && queryParams.get("module")){
            UA_SAAS_SERVICE_APP.widgetContext.module = queryParams.get("module");
            UA_SAAS_SERVICE_APP.widgetContext.entityId = queryParams.get("entityId");
            // UA_SAAS_SERVICE_APP.injectShowSearchContact(); 
        }

        $("#ac_name_label_saas .anl_servicename").text('Freshdesk');
        this.insertIncomingConfigItems();
        UA_SAAS_SERVICE_APP.getCurrentUserInfo(function(response){
            UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
            // if(response.error && response.error.message){
            //     showErroWindow('Error : ', response.error.message);
            // }
            if(response && response.api_domain){
                UA_OAUTH_PROCESSOR.currentSAASDomain = response.api_domain;
                UA_SAAS_SERVICE_APP.API_BASE_URL = response.api_domain;
            }
            appsConfig.UA_DESK_ORG_ID = response.api_domain;
            UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO = {
                'id': response.api_domain,
                'name': response.api_domain
            };
            UA_SAAS_SERVICE_APP.CURRENT_USER_INFO = response.data;
            appsConfig.APP_UNIQUE_ID = UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.id;
            $("#saasAuthIDName").text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.contact.name).attr('title', `Authorized Freshdesk Account: (${UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.contact.email})`);
            setTimeout((function(){
                $('#ac_name_label_saas .ac_name_id').text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.contact.email);
            }), 2000);
            UA_SAAS_SERVICE_APP.proceedToAppInitializationIfAPPConfigResolved();
        });
        
        /*UA_SAAS_SERVICE_APP.getCurrentOrgInfo(function(response){
            UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO = response.data;
            appsConfig.UA_DESK_ORG_ID = UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO.organisation_id+"";
            //$("#saasAuthIDName").text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.full_name).attr('title', `Authorized Zoho Account: (${UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.email})`);
            UA_SAAS_SERVICE_APP.proceedToAppInitializationIfAPPConfigResolved();
        });*/
    },
    
    addSAASUsersToLICUtility: async function(){
        $('.saasServiceName').text("FreshDesk");
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/api/v2/agents`, "GET", null);
        response.data.sort(function(a, b) {
            return a.contact.name.localeCompare(b.contact.name);
        });
        response.data.forEach(item=>{
            if(item.contact.active){
                UA_LIC_UTILITY.addFetchedSAASUser(item.id, item.contact.name, item.contact.email, null, item.type.includes("Administrator") === true, item.contact.mobile, url = null);
            }
        });
        UA_LIC_UTILITY.saasUserListFetchCompleted();
    },
    
    proceedToAppInitializationIfAPPConfigResolved: function(){
        if(appsConfig.APP_UNIQUE_ID && appsConfig.UA_DESK_ORG_ID){
            UA_SAAS_SERVICE_APP.renderInitialElements();
            UA_SAAS_SERVICE_APP.addSAASUsersToLICUtility();
            UA_APP_UTILITY.appsConfigHasBeenResolved();
            if(UA_SAAS_SERVICE_APP.widgetContext.module == 'contact')
            {
                UA_SAAS_SERVICE_APP.injectShowSearchContact();
            }
            if(isUASMSApp){
                UA_SAAS_SERVICE_APP.addNewWebhook();
            }
            // if(UA_SAAS_SERVICE_APP.widgetContext.module == 'ticket')
            // {
            //     UA_SAAS_SERVICE_APP.renderInitialElementsForFreshDeskList();
            // }
        }
    },
    
    renderInitialElements: async function(){
        UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown('users', ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder']);
//        UA_APP_UTILITY.renderSavedTemplatesInDropdowns();
        if(!UA_SAAS_SERVICE_APP.widgetContext.module){
            UA_SAAS_SERVICE_APP.entityDetailFetched(null);
            console.log('Module does not exist, rendering skipped');
            $("#recip-count-holder").text("Add recipients to proceed");
            return;
        }
                
        ["phone", "mobile"].forEach(item=>{
            // if(contactData[item]){
                UA_APP_UTILITY.addRecipientPhoneFieldType(item, item);
            // }
        });

        await UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(UA_SAAS_SERVICE_APP.widgetContext.module, UA_SAAS_SERVICE_APP.widgetContext.entityId, UA_SAAS_SERVICE_APP.addRecordsAsRecipients);
        await UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], console.log);
        if(UA_SAAS_SERVICE_APP.APP_CLIENT && UA_SAAS_SERVICE_APP.APP_CLIENT.data){
            var contactData = await UA_SAAS_SERVICE_APP.APP_CLIENT.data.get("contact");
            contactData = contactData.contact;
            UA_SAAS_SERVICE_APP.addRecordsAsRecipients([
                contactData
            ]);   
        }            
    },

    // renderInitialElementsForFreshDeskList: async function(){
    //     $(".ssf-new-recip-form").prepend(`<div id="ssf-recip-list-add-holdr" class="ssf-recip-choice-btn" style="margin-top: -5px;background: none;border: none;box-shadow: none;"></div>`);
    //     UA_SAAS_SERVICE_APP.renderContactList();
    // },
    
    // renderContactList: async function(){
    //     var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${UA_SAAS_SERVICE_APP.widgetContext.module}s/filters`, "GET", null);
    //     UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
    //     console.log('renderList',response);
    //     let listDropdownItems = [];
    //     response.data.filters.forEach(item=>{
    //         UA_SAAS_SERVICE_APP.fetchedList[item.id] = item;
    //         listDropdownItems.push({
    //             'label': item.name,
    //             'value': item.id
    //         });
    //     });

    //     UA_APP_UTILITY.renderSelectableDropdown('#ssf-recip-list-add-holdr', 'Select a filter', listDropdownItems, 'UA_SAAS_SERVICE_APP.insertManageListContacts', false, false);
    
    // },

    // insertManageListContacts: async function(listId){

    //     var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${UA_SAAS_SERVICE_APP.widgetContext.module}s/view/${listId}`, "GET", null);
    //     UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
    //     console.log('renderList',response);
    //     if(response.data && response.data[`${UA_SAAS_SERVICE_APP.widgetContext.module}s`] && response.data[`${UA_SAAS_SERVICE_APP.widgetContext.module}s`].length > 0) {
    //         response.data[`${UA_SAAS_SERVICE_APP.widgetContext.module}s`].forEach(item=>{
    //             UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(UA_SAAS_SERVICE_APP.widgetContext.module,item.id, async function(contactList){
    //                 UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
    //                 UA_SAAS_SERVICE_APP.addRecordsAsRecipients(contactList);
    //             });
    //         });
    //     }
            
    // },
    
    sendPhoneFieldsForRecipientType: function(fieldsArray){
        fieldsArray.forEach(item=>{
            if(item.display_label === "phone" || item.display_label === "mobile"){
                UA_APP_UTILITY.addRecipientPhoneFieldType(item.api_name, item.display_label);
            }
        });
    },
    
    populateModuleItemFieldsInDropDown: async function (module, target, fieldsCallback=(()=>{})){
        /*var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.API_BASE_URL}/settings/fields?module=${module}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log(response);
        response.data.fields.forEach(item=>{
            dropDownValues.push({
                "label": item.display_label,
                "value": (module.toUpperCase() === "USERS" ? "CURRENT_USER": module.toUpperCase())+'.'+item.api_name
            });
        }); */
        if(!module || UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.includes(module) == true){
            return;
        }
        UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.push(module);
        var moduleToLoad = module === "users" ? UA_SAAS_SERVICE_APP.CURRENT_USER_INFO : UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS)[0]];
        var moduleFields = Object.keys(moduleToLoad);
        var fieldsArray = [];
        var dropDownValues = [];
        moduleFields.forEach(item=>{
            var itemVal = moduleToLoad[item];
            if(typeof itemVal !== "object" && itemVal !== null && typeof itemVal!=="boolean"){
                fieldsArray.push({
                    "api_name": item,
                    "display_label": item
                });
                dropDownValues.push({
                    "label": item,
                    "value": (module.toUpperCase() === "USERS" ? "CURRENT_USER": module.toUpperCase())+'.'+item
                });
            }
        });
        fieldsCallback(fieldsArray);
        if(typeof target === "string"){
            target = [target];
        }
        target.forEach(item=>{
            UA_APP_UTILITY.renderSelectableDropdown(item, `${module} fields`, dropDownValues, 'UA_APP_UTILITY.templateMergeFieldSelected', false, true);
        });
        return true;
    },
    
    injectShowSearchContact :async function(){
        $(".ssf-new-recip-form").prepend(`<div id="searchAndAddFromContactBtn" class="ssf-recip-choice-btn" onclick="$('.item-list-popup').show()">
                                            <i class="material-icons">search</i> Search from ${UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase().replace("_"," "):"contact"}
                                      </div>`);
        $('.pageContentHolder').append(`<div class="item-list-popup" style="display:none">
                        <div class="item-list-popup-title">
                            Search ${(UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase().replace("_"," "):"contact").capitalize()} <div class="pop-win-close" onclick="$('.item-list-popup').hide();">x</div>
                        </div>
                        <div class="item-search-box">
                            <input class="input-form" id="contact-seach-name" type="text" autocomplete="off" placeholder="Name of the ${UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase():"contact"}"/><br>
                            <div class="btn-save" onclick="UA_SAAS_SERVICE_APP.searchRecordsAndRender($('#contact-seach-name').val(),UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase():'contact')">
                                Search ${(UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase().replace("_"," "):"contact").capitalize()}
                            </div>
                            <div class="btn-reset" onclick="UA_SAAS_SERVICE_APP.resetsearchContacts()">
                                Reset
                            </div>
                        </div>
                        <div class="item-list-popup-content" id="contact-search-items">

                        </div>
                    </div>`);
    },

    resetsearchContacts: function(){
        $("#contact-seach-name").val("");
        $("#contact-search-items").html("");
    },

    searchRecordsAndRender: async function(searchTerm,module){
        if(!valueExists(searchTerm)){
            return;
        }
        UA_SAAS_SERVICE_APP.widgetContext.module = UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module:"contact";
            let url = '';
            if(module && module == "contact") {
                url = `/api/v2/contacts/autocomplete?term=${searchTerm}`;
            }
            await UA_SAAS_SERVICE_APP.getAPIResponseAndCallback( url, function(response){
                console.log(response);
                $("#contact-search-items").html(" ");
                if(response && response.data && response.data && response.data.length>0)
                {
                    var search_items = response.data;
                    if(search_items && search_items.length > 50){
                        search_items.splice(0,49);
                    }
                    UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module] = {};
                    search_items.forEach((obj)=>{
                        UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][obj.id] = obj;
                        var fullName = obj.name ? obj.name:module+"#"+obj.id;
                        if(module)
                        {
                            let itemHTML = `<div class="item-list-popup-item"><span class="c-name" style="cursor:pointer;" onclick="UA_SAAS_SERVICE_APP.selectPhoneNumber('${obj.id}', '${module.trim()}', '${fullName}','${module}','${true}')"> ${fullName} </span>`;
                            itemHTML =  itemHTML + `${'<span class="c-phone '+(UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[obj.id] ? 'alreadyadded': '')+`"> <i class="material-icons">phone_iphone</i>phone (${obj.phone}) </span>`}`;
                            itemHTML =  itemHTML + `${'<span class="c-phone '+(UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[obj.id] ? 'alreadyadded': '')+`"> <i class="material-icons">phone_iphone</i>mobile (${obj.mobile}) </span>`}`;
                            itemHTML =  itemHTML + `</div>`;
                            $("#contact-search-items").append(itemHTML);
                        }
                    });
                }
                else
                {
                    $("#contact-search-items").html("No records found. <br><br> <span class=\"c-silver\"> This search will include <br> only customers having phone numbers</span>");
                    return;
                }
            }).catch(err=>{
                console.log(err);
            });
    },

    selectPhoneNumber: async function(id,number,name,module,isSelected){
        var receipNumberID = id;
        if(!UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] && !UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id]){
            // if(true){
            UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] = {
                'id': id,
                'name': name,
                'module': module,
                'isSelected': isSelected
            };
            $.extend(UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id],UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID]) ;

            if(module && module == "contact") {
                url = `/contacts/${id}`;
            }
            await UA_SAAS_SERVICE_APP.getAPIResponseAndCallback( url, function(response){
                if(response && response.data)
                {
                    UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id] = response.data;
                }
            });
            await UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(module, id, UA_SAAS_SERVICE_APP.addRecordsAsRecipients);
            await UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
            // UA_SAAS_SERVICE_APP.addRecordsAsRecipients([UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id]]);
        }
        else {
            console.log(module+' already added');
            alert(`This ${module} is already added.`);
            return;
        }
    },
    
    getCurrentUserInfo : async function(callback){
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/api/v2/agents/me', callback);
    },
    
    getCurrentOrgInfo : async function(callback){
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/api/v2/account', callback);
    },
    
    getAPIResponseAndCallback: async function(url, callback){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${url}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        callback(response);
    },
    
    showErrorIfInAPICall : async function(response){
        if(response.code === 401 || response.code === 403){
            var loggedInUser = await UA_SAAS_SERVICE_APP.APP_CLIENT.data.get("loggedInUser");
            loggedInUser = loggedInUser.loggedInUser;
            UA_OAUTH_PROCESSOR.showReAuthorizationError("Freshdesk", `<a href="https://${UA_SAAS_SERVICE_APP.API_BASE_URL}/a/profiles/${loggedInUser.id}/edit" target="_blank">Click to get API key</a>`, false, false);
        }
    },
    
    initiateAuthFlow: function(){
        UA_OAUTH_PROCESSOR.initiateAuth();
    },
    
    fetchStoreExecuteOnRecordDetails: async function(module, recordIds, callback){
        if(typeof recordIds == 'string' || typeof recordIds == "number") {
            recordIds = recordIds.toString().split(",");
        }
        $("#recip-count-holder").text(`fetching ${recordIds.length} ${module.toLocaleLowerCase()} information...`);
        // await recordIds.forEach(async (id)=>{
        for(var i=0; i < recordIds.length; i++){
            let id = recordIds[i];
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/api/v2/${module}s/${id}?${module=='ticket'?"include=requester":""}`, "GET", null);
            UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
            if(response.data && response.data.id) {
                UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[response.data.id] = response.data;
                if(module == "ticket" && response.data.requester){
                    UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[response.data.id]['email'] = response.data.requester.email;
                    UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[response.data.id]['phone'] = response.data.requester.phone;
                    UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[response.data.id]['mobile'] = response.data.requester.mobile;
                    UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[response.data.id]['name'] = response.data.requester.name;
                }
            }
        }
        $("#recip-count-holder").text(`Selected ${recordIds.length} ${module.toLocaleLowerCase()}`);
        callback(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
        UA_SAAS_SERVICE_APP.entityDetailFetched();
    },
    
    addRecordsAsRecipients: function(contactsList){
        for(var item in contactsList){
            if($(`[data-recip-id=${contactsList[item]['id']}]`).length == 0) {
                UA_APP_UTILITY.addInStoredRecipientInventory(contactsList[item]);
            }
        }
    },
    
    addSentSMSAsRecordInHistory: async function(sentMessages){
        if(UA_SAAS_SERVICE_APP.widgetContext.module!=="ticket"){
            return;
        }
//        var historyDataArray = [];
        
        var credAdProcess3 = curId++;
        showProcess(`Adding ${Object.keys(sentMessages).length} messages to ticket comments...`, credAdProcess3);
        for(var sentMessageId in sentMessages){
            let sentMessage = sentMessages[sentMessageId];
            let recModule = sentMessage.module?sentMessage.module:UA_SAAS_SERVICE_APP.widgetContext.module; 
            let recId = sentMessage.moduleId?(sentMessage.moduleId.indexOf("newmanual")!==0?sentMessage.moduleId:UA_SAAS_SERVICE_APP.widgetContext.entityId):UA_SAAS_SERVICE_APP.widgetContext.entityId;
            let commentContent = `<u><i>${appPrettyName} Extension</i></u> - `;
            commentContent+=`<b>${sentMessage.channel} ${sentMessage.status} from ${sentMessage.from} to ${sentMessage.to}</b>:<br>
                             ${sentMessage.message}<br>
                                `;
            let response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/api/v2/${recModule}s/${recId}/notes`, "POST", {
                body: commentContent,
                private: true
            });
            UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
            // console.log('addSentSMSAsRecordInHistory',response);
            if(UA_SAAS_SERVICE_APP.APP_CLIENT && UA_SAAS_SERVICE_APP.APP_CLIENT.interface){
                UA_SAAS_SERVICE_APP.APP_CLIENT.interface.trigger("showNotify", {
                    type: "success",
                    message: `All Messages has been added to ${UA_SAAS_SERVICE_APP.widgetContext.module} notes`
                /* The "message" should be plain text */
                }).then(function(data) {
                // data - success message
                }).catch(function(error) {
                // error - error object
                });
            }
        }
        processCompleted(credAdProcess3);      
    },
    entityDetailFetched: function(data){
        setTimeout(()=>{UA_APP_UTILITY.fetchedAllModuleRecords();}, 2000);
        if(typeof UA_TPA_FEATURES.callOnEntityDetailFetched === "function"){
            UA_TPA_FEATURES.callOnEntityDetailFetched(data);
        }
    },

    insertIncomingConfigItems: function(){
        var ignored = UA_SAAS_SERVICE_APP.isWebhookAlreadyExists();
        if(isUASMSApp){
            $('#incoming-module-config-item').append(`
            <div class="tpaichan-item">
                <label class="switch">
                <input ${ignored ? 'checked' : ''} onchange="UA_APP_UTILITY.toggleSAASTicketReplyCaptureConfig()" type="checkbox" id="saas-switch-incoming-comments-enable-capture">
                <div class="slider round"></div>
                </label> Listen for ticket public comments to send replies
            </div>`);
        }
    },
    
    EXISTING_WEBHOOK_ID: null,
    isWebhookAlreadyExists: async function(){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/api/v2/automations/4/rules`, "GET", {}, {'Accept' :"application/json"});
        if(UA_SAAS_SERVICE_APP.showIfErrorInAPIResult(response)){
            return;
        }
        let existingServiceID = null;
        if(!response || !response.data || response.data.length <= 0){
            return false;
        }
        response.data.forEach((item)=>{
            if(item.actions && item.actions && item.actions[0] && item.actions[0].url && item.actions[0].url.startsWith(UA_APP_UTILITY.getSAASWebhookBaseURL())){
                existingServiceID = item.id;
            }            
        });
        UA_SAAS_SERVICE_APP.EXISTING_WEBHOOK_ID = existingServiceID;
        // await UA_SAAS_SERVICE_APP.isTriggerAlreadyExists(existingServiceID);
        return UA_SAAS_SERVICE_APP.EXISTING_WEBHOOK_ID !== null;
    },

    addNewWebhook: async function(){
        if(!UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS){
            UA_APP_UTILITY.CALL_AFTER_PERSONAL_COMMON_SETTING_RESOLVED.push(UA_SAAS_SERVICE_APP.addNewWebhook);
            return false;
        }
        if(UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.hasOwnProperty('ua_incoming_saas_ticket_reply_enable_capture_'+currentUser.uid) && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS['ua_incoming_saas_ticket_reply_enable_capture_'+currentUser.uid] === false){
            return;
        }
        if(UA_APP_UTILITY.PERSONAL_WEBHOOK_TOKEN && appsConfig.UA_DESK_ORG_ID){
            var webhookExists = await UA_SAAS_SERVICE_APP.isWebhookAlreadyExists();
            if(!webhookExists){
                var webhookData = {
                    "name": "Ulgebra Intergrations - "+appPrettyName+" Comment Listener",
                    "position": 1,
                    "active": true,
                    "performer": {
                        "type": 1
                    },
                    "events": [
                        {
                            "field_name": "note_type",
                            "value": "public"
                        }
                    ],
                    "actions": [
                        {
                            "request_type": "POST",
                            "url": UA_APP_UTILITY.getAuthorizedSAASWebhookURL(),
                            "field_name": "trigger_webhook",
                            "content_type": "JSON",
                            "content_layout": "1",
                            "content": {
                                "ticket_id": "{{ticket.id}}",
                                "ticket_latest_public_comment": "{{ticket.latest_public_comment}}",
                                "ticket_latest_private_comment": "{{ticket.latest_private_comment}}",
                                "ticket_contact_mobile": "{{ticket.contact.mobile}}",
                                "ticket_contact_email": "{{ticket.contact.email}}",
                                "ticket_contact_phone": "{{ticket.contact.phone}}"
                            }
                        }
                    ]
                };
                var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/api/v2/automations/4/rules`, "POST", webhookData);
                if(UA_SAAS_SERVICE_APP.showIfErrorInAPIResult(response)){
                    return false;
                }
                UA_SAAS_SERVICE_APP.EXISTING_WEBHOOK_ID = response.data.id;
                UA_SAAS_SERVICE_APP.incomingCaptureStatusChanged();
                return response.data && (response.data.id !== null);
            }
        }
        return true;
    },

    incomingCaptureStatusChanged: function(){
        if(UA_SAAS_SERVICE_APP.EXISTING_WEBHOOK_ID){
            document.getElementById('saas-switch-incoming-comments-enable-capture').checked = true;
        }
        else{
            document.getElementById('saas-switch-incoming-comments-enable-capture').checked = false;
        }
    },

    showIfErrorInAPIResult: function(response){
        console.log(response);
        if((response.code === 401 || response.message === "Authentication failed") || (response.error && response.error.code === 401) || (response.data && (response.data.code === 401 || response.data.message === "Authentication failed"))){
            UA_OAUTH_PROCESSOR.showReAuthorizationError();
            return true;
        }
        return false;
    },
    deleteExistingWebhook: async function(){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/api/v2/automations/4/rules/${UA_SAAS_SERVICE_APP.EXISTING_WEBHOOK_ID}`, "DELETE", {});
        if (UA_SAAS_SERVICE_APP.showIfErrorInAPIResult(response)){
            return false;
        }
        UA_SAAS_SERVICE_APP.EXISTING_WEBHOOK_ID = null;
        UA_SAAS_SERVICE_APP.incomingCaptureStatusChanged();
        return true;
    },
    // RENDER_RAW_FIELD_ID_IN_FIELDMAPPING: true,
    LOOKUP_SUPPORTED_MODULES: {
        "tickets": {
            "displayLabel": "Tickets",
            "apiName": "tickets",
            "autoLookup": true,
        },
        "contacts": {
            "displayLabel": "Contacts",
            "apiName": "contacts",
            "hideDisplay": true
        }
    },
    allModuleFieldsFetched: function(){
        let fieldsPresentModuleCount = 0;
        Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).forEach(item=>{
            if(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[item].fields && Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[item].fields).length > 0){
                fieldsPresentModuleCount++;
            }
        });
        return Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).length === fieldsPresentModuleCount;
    },
    retrieveModuleFieldsForLookUp: function(callback){
        $('#ua-loo-search-field-ddholder').hide();
        if (UA_SAAS_SERVICE_APP.allModuleFieldsFetched()) {
            callback();
        }
        let primaryFieldsToCreate = ["subject"];
        let primaryFieldsToViewForModule = [];
        Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).forEach(async moduleId=>{
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/api/v2/${moduleId.substr(0, moduleId.length - 1)}_fields`, "GET", null);
            // if(response.error && response.error.errorCode === "LICENSE_ACCESS_LIMITED"){
            //     response = UA_SAAS_SERVICE_APP.getSampleMockData("layoutfields_tickets");
            // }
            if(!response.data || response.data.length < 1){
                return;
            }
            let fieldAPINameMap = {};
            console.log(response);
            response.data.forEach(fieldItem=>{
                try{
                    primaryFieldsToCreate = [];
                    primaryFieldsToViewForModule = [];
                    if(!fieldItem.choices){
                        fieldItem.field_type = "text";
                    }
                    if(fieldItem.type.includes("date") === true){
                        fieldItem.field_type = "datetime";
                    }
                    if(fieldItem.choices){
                        fieldItem.field_type = "picklist";
                        fieldItem.pick_list_values = [];
                        if(fieldItem.choices.length < 1){
                            return true;
                        }
                        Object.keys(fieldItem.choices).forEach(optItemKey=>{
                            let optItemValue = fieldItem.choices[optItemKey];
                            fieldItem.pick_list_values.push({
                                'actual_value': fieldItem.name=="status"?optItemKey:optItemValue,
                                'display_value': fieldItem.name=="status"?optItemValue:optItemKey
                            });
                        });
                    }
                    if(fieldItem.name == "agent") fieldItem.name = "responder";
                    if(["group","agent","responder"].includes(fieldItem.name) == true)fieldItem.name = fieldItem.name + "_id";
                    if(["ticket_type"].includes(fieldItem.name) == true)fieldItem.name = fieldItem.name.split("_")[1];
                    
                    if(["requester", "company"].includes(fieldItem.name) != true){
                        fieldAPINameMap[fieldItem.name] = {
                            'display_label': fieldItem.label_for_customers,
                            'data_type': fieldItem.field_type,
                            'api_name': fieldItem.name,
                            'field_read_only': false,
                            'system_mandatory': (fieldItem.required_for_customers || fieldItem.required_for_agents)?true:false,
                            'pick_list_values' : fieldItem.pick_list_values
                        };
                    }
                    if(fieldItem.required_for_customers || fieldItem.required_for_agents){
                        primaryFieldsToCreate.push(fieldItem.name);
                    }
                }
                catch(ex){
                    console.log(ex);
                }
            });
            fieldAPINameMap.id = {
              'display_label': 'ID',
              'data_type': 'number',
              'api_name': 'id',
              'field_read_only': true
            };
            UA_TPA_FEATURES.setCreateModuleRecordFields(moduleId, primaryFieldsToCreate);
            UA_TPA_FEATURES.setViewModuleRecordFields(moduleId, primaryFieldsToViewForModule);
            UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[moduleId].fields = fieldAPINameMap;
            if(UA_SAAS_SERVICE_APP.allModuleFieldsFetched()){
                callback();
            }
        });
    }
};


async function sendAlteredConfigParamToFreshdesk(){
    parent.postMessage({
            "eventType": "setConfigValue",
            "key": "isAllow",
            "value": true
         }, "*");
}