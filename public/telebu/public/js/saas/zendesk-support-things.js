var UA_SAAS_SERVICE_APP = {
    
    CURRENT_USER_INFO : null,
    CURRENT_ORG_INFO : null,
    CURRENT_OAUTH_SCOPE_NEEDED : ['tickets:read', 'tickets:write', 'users:read', 'users:write',  'webhooks:read', 'webhooks:write', 'organizations:read', 'triggers:read', 'triggers:write'],
    FETCHED_RECORD_DETAILS: {},
    CURRENT_MODULE_FIELDS_DROPDOWN : [],
    SELECTED_RECEIPS: {},
    SEARCH_MODULE_RECORD_MAP: {},
    fetchedList : {},
    widgetContext: {},
    appsConfig: {},
    APP_API_NAME: "pinnaclesms__",
    isModelOpened: false,
    DESK_APP: null,
    FETCHED_DEPARTMENTS: {},
    supportedIncomingModules: [
        {
            "selected": true,
            "value": "tickets",
            "label": "tickets"
        }
    ],
    getAPIBaseUrl : function(){
        return `/api/v2`;
    },
    
    provideSuggestedCurrentEnvLoginEmailID: async function(){
        var client = ZAFClient.init();
        if(!client){
            return false;
        }
        UA_SAAS_SERVICE_APP.DESK_APP = client;
        UA_SAAS_SERVICE_APP.DESK_APP.context().then(function(context){
            if(context.location === "ticket_sidebar"){
                UA_SAAS_SERVICE_APP.applySpecificCSS();
                currentUserCall = UA_SAAS_SERVICE_APP.DESK_APP.get('currentUser');
            }
            else{
                currentUserCall = client.context();
            }
            currentUserCall.then(cuser=>{
                UA_APP_UTILITY.receivedSuggestedCurrentEnvLoginEmailID(cuser.currentUser.email);
            });
        });
    },
    
    initiateAPPFlow: async function(){
        var client = ZAFClient.init();
        if(client){
            UA_SAAS_SERVICE_APP.DESK_APP = client;
            client.invoke('resize', { width: '100%', height: '600px' });
            var locationVSConfigMap = {
                "ticket_sidebar":{
                    "targetObj" : "ticket"
                },
                "user_sidebar":{
                    "targetObj" : "user"
                },
                "top_bar": {
                    "targetObj": "contact"
                }
            };
            var currentUserCall = null;
            client.context().then(function(context){
                client.get(locationVSConfigMap[context.location]['targetObj']).then(function(data) {
                    var numbersAdded = 0;
                    entityObj = data[locationVSConfigMap[context.location]['targetObj']];
                    UA_SAAS_SERVICE_APP.widgetContext.module = locationVSConfigMap[context.location]['targetObj']+"s";
                    UA_SAAS_SERVICE_APP.widgetContext.entity = entityObj;
                    UA_SAAS_SERVICE_APP.widgetContext.entityId = entityObj.id+"";
                    UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[entityObj.id+""] = entityObj;
                });
            });
        }
        UA_SAAS_SERVICE_APP.injectShowSearchContact();
        UA_SAAS_SERVICE_APP.initiateAPPFlow_SA();
    },

    initiateAPPFlow_SA: function(){

        if(!UA_SAAS_SERVICE_APP.widgetContext.module && queryParams.get("module")){
            UA_SAAS_SERVICE_APP.widgetContext.module = queryParams.get("module").endsWith('s') ? queryParams.get("module").substr(0, queryParams.get("module").length-1) : queryParams.get("module");
            UA_SAAS_SERVICE_APP.widgetContext.entityId = queryParams.get("entityId");
            UA_SAAS_SERVICE_APP.injectShowSearchContact(); 
        }
        $("#ac_name_label_saas .anl_servicename").text('Zendesk Support');
        this.insertIncomingConfigItems();
        UA_SAAS_SERVICE_APP.getCurrentUserInfo(function(response){
            if(extensionName.startsWith('lookupfor') || queryParams.get("disableSAASSdk")){
                if(response.api_domain){
                    UA_OAUTH_PROCESSOR.currentSAASDomain = response.api_domain;
                }
            }
            UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO = {
                'id': response.api_domain,
                'name': response.api_domain
            };
            appsConfig.UA_DESK_ORG_ID = response.api_domain;
            UA_SAAS_SERVICE_APP.CURRENT_USER_INFO = response.data.user;
            appsConfig.APP_UNIQUE_ID = UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.id;
            $("#saasAuthIDName").text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.name).attr('title', `Authorized Zendesk Account: (${UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.name})`);
            setTimeout((function(){
                $('#ac_name_label_saas .ac_name_id').text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.email);
            }), 2000);
            UA_SAAS_SERVICE_APP.proceedToAppInitializationIfAPPConfigResolved();
        });
    },
    
    saasUsersListCache: [],
    addSAASUsersToLICUtility: async function(url=null){
        try{
            url = url?url:`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/users?per_page=100&role[]=agent&role[]=admin`
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(url, "GET", null);
            if(response && response.data && response.data.users){
                response.data.users.sort(function(a, b) {
                    return a.name.localeCompare(b.name);
                });
                response.data.users.forEach(item=>{
                    UA_SAAS_SERVICE_APP.saasUsersListCache.push(item);
                });

                if(response.data.next_page){
                    await UA_SAAS_SERVICE_APP.addSAASUsersToLICUtility(`/api/v2/users?${response.data.next_page.split("?")[1]}`);
                    return;
                }
                else{
                    if(UA_SAAS_SERVICE_APP.saasUsersListCache.length < 1) return;
                    UA_SAAS_SERVICE_APP.saasUsersListCache.forEach(item=>{
                        UA_LIC_UTILITY.addFetchedSAASUser(item.id, item.name, item.email, null, item.role === "admin", item.phone = null, null);
                    });
                    UA_LIC_UTILITY.saasUserListFetchCompleted();
                }
            }
        }
        catch(err){
            console.log(err);
        }
    },
    
    proceedToAppInitializationIfAPPConfigResolved: function(){
        if(appsConfig.APP_UNIQUE_ID && appsConfig.UA_DESK_ORG_ID){
            UA_SAAS_SERVICE_APP.renderInitialElements();
            UA_SAAS_SERVICE_APP.addSAASUsersToLICUtility();
            UA_APP_UTILITY.appsConfigHasBeenResolved();
            if(UA_SAAS_SERVICE_APP.widgetContext.module)
            {
                UA_SAAS_SERVICE_APP.renderInitialElementsForZohoDeskList();
            }
            this.fetchAndStoreDefaultDepartments(function(response){
                response.data.organizations.forEach(item=>{
                    UA_SAAS_SERVICE_APP.FETCHED_DEPARTMENTS[item.id] = item;
                });
                UA_SAAS_SERVICE_APP.renderDefaultDepartmentsInIncomingSelect();
            });
            if(isUASMSApp){
                UA_SAAS_SERVICE_APP.addNewWebhook();
            }
        }
    },
    
    renderInitialElements: async function(){
        UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown('users', ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder']);
//        UA_APP_UTILITY.renderSavedTemplatesInDropdowns();
        /*if(UA_SAAS_SERVICE_APP.widgetContext.module !== "ticket" && $("#inp_chk_create_new_ticket_on_msg").length < 1){
            $('.ssf-win-actions-holder').prepend(`<div style="padding: 0px 0px 10px 0px;"><input id="inp_chk_create_new_ticket_on_msg" name="inp_chk_create_new_ticket_on_msg" type="checkbox"> <label for="inp_chk_create_new_ticket_on_msg">Create a new ticket </label></div>`);
        }*/
        if(!UA_SAAS_SERVICE_APP.widgetContext.module){
            UA_SAAS_SERVICE_APP.entityDetailFetched(null);
            console.log('Module does not exist, rendering skipped');
            $("#recip-count-holder").text("Add recipients to proceed");
            return;
        }
        await UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
        UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(UA_SAAS_SERVICE_APP.widgetContext.module, UA_SAAS_SERVICE_APP.widgetContext.entityId, UA_SAAS_SERVICE_APP.addRecordsAsRecipients);
    },

    applySpecificCSS: function(){
        $('.siteName').css('visibility', 'hidden');
        $('#nav_viewTypeSwitch').css('left', '-100px');
        $('div#nav_viewTypeSwitch .dropdownOuter').css('width', 'max-content');
    },

    renderInitialElementsForZohoDeskList: async function(){
        /*$(".ssf-new-recip-form").prepend(`<div id="ssf-recip-list-add-holdr" class="ssf-recip-choice-btn" style="margin-top: -5px;background: none;border: none;box-shadow: none;"></div>`);
        UA_SAAS_SERVICE_APP.renderContactList();*/
    },
    
    renderContactList: async function(){
        // let departmentId = await ZOHODESK.get('department.id');
        // departmentId = departmentId['department.id'];
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/views?module=${UA_SAAS_SERVICE_APP.widgetContext.module}s`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('renderList',response);
        let listDropdownItems = [];
        if(response && response.data && response.data.data.length > 0){
            response.data.data.forEach(item=>{
                UA_SAAS_SERVICE_APP.fetchedList[item.id] = item;
                listDropdownItems.push({
                    'label': item.name,
                    'value': item.id
                });
            });
        }

        UA_APP_UTILITY.renderSelectableDropdown('#ssf-recip-list-add-holdr', 'Select a filter', listDropdownItems, 'UA_SAAS_SERVICE_APP.insertManageListContacts', false, false);
    
    },

    insertManageListContacts: async function(listId){

        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${UA_SAAS_SERVICE_APP.widgetContext.module}s?viewId=${listId}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('renderList',response);
        if(response && response.data && response.data.data && response.data.data.length > 0) {
            response.data.data.forEach(item=>{
                UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id] = item;
                UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(UA_SAAS_SERVICE_APP.widgetContext.module, item.id, UA_SAAS_SERVICE_APP.addRecordsAsRecipients);
                UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
            });
        }
        else{
            alert(`We can't found records in list or list is empty.`);
        }
            
    },
    
    sendPhoneFieldsForRecipientType: function(fieldsArray){
        fieldsArray.forEach(item=>{
            if(item.display_label === "phone" || item.display_label === "mobile"){
                UA_APP_UTILITY.addRecipientPhoneFieldType(item.api_name, item.display_label);
            }
        });
    },
    
    populateModuleItemFieldsInDropDown: async function (module, target, fieldsCallback=(()=>{})){
        /*var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.API_BASE_URL}/settings/fields?module=${module}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log(response);
        response.data.fields.forEach(item=>{
            dropDownValues.push({
                "label": item.display_label,
                "value": (module.toUpperCase() === "USERS" ? "CURRENT_USER": module.toUpperCase())+'.'+item.api_name
            });
        }); */
        try{
        if(!module || UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.includes(module) == true){
            return;
        }
        UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.push(module);
        var moduleToLoad = module === "users" ? UA_SAAS_SERVICE_APP.CURRENT_USER_INFO : UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[UA_SAAS_SERVICE_APP.widgetContext.entityId?UA_SAAS_SERVICE_APP.widgetContext.entityId:Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS)[0]];
        var moduleFields = Object.keys(moduleToLoad);
        var fieldsArray = [];
        var dropDownValues = [];
        moduleFields.forEach(item=>{
            let itemVal = moduleToLoad[item];
            if(typeof itemVal !== "object" && itemVal !== null && typeof itemVal!=="boolean"){
                fieldsArray.push({
                    "api_name": item,
                    "display_label": item
                });
                dropDownValues.push({
                    "label": item,
                    "value": (module.toUpperCase() === "USERS" ? "CURRENT_USER": module.toUpperCase())+'.'+item
                });
            }
        });
        fieldsCallback(fieldsArray);
        if(typeof target === "string"){
            target = [target];
        }
        target.forEach(item=>{
            UA_APP_UTILITY.renderSelectableDropdown(item, `${module} fields`, dropDownValues, 'UA_APP_UTILITY.templateMergeFieldSelected', false, true);
        });
        return true;
    }catch(e){ console.log(e); return false}
    },
    
    injectShowSearchContact :async function(){
        return false;
        // UA_SAAS_SERVICE_APP.widgetContext.module = UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module:"contact";
        $(".ssf-new-recip-form").prepend(`<div id="searchAndAddFromContactBtn" class="ssf-recip-choice-btn" onclick="$('.item-list-popup').show()">
                                            <i class="material-icons">search</i> Search from ${UA_SAAS_SERVICE_APP.widgetContext.module ? UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase():"contact"}
                                      </div>`);
        $('.pageContentHolder').append(`<div class="item-list-popup" style="display:none">
                        <div class="item-list-popup-title">
                            Select a ${UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase():"contact"} <div class="pop-win-close" onclick="$('.item-list-popup').hide();">x</div>
                        </div>
                        <div class="item-search-box">
                            <input class="input-form" id="contact-seach-name" type="text" autocomplete="off" placeholder="name of the ${UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase():"contact"}"/><br>
                            <div class="btn-save" onclick="UA_SAAS_SERVICE_APP.searchRecordsAndRender($('#contact-seach-name').val(),UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase():'contact')">
                                Search ${UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase():"contact"}
                            </div>
                            <div class="btn-reset" onclick="UA_SAAS_SERVICE_APP.resetsearchContacts()">
                                Reset
                            </div>
                        </div>
                        <div class="item-list-popup-content" id="contact-search-items">

                        </div>
                    </div>`);
    },

    resetsearchContacts: function(){
        $("#contact-seach-name").val("");
        $("#contact-search-items").html("");
    },

    searchRecordsAndRender: async function(searchTerm,module){
        if(!valueExists(searchTerm)){
            return;
        }
        UA_SAAS_SERVICE_APP.widgetContext.module = UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module:"contact";
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${module}s/search?_all=${searchTerm}*`,"GET",{});
            if(response)
            {    
                console.log(response);
                UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
                $("#contact-search-items").html(" ");
                if(response.data && response.data["data"].length>0)
                {
                    var search_items = response.data["data"];
                    if(search_items && search_items.length > 50){
                        search_items.splice(0,49);
                    }
                    UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module] = {};
                    search_items.forEach((obj)=>{
                        UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][obj.id] = obj;
                        let fullName = "";
                        if(module)
                        {
                            switch(module){
                                case("contact"): {
                                    fullName = obj.firstName ? obj.firstName:"";
                                    if(obj.lastName){
                                        fullName = fullName+" "+ obj.lastName;
                                    } 
                                    fullName = fullName?fullName:"Contact#"+obj.id;
                                    break;
                                }
                                case("ticket"):{
                                    fullName = "Ticket#"+obj.ticketNumber;
                                    break;
                                }
                                case("account"):{
                                    fullName = obj.accountName ? obj.accountName:"Account#"+obj.code;
                                    break;
                                }
                            }
                        }
                        let itemHTML = `<div class="item-list-popup-item" style="cursor:pointer;" onclick="UA_SAAS_SERVICE_APP.selectPhoneNumber('${obj.id}', '${module.trim()}', '${fullName}','${module}','${true}')"><span class="c-name"> ${fullName} </span>`;
                        itemHTML =  itemHTML + `${'<span class="c-phone '+(UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[module+"_"+obj.id] ? 'alreadyadded': '')+'"><i class="material-icons">phone_iphone</i>(phone)</span>'}`;
                        itemHTML =  itemHTML + `</div>`;
                        $("#contact-search-items").append(itemHTML);
                    });
                }
                else
                {
                    $("#contact-search-items").html("No records found. <br><br> <span class=\"c-silver\"> This search will include <br> only customers having phone numbers</span>");
                    return;
                }
            }
    },

    selectPhoneNumber: async function(id,number,name,module,isSelected){
        var receipNumberID = module + "_" + id;
        if(!UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] && !UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id]){
            UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] = {
                'id': id,
                'name': name,
                'module': module,
                'isSelected': isSelected
            };
            $.extend(UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id],UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID]) ;
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id] = UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id];
            // UA_SAAS_SERVICE_APP.renderInitialElements(module,id);
            
            if(UA_SAAS_SERVICE_APP.widgetContext.module !== "ticket" && $("#inp_chk_create_new_ticket_on_msg").length < 1){
                $('.ssf-win-actions-holder').prepend(`<div style="padding: 0px 0px 10px 0px;"><input id="inp_chk_create_new_ticket_on_msg" name="inp_chk_create_new_ticket_on_msg" type="checkbox"> <label for="inp_chk_create_new_ticket_on_msg">Create a new ticket </label></div>`);
            }
            await UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
            UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(module, id, UA_SAAS_SERVICE_APP.addRecordsAsRecipients);
            // UA_SAAS_SERVICE_APP.addRecordsAsRecipients([UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id]]);
        }
        else {
            console.log(module+' already added');
            // showErroWindow("Alert Message",`This ${module.toLocaleLowerCase()} is already added.`)
            alert(`This ${module.toLocaleLowerCase()} is already added.`);
            return;
        }
    },

    getCurrentUserInfo : async function(callback){
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/users/me.json', callback);
    },
    
    getCurrentOrgInfo : async function(callback){
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/organizations.json', callback);
    },
    
    getAPIResponseAndCallback: async function(url, callback){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}${url}${UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO ? '?orgId='+UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO.id : ''}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        callback(response);
    },
    
    showErrorIfInAPICall : function(response){
        if(response.code === 401 || response.code === 403){
            UA_OAUTH_PROCESSOR.showReAuthorizationError('Zendesk Support');
        }
    },
    
    initiateAuthFlow: function(){
        UA_OAUTH_PROCESSOR.showReAuthorizationError('Zendesk Support');
    },
    
    fetchStoreExecuteOnRecordDetails: async function(module, recordIds, callback){

        /*if(typeof recordIds !== "object"){
            var recordId = recordIds;
            recordIds = [recordId];
        }
//        recordIds = ["5318123000000377412"];
        $("#recip-count-holder").text(`fetching ${recordIds.length} ${module.toLocaleLowerCase()} information...`);
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.API_BASE_URL}/${module}?ids=${recordIds.join(',')}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('fetchStoreExecuteOnRecordDetails',response);
        response.data.data.forEach(recordItem=>{
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordItem.id] = recordItem;
        });
        $("#recip-count-holder").text(`Selected ${recordIds.length} ${module.toLocaleLowerCase()}`);*/
        
        callback(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
        UA_SAAS_SERVICE_APP.entityDetailFetched();
    },

    fetchAndStoreDefaultDepartments: async function(callback) { 
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/organizations', callback);
    },
    
    addRecordsAsRecipients: function(contactsList){
        for(var item in contactsList){
            let contactItem = contactsList[item];
            if($(`[data-recip-id=${contactsList[item]['id']}]`).length === 0) {
                if(!contactItem.name){
                    if(UA_SAAS_SERVICE_APP.widgetContext.module === "users") {
                        contactItem.name = contactItem.name;
                    }
                    else if(UA_SAAS_SERVICE_APP.widgetContext.module === "tickets") {
                        contactItem = contactItem.requester;
                    }
                }
                let i = 0;
                let j =0;
                contactItem.identities.forEach(identityItem=>{
                    if(identityItem.type === "phone_number"){
                        let propName = "phone";
                        i++;
                        if(i > 1){
                            propName = propName+'_'+i;
                        }
                        contactItem[propName] = identityItem.value;
                        UA_APP_UTILITY.addRecipientPhoneFieldType(propName, propName);
                    }
                    contactItem.emails = [];
                    if(identityItem.type === "email"){
                        j++;
                        let propName = "email";
                        if(j > 1){
                            propName = propName+'_'+j;
                        }
                        contactItem.emails.push({
                            'label': propName,
                            'value': identityItem.value
                        })
                    }
                    try{
                        if(identityItem.type === "any_channel" && identityItem.value){
                            if(identityItem.value.indexOf("@") > 0){
                                j++;
                                let propName = "email";
                                if(j > 1){
                                    propName = propName+'_'+j;
                                }
                                contactItem.emails.push({
                                    'label': propName,
                                    'value': identityItem.value
                                })
                            }
                            else{
                                i++;
                                let propName = "phone";
                                if(i > 1){
                                    propName = propName+'_'+i;
                                }
                                identityItem.value = identityItem.value.indexOf(":") > 0 ? identityItem.value.split(":")[1] : identityItem.value;
                                contactItem[propName] = identityItem.value;
                                UA_APP_UTILITY.addRecipientPhoneFieldType(propName, propName);
                            }
                        }
                    }
                    catch(ex){
                        console.log(ex);
                    }
                });
                UA_APP_UTILITY.addInStoredRecipientInventory(contactItem);
            }
        }
    },
    showExpandedModalView: function(){
        UA_SAAS_SERVICE_APP.DESK_APP.instance.modal({
                                        url: window.location.href+"&openMode=modal",
                                        title: appPrettyName+" SMS"
                                        }).then(function(modalInfo) {
                                        var modalInstance = UA_SAAS_SERVICE_APP.DESK_APP.instance.getWidgetInstance(modalInfo.widgetID);
                                        modalInstance.on('modal.opened', function(data) {
                                        // console.log('modal opened ++++++++++++++++++', data);
                                        
                                            modalInstance.emit('event', {
                                                "dataType": "initialContextModules",
                                                'UA_SAAS_SERVICE_APP.widgetContext.module': UA_SAAS_SERVICE_APP.widgetContext.module,
                                                'UA_SAAS_SERVICE_APP.widgetContext.entity' : UA_SAAS_SERVICE_APP.widgetContext.entity
                                            });
                                        });
                                        }).catch(function(err) {
                                        console.log(err, "Modal error");
                                    });
    },
    
    addSentSMSAsRecordInHistory: async function(sentMessages){
//        var historyDataArray = [];
        if(!UA_SAAS_SERVICE_APP.widgetContext.module){
            return;
        }
        var credAdProcess3 = curId++;
        showProcess(`Adding ${Object.keys(sentMessages).length} messages to ${UA_SAAS_SERVICE_APP.widgetContext.module} comments...`, credAdProcess3);

        var commentContent = `<u><b>${appPrettyName} Integration</b></u> - `;
        for(var sentMessageId in sentMessages){
            let sentMessage = sentMessages[sentMessageId];
            let recModule = sentMessage.module?sentMessage.module:UA_SAAS_SERVICE_APP.widgetContext.module; 
            let recId = sentMessage.moduleId?(sentMessage.moduleId.indexOf("newmanual")!==0?sentMessage.moduleId:UA_SAAS_SERVICE_APP.widgetContext.entityId):UA_SAAS_SERVICE_APP.widgetContext.entityId;
//            let historyData = {};
//            historyData['Name'] = 'SMS to '+(sentMessage.contact.name ? sentMessage.contact.name : sentMessage.number);
//            historyData[UA_SAAS_SERVICE_APP.APP_API_NAME+'Message'] = sentMessage.message;
//            historyData[UA_SAAS_SERVICE_APP.APP_API_NAME+'Customer_Number'] = sentMessage.to;
//            historyData[UA_SAAS_SERVICE_APP.APP_API_NAME+'From'] = sentMessage.from;
//            historyData[UA_SAAS_SERVICE_APP.APP_API_NAME+'Direction'] = "Outbound";
//            historyData[UA_SAAS_SERVICE_APP.APP_API_NAME+'Status'] = sentMessage.status;
//            historyDataArray.push(historyData);
            let commentContent = `<u><b>${appPrettyName} Integration</b></u> - `;
            commentContent+=` Outgoing ${sentMessage.channel} ${sentMessage.status.toLocaleLowerCase()} from ${sentMessage.from} to ${sentMessage.to}:<br>
                             ${sentMessage.message}
                                `;
                    // if(document.getElementById('inp_chk_create_new_ticket_on_msg').checked){
                    //     let departmentId = await ZOHODESK.get('department.id');
                    //     departmentId = departmentId['department.id'];
                    //     let contactId =  null;
                    //     if(UA_SAAS_SERVICE_APP.widgetContext.module === "contact"){
                    //         contactId = recId;
                    //     }
                    //     if(UA_SAAS_SERVICE_APP.widgetContext.module === "ticket"){
                    //         contactId = UA_SAAS_SERVICE_APP.widgetContext.entity.contactId;
                    //     }
                    //     UA_SAAS_SERVICE_APP.createTicketForContact(departmentId, contactId, appPrettyName+" Message Sent to "+sentMessage.to, commentContent, sentMessage.to);
                    // }
            
            let response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/${recModule.toLocaleLowerCase()}/${recId}.json`, "PUT", {
                "ticket":{
                    "comment":{
                        public: false,
                        type: "comment",
                        html_body: commentContent.replace(/\n/g, "<br>"),
                        "via": {
                                "channel": UA_APP_UTILITY.getAppPrettyName(extensionName),
                                "source": {
                                "to": {
                                    "name": sentMessage.to
                                },
                                "from": {
                                    "name": sentMessage.from
                                },
                                "rel": "direct_message"
                            }
                        }
                    }
                }
            });
            
            UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
            // console.log('addSentSMSAsRecordInHistory',response);
        }
        processCompleted(credAdProcess3);
    },

    async createTicketForContact(departmentId, contactId, subject, description, phone){
        var credAdProcess3 = curId++;
        showProcess(`Creating ticket...`, credAdProcess3);
        var ticketCreateObj = {
            subject: subject,
            description: description,
            departmentId: departmentId,
            phone: phone
        };
        if(contactId){
            ticketCreateObj.contactId = contactId;
        }
        else{
            ticketCreateObj.contact = {
                'phone': phone,
                'name': phone
            };
        }
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/tickets`, "POST", ticketCreateObj);
        processCompleted(credAdProcess3);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        // console.log('addSentSMSAsRecordInHistory',response);
        
    },
    entityDetailFetched: function(data){
        setTimeout(()=>{UA_APP_UTILITY.fetchedAllModuleRecords();}, 2000);
        if(typeof UA_TPA_FEATURES.callOnEntityDetailFetched === "function"){
            UA_TPA_FEATURES.callOnEntityDetailFetched(data);
        }
    },
    renderDefaultDepartmentsInIncomingSelect: function(){
        $('#inc-sel-incoming-def-dept').html('');
        for(let item in this.FETCHED_DEPARTMENTS){
            let department = this.FETCHED_DEPARTMENTS[item];
            $('#inc-sel-incoming-def-dept').append(`<option value="${department.id}">${department.name}</option>`);
        }
    },
    insertIncomingConfigItems: function(){
        $('#incoming-module-config-item').append(`
            <div class="tpaichan-item">
                Default department for ticket creation <select onchange="UA_SAAS_SERVICE_APP.updateDefaultDepartmentIdForTicket()" id="inc-sel-incoming-def-dept"><option>Loading...</option></select>
            </div>`);
        if(isUASMSApp){
            $('#incoming-module-config-item').append(`
            <div class="tpaichan-item">
                <label class="switch">
                <input onchange="UA_APP_UTILITY.toggleSAASTicketReplyCaptureConfig()" type="checkbox" id="saas-switch-incoming-comments-enable-capture">
                <div class="slider round"></div>
                </label> Listen for ticket public comments to send replies
            </div>`);
        }
    },
    updateDefaultDepartmentIdForTicket: function(){
        UA_APP_UTILITY.saveSettingInCredDoc('ua_incoming_saas_default_department_id', $('#inc-sel-incoming-def-dept').val());
    },
    EXISTING_WEBHOOK_ID: null,
    serviceSID: null,
    existingPreWebhookID: null,
    isWebhookAlreadyExists: async function(){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/webhooks`, "GET", {}, {'Accept' :"application/json"});
        if(UA_SAAS_SERVICE_APP.showIfErrorInAPIResult(response)){
            return;
        }
        let existingServiceID = null;
        if(!response || !response.data){
            return false;
        }
        response.data.webhooks.forEach((item)=>{
            let url = item.endpoint;
            if(url && url.startsWith(UA_APP_UTILITY.getSAASWebhookBaseURL())){
                existingServiceID = item.id;
            }
        });
        UA_SAAS_SERVICE_APP.existingPreWebhookID = existingServiceID;
        await UA_SAAS_SERVICE_APP.isTriggerAlreadyExists(existingServiceID);
        return UA_SAAS_SERVICE_APP.EXISTING_WEBHOOK_ID !== null;
    },
    existingTriggerID: null,
    isTriggerAlreadyExists: async function(webhookID){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/triggers/search?query=ULGEBRA`, "GET", {}, {'Accept' :"application/json"});
        if(UA_SAAS_SERVICE_APP.showIfErrorInAPIResult(response)){
            return;
        }
        let existingServiceID = null;
        if(!response || !response.data){
            return false;
        }
        response.data.triggers.forEach((item)=>{
            try{
                item.actions.forEach(actionItem=>{
                    if(actionItem.field === "notification_webhook" && actionItem.value[0] === webhookID){
                        existingServiceID = item.id;
                    }
                });
            }
            catch(ex){ console.log(ex);}
        });
        UA_SAAS_SERVICE_APP.EXISTING_WEBHOOK_ID = existingServiceID;
        UA_SAAS_SERVICE_APP.incomingCaptureStatusChanged();
        UA_SAAS_SERVICE_APP.serviceSID = existingServiceID;
        return existingServiceID !== null;
    },

    addNewTrigger: async function(webhookID){
        if(!webhookID){
            return false;
        }
            var webhookExists = await UA_SAAS_SERVICE_APP.isWebhookAlreadyExists();
            if(!webhookExists){
                var webhookData = {
                    "trigger":{
                      "title": 'ULGEBRA_API_INTEGRATION_WEBHOOK_TRIGGER_'+extensionName,
                      "actions": [
                        {
                          "field": "notification_webhook",
                          "value": [
                            webhookID,
                            "{\"content\": \"{{ticket.latest_public_comment.value}}\", \"ticket_id\": \"{{ticket.id}}\"}"
                          ]
                        }],
                        "conditions": {
                            "any": [
                              {
                                "field": "comment_is_public",
                                "operator": "is",
                                "value": "true"
                              }
                            ]
                        }
                    }
                };
                var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/triggers`, "POST", webhookData);
                if(UA_SAAS_SERVICE_APP.showIfErrorInAPIResult(response)){
                    return false;
                }
                UA_SAAS_SERVICE_APP.existingPreWebhookID = response.data.trigger.id;
                UA_SAAS_SERVICE_APP.EXISTING_WEBHOOK_ID = UA_SAAS_SERVICE_APP.existingPreWebhookID;
                UA_SAAS_SERVICE_APP.incomingCaptureStatusChanged();
                return response.data && (response.data.trigger.id !== null);
            }
            return true;
    },

    addNewWebhook: async function(){
        if(!UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS){
            UA_APP_UTILITY.CALL_AFTER_PERSONAL_COMMON_SETTING_RESOLVED.push(UA_SAAS_SERVICE_APP.addNewWebhook);
            return false;
        }
        if(UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.hasOwnProperty('ua_incoming_saas_ticket_reply_enable_capture_'+currentUser.uid) && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS['ua_incoming_saas_ticket_reply_enable_capture_'+currentUser.uid] === false){
            return;
        }
        if(UA_APP_UTILITY.PERSONAL_WEBHOOK_TOKEN && appsConfig.UA_DESK_ORG_ID){
            var webhookExists = await UA_SAAS_SERVICE_APP.isWebhookAlreadyExists();
            if(!webhookExists){
                var webhookData = {
                    "webhook":{
                      "endpoint": UA_APP_UTILITY.getAuthorizedSAASWebhookURL(),
                      "http_method":"POST",
                      "name": "Ulgebra Intergrations - "+appPrettyName+" Comment Listener",
                      "status":"active",
                      "request_format":"json",
                      "subscriptions":["conditional_ticket_events"]
                    }
                };
                var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/webhooks`, "POST", webhookData);
                if(UA_SAAS_SERVICE_APP.showIfErrorInAPIResult(response)){
                    return false;
                }
                UA_SAAS_SERVICE_APP.existingPreWebhookID = response.data.webhook.id;
                await UA_SAAS_SERVICE_APP.addNewTrigger(UA_SAAS_SERVICE_APP.existingPreWebhookID);
                return response.data && (response.data.webhook.id !== null);
            }
            return true;
        }
        return false;
    },
    incomingCaptureStatusChanged: function(){
        if(UA_SAAS_SERVICE_APP.EXISTING_WEBHOOK_ID){
            document.getElementById('saas-switch-incoming-comments-enable-capture').checked = true;
        }
        else{
            document.getElementById('saas-switch-incoming-comments-enable-capture').checked = false;
        }
    },

    showIfErrorInAPIResult: function(response){
        console.log(response);
        if((response.code === 401 || response.message === "Authentication failed") || (response.error && response.error.code === 401) || (response.data && (response.data.code === 401 || response.data.message === "Authentication failed"))){
            UA_OAUTH_PROCESSOR.showReAuthorizationError(UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED);
            return true;
        }
        return false;
    },
    deleteExistingWebhook: async function(){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/webhooks/${UA_SAAS_SERVICE_APP.EXISTING_WEBHOOK_ID}`, "DELETE", {});
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.getAPIBaseUrl()}/triggers/${UA_SAAS_SERVICE_APP.existingPreWebhookID}`, "DELETE", {});
        if (UA_SAAS_SERVICE_APP.showIfErrorInAPIResult(response)){
            return false;
        }
        UA_SAAS_SERVICE_APP.EXISTING_WEBHOOK_ID = null;
        UA_SAAS_SERVICE_APP.incomingCaptureStatusChanged();
        return true;
    }
};