const UA_OAUTH_PROCESSOR = {
    domainExtnMap: {
       "IN": ".in",
       "EU": ".eu",
       "CN": ".com.cn",
       "AU": ".com.au",
       "US": ".com"
   },
   currentSAASDomain: "",
   initiateAuth: function (scope) {
       var scopeText = typeof scope === "object" ? scope.join(' ') : scope;
//        $('.ua_service_login_btn').text('Processing...');
       if(!extensionName){
           showErroWindow('Application error!', 'Unable to load app type. Kindly <a target="_blank" href="https://apps.ulgebra.com/contact">contact developer</a>');
           return;
       }
       let fullDomainValue = $('#inp_saas_api_domain').val().trim();
       fullDomainValue = fullDomainValue.replace("https://",'').replace("/","");
       if(!fullDomainValue){
            showErroWindow('Microsoft Dynamics CRM Domain is Empty!', 'Please enter your Microsoft Dynamics domain');
            return false;
       }
       window.open(`https://login.microsoftonline.com/d38e9b96-dd66-4ca6-9f78-793510fa135a/oauth2/authorize?client_id=ee9967b7-9381-489a-9650-f7230aeb0389&response_type=code&redirect_uri=${APP_ENV_CLIENT_URL}/oauth-home&response_mode=query&scope=openid%20offline_access%20https%3A%2F%2Fadmin.services.crm.dynamics.com%2Fuser_impersonation%20https%3A%2F%2Fgraph.microsoft.com%2FUser.Read&state=${encodeURIComponent("https://"+fullDomainValue)}:::${extensionName}`, 'Authorize', 'popup');
   },
   
   getAPIResponse: async function (url, method, data) {
       var credAdProcess3 = curId++;
       showTopProgressBar(credAdProcess3);
       var returnResponse = await UA_APP_UTILITY.makeUAServiceAuthorizedHTTPCall(url, method, data, {"Authorization": "Bearer {{AUTHTOKEN}}", "Content-Type": "application/json"});
       removeTopProgressBar(credAdProcess3);
       return returnResponse;
   },
   
   showReAuthorizationError: function(scope = []){
       if($('#UA_SAAS_AUTH_BTN').length > 0){
           console.log('SaaS auth window already showing.. skipping.');
           return;
       }
      showErroWindow("Microsoft Dynamics CRM Authentication Needed!", `You need to authorize your Microsoft Dynamics CRM Account to proceed <br><br> 
        <div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
            <div style="font-size: 18px;margin-bottom: 5px;margin-top: 20px;">Microsoft Dynamics CRM Domain</div>
                <input id="inp_saas_api_domain" value="${UA_OAUTH_PROCESSOR.currentSAASDomain ? UA_OAUTH_PROCESSOR.currentSAASDomain : '' }" type="text" placeholder="eg: https://mydomain.crm.dynamics.com" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
                <br>              
            <button id="UA_SAAS_AUTH_BTN" class="ua_service_login_btn ua_primary_action_btn" onclick="UA_OAUTH_PROCESSOR.initiateAuth('${scope.join(' ')}')">Authorize Now</button>
        </div>`, false, false, 1000);
   }
   
};