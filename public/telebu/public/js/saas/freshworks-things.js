const UA_OAUTH_PROCESSOR = {
    
    currentSAASDomain: "",
    
    initiateAuth: function (scope) {
        UA_OAUTH_PROCESSOR.showReAuthorizationError(scope);
    },
    
    getAPIResponse: async function (url, method, data) {
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var returnResponse = await UA_APP_UTILITY.makeUAServiceAuthorizedHTTPCall(url, method, data, {"Authorization": "Basic {{AUTHTOKEN}}", "Content-Type": "application/json", "Accept": "application/json"});
        removeTopProgressBar(credAdProcess3);
        return returnResponse;
    },
    
    proceedToSaveSaaSAPIkey : async function(){
        let authtoken = $("#inp_saas_api_key").val();
        let api_domain = $("#inp_saas_api_domain").val().replace("https://","").replace("/","");
        if(!valueExists(authtoken)){
            showErroMessage("Please fill API Key");
            return;
        }
        if(extensionName.indexOf("lookupfor") > -1 && (!valueExists(api_domain) || api_domain == ".com")){
            showErroMessage("Please fill domain");
            return;
        }
        let sampleDomain = extensionName.endsWith('freshworkscrm') ? '.myfreshworks.com' : '.freshdesk.com';
        let isValidDomain = api_domain.endsWith(sampleDomain);
        if(!isValidDomain){
            showErroMessage("Please provide valid domain in this format - \"mycompany"+sampleDomain+'"');
            return;
        }
        UA_OAUTH_PROCESSOR.currentSAASDomain = api_domain;
        await UA_APP_UTILITY.saveAPIKeyInCredentials('saas', '__multiple_keys__', {
            'authtoken': authtoken,
            'api_domain': UA_OAUTH_PROCESSOR.currentSAASDomain
        }, UA_APP_UTILITY.reloadWindow);
    },
    
    showReAuthorizationError: function(serviceName = "Freshworks", getAPIKeyHelpTip){
        if($('#inp_saas_api_key').length > 0){
            console.log('SaaS auth window already showing.. skipping.');
            return;
        }
        let sampleDomain = extensionName.endsWith('freshworkscrm') ? '.myfreshworks.com' : '.freshdesk.com';
        showErroWindow("Authentication needed!", `You need to authorize your ${serviceName} Account to proceed. <br><br> <div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
            <div style="font-size: 18px;margin-bottom: 5px;margin-top: 20px;">${serviceName} Domain</div>
            <input id="inp_saas_api_domain" value="${UA_OAUTH_PROCESSOR.currentSAASDomain}" type="text" placeholder="e.g mycompany${sampleDomain}" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
            <div style="font-size: 18px;margin-bottom: 5px;margin-top: 20px;">${serviceName} API Key</div> ${getAPIKeyHelpTip ? '<div class="help_apikey_tip">'+getAPIKeyHelpTip+'</div>' : ''}
            <input id="inp_saas_api_key" type="text" placeholder="Enter your API Key" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
            <button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_OAUTH_PROCESSOR.proceedToSaveSaaSAPIkey()">Authorize Now</button>
        </div>`, true, false, 1000);
        $('#inp_saas_api_key').focus();
       
    }
    
};