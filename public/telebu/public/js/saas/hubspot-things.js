const UA_OAUTH_PROCESSOR = {
     domainExtnMap: {
        "IN": ".in",
        "EU": ".eu",
        "CN": ".com.cn",
        "AU": ".com.au",
        "US": ".com"
    },
    currentSAASDomain: ".com",
    initiateAuth: function (scope) {
        var scopeText = typeof scope === "object" ? scope.join(' ') : scope;
//        $('.ua_service_login_btn').text('Processing...');
        if(!extensionName){
            showErroWindow('Application error!', 'Unable to load app type. Kindly <a target="_blank" href="https://apps.ulgebra.com/contact">contact developer</a>');
            return;
        }
        window.open(` https://app.hubspot.com/oauth/authorize?client_id=${UA_TPA_FEATURES.clientIds[extensionName]}&redirect_uri=${APP_ENV_CLIENT_URL}/oauth-home?appCode=${extensionName}&scope=${encodeURIComponent(scopeText)}&state=${currentUser.uid}:::${extensionName}`, 'Authorize', 'popup');
    },
    
    getAPIResponse: async function (url, method, data) {
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var returnResponse = await UA_APP_UTILITY.makeUAServiceAuthorizedHTTPCall(url, method, data, {"Authorization": "Bearer {{AUTHTOKEN}}", "Content-Type": "application/json"});
        removeTopProgressBar(credAdProcess3);
        return returnResponse;
    },
    
    showReAuthorizationError: function(scope){
        if($('#UA_SAAS_AUTH_BTN').length > 0){
            console.log('SaaS auth window already showing.. skipping.');
            return;
        }
       showErroWindow("HubSpot Authentication Needed!", `You need to authorize your HubSpot Account to proceed <br><br> <button id="UA_SAAS_AUTH_BTN" class="ua_service_login_btn ua_primary_action_btn" onclick="UA_OAUTH_PROCESSOR.initiateAuth('${scope.join(' ')}')">Authorize Now</button>`, false, false, 1000);
    }
    
};



/*  

   function addwebhook(){
     var body={
   "actionUrl": "https://us-central1-ulgebra-license.cloudfunctions.net/sms_workflowWebhookHandler?extensionName=twilioforhubspotcrm",
   "published": true,
   "inputFields": [
     {
       "typeDefinition": {
         "name": "workflow_code",
         "type": "string",
         "fieldType": "textarea"
       },
       "supportedValueTypes": [
         "STATIC_VALUE"
       ],
       "isRequired": true
     }
   ],
   "labels": {
     "en": {
       "inputFieldLabels": {
         "workflow_code": "Workflow code"
       },
       "inputFieldDescriptions": {
         "workflow_code": "Replace workflow code from pinnacle app panel"
       },
       "actionName": "Ulgebra Send Twilio WhatsApp/SMS",
       "actionDescription": "Send WhatsApp/SMS via Twilio Ulgebra",
       "appDisplayName": "Ulgebra Twilio",
       "actionCardContent": "Ulgebra Send Twilio WhatsApp/SMS"
     }
   },
   "objectTypes": [
     "CONTACT","COMPANY","DEAL", "TICKET"
   ]
 };

fetch("https://api.hubspot.com/automation/v4/actions/886027?hapikey=", {'headers': {'Content-Type': 'application/json'}, "method": "DELETE", "body": JSON.stringify(body), json: true})



       
   }
 
 */