var UA_SAAS_SERVICE_APP = {
    API_BASE_URL : "",
    CURRENT_USER_INFO : null,
    CURRENT_ORG_INFO : null,
    CURRENT_OAUTH_SCOPE_NEEDED : [],
    FETCHED_RECORD_DETAILS: {},
    CURRENT_MODULE_FIELDS_DROPDOWN : [],
    SELECTED_RECEIPS: {},
    SEARCH_MODULE_RECORD_MAP: {},
    widgetContext: {},
    appsConfig: {},
    APP_API_NAME: "pinnaclesms__",
    isModelOpened: false,
    APP_CLIENT: null,
    fetchedList : {},
    supportedIncomingModules: [
        {
            "selected": true,
            "value": "contact",
            "label": "Sync Events to latest conatct or create new conatct"
        },
        {
            "value": "lead",
            "label": "Sync Events to latest lead or create new lead"
        },
        {
            "value": "account",
            "label": "Sync Events to latest account or create new account"
        }
    ],
    
    getAPIBaseUrl : function(){
        return ``;
    },

    provideSuggestedCurrentEnvLoginEmailID: async function(){
       
    },
    
    initiateAPPFlow: async function(){
        
        UA_SAAS_SERVICE_APP.initiateAPPFlow_SA(true);
        
    },
    
    initiateAPPFlow_SA: async function(isSMS = false){
        $("#ac_name_label_saas .anl_servicename").text('SalesForce CRM');
        if(eventApps.includes(extensionName.split("for")[0]) == true){
            UA_SAAS_SERVICE_APP.supportedIncomingModules.push({
                "value": "activity_only",
                "label": "Only Event"
            });
        }
        UA_SAAS_SERVICE_APP.widgetContext.module = queryParams.get("module");
        UA_SAAS_SERVICE_APP.widgetContext.entityId = queryParams.get("entityId");
        UA_SAAS_SERVICE_APP.injectShowSearchContact(); 

        UA_SAAS_SERVICE_APP.getCurrentUserInfo(function(response){
            UA_OAUTH_PROCESSOR.currentSAASDomain = response.api_domain;
            UA_SAAS_SERVICE_APP.CURRENT_USER_INFO = response.data;
            appsConfig.APP_UNIQUE_ID = UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.user_id;
            appsConfig.UA_DESK_ORG_ID = UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.organization_id;
            $("#saasAuthIDName").text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.name);//.attr('title', `Authorized Zoho Account: (${UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.contact.email})`);
            setTimeout((function(){
                $('#ac_name_label_saas .ac_name_id').text(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.email);
            }), 2000);
            UA_SAAS_SERVICE_APP.proceedToAppInitializationIfAPPConfigResolved();
        });
    },
    
    addSAASUsersToLICUtility: async function(){
        $('.saasServiceName').text("SalesForce CRM");
        var response = await UA_SAAS_SERVICE_APP.getQueryAPIResponseAndCallback(`select name,email,profile.name,smallphotourl,id,phone from user`);
        
        if(!response || !response.data || !response.data.records){
            return;
        }
//        response.data.records.forEach(item=>{
//            if(item.name === "Account Admin"){
//                admin_role_id = item.id;
//            }
//        });
        response.data.records.sort(function(a, b) {
            return a.Name.localeCompare(b.Name);
        });
        response.data.records.forEach(item=>{
            item = UA_SAAS_SERVICE_APP.getSmallCaseConvertedObject(item);
            UA_LIC_UTILITY.addFetchedSAASUser(item.id, item.name, item.email, item.smallphotourl, item.profile && item.profile.name === "System Administrator", item.phone, null);
        });
        UA_LIC_UTILITY.saasUserListFetchCompleted();
    },
    
    getSmallCaseConvertedObject: function(obj){
        if(obj){
            Object.keys(obj).forEach(key=>{
                let originalKey = key;
               if(typeof obj[originalKey] === "object"){
                   obj[key.toLowerCase()] = UA_SAAS_SERVICE_APP.getSmallCaseConvertedObject(obj[originalKey]);
               }
               else{
                   obj[key.toLowerCase()] = obj[originalKey]; 
               }
               delete obj[originalKey];
            });
        }
        return obj;
    },
    
    proceedToAppInitializationIfAPPConfigResolved: function(){
        if(appsConfig.APP_UNIQUE_ID && appsConfig.UA_DESK_ORG_ID){
            UA_SAAS_SERVICE_APP.renderInitialElements();
            UA_SAAS_SERVICE_APP.addSAASUsersToLICUtility();
            UA_APP_UTILITY.appsConfigHasBeenResolved();
            if(UA_SAAS_SERVICE_APP.widgetContext.module)
            {
                UA_SAAS_SERVICE_APP.renderInitialElementsForSalesporcecrmList();
            }
        }
    },
    
    renderInitialElements: async function(){
        UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown('users', ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder']);
//        UA_APP_UTILITY.renderSavedTemplatesInDropdowns();
        if(!UA_SAAS_SERVICE_APP.widgetContext.module){
            UA_SAAS_SERVICE_APP.entityDetailFetched(null);
            console.log('Module does not exist, rendering skipped');
            $("#recip-count-holder").text("Add recipients to proceed");
            return;
        }
        
        
        /*
        var contactData = await UA_SAAS_SERVICE_APP.APP_CLIENT.data.get("contact");
        contactData = contactData.contact;
        
        
        UA_SAAS_SERVICE_APP.addRecordsAsRecipients([
            contactData
        ]); */
        
        UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(UA_SAAS_SERVICE_APP.widgetContext.module, UA_SAAS_SERVICE_APP.widgetContext.entityId, function(resp){
            UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], console.log);
            UA_SAAS_SERVICE_APP.addRecordsAsRecipients(resp);
        });
    },

    renderInitialElementsForSalesporcecrmList: async function(){
        $(".ssf-new-recip-form").prepend(`<div id="ssf-recip-list-add-holdr" class="ssf-recip-choice-btn" style="margin-top: -5px;background: none;border: none;box-shadow: none;"></div>`);
        UA_SAAS_SERVICE_APP.renderContactList();
    },
    
    renderContactList: async function(){
        try{
            let url = `/services/data/v55.0/sobjects/${UA_SAAS_SERVICE_APP.widgetContext.module}/listviews`;
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(url, "GET", null);
            // UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
            console.log('renderList',response);
            let listDropdownItems = [];
            response.data["listviews"].forEach(item=>{
                UA_SAAS_SERVICE_APP.fetchedList[item.id] = item;
                listDropdownItems.push({
                    'label': item.label,
                    'value': item.id
                });
            });
            
            UA_APP_UTILITY.renderSelectableDropdown(`#ssf-recip-list-add-holdr`, 'Select a List', listDropdownItems, 'UA_SAAS_SERVICE_APP.insertManageListContacts', false, false);
        }
        catch(ex){
            console.log(ex);
        }
    
    },

    insertManageListContacts: async function(listId){
        let url = `/services/data/v55.0/sobjects/${UA_SAAS_SERVICE_APP.widgetContext.module}/listviews/${listId}/results`;
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(url, "GET", null);
        // UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('renderList',response);
        if(response.data && response.data[`records`] && response.data[`records`].length > 0) {
            response.data[`records`].forEach(item=>{
                let columns = item.columns; 
                let result = columns.find(({ fieldNameOrPath }) => fieldNameOrPath === 'Id');
                UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(UA_SAAS_SERVICE_APP.widgetContext.module,result.value, async function(contactList){
                    UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(UA_SAAS_SERVICE_APP.widgetContext.module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);
                    UA_SAAS_SERVICE_APP.addRecordsAsRecipients(contactList);
                });
            });
        }
            
    },
    
    sendPhoneFieldsForRecipientType: function(fieldsArray){
        fieldsArray.forEach(item=>{
            if(["Phone", "MobilePhone", "HomePhone", "OtherPhone", "AssistantPhone"].includes(item.display_label)){
                UA_APP_UTILITY.addRecipientPhoneFieldType(item.api_name, item.display_label);
            }
        });
    },
    
    populateModuleItemFieldsInDropDown: async function (module, target, fieldsCallback=(()=>{})){
        /*var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${UA_SAAS_SERVICE_APP.API_BASE_URL}/settings/fields?module=${module}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log(response);
        response.data.fields.forEach(item=>{
            dropDownValues.push({
                "label": item.display_label,
                "value": (module.toUpperCase() === "USERS" ? "CURRENT_USER": module.toUpperCase())+'.'+item.api_name
            });
        }); */
        if(!module || UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.includes(module) == true){
            return;
        }
        UA_SAAS_SERVICE_APP.CURRENT_MODULE_FIELDS_DROPDOWN.push(module);
        var moduleToLoad = module === "users" ? UA_SAAS_SERVICE_APP.CURRENT_USER_INFO : UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[UA_SAAS_SERVICE_APP.widgetContext.entityId?UA_SAAS_SERVICE_APP.widgetContext.entityId:Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS)[0]];
        var moduleFields = Object.keys(moduleToLoad);
        var fieldsArray = [];
        var dropDownValues = [];
        moduleFields.forEach(item=>{
            let itemVal = moduleToLoad[item];
            if(typeof itemVal !== "object" && itemVal !== null && typeof itemVal!=="boolean"){
                fieldsArray.push({
                    "api_name": item,
                    "display_label": item
                });
                dropDownValues.push({
                    "label": item,
                    "value": (module.toUpperCase() === "USERS" ? "CURRENT_USER": module.toUpperCase())+'.'+item
                });
            }
        });
        fieldsCallback(fieldsArray);
        if(typeof target === "string"){
            target = [target];
        }
        target.forEach(item=>{
            UA_APP_UTILITY.renderSelectableDropdown(item, `${module} fields`, dropDownValues, 'UA_APP_UTILITY.templateMergeFieldSelected', false, true);
        });
        return true;
    },
    
    injectShowSearchContact :async function(){
        $(".ssf-new-recip-form").prepend(`<div id="searchAndAddFromContactBtn" class="ssf-recip-choice-btn" onclick="$('.item-list-popup').show()">
                                            <i class="material-icons">search</i> Search from ${UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()}
                                      </div>`);
        $('.pageContentHolder').append(`<div class="item-list-popup" style="display:none">
                        <div class="item-list-popup-title">
                            Search ${(UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()).capitalize()} <div class="pop-win-close" onclick="$('.item-list-popup').hide();">x</div>
                        </div>
                        <div class="item-search-box">
                            <input class="input-form" id="contact-seach-name" type="text" autocomplete="off" placeholder="Name of the ${UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase():"contact"}"/><br>
                            <div class="btn-save" onclick="UA_SAAS_SERVICE_APP.searchRecordsAndRender($('#contact-seach-name').val(),UA_SAAS_SERVICE_APP.widgetContext.module?UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase():'contact')">
                                Search ${(UA_SAAS_SERVICE_APP.widgetContext.module.toLocaleLowerCase()).capitalize()}
                            </div>
                            <div class="btn-reset" onclick="UA_SAAS_SERVICE_APP.resetsearchContacts()">
                                Reset
                            </div>
                        </div>
                        <div class="item-list-popup-content" id="contact-search-items">

                        </div>
                    </div>`);
    },

    resetsearchContacts: function(){
        $("#contact-seach-name").val("");
        $("#contact-search-items").html("");
    },

    searchRecordsAndRender: async function(searchTerm,module){
        if(!valueExists(searchTerm)){
            return;
        }
        UA_SAAS_SERVICE_APP.widgetContext.module = UA_SAAS_SERVICE_APP.widgetContext.module;
            await UA_SAAS_SERVICE_APP.getAPIResponseAndCallback(`/services/data/v55.0/search/suggestions?q=${searchTerm}&sobject=${module}` , function(response){
                console.log(response);
                $("#contact-search-items").html(" ");
                if(response && response.data && response.data.autoSuggestResults && response.data.autoSuggestResults.length>0)
                {
                    var search_items = response.data.autoSuggestResults;
                    if(search_items && search_items.length > 50){
                        search_items.splice(0,49);
                    }
                    UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module] = {};
                    search_items.forEach((obj)=>{
                        UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][obj.Id] = obj;
                        let fullName = obj.Name ? obj.Name:module+"#"+obj.Id;
                        if(module)
                        {
                            let itemHTML = `<div class="item-list-popup-item"><span class="c-name" style="cursor:pointer;" onclick="UA_SAAS_SERVICE_APP.selectPhoneNumber('${obj.Id}', '${module.trim()}', '${fullName}','${module}','${true}')"> ${fullName} </span>`;
                            // itemHTML =  itemHTML + `${'<span class="c-phone '+(UA_SAAS_SERVICE_APP.SELECTED_RECEIPS["customers_"+obj.id] ? 'alreadyadded': '')+'" onclick="UA_SAAS_SERVICE_APP.selectPhoneNumber(\''+obj.id+'\', \''+"customers"+'\', \''+fullName+'\',\''+module+'\',\''+true+'\')"> <i class="material-icons">phone_iphone</i>( Customer ) </span>'}`;
                            itemHTML =  itemHTML + `</div>`;
                            $("#contact-search-items").append(itemHTML);
                        }    
                        
                    });
                }
                else
                {
                    $("#contact-search-items").html("No records found. <br><br> <span class=\"c-silver\"> This search will include <br> only customers having phone numbers</span>");
                    return;
                }
            }).catch(err=>{
                console.log(err);
            });
    },

    selectPhoneNumber: async function(id,number,name,module,isSelected){
        var receipNumberID = module + "_" + id;
        if(!UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] && !UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[id]){
            // if(true){
            UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID] = {
                'id': id,
                'name': name,
                'module': module,
                'isSelected': isSelected
            };
            $.extend(UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id],UA_SAAS_SERVICE_APP.SELECTED_RECEIPS[receipNumberID]) ;
            // UA_SAAS_SERVICE_APP.renderInitialElements(module,id);

            await UA_SAAS_SERVICE_APP.fetchStoreExecuteOnRecordDetails(module, id, UA_SAAS_SERVICE_APP.addRecordsAsRecipients);
            await UA_SAAS_SERVICE_APP.populateModuleItemFieldsInDropDown(module, ['#ssf-fitem-template-var-holder', '#ssf-fitem-new-template-var-holder'], UA_SAAS_SERVICE_APP.sendPhoneFieldsForRecipientType);

            // UA_SAAS_SERVICE_APP.addRecordsAsRecipients([UA_SAAS_SERVICE_APP.SEARCH_MODULE_RECORD_MAP[module][id]]);
        }
        else {
            console.log(module+' already added');
            alert(`This ${module} is already added.`);
            return;
        }
    },
    
    getCurrentUserInfo : async function(callback){
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('https://login.salesforce.com/services/oauth2/userinfo', callback);
    },
    
    getCurrentOrgInfo : async function(callback){
        UA_SAAS_SERVICE_APP.getAPIResponseAndCallback('/crm/sales/api/account', callback);
    },
    
    getQueryAPIResponseAndCallback: async function(query, callback){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/services/data/v55.0/query/?q=`+encodeURIComponent(query), "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        if(!callback){
            return response;
        }
        callback(response);
    },
    
    getAPIResponseAndCallback: async function(url, callback){
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`${url}`, "GET", null);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        callback(response);
    },
    
    showErrorIfInAPICall : async function(response){
        if(response.code === 401 || response.code === 403){
            UA_OAUTH_PROCESSOR.showReAuthorizationError();
        }
    },
    
    initiateAuthFlow: function(){
        UA_OAUTH_PROCESSOR.initiateAuth();
    },
    
    fetchStoreExecuteOnRecordDetails: async function(module, recordIds, callback){
        $("#recip-count-holder").text(`fetching ${module.toLocaleLowerCase()} information...`);
        var queryParams = '';
        
        if(!valueExists(recordIds)){
            return;
        }
        
        let relatedModuleQuery = '';
        if(module === "opportunity"){
            relatedModuleQuery = `,(SELECT contact.phone,contact.name,contact.otherphone,contact.mobilephone,contact.assistantphone,contact.homephone,contact.email,contact.id FROM OpportunityContactRoles)`;
        }
        let filterQuery = '';
        if(recordIds.split(',').length > 1){
            let filterElements = '';
            recordIds.split(',').forEach(item=>{
                if(filterElements!==''){
                    filterElements+=',';
                }
                filterElements+= `'${item}'`;
            });
            filterQuery = ` id in (${filterElements})`;
        }
        else{
            filterQuery = ` id='${recordIds}'`;
        }
        
        var response = await UA_SAAS_SERVICE_APP.getQueryAPIResponseAndCallback(`select fields(all)${relatedModuleQuery} from ${module.endsWith('s') ? module.substr(0, module.length-1) : module} where ${filterQuery}`);
        UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        console.log('fetchStoreExecuteOnRecordDetails',response);
        response.data.records.forEach(recordItem=>{
            if(UA_SAAS_SERVICE_APP.widgetContext.module === "opportunity"){
                if(recordItem.OpportunityContactRoles){
                   let i = 1;
                   recordItem.OpportunityContactRoles.records.forEach(contactRelatedItem=>{
                        Object.keys(contactRelatedItem.Contact).forEach(keyName=>{
                            let propertyName = 'contact_'+i+'_'+keyName;
                            recordItem[propertyName] = contactRelatedItem[keyName];
                        });
                    });
                } 
            }
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordItem.Id] = recordItem;
            if(response.data.records.length === 1){
                UA_SAAS_SERVICE_APP.widgetContext.entity = recordItem;
            };
        });
        $("#recip-count-holder").text(`Selected ${recordIds.length} ${module.toLocaleLowerCase()}`);
        callback(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
        UA_SAAS_SERVICE_APP.entityDetailFetched();
    },
    
    addRecordsAsRecipients: function(contactsList){
        let phoneFields = ["Phone", "MobilePhone", "HomePhone", "OtherPhone", "AssistantPhone"];
        if(UA_SAAS_SERVICE_APP.widgetContext.module === "opportunity"){
            for (var item in contactsList) {
                let contactItem = contactsList[item];
                if(contactItem.OpportunityContactRoles){
                   let i = 1;
                   contactItem.OpportunityContactRoles.records.forEach(contactRelatedItem=>{
                         ["Phone", "MobilePhone", "HomePhone", "OtherPhone", "AssistantPhone"].forEach(mobileFieldItem => {
                            if (contactRelatedItem.Contact[mobileFieldItem]) {
                                let propertyName = 'contact_'+i+'_'+mobileFieldItem;
                                contactsList[item][propertyName] = contactRelatedItem.Contact[mobileFieldItem];
                                UA_APP_UTILITY.addRecipientPhoneFieldType(propertyName, propertyName);
                                if(!phoneFields.includes(propertyName)){
                                    phoneFields.push(propertyName);
                                }
                            }
                        });
                       i++;
                   });
               } 
            }
        }
        for (var item in contactsList) {
            if($(`[data-recip-id=${contactsList[item]['id']}]`).length === 0) {
                contactsList[item].id = contactsList[item].Id;
                contactsList[item].name = contactsList[item].Name;
                phoneFields.forEach(mobileFieldItem => {
                    if (contactsList[item][mobileFieldItem]) {
                        UA_APP_UTILITY.addRecipientPhoneFieldType(mobileFieldItem, mobileFieldItem);
                    }
                });
                UA_APP_UTILITY.addInStoredRecipientInventory(contactsList[item]);
            }
        }
    },
    
    addSentSMSAsRecordInHistory: async function(sentMessages){
        if(!UA_SAAS_SERVICE_APP.widgetContext.module){
            return;
        }
//        var historyDataArray = [];

        var credAdProcess3 = curId++;
        showProcess(`Adding ${Object.keys(sentMessages).length} messages to comments...`, credAdProcess3);

        
        for(var sentMessageId in sentMessages){
            let commentContent = appPrettyName+` Extension: `;
            let sentMessage = sentMessages[sentMessageId];
            let recModule = sentMessage.module ? sentMessage.module:UA_SAAS_SERVICE_APP.widgetContext.module; 
            let recId = sentMessage.moduleId?(sentMessage.moduleId.indexOf("newmanual")!==0?sentMessage.moduleId:UA_SAAS_SERVICE_APP.widgetContext.entityId):UA_SAAS_SERVICE_APP.widgetContext.entityId;
            
            commentContent+=`${sentMessage.channel} ${sentMessage.status} from ${sentMessage.from} to ${sentMessage.to}:
                             ${sentMessage.message}`;
                let response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/services/data/v55.0/sobjects/Task`, "POST", {
                            "Subject": `${sentMessage.channel} ${sentMessage.status} Via ${appPrettyName}`,
                            "Description": commentContent,
                            "WhoId": ['account', 'opportunity'].includes(recModule) ? null : recId,
                            "WhatId": ['account', 'opportunity'].includes(recModule) ? recId : null,
                            "Status": "Completed"
                    });
                console.log(response);
                
            UA_SAAS_SERVICE_APP.showErrorIfInAPICall(response);
        }
        
        processCompleted(credAdProcess3);
        
    },
    entityDetailFetched: function(data){
        setTimeout(()=>{UA_APP_UTILITY.fetchedAllModuleRecords();}, 2000);
        if(typeof UA_TPA_FEATURES.callOnEntityDetailFetched === "function"){
            UA_TPA_FEATURES.callOnEntityDetailFetched(data);
        }
    },

// LookUp functions
    LOOKUP_SUPPORTED_MODULES: {
        "contact": {
            "displayLabel": "Contact",
            "apiName": "contact",
            "autoLookup": true,
        },
        "lead": {
            "displayLabel": "Leads",
            "apiName": "lead",
            "autoLookup": true,
        },
        "opportunity": {
            "displayLabel": "Opportunity",
            "apiName": "opportunity",
            "autoLookup": true,
        },
        "account": {
            "displayLabel": "Accounts",
            "apiName": "account",
            "autoLookup": true,
        }
    },
    LOOKUP_GLOBAL_SEARCH_COMPLETED: true,
    allModuleFieldsFetched: function(){
        let fieldsPresentModuleCount = 0;
        Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).forEach(item=>{
            if(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[item].fields && Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[item].fields).length > 0){
                fieldsPresentModuleCount++;
            }
        });
        return Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).length === fieldsPresentModuleCount;
    },

    retrieveModuleFieldsForLookUp: function(callback){
        $('#ua-loo-search-field-ddholder').hide();
        if (UA_SAAS_SERVICE_APP.allModuleFieldsFetched()) {
            callback();
        }
        var primaryFieldsToCreate = [];
        let primaryFieldsToViewForModule = ["id"];

        Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).forEach(async (moduleId)=>{
            var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/services/data/v55.0/sobjects/${moduleId}/describe`, "GET", null);
            let fieldAPINameMap = {};
            if(!response.data || !response.data.fields || response.data.fields.length < 1){
                return;
            }
            var fields = response.data.fields;
            for(var i=0; i < fields.length; i++){
                let fieldItem = fields[i];
               // if(fieldItem.type != "group_field" && fieldItem.type != "auto_complete") {
//                    if(["lookup"].includes(fieldItem.fieldType)){
//                        fieldItem.field_read_only = true;
//                    }

                    fieldItem['display_label'] = fieldItem.label;
                    fieldItem['api_name'] = fieldItem.name;
                    fieldItem['data_type'] = fieldItem.type;
                    fieldItem['field_read_only'] = ["reference"].includes(fieldItem.type) || fieldItem.calculated || !fieldItem.updateable;
                    fieldItem["pick_list_values"] = [];
                    if(fieldItem['type'] === "date"){
                        fieldItem['data_type'] = "datetime";
                    }
                    fieldItem.picklistValues.forEach(pickListItem=>{
                        fieldItem["pick_list_values"].push({
                            "actual_value": pickListItem.value,
                            "display_value": pickListItem.label,
                            "default_value": pickListItem.defaultValue
                        });
                    });
                    fieldAPINameMap[fieldItem.name] = fieldItem;
               // }
            }
            fieldAPINameMap.id = {
                'display_label': 'Id',
                'data_type': 'number',
                'api_name': 'Id',
                'field_read_only': true
            };            
            
            switch(moduleId){
                case("lead"): {
                    primaryFieldsToCreate = ["Name", "Company", "Status"];
                    primaryFieldsToViewForModule = ["Name","Status","Email","Phone", "Company"];
                    break;
                }
                case("account"):{
                    primaryFieldsToCreate = ["Name"];
                    primaryFieldsToViewForModule = ["Name", "Email","Phone"];
                    break;
                }
                case("contact"):{
                    primaryFieldsToCreate = ["Name", "Phone", "Email"];
                    primaryFieldsToViewForModule = ["Name", "Phone", "Email"];
                    break;
                }
                case("opportunity"):{
                    primaryFieldsToCreate = ["Name", "StageName","CloseDate"];
                    primaryFieldsToViewForModule = ["Name", "StageName","CloseDate"];
                    break;
                }
            }

            UA_TPA_FEATURES.setCreateModuleRecordFields(moduleId, primaryFieldsToCreate);
            UA_TPA_FEATURES.setViewModuleRecordFields(moduleId, primaryFieldsToViewForModule);
            UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[moduleId].fields = fieldAPINameMap;
            if(UA_SAAS_SERVICE_APP.allModuleFieldsFetched()){
                callback();
            }
        });
    },
    lastLookUpSearchValue: null,
    getSearchedResultItems: async function(searchTerm, module, field){
        if(UA_SAAS_SERVICE_APP.lastLookUpSearchValue && UA_SAAS_SERVICE_APP.lastLookUpSearchValue === searchTerm && Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS) > 0){
            return;
        }
        UA_SAAS_SERVICE_APP.lastLookUpSearchValue = searchTerm;
        UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS = {};
        field = "Phone";
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/services/data/v55.0/search/?q=${encodeURIComponent("FIND {"+searchTerm+"} IN "+field+" FIELDS returning contact(fields(all) limit 10),account(fields(all) limit 10),lead(fields(all) limit 10),opportunity(fields(all) limit 10)")}`);
        $('.lhni-count').text('0');
        UA_OAUTH_PROCESSOR.currentSAASDomain = response.api_domain;
        if(response && response.data && response.data.searchRecords){
            response.data.searchRecords.forEach(item=>{
                item.name = item.Name;
                item.id = item.Id;
                item.__RecordModuleID = item.attributes.type.toLowerCase();
                item.webURL = `https://${UA_OAUTH_PROCESSOR.currentSAASDomain}/lightning/r/${item.__RecordModuleID}/${item.id}/view`;
                UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id] = item;
            });
        }
        return response.data;
    },
    
    getNotesOfRecord: async function(module, entityId){
        let queryWhereCond = "";
        if(['account', 'opportunity'].includes(module)){
            queryWhereCond = ` WHERE WhatId='${entityId}' `;
        }else{
            queryWhereCond = ` WHERE WhoId='${entityId}' `;
        }
        var response = await UA_SAAS_SERVICE_APP.getQueryAPIResponseAndCallback(`SELECT subject,description,id,lastmodifieddate FROM Task ${queryWhereCond} ORDER BY LastModifiedDate DESC limit 25`);
            let notesArray = [];
            if(response && response.data && response.data.records && response.data.records.length > 0){
                for(var i=0;i < response.data.records.length; i++){
                    let noteItem = response.data.records[i];
                    notesArray.push({
                        'id': noteItem.Id,
                        'content': noteItem.Description,
                        'title': noteItem.Subject,
                        'time': noteItem.LastModifiedDate
                    });
                }
            }
        return notesArray;
    },
    
    addModuleRecordNote: async function(module, entityId, noteContent){
        var noteResponse = await UA_OAUTH_PROCESSOR.getAPIResponse(`/services/data/v55.0/sobjects/Task`, "POST", {
                            "Subject": `Note - ${noteContent}`,
                            "Description": noteContent,
                            "WhoId": ['account', 'opportunity'].includes(module) ? null : entityId,
                            "WhatId": ['account', 'opportunity'].includes(module) ? entityId : null,
                            "Status": "Completed"
                    });
        if(noteResponse.data && noteResponse.data.id){
            return noteResponse.data;
        }
        else{
            return {
                error: true
            };
        }
        return false;
    },
    
    updateRecordByField: async function(module, entityId, fieldName, fieldValue){
        let updateObjPayload = {};
        updateObjPayload[`${UA_SAAS_SERVICE_APP.moduleIDs[module]}`] = {};
        if(fieldName.startsWith("cf")){
            updateObjPayload[`${UA_SAAS_SERVICE_APP.moduleIDs[module]}`]["custom_field"] = {};
            updateObjPayload[`${UA_SAAS_SERVICE_APP.moduleIDs[module]}`]["custom_field"][fieldName] = fieldValue;
        }
        else{
            updateObjPayload[`${UA_SAAS_SERVICE_APP.moduleIDs[module]}`][fieldName] = fieldValue;
        }
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/crm/sales/api/${UA_SAAS_SERVICE_APP.moduleIDs[module]}s/${entityId}`, "PUT", updateObjPayload);
        if(response.data && response.status == 200){
            return response;
        }
        else if(response.error && response.error.message){
            showErroWindow('Unable to create '+module, response.error.message);
            return {
                error: {
                    'message': response.error.message
                }
            }
        }
        else{
            return {
                error: {
                    'message': 'Error, Try again'
                }
            }
        }
    },
    
    createRecordInModule: async function(module, fieldMap){
        if(!fieldMap){
            return;
        }
        let updateObjPayload = fieldMap;
        var response = await UA_OAUTH_PROCESSOR.getAPIResponse(`/services/data/v39.0/sobjects/${module}`, "POST", updateObjPayload);
        if(response.data && response.data[`${UA_SAAS_SERVICE_APP.moduleIDs[module]}`]){
            let resultItemId = response.data[`${UA_SAAS_SERVICE_APP.moduleIDs[module]}`].id;
            let recordItem = response.data[`${UA_SAAS_SERVICE_APP.moduleIDs[module]}`];

            recordItem.name = module=="Contacts"?recordItem.display_name:recordItem.name;
            recordItem.webURL = `https://${UA_SAAS_SERVICE_APP.API_BASE_URL}/crm/sales/${module.toLocaleLowerCase()}/${recordItem.id}`;
            UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[resultItemId] = recordItem;
            return recordItem;
        }
        else if(response.error){
            var errMsg = "Error, Try again";
            if(response.error && response.error.message){
                errMsg = response.error.message;
            }
            else if(response.error && response.error.errors){
                errMsg = response.error.errors.message[0];
            }
            showErroWindow('Unable to create '+module, errMsg);
            return {
                error: {
                    'message': errMsg
                }
            };
        }
        else{
            return {
                error: {
                    'message': 'Error, Try again'
                }
            };
        }
    },
    
    getNewRecordInSAASWebURL: function(module){
        return `https://${UA_OAUTH_PROCESSOR.currentSAASDomain}/lightning/o/${module.capitalize()}/new`;
    }
};
