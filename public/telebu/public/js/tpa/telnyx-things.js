var UA_TPA_FEATURES = {
    workflowCode: {
        "SMS": {
            "from": "FILL_HERE",
            "to": "FILL_HERE",
            "text": "FILL_HERE",
            "module": "FILL_HERE",
            "recordId": "FILL_HERE",
            "channel": "SMS",
            "ulgebra_webhook_authtoken": null
        }
    },
    chosenMessgeTemplate: null,
    'clientIds': {
        'telnyxforpipedrive': ""
    },
    APP_SAVED_CONFIG: null,
    getSupportedChannels: function () {
        return [UA_APP_UTILITY.MESSAGING_CHANNELS.SMS, UA_APP_UTILITY.MESSAGING_CHANNELS.WHATSAPP];
    },
    tpaSortName: 'Telnyx',
    tpaApiName: 'telnyx',
    tpaSendersArray: [],
    renderInitialElements: async function () {
        
        $(".incoming-config-tpa-entity").text('messages');
        
        $("#ac_name_label_tpa .anl_servicename").text(UA_TPA_FEATURES.tpaSortName);
        UA_TPA_FEATURES.getCurrentAccountOrBalanceInfo(async function (bal_resp) {
            
            $('#ac_name_label_tpa .ac_name_id').html(`Balance : <b>${bal_resp.available_credit}</b>`);
            
            var sendersArray = [];
            
            var fetchedSendersListResponse = await UA_TPA_FEATURES.getSenders();
            if(fetchedSendersListResponse) {
                fetchedSendersListResponse.forEach(item=>{
                    if(item) {
                        UA_TPA_FEATURES.tpaSendersArray.push(item);
                        UA_APP_UTILITY.addMessaegSender(item.phone_number, UA_APP_UTILITY.MESSAGING_CHANNELS.SMS, item.phone_number.replace(/\D/g,''));
                        sendersArray.push({
                            'label': item.phone_number.replace(/\D/g,''),
                            'value': item.phone_number
                        });
                    }
                });
                UA_APP_UTILITY.TPA_SENDERS_FETCH_COMPLETED();
            }
        
            UA_APP_UTILITY.getPersonalWebhookAuthtoken();

            var countryCallCodeArray = [];
            UA_APP_UTILITY.countryCallingCodeArray.forEach(item => {
                countryCallCodeArray.push({
                    'label': `${item.name} (${item.dial_code})`,
                    'value': item.dial_code
                });
            });

            UA_APP_UTILITY.renderSelectableDropdown('#ssf-new-recip-countrycode', 'Select country', countryCallCodeArray, 'UA_APP_UTILITY.log');
        
        });

    },

    renderWorkflowBodyCode: function (accessToken) {

        UA_TPA_FEATURES.workflowCode.SMS.ulgebra_webhook_authtoken = accessToken.saas;
        UA_APP_UTILITY.addWorkflowBodyCode('sms', 'For Sending SMS', UA_TPA_FEATURES.workflowCode.SMS);
        
        //UA_TPA_FEATURES.actualPersonalCommonSettingResolved();
        
    },

    prepareAndSendSMS: function () {
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        if (!UA_APP_UTILITY.currentSender) {
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var senderId = UA_APP_UTILITY.currentSender.value;
        if (!valueExists(senderId)) {
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        //senderId = senderId === "default" ? "" : senderId
        var recipNumbers = UA_APP_UTILITY.getCurrentMessageRecipients();
        
        if (recipNumbers.length === 0) {
            showErroWindow('Recipient list is empty!', "Kindly add recipients to send SMS.");
            return false;
        }
        
        var messageText = $("#inp-ssf-main-message").val().trim();
        
        if (!valueExists(messageText)) {
            showErroWindow('Message is empty!', "Kindly fill message to proceed.");
            return false;
        }
        
        $('#sms-prog-window').show();
        $('#msgItemsProgHolder').html('');
        $('#totMsgDisp').text(`Sending messages 0/${recipNumbers.length}...`);

        var messageNumberMapArray = [];
        var messageHistoryMap = {};
        
        recipNumbers.forEach( item => {
            let resolvedMessageText = UA_APP_UTILITY.getTemplateAppliedMessage(messageText, UA_APP_UTILITY.storedRecipientInventory[item.id]);
            let historyUID = item.id+''+new Date().getTime()+''+Math.round(Math.random(100000,99999)*1000000);
            messageNumberMapArray.push({
                'number': item.number,
                'text': resolvedMessageText,
                'clientuid': historyUID
            });
            messageHistoryMap[historyUID] = {
                'contact':{
                    'name': UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].Full_Name : item.number
                },
                'from': senderId,
                'to': item.number,
                'message': resolvedMessageText,
                'status': 'NOT_SENT',
                'module': UA_SAAS_SERVICE_APP.widgetContext.module,
                'moduleId': item.id,
                'channel': 'SMS'
            };
            
            if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
                let messagePayload = {
                            "from": senderId,
                            "to": '+'+item.number.replace(/\D/g,''),
                            "text": resolvedMessageText 
                        };
                UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            }

            $('#msgItemsProgHolder').append(`
                <div id="msg-resp-item-${parseInt(item.number)}" class="msgRespItem" title="${getSafeString(resolvedMessageText)}">
                  <div class="mri-label"><b>${$('.msgRespItem').length+1}</b>. SMS to ${UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].name+'-' : ''} ${item.number}</div>
                  <div class="mri-status">Sending...</div>
                </div>
            `);
            
        });
        
        if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        
        UA_TPA_FEATURES.sendBulkSMS(senderId, messageNumberMapArray, (function(successSentMsgRes, successSentMsgCount){
            $('#totMsgDisp').text(`All messages have been processed. TOTAL: ${messageNumberMapArray.length}, SENT: ${successSentMsgCount}, FAILED: ${messageNumberMapArray.length-successSentMsgCount}`);
            if(successSentMsgRes && typeof(successSentMsgRes) == 'object' && successSentMsgRes.data && typeof(successSentMsgRes.data) == 'object' && successSentMsgRes.data.data && typeof(successSentMsgRes.data.data) == 'object' && successSentMsgRes.data.data.id && successSentMsgRes.data.data.type && successSentMsgRes.data.data.type === "SMS") {
                UA_APP_UTILITY.resetMessageFormFields(); 
            }
            else{
                showErroWindow('Unable to send message', 'kindly check <b>Manage numbers for sending and receiving messages</b> option in settings page.');
                return;              
            }
        }), messageHistoryMap);


    },
    showMessagingWorkflowCode: async function(messagePayload) {
        
        if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        
    },
    insertTemplateContentInMessageInput: function (templateId) {
        let curTemplateItem = UA_APP_UTILITY.fetchedTemplates[templateId];
        UA_TPA_FEATURES.chosenMessgeTemplate = curTemplateItem;
        $("#messageAttachmentInputHolder").hide();
        UA_APP_UTILITY.currentAttachedFile = null;
        $("#attachedfile").text('Attach file');
        $('#inputFileAttach').val('');
        if (curTemplateItem.templateType === "placeholder_template") {
            $("#primary-send-btn").text('Send WhatsApp').css('background-color', 'green');
            if (curTemplateItem.head_mediatype && curTemplateItem.head_mediatype !== "") {
                $("#attachedfile").text('Attach ' + curTemplateItem.head_mediatype);
                $("#messageAttachmentInputHolder").show();
            }
            //            if(curTemplateItem.head_media_url){
            //                $("#messageAttachmentInputHolder").show();
            //            }
            $("#ssf-fitem-template-placeholder-holder-listholder").html("");
            curTemplateItem.placeholders.forEach(item => {
                $("#ssf-fitem-template-placeholder-holder-listholder").append(`
                    <div class="ssf-temp-placehold-item">
                            <span class="ssf-temp-placehold-item-label">{{${item}}}</span> <span class="material-icons ssf-temp-placehold-item-icon">double_arrow</span> <input onblur="UA_APP_UTILITY.lastFocusedInputElemForPlaceholderInsert = 'ssf-templ-place-input-${item}'" id="ssf-templ-place-input-${item}" data-placeholder-id="${item}" class="ssf-temp-placehold-item-input" type="text" placeholder="Type or choose field from above">
                        </div>
                `);
            });
            if (curTemplateItem.placeholders.length > 0) {
                $("#ssf-fitem-template-placeholder-holder").show();
            } else {
                $("#ssf-fitem-template-placeholder-holder").hide();
            }
            $('#inp-ssf-main-message').attr('readonly', true);
        }
        else {
            $("#primary-send-btn").text('Send SMS').css('background-color', 'royalblue');
            $("#ssf-fitem-template-placeholder-holder").hide();
            $('#inp-ssf-main-message').removeAttr('readonly');
        }
        $('#inp-ssf-main-message').val(curTemplateItem.message).focus();
    },

    _getServiceTemplates: async function () {
        let templatesResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "/", "post");
        if (templatesResponse.code === 401 || templatesResponse.data.code === 401 || templatesResponse.data.message === "Authentication failed") {
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return;
        }
        return templatesResponse;
    },

    _getServiceWATemplates: async function (silenceAuthError = false, page = 1) {
        let templatesResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://rcsgui.karix.solutions/api/v1.0/template/${UA_TPA_FEATURES.APP_SAVED_CONFIG.waba_id}?status=APPROVED&page=` + page, "get");
        if (templatesResponse.code === 401 || templatesResponse.data.code === 401 || templatesResponse.data.message === "Authentication failed") {
            if (silenceAuthError) {
                return {
                    data: {
                        data: []
                    }
                };
            }
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return;
        }
        return templatesResponse;
    },

    showReAuthorizeERROR: function (showCloseOption = false) {
        if ($('#inp_tpa_api_key').length > 0) {
            console.log('already showing reautherr');
            return;
        }
        showErroWindow("Authorization needed!", `You need to authorize your ${UA_TPA_FEATURES.tpaSortName} Account to proceed. <br><br> 
        <div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
            <div style="
                font-size: 24px;
                font-weight: bold;
                margin-top: 20px;
                margin-bottom: 15px;
            ">SMS Channel Configuration</div>
            <div style="margin-left:20px">
                <div style="font-size: 16px;margin-bottom: 5px;margin-top: 10px;color: rgb(80,80,80);">${UA_TPA_FEATURES.tpaSortName} API Key</div>
                <div class="help_apikey_tip"><a target="_blank" href="https://portal.telnyx.com/#/app/api-keys">Create or Get Here</a></div>
                <input id="inp_tpa_api_key" type="text" placeholder="Enter your API Key" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
            </div>
            <!--div style="margin-left:20px">
                <div style="font-size: 16px;margin-bottom: 0px;margin-top: 10px;color: rgb(80,80,80);">${UA_TPA_FEATURES.tpaSortName} SMS Sender IDs</div>
                <div style="font-size:14px;color:silver;margin-bottom:5px">Provide comma separated in case of multiple sender IDs</div>
                <input id="inp_tpa_senders" type="text" placeholder="Enter ${UA_TPA_FEATURES.tpaSortName} SMS Sender IDs" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
            </div-->
            
            <!--div style="
                font-size: 24px;
                font-weight: bold;
                margin-top: 20px;
                margin-bottom: 15px;
            ">WhatsApp Channel Configuration</div>
            <div style="margin-left:20px;margin-bottom:5px">
            <div style="font-size: 16px;margin-bottom: 5px;margin-top: 10px;color: rgb(80,80,80);">Karix WhatsApp WABA ID</div>
            <input id="inp_tpa_waba_id" type="text" placeholder="Enter WABA ID" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
            </div-->
            <button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.saveTPAAPIKey(UA_APP_UTILITY.reloadWindow)">Authorize</button>
        </div>`, showCloseOption, false, 500);
        $('#inp_tpa_api_key').focus();
        UA_LIC_UTILITY.showExistingInvitedAdminDDHTML(true);
    },

    showReAuthorizeERRORWithClose: function () {
        UA_TPA_FEATURES.showReAuthorizeERROR();
    },

    saveTPAAPIKey: async function(callback){
        let authtoken = $("#inp_tpa_api_key").val();
        if(!valueExists(authtoken)){
            showErroMessage("Please fill API Key");
            return;
        }

        let isTpaAccountAutherized = await UA_TPA_FEATURES.isTpaAccountAutherized(extensionName, `https://api.telnyx.com/v2/balance`, "GET", {}, authtoken);
        
        if(isTpaAccountAutherized) {
            await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', 'authtoken', authtoken, callback);
        }
        else {
            showErroMessage("<b>Please provide Authorization</b></br></br>API Key is Incorrect.");
            return;
        }

    },
    isTpaAccountAutherized: async function (extensionName, url, method, data, authtoken) {

        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = { "Content-Type": "application/json", "Authorization": "Bearer "+ authtoken };
        
        return await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, url: url, method: method, data: data, headers: headers}).then(async (response) => {
            if(response && typeof(response) === 'object' && response.data && typeof(response.data) === 'object' && response.data.data && typeof(response.data.data) === 'object' && response.data.data.data && typeof(response.data.data.data) === 'object' && String(response.data.data.data.available_credit)) {
                console.log(response);
                removeTopProgressBar(credAdProcess3);
                return true;
            }
            else {
                removeTopProgressBar(credAdProcess3);
                return false;
            }
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
    },

    saveTPAWABAAPIKey: async function (callback) {
        let authtoken = $("#inp_tpa_waba_api_key").val();
        if (!valueExists(authtoken)) {
            showErroMessage("Please Enter API Key");
            return;
        }
        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', 'waba_authtoken', authtoken, callback);
    },

    sendBulkSMS: async function(sender, numberMessageMapArray, callback, messageHistoryMap) {
        
        let successSentMsgCount = 0;
        
        for(let MsgMapArr=0; MsgMapArr < numberMessageMapArray.length; MsgMapArr++) {

            
            let msgUrl = 'https://api.telnyx.com/v2/messages';
            let msgText = numberMessageMapArray[MsgMapArr].text;
            let msgNumber = '+'+numberMessageMapArray[MsgMapArr].number.replace(/\D/g,'');
            let messagePayload = { 
                                    "text": msgText,
                                    "to": msgNumber,
                                    "from": sender
                                };
            
            let successSentMsgRes = await UA_TPA_FEATURES.getAPIResponse(extensionName, msgUrl, "POST", messagePayload);
            if(successSentMsgRes && typeof(successSentMsgRes) == 'object' && successSentMsgRes.data && typeof(successSentMsgRes.data) == 'object' && successSentMsgRes.data.data && typeof(successSentMsgRes.data.data) == 'object' && successSentMsgRes.data.data.id && successSentMsgRes.data.data.type && successSentMsgRes.data.data.type === "SMS") {
                successSentMsgCount++;                
                messageHistoryMap[numberMessageMapArray[MsgMapArr].clientuid].status = successSentMsgRes.data.data.to[0].status;
                $(`#msg-resp-item-${parseInt(numberMessageMapArray[MsgMapArr].number)} .mri-status`).text(successSentMsgRes.data.data.to[0].status).css({'color': 'green'});
                messageHistoryMap[numberMessageMapArray[MsgMapArr].clientuid].id = successSentMsgRes.data.data.id+"";
            }
            else {
                if(successSentMsgRes && typeof(successSentMsgRes) == 'object' && successSentMsgRes.error && typeof(successSentMsgRes.error) == 'object' && successSentMsgRes.error.errors && typeof(successSentMsgRes.error.errors) == 'object' && successSentMsgRes.error.errors[0].title && successSentMsgRes.error.errors[0].detail) {
                    $(`#msg-resp-item-${parseInt(numberMessageMapArray[MsgMapArr].number)} .mri-status`).text(successSentMsgRes.error.errors[0].detail).css({'color': 'crimson'});
                }
                else {
                    $(`#msg-resp-item-${parseInt(numberMessageMapArray[MsgMapArr].number)} .mri-status`).text(`Failed`).css({'color': 'crimson'});
                }
                messageHistoryMap[numberMessageMapArray[MsgMapArr].clientuid].id = "";
            }

            $('#totMsgDisp').text(`Sending messages ${MsgMapArr+1}/${numberMessageMapArray.length}...`);
            if(MsgMapArr == numberMessageMapArray.length-1) {
                if(extensionName.endsWith('desk')) {
                    await UA_SAAS_SERVICE_APP.addSentSMSAsRecordInHistory(messageHistoryMap);
                }
                await callback(successSentMsgRes, successSentMsgCount);
            }
            
        }

    },

    _proceedToSendSMSAndExecuteCallback: async function (messagePayload, callback) {
        if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        var sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://api.telnyx.com/v2/messages", "POST", messagePayload);
        callback(sentAPIResponse , messagePayload);
    },

    getAPIResponse: async function (extensionName, url, method, data) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = { "Content-Type": "application/json" };
        await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({ appCode: extensionName, TPAService: UA_TPA_FEATURES.tpaApiName, url: url, method: method, data: data, headers: headers, _ua_lic_adminUserID: UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID }).then((response) => {
            response = response.data;
            returnResponse = response;
            removeTopProgressBar(credAdProcess3);
            return true;
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
        return returnResponse;
    },

    getCurrentAccountOrBalanceInfo: async function (callback) {

        let credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
       
        let response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://api.telnyx.com/v2/balance", "GET", {});
        if(response && typeof(response) === 'object' && response.data && typeof(response.data) === 'object' && response.data.data && typeof(response.data.data) === 'object' && String(response.data.data.available_credit)) {
            callback(response.data.data);
            removeTopProgressBar(credAdProcess3);
            return true;
        }
        else {
            UA_TPA_FEATURES.showReAuthorizeERROR();
            removeTopProgressBar(credAdProcess3);
            return false;
        }           
        
    },
    getSenders: async function() {
        
        let credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        
        let response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://api.telnyx.com/v2/phone_numbers", "GET", {});
        if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.data && typeof(response.data.data) == 'object' && response.data.data.length) {
            removeTopProgressBar(credAdProcess3);
            return response.data.data;
        }
        else {
            removeTopProgressBar(credAdProcess3);
            return false;
        } 
        
    },
    getSafeString: function(rawStr) {
        if (!rawStr || rawStr+"".trim() === "") {
            return "";
        }

        return $('<textarea/>').text(rawStr).html();
    },
    actualPersonalCommonSettingResolved: function() {
        UA_TPA_FEATURES.addNewWebhook();
        UA_APP_UTILITY.insertIncomingWD_supportedModules();
        UA_TPA_FEATURES.insertIncomingWebhookDialog();
        $('#suo-item-incoming-nav').html(`<span class="material-icons">sync</span>  Incoming & Outgoing events sync <span class="anl_servicename"></span><div class="ac_name_id">Click to configure
                                    </div>`);
    },
    incomingCaptureStatusChanged: function(){
        if(UA_TPA_FEATURES.EXISTING_WEBHOOK_ID){
            document.getElementById('tpa-switch-incoming-enable-capture').checked = true;
            $('.tpaichan-devider').fadeIn();
        }
        else{
            document.getElementById('tpa-switch-incoming-enable-capture').checked = false;
            $('.tpaichan-devider:not(#master-tpai-incom-chan-item)').fadeOut();
        }
    },
    deleteExistingWebhook: async function(){
        
        $('.tpaichan-devider').last().find('#tpaichan-user-mapping-text').children().each(function() {
            if($(this).attr('toggle') === 'on') {
                $(this).click();
            }
        });
        
        UA_TPA_FEATURES.EXISTING_WEBHOOK_ID = null;
        UA_TPA_FEATURES.incomingCaptureStatusChanged();
        return true;
    },
    showIncomingWebhookDialog: function(){
        $("#error-window-tpa_service_senders").show();
        $("#incoming-channel-config-item").parent().remove();
    },
    removeIncomingWebHookFunction: async function(selectedNumberId){
        
        let response = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://api.telnyx.com/v2/phone_numbers/${selectedNumberId}/messaging`, "PATCH", {"messaging_profile_id": ""});
        
        if(response && typeof(response) === 'object' && response.data && typeof(response.data) === 'object' && response.data.data && typeof(response.data.data) === 'object' && !response.data.data.messaging_profile_id && response.data.data.messaging_profile_id === "") {
            return true;
        }
        else {
            return false;
        }
        
    },
    EXISTING_WEBHOOK_ID: null,
    addNewWebhookforTpa: async function(selectedNumberId, ulgWebhookId){
        
        let response = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://api.telnyx.com/v2/phone_numbers/${selectedNumberId}/messaging`, "PATCH", {"messaging_profile_id": ulgWebhookId});
        
        if(response && typeof(response) === 'object' && response.data && typeof(response.data) === 'object' && response.data.data && typeof(response.data.data) === 'object' && response.data.data.messaging_profile_id && response.data.data.messaging_profile_id === ulgWebhookId) {
            return true;
        }
        else {
            return false;
        }
        
    },
    addNewWebhook: async function(selectedNumberId, ulgWebhookId){
        
        if(!UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS){
            UA_APP_UTILITY.CALL_AFTER_PERSONAL_COMMON_SETTING_RESOLVED.push(UA_TPA_FEATURES.addNewWebhook);
            return false;
        }
        if(UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.hasOwnProperty('ua_incoming_enable_capture_'+currentUser.uid) && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS['ua_incoming_enable_capture_'+currentUser.uid] === false){
            return;
        }
        
        UA_TPA_FEATURES.EXISTING_WEBHOOK_ID = true;
        UA_TPA_FEATURES.incomingCaptureStatusChanged();
        
    },
    insertIncomingWebhookDialog: async function() {
        
        let ulgWebhookId = '';
        let response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://api.telnyx.com/v2/messaging_profiles", "GET", {});
        
        if(response && typeof(response) === 'object' && response.data && typeof(response.data) === 'object' && response.data.data && typeof(response.data.data) === 'object' && response.data.data.length && response.data.data.filter(function(arr){return arr.webhook_url === UA_APP_UTILITY.getAuthorizedWebhookURL()})[0]) {
            ulgWebhookId = response.data.data.filter(function(arr){return arr.webhook_url === UA_APP_UTILITY.getAuthorizedWebhookURL()})[0].id;
        }
        else {
            response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://api.telnyx.com/v2/messaging_profiles", "POST", {"name": "zohocrmWebhook","webhook_url": UA_APP_UTILITY.getAuthorizedWebhookURL()} );
            if(response && typeof(response) === 'object' && response.data && typeof(response.data) === 'object' && response.data.data && typeof(response.data.data) === 'object' && response.data.data.id) {
                ulgWebhookId = response.data.data.id;
            }
            else {
                UA_TPA_FEATURES.showReAuthorizeERROR();
                return;
            }
        }       
        
        if(UA_TPA_FEATURES.tpaSendersArray.length) {
            
            $('.tpaichan-devider').last().after(`<div class="tpaichan-devider" id="tpaichan-fields-mapping" style="">
                    <div id="tpaichan-fields-mapping-title">Manage numbers for sending and receiving messages</div>
                    <div id="tpaichan-user-mapping-text">
            
                    </div>
                </div>`);
            
            for(let i=0; i < UA_TPA_FEATURES.tpaSendersArray.length; i++) {
                
                let isOff = true;
                $('.tpaichan-devider').last().find('#tpaichan-user-mapping-text').append(`<div style="margin-bottom: 0px; position: relative; width: calc(100% - 0px); cursor: pointer; box-sizing: border-box; padding: 12px 10px 10px 0px;" onclick="ulgSettingToggleClick_${UA_TPA_FEATURES.tpaSendersArray[i].id}ActFunction(this);" class="incomingWebhookDiv_${UA_TPA_FEATURES.tpaSendersArray[i].id}">
                    <style>.incomingWebhookDiv_${UA_TPA_FEATURES.tpaSendersArray[i].id}:hover { box-shadow: inset -320px 0px 40px #cbcbc81c; }</style>
                    <div style="display: flex; justify-content: space-between; width: 100%;"><b style="width: 150px; position: relative; display: inline-block; text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">${UA_TPA_FEATURES.tpaSendersArray[i].phone_number}</b> <span class="incomingWebhookStatus_${UA_TPA_FEATURES.tpaSendersArray[i].id}" style="position: relative; right: 35px;"></span></div>
                    <div class="ac_name_label_tpaIncoming_${UA_TPA_FEATURES.tpaSendersArray[i].id}" style="font-size: 12px; text-align: right; display: block; color: grey; position: absolute; right: 10px; top: 8.5px;"></div>
                </div>`);
                
                
                if(UA_TPA_FEATURES.tpaSendersArray[i].messaging_profile_id === ulgWebhookId) {
                    $(`.incomingWebhookStatus_${UA_TPA_FEATURES.tpaSendersArray[i].id}`).text('Enabled');
                    $(`.incomingWebhookDiv_${UA_TPA_FEATURES.tpaSendersArray[i].id}`).attr('toggle', 'on');
                    $('.incomingCreateModuleDiv').show();
                    isOff = false;
                }
                else {
                    $(`.incomingWebhookStatus_${UA_TPA_FEATURES.tpaSendersArray[i].id}`).text('Disabled');
                    $('.incomingCreateModuleDiv').hide();
                }
                
                UA_TPA_FEATURES.ulgON_OFFToggleDiv(`ulgSettingToggleClick_${UA_TPA_FEATURES.tpaSendersArray[i].id}`, `.ac_name_label_tpaIncoming_${UA_TPA_FEATURES.tpaSendersArray[i].id}`, '25px', `UA_TPA_FEATURES.addNewWebhookforTpa("${UA_TPA_FEATURES.tpaSendersArray[i].id}", "${ulgWebhookId}")`, `UA_TPA_FEATURES.removeIncomingWebHookFunction("${UA_TPA_FEATURES.tpaSendersArray[i].id}")`, isOff, UA_TPA_FEATURES.tpaSendersArray[i].id);

            }
        }

    },
    textCopyInCommand: function(selected) {
        let copyFrom = $('<textarea/>');
        copyFrom.text($(selected).attr('data-copyText'));
        $('body').append(copyFrom);
        copyFrom.select();
        document.execCommand('copy');
        copyFrom.remove();
    },
    ulgON_OFFToggleDiv : async function(ulgToggle, ulgToggleParent, toggleButtSize, toggleActionFunctionOn, toggleActionFunctionOff, isOff, uniqId) {      

        let toggleOn = `<path xmlns="http://www.w3.org/2000/svg" d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm0 16H5V5h14v14zM17.99 9l-1.41-1.42-6.59 6.59-2.58-2.57-1.42 1.41 4 3.99z"/>` 
        let toggleOff = `<path style="fill: #727f8c;" d="M19 5v14H5V5h14m0-2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2z"/>` 
        
        $(ulgToggleParent).append(`<svg class="${ulgToggle}" xmlns="http://www.w3.org/2000/svg" height="${toggleButtSize}" viewBox="0 0 24 24" width="${toggleButtSize}" fill="#000000" style="transition: 0.3s; cursor: pointer; fill: #1e5f9f; display: inline-block; margin-right: -10px; position: relative; right: 10px;">
            ${isOff ? toggleOff : toggleOn}
        </svg>`);

        $('<script>').attr('type', 'text/javascript').text(`
async function ${ulgToggle}ActFunction(selected) {
    if($(selected).attr('toggle') == 'on') {
        UA_TPA_FEATURES.wcConfirm('<div style="width: 100%; height: 100px; display: flex; align-items: center; justify-content: center;flex-direction: column; justify-content: center;"><div class="tpaIncomingLoderSet"></div><div class="tpaIncomingTextSet" style="font-size: 13px; height: 0px; position: relative; top: 8px;">Webhook Removing...</div></div>','','Okay',false,true);
        UA_TPA_FEATURES.ulgloaderDiv(".tpaIncomingLoderSet", "30px");
        if(await ${toggleActionFunctionOff}) {
            $(selected).attr('toggle', 'off');
            $('.incomingWebhookStatus_${uniqId}').text('Disabled');
            $('.incomingCreateModuleDiv').hide();
            $(selected).find('.${ulgToggle}').html('${toggleOff}').hide().show(100);
            $('.tpaIncomingTextSet').text('Webhook removed');
            setTimeout(function() {UA_TPA_FEATURES.wcConfirmHide();}, 1500);
        }
        else {
            $('.incomingCreateModuleDiv').show();
            $('.tpaIncomingTextSet').text('Webhook delete failed');
            setTimeout(function() {UA_TPA_FEATURES.wcConfirmHide();}, 1500);
        }
    }
    else {
        UA_TPA_FEATURES.wcConfirm('<div style="width: 100%; height: 100px; display: flex; align-items: center; justify-content: center;flex-direction: column; justify-content: center;"><div class="tpaIncomingLoderSet"></div><div class="tpaIncomingTextSet" style="font-size: 13px; height: 0px; position: relative; top: 8px;">Webhook Creating...</div></div>','','Okay',false,true);
        UA_TPA_FEATURES.ulgloaderDiv(".tpaIncomingLoderSet", "30px");
        if(await ${toggleActionFunctionOn}) {
            $(selected).attr('toggle', 'on');
            $('.incomingWebhookStatus_${uniqId}').text('Enabled');
            $('.incomingCreateModuleDiv').show();
            $(selected).find('.${ulgToggle}').html('${toggleOn}').hide().show(100);
            $('.tpaIncomingTextSet').text('Webhook Created');
            setTimeout(function() {UA_TPA_FEATURES.wcConfirmHide();}, 1500);
        }
        else {
            $('.incomingCreateModuleDiv').hide();
            $('.tpaIncomingTextSet').text('Webhook Create failed');
            setTimeout(function() {UA_TPA_FEATURES.wcConfirmHide();}, 1500);
        }
    }
}`).prependTo(ulgToggleParent);

    },
    ulgloaderDiv : async function(ulgLoaderParent, ulgLoaderSize) {

        $(ulgLoaderParent).append(`<div class="loaderOuterDiv"><div class="wcMsgLoadingInner" title="loading…"><svg class="wcMsgLoadingSVG" width="calc(${ulgLoaderSize}/2)" height="calc(${ulgLoaderSize}/2)" viewBox="0 0 46 46" role="status"><circle class="wcMsgLoadingSvgCircle" cx="23" cy="23" r="20" fill="none" stroke-width="6" style="stroke: rgb(37 162 94);"></circle></svg></div></div>`);

        let ulg_loader_popup_css = `/* message loading style */

.wcMsgLoadingInner {
    margin: 0 auto;
    background-color: #fff;
    border-radius: 50%;
    box-shadow: 0 1px 1px 0 rgba(0,0,0,.06),0 2px 5px 0 rgba(0,0,0,.2);
    display: flex;
    align-items: center;
    justify-content: center;
    width: ${ulgLoaderSize};
    height: ${ulgLoaderSize};
    color: rgba(0,0,0,0.25);
}

.wcMsgLoadingSvgCircle {
    stroke: #ccc;
    stroke-dasharray: 1,150;
    stroke-dashoffset: 0;
    stroke-linecap: round;
    animation: wcMsgLoadingSvgCircle 1.5s ease-in-out infinite;
}

.wcMsgLoadingSVG {
    animation: wcMsgLoadingSVG 2s linear infinite;
}

/* message loading style */

@keyframes wcMsgLoadingSVG{
    to{transform:rotate(1turn)}
}

@keyframes wcMsgLoadingSvgCircle{
    0%{stroke-dasharray:1,150;stroke-dashoffset:0}
    50%{stroke-dasharray:90,150;stroke-dashoffset:-35}
    to{stroke-dasharray:90,150;stroke-dashoffset:-124}
}`;

        $('<style>').attr('type', 'text/css').text(ulg_loader_popup_css).prependTo('.loaderOuterDiv');        

    },
    wcConfirm : async function(htmlText, runFunc, confirmTitle, alart, buttonHide) {

        $('.wcConfirmOuter').remove();
        $('body').append(`<div class="wcConfirmOuter">
            <div class="wcConfirmInner divPopUpShow">
                <div class="wcConfirmBody">
                    ${htmlText}
                </div>
                <div class="wcConfirmButt" style="display:${buttonHide?'none':'flex'};">
                    <div class="wcConfirmCancel" style="display:${alart?'none':'block'};" onclick="ulg_style_things.wcConfirmHide()">Cancel</div>
                    <div class="wcConfirmYes" onclick="UA_TPA_FEATURES.wcConfirmHide();${runFunc}">${confirmTitle}</div>
                </div>
            </div>
        </div>`);

        let ulg_confirm_popup_css = `/* Alart function style */

.wcConfirmOuter {
    position: fixed;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    background-color: #36363680;
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    align-content: center;
    justify-content: center;
    align-items: center;
    z-index: 1100;
}

.wcConfirmInner {
    position: absolute;
    min-width: 417px;
    max-width: calc(100% - 500px);
    min-height: 100px;
    max-height: max-content;
    border-radius: 5px;
    background-color: #f7f7f7;
    display: flex;
    justify-content: flex-start;
    align-content: space-between;
    align-items: flex-end;
    flex-wrap: nowrap;
    flex-direction: column;
    box-shadow: 0px 2px 12px #767676;
}

.wcConfirmBody {
    position: relative;
    width: 100%;
    height: max-content;
    min-height: 50px;
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    align-content: center;
    align-items: center;
    box-sizing: border-box;
    padding: 20px 30px;
    justify-content: flex-start;
}

.wcConfirmButt {
    position: relative;
    display: flex;
    justify-content: flex-end;
    width: 100%;
    align-items: flex-start;
    flex-wrap: nowrap;
    align-content: center;
    flex-direction: row;
    height: 54px;
    /* right: 20px; */
    padding-right: 20px;
    /* bottom: 20px; */
    box-sizing: border-box;
}

.wcConfirmYes {
    background-color: #3f4fcc;
    color: white;
    box-sizing: border-box;
    padding: 2px 10px;
    border-radius: 3px;
    margin: 5px 5px;
    box-shadow: inset 0px 0px 11px #4244707a;
    cursor: pointer;
}

.wcConfirmCancel {
    background-color: #d6d6d6;
    color: #525252;
    box-sizing: border-box;
    padding: 2px 10px;
    border-radius: 3px;
    margin: 5px 5px;
    box-shadow: inset 0px 0px 11px #c5c5c57a;
    cursor: pointer;
}

/* Alart function style */`;

        $('<style>').attr('type', 'text/css').text(ulg_confirm_popup_css).prependTo('.wcConfirmOuter');

        $('<script>').attr('type', 'text/javascript').text(`
        $(document).keypress(function(e) {

            if($('.wcConfirmYes').is(':visible'))
            if(e.which == 13) {
                $('.wcConfirmYes').click();
                e.preventDefault();
            }

        });`).prependTo('.wcConfirmOuter');
        

    },
    wcConfirmHide : async function() {

        $('.wcConfirmInner').removeClass('divPopUpHide').removeClass('divPopUpShow').addClass('divPopUpHide').hide();
        $('.wcConfirmOuter').fadeOut(0);
        await setTimeout(function() { $('.wcConfirmInner').removeClass('divPopUpHide').removeClass('divPopUpShow').html('').hide().fadeOut(); $('.wcConfirmOuter').remove(); }, 300);

    }
};