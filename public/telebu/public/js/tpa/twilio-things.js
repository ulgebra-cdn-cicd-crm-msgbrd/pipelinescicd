var UA_TPA_FEATURES = {
    chosenMessgeTemplate : null,
    'clientIds': {
        'twilioforhubspotcrm' : "28e3fcba-92f6-4301-8a82-7381f8b658a9",
        'twilioforbitrix24' : "app.618df869835df8.59780487",
        'twilioforpipedrive': 'c8a6c923cf443ed6',
        'twilioforshopify': 'd17e3d73885c6dd7d3d61a4ab7d8ad84'
    },
    APP_SAVED_CONFIG : null,
    getSupportedChannels: function(){
        return [ UA_APP_UTILITY.MESSAGING_CHANNELS.SMS, UA_APP_UTILITY.MESSAGING_CHANNELS.MMS, UA_APP_UTILITY.MESSAGING_CHANNELS.WHATSAPP ];
    },
    workflowCode : {
        "SMS": {
            "From": "FILL_HERE",
            "To": "FILL_HERE",
            "Body":"FILL_HERE_Message_Text",
            "module": "FILL_HERE",
            "recordId": "FILL_HERE",
            "channel": "SMS",
            "ulgebra_webhook_authtoken": null
        },
        "MMS": {
            "From": "FILL_HERE",
            "To": "FILL_HERE",
            "Body":"FILL_HERE_Message_Text",
            "MediaUrl": "FILL_HERE",
            "module": "FILL_HERE",
            "recordId": "FILL_HERE",
            "channel": "MMS",
            "ulgebra_webhook_authtoken": null
        },
        "WhatsApp":{
            "From": "FILL_HERE",
            "To": "FILL_HERE",
            "Body":"FILL_HERE_Message_Text",
            "MediaUrl": "FILL_HERE",
            "module": "FILL_HERE",
            "recordId": "FILL_HERE",
            "channel": "WhatsApp",
            "ulgebra_webhook_authtoken": null
        }
    },
    accountSID: null,
    serviceSID : null,
    incomingDataSchemaJSON: {
        "message": {
            "AccountSid": {
                example: "ACaf4d4f141ceaf613a1743b42ed6c602c"
            },
            "ApiVersion": {
                example: "2010-04-01"
            },
            "Body": {
                example: "Hi, how are you?"
            },
            "From": {
                example: "+919994411345"
            },
            "MessageSid": {
                example: "SMd85014e10671da8bce767a5eba08b289"
            },
            "NumSegments": {
                example: "1"
            },
            "ProfileName": {
                example: "Vijay"
            },
            "SmsMessageSid": {
                example: "SMd85014e10671da8bce767a5eba08b289"
            },
            "SmsSid": {
                example: "SMd85014e10671da8bce767a5eba08b289"
            },
            "SmsStatus": {
                example: "received"
            },
            "To": {
                example: "+14155238886"
            },
            "WaId": {
                example: "919994411345"
            },
            "ReferralNumMedia": {
                example: "0"
            },
            "MediaContentType0":{
                example: "image/jpeg"
            },
            "MediaUrl0": {
                example: "https://api.twilio.com/2010-04-01/Accounts/ACaf4d4f141ceaf613a1743b42ed6c602c/Messages/MM879f4f89bdf3b475a5ed414bcd5b29e2/Media/ME8c3c58035abe73f7ca7746f317c2fc37"
            },
            "NumMedia": {
                example: "0"
            },
            "_UA_Channel": {
                example: "SMS"
            }
        }
    },
    renderInitialElements: async function(){
        UA_APP_UTILITY.addRTNotificationNavigation();
        $("#ac_name_label_tpa .anl_servicename").text('Twilio');
        
        $(".incoming-config-tpa-entity").text('messages');
        $('#incoming-channel-display-text').html(`Add your twilio Phone Numbers &amp; WhatsApp Numbers to <b>Ulgebra Integrations API</b> to sync incoming twilio messages.
                    <br><br>
                    <a target="_blank" nofollow="" id="tpa_twilio_service_sender_url" href="#"><button class="ua_service_login_btn ua_primary_action_btn">Please wait...</button></a>`);
        UA_TPA_FEATURES.renderFieldMappingCriteriaExtra();
        UA_TPA_FEATURES.getCurrentAccountOrBalanceInfo(async function(resp){
            let accountData = resp.data.accounts[0];
            UA_TPA_FEATURES.accountSID = accountData.sid;
            $('#ac_name_label_tpa .ac_name_id').text(`SID - ${UA_TPA_FEATURES.accountSID}`);
            
            if(!UA_TPA_FEATURES.accountSID){
                return;
            }
            
            UA_TPA_FEATURES.runAfterSIDResolved.forEach(item=>{
                item();
            });
            
            if(UA_TPA_FEATURES.loadConvAfterSIDResolved){
                UA_TPA_FEATURES.loadConvAfterSIDResolved();
            }
            
            try{
                var numbersAPIResp = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://api.twilio.com/2010-04-01/Accounts/${UA_TPA_FEATURES.accountSID}/IncomingPhoneNumbers.json`, "GET");
                if (numbersAPIResp.error) {
                    showErroMessage(numbersAPIResp.error.message);
                    return true;
                }
                numbersAPIResp.data.incoming_phone_numbers.forEach(item=>{
                    item.value = item.phone_number;
                    UA_APP_UTILITY.addMessaegSender(item.phone_number, UA_APP_UTILITY.MESSAGING_CHANNELS.SMS, item.friendly_name, item);
                });
                UA_APP_UTILITY.TPA_SENDERS_FETCH_COMPLETED();
            }
            catch(ex){
                UA_APP_UTILITY.TPA_SENDERS_FETCH_COMPLETED();
                console.log(ex);
            }
            
//            var sendersArray = [];
//            UA_TPA_FEATURES.APP_SAVED_CONFIG.senders.split(',').forEach(item=>{
//                    sendersArray.push({
//                        'label': item,
//                        'value': item
//                    });
//            });
//            UA_APP_UTILITY.addMessaegSender('919994411345', UA_APP_UTILITY.MESSAGING_CHANNELS.SMS, "919994411345");
            
        });
        
//        var fetchedTemplatesResponse = await UA_TPA_FEATURES._getServiceTemplates();
//        console.log('fetchedtemoktes ', fetchedTemplatesResponse);
//        fetchedTemplatesCollection = fetchedTemplatesResponse.data.data;
        
        var sendersUniqueArray = [];
        var templatesArray = [];
//        fetchedTemplatesCollection.forEach(item=>{
//            UA_APP_UTILITY.fetchedTemplates[item.id] = {
//                'message': item.message
//            };
//            templatesArray.push({
//                'label': item.name,
//                'value': item.id
//            });
//            if(sendersUniqueArray.indexOf(item.senderid) === -1){
//                sendersUniqueArray.push(item.senderid);
//                sendersArray.push({
//                    'label': item.senderid,
//                    'value': item.senderid
//                });
//            }
//        });
        
//        var approvedTemplateDDId = UA_APP_UTILITY.renderSelectableDropdown('#ssf-fitem-template-var-holder', 'Insert Pinnacle template', templatesArray, 'UA_TPA_FEATURES.insertTemplateContentInMessageInput', false, false);
        
        UA_APP_UTILITY.getPersonalWebhookAuthtoken();
       
        var countryCallCodeArray = [];
        UA_APP_UTILITY.countryCallingCodeArray.forEach(item=>{
            countryCallCodeArray.push({
                'label': `${item.name} (${item.dial_code})`,
                'value': item.dial_code
            });
        });
        
        UA_APP_UTILITY.renderSelectableDropdown('#ssf-new-recip-countrycode', 'Select country', countryCallCodeArray, 'UA_APP_UTILITY.log');
        
//        var fetchedTemplatesWAResponse = await UA_TPA_FEATURES._getServiceWATemplates(true);
//        console.log('wa fetchedtemoktes ', fetchedTemplatesWAResponse);
//         var waTemplatesArray = [];
//        fetchedTemplatesWAResponse.data.data.forEach(item=>{
//            UA_APP_UTILITY.fetchedTemplates[item.tempid] = {
//                'message': item.body_message,
//                'templateType': 'placeholder_template',
//                'placeholders': item.placeholders ? item.placeholders.split(',') : [],
//                'tempid': item.tempid,
//                'head_media_url': item.head_media_url,
//                'head_mediatype': item.head_mediatype
//            };
//            waTemplatesArray.push({
//                'label': item.temptitle + ' (WhatsApp)',
//                'value': item.tempid
//            });
//        });
//        
//        UA_APP_UTILITY.renderSelectableDropdown('#ssf-fitem-template-var-holder', 'Insert Pinnacle template', waTemplatesArray, 'UA_TPA_FEATURES.insertTemplateContentInMessageInput', false, false, approvedTemplateDDId);
        
        
        
    },
    
    appsConfigHasBeenResolved: function(){
        UA_TPA_FEATURES.addNewWebhook();
        UA_TPA_FEATURES.fetchOldPipedriveTwilioTemplates();
        UA_TPA_FEATURES.renderOldTemplatesOfSAAS();
    },

    renderFieldMappingCriteriaExtra: function(){
        UA_FIELD_MAPPPING_UTILITY.renderIncomingFieldMappingChannelOptions(`When incoming message received to `, 'direction-in-to=');
    },
    
    renderWorkflowBodyCode :function(accessToken){
        UA_TPA_FEATURES.workflowCode.SMS.ulgebra_webhook_authtoken = accessToken.saas;
        UA_TPA_FEATURES.workflowCode.MMS.ulgebra_webhook_authtoken = accessToken.saas;
        UA_TPA_FEATURES.workflowCode.WhatsApp.ulgebra_webhook_authtoken = accessToken.saas;
        
        UA_APP_UTILITY.addWorkflowBodyCode('sms', 'For Sending SMS', UA_TPA_FEATURES.workflowCode.SMS);
        
        UA_APP_UTILITY.addWorkflowBodyCode('mms', 'For Sending MMS', UA_TPA_FEATURES.workflowCode.MMS);
        
        UA_APP_UTILITY.addWorkflowBodyCode('whatsapp', 'For Sending WhatsApp', UA_TPA_FEATURES.workflowCode.WhatsApp);
    },
    
    insertTemplateContentInMessageInput :function(templateId){
        try{   
            UA_APP_UTILITY.removeCurrentUploadedAttachment();
        }catch(er){ console.log(er); }
        var curTemplateItem = UA_APP_UTILITY.fetchedTemplates[templateId];
        UA_TPA_FEATURES.chosenMessgeTemplate = curTemplateItem;
        $("#ssf-fitem-template-placeholder-holder-listholder").html("");
        var placeHolderItems = UA_TPA_FEATURES.getMatchingMsgTemplateList(curTemplateItem.message);
        curTemplateItem.placeholders = [];
        placeHolderItems.forEach(item => {
            item  = item.replace(/[^0-9]/g, '');
            curTemplateItem.placeholders.push(item);
            $("#ssf-fitem-template-placeholder-holder-listholder").append(`
                    <div class="ssf-temp-placehold-item">
                            <span class="ssf-temp-placehold-item-label">${item}</span> <span class="material-icons ssf-temp-placehold-item-icon">double_arrow</span> <input onblur="UA_APP_UTILITY.lastFocusedInputElemForPlaceholderInsert = 'ssf-templ-place-input-${item}'" id="ssf-templ-place-input-${item}" data-placeholder-id="${item}" class="ssf-temp-placehold-item-input" type="text" placeholder="Type or choose field from above">
                        </div>
                `);
        });
        if (curTemplateItem.placeholders.length > 0) {
            $("#ssf-fitem-template-placeholder-holder").show();
        } else {
            $("#ssf-fitem-template-placeholder-holder").hide();
        }
        $('#inp-ssf-main-message').val(curTemplateItem.message);
        try{
            if(curTemplateItem && curTemplateItem.attachmentData && curTemplateItem.attachmentData.isForNewTemp){
                UA_APP_UTILITY.currentAttachedFile = curTemplateItem.attachmentData;
                $('.cht-attachmentNameHolder.inForm').css({'display': 'inline-block'});
                $('.cht-attachmentNameHolder.inForm .cht-attch-close').hide();
                $('.cht-attachmentNameHolder.inForm .cht-attch-prog').hide();
                $('.cht-attachmentNameHolder.inForm .cht-attch-status').text(getSafeString(curTemplateItem.attachmentData.fileMeta.name));
            }
        }
        catch(err){
            console.log(err);
        }
    },
    
    _getServiceTemplates: async function(){
        var templatesResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "/", "post");
        if(templatesResponse.code === 401 || templatesResponse.data.code === 401 || templatesResponse.data.message === "Authentication failed"){
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return;
        }
        return  templatesResponse;
    },
    
    _getServiceWATemplates: async function(silenceAuthError = false){
        var templatesResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "/", "post");
        if(templatesResponse.code === 401 || templatesResponse.data.code === 401 || templatesResponse.data.message === "Authentication failed"){
            if(silenceAuthError){
                return {
                    data:{
                        data: []
                    }
                };
            }
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return;
        }
        return  templatesResponse;
    },
    
    getMatchingMsgTemplateList: function(messageText){
        var matches = messageText.match(/\{\{[A-Za-z0-9._\-]+\}\}*/g);
        return matches ? matches : [];
    },
    
    showReAuthorizeERROR: function(showCloseOption = false){
        if($('#inp_tpa_sid').length > 0){
            console.log('already showing reautherr');
            return;
        }
        showErroWindow("Authorization needed!", `${UA_LIC_UTILITY.getAskSharedAccessAdminTip()} You need to authorize your Twilio Account to proceed. <br><br> 
        <div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
            
            <div style="margin-left:20px">
                <div style="font-size: 16px;margin-bottom: 5px;margin-top: 10px;color: rgb(80,80,80);font-weight:bold">Twilio Account SID</div>
                <div class="help_apikey_tip"><a target="_blank" href="https://www.twilio.com/console">Get Here</a></div>
                <input id="inp_tpa_sid" type="text" placeholder="Enter Twilio SID" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
            </div>
            <div style="margin-left:20px">
                <div style="font-size: 16px;margin-bottom: 5px;margin-top: 10px;color: rgb(80,80,80);font-weight:bold">Twilio Account Auth Token</div>
        <div class="help_apikey_tip"><a target="_blank" href="https://www.twilio.com/console">Get Here</a></div>
                <input id="inp_tpa_authtoken" type="text" placeholder="Enter Twilio Authtoken" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
            </div>
            
            <button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.saveTPAAPIKey(UA_APP_UTILITY.reloadWindow)">Authorize</button>
        </div>`, showCloseOption, false, 500);
        $('#inp_tpa_api_key').focus();
        UA_LIC_UTILITY.showExistingInvitedAdminDDHTML(true);
    },
    
    showReAuthorizeERRORWithClose: function(){
        UA_TPA_FEATURES.showReAuthorizeERROR();
    },
    
    saveTPAAPIKey: async function(callback){
        let sid = $("#inp_tpa_sid").val();
        if(!valueExists(sid)){
            showErroMessage("Please Enter Twilio SID");
            return;
        }
        let authtoken = $("#inp_tpa_authtoken").val();
        if(!valueExists(authtoken)){
            showErroMessage("Please Enter Twilio Authtoken");
            return;
        }
        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', '__multiple_keys__', {
            'authtoken': authtoken,
            'sid': sid,
            'senders': []
        },
        callback);
    },
    
    saveTPAWABAAPIKey: async function(callback){
        let authtoken = $("#inp_tpa_waba_api_key").val();
        if(!valueExists(authtoken)){
            showErroMessage("Please fill API Key");
            return;
        }
        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', 'waba_authtoken', authtoken, callback);
    },
    
    
    
    prepareAndSendSMS : function(){
        if(!UA_APP_UTILITY.currentSender){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var senderId = UA_APP_UTILITY.currentSender.value;
        if(senderId === "_-_other_-_"){
            senderId = $('#DD_HOLDER_SENDER_LIST .dd-item-other-input').val().trim();
        }
        if(!valueExists(senderId)){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var messageText = $("#inp-ssf-main-message").val();
        if(!valueExists(messageText)){
            showErroWindow('Message is empty!', "Kindly fill message to proceed.");
            return false;
        }
        var recipNumbers = UA_APP_UTILITY.getCurrentMessageRecipients();
        if(recipNumbers.length === 0){
            showErroWindow('Recipient list is empty!', "Kindly add recipients to send SMS.");
            return false;
        }
        if(!UA_APP_UTILITY.fileUploadComplete){
            showErroWindow('File upload is not complete!', "Please wait while your file is uploading and try again.");
            return;
        }
        if(UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.MESSAGE_FORM){
            $('#sms-prog-window').show();
        }
         $('#msgItemsProgHolder').html('');
          var totalCount = recipNumbers.length;
          $('#totMsgDisp').text(`Sending messages 0/${totalCount}...`);
          var messagesSentCount = 0;
          var messageStatusMap = {
              'success': 0,
              'failed' : 0
          };
        senderId = UA_APP_UTILITY.currentSender.channel.id === "WHATSAPP" ? "whatsapp:+"+senderId.replace(/\D/g,'').trim() : senderId;
        recipNumbers.forEach(item=>{
            let messageURL = null;
            if(UA_APP_UTILITY.currentAttachedFile && UA_APP_UTILITY.currentAttachedFile.mediaUrl){
                messageURL = UA_APP_UTILITY.currentAttachedFile.mediaUrl;
            }
            let contactRecordId = UA_APP_UTILITY.storedRecipientInventory[item.id] ? item.id : null;
            let resolvedMessageText = UA_APP_UTILITY.getTemplateAppliedMessage(messageText, UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id]);
            if(UA_TPA_FEATURES.chosenMessgeTemplate && UA_TPA_FEATURES.chosenMessgeTemplate.placeholders){
                UA_TPA_FEATURES.chosenMessgeTemplate.placeholders.forEach(item=>{
                    let placeHolderValue = $('#ssf-templ-place-input-'+item).val();
                    //if(contactRecordId){
                        placeHolderValue = UA_APP_UTILITY.getTemplateAppliedMessage(placeHolderValue, UA_APP_UTILITY.storedRecipientInventory[contactRecordId]);
                    //}
                    resolvedMessageText = resolvedMessageText.replace('{{'+item+'}}', placeHolderValue);
                });
            }
            item.number = (UA_APP_UTILITY.currentSender.channel.id === "WHATSAPP" ? "whatsapp:+" : "+") + item.number.replace(/\D/g,'');
            let messagePayload = {
                "From": senderId,
                "To": item.number,
                "Body": resolvedMessageText
            };
            if(messageURL){
                messagePayload.MediaUrl = messageURL;
            }
            if(!contactRecordId && UA_APP_UTILITY.isConvChatView()){
                let fetchedKeys = Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
                contactRecordId = fetchedKeys.length === 1 ? fetchedKeys[0] : null;
            }
            let messageHistoryMap = {
                'contact':{
                    'name': UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].Full_Name : item.number
                },
                'from': UA_APP_UTILITY.currentSender.channel.id === "WHATSAPP" ? "+"+senderId.replace(/\D/g,'').trim() : senderId,
                'to': "+"+item.number.replace(/\D/g,''),
                'message': resolvedMessageText,
                'status': 'NOT_SENT',
                'module': UA_SAAS_SERVICE_APP.widgetContext.module,
                'moduleId': contactRecordId,
                'channel': UA_APP_UTILITY.currentSender.channel.label
            };
            $('#msgItemsProgHolder').append(`
              <div id="msg-resp-item-${parseInt(item.number)}" class="msgRespItem" title="${getSafeString(resolvedMessageText)}">
                  <div class="mri-label"><b>${$('.msgRespItem').length+1}</b>. ${UA_APP_UTILITY.currentSender.channel.label} to ${UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].name+'-' : ''} ${item.number}</div>
                  <div class="mri-status">Sending...</div>
              </div>
          `);
            UA_TPA_FEATURES._proceedToSendWAAndExecuteCallback(messagePayload, function(response){
                if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
                    return false;
                }
                UA_APP_UTILITY.resetMessageFormFields();
                console.log(response);
                if(response.error && UA_APP_UTILITY.isConvChatView()){
                    showErroWindow('Error from Twilio', response.error.message+ ` <br><br> `+(response.error.more_info ? `<a target="_blank" href="${response.error.more_info}"><button class="ua_service_login_btn ua_primary_action_btn">Read more</button></a>`: ''));
                    return false;
                }
                if(UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CHAT && response.data.sid){
                    UA_APP_UTILITY.addMessageInBox(UA_TPA_FEATURES.convertMessageToUASchema(response.data), true);
                }
                messagesSentCount++;
                let isError = response.error && response.error.message;
                $(`#msg-resp-item-${parseInt(item.number)} .mri-status`).html(isError ? 'Unable to send message. '+response.error.message+` <a target="_blank" href="${response.error.more_info}">Know more</a>`: UA_APP_UTILITY.currentSender.channel.label+' Message Sent.').css({'color': isError ? 'crimson' : 'green'});
                isError ?  messageStatusMap.failed++ : messageStatusMap.success++;
                if(messagesSentCount === totalCount){
                    $('#totMsgDisp').text(`All messages have been processed. TOTAL: ${totalCount}, SENT: ${messageStatusMap.success}, FAILED: ${messageStatusMap.failed}`);
                }else{
                    $('#totMsgDisp').text(`Sending messages ${messagesSentCount}/${totalCount}...`);
                }
                messageHistoryMap.status = isError ? "FAILED - "+response.error.message : "SUCCESS" ;
                UA_SAAS_SERVICE_APP.addSentSMSAsRecordInHistory({
                    "message1": messageHistoryMap
                });
            });
        });
        
        
    },
    
    sendSMS : function(sender, number, text, callback){
        
        var messageArray = [{
            "number": number,
            "text": text
        }];
        
        var messagePayload = {
            "sender": sender,
            "message": messageArray,
            "messagetype": "TXT"
        };
        
        UA_TPA_FEATURES._proceedToSendSMSAndExecuteCallback(messagePayload, callback);
        
    },
    
    _proceedToSendWAAndExecuteCallback: async function(messagePayload, callback){
        if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
            messagePayload.channel = "SMS";
            if(messagePayload.To.startsWith("whatsapp")){
                messagePayload.channel = "WhatsApp";
                messagePayload.To = messagePayload.To.replace("whatsapp:", "");
                messagePayload.From = messagePayload.From.replace("whatsapp:", "");
            }
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        var sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://api.twilio.com/2010-04-01/Accounts/${UA_TPA_FEATURES.accountSID}/Messages.json`, "POST", messagePayload);
        callback(sentAPIResponse);
    },
    
    getAPIResponse: async function (extensionName, url, method, data) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = {"Content-Type": "application/x-www-form-urlencoded"};
        await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, TPAService: "twilio",url: url, method: method, data: data, headers: headers, _ua_lic_adminUserID: UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID}).then((response) => {
            response = response.data;
            returnResponse = response;
            removeTopProgressBar(credAdProcess3);
            return true;
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
        return returnResponse;
    },
    
    getCurrentAccountOrBalanceInfo: async function(callback){
        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://api.twilio.com/2010-04-01/Accounts.json", "GET", {});
        if((response.code === 401 || response.message === "Authentication failed") || (response.error && response.error.code === 401) || (response.data && (response.data.code === 401 || response.data.message === "Authentication failed"))){
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return;
        }
        callback(response);
    },
    EXISTING_WEBHOOK_ID: null,
    isWebhookAlreadyExists: async function(){
        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://messaging.twilio.com/v1/Services", "GET", {});
        if(UA_TPA_FEATURES.showIfErrorInAPIResult(response)){
            return;
        }
        let existingServiceID = null;
        response.data.services.forEach((item)=>{
            let url = item.inbound_request_url;
            if(url && url.startsWith(UA_APP_UTILITY.getWebhookBaseURL())){
                existingServiceID = item.sid;
            }
        });
        UA_TPA_FEATURES.EXISTING_WEBHOOK_ID = existingServiceID;
        UA_TPA_FEATURES.serviceSID = existingServiceID;
        UA_TPA_FEATURES.incomingCaptureStatusChanged();
        return existingServiceID !== null;
    },
    
    showIfErrorInAPIResult: function(response){
        if((response.code === 401 || response.message === "Authentication failed") || (response.error && response.error.code === 401) || (response.data && (response.data.code === 401 || response.data.message === "Authentication failed"))){
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return true;
        }
        if(response.error && response.error.message){
            showErroWindow('Error from Twilio!', response.error.message+` <br><br> <a target="_blank" href="${response.error.more_info}">Know more</a>`);
            return true;
        }
        return false;
    },
    
    showNumbersFromService: function(){
        
    },
    
    addNewWebhook: async function(){
        if(!UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS){
            UA_APP_UTILITY.CALL_AFTER_PERSONAL_COMMON_SETTING_RESOLVED.push(UA_TPA_FEATURES.addNewWebhook);
            return false;
        }
        if(UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.hasOwnProperty('ua_incoming_enable_capture_'+currentUser.uid) && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS['ua_incoming_enable_capture_'+currentUser.uid] === false){
            return;
        }
        if(UA_APP_UTILITY.PERSONAL_WEBHOOK_TOKEN && appsConfig.UA_DESK_ORG_ID){
            var webhookExists = await UA_TPA_FEATURES.isWebhookAlreadyExists();
            if(!webhookExists){
                var webhookData = {
                        "FriendlyName": "Ulgebra Integrations API",
                        "InboundRequestUrl": UA_APP_UTILITY.getAuthorizedWebhookURL()+`&_ua_action=message-add`,
                        "StatusCallback": UA_APP_UTILITY.getAuthorizedWebhookURL()+`&_ua_action=message-status`,
                        "InboundMethod": "POST"
                    };
                var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://messaging.twilio.com/v1/Services", "POST", webhookData);
                if(UA_TPA_FEATURES.showIfErrorInAPIResult(response)){
                    return false;
                }
                UA_TPA_FEATURES.serviceSID = response.data.sid;
                UA_TPA_FEATURES.EXISTING_WEBHOOK_ID = UA_TPA_FEATURES.serviceSID;
                UA_TPA_FEATURES.incomingCaptureStatusChanged();
                UA_TPA_FEATURES.showIncomingWebhookDialog();
                return response.data && (response.data.sid !== null);
            }
            return true;
        }
        return false;
    },
    
    fetchConversationsAndAddToChatBox: async function(clearConvBox = true){
        if(!valueExists(UA_TPA_FEATURES.accountSID)){
            UA_TPA_FEATURES.runAfterSIDResolved.push(()=>{
                UA_TPA_FEATURES.fetchConversationsAndAddToChatBox(clearConvBox);
            });
            return true;
        }
        var senderId = "";
        var isWhatsAppChannel = false;
        if(UA_APP_UTILITY.currentSender){
            senderId = UA_APP_UTILITY.currentSender.value;
            isWhatsAppChannel = UA_APP_UTILITY.currentSender.channel === UA_APP_UTILITY.MESSAGING_CHANNELS.WHATSAPP;
            if (isWhatsAppChannel) {
                senderId = "whatsapp:+" + senderId.replace(/\D/g, '');
            }
       }
       var recipientId = "";
       if(UA_APP_UTILITY.chatCurrentRecipient){
            recipientId = UA_APP_UTILITY.chatCurrentRecipient.address;
            if(valueExists(recipientId)){
                recipientId = (isWhatsAppChannel ? "whatsapp:+" : (recipientId.indexOf('+')!==0 ? "+" : '')) +recipientId;
            }
        }
        if(UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CONVERSATIONS){
            if(clearConvBox){
                $("#uaCHATConvListHolder").html("");
                $("#chatbox-message-holder").html("");
            }
            if ((!senderId || !recipientId)) {
                let targetNumber = senderId ? senderId : recipientId;
                UA_TPA_FEATURES.getConversationsList(`&From=${encodeURIComponent(targetNumber)}&PageSize=250`);
                UA_TPA_FEATURES.getConversationsList(`&To=${encodeURIComponent(targetNumber)}&PageSize=250`);
                if (targetNumber.indexOf("whatsapp:") === 0) {
                    targetNumber = targetNumber.replace("whatsapp:", "");
                }
                else {
                    targetNumber = "whatsapp:" + targetNumber;
                }
                UA_TPA_FEATURES.getConversationsList(`&From=${encodeURIComponent(targetNumber)}&PageSize=250`);
                UA_TPA_FEATURES.getConversationsList(`&To=${encodeURIComponent(targetNumber)}&PageSize=250`);
            }
            else{
                UA_TPA_FEATURES.getConversationsList(`&From=${encodeURIComponent(senderId)}&To=${encodeURIComponent(recipientId)}&PageSize=250`);
                UA_TPA_FEATURES.getConversationsList(`&From=${encodeURIComponent(recipientId)}&To=${encodeURIComponent(senderId)}&PageSize=250`);
            }
            return;
        }
        else{
            if ((!senderId || !recipientId)) {
                return;
            }
        }
    },
    
    fetchMessagesAndAddToChatBox: async function(){
        if(!valueExists(UA_TPA_FEATURES.accountSID)){
            UA_TPA_FEATURES.runAfterSIDResolved.push(()=>{
                UA_TPA_FEATURES.fetchMessagesAndAddToChatBox();
            });
            return true;
        }
        var senderId = "";
        var isWhatsAppChannel = false;
        if(UA_APP_UTILITY.currentSender){
            senderId = UA_APP_UTILITY.currentSender.value;
            isWhatsAppChannel = UA_APP_UTILITY.currentSender.channel === UA_APP_UTILITY.MESSAGING_CHANNELS.WHATSAPP;
            if (isWhatsAppChannel) {
                senderId = "whatsapp:+" + senderId.replace(/\D/g, '');
            }
       }
       var recipientId = "";
       if(UA_APP_UTILITY.chatCurrentRecipient){
            recipientId = UA_APP_UTILITY.chatCurrentRecipient.address;
            if(valueExists(recipientId)){
                recipientId = (isWhatsAppChannel ? "whatsapp:+" : (recipientId.indexOf('+')!==0 ? "+" : '')) +recipientId;
            }
        }
        if ((!senderId || !recipientId)) {
            return;
        }
        /*
        if(UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CONVERSATIONS){
            $("#uaCHATConvListHolder").html("");
            $("#chatbox-message-holder").html("");
            if ((!senderId || !recipientId)) {
                let targetNumber = senderId ? senderId : recipientId;
                UA_TPA_FEATURES.getConversationsList(`&From=${encodeURIComponent(targetNumber)}&PageSize=250`);
                UA_TPA_FEATURES.getConversationsList(`&To=${encodeURIComponent(targetNumber)}&PageSize=250`);
                if (targetNumber.indexOf("whatsapp:") === 0) {
                    targetNumber = targetNumber.replace("whatsapp:", "");
                }
                else {
                    targetNumber = "whatsapp:" + targetNumber;
                }
                UA_TPA_FEATURES.getConversationsList(`&From=${encodeURIComponent(targetNumber)}&PageSize=250`);
                UA_TPA_FEATURES.getConversationsList(`&To=${encodeURIComponent(targetNumber)}&PageSize=250`);
            }
            else{
                UA_TPA_FEATURES.getConversationsList(`&From=${encodeURIComponent(senderId)}&To=${encodeURIComponent(recipientId)}&PageSize=250`);
                UA_TPA_FEATURES.getConversationsList(`&From=${encodeURIComponent(recipientId)}&To=${encodeURIComponent(senderId)}&PageSize=250`);
            }
            return;
        }
        else{
            if ((!senderId || !recipientId)) {
                return;
            }
        }
            */
       var inMessagesAPI = `https://api.twilio.com/2010-04-01/Accounts/${UA_TPA_FEATURES.accountSID}/Messages.json?From=${encodeURIComponent(senderId)}&To=${encodeURIComponent(recipientId)}&PageSize=99`;
       var outMessagesAPI = `https://api.twilio.com/2010-04-01/Accounts/${UA_TPA_FEATURES.accountSID}/Messages.json?From=${encodeURIComponent(recipientId)}&To=${encodeURIComponent(senderId)}&PageSize=99`;
       if(senderId && recipientId){
            UA_APP_UTILITY.CURRENT_CONVERSATION_ID = UA_APP_UTILITY.getCleanStringForHTMLAttribute(senderId+'::'+recipientId);
            UA_APP_UTILITY.initiateRTListeners(senderId+'::'+recipientId);
       }
       $("#chatbox-message-holder").html("");
       UA_TPA_FEATURES.messageDirectionsFetched = 0;
       UA_TPA_FEATURES.LOCAL_MESSAGE_STORAGE = {};
       UA_TPA_FEATURES.fetchMessagesAPIAndAddTemp(inMessagesAPI, false);
       UA_TPA_FEATURES.fetchMessagesAPIAndAddTemp(outMessagesAPI, true);
    },
    
    newRTEventReceived: function(CID, MID){
        if(!UA_APP_UTILITY.FETCHED_MESSAGES[MID.a]){
            UA_TPA_FEATURES.fetchMessageByIDAndAddToChatBox(CID, MID.a);
        }
    },
    runAfterSIDResolved: [],
    fetchMessageByIDAndAddToChatBox: async function(convID, messageID, isNewMessage=false, addToConvBox=false){
        if(!valueExists(UA_TPA_FEATURES.accountSID)){
            UA_TPA_FEATURES.runAfterSIDResolved.push(()=>{
                UA_TPA_FEATURES.fetchMessageByIDAndAddToChatBox(convID, messageID);
            });
            return true;
        }
        var senderId = "";
        var isWhatsAppChannel = false;
        if(UA_APP_UTILITY.currentSender){
            senderId = UA_APP_UTILITY.currentSender.value;
            isWhatsAppChannel = UA_APP_UTILITY.currentSender.channel === UA_APP_UTILITY.MESSAGING_CHANNELS.WHATSAPP;
            if (isWhatsAppChannel) {
                senderId = "whatsapp:+" + senderId.replace(/\D/g, '');
            }
       }
       var recipientId = "";
       if(UA_APP_UTILITY.chatCurrentRecipient){
            recipientId = UA_APP_UTILITY.chatCurrentRecipient.address;
            if(valueExists(recipientId)){
                recipientId = (isWhatsAppChannel ? "whatsapp:+" : "+") +recipientId;
            }
        }
       let currentConvID = UA_APP_UTILITY.getCleanStringForHTMLAttribute(senderId+'::'+recipientId);
       if(convID === currentConvID || (addToConvBox & isNewMessage)){
            var messageAPI = `https://api.twilio.com/2010-04-01/Accounts/${UA_TPA_FEATURES.accountSID}/Messages/${messageID}.json`;
            var resp = await UA_TPA_FEATURES.getAPIResponse(extensionName, messageAPI, "GET");
            if(resp.error){
                console.log('Unable to fetch Twilio message', resp.error.message);
            }
            resp.data.isUnRead = true;
            if(addToConvBox){
                UA_APP_UTILITY.addConversationItemToConversationBox(UA_TPA_FEATURES.convertMsgItemToUAConversationSchema(resp.data));
            }
            UA_APP_UTILITY.addMessageInBox(UA_TPA_FEATURES.convertMessageToUASchema(resp.data), true);
        }
        else{
            console.log('ignoring new message since current conv id is different');
        }
    },
    
    messageDirectionsFetched: 0,
    
    LOCAL_MESSAGE_STORAGE: {},
    
    fetchMessagesAPIAndAddTemp: async function(url, isIncoming=false){
        var resp = await UA_TPA_FEATURES.getAPIResponse(extensionName, url, "GET");
        if(resp.error){
            showErroWindow('Unable to fetch Twilio conversations', resp.error.message);
        }
        let isFindLastRecievedMsg = false;
        await resp.data.messages.forEach(async (item)=>{
            UA_TPA_FEATURES.LOCAL_MESSAGE_STORAGE[new Date(item.date_created).getTime()] = item;
            try{
                if(isFindLastRecievedMsg == false && item.status == "received" && UA_APP_UTILITY.currentSender && UA_APP_UTILITY.currentSender.channel.id && UA_APP_UTILITY.currentSender.channel.id.toLowerCase().indexOf("whatsapp") > -1){
                    isFindLastRecievedMsg = true;
                    let convEndedHours = await UA_APP_UTILITY.diff_hours(new Date(item.date_created), new Date());
                    if(parseInt(convEndedHours) > 24){
                        console.log("Only you can send template messages to this contact.");
                    }
                }
            }
            catch(err){console.log(err);}
        });
        UA_TPA_FEATURES.messageDirectionsFetched++;
        if(UA_TPA_FEATURES.messageDirectionsFetched === 2){
            Object.keys(UA_TPA_FEATURES.LOCAL_MESSAGE_STORAGE).sort().reverse().forEach(item=>{
                UA_APP_UTILITY.addMessageInBox(UA_TPA_FEATURES.convertMessageToUASchema(UA_TPA_FEATURES.LOCAL_MESSAGE_STORAGE[item]), false);
            });
            UA_APP_UTILITY.scrollChatBoxToLatest();
        }

        return resp;
    },
    
    addMsgMediaURLAfterResolving: async function(msgObj){
        if (parseInt(msgObj.num_media) > 0) {
            var mediaAPIResp = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://api.twilio.com" + msgObj.subresource_uris.media, "GET");
            if (mediaAPIResp.error) {
                showErroMessage(mediaAPIResp.error.message);
                return true;
            }
            mediaAPIResp = mediaAPIResp.data;
            if (mediaAPIResp.media_list && mediaAPIResp.media_list.length) {
                msgObj.attachments = [];
                mediaAPIResp.media_list.forEach(function (media) {
                    let url = "https://api.twilio.com" + media.uri.slice(0, -5);
                    msgObj.attachments.push({
                        'url': url,
                        'type': UA_TPA_FEATURES.getTwilioTempMediaType(media.content_type)
                    });
                });
                UA_APP_UTILITY.FETCHED_MESSAGES[msgObj.sid].attachments = msgObj.attachments;
                UA_APP_UTILITY.addMessageInBox(UA_APP_UTILITY.FETCHED_MESSAGES[msgObj.sid], false);
            }
        }
    },
    
    getTwilioTempMediaType : function(content_type){
        if(content_type.indexOf("image") != -1){
            return "image";
        }
        else if(content_type.indexOf("video") != -1){
            return "video";
        }
        else if(content_type.indexOf("audio") != -1){
            return "audio";
        }
        return "file";
    },
    
    convertMessageToUASchema: function(msgObj){
        
        var messageChannel = msgObj.from.indexOf('whatsapp') > -1 ? UA_APP_UTILITY.MESSAGING_CHANNELS.WHATSAPP : UA_APP_UTILITY.MESSAGING_CHANNELS.SMS;
        let originalFrom = msgObj.from;
        let originalTo = msgObj.to;
        msgObj.from = msgObj.from.indexOf('whatsapp') > -1 ? msgObj.from.split(":")[1] : msgObj.from;
        msgObj.to = msgObj.to.indexOf('whatsapp') > -1 ? msgObj.to.split(":")[1] : msgObj.to;
        var attachmentsArr = [];
        if (parseInt(msgObj.num_media) > 0) {
            for(var i=0;i<msgObj.num_media;i++){
                attachmentsArr.push({
                    'type': 'file',
                    'url': '-_-_-_-'
                });
            }
        }
        var modifiedMsgObj = {
            'id': msgObj.sid,
            'text': this.getSafeStringForChat(msgObj.body ? msgObj.body : "") + (msgObj.caption ? msgObj.caption : ''),
            'channel': messageChannel,
            'isIncoming': msgObj.direction === "inbound",
            'from': msgObj.from,
            'to': msgObj.to,
            'createdTime': new Date(msgObj.date_sent?msgObj.date_sent:msgObj.date_created).getTime(),
            'status': msgObj.status,
            'isUnRead': msgObj.isUnRead,
            'attachments': attachmentsArr,
            'conversationID': msgObj.direction === "inbound" ? (originalTo+"::"+originalFrom) : (originalFrom+"::"+originalTo)
        };
        this.addMsgMediaURLAfterResolving(msgObj);
        return modifiedMsgObj;
    },
    
    getSafeStringForChat: function (rawStr) {
        return getSafeString(rawStr);
//        if (!rawStr || rawStr.trim() === ""){
//            return "";
//        }
//
//        let urlRegex = /(https?:\/\/[^\s]+)/g;
//        let phoneNum = /\s*(?:\+?(\d{1,3}))?([- (]*(\d{3})[- )]*)?((\d{3})[- ]*(\d{2,4})(?:[-x ]*(\d+))?)/gm;
//        let urlArr = [];
//
//        return $('<textarea/>').text(rawStr).html().replace(urlRegex, function (url) {
//            urlArr.push(url);
//            return `${ `<a href="${url}" target="_blank" class="${'addChatUrl'}">${url}</a>` }`;
//        }).replace(phoneNum, function (num) {
//            let number = num + "";
//
//            let itIsUrl = false;
//            if (number.length >= 6 && number.length <= 15) {
//                urlArr.forEach(function (url) {
//                    if (url.includes(number)) {
//                        itIsUrl = true;
//                    }
//                });
//
//                if (itIsUrl) {
//                    return num;
//                } else
//                    return ``;
//            } else
//                return num;
//        });
    },
    showIncomingWebhookDialog: function(){
        UA_TPA_FEATURES.refreshWDSendersURL();
        $("#tpa_twilio_service_sender_url button").text("Add Numbers Now");
        $("#error-window-tpa_service_senders").show();
    },
    refreshWDSendersURL: function(){
        $("#tpa_twilio_service_sender_url").attr("href", "https://www.twilio.com/console/sms/services/"+UA_TPA_FEATURES.EXISTING_WEBHOOK_ID+"/senders");
    },
    actualOrgCommonSettingResolved: function(){
        UA_TPA_FEATURES.insertIncomingWD_supportedModules();
        UA_TPA_FEATURES.addNewWebhook();
    },
    loadConvAfterSIDResolved : null,
    addNewConversationByIDInConversationBox: function(cid, mid){
        if(UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CONVERSATIONS){
            UA_TPA_FEATURES.fetchMessageByIDAndAddToChatBox(cid, mid, true, true);
        }
    },
    getConversationsList: async function(apiParams = '&PageSize=250'){
        if(!UA_TPA_FEATURES.accountSID){
            UA_TPA_FEATURES.loadConvAfterSIDResolved = ()=>{
                UA_TPA_FEATURES.getConversationsList(apiParams);
            };
            return;
        }
        UA_APP_UTILITY.LOCAL_FETCHED_CONVERSATION_IDS = [];
        var messageAPI = `https://api.twilio.com/2010-04-01/Accounts/${UA_TPA_FEATURES.accountSID}/Messages.json?`+apiParams;
        var resp = await UA_TPA_FEATURES.getAPIResponse(extensionName, messageAPI, "GET");
        resp.data.messages.forEach(messageItem=>{
            let convItem = UA_TPA_FEATURES.convertMsgItemToUAConversationSchema(messageItem);
            UA_APP_UTILITY.addConversationItemToConversationBox(convItem);
        });
    },
    convertMsgItemToUAConversationSchema: function(messageItem){
        let customerNumber = messageItem.direction === "outbound-api" ? messageItem.to : messageItem.from;
        let conversationID = messageItem.direction === "outbound-api" ? messageItem.from + "::" + messageItem.to : messageItem.to + "::" + messageItem.from;
        customerNumber = customerNumber.replace("whatsapp:", '');
        let channelName = messageItem.from.indexOf("whatsapp") === 0 ? "whatsapp" : "sms";
        //messageItem.from = messageItem.from.replace("whatsapp:", '');
        //messageItem.to = messageItem.to.replace("whatsapp:", '');
        return {
            'id': conversationID,
            'name': customerNumber,
            'status': messageItem.status,
            'channel': channelName === "whatsapp" ? UA_APP_UTILITY.MESSAGING_CHANNELS.WHATSAPP : UA_APP_UTILITY.MESSAGING_CHANNELS.SMS,
            'message': messageItem.body,
            'time': new Date(messageItem.date_sent ? messageItem.date_sent : messageItem.date_created).getTime()
        };
    },
    loadConversationInChatBox: function(convID){
        let customerNumber = convID.split("::")[1];
        let sender = convID.split("::")[0];
        let channel = customerNumber.indexOf("whatsapp") === 0 ? UA_APP_UTILITY.MESSAGING_CHANNELS.WHATSAPP : UA_APP_UTILITY.MESSAGING_CHANNELS.SMS;
        UA_APP_UTILITY.currentSender = {
            'channel': channel,
            'value': sender.replace("whatsapp:+", ''),
            'label': "WhatsApp: "+sender.replace("whatsapp:+", '')
        };
        UA_APP_UTILITY.chatCurrentRecipient = {
            'channel': channel,
            'value': customerNumber.replace("whatsapp:+", ''),
            'address': customerNumber.replace("whatsapp:+", ''),
            'label': "WhatsApp: "+customerNumber.replace("whatsapp:+", '')
        };
        $("#DD_HOLDER_SENDER_LIST .dropdownDisplayText").text(UA_APP_UTILITY.currentSender.label);
        $("#DD_HOLDER_RECIPIENT_LIST .dropdownDisplayText").text(UA_APP_UTILITY.chatCurrentRecipient.label);
        $('.ua-conv-list-item.selected').removeClass('selected');
        $('#CONV-ITEM-'+UA_APP_UTILITY.getCleanStringForHTMLAttribute(convID)).addClass('selected');
        $("#primary-send-btn").text('Send ' + UA_APP_UTILITY.currentSender.channel.label);
        $("#messageAttachmentInputHolder").toggle(UA_APP_UTILITY.currentSender.channel.attachmentSupported);
        if (typeof UA_TPA_FEATURES.messageChannelSelected === "function") {
            UA_TPA_FEATURES.messageChannelSelected();
        }
        $('.messageActionFooter').show();
        UA_APP_UTILITY.refreshMessageBox();
    },
    
    insertIncomingWD_supportedModules: function(){
        UA_APP_UTILITY.insertIncomingWD_supportedModules();
        $("#incoming-channel-config-item").hide();
    },
    
    performConversationSearchOnNumber: function(val, clearConvBox = true){
        if(!valueExists(val)){
            return;
        }
        $("#DD_HOLDER_RECIPIENT_LIST .dropdownDisplayText").text(getSafeString(val));
        UA_APP_UTILITY.chatCurrentRecipient = {
            'channel': UA_APP_UTILITY.MESSAGING_CHANNELS.SMS,
            'address': val,
            'label': val,
            'value': val
        };
        UA_TPA_FEATURES.fetchConversationsAndAddToChatBox(clearConvBox);
    },
    
    fetchConversationsForAllCurrentRecipients: async function(){
        $("#uaCHATConvListHolder").html("");
        UA_APP_UTILITY.getCurrentMessageRecipients(true).splice(0,5).forEach(recipItem=>{
            UA_TPA_FEATURES.performConversationSearchOnNumber(recipItem.number, false);
        });
    },
    
    messageSenderSelected: function(refreshConversationList = true){
        if (UA_APP_UTILITY.isChatView()) {
            UA_APP_UTILITY.refreshMessageBox();
        }
        if (UA_APP_UTILITY.isConvView() && refreshConversationList) {
            UA_APP_UTILITY.refreshConversationBox();
        }
    },
    
    deleteExistingWebhook: async function(){
        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, 'https://messaging.twilio.com/v1/Services/'+UA_TPA_FEATURES.EXISTING_WEBHOOK_ID, "DELETE", {});
        if (UA_TPA_FEATURES.showIfErrorInAPIResult(response)){
            return false;
        }
        UA_TPA_FEATURES.EXISTING_WEBHOOK_ID = null;
        UA_TPA_FEATURES.incomingCaptureStatusChanged();
        return true;
    },
    incomingCaptureStatusChanged: function(){
        if(UA_TPA_FEATURES.EXISTING_WEBHOOK_ID){
            document.getElementById('tpa-switch-incoming-enable-capture').checked = true;
            $('#incoming-channel-display-text').show();
            UA_TPA_FEATURES.refreshWDSendersURL();
        }
        else{
            document.getElementById('tpa-switch-incoming-enable-capture').checked = false;
            $('#incoming-channel-display-text').hide();
        }
    },
    
    fetchOldPipedriveTwilioTemplates: function(){
        if(extensionName === "twilioforpipedrive"){
            try{
                db.collection("zdeskcreds").doc(appsConfig.UA_DESK_ORG_ID).collection(extensionName).doc(appsConfig.APP_UNIQUE_ID).collection('msg-templates').get().
                    then((ques) => {
                        if (!ques.docs.length) {
                            return;
                        }
                        let templatesArray = [];
                        ques.forEach(doc => {
                            let temp = doc.data();
                            UA_APP_UTILITY.fetchedTemplates[doc.id] = {
                                'message': temp.message,
                                'templateType': 'placeholder_template',
                                'tempid': doc.id
                            };
                            templatesArray.push({
                                'label': temp.title,
                                'value': doc.id
                            });
                        });
                        if(templatesArray.length > 0){
                            let approvedTemplateDDId = null;
                            UA_APP_UTILITY.renderSelectableDropdown('#ssf-fitem-template-var-holder', 'Insert Saved template', templatesArray, 'UA_TPA_FEATURES.insertTemplateContentInMessageInput', false, false, approvedTemplateDDId);
                        }

                    });
                }
                catch (ex){console.log(ex);}
            }
            
    },
    
    renderOldTemplatesOfSAAS: async function(){
        try{
            if(extensionName === "twilioforzohocrm"){
                await UA_SAAS_SERVICE_APP.searchRecord(UA_SAAS_SERVICE_APP.APP_API_NAME[extensionName].API_NAME + "WhatsApp_Templates", `(${UA_SAAS_SERVICE_APP.APP_API_NAME[extensionName].API_NAME + "Module_Name"}:equals:${UA_SAAS_SERVICE_APP.widgetContext.module})`).then(function(resp) {           

                    var fetchedTemplatesResponse = resp;
                    if(fetchedTemplatesResponse) {
                        console.log('fetchedtemoktes ', fetchedTemplatesResponse);
                        let fetchedTemplatesCollection = fetchedTemplatesResponse;

                        //UA_APP_UTILITY._feature_showUAMsgTemplates = false;
                        
                        var templatesArray = [];
                        fetchedTemplatesCollection.forEach((item, key)=>{
                            let tempMsgText = item[UA_SAAS_SERVICE_APP.APP_API_NAME[extensionName].API_NAME + "Message"].split(UA_SAAS_SERVICE_APP.widgetContext.module+'.');
                            for(let i=0; i<tempMsgText.length;i++) {
                                if(i!==0) {
                                    tempMsgText[i] = tempMsgText[i].split('}')[0].replaceAll(' ', '_')+"}"+tempMsgText[i].split('}')[1];
                                }
                                else {
                                    tempMsgText[i] = tempMsgText[i]+UA_SAAS_SERVICE_APP.widgetContext.module+'.';
                                }
                            }
                            tempMsgText = tempMsgText.join('');
                            UA_APP_UTILITY.fetchedTemplates[key+''] = {
                                'message': tempMsgText
                            };
                            templatesArray.push({
                                'label': item['Name'],
                                'value': key+''
                            });
                        });
                        
                        var approvedTemplateDDId = UA_APP_UTILITY.renderSelectableDropdown('#ssf-fitem-template-var-holder', `Insert Old ${appPrettyName} template`, templatesArray, 'UA_TPA_FEATURES.insertTemplateContentInMessageInput', false, false);
                    }

                });
            }
        }catch(ex){ console.log(ex); }
    },

    getConvRelatedRecordsFromSaas: async function(convObj){
        try{
            if(!UA_SAAS_SERVICE_APP.searchRelatedRecordsToAllModules || typeof UA_SAAS_SERVICE_APP.searchRelatedRecordsToAllModules !== 'function') return;
            var relatedRocords = [];
            if(extensionName.indexOf("forzohodesk") > -1){                
                relatedRocords = await UA_SAAS_SERVICE_APP.searchRelatedRecordsToAllModules(convObj.id);
            }
            else{
                var to = "";
                if(convObj.id){
                    to = convObj.id.split("::")[1].replace("whatsapp:","");
                }
                if(to && parseInt(to).toString().length > 0 && parseInt(to) != NaN){
                    relatedRocords = await UA_SAAS_SERVICE_APP.searchRelatedRecordsToAllModules(to, "phone");
                }
            }
            return relatedRocords;     
        }
        catch(err){
            console.log(err);
            return [];
        }   
    }

    
};