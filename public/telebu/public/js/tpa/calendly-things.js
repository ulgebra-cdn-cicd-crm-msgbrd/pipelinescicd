class CalendlyLocal {
    _me = {}
    _contact = {}
    _filter = ''
    _upcomingEvents = []
    _pastEvents = []
    _currentEvent = {}
    _eventsInvitee = {}
    _eventTypes = {}
    _members = {}
    _currentMember = null
    _isNoEvents = true
    _eventProfile = null
    _showAllEventsifEmailNotPresent = true
    _fullpageApp = false
    //sett
    set me(data) {
        this._me = data
    }
    isUpcoming(time) {
        if (new Date(time) > new Date())
            return true
        return false
    }
    isEventActive(event) {
        if (event.status === 'active')
            return true
        return false
    }
    set showAllEventsifEmailNotPresent(data) {
        this._showAllEventsifEmailNotPresent = data
    }
    set events(data) {

        this.reset()
        let cancelledPastEvents = [];
        let cancelledUpcomingEvents = [];
        if (this._contact.email || this._showAllEventsifEmailNotPresent) {
            data.collection.reverse().forEach((event) => {
                if (this.isUpcoming(event.end_time)) {
                    if (!this.isEventActive(event)) {
                        cancelledUpcomingEvents.push(event)
                    }
                    this._upcomingEvents.push(event)
                    UI.listUpcomingEvents(event)
                } else {
                    if (!this.isEventActive(event)) {
                        cancelledPastEvents.push(event)
                    }
                    this._pastEvents.push(event)
                    UI.listPastEvents(event)
                }
            });
        } else {
            UI.noEmailAlert()
        }


        UI.eventsCount(
            this._upcomingEvents.length - cancelledUpcomingEvents.length,
            this._pastEvents.length - cancelledPastEvents.length
        )

        this._isNoEvents =
            UI.eventsBetween(
                this._upcomingEvents.length, this._pastEvents.length, //up,past
                this._currentMember?.user || this._me?.resource, //scheduling_url,name
                this._contact?.email //email
            )

        UI.lag()
        UI.refresh(true)

    }
    set currentEvent(data) {
        this._currentEvent = data
        UI.listUpcomingEvents(this._currentEvent.resource)
    }
    set currentEventTitle(data) {
        this._currentEvent = data
        UI.listUpcomingEventsTitle(this._currentEvent.resource)
    }


    set contact(data) {
        this._contact = data
        this._filter = data.email ? this._filter + "&invitee_email=" + encodeURIComponent(data.email) : ''
    }
    set members(data) {
        this._members = data
        UI.loadMembers(this)
    }
    set currentMember(data) {
        this._currentMember = this._members.collection[parseInt(data)]
    }
    set eventsInvitee(data) {
        this._eventsInvitee = data
        this._eventsInvitee.collection.forEach(invitee => {
            UI.listInvitee(invitee, this.isUpcoming(this._currentEvent.resource.end_time))
            UI.lag(false)
        })
    }
    set eventTypes(data) {
        this._eventTypes = data
        // $('#types').html("")
        data.collection.forEach(elm => {
            UI.listEventTypes(elm)
        })
        UA_TPA_FEATURES.singleMeetingLink()
        UI.lag()
    }
    set eventProfile(data) {
        this._eventProfile = data
        $('#peana-event-types-links').html("")
        UI.listEventProfile(data)
    }

    reset() {
        this._pastEvents = []
        this._upcomingEvents = []
        UI.reset()
    }

    //get
    get meUrl() {
        return 'https://api.calendly.com/users/me'
    }
    get listEventsUrl() {
        return (`https://api.calendly.com/scheduled_events?user=${this._me.resource.uri}&organization=${this._me.resource.current_organization}${this._filter}&sort=start_time:desc&count=50`)
    }
    get email() {
        return this._contact.email ? true : false
    }
    get schedulingUrl() {
        return this._currentMember?.user?.scheduling_url || this._me.resource.scheduling_url
    }
    get schedulingData() {
        return this._currentMember?.user || this._me.resource
    }
    get membersUrl() {
        return `https://api.calendly.com/organization_memberships?count=20&organization=${this._me.resource.current_organization}`
    }
    get eventTypesUrl() {
        return `https://api.calendly.com/event_types?user=${this._currentMember?.user?.uri || this._me.resource.uri}`
    }
    get listMemberUrl() {
        let res = null
        try {
            res = (`https://api.calendly.com/scheduled_events?user=${this._currentMember.user.uri}&organization=${this._currentMember.organization}${this._filter}&sort=start_time:desc&count=50`)

        } catch (err) {
            // console.log(err)
        }
        return res
    }
    get isNoEvents() {
        return this._isNoEvents
    }

}
class UI {
    static reset() {
        $('#upcomming').html('')
        $('#past').html('')
        $('#peana-event-types-links').html("")
        $("#upcommingCount").text(0)
        $("#pastCount").text(0)
    }
    static refresh(b) {
        if (!b) {
            $('#refresh>span').addClass('rotate')
            $('.refresh>span').addClass('rotate')

        } else {
            $('#refresh>span').removeClass('rotate')
            $('.refresh>span').removeClass('rotate')
        }
    }
    static event(evnt, className = "events") {
        const { status, name, start_time, end_time, updated_at, created_at, uri } = evnt
        // const uuid = uri.split('/').pop()
        const eventTime = (formatAMPM(start_time) + ' - ' + formatAMPM(end_time)).replaceAll(' ', '&nbsp;')
        return (`
      <div class="${className}" data-uri="${uri}" onclick="UA_TPA_FEATURES.expandEvent(this)">
          <div class='eventStatus' style='color: ${status === 'active' ? '#248750' : 'red'}' >${(status)}</div>
          <div class="field-title">${getSafeString(name)}</div>
          <div class="event-field">
            ${shortDate(start_time)}, ${eventTime}
          </div>
        <div class='createdAt' title="Created at ${getTimeString(created_at)}">${getTimeString(updated_at)}</div>
    </div>
      `)
    }
    static listUpcomingEvents(evnt) {
        $('#upcomming').append(this.event(evnt))
    }
    static listUpcomingEventsTitle(evnt) {
        $('#eventee-title').html(this.event(evnt, "event"))
    }
    static listPastEvents(evnt) {
        $('#past').prepend(this.event(evnt))
    }
    static eventsBetween(upcomungCount, pastCount, resource, email) {
        email = UA_TPA_FEATURES.CURRENT_EVENT_SEARCH_HISTORY_CONTACT_EMAIL;
        const msg = upcomungCount || pastCount ? 'Showing events' : 'No meetings'
        $('#showing').html(` ${msg} for ${!UA_TPA_FEATURES.ALL_AGENT_VIEW_SELECTED ? getSafeString(resource.name).replaceAll(' ', '&nbsp;') : ' all calendly agents '}  ${email ? `& <br/>${getSafeString(email)}` : ''}`)
        return upcomungCount || pastCount ? false : ` ${msg} for ${resource.name}  ${email ? '& ' + email : ''}`
    }
    static lag(con) {
        if (con) {
            $('.lag').show()
            $('#main').hide()
        } else {
            $('.lag').hide()
            $('#main').show()
        }

    }
    static listInvitee(invitee, isUpcoming) {
        const { cancel_url, reschedule_url, status, email, name, timezone, questions_and_answers, cancellation, rescheduled } = invitee
        function eventeeFIeld(title, data) {
            return (
                `<div class="event-field">
            <div class="field-title">${title}</div>
            <div class="field-data">${data}</div>
        </div>`
            )
        }
        function questionsAndAnswers() {
            let temp = ''
            if (questions_and_answers.length)
                questions_and_answers.forEach(qtn => {
                    temp += (
                        `<div class='field-qtn'>${qtn.question}</div>
                <div class="field-ans">${qtn.answer}</div>`
                    )
                })
            else
                temp = 'No question'
            return temp
        }
        function footer() {
            if (status === 'active') {
                if (isUpcoming) {
                    return safeLink('Reschedule Event', reschedule_url, "success") + safeLink('Cancel Event', cancel_url, "danger")
                } else {
                    return 'Past Meeting'
                }
            } else {
                return `Canceled ${rescheduled ? '& rescheduled' : ''}  by ` + cancellation.canceled_by
            }
        }
        $('.invitee').append(`
    <div class="event">
        ${eventeeFIeld('EMAIL', email)}
        ${eventeeFIeld('NAME', name)}
        ${eventeeFIeld('TIME ZONE', timezone)}
        ${eventeeFIeld('Meeting Notes', questionsAndAnswers())}
        ${cancellation?.reason ? eventeeFIeld('Cancellation Reason', cancellation.reason) : ''}
        <div class="field-links">
           ${footer()}      
        </div>
    </div><br/>
`)
    }
    static listEventProfile(elm) {
        const profile = `
        <div class="profile-pan" >
    ${elm.avatar_url ?
                `<img class="avatar-img avatar" src="${elm.avatar_url}" alt="${elm.name} avatar">`
                : '<span class="material-icons avatar">account_circle</span>'
            }
    <div class="profile">
        <div class="profile-name">${elm.name}</div>
        <a href='${elm.scheduling_url}' target='_blank' class="profile-url">${(elm.scheduling_url).replaceAll('https://', '')}</a>
    </div>
</div>`
        $('#peana-event-types-links').append(profile)
    }
    static listEventTypes(elm) {
        const secret = '<span class="material-icons" title="Secret Meeting">visibility_off</span>'
        $('#peana-event-types-links').append(
            `<div class="events-links eventTypes ${!elm.active ? 'disabled' : ''}" 
            style="border: 1px solid ${elm.color};" 
            data-url="${elm.scheduling_url}" data-active="${elm.active}">
            ${elm.active ? '<div class="eventStatus" style="color: #248750">active</div>' : ''}
            <div class="field-title" style="font-size:13px">${elm.name}&nbsp; ${elm.secret ? secret : ''}</div>
            <a target="_blank" href="${elm.scheduling_url}" class="event-field content_copy copy-link"  data-url="${elm.scheduling_url}" style="cursor:pointer;float:left;">
                <span class="material-icons" >open_in_new</span>
                &nbsp;Open
            </a>
            <div class="event-field content_copy single-use-meeting"  data-uri="${elm.uri}" style="cursor:pointer;
              display: inline-flex !important;" onclick="UA_TPA_FEATURES.singleMeetingLink(this)">
                    <span class="material-icons" >content_copy</span>
                    &nbsp;Copy single-use link
                </div>
            <div class='createdAt' title="Created at ${getTimeString(elm.created_at)}">${getTimeString(elm.updated_at)}</div>
        </div>`
        )

    }
    
    static eventsCount(upcomingCount, pastCount) {
        $('#upcommingCount').text(upcomingCount)
        $('#pastCount').text(pastCount)
        if(pastCount === 0){
            $('#ev-show-past-ev-btn-holder button').text("No past meetings").attr('disabled', true);
        }
        else{
            $('#ev-show-past-ev-btn-holder button').text("Show "+pastCount+" past meetings").removeAttr('disabled');
        }
    }
    static calendlyError(err) {
        const link = err.status === 401 ? 'Check<a nofollow noopener noreferrer href="https://calendly.com/integrations/api_webhooks" target="_blank"> here </a>' : ''
        let message = ''
        try {
            message = 'Calendly error: ' + JSON.parse(err.response || err.responseText).message
        } catch (error) {
            console.log(error)
            message = 'Something went wrong. try again.'
        }

        try {
            if (extensionName) {
                message = 'Calendly error: ' + err.error.message
            }
        } catch (error) {
            console.log(error)
            message = 'Something went wrong. try again...'
        }

        $('body').html(
            `<div class="alert alert-danger" role="alert">
        ${message}<br/>${link}
    </div>`)
    }
    static displayError(msg, className) {
        let outputmsg = typeof (msg) === "string" ? msg : "Something went wrong"
        $('body').html(`<div class="alert alert-danger" role="alert">${outputmsg}</div>`)
    }
    static loadMembers(self) {
        const [members, u, fullpageApp] = [self._members, self._me?.resource, self._fullpageApp]

        let option = ''
        members.collection.forEach((v, i) => {
            option += `<option value = "${i}" data-email="${v.user.email}"  ${v.user.uri === u.uri ? 'selected' : ''} >${v.user.name}</option>`
        })
        const select = `<select onchange="UA_TPA_FEATURES.changeMembers(this)"
        style="    
        border: none;
        background-color: inherit;
        border-radius: 0px;
        font-size: inherit;
        width: 150px;
        padding: 2px;
        border-bottom:1px dashed;
        color:#0eca71;
        cursor:pointer;
        padding-right:5px;
        margin: 5px;
        ">${option}</select>`
        let html = ``;
        if (fullpageApp) {
            // $("#refresh").remove()
            html = `<div 
                    style="display: flex;
                    align-items: center;
                    gap: 5px;
                ">
                    <span style="white-space: nowrap;">Events for </span> 
                    ${select} and 
                    <form onsubmit="UA_TPA_FEATURES.changeContactAndRefresh();return false" 
                    style="
                    display: flex;
                    align-items: center;
                    gap: 5px;">
                    <style>
                        input#changeContactInput::placeholder {
                            color: #0eca71;
                        }
                    </style>
                        <input type="email" value="${calendly._contact?.email || ""}" id="changeContactInput"
                        placeholder="All Contacts"
                            style="
                            border:none;
                            border-bottom:1px dashed #0eca71;;
                            color:#0eca71;
                            background-color:inherit;
                            line-height: 0;
                            text-overflow: ellipsis;
                            width:180px;" requered>
                            <button class="btn btn-sm btn-secondary refresh" title="Refresh events" onclick="(self)">
                                <span class="material-icons md">refresh</span>
                            </button>
                    </form>
                    
                </div>`
        }
        else {
            html = select
        }

        $('#members').html(html)
        // setTimeout(setLoggeUserSelectMember, 1000)
    }
    static loadMembersAndFilter(members, u) {
        let option = ''
        members.collection.forEach((v, i) => {
            option += `<option value = "${i}" data-email="${v.user.email}"  ${v.user.uri === u.uri ? 'selected' : ''} >${v.user.name}</option>`
        })
        $('#members').html(`<select>${option}</select>`)
        // setTimeout(setLoggeUserSelectMember, 1000)
    }
    static noEmailAlert() {
        const text = `<div style="text-align:center;color:tomato;font-size:16px;margin-bottom:30px">No email exist to fetch events for</div>`
        $('#upcomming').append(text)
        $('#past').append(text)
    }
    static basicInitElements(){
        /*return ` <div class="lag">
        <img src="https://app.azurna.com/calendly/spinner.svg" alt="loading..." />
    </div>
    <div id='main' style="display: none;">
        <div class="btn-pan">
            <button class=" btn btn-primary btn-sm" id="calendly" title="New Appointment"
                style="display: flex;align-items: center;" onclick="UA_TPA_FEATURES.prepareNewEvent()">
                <span class="material-icons md">add</span>
                <span>New Appointment</span>
            </button>
            
        </div>
        <div id='members' style="margin:auto">
            Loading...
        </div>
    
        
    </div>`*/
    }

}
const calendly = new CalendlyLocal()

var UA_TPA_FEATURES = {
    'clientIds': {
        'calendlyforhubspotcrm' : "e6c60ffe-b50a-4842-8a53-e67d95c94136",
        'calendlyforbitrix24' : "app.618df3863830e4.72087882",
        'calendlyforpipedrive': '1c850596d819c2d9'
    },
    renderInitialElements: async () => {
        UA_TPA_FEATURES.insertIncomingConfigItems();
        $(".pageContentHolder").html("")
        $(".pageContentHolder").append(UI.basicInitElements());
        $("#suo-item-workflow-nav").hide()
        $("#ac_name_label_tpa .anl_servicename").text('Calendly');
        
        $(".incoming-config-tpa-entity").text('events');
        $('#incoming-channel-display-text').text('Choose which event type appointments should be captured in this integration.');
        
        calendly._me = (await UA_TPA_FEATURES._getAboutMe()).data
        if (!calendly?._me?.resource) {
            UA_TPA_FEATURES.showReAuthorizeERROR()
            return;
        }
        $('#ac_name_label_tpa .ac_name_id').text(calendly._me.resource.email);
        UA_TPA_FEATURES.getMyCalendlyEventTypes();
        UA_TPA_FEATURES.setMembers();
        UA_APP_UTILITY.getPersonalWebhookAuthtoken();
        UA_TPA_FEATURES.getMyOrgCalendlyEventTypes();
    },
    showReAuthorizeERROR: function (showCloseOption = false) {
        if ($('#inp_tpa_api_key').length > 0) {
            console.log('already showing reautherr');
            return;
        }
        showErroWindow("Authorization needed!", `You need to authorize your Calendly Account to proceed. <br><br> 
    <div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
        <div style="
            font-size: 24px;
            font-weight: bold;
            margin-top: 20px;
            margin-bottom: 15px;
        ">Calendly Configuration</div>
        <div style="margin-left:20px">
            <div style="font-size: 16px;margin-bottom: 5px;margin-top: 10px;color: rgb(80,80,80);">Calendly Personal Access Token</div>
            <div class="help_apikey_tip"><a target="_blank" href="https://calendly.com/integrations/api_webhooks">Get Here</a></div>
            <input id="inp_tpa_api_key" type="text" placeholder="Enter your personal access token" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
        </div>
        <button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.saveTPAAPIKey(UA_APP_UTILITY.reloadWindow)">Authorize</button>
    </div>`, showCloseOption, false, 500);
        $('#inp_tpa_api_key').focus();
        UA_LIC_UTILITY.showExistingInvitedAdminDDHTML(true);
    },
    showReAuthorizeERRORWithClose: function () {
        UA_TPA_FEATURES.showReAuthorizeERROR();
    },
    saveTPAAPIKey: async function (callback) {
        let authtoken = $("#inp_tpa_api_key").val();
        if (!valueExists(authtoken)) {
            showErroMessage("Please Enter Personal Access Token");
            return;
        }

        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', 'authtoken',
            authtoken, callback);
    },
    _getAboutMe: async function () {
        sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, calendly.meUrl, "GET");
        return sentAPIResponse
    },
    CURRENT_EVENT_SEARCH_HISTORY_CONTACT_EMAIL : null,
    _getEvents: async function (referer, agentsType = null) {
        let url = calendly.listMemberUrl || calendly.listEventsUrl;
        if(UA_TPA_FEATURES.CURRENT_SELECTED_AGENT){
            url = UA_TPA_FEATURES.updateQueryStringParameter(url, 'user', UA_TPA_FEATURES.CURRENT_SELECTED_AGENT.uri);
        }
        if(agentsType=== "allagents"){
            url = UA_TPA_FEATURES.updateQueryStringParameter(url, 'user', null).replace("user=null",'');
        }
        let searchInviteeEmail = $('#inp-penea-cur-invitee-email').val().trim();
        if(searchInviteeEmail){
            if(!calendly._contact || calendly._contact.email !== searchInviteeEmail){
                UA_TPA_FEATURES.CURRENT_EVENT_SEARCH_HISTORY_CONTACT_EMAIL = searchInviteeEmail;
                url = UA_TPA_FEATURES.updateQueryStringParameter(url, 'invitee_email', encodeURIComponent(searchInviteeEmail));
            }
        }
        else if(searchInviteeEmail.trim() === ""){
            url = UA_TPA_FEATURES.updateQueryStringParameter(url, 'invitee_email', null).replace("invitee_email=null",'');
        }
        sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, url, "GET",undefined,referer);
        return (sentAPIResponse)
    },
    updateQueryStringParameter: function (uri, key, value) {
        var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";
        if (uri.match(re)) {
          return uri.replace(re, '$1' + key + "=" + value + '$2');
        }
        else {
          return uri + separator + key + "=" + value;
        }
      },
    _getCurrentMember: async function () {
        sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, calendly.membersUrl, "GET");
        return (sentAPIResponse)
    },
    _getCurrentEvemt: async function (url) {
        sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, url, "GET");
        return (sentAPIResponse)
    },
    _getCurrentInvitee: async function (url) {
        sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, url, "GET");
        return (sentAPIResponse)
    },
    _getCurrentEvemtType: async function (url) {
        sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, url, "GET");
        return (sentAPIResponse)
    },
    _getSchedulingLinks: async function (data) {
        sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://api.calendly.com/scheduling_links", "POST", data);
        return (sentAPIResponse)
    },
    getAPIResponse: async function (extensionName, url, method, data, referer) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = { "Content-Type": "application/json" };
        await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({ appCode: extensionName, TPAService: "calendly", url: url, method: method, data: data, headers: headers, _ua_lic_adminUserID: UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID }).then((response) => {
            /*if (response.data.error?.title) {
                showErroWindow(response.data.error?.title, response.data.error?.message)
                setTimeout(function(){
                    if(referer === "changeMembers"){
                      window.location.reload()
                    }
                    
                },5000)
                
            }*/
            response = response.data;
            returnResponse = response;
            removeTopProgressBar(credAdProcess3);
            return true;
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
        return returnResponse;
    },
    callOnEntityDetailFetched : async function(data){
            let firstEndityId = Object.keys(UA_APP_UTILITY.storedRecipientInventory)[0] 
            let contact = data || UA_APP_UTILITY.storedRecipientInventory[firstEndityId] || { email: null }
            if(contact){
                Object.keys(contact).forEach(key=>{
                    contact[key.toLowerCase()] = contact[key];
                })
            }
            try{
                if(contact.email && (typeof contact.email === "array" || typeof contact.email === "object")){
                    contact.emails = contact.email;
                    let primaryEmail = null;
                    let anyValidEmail = null;
                    contact.emails.forEach(item=>{
                        if(item.primary){
                            primaryEmail = item.value;
                        }
                        if(valueExists(item.value)){
                            anyValidEmail = item.value;
                        }
                    })
                    contact.email = primaryEmail ? primaryEmail : anyValidEmail;
                }
                if(contact.phone && (typeof contact.phone === "array" || typeof contact.phone === "object")){
                    contact.phones = contact.phone;
                    let primaryPhone = null;
                    let anyValidPhone = null;
                    contact.phones.forEach(item=>{
                        if(item.primary){
                            primaryPhone = item.value;
                        }
                        if(valueExists(item.value)){
                            anyValidPhone = item.value;
                        }
                    })
                    contact.phone = primaryPhone ? primaryPhone : anyValidPhone;
                }
            }
            catch(ex){console.log(ex)};
            contact.email = contact.Email || contact.email
            calendly.contact = contact
            $('#inp-penea-cur-invitee-email').val(contact.email);
            UA_TPA_FEATURES.CURRENT_EVENT_SEARCH_HISTORY_CONTACT_EMAIL = contact.email;
            UA_TPA_FEATURES.renderNewMeetingIframeForAgent();
            UA_TPA_FEATURES.loadSeduledEvents()
            try{
                UA_TPA_FEATURES.renderCopyModuleFieldItems("#peanea-copy-fields-list-dropdown-holder");
            }
            catch(err){
                console.log(err);
            }
    },
    MY_FETCHED_CALENDLY_EVENT_TYPES: {},
    MY_ORG_FETCHED_CALENDLY_EVENT_TYPES: {},
    getMyCalendlyEventTypes: async function(){
        let url = calendly.eventTypesUrl;
        let sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, url, "GET");
        sentAPIResponse.data.collection.forEach(item=>{
            let chanId = item.uri.split('/')[item.uri.split('/').length-1];
            UA_TPA_FEATURES.MY_FETCHED_CALENDLY_EVENT_TYPES[chanId] = item;
        });
    },
    IS_NON_ADMIN_ACCOUNT: false,
    getMyOrgCalendlyEventTypes: async function(){
        let sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://api.calendly.com/event_types?organization=${calendly._me.resource.current_organization}`, "GET");
        if(sentAPIResponse.error){
            UA_TPA_FEATURES.IS_NON_ADMIN_ACCOUNT = true;
            return false;
        }
        sentAPIResponse.data.collection.forEach(item=>{
            let chanId = item.uri.split('/')[item.uri.split('/').length-1];
            UA_TPA_FEATURES.MY_ORG_FETCHED_CALENDLY_EVENT_TYPES[chanId] = item;
        });
        UA_TPA_FEATURES.insertIncomingWD_channels();
    },
    actualOrgCommonSettingResolved: function(){
        UA_TPA_FEATURES.insertIncomingWD_supportedModules();
        UA_TPA_FEATURES.addNewWebhook();
    },
    prepareNewEvent: async function () {
        calendly.eventProfile = calendly.schedulingData
        let url = calendly.eventTypesUrl;
        if(UA_TPA_FEATURES.CURRENT_SELECTED_AGENT){
            calendly.eventProfile = UA_TPA_FEATURES.CURRENT_SELECTED_AGENT;
            url = 'https://api.calendly.com/event_types?user='+UA_TPA_FEATURES.CURRENT_SELECTED_AGENT.uri;
        }
        calendly.eventTypes = (await UA_TPA_FEATURES._getCurrentEvemtType(url)).data
    },
    loadSeduledEvents: async function (referer, agentsType = null) {
        calendly.events = (await UA_TPA_FEATURES._getEvents(referer, agentsType)).data
    },
    setMembers: async function () {
        calendly.members = (await UA_TPA_FEATURES._getCurrentMember()).data
        UA_TPA_FEATURES.renderCalendlyMembersInChooseListView();
    },
    changeMembers: async function (e) {
        calendly.currentMember = parseInt(e.value)
        UA_TPA_FEATURES.loadSeduledEvents("changeMembers")
    },
    expandEvent: async function (e) {
        myModel(`<div id='eventee-title'></div>
                    <div class="invitee"></div>
                    <div class="lag">
                            <img src="https://app.azurna.com/calendly/spinner.svg" alt="loading">
                    </div>   
                    </div>
            `)
        let uri = $(e).attr('data-uri')
        calendly.currentEventTitle = (await UA_TPA_FEATURES._getCurrentEvemt(uri)).data
        calendly.eventsInvitee = (await UA_TPA_FEATURES._getCurrentInvitee(uri + '/invitees')).data
    },
    singleMeetingLink: async function (e) {

        const oldE = $(e).html()
        $(e).html(`<span class="material-icons rotate" >sync</span>&nbsp;Fetching..`)
        const eventUrl = $(e).attr("data-uri")

        const body = {
            "max_event_count": 1,
            "owner": eventUrl,
            "owner_type": "EventType"
        }

        try {
            $(".single-use-meeting").html("<span class='custom-btn'>Get single-use link</span>")
            $(".single-use-meeting").click(function () {
                $(this).html(`<span class="material-icons rotate" >sync</span>&nbsp;Fetching..`)
            })
            $(".single-use-meeting").show();
            if (e) {
                let booking_url = (await UA_TPA_FEATURES._getSchedulingLinks(body)).data.resource.booking_url
                e.outerHTML = `<div onclick="prepareToCopyToClipBoard(this)"  class="event-field content_copy copy-link" data-url="${booking_url}" style="cursor:pointer;float:left;">
                                    <span class="material-icons">content_copy</span>
                                    &nbsp;Copy single-use link
                                </div>`
            }

        } catch (err) {
            console.log(err)

        }
    },
    changeContactAndRefresh: function () {
        calendly.contact = { email: userinput, name: "" }
        UA_TPA_FEATURES.loadSeduledEvents()
    },
    FETCHED_EVENT_AGENTS: {},
    renderCalendlyMembersInChooseListView: function(){
        calendly._members.collection.sort((a,b) => a.user.name.localeCompare(b.user.name));
        calendly._members.collection.unshift({
            'user': calendly._me.resource
        });
        calendly._members.collection.forEach(memberItem=>{
            memberItem.user.current_organization = calendly._me.resource.current_organization;
            let memberEmail = memberItem.user.email;
            if(UA_TPA_FEATURES.FETCHED_EVENT_AGENTS.hasOwnProperty(memberEmail)){
                return;
            }
            UA_TPA_FEATURES.cacheUserDetailsIfNotPresentInOrgSetting(memberItem.user);
            UA_TPA_FEATURES.FETCHED_EVENT_AGENTS[memberEmail] = memberItem.user;
            $('.peana-userlist').append(`<div data-agentemail="${memberEmail}" id="peaneai-${UA_APP_UTILITY.getCleanStringForHTMLAttribute(memberEmail)}" onclick="UA_TPA_FEATURES.renderNewMeetingIframeForAgent('${memberEmail}')" class="peana-u-item ${calendly._me.resource.email === memberEmail ? 'selected': ''}">
                            <div class="peanai-pic" style="background-image:url('${memberItem.user.avatar_url}')"></div>
                            <div class="peanai-det">
                                <div class="peanai-top">
                                    ${memberItem.user.name}
                                </div>
                                <div class="peanai-bottom">
                                    ${memberItem.user.email}
                                </div>
                            </div>
                        </div>`);
        });
        UA_TPA_FEATURES.renderUserFieldMappingItems();
        if(calendly._members.collection.length === 2){
            $('.peana-userlist').hide();
            $('.peana-ttl').css('padding-bottom', '10px');
            UA_TPA_FEATURES.renderNewMeetingIframeForAgent()
        }
    },
    cacheUserDetailsIfNotPresentInOrgSetting: function(userItem){
        if(!UA_APP_UTILITY.ACTUAL_ORG_COMMON_SETTINGS){
            UA_APP_UTILITY.CALL_AFTER_ORG_COMMON_SETTING_RESOLVED.push(()=>{UA_TPA_FEATURES.cacheUserDetailsIfNotPresentInOrgSetting(userItem);});
            return;
        }
        let userId = userItem.uri.split('/')[userItem.uri.split('/').length-1];
        UA_APP_UTILITY.addToOrgCacheIfNotExists('calendly_my_org_members_'+userId, userItem);
    },
    CURRENT_SELECTED_AGENT: null,
    ALL_AGENT_VIEW_SELECTED : false,
    renderNewMeetingIframeForAgent: function(agentEmail){
        if(agentEmail){
            if(agentEmail === "allAgents"){
                UA_TPA_FEATURES.ALL_AGENT_VIEW_SELECTED = !UA_TPA_FEATURES.ALL_AGENT_VIEW_SELECTED;
                $('#peaneai-allAgents').toggleClass('selected', UA_TPA_FEATURES.ALL_AGENT_VIEW_SELECTED);
                $('.peana-userlist').toggleClass('allAgents', UA_TPA_FEATURES.ALL_AGENT_VIEW_SELECTED);
                if(UA_TPA_FEATURES.ALL_AGENT_VIEW_SELECTED){
                    UA_TPA_FEATURES.loadSeduledEvents(null, 'allagents');
                }
                else{
                    UA_TPA_FEATURES.loadSeduledEvents();
                }
                return;
            }
            UA_TPA_FEATURES.ALL_AGENT_VIEW_SELECTED = false;
            $('.peana-u-item').removeClass('selected');
            $('.peana-userlist').removeClass('allAgents');
            $('#peaneai-'+UA_APP_UTILITY.getCleanStringForHTMLAttribute(agentEmail)).addClass('selected');
//            $('.peana-meetin-iframe-loader').show();
            $("#peanea-main-new-meeting-iframe").css('opacity', '0.2');
            UA_TPA_FEATURES.CURRENT_SELECTED_AGENT = UA_TPA_FEATURES.FETCHED_EVENT_AGENTS[agentEmail];
        }
        else{
            UA_TPA_FEATURES.CURRENT_SELECTED_AGENT =  calendly._me.resource;
        }
        calendly._me = {
            "resource":UA_TPA_FEATURES.CURRENT_SELECTED_AGENT
        };
        let contact_name = valueExists((calendly._contact.name)) ? encodeURIComponent(calendly._contact.name) : "";
        let contact_email = valueExists((calendly._contact.email)) ? encodeURIComponent(calendly._contact.email) : "";
        let contact_phone = "";
        if(calendly._contact.phone){
            contact_phone = encodeURIComponent(calendly._contact.phone);
        }
        if(calendly._contact.mobile){
            contact_phone = encodeURIComponent(calendly._contact.mobile);
        }
        let iframeurl = (agentEmail ? UA_TPA_FEATURES.FETCHED_EVENT_AGENTS[agentEmail].scheduling_url: calendly._me.resource.scheduling_url)+`?name=${contact_name}&email=${contact_email}&phone_number=${contact_phone}`;
        $("#peanea-main-new-meeting-iframe").attr('src', iframeurl);
        UA_TPA_FEATURES.reAdjustNewBookingIframeView();
        if(UA_APP_UTILITY.CURRENT_EVENT_PAGE_VIEW === "links"){
            UA_TPA_FEATURES.prepareNewEvent();
        }
        if(UA_APP_UTILITY.CURRENT_EVENT_PAGE_VIEW === "history"){
            UA_TPA_FEATURES.loadSeduledEvents();
        }
    },
    reAdjustNewBookingIframeView: function(){
        let iframeLength = $('#peanea-main-new-meeting-iframe').outerWidth();
        if(iframeLength < 650){
            $('#peanea-main-new-meeting-iframe').removeClass('reduceTopCalendlyPadding');
        }
        else{
            $('#peanea-main-new-meeting-iframe').addClass('reduceTopCalendlyPadding');
        }
        if(!calendly._members.collection || calendly._members.collection.length === 2){
            $('.reduceTopCalendlyPadding').css({'margin-top' : '-20px'});
        }
    },
    EXISTING_WEBHOOK_ID: null,
    WEBHOOK_PERMISSION_DENIED: false,
    isWebhookAlreadyExists: async function(){
        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://api.calendly.com/webhook_subscriptions?scope=organization&organization=${calendly._me.resource.current_organization}`, "GET", {});
        if((response.error && response.error.code === 403) || UA_TPA_FEATURES.showIfErrorInAPIResult(response)){
            UA_TPA_FEATURES.WEBHOOK_PERMISSION_DENIED = true;
            UA_TPA_FEATURES.IS_NON_ADMIN_ACCOUNT = true;
            return false;
        }
        let existingServiceID = null;
        response.data.collection.forEach((item)=>{
            let url = item.callback_url;
            if(url && url.startsWith(UA_APP_UTILITY.getWebhookBaseURL())){
                existingServiceID = item.uri;
            }
        });
        UA_TPA_FEATURES.EXISTING_WEBHOOK_ID = existingServiceID;
        UA_TPA_FEATURES.serviceSID = existingServiceID;
        return existingServiceID !== null;
    },
    
    showIfErrorInAPIResult: function(response){
        if((response.code === 401 || response.message === "Authentication failed") || (response.error && response.error.code === 401) || (response.data && (response.data.code === 401 || response.data.message === "Authentication failed"))){
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return true;
        }
        if(response.error && response.error.message){
            showErroWindow('Error from Calendly!', response.error.message);
            return true;
        }
        return false;
    },
    
    addNewWebhook: async function(){
        if(!UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS){
            UA_APP_UTILITY.CALL_AFTER_PERSONAL_COMMON_SETTING_RESOLVED.push(UA_TPA_FEATURES.addNewWebhook);
            return false;
        }
        if(UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.hasOwnProperty('ua_incoming_enable_capture_'+currentUser.uid) && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS['ua_incoming_enable_capture_'+currentUser.uid] === false){
            return;
        }
        if(UA_TPA_FEATURES.IS_NON_ADMIN_ACCOUNT){
            return false;
        }
        if(UA_APP_UTILITY.PERSONAL_WEBHOOK_TOKEN && appsConfig.UA_DESK_ORG_ID){
            var webhookExists = await UA_TPA_FEATURES.isWebhookAlreadyExists();
            if(!webhookExists && !UA_TPA_FEATURES.WEBHOOK_PERMISSION_DENIED){
                var webhookData = {
                            "events":["invitee.created","invitee.canceled"],
                            "scope":"user",
                            "organization": calendly._me.resource.current_organization,
                            "user": calendly._me.resource.uri,
                            "url": UA_APP_UTILITY.getAuthorizedWebhookURL()
                        };
                var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://api.calendly.com/webhook_subscriptions?scope=organization&organization=${calendly._me.resource.current_organization}`, "POST", webhookData);
                if(UA_TPA_FEATURES.showIfErrorInAPIResult(response)){
                    return false;
                }
                UA_TPA_FEATURES.EXISTING_WEBHOOK_ID = response.data.resource.uri;
                UA_TPA_FEATURES.incomingCaptureStatusChanged();
                UA_TPA_FEATURES.showIncomingWebhookDialog();
                return response.data && (response.data.resource.uri !== null);
            }
            UA_TPA_FEATURES.incomingCaptureStatusChanged();
            return true;
        }
        return false;
    },
    incomingCaptureStatusChanged: function(){
        if(UA_TPA_FEATURES.EXISTING_WEBHOOK_ID){
            document.getElementById('tpa-switch-incoming-enable-capture').checked = true;
            $('.tpaichan-devider').fadeIn();
        }
        else{
            document.getElementById('tpa-switch-incoming-enable-capture').checked = false;
            $('.tpaichan-devider:not(#master-tpai-incom-chan-item)').fadeOut();
        }
    },
    deleteExistingWebhook: async function(){
        if(UA_TPA_FEATURES.IS_NON_ADMIN_ACCOUNT){
            return false;
        }
        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, UA_TPA_FEATURES.EXISTING_WEBHOOK_ID, "DELETE", {});
        if (UA_TPA_FEATURES.showIfErrorInAPIResult(response)){
            return false;
        }
        UA_TPA_FEATURES.EXISTING_WEBHOOK_ID = null;
        UA_TPA_FEATURES.incomingCaptureStatusChanged();
        return true;
    },
    pageEventNavItemSelected: function(viewType){
        $("#peaneai-allAgents").hide();
        $('.peana-userlist').removeClass('allAgents');
        if(viewType === "new"){
            if(calendly._members.collection.length === 2){
                UA_TPA_FEATURES.renderNewMeetingIframeForAgent();
            }
            else{
                UA_TPA_FEATURES.reAdjustNewBookingIframeView();
            }
        }
        if(viewType === "links"){
            UA_TPA_FEATURES.prepareNewEvent();
        }
        if(viewType === "history"){
            if(UA_TPA_FEATURES.IS_NON_ADMIN_ACCOUNT){
                $("#peaneai-allAgents").hide();
                UA_TPA_FEATURES.ALL_AGENT_VIEW_SELECTED = false;
                UA_TPA_FEATURES.loadSeduledEvents();
                $('.peana-userlist').toggleClass('allAgents', false);
                return true;
            }
            $("#peaneai-allAgents").show();
            UA_TPA_FEATURES.ALL_AGENT_VIEW_SELECTED = true;
            UA_TPA_FEATURES.insertAllAgentViewIfNotPresent();
            UA_TPA_FEATURES.loadSeduledEvents(null, 'allagents');
            $('#peaneai-allAgents').addClass('selected');
            $('.peana-userlist').toggleClass('allAgents', UA_TPA_FEATURES.ALL_AGENT_VIEW_SELECTED);
        }
    },
    insertAllAgentViewIfNotPresent: function(){
        if($('#peaneai-allAgents').length > 0){
            return false;
        }
        $('.peana-userlist').prepend(`<div data-agentemail="allAgents" id="peaneai-allAgents" onclick="UA_TPA_FEATURES.renderNewMeetingIframeForAgent('allAgents')" class="peana-u-item selected" style="padding-left: 20px;padding-bottom: 8px;">
                            <div class="peanai-det">
                                <div class="peanai-top">
                                    All agents
                                </div>
                            </div>
                        </div>`);
    },
    renderWorkflowBodyCode: function(){
//        UA_TPA_FEATURES.addNewWebhook();
    },
    showIncomingWebhookDialog: function(){
        $("#error-window-tpa_service_senders").show();
        UA_TPA_FEATURES.refreshCalendlyEventPhoneFieldsDataInCache();
    },
    insertIncomingWD_supportedModules: function(){
        UA_APP_UTILITY.insertIncomingWD_supportedModules();
    },
    insertIncomingWD_channels: function(){
        if (!UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS) {
            UA_APP_UTILITY.CALL_AFTER_PERSONAL_COMMON_SETTING_RESOLVED.push(() => {
                UA_TPA_FEATURES.insertIncomingWD_channels();
            });
            return;
        }
        let supportedChannelDD = ``;
        for(var i in UA_TPA_FEATURES.MY_ORG_FETCHED_CALENDLY_EVENT_TYPES){
            let ignored = false;
            let chanItem = UA_TPA_FEATURES.MY_ORG_FETCHED_CALENDLY_EVENT_TYPES[i];
            let chanId = chanItem.uri.split('/')[chanItem.uri.split('/').length-1];
            if (UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.hasOwnProperty('ua_incoming_tpa_ignored_channels')) {
                ignored = UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.ua_incoming_tpa_ignored_channels.includes(chanId);
            }
            supportedChannelDD += `<div class="tpaichan-item"> <label class="switch"><input ${!ignored ? 'checked' : ''} onchange="UA_TPA_FEATURES.toggleIncomingChannelConfig('${chanId}')" type="checkbox" id="tpa-switch-incoming-channel-${chanId}"><div class="slider round"></div></label> <span class="tpaichani-name">${'<a target="_blank" href="'+chanItem.scheduling_url+'"> <b>'+chanItem.name+'</b>'+ ' - '+ chanItem.slug+ '</a> - '+chanItem.duration+' mins'}</span> </div>`;
            UA_FIELD_MAPPPING_UTILITY.renderIncomingFieldMappingChannelOptions(`Event: ${'<b>'+chanItem.name+'</b>'+ ' - '+ chanItem.slug+ ' - '+chanItem.duration+' mins' + ' - ' +chanItem.scheduling_url}`, 'eventtype='+chanId);
        }
        $('#error-window-tpa_service_senders #incoming-channel-config-item').html(supportedChannelDD);
    },
    toggleIncomingChannelConfig: function(chanId){
        if(!UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.ua_incoming_tpa_ignored_channels){
            UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.ua_incoming_tpa_ignored_channels = [];
        }
        if(UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.ua_incoming_tpa_ignored_channels.includes(chanId)){
            UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.ua_incoming_tpa_ignored_channels.remove_by_value(chanId);
        }
        else{
            UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.ua_incoming_tpa_ignored_channels.push(chanId);
        }
        UA_APP_UTILITY.saveSettingInCredDoc('ua_incoming_tpa_ignored_channels', UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.ua_incoming_tpa_ignored_channels);
    },
    refreshCalendlyEventPhoneFieldsDataInCache: async function(){
        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://api.calendly.com/event_types?count=99&organization=${calendly._me.resource.current_organization}`, "GET", {});
        var phoneFields = [];
        response.data.collection.forEach(item=>{
                        item.custom_questions.forEach(questionItem => {
                            if (questionItem.type === "phone_number"){
                                phoneFields.push(questionItem.name);
                            }
                        });
        });
        UA_APP_UTILITY.saveSettingInOrgCommonCredDoc('_ua_app_cache_calendly_phone_fields', phoneFields);
    },
    insertIncomingConfigItems: function(){
        $('#incoming-module-config-item').append(`
            <div class="tpaichan-item" style="display:${extensionName == "calendlyforpipedrive"?"block":"none"}">
                <label class="switch">
                    <input ${extensionName == "calendlyforpipedrive"?"checked":"disabled readonly checked"} onchange="UA_TPA_FEATURES.updateIncomingActivitiesToggle()" type="checkbox" id="tpa-switch-incoming-new-activity-create-disable">
                    <div class="slider round"></div>
                </label> Create activities in ${saasServiceName}
            </div>`);
        UA_APP_UTILITY.CALL_AFTER_PERSONAL_COMMON_SETTING_RESOLVED.push(UA_TPA_FEATURES.setCreateNewEventConfig);
    },

    setCreateNewEventConfig: function(){
        if(UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.hasOwnProperty("ua_app_setting_create_new_incoming_activity")){
            $("#tpa-switch-incoming-new-activity-create-disable").prop("checked", UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.ua_app_setting_create_new_incoming_activity);
        }
    },
    
    updateIncomingActivitiesToggle: async function(){        
        UA_APP_UTILITY.saveSettingInCredDoc('ua_app_setting_create_new_incoming_activity', document.getElementById('tpa-switch-incoming-new-activity-create-disable').checked);
    },

    renderUserFieldMappingItems: function(){
        if(!UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS){
            UA_APP_UTILITY.CALL_AFTER_PERSONAL_COMMON_SETTING_RESOLVED.push(UA_TPA_FEATURES.renderUserFieldMappingItems);
            return;
        }
        if(Object.keys(UA_LIC_UTILITY.SAAS_FETCHED_USERS_IDS_LIST).length === 0){
            UA_LIC_UTILITY.CALL_AFTER_SAAS_USERS_FETCHED.push(UA_TPA_FEATURES.renderUserFieldMappingItems);
            return;
        }
        $('#tpaichan-user-mapping').show();
        var userFieldDDOptions = `<option value="-">Select a user</option>`;
        for(var item in UA_LIC_UTILITY.SAAS_FETCHED_USERS_IDS_LIST){
            let saasAgentItem = UA_LIC_UTILITY.SAAS_FETCHED_USERS_IDS_LIST[item];
            userFieldDDOptions+=`<option value="${saasAgentItem.email}">${saasAgentItem.email}</option>`;
        }
        $('#tpaichan-user-mapping-list-holder').append(`<div id="tfmsf-default" class="tfmsf-row-item">
                                <div class="tfmsf-col tpaichan-field-mapping-saas-fields">
                                    Default
                                </div>  
                                <div class="tfmsf-col tpaichan-field-mapping-saas-divider">
                                    =
                                </div>
                                <div class="tfmsf-col tpaichan-field-mapping-tpa-fields">
                                    <input type="text" disabled readonly value="${UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.email}"/>
                                </div>
                            </div>`);
        for(var item in UA_TPA_FEATURES.FETCHED_EVENT_AGENTS){
            let tpaAgentItem = UA_TPA_FEATURES.FETCHED_EVENT_AGENTS[item];
            let currentSelectedValue = UA_APP_UTILITY.getUserMappingFieldValue(tpaAgentItem.email) ? UA_APP_UTILITY.getUserMappingFieldValue(tpaAgentItem.email) : tpaAgentItem.email;
            let safeEmailElemID = UA_APP_UTILITY.getCleanStringForHTMLAttribute(tpaAgentItem.email);
            $('#tpaichan-user-mapping-list-holder').append(`<div id="tfmsf-${safeEmailElemID}" class="tfmsf-row-item">
                                <input class="inp-tpa-field-mapping-property" type="hidden" value="${tpaAgentItem.email}"/>
                                <div class="tfmsf-col tpaichan-field-mapping-saas-fields">
                                    ${tpaAgentItem.email}
                                </div>  
                                <div class="tfmsf-col tpaichan-field-mapping-saas-divider">
                                    =
                                </div>
                                <div class="tfmsf-col tpaichan-field-mapping-tpa-fields">
                                    <select class="inp-saas-field-mapping-property" onchange="UA_APP_UTILITY.updateUserMappingFieldValue('${tpaAgentItem.email}', '#tfmsf-${safeEmailElemID}')" value="${currentSelectedValue}">
                                        ${userFieldDDOptions}
                                    </select>
                                </div>
                            </div>`);
            $(`#tfmsf-${safeEmailElemID} .tpaichan-field-mapping-tpa-fields select`).val(currentSelectedValue);
        }
    },

    renderCopyModuleFieldItems: async function(target){
        try{
            if(Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS).length > 0){
                let obj = UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS)[0]];
                let dropDownValues = [];
                Object.keys(obj).forEach(item=>{
                    if(typeof obj[item] !== "object"){
                        dropDownValues.push({
                            "label": item,
                            "value": UA_SAAS_SERVICE_APP.widgetContext.module.toUpperCase()+'.'+item
                        });
                    }
                });
                UA_APP_UTILITY.renderSelectableDropdown(target, `Copy ${UA_SAAS_SERVICE_APP.widgetContext.module} fields`, dropDownValues, "UA_APP_UTILITY.copyFieldsSelected", false, true);
            }
        }
        catch(err){
            console.log(err);
        }
    }
};




function fullScreenIframe(url) {
    myModel(`<iframe style="height:100vh;width:75vh;border:none;"  src="${url}"></iframe>`)
}



function safeLink(text, url, clr) {
    return (`<a nofollow noopener noreferrer href="${url}" class="btn btn-${clr} btn-sm active" target="_blank" role="button" aria-pressed="true">${text}</a>    `)
}
function shortDate(str) {
    const date = new Date(str)
    const dateArr = date.toDateString().split(' ')
    const year = parseInt(dateArr.pop())
    const textDate = (dateArr + ',' + year).replaceAll(',', ', ')
    const textDateArr = textDate.split(' ')
    textDateArr[1] = textDateArr[1].replace(',', '')
    return textDateArr.join(' ')
}
function formatAMPM(str) {
    const date = new Date(str)
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}
function getTimeString(previous) {
    previous = new Date(previous);
    var msPerMinute = 60 * 1000;
    var msPerHour = msPerMinute * 60;
    var msPerDay = msPerHour * 24;
    var msPerMonth = msPerDay * 30;
    var msPerYear = msPerDay * 365;
    var elapsed = new Date() - previous;
    if (elapsed < msPerMinute) {
        return Math.round(elapsed / 1000) + ' seconds ago';
    }
    else if (elapsed < msPerHour) {
        return Math.round(elapsed / msPerMinute) + ' minutes ago';
    }
    else if (elapsed < msPerDay) {
        return Math.round(elapsed / msPerHour) + ' hours ago';
    }
    else if (elapsed < msPerMonth) {
        return ' ' + Math.round(elapsed / msPerDay) + ' days ago';
    }
    else if (elapsed < msPerYear) {
        return ' ' + Math.round(elapsed / msPerMonth) + ' months ago';
    }
    else {
        return ' ' + Math.round(elapsed / msPerYear) + ' years ago';
    }
}
function getSafeString(rawStr) {
    if (!rawStr || rawStr.trim() === "") {
        return "";
    }
    return $('<textarea/>').text(rawStr).html();
}
function prepareToCopyToClipBoard(e) {
    const TextToCopy = $(e).attr('data-url')
    if (copyToClipBoard(TextToCopy)) {
        const x = $(e).html()
        $(e).html(`<span class="material-icons" >done</span>&nbsp;Copied`)
        setTimeout(function () {
            $(e).html(x)
        }, 2000)
    }
}


function expandEventType(e) {
    console.log(e)
}



function copyToClipBoard(TextToCopy) {
    const TempText = document.createElement("input");
    TempText.value = TextToCopy;
    document.body.appendChild(TempText);
    TempText.select();
    isCopied = document.execCommand("copy");
    document.body.removeChild(TempText);
    if (isCopied) {
        return true;
    } else {
        document.body.removeChild(TempText);
        console.log('err to copy clipboard')
    }
}




function myModel(data) {
    $('body').append(
        `<div class="newCalendlyEventIframe">
        <button onclick="$('.newCalendlyEventIframe').remove()" style="
            position: absolute;
            right: 15px;
            top: 10px;
            background-color: crimson;
            color: white;
            font-size: 19px !important;
            border-radius: 50px;
            border: none;
            padding: 4px 11px 6px;
            cursor: pointer;
            text-align: center;
            z-index:2;
            ">x
        </button>
        <div style="display: inline-block;margin-top: 45px;" class="myModel">
            <div style="
                padding:10px;
                border-radius:2px;
                min-width:70vw;
                ">
                <div>${data}</div>
            </div>
        </div>
    </div>`
    );
}
