var UA_TPA_FEATURES = {
    
    chosenMessgeTemplate : null,
    
    'clientIds': {
        'lookupforhubspotcrm' : "870ca14e-3b45-4332-9636-32168ff05b12",
        'lookupforbitrix24' : "app.63030c3eef5559.49583025",
        'lookupforpipedrive': '03e34563487a2340'
    },
    
    APP_SAVED_CONFIG : null,
    
    CURRENT_LOOKUP_MODULE: null,
    
    CREATE_MODULE_RECORD_FIELDS: {},
    
    VIEW_MODULE_RECORD_FIELDS: {},
    
    getSupportedChannels: function(){
        return [];
    },
    
    workflowCode : {
        
    },
    
    CALL_AFTER_ACTION_COMPLETE: {},
    
    addRUN_AFTER_ACTION_COMPLETE: function(actionName, callback){
        if(!UA_TPA_FEATURES.CALL_AFTER_ACTION_COMPLETE[actionName]){
            UA_TPA_FEATURES.CALL_AFTER_ACTION_COMPLETE[actionName] = [];
        }
        if(UA_TPA_FEATURES.CALL_AFTER_ACTION_COMPLETE[actionName].includes(callback)){
            console.log('callback already exists');
            return;
        }
        UA_TPA_FEATURES.CALL_AFTER_ACTION_COMPLETE[actionName].push(callback);
    },
    
    callRUN_AFTER_ACTION_COMPLETE: function(actionName){
        if(UA_TPA_FEATURES.CALL_AFTER_ACTION_COMPLETE[actionName]){
            UA_TPA_FEATURES.CALL_AFTER_ACTION_COMPLETE[actionName].forEach(item=>{
                item();
            });
        }
        UA_TPA_FEATURES.CALL_AFTER_ACTION_COMPLETE[actionName] = [];
    },
    
    setCreateModuleRecordFields: function(module, fieldNameArray){
        if(!UA_APP_UTILITY.ORG_COMMON_SETTINGS){
             UA_APP_UTILITY.CALL_AFTER_PERSONAL_COMMON_SETTING_RESOLVED.push(()=>{UA_TPA_FEATURES.setCreateModuleRecordFields(module, fieldNameArray)});
             return false;
        }
        let savedModuleConfigFields = UA_APP_UTILITY.ORG_COMMON_SETTINGS['ua_app_lookup_field_config_'+module.toLowerCase()+'_create'];
        if(savedModuleConfigFields){
            UA_TPA_FEATURES.CREATE_MODULE_RECORD_FIELDS[module] = savedModuleConfigFields;
        }
        else{
            UA_TPA_FEATURES.CREATE_MODULE_RECORD_FIELDS[module] = fieldNameArray;
        }
        UA_TPA_FEATURES.callRUN_AFTER_ACTION_COMPLETE('FETCHED_FIELDS_CREATE_'+module);
    },
    
    allModuleFieldsFetched: function(){
        return Object.keys(UA_TPA_FEATURES.VIEW_MODULE_RECORD_FIELDS).length === Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).length;
    },
    
    setViewModuleRecordFields: function(module, fieldNameArray){
        if(!UA_APP_UTILITY.ORG_COMMON_SETTINGS){
             UA_APP_UTILITY.CALL_AFTER_PERSONAL_COMMON_SETTING_RESOLVED.push(()=>{UA_TPA_FEATURES.setViewModuleRecordFields(module, fieldNameArray)});
             return false;
        }
        let savedModuleConfigFields = UA_APP_UTILITY.ORG_COMMON_SETTINGS['ua_app_lookup_field_config_'+module.toLowerCase()+'_view'];
        if(savedModuleConfigFields){
            UA_TPA_FEATURES.VIEW_MODULE_RECORD_FIELDS[module] = savedModuleConfigFields;
        }
        else{
            UA_TPA_FEATURES.VIEW_MODULE_RECORD_FIELDS[module] = fieldNameArray;
        }
        UA_TPA_FEATURES.callRUN_AFTER_ACTION_COMPLETE('FETCHED_FIELDS_VIEW_'+module);
        UA_TPA_FEATURES.CURRENT_LOOKUP_MODULE = $("#inp-lookup-search-module").val();
        if(UA_TPA_FEATURES.allModuleFieldsFetched()){
            if(queryParams.get("iframeId")){
                UA_TPA_FEATURES.renderNewTempModuleFields();
            }
            UA_TPA_FEATURES.refreshLookUpResultItems(true);
        }
    },
    updateCurrentLookupModule: function(){
        if(!UA_SAAS_SERVICE_APP.LOOKUP_GLOBAL_SEARCH_COMPLETED){
            UA_TPA_FEATURES.refreshLookUpResultItems();
        }
        localStorage.setItem('_ua_app_ui_fields_cache_'+extensionName+'-inp-lookup-search-module', $("#inp-lookup-search-module").val());
    },
    selectModuleNavItem: function(moduleId){
        $('#inp-lookup-search-module').val(moduleId);
        $('.loo-header-nav-item').removeClass('selected');
        $('#ua-loo-navmod-'+moduleId).addClass('selected');
        if(!UA_SAAS_SERVICE_APP.LOOKUP_GLOBAL_SEARCH_COMPLETED){
            $('#ua-loo-navmod-'+moduleId+' .lhni-count').text('..');
        }
        UA_TPA_FEATURES.CURRENT_LOOKUP_MODULE = moduleId;
        UA_TPA_FEATURES.updateCurrentLookupModule();
        $('.loo-results-item-holder-module-holder').hide();
        $(`#loo-item-module-outer-holder-${moduleId}`).show();
        if(UA_SAAS_SERVICE_APP.LOOKUP_GLOBAL_SEARCH_COMPLETED){
            UA_TPA_FEATURES.showAddNewFormIfModuleRecordListEmpty(moduleId);
        }
    },
    
    renderInitialElements: async function(){
        $('.siteName a').text('LookUp '+ saasServiceName);
        $('body').append(`<div class="whatcetra-max-frame-btn" onclick="UA_TPA_FEATURES.maximizeAppIframe()"><span class="material-icons">aspect_ratio</span></div>`);
        $('.signUserInfoHolder').append(`<div onclick="UA_TPA_FEATURES.minimizeAppIframe()" class="wc-top-minimize-btn">-</div>`);
//        $('#saasAuthIDName').hide();
        
        $('.userInfoBtn').css({'right' : '30px'});
        $("#inp-lookup-search-term").val('');
        $("#suo-item-workflow-nav, #suo-item-lic-users-manage-users").hide();
        UA_TPA_FEATURES.fetchAndStoreSAASFields();
        Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).forEach(item=>{
            if(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[item].hideDisplay){
                return;
            }
            $('#inp-lookup-search-module').append(`<option value="${item}">${item.toLowerCase().capitalize()}</option>`);
            $('#loo-results-item-holder-allmodules').append(`
                <div class="loo-results-item-holder-module-holder" id="loo-item-module-outer-holder-${item}">
                    <div style="text-align:right">
                    <div class="lri-btn-create-record" onclick="UA_TPA_FEATURES.showAddNewRecordItem()">
                        <i class="material-icons">add</i>
                        <span>New ${item.endsWith('s') ? item.substr(0, item.length-1).capitalize() : item}</span>
                    </div>
                    </div>
                    <div class="loo-results-item-holder" id="loo-results-item-holder-module-${item}">
                    </div>
                </div>
            `);
            $('.loo-header-nav').append(`
                <div onclick="UA_TPA_FEATURES.selectModuleNavItem('${item}')" class="loo-header-nav-item" id="ua-loo-navmod-${item}">
                    <div class="lhni-label">
                        ${(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[item].displayLabel ? UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[item].displayLabel : item).capitalize()}
                    </div>
                    <div class="lhni-count">
                        ..
                    </div>
                </div>
            `);
        });
        if(valueExists(queryParams.get('lvalue'))){
            $("#inp-lookup-search-term").val(queryParams.get('lvalue').trim());
        }
        $("#ac_name_label_tpa .anl_servicename").text('Ulgebra LookUp');
        $("#ac_name_label_tpa").hide();
        if(localStorage.getItem('_ua_app_ui_fields_cache_'+extensionName+'-inp-lookup-search-module')){
            $('#inp-lookup-search-module').val(localStorage.getItem('_ua_app_ui_fields_cache_'+extensionName+'-inp-lookup-search-module'));
        }
        
    },
    
    fetchAndStoreSAASFields: function(){
        UA_SAAS_SERVICE_APP.retrieveModuleFieldsForLookUp(console.log);
    },
    
    lookUpModuleBeingRefreshedDisplay : function(){
        
        $('.loo-results-item-holder-module-holder').hide();
        $('#loo-item-module-outer-holder-'+UA_TPA_FEATURES.CURRENT_LOOKUP_MODULE).show();
        
        if(true){
            return false;
        }
        //$('.loo-results-item-holder').html('');
        
        UA_TPA_FEATURES.CURRENT_LOOKUP_MODULE = $("#inp-lookup-search-module").val();
        //let moduleSingularValue = (UA_TPA_FEATURES.CURRENT_LOOKUP_MODULE.endsWith('s') ? UA_TPA_FEATURES.CURRENT_LOOKUP_MODULE.substr(0, UA_TPA_FEATURES.CURRENT_LOOKUP_MODULE.length-1): UA_TPA_FEATURES.CURRENT_LOOKUP_MODULE);
        //$('.lri-current-module-disp').text(moduleSingularValue.toLowerCase().capitalize());
        if(!Object.keys(UA_TPA_FEATURES.VIEW_MODULE_RECORD_FIELDS[UA_TPA_FEATURES.CURRENT_LOOKUP_MODULE]).length === 0){
            UA_TPA_FEATURES.addRUN_AFTER_ACTION_COMPLETE('FETCHED_FIELDS_VIEW_'+UA_TPA_FEATURES.CURRENT_LOOKUP_MODULE, refreshLookUpResultItems);
            return false;
        }
            
        //let currentLookupField = $('#inp-lookup-search-field').val();
        //let lookUpSearchTerm = $('#inp-lookup-search-term').val().trim();
        
//        if(!valueExists(lookUpSearchTerm)){
//            return false;
//        }
        
        // $("#loo-result-count").text('Searching...');
        // $('.ua-loo-searchterm-current').text(lookUpSearchTerm);
        // $('.ua-loo-module-current').text(UA_TPA_FEATURES.CURRENT_LOOKUP_MODULE);
        UA_TPA_FEATURES.renderNewTempModuleFields();
    },
    LATEST_LOOK_UP_ID: null,
    lookUpSearchValueUpdated: function(){
        $('.lhni-count').text('..');
        $('.loo-results-item-holder').html('');
        UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS = {};
        UA_TPA_FEATURES.refreshLookUpResultItems(true);
    },
    refreshLookUpResultItems: async function(autoLookupInAllModules = false){
        
        let lookUpSearchTerm = $('#inp-lookup-search-term').val().trim();
        
        if(!valueExists(lookUpSearchTerm)){
            return false;
        }
        
        let currentLookUpID = new Date().getTime();
        UA_TPA_FEATURES.LATEST_LOOK_UP_ID = currentLookUpID;
       
        UA_TPA_FEATURES.lookUpModuleBeingRefreshedDisplay();
        
        UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS = {};
        let looupSearchModules = [];
        if(autoLookupInAllModules){
            Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).forEach(moduleName=>{
                if(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[moduleName].autoLookup){
                    looupSearchModules.push(moduleName);
                }
            });
        }
        let lookedupModules = 0;
        for(var i in looupSearchModules){
            let currentLookUpModule = looupSearchModules[i];
            if(typeof currentLookUpModule === "function"|| !currentLookUpModule){
                return;
            }
            if(Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS).length > 0){
                console.log('existing auto lookup since record exists');
                return;
            }
            if(currentLookUpID !== UA_TPA_FEATURES.LATEST_LOOK_UP_ID){
                console.log('return since new lookup requested');
                return;
            }
            lookedupModules++;
            $('.loo-header-nav-item').removeClass('selected');
            $('#ua-loo-navmod-'+currentLookUpModule).addClass('selected');
            $('#inp-lookup-search-module').val(currentLookUpModule);
            UA_TPA_FEATURES.CURRENT_LOOKUP_MODULE = currentLookUpModule;
            UA_TPA_FEATURES.lookUpModuleBeingRefreshedDisplay();
            let currentLookupField = $('#inp-lookup-search-field').val();
            let lookUpSearchTerm = $('#inp-lookup-search-term').val().trim();
            if(!UA_SAAS_SERVICE_APP.LOOKUP_GLOBAL_SEARCH_COMPLETED){
                $('#ua-loo-navmod-'+currentLookUpModule+' .lhni-count').text('.');
            }
            await UA_SAAS_SERVICE_APP.getSearchedResultItems(lookUpSearchTerm, currentLookUpModule, currentLookupField);
            if(currentLookUpID !== UA_TPA_FEATURES.LATEST_LOOK_UP_ID){
                console.log('return since new lookup requested');
                return;
            }
            let isLastLookUp = lookedupModules === looupSearchModules.length;
            if(!UA_SAAS_SERVICE_APP.LOOKUP_GLOBAL_SEARCH_COMPLETED){
                $('#ua-loo-navmod-'+currentLookUpModule+' .lhni-count').text(Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS).length);
            }
            if(Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS).length === 0){
                if(isLastLookUp && !UA_SAAS_SERVICE_APP.LOOKUP_GLOBAL_SEARCH_COMPLETED){
                    UA_TPA_FEATURES.showAddNewRecordItem();
                }
            }
            for(var i=0; i < Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS).length; i++){
                var recordId = Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS)[i];
                UA_TPA_FEATURES.renderModuleRecordItem(recordId);
            }
            if(UA_SAAS_SERVICE_APP.LOOKUP_GLOBAL_SEARCH_COMPLETED){
                UA_TPA_FEATURES.showAddNewFormIfModuleRecordListEmpty(currentLookUpModule);
            }
            $("#loo-result-count").text(Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS).length);
        }

    },
    
    showAddNewFormIfModuleRecordListEmpty: function(module){
        if($('#loo-results-item-holder-module-'+module+' .loo-result-item:not(.ua-loo-new-record-item)').length === 0){
            UA_TPA_FEATURES.showAddNewRecordItem();
        }
        else{
            $('#UA-LOO-REC-___UA_NEW_'+module).hide();
        }
    },
    
    renderModuleRecordItem: function(recordId, renderAtTop = false){
        try{
            let recordItem = UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordId];
            if(!recordItem.__RecordModuleID){
                recordItem.__RecordModuleID = UA_TPA_FEATURES.CURRENT_LOOKUP_MODULE;
            }
            recordItem._uaProps = {};
            UA_TPA_FEATURES.renderBasicRecordDetail(recordItem, renderAtTop);
            UA_TPA_FEATURES.VIEW_MODULE_RECORD_FIELDS[recordItem.__RecordModuleID].forEach(fieldName=>{
                UA_TPA_FEATURES.renderFieldNow(recordItem, fieldName);
            });
            let hiddenFieldsCount = Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[recordItem.__RecordModuleID].fields).length - UA_TPA_FEATURES.VIEW_MODULE_RECORD_FIELDS[recordItem.__RecordModuleID].length;
            UA_TPA_FEATURES.addRecordListItemShowHiddenFieldsActionBtn(recordItem.__RecordModuleID, recordId, hiddenFieldsCount);
            if(recordItem.minimizeOnDefault){
                 $(`#UA-LOO-REC-${recordId} .minimize-on-item-minimize`).hide();
            }
        }
        catch(ex){
            console.log(ex);
        }
    },
    
    showHiddenFieldsForRecordItem: function(module, recordId){
        $('#UA-LOO-REC-'+recordId+' .btn-ua-loo-hidden-fields').hide();
        if(recordId.startsWith('___UA_NEW_')){
            Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[module].fields).forEach(objectFieldName => {
                if(!UA_TPA_FEATURES.CREATE_MODULE_RECORD_FIELDS[module].includes(objectFieldName)){
                    UA_TPA_FEATURES.renderFieldNow({'id': recordId, __RecordModuleID: module}, objectFieldName, true);
                }
            });
        }
        else{
            let recordItem = UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordId];
            Object.keys(recordItem).forEach(objectFieldName => {
                if(!UA_TPA_FEATURES.VIEW_MODULE_RECORD_FIELDS[UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordId].__RecordModuleID].includes(objectFieldName)){
                    UA_TPA_FEATURES.renderFieldNow(recordItem, objectFieldName);
                }
            });
        }
    },
    
    appsConfigHasBeenResolved: function(){
        
    },
    
    renderWorkflowBodyCode :function(accessToken){
        
    },
    
    getSafeStringForChat: function (rawStr) {
        return getSafeString(rawStr);
    },
    showIncomingWebhookDialog: function(){
        
    },
    actualOrgCommonSettingResolved: function(){
       
    },
    fieldInputTypeMap: {
        "website": "url",
        "email" : "email",
        "phone": "tel",
        "integer": "number",
        "currency": "number",
        "datetime": "datetime-local",
        "double" : "number",
        "textarea": "textarea",
        "address": "textarea",
        "int": "number"
    },
    LIST_VIEW_EXPANDED: true,
    toggleRecordItemListView:function(recordId){
        if(!recordId){
            UA_TPA_FEATURES.LIST_VIEW_EXPANDED = !UA_TPA_FEATURES.LIST_VIEW_EXPANDED;
            if(UA_TPA_FEATURES.LIST_VIEW_EXPANDED){
                $(`.maxnimize-on-item-maxnimize`).show();
            }
            else{
                $(`.minimize-on-item-minimize`).hide();
            }
            return;
        }
        if($(`#UA-LOO-REC-${recordId} .lri-fields-list`).css('display') === 'none'){
            $(`#UA-LOO-REC-${recordId} .maxnimize-on-item-maxnimize`).show();
        }
        else{
            $(`#UA-LOO-REC-${recordId} .minimize-on-item-minimize`).hide();
        }
        if(recordId){
            if($(`#UA-LOO-REC-${recordId} .lri-fields-list.minimize-on-item-minimize`).css('display') !== "none"){
                if(typeof UA_SAAS_SERVICE_APP.callbackOnMaximizeModuleRecordItem === "function"){
                    UA_SAAS_SERVICE_APP.callbackOnMaximizeModuleRecordItem(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordId].__RecordModuleID, recordId);
                }
            }
        }
    },
    showAddNewRecordItem: function(module = null, parentRecordId = null){
        if(!module){
            module = UA_TPA_FEATURES.CURRENT_LOOKUP_MODULE;
            UA_TPA_FEATURES.CURRENT_LOOKUP_MODULE = $("#inp-lookup-search-module").val();
        }
        if(Object.keys(UA_TPA_FEATURES.CREATE_MODULE_RECORD_FIELDS[module]) === 0){
            return false;
        }
        if($('#UA-LOO-REC-___UA_NEW_'+module).length > 0){
            $('#UA-LOO-REC-___UA_NEW_'+module).show();
            return;
        }
        let moduleSingularValue = (module.endsWith('s') ? module.substr(0, module.length-1): module);
        if(Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[module].fields).length === 0){
            return;
        }
        UA_TPA_FEATURES.renderBasicRecordDetail({
            'name': 'Create New '+ moduleSingularValue,
            'id': '___UA_NEW_'+module,
            '_uaProps': {
                'record_class': 'ua-loo-new-record-item'
            },
            'webURL': UA_SAAS_SERVICE_APP.getNewRecordInSAASWebURL(module),
            '__RecordModuleID': module
        });
        Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[module].fields).forEach(fieldName=>{
            if(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[module].fields[fieldName].system_mandatory && !UA_TPA_FEATURES.CREATE_MODULE_RECORD_FIELDS[module].includes(fieldName)){
                UA_TPA_FEATURES.CREATE_MODULE_RECORD_FIELDS[module].push(fieldName);
            }
        });
        UA_TPA_FEATURES.CREATE_MODULE_RECORD_FIELDS[module].forEach(fieldName=>{
            let fieldConfig = UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[module].fields[fieldName];
            if(!fieldConfig || fieldConfig.field_read_only){
                return;
            }
            UA_TPA_FEATURES.renderFieldNow({'id': '___UA_NEW_'+module, __RecordModuleID: module}, fieldName, true);
        });
        let hiddenFieldsCount = Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[module].fields).length - UA_TPA_FEATURES.CREATE_MODULE_RECORD_FIELDS[module].length;
        UA_TPA_FEATURES.addRecordListItemShowHiddenFieldsActionBtn(module, '___UA_NEW_'+module, hiddenFieldsCount);
        $('#UA-LOO-REC-___UA_NEW_'+module).append(`
            <div class="ua-loo-new-rci-actions minimize-on-item-minimize">
                <div class="ua-loo-new-rci-action-item create-submit-button" onclick="UA_TPA_FEATURES.createRecordInModule('${module}', '${parentRecordId}')">
                    Create New ${moduleSingularValue.capitalize()+ " in "+ saasServiceName}
                </div>
                <div class="ua-loo-new-rci-action-item create-cancel-button" onclick="$('#UA-LOO-REC-___UA_NEW_${module}, .loo-new-item-popupwindow').remove()">
                    Cancel
                </div>
            </div>
        `);
        window.location.hash = '#ua-loo-result-actionholdr';
    },
    addRecordListItemShowHiddenFieldsActionBtn: function(module, recordId, hiddenFieldsCount){
        if(hiddenFieldsCount > 0){
            $('#UA-LOO-REC-'+recordId+' .lri-fields-act-list').append(`<div onclick="UA_TPA_FEATURES.showHiddenFieldsForRecordItem('${module}', '${recordId}')" class="ua-loo-rci-field-act-item btn-ua-loo-hidden-fields">Show ${hiddenFieldsCount} hidden fields</div>`);
        }
        $('#UA-LOO-REC-'+recordId+' .lri-fields-act-list').append(`<div onclick="UA_TPA_FEATURES.openConfigureFieldsWindow('${module}', '${recordId.startsWith('___UA_NEW_') ? 'CREATE':'VIEW'}')" class="ua-loo-rci-field-act-item">Configure display fields</div>`);
    },
    openConfigureFieldsWindow:function(module, action){
        $('#loo-fields-config-window').attr({'data-config-field-module': module, 'data-config-field-action': action}).show();
        UA_TPA_FEATURES.renderFieldConfigurationInWindow(module, action);
    },
    configFieldElemDragEnter: function(event){
        event.preventDefault();
        if(!event.originalTarget){
            event.originalTarget = event.currentTarget;
        }
        if(!$(event.originalTarget).hasClass('dragoverwaiting')){
            $(event.originalTarget).addClass('dragoverwaiting');
        }
    },
    configFieldElemDragLeave: function(event){
        if(!event.originalTarget){
            event.originalTarget = event.currentTarget;
        }
        $(event.originalTarget).removeClass('dragoverwaiting');
    },
    configFieldElemDragDropped: function(event){
        event.preventDefault();
        if(!event.originalTarget){
            event.originalTarget = event.currentTarget;
        }
        $('.lfct-item.dragoverwaiting').removeClass('dragoverwaiting');
        let targetItemId = event.dataTransfer.getData('text/plain');
        if(event.originalTarget.id === targetItemId){
            return;
        }
        let htmlToMoveElem = $('#'+targetItemId)[0].outerHTML;
        $('#'+targetItemId).remove();
        $(event.originalTarget).before(htmlToMoveElem);
        UA_TPA_FEATURES.addEventListenersForConfigFieldItem(document.getElementById(targetItemId));
        let module = $('#loo-fields-config-window').attr('data-config-field-module');
        let action = $('#loo-fields-config-window').attr('data-config-field-action');
        let selectedFields = UA_TPA_FEATURES.getUpdatedFieldsListInConfigWindow();
        if(action === "VIEW"){
            if(JSON.stringify(UA_TPA_FEATURES.VIEW_MODULE_RECORD_FIELDS[module]) === JSON.stringify(selectedFields)){
                console.log('existing fields are same, no update needed');
                return;
            }
            UA_TPA_FEATURES.VIEW_MODULE_RECORD_FIELDS[module] = selectedFields;
        }
        if(action === "CREATE"){
            if(JSON.stringify(UA_TPA_FEATURES.VIEW_MODULE_RECORD_FIELDS[module]) === JSON.stringify(selectedFields)){
                console.log('existing fields are same, no update needed');
                return;
            }
            UA_TPA_FEATURES.CREATE_MODULE_RECORD_FIELDS[module] = selectedFields;
        }
        UA_APP_UTILITY.saveSettingInCredDoc('ua_app_lookup_field_config_'+module.toLowerCase()+'_'+action.toLowerCase(), selectedFields);
    },
    configFieldElemDragOver: function(event){
        event.preventDefault();
    },
    configFieldElemDragStart: function(event){
        if(!event.originalTarget){
            event.originalTarget = event.currentTarget;
        }
        event.dataTransfer.setData('text/plain', event.originalTarget.id);
    },
    filterListResultFields:function(ev){
        var val = ev.value.toLowerCase();
        if(val !== ""){
            document.querySelectorAll('.fieldItemHolder').forEach(item=>{
                if(item.getAttribute('data-fieldname').toLowerCase().indexOf(val) < 0){
                    item.style.display = 'none';
                }else{
                    item.style.display = 'block';
                }
            });
        }else{
            $('.tblfldhide').show();
            $('.fieldItemHolder').show();
        }
    },
    
    getUpdatedFieldsListInConfigWindow: function(){
        let updatedFieldsList = [];
        let showFieldElems = $('#loo-fields-config-table-col-show .lfct-item');
        showFieldElems.each(function(i){
            let item = $(this);
            if(item.hasClass('lfct-drop-space-receiver')){
                return false;
            }
            let fieldName = item.attr('data-fieldname');
            if(valueExists(fieldName) && !updatedFieldsList.includes(fieldName)){
                updatedFieldsList.push(fieldName);
            }
        });
        return updatedFieldsList;
    },
    
    renderFieldConfigurationInWindow: function(module, action){
        $('#loo-fields-config-window .error-window-subtitle').text('for '+(action === "CREATE" ? " Create New " : " Viewing ")+module.capitalize());
        $('.loo-fields-config-table-fields-holder').html('');
        let selectedFieldsList = [];
        if(action === "VIEW"){
            selectedFieldsList = UA_TPA_FEATURES.VIEW_MODULE_RECORD_FIELDS[module];
        }
        if(action === "CREATE"){
            selectedFieldsList = UA_TPA_FEATURES.CREATE_MODULE_RECORD_FIELDS[module];
        }
        selectedFieldsList.forEach(fieldName=>{
            try{
               let fieldConfig = UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[module].fields[fieldName];
               if(action === "CREATE"){
                   if(!fieldConfig || fieldConfig.field_read_only){
                       return;
                   }
               }
               if(!fieldConfig){
                   fieldConfig = {
                       'api_name': fieldName,
                       'display_label': fieldName
                   }
               }
               UA_TPA_FEATURES.renderConfigFieldInWindowCol('#loo-fields-config-table-col-show', fieldName, fieldConfig.display_label);
            }
            catch(ex){console.log(ex);}
        });
        let hiddenFieldsArray = [];
        Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[module].fields).forEach(fieldName=>{
            try{
                let fieldConfig = UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[module].fields[fieldName];
                if(action === "CREATE"){
                   if(!fieldConfig || fieldConfig.field_read_only){
                       return;
                   }
               }
                if(!selectedFieldsList.includes(fieldName)){
                    hiddenFieldsArray.push(fieldName);
                }
            }
            catch(ex){console.log(ex);}
        });
        hiddenFieldsArray.sort().forEach(fieldName=>{
            let fieldConfig = UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[module].fields[fieldName];
            UA_TPA_FEATURES.renderConfigFieldInWindowCol('#loo-fields-config-table-col-hide', fieldName, fieldConfig.display_label);
        });
        
        $('#loo-fields-config-table-col-hide .loo-fields-config-table-fields-holder .lfct-item').sort(function(a, b) {
            return $(b).attr('data-fieldlabel') < ($(a).attr('data-fieldlabel')) ? 1 : -1;
        }).appendTo('#loo-fields-config-table-col-hide .loo-fields-config-table-fields-holder');
        
        document.querySelectorAll('.lfct-item').forEach(listItem=>{
            UA_TPA_FEATURES.addEventListenersForConfigFieldItem(listItem);
        });
         $('.loo-fields-config-table-col').scrollTop(0);
    },
    
    renderConfigFieldInWindowCol: function(target, fieldName, fieldLabel){
        $(target+' .loo-fields-config-table-fields-holder').append(`<div id="ulfc-item-${fieldName}" data-fieldname="${fieldName}" data-fieldlabel="${UA_APP_UTILITY.getCleanStringForHTMLAttribute(fieldLabel)}" draggable="true" class="lfct-item">
                        ${fieldLabel}
        </div>`);
    },
    
    addEventListenersForConfigFieldItem: function(listItem){
        listItem.addEventListener('dragenter', UA_TPA_FEATURES.configFieldElemDragEnter);
            listItem.addEventListener('dragover', UA_TPA_FEATURES.configFieldElemDragOver);
            listItem.addEventListener('drop', UA_TPA_FEATURES.configFieldElemDragDropped);
            listItem.addEventListener('dragleave', UA_TPA_FEATURES.configFieldElemDragLeave);
            listItem.addEventListener('dragstart', UA_TPA_FEATURES.configFieldElemDragStart);
    },
    
    fetchAndRenderRecordNotes: async function(module, recordId){
        if($('#UA-LOO-REC-'+recordId+' .lri-notes-container').css('display') === 'block'){
            $('#UA-LOO-REC-'+recordId+' .lri-notes-container').slideUp();
            return;
        }
        $('#UA-LOO-REC-'+recordId+' .lri-notes-container').slideDown();
        $('#UA-LOO-REC-'+recordId+' .lri-notesHistoryHolder').html('Fetching notes from '+saasServiceName+'...');
        let notesArray = await UA_SAAS_SERVICE_APP.getNotesOfRecord(module, recordId);
        $('#UA-LOO-REC-'+recordId+' .lri-notesHistoryHolder').html('');
        notesArray.forEach(noteItem=>{
           $('#UA-LOO-REC-'+recordId+' .lri-notesHistoryHolder').append(`<div class='note-item'>
                            <div class='note-item-ttl'>${getSafeString(noteItem.title ? noteItem.title : "")}</div>
                            <div class='note-item-cont'>${getSafeString(noteItem.content ? noteItem.content : "")}</div>
                            <div class='note-item-time' title="${new Date(noteItem.time).toString()}">${getTimeString(noteItem.time)}</div>
                        </div>`);});
    },
    
    addModuleRecordNote: async function(module, recordId){
        let noteContent = $('#UA-LOO-REC-'+recordId+' .note-content-inp').val().trim();
        if(!valueExists(noteContent)){
            return false;
        }
        $('#UA-LOO-REC-'+recordId+' .save-note-btn').text('Saving note...').attr('disabled',true).css('color', 'white');
        let addedNoteResponse = await UA_SAAS_SERVICE_APP.addModuleRecordNote(module, recordId, noteContent);
        $('#UA-LOO-REC-'+recordId+' .save-note-btn').text('Save note').removeAttr('disabled');
        if(!addedNoteResponse.error){
            $('#UA-LOO-REC-'+recordId+' .note-content-inp').val('');
            $('#UA-LOO-REC-'+recordId+' .lri-notesHistoryHolder').html('Note added Successfully..');
            $('#UA-LOO-REC-'+recordId+' .lri-notes-container').css('display', 'none');
            $('#UA-LOO-REC-'+recordId+' .lri-addNote-Holder').slideUp();
            UA_TPA_FEATURES.fetchAndRenderRecordNotes(module, recordId);
        }
    },
    
    renderBasicRecordDetail: function(contactItem, renderAtTop){
        if($(`#UA-LOO-REC-${contactItem.id}`).length > 0){
            console.log('element already exists');
            return false;
        }
        if(contactItem.__RecordModuleID && !contactItem.id.startsWith('___UA_NEW_') && UA_SAAS_SERVICE_APP.LOOKUP_GLOBAL_SEARCH_COMPLETED){
            let newCount = 1;
            let elemCount = parseInt($(`#ua-loo-navmod-${contactItem.__RecordModuleID} .lhni-count`).text());
            if(elemCount){
                newCount = ++elemCount;
            }
            $(`#ua-loo-navmod-${contactItem.__RecordModuleID} .lhni-count`).text(newCount);
        }
        let labelsHTML = '';
        if(contactItem.__labels){
            contactItem.__labels.forEach(labelItem=>{
                labelsHTML+=`<div class="lri-label-item" ${labelItem.color ? 'style="background-color:'+labelItem.color+'"': ''}>${labelItem.label}</div>`;
            });
        }
        let resultHTML = `
                    <div class="loo-result-item ${contactItem._uaProps.record_class ? contactItem._uaProps.record_class : ''}" id="UA-LOO-REC-${contactItem.id}">
                        <div class="lri-ri-min" onclick="UA_TPA_FEATURES.toggleRecordItemListView('${contactItem.id}')"><i class="material-icons">aspect_ratio</i></div>
                        <div class="lri-ttl">
                             <a href="${contactItem.webURL}" target="_blank">${getSafeString(contactItem.name)}</a>
                        </div>
                        <div class="lri-labels-holder">${labelsHTML}</div>
                        <div class="lri-actions">
                            <a href="${contactItem.webURL}" target="_blank">
                                <div class="lri-act-item lriai-link">
                                    <span class="saasServiceName">${saasServiceName}</span> <i class="material-icons">open_in_new</i>
                                </div>
                            </a>
                            <div class="lri-act-item lriai-note" onclick="UA_TPA_FEATURES.fetchAndRenderRecordNotes('${contactItem.__RecordModuleID}', '${contactItem.id}')">
                                 Notes <i class="material-icons">sticky_note_2</i>
                            </div>
                            <div style="display:none" class="lri-act-item lriai-export-chat" onclick="UA_TPA_FEATURES.fetchAndRenderWAChatMessagesForNote('${contactItem.__RecordModuleID}', '${contactItem.id}')">
                                 Export Chat <i class="material-icons">format_list_bulleted_add</i>
                            </div>
                        </div>
                        <div class="lri_relatedModulesHolder"></div>
                        <div id="lri_${contactItem.id}_noteHolder" class='lri-notes-container minimize-on-item-minimize'>
                                <div class='lri-note-ttl'> <span class='material-icons'> sticky_note_2 </span> Notes <button onclick="$('#UA-LOO-REC-${contactItem.id} .lri-addNote-Holder').slideDown()" class='add-note-button'> + Add Note</button> </div>
                                <div style='display:none' class='lri-addNote-Holder'>
                                  <div class='note-inp-holder'>
                                    <textarea class='note-content-inp' placeholder='Add a note' type='text'></textarea>
                                  </div>
                                  <button onclick="UA_TPA_FEATURES.addModuleRecordNote('${contactItem.__RecordModuleID}','${contactItem.id}')" class='save-note-btn'>Save Note</button>
                                  <button onclick="$('#UA-LOO-REC-${contactItem.id} .lri-addNote-Holder').hide()" class='cancel-note-btn'>Cancel</button><br><br><br>
                                </div>
                                <div class="lri-notesHistoryHolder">
                                </div>
                        </div>
                        <div class="lri-fields-list minimize-on-item-minimize maxnimize-on-item-maxnimize" >
                            
                        </div>
                        <div class="lri-fields-act-list minimize-on-item-minimize maxnimize-on-item-maxnimize">
                            
                        </div>
                    </div>
        `;
        let renderTarget = '#loo-results-item-holder-module-'+contactItem.__RecordModuleID;
        if(contactItem.__isRelatedModuleItem){
            renderTarget = `#lri_${contactItem.__RelatedRecordID}_relatesITEM_${contactItem.__RecordModuleID}_Holder .lri-realtedItemsHistoryHolder`;
        }
        if(contactItem.id.startsWith('___UA_NEW_') || renderAtTop){
            if(UA_TPA_FEATURES.CURRENT_LOOKUP_MODULE !== contactItem.__RecordModuleID){
                renderTarget = `.pageLookUpPageHolder`;
                resultHTML = `<div class="windowOuterHolder loo-new-item-popupwindow"> <div class="windowInner">`+resultHTML+`</div> </div>`;
            }
            $(renderTarget).prepend(resultHTML);
        }
        else{
            $(renderTarget).append(resultHTML);
        }
        if(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[contactItem.__RecordModuleID].relatedModules){
            UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[contactItem.__RecordModuleID].relatedModules.forEach(relatedModuleItem=>{
                $(`#UA-LOO-REC-${contactItem.id} .lri-actions`).append(`
                            <div class="lri-act-item lriai-related-module" onclick="UA_TPA_FEATURES.fetchAndRenderRecordRelatedModule('${contactItem.__RecordModuleID}', '${relatedModuleItem}' , '${contactItem.id}')">
                                 ${relatedModuleItem.capitalize()} <i class="material-icons">expand_more</i>
                            </div>`);
            });
        }
        if(queryParams.get("iframeId")){
            $(`#UA-LOO-REC-${contactItem.id} .lri-actions`).append(`<div class="lriai-temp ${contactItem.id}" style="display: inline-block;margin-left: 10px;"></div>`);
            UA_APP_UTILITY.renderSavedTemplatesInDropdowns(`.lriai-temp.${contactItem.id}@${contactItem.id}`);
        }
        if(contactItem.__isRelatedModuleItem){
            $(`#UA-LOO-REC-${contactItem.id} .lriai-note`).remove();
            $(`#UA-LOO-REC-${contactItem.id} .lri_relatedModulesHolder`).remove();
            $(`#UA-LOO-REC-${contactItem.id} .lri-notes-container`).remove();
        }
        try{
            if(UA_TPA_FEATURES.CURRENT_WA_OWNER_NUMBER){
                $('.lriai-export-chat').css('display', 'inline-block');
            }
        }catch(Ex){console.log(Ex);}
    },
    
    fetchAndRenderRecordRelatedModule: async function(module, relatedModule, recordId){
        let targetItemHolder = `#lri_${recordId}_relatesITEM_${relatedModule}_Holder`;
        if($(targetItemHolder).length === 0){
            $(`#UA-LOO-REC-${recordId} .lri_relatedModulesHolder`).append(`
                <div id="lri_${recordId}_relatesITEM_${relatedModule}_Holder" class='lri-rel-list-container minimize-on-item-minimize'>
                                    <div class='lri-note-ttl'> <span class='material-icons'> expand_more </span> ${relatedModule.capitalize()} <button onclick="UA_TPA_FEATURES.showAddNewRecordItem('${relatedModule}', '${recordId}')" class='add-note-button'> + Create ${relatedModule.capitalize().substring(0, relatedModule.length-1)}</button> </div>
                                    <div style='display:none' class='lri-relatedItem-Holder'>
                                      <button onclick="UA_TPA_FEATURES.showAddNewRecordItem('${relatedModule}')" class='save-note-btn'>Save Note</button>
                                      <button onclick="$('#UA-LOO-REC-${recordId} .lri-relatedItem-Holder').hide()" class='cancel-note-btn'>Cancel</button><br><br><br>
                                    </div>
                                    <div class="lri-realtedItemsHistoryHolder">
                                    </div>
                            </div>
            `);
            let relatedListResults = await UA_SAAS_SERVICE_APP.fetchRecordRelatedModuleItems(module, relatedModule, recordId);
            relatedListResults.forEach(relRecItem=>{
                UA_TPA_FEATURES.renderModuleRecordItem(relRecItem.id);
            });
        }
        else{
            $(targetItemHolder).slideToggle();
        }
        
    },
    
    renderFieldNow: function (contactItem, fieldItem, hideReadOnly = false) {
            try{
            let fieldInputType = "text";
            let fieldLabel = fieldItem;
            let additionalFieldHolderSelector = '#UA-LOO-REC-'+contactItem.id+' .lri-fields-list';
            let additionalFieldHolderElement = $(additionalFieldHolderSelector);
            let currentFieldConfig = UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[contactItem.__RecordModuleID].fields[fieldItem];
            currentFieldConfig = currentFieldConfig ? currentFieldConfig : {};
            let isNewRecordCreate = contactItem.id.startsWith('___UA_NEW_');
            if(hideReadOnly && currentFieldConfig.field_read_only){
                return true;
            }
            if(fieldItem.startsWith("$") || currentFieldConfig.data_type === "profileimage"){
                    return true;
            }
            if(currentFieldConfig.data_type){
                fieldInputType = UA_TPA_FEATURES.fieldInputTypeMap[currentFieldConfig.data_type];
                fieldLabel = currentFieldConfig.display_label;
            }
            if(isNewRecordCreate){
                if(currentFieldConfig._ua_ignoreOnCreateView){
                    return false;
                }
                if(currentFieldConfig.data_type === "datetime"){
                    return false;
                }
                if(currentFieldConfig.data_type === "boolean" && !currentFieldConfig.system_mandatory){
                    return false;
                }
            }
            if(isNewRecordCreate){
                let phoneDisplayNames = ["phone", "mobile", "phonenumber", "mobilenumber", "phone number", "mobile number"];
                if(currentFieldConfig.data_type === "phone" || currentFieldConfig.data_type === "mobile" || phoneDisplayNames.includes(fieldLabel.toLowerCase())){
                    contactItem[fieldItem] = UA_TPA_FEATURES.CURRENT_LOOKUP_LOAD_CONTACT_PHONE_NUMBER ? UA_TPA_FEATURES.CURRENT_LOOKUP_LOAD_CONTACT_PHONE_NUMBER : null;
                }
                let contactNameDisplayNames = ["name", "first name", "last name", "full name", "firstname", "lastname", "fullname"];
                if(contactNameDisplayNames.includes(fieldLabel.toLowerCase())){
                    contactItem[fieldItem] = UA_TPA_FEATURES.CURRENT_LOOKUP_LOAD_CONTACT_NAME ? UA_TPA_FEATURES.CURRENT_LOOKUP_LOAD_CONTACT_NAME : null;
                }
            }
            if(currentFieldConfig.display_label){
                fieldLabel = currentFieldConfig.display_label;
            }
            fieldInputType = fieldInputType ? fieldInputType : "text";
            if(contactItem[fieldItem] === null || contactItem[fieldItem] === undefined){
                additionalFieldHolderElement.append(`
                    <div class="fieldItemHolder ${currentFieldConfig.system_mandatory ? 'cd-field-mandatory': ''}" data-fieldname="${fieldItem}" >
                        <div class="cd-field-label">${fieldLabel}</div>
                        <div class="cd-field-edit"><${fieldInputType === "textarea" ? "textarea" : "input"} ${currentFieldConfig.field_read_only || fieldInputType === "datetime-local" ? 'readonly': ''} ${currentFieldConfig.length ? 'maxlength="'+currentFieldConfig.length+'"': ''} onkeyup="UA_TPA_FEATURES.updateFieldIfEnterField(event, '${fieldItem}', '${contactItem["id"]}')" onclick="UA_TPA_FEATURES.doFieldSpecificUI('${fieldItem}', '${contactItem["id"]}')" data-fieldname="${fieldItem}" id="ad_${contactItem["id"].substr(-5)}_field_${fieldItem}" type="${fieldInputType}" value=""/></div>
                    </div>
                `);
            }
            else if(Array.isArray(contactItem[fieldItem])){
                let innnerObj = contactItem[fieldItem];
                additionalFieldHolderElement.append(`<div id="field-tc-${contactItem['id']}-${fieldItem}"  class="fieldItemHolder tblfldhide cd-field-label" data-fieldname="${fieldItem}">${fieldLabel}</div> <div class="ad-field-table-row-start"></div>`);
                Object.keys(innnerObj).forEach(innerFieldItem=>{
                    try{
                        let innerObjFieldValue = innnerObj[innerFieldItem];
                        let innerObjFieldValueHTML = innerObjFieldValue;
                        if(typeof innerObjFieldValue === "object"){
                            innerObjFieldValueHTML = "";
                            Object.keys(innerObjFieldValue).forEach(innerObjFieldValueKey=>{
                                try{
                                     innerObjFieldValueHTML+=(`
                                        <div class="tblfldhide ad-field-table-row">
                                            <div class="ad-field-table-name">
                                                ${innerObjFieldValueKey}
                                            </div>
                                            <div class="ad-field-table-value">
                                                ${getSafeString(innerObjFieldValue[innerObjFieldValueKey])}
                                            </div>
                                        </div>
                                    `);
                                }catch(ex){
                                    console.log(ex);
                                }
                            });
                        }
                        $(`${additionalFieldHolderSelector} #field-tc-${contactItem['id']}-${fieldItem}`).append(`
                            <div class="tblfldhide ad-field-table-row" data-fieldname="${fieldItem}">
                                <div class="ad-field-table-name">
                                    ${innerFieldItem+1}
                                </div>
                                <div class="ad-field-table-value">
                                    ${innerObjFieldValueHTML}
                                </div>
                            </div>
                        `);
                    }catch(ex){
                        console.log(ex);
                    }
                });
                additionalFieldHolderElement.append(`<div class="ad-field-table-row-end"></div>`);
            }
            else if(typeof contactItem[fieldItem] === "object"){

                let innnerObj = contactItem[fieldItem];
                additionalFieldHolderElement.append(`<div id="field-tc-${contactItem['id']}-${fieldItem}" class="fieldItemHolder tblfldhide cd-field-label" data-fieldname="${fieldItem}" for="ad_field_${fieldItem}">${fieldLabel}</div> <div class="ad-field-table-row-start"></div>`);
                Object.keys(innnerObj).forEach(innerFieldItem=>{
                    try{
                        $(`${additionalFieldHolderSelector} #field-tc-${contactItem['id']}-${fieldItem}`).append(`
                            <div class="tblfldhide ad-field-table-row">
                                <div class="ad-field-table-name">
                                    ${innerFieldItem}
                                </div>
                                <div class="ad-field-table-value">
                                    ${getSafeString(innnerObj[innerFieldItem])}
                                </div>
                            </div>
                        `);
                    }catch(ex){
                        console.log(ex);
                    }
                });
                additionalFieldHolderElement.append(`<div class="ad-field-table-row-end"></div>`);
            }
            else{
                let fieldValue = contactItem[fieldItem];
                let isReadOnlyField = currentFieldConfig.field_read_only || fieldInputType === "datetime-local" || fieldItem === "id";
                if(fieldInputType === "datetime-local"){
                    var d = new Date(fieldValue);
                    fieldValue = new Date(fieldValue).toLocaleString()+" ("+getTimeString(fieldValue)+" )";
                    fieldInputType = "text";
                }
                additionalFieldHolderElement.append(`
                    <div class="fieldItemHolder" data-fieldname="${fieldItem}" >
                        <div class="cd-field-label" data-fieldname="${fieldItem}" for="ad_field_${fieldItem}">${fieldLabel}</div>
                        <div class="cd-field-edit"><${fieldInputType === "textarea" ? "textarea" : "input"} ${isReadOnlyField ? 'readonly': ''} ${currentFieldConfig.length ? 'maxlength="'+currentFieldConfig.length+'"': ''} onkeyup="UA_TPA_FEATURES.updateFieldIfEnterField(event, '${fieldItem}', '${contactItem["id"]}')" onclick="UA_TPA_FEATURES.doFieldSpecificUI('${fieldItem}', '${contactItem["id"]}')" data-fieldname="${fieldItem}" id="ad_${contactItem["id"].substr(-5)}_field_${fieldItem}" type="${fieldInputType}" value=""/></div>
                    </div>
                `);
                $(`#ad_${contactItem["id"].substr(-5)}_field_${fieldItem}`).val(fieldValue ? fieldValue : '');
            }
        }catch(ex){
            console.log(ex);
        }
    },
    
    insertIncomingWD_supportedModules: function(){},
    
    addNewWebhook: function(){},
    
    doFieldSpecificUI: function(fieldName, recordId){
        if(!$(`#ad_${recordId.substr(-5)}_field_`+fieldName).attr('readonly')){
            if(!recordId.startsWith('___UA_NEW')){
                UA_TPA_FEATURES.showEditSaveOption(fieldName, recordId);
            }
        }
        let module = recordId.startsWith('___UA_NEW') ? recordId.replace('___UA_NEW_', "") : UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordId].__RecordModuleID;
        let fieldConfig = UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[module].fields[fieldName];
        if(fieldConfig.field_read_only === true){
            $(`#ad_${recordId.substr(-5)}_field_`+fieldName).attr({'disabled': 'true'});
        }
        if(fieldConfig.data_type === "picklist"){
            $('body').append(`<div class="adp-dd-focus-holder" onclick="UA_TPA_FEATURES.adpDDFocusRemove('${fieldName}', '${recordId}')"></div>`);
            $(`#ad_${recordId.substr(-5)}_field_`+fieldName).attr({'disabled': 'true'});
            var pickListHTML = `<div id="adp_${recordId.substr(-5)}-dd-${fieldName}" class="adp-holder">`;
            fieldConfig.pick_list_values.forEach(item=>{
                pickListHTML += `<div onclick="UA_TPA_FEATURES.adpDDSelected('${fieldName}','${item.actual_value}', '${recordId}')" class="adp-field-item">${item.display_value}</div>`;
            });
            pickListHTML+=`</div>`;
            $(`#ad_${recordId.substr(-5)}_field_`+fieldName).after(pickListHTML);
        }
    },
    
    updateFieldIfEnterField: function(e, fieldName, recordId){
        if (e.keyCode === 13) {
            if(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordId].__RecordModuleID].fields[fieldName].data_type !=="textarea"){
                UA_TPA_FEATURES.updateRecordByField(fieldName, recordId);
            }
        }
    },

    showEditSaveOption: function (fieldName, recordId){
        if($(`#ad_${recordId.substr(-5)}_edit_save_btn_`+fieldName).length >0){ return; };
        $(`#ad_${recordId.substr(-5)}_field_`+fieldName).animate({'width': "-=40"}, 250, function(){ $(`#ad_${recordId.substr(-5)}_field_`+fieldName).after(`<button id="ad_${recordId.substr(-5)}_edit_save_btn_${fieldName}" class="ad_edit_save_btn" onclick="UA_TPA_FEATURES.updateRecordByField('${fieldName}', '${recordId}')"><span class="material-icons">check_circle</span></button>`); });
        //$('#ad_field_'+fieldName).after(`<button id="ad_edit_save_btn_${fieldName}" class="ad_edit_save_btn" onclick="updateContactOrLeadByField('${fieldName}')">Save</button>`);
    },

    hideEditSaveOption: function(fieldName, recordId){
       $(`#ad_${recordId.substr(-5)}_field_`+fieldName).animate({'width': "+=40"});
       $(`#ad_${recordId.substr(-5)}_edit_save_btn_`+fieldName).remove();
    },
    
    adpDDFocusRemove: function(fieldName, recordId){
        $(`#adp_${recordId.substr(-5)}-dd-`+fieldName).remove();
        $('.adp-dd-focus-holder').remove();
        UA_TPA_FEATURES.hideEditSaveOption(fieldName, recordId);
        $(`#ad_${recordId.substr(-5)}_field_`+fieldName).removeAttr('disabled');
    },

    adpDDSelected: function(fieldName, fieldValue, recordId){
       $(`.adp-dd-focus-holder`).remove();
       $(`#adp_${recordId.substr(-5)}-dd-`+fieldName).remove();
       $(`#ad_${recordId.substr(-5)}_field_`+fieldName).val(fieldValue);
       if(!recordId.startsWith('___UA_NEW')){
           UA_TPA_FEATURES.updateRecordByField(fieldName, recordId);
       }
       else{
           $(`#ad_${recordId.substr(-5)}_field_`+fieldName).removeAttr('disabled');
       }
   },
   
   updateRecordByField: async function(fieldName, recordId){
        if(recordId.startsWith('___UA_NEW')){
            return;
        }
        var value = $(`#ad_${recordId.substr(-5)}_field_`+fieldName).val().trim();
        $(`#ad_${recordId.substr(-5)}_field_` + fieldName).attr({'disabled': 'true'});
        $(`#ad_${recordId.substr(-5)}_edit_save_btn_` + fieldName).html('<span class="material-icons spinnow">autorenew</span></button>');
        $(`#ad_${recordId.substr(-5)}_field_`+fieldName).addClass('field-update-in-progress');
        var updateAPIResponse = await UA_SAAS_SERVICE_APP.updateRecordByField(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[recordId].__RecordModuleID, recordId, fieldName, value);
        $(`#ad_${recordId.substr(-5)}_field_` + fieldName).removeAttr('disabled');
        if(updateAPIResponse.error){
            $(`#ad_${recordId.substr(-5)}_field_` + fieldName).css('border', '1px solid crimson');
            $(`#ad_${recordId.substr(-5)}_edit_save_btn_` + fieldName).html(`<span title="${UA_APP_UTILITY.getCleanStringForHTMLAttribute(updateAPIResponse.error.message ? updateAPIResponse.error.message : '')}" class="material-icons ">sync_problem</span></button>`);
         }
         else{
             $(`#ad_${recordId.substr(-5)}_field_`+fieldName).addClass('field-update-success-progress');
             $(`#ad_${recordId.substr(-5)}_edit_save_btn_` + fieldName).html('<span class="material-icons">cloud_done</span></button>');
         }
         setTimeout(function () {
             $(`#ad_${recordId.substr(-5)}_field_`+fieldName).removeClass('field-update-in-progress').removeClass('field-update-success-progress');
             UA_TPA_FEATURES.hideEditSaveOption(fieldName, recordId);
         }, 1000);
   },
   
   createRecordInModule: async function(module, parentRecordId = null){
       let createFieldsMap = {};
       let requiredFieldComplete = true;
       $('#UA-LOO-REC-___UA_NEW_'+module+' .cd-field-edit input, #UA-LOO-REC-___UA_NEW_'+module+' .cd-field-edit textarea').each(function(i){
            let item = $(this);
            let fieldName = item.attr('data-fieldname');
            let fieldValue = item.val().trim();
            if(valueExists(fieldName) && valueExists(fieldValue)){
                createFieldsMap[fieldName] = fieldValue;
            }
            else{
                if(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[module].fields[fieldName].system_mandatory){
                    requiredFieldComplete = false;
                    item.css('border', '1px solid red');
                }
            }
       });
       if(requiredFieldComplete && Object.keys(createFieldsMap).length > 0){
           let createRecordAPIResponse = await UA_SAAS_SERVICE_APP.createRecordInModule(module, createFieldsMap, parentRecordId);
           if(createRecordAPIResponse.error){
                showErroWindow('Unable to create '+module, 'Check the value in the fields & try again.');
           }
           else{
               $('#UA-LOO-REC-___UA_NEW_'+module).remove();
               UA_TPA_FEATURES.renderModuleRecordItem(createRecordAPIResponse.id, true);
           }
       }
    },
    CURRENT_LOOKUP_LOAD_CONTACT_PHONE_NUMBER: null,
    CURRENT_LOOKUP_LOAD_CONTACT_NAME: null,
    minimizeAppIframe: function () {
        let params = new URLSearchParams(location.search);
        if(params.get("iframeId")){
            window.parent.postMessage({
                "type": "minimizeAppIframe",
                "iframeid": params.get("iframeId")
            },"*");
            $(".whatcetra-max-frame-btn").show();
            return;
        }

        window.parent.postMessage({
            "type": "minimizeAppIframe",
            "iframeid": "contactlead-iframe"
        },"*");
        
        window.parent.postMessage({
            "type": "minimizeAppIframe",
            "iframeid": "contactlead-iframe-hubspot"
        },"*");

        $(".whatcetra-max-frame-btn").show();
    },

    maximizeAppIframe: function () {
        let params = new URLSearchParams(location.search);
        if(params.get("iframeId")){
            window.parent.postMessage({
                "type": "maximizeAppIframe",
                "iframeid": params.get("iframeId")
            },"*");
            $(".whatcetra-max-frame-btn").hide();
            return;
        }
        
        window.parent.postMessage({
            "type": "maximizeAppIframe",
            "iframeid": "contactlead-iframe"
        },"*");
        
        window.parent.postMessage({
            "type": "maximizeAppIframe",
            "iframeid": "contactlead-iframe-hubspot"
        },"*");
               
        $(".whatcetra-max-frame-btn").hide();
    },
    
    refreshRecordItemInUI: function(recordId){
        $('#UA-LOO-REC-'+recordId+' .lri-fields-list').html('');
        $('#UA-LOO-REC-'+recordId+' .lri-fields-act-list').html('');
        UA_TPA_FEATURES.renderModuleRecordItem(recordId);
    },

    renderTempPrev: async function(){
        var tempPrevHtml = `<div class="windowPageOuter" id="tempMsgPrevWindow" style="display: none;">
                                <div class="windowPageInner">
                                    <div class="windowPageTitle">Message Preview
                                        <button class="win-ttl-close" onclick="$('#tempMsgPrevWindow').hide()">x</button>
                                    </div>
                                    <div class="windowPageContent">
                                        <div class="ssf-fitem messageInputHolder">
                                            <div class="ssf-fitem-ans">
                                                <textarea id="inp-ssf-main-message" placeholder="No template message"></textarea>
                                            </div>
                                        </div>
                                        <div style="display: inline-block;padding: 0px 10px;margin-left: 5px;">
                                            <div class="ssf-fitem ssf-win-actions-holder">
                                                <button class="ssf-action-btn primary-form-btn" onclick="UA_TPA_FEATURES.copyToClipboard_btn('inp-ssf-main-message')">Copy</button>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>`;
        $(".pageContentHolder").append(tempPrevHtml);
    },
/* <div class="ssf-fitem ssf-win-actions-holder">
    <button class="ssf-action-btn primary-form-btn" onclick="UA_TPA_FEATURES.sendTempMsgViaWApp($('#inp-ssf-main-message').val())">add message</button>
</div> */
    insertTemplateContentInMessageInput : async function(value){
        var templateId = value.split(":::")[0];
        var entityId = value.split(":::")[1];
        var curTemplateItem = UA_APP_UTILITY.fetchedTemplates[templateId];
        UA_TPA_FEATURES.chosenMessgeTemplate = curTemplateItem;

        $('#inp-ssf-main-message').removeAttr('readonly');
        let resolvedMessageText = await UA_APP_UTILITY.getTemplateAppliedMessage(curTemplateItem.message, UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[entityId]);
        $('#inp-ssf-main-message').val(resolvedMessageText).focus();
        $("#tempMsgPrevWindow").show();
    },

    renderNewTempModuleFields: async function(){
        $("#ssf-fitem-new-template-var-holder").empty();
        var moduleFieldsResponse = UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[UA_TPA_FEATURES.CURRENT_LOOKUP_MODULE].fields;
        let moduleFieldsKeys = Object.keys(moduleFieldsResponse);
        var moduleFields = [];
        moduleFieldsKeys.forEach(item=>{
            item = moduleFieldsResponse[item];
            if(["string" , "number", "integer", "text"].includes(item.data_type) && item.display_label){
                moduleFields.push(item);
            }
        });
        moduleFields.sort(function(a, b) {
            return a.display_label.localeCompare(b.display_label);
        });
        var dropDownValues = [];
        moduleFields.forEach(item=>{
            dropDownValues.push({
                "label": item.display_label,
                "value": UA_TPA_FEATURES.CURRENT_LOOKUP_MODULE +'.'+item.api_name
            });
        });

        UA_APP_UTILITY.renderSelectableDropdown("#ssf-fitem-new-template-var-holder", `Insert ${UA_TPA_FEATURES.CURRENT_LOOKUP_MODULE} fields`, dropDownValues, 'UA_APP_UTILITY.templateMergeFieldSelected', false, true);
        UA_TPA_FEATURES.renderTempPrev();
    },

    copyToClipboard: function(text) {
        var sampleTextarea = document.createElement("textarea");
        document.body.appendChild(sampleTextarea);
        sampleTextarea.value = text; //save main text in it
        sampleTextarea.select(); //select textarea contenrs
        document.execCommand("copy");
        document.body.removeChild(sampleTextarea);
    },

    copyToClipboard_btn: function(id){
        var text = document.getElementById(id).value;
        UA_TPA_FEATURES.copyToClipboard(text);
        $("#"+id).select()
    },

    sendTempMsgViaWApp: async function(text){
        let params = new URLSearchParams(location.search);
        if(params.get("iframeId")){
            window.parent.postMessage({
                "type": "sendTempMsgViaWApp",
                "send": true,
                "text": text
            },"*");
            $('#tempMsgPrevWindow').hide();
            return;
        }
    },
    selectToggleChooseWAMessages: function(){
        let chooseAllMessages = $("#fwnai-select-checkbox").prop('checked');
        $('#fwnai-messages-list-holder input[type="checkbox"]').prop('checked', chooseAllMessages);
        $('#fwnai-messages-list-holder .fwnai').toggleClass('selected', chooseAllMessages);
    },
    CURRENT_LOADED_WA_MESSAGES: {},
    CURRENT_EXPORT_CHAT_MODULE_ID: null,
    CURRENT_EXPORT_CHAT_MODULE_RECORD_ID: null,
    fetchAndRenderWAChatMessagesForNote: function(moduleId, recordId){
        this.CURRENT_EXPORT_CHAT_MODULE_ID= moduleId;
        this.CURRENT_EXPORT_CHAT_MODULE_RECORD_ID = recordId;
        let params = new URLSearchParams(location.search);
        if(params.get("iframeId")){
            window.parent.postMessage({
                "type": "getChats",
                "id": parseInt(UA_TPA_FEATURES.CURRENT_LOOKUP_LOAD_CONTACT_PHONE_NUMBER)+'@c.us',
                "targetElemID": '#'+params.get("iframeId")
            }, '*');
            $("#fullscreen-wa-note-app-container").show();
            $("#fwnai-select-checkbox").prop('checked', false);
        }
    },
    CURRENT_WA_OWNER_NUMBER: {},
    loadWaMessagesToListHolder: function(messagesList){
        $('#fwnai-messages-list-holder').html('');
        if($("#fullscreen-wa-note-app-container").css('display') === 'none'){
            return false;
        }
        UA_TPA_FEATURES.CURRENT_LOADED_WA_MESSAGES = {};
        let msgsHTML = ``;
        messagesList.forEach(item=>{
            try{
                let isIncoming = item.id.split('_')[0] === 'false';
                let contactNumber = item.id.split('@')[0].split('_')[1];
                if(item.m.startsWith('/9j/')){
                    return false;
                }
                let resolvedMessageItem = {
                    'id': item.id,
                    'status': isIncoming ? 'Received' : 'Sent',
                    'from': isIncoming ? contactNumber : UA_TPA_FEATURES.CURRENT_WA_OWNER_NUMBER,
                    'to': isIncoming ? UA_TPA_FEATURES.CURRENT_WA_OWNER_NUMBER: contactNumber,
                    'text': item.m,
                    'time': parseInt(item.t)
                };
                resolvedMessageItem.id = UA_APP_UTILITY.getCleanStringForHTMLAttribute(resolvedMessageItem.id);
                UA_TPA_FEATURES.CURRENT_LOADED_WA_MESSAGES[resolvedMessageItem.id] = resolvedMessageItem;
                msgsHTML += `<div class="fwnai fwnai-${isIncoming ? 'i': 'o'}" id="${resolvedMessageItem.id}">
                    <input type="checkbox" id="chk-${resolvedMessageItem.id}" onchange="UA_TPA_FEATURES.toggleWANoteMsgItemSelected(this)"/>
                    <div class="fwnaim">
                        ${resolvedMessageItem.text}
                    </div>
                    <i class="fwnait">
                        ${new Date(resolvedMessageItem.time).toLocaleString()}
                    </i>
                </div>`;
            }catch(e){ console.log(e);}
        });
        $('#fwnai-messages-list-holder').html(msgsHTML);
        var objDiv = document.getElementById("fwnai-messages-list-holder");
        objDiv.scrollTop = objDiv.scrollHeight;
        $('body').css('overflow-y', 'hidden');
    },
    triggerAddSelectedMessagesAsNote: function(){
        let selectedMessagesIds = [];
        $('#fwnai-messages-list-holder .fwnai').each(function(i){ 
            if($('#'+$(this).attr('id')+' input[type="checkbox"]').prop('checked')){
                selectedMessagesIds.push($(this).attr('id'));
            }
        });
        let noteContent = ` --- Imported Messages from WhatsApp on ${new Date().toLocaleString('en-US', {hour12: true})} ---\n\n`;
        selectedMessagesIds.forEach(item=>{
            let msgItem = UA_TPA_FEATURES.CURRENT_LOADED_WA_MESSAGES[item];
            if(!msgItem){
                return;
            }
            noteContent+=`[${new Date(msgItem.time).toLocaleString('en-US', {hour12: true})}] ${msgItem.status} from ${msgItem.from} to ${msgItem.to} : ${msgItem.text}\n`;
        });
        console.log('Add this note to '+this.CURRENT_EXPORT_CHAT_MODULE_ID+ ":"+this.CURRENT_EXPORT_CHAT_MODULE_RECORD_ID);
        $("#fullscreen-wa-note-app-container").hide();
        $('#UA-LOO-REC-'+this.CURRENT_EXPORT_CHAT_MODULE_RECORD_ID+' .note-content-inp').val(noteContent);
        $('#lri_'+this.CURRENT_EXPORT_CHAT_MODULE_RECORD_ID+'_noteHolder').show();
        $('#lri_'+this.CURRENT_EXPORT_CHAT_MODULE_RECORD_ID+'_noteHolder .lri-addNote-Holder').show();
        UA_TPA_FEATURES.addModuleRecordNote(UA_TPA_FEATURES.CURRENT_EXPORT_CHAT_MODULE_ID, UA_TPA_FEATURES.CURRENT_EXPORT_CHAT_MODULE_RECORD_ID);
    },
    toggleWANoteMsgItemSelected: function(elem){
        let chkElemId = $(elem).attr('id');
        $('#'+ chkElemId.replace('chk-', '')).toggleClass('selected', $(elem).prop('checked'));
    }


};

window.addEventListener("message", (event) => {
    try{
        if (event.origin !== "https://web.whatsapp.com")
            return;
        let message = event.data;
        if(message.type === "contactChange" ){
            window.parent.postMessage({"type": "wc_getOwner","targetElemID": '#'+queryParams.get("iframeId")}, '*');
            if(currentUser)
            {
                phoneNumber = message.phoneNumber;
                if(phoneNumber.indexOf('+') !== 0){
                    phoneNumber = "+"+phoneNumber;
                }
                contactName = message.contactName;
                if(valueExists(phoneNumber)){
                    UA_TPA_FEATURES.CURRENT_LOOKUP_LOAD_CONTACT_PHONE_NUMBER = phoneNumber;
                    $("#inp-lookup-search-term").val(phoneNumber);
                }
                if(valueExists(contactName)){
                    UA_TPA_FEATURES.CURRENT_LOOKUP_LOAD_CONTACT_NAME = contactName;
                }
                UA_TPA_FEATURES.lookUpSearchValueUpdated();
            }
        } 
        else if(message.type === "storageChange"){
            console.log('storechange', message);
        } 
        else if(message.type === "fetchedWACHATS"){
            UA_TPA_FEATURES.loadWaMessagesToListHolder(event.data.chats);
        }
        else if(message.type === "wcOwnerNumberInit"){
            UA_TPA_FEATURES.CURRENT_WA_OWNER_NUMBER = event.data.number;
        }
        else if(message.type === "IncomingSignal"){
            //addToQueue(message);
            //sendMsgToExtension(message);
        }
    }
    catch(exc){
        console.log(exc);
    }
    
}, false);