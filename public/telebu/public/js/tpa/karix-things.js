var UA_TPA_FEATURES = {
    chosenMessgeTemplate : null,
    'clientIds': {
        'karixforhubspotcrm' : "18454e29-584d-422e-be15-a248b7d0af9b"
    },
    APP_SAVED_CONFIG : null,
    getSupportedChannels: function(){
        return [ UA_APP_UTILITY.MESSAGING_CHANNELS.SMS, UA_APP_UTILITY.MESSAGING_CHANNELS.WHATSAPP ];
    },
    workflowCode : {
        "incoming": {
            "ulgebra_webhook_authtoken": null
        },
        "SMS": {
            "ver": "1.0",
            "send": "FILL_HERE",
            "dest": "FILL_HERE",
            "text": "FILL_HERE",
            "type": "PM",
            "module": "FILL_HERE",
            "recordId": "FILL_HERE",
            "ulgebra_webhook_authtoken": null,
            "channel": "SMS"
        },
        "WhatsApp_TEMPLATE":{
                "message": {
                    "channel": "WABA",
                    "content": {
                    	"preview_url": false, 
                    	"type": "TEMPLATE",
                    	"template":{
                    		"templateId":"FILL_HERE",
                    		"parameterValues": {
							    "0": "FILL_HERE"
							}
                    	}
                    },
                    "recipient":{
                        "to": "FILL_HERE",
                        "recipient_type": "individual"
                    },
                    "sender": {
                        "from": "FILL_HERE"
                    }
                },
                "metaData":{
                    "version":" v1.0.9" 
                },
                "module": "FILL_HERE",
                "recordId": "FILL_HERE",
                "ulgebra_webhook_authtoken": null,
                "channel": "WhatsApp"
            },
        "WhatsApp_TEMPLATE_MEDIA":{
                "message": {
                    "channel": "WABA",
                    "content": {
                    	"preview_url": false, 
                    	"type": "MEDIA_TEMPLATE",
                    	"mediaTemplate":{
                    		"templateId":"FILL_HERE",
                    		"media":{
                                    "url": "FILL_HERE",
                                    "type": "FILL_HERE sample(image/document)"
                    		},
                    		"bodyParameterValues": {
							    "0": "FILL_HERE"
							}
                    	}
                    },
                    "recipient":{
                        "to": "FILL_HERE",
                        "recipient_type": "individual"
                    },
                    "sender": {
                        "from": "FILL_HERE"
                    }
                },
                "metaData":{
                    "version":" v1.0.9" 
                },
                "module": "FILL_HERE",
                "recordId": "FILL_HERE",
                "ulgebra_webhook_authtoken": null,
                "channel": "WhatsApp"
            }
    },
    renderInitialElements: async function(){
        $('#suo-item-workflow-nav').after(`<div id="suo-item-workflow-nav"   class="suo-item suo-item-ac-labels" onclick="UA_TPA_FEATURES.showIncomingWebhookDialog()">
                                        <span class="material-icons">sync</span>  Incoming messages webhook<span class="anl_servicename"></span>
                                    </div>`);
        $("#ac_name_label_tpa .anl_servicename").text('Karix');
        UA_TPA_FEATURES.getCurrentAccountOrBalanceInfo(async function(resp){
            UA_TPA_FEATURES.APP_SAVED_CONFIG = resp.ulgebraAppConfiguration;
            $('#ac_name_label_tpa .ac_name_id').text(UA_TPA_FEATURES.APP_SAVED_CONFIG.authtoken);
            
            var sendersArray = [];
            UA_TPA_FEATURES.APP_SAVED_CONFIG.senders.split(',').forEach(item=>{
                    UA_APP_UTILITY.addMessaegSender(item, UA_APP_UTILITY.MESSAGING_CHANNELS.SMS, item);
                    sendersArray.push({
                        'label': item,
                        'value': item
                    });
            });
            UA_APP_UTILITY.TPA_SENDERS_FETCH_COMPLETED();
            var fetchedTemplatesWAResponse = await UA_TPA_FEATURES._getServiceWATemplates(true);
            console.log('wa fetchedtemoktes ', fetchedTemplatesWAResponse);
             var waTemplatesArray = [];
            fetchedTemplatesWAResponse.data.response.templates.forEach(item=>{
                if(item.template_create_status!== "APPROVED"){
                    return false;
                }
                var templateMessage = "";
                item.components.forEach(componentItem=>{
                    if(componentItem.type === "BODY"){
                        templateMessage = componentItem.text;
                    }
                    if(componentItem.type === "FOOTER"){
                        templateMessage += '\n'+ componentItem.text;
                    }
                });
                let placeHolders = [];
                for(var i=0; i<item.template_place_holder_count; i++){
                    placeHolders.push(i+1);
                }
                UA_APP_UTILITY.fetchedTemplates[item.template_name] = {
                    'message': templateMessage,
                    'templateType': 'placeholder_template',
                    'placeholders': placeHolders,
                    'tempid': item.template_name,
                    'head_media_url': item.head_media_url,
                    'head_mediatype': item.media_type
                };
                waTemplatesArray.push({
                    'label': item.template_name + ' (WhatsApp)',
                    'value': item.template_name
                });
            });

            UA_APP_UTILITY.renderSelectableDropdown('#ssf-fitem-template-var-holder', 'Insert Karix template', waTemplatesArray, 'UA_TPA_FEATURES.insertTemplateContentInMessageInput', false, false);
            
        });
        
//        var fetchedTemplatesResponse = await UA_TPA_FEATURES._getServiceTemplates();
//        console.log('fetchedtemoktes ', fetchedTemplatesResponse);
//        fetchedTemplatesCollection = fetchedTemplatesResponse.data.data;
        
        var sendersUniqueArray = [];
        var templatesArray = [];
//        fetchedTemplatesCollection.forEach(item=>{
//            UA_APP_UTILITY.fetchedTemplates[item.id] = {
//                'message': item.message
//            };
//            templatesArray.push({
//                'label': item.name,
//                'value': item.id
//            });
//            if(sendersUniqueArray.indexOf(item.senderid) === -1){
//                sendersUniqueArray.push(item.senderid);
//                sendersArray.push({
//                    'label': item.senderid,
//                    'value': item.senderid
//                });
//            }
//        });
        
//        var approvedTemplateDDId = UA_APP_UTILITY.renderSelectableDropdown('#ssf-fitem-template-var-holder', 'Insert Pinnacle template', templatesArray, 'UA_TPA_FEATURES.insertTemplateContentInMessageInput', false, false);
        
        UA_APP_UTILITY.getPersonalWebhookAuthtoken();
       
        var countryCallCodeArray = [];
        UA_APP_UTILITY.countryCallingCodeArray.forEach(item=>{
            countryCallCodeArray.push({
                'label': `${item.name} (${item.dial_code})`,
                'value': item.dial_code
            });
        });
        
        UA_APP_UTILITY.renderSelectableDropdown('#ssf-new-recip-countrycode', 'Select country', countryCallCodeArray, 'UA_APP_UTILITY.log');
   
    },
    
    renderWorkflowBodyCode :function(accessToken){
        
        UA_TPA_FEATURES.workflowCode.SMS.ulgebra_webhook_authtoken = accessToken.saas;
        UA_TPA_FEATURES.workflowCode.WhatsApp_TEMPLATE.ulgebra_webhook_authtoken = accessToken.saas;
        UA_TPA_FEATURES.workflowCode.WhatsApp_TEMPLATE_MEDIA.ulgebra_webhook_authtoken = accessToken.saas;
        UA_TPA_FEATURES.workflowCode.incoming.ulgebra_webhook_authtoken = accessToken.tpa;
        
        UA_APP_UTILITY.addWorkflowBodyCode('sms', 'For Sending SMS', UA_TPA_FEATURES.workflowCode.SMS);
        
        UA_APP_UTILITY.addWorkflowBodyCode('whatsapp_template', 'For Sending WhatsApp Text Template message', UA_TPA_FEATURES.workflowCode.WhatsApp_TEMPLATE);
        
        UA_APP_UTILITY.addWorkflowBodyCode('whatsapp_template_media', 'For Sending WhatsApp Media Template message', UA_TPA_FEATURES.workflowCode.WhatsApp_TEMPLATE_MEDIA);
    
    },
    
    
    
    prepareAndSendSMS: function(){
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        if(UA_TPA_FEATURES.chosenMessgeTemplate && UA_TPA_FEATURES.chosenMessgeTemplate.templateType === "placeholder_template"){
            return UA_TPA_FEATURES.prepareAndSendBulkWhatsApp();
        }
        if(UA_APP_UTILITY.currentSender.channel === UA_APP_UTILITY.MESSAGING_CHANNELS.WHATSAPP){
            return UA_TPA_FEATURES.prepareAndSendBulkWhatsApp();
        }
        if(!UA_APP_UTILITY.currentSender){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var senderId = UA_APP_UTILITY.currentSender.value;
        if(!valueExists(senderId)){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var recipNumbers = UA_APP_UTILITY.getCurrentMessageRecipients();
        var messageText = $("#inp-ssf-main-message").val().trim();
        if(!valueExists(messageText)){
            showErroWindow('Message is empty!', "Kindly fill message to proceed.");
            return false;
        }
        if(recipNumbers.length === 0){
            showErroWindow('Recipient list is empty!', "Kindly add recipients to send SMS.");
            return false;
        }
        var messageNumberMapArray = [];
        recipNumbers.forEach(item=>{
            let contactRecordId = UA_APP_UTILITY.storedRecipientInventory[item.id] ? item.id : null;
            let resolvedMessageText = UA_APP_UTILITY.getTemplateAppliedMessage(messageText, UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id]);
            let historyUID = item.id+''+new Date().getTime()+''+Math.round(Math.random(100000,99999)*1000000);
            messageNumberMapArray.push({
                'number': item.number,
                'text': resolvedMessageText,
                'clientuid': historyUID
            });
            if(!contactRecordId && UA_APP_UTILITY.isConvChatView()){
                let fetchedKeys = Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
                contactRecordId = fetchedKeys.length === 1 ? fetchedKeys[0] : null;
            }
            let messageHistoryMap = {
                'contact':{
                    'name': UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].Full_Name : item.number
                },
                'from': senderId,
                'to': item.number,
                'message': resolvedMessageText,
                'status': 'NOT_SENT',
                'module': UA_SAAS_SERVICE_APP.widgetContext.module,
                'moduleId': item.id,
                'channel': "SMS"
            };
            UA_TPA_FEATURES.sendSMS(senderId, item.number, resolvedMessageText, (async function(apiResponse){
                if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
                   return false;
               }
            if(apiResponse.data.indexOf("Request accepted") !== 0){
                showErroWindow('Unable to send message', apiResponse.data);
                return;
            }
            else{
                showErroWindow('Completed', `Your ${messageNumberMapArray.length} messages has been sent successfully.`, false, true);
                UA_APP_UTILITY.resetMessageFormFields();
                console.log(apiResponse);
                messageHistoryMap.status = "SENT";
                await UA_SAAS_SERVICE_APP.addSentSMSAsRecordInHistory({"message1": messageHistoryMap});
            }
            if(UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CHAT && response.data){
                messageHistoryMap["id"] = new Date().getTime();
                messageHistoryMap["date_created"] = new Date();
                messageHistoryMap["body"] = messageHistoryMap.message;
                // UA_TPA_FEATURES.fetchMessagesAndAddToChatBox(true);
                UA_APP_UTILITY.addMessageInBox(UA_TPA_FEATURES.convertMessageToUASchema(messageHistoryMap), true);
            }
        }));
        });
    },
    
    
    
    
    
    insertTemplateContentInMessageInput :function(templateId){
        var curTemplateItem = UA_APP_UTILITY.fetchedTemplates[templateId];
        UA_TPA_FEATURES.chosenMessgeTemplate = curTemplateItem;
        $("#messageAttachmentInputHolder").hide();
        UA_APP_UTILITY.currentAttachedFile = null;
        $("#attachedfile").text('Attach file');
        $('#inputFileAttach').val('');
        if(curTemplateItem.templateType === "placeholder_template"){
            $("#primary-send-btn").text('Send WhatsApp').css('background-color', 'green');
            if(curTemplateItem.head_mediatype && curTemplateItem.head_mediatype!==""){
                $("#attachedfile").text('Attach '+curTemplateItem.head_mediatype);
                $("#messageAttachmentInputHolder").show();
            }
//            if(curTemplateItem.head_media_url){
//                $("#messageAttachmentInputHolder").show();
//            }
            $("#ssf-fitem-template-placeholder-holder-listholder").html("");
            curTemplateItem.placeholders.forEach(item=>{
                $("#ssf-fitem-template-placeholder-holder-listholder").append(`
                    <div class="ssf-temp-placehold-item">
                            <span class="ssf-temp-placehold-item-label">{{${item}}}</span> <span class="material-icons ssf-temp-placehold-item-icon">double_arrow</span> <input onblur="UA_APP_UTILITY.lastFocusedInputElemForPlaceholderInsert = 'ssf-templ-place-input-${item}'" id="ssf-templ-place-input-${item}" data-placeholder-id="${item}" class="ssf-temp-placehold-item-input" type="text" placeholder="Type or choose field from above">
                        </div>
                `);
            });
            if(curTemplateItem.placeholders.length > 0){
                $("#ssf-fitem-template-placeholder-holder").show();
            }else{
                $("#ssf-fitem-template-placeholder-holder").hide();
            }
            $('#inp-ssf-main-message').attr('readonly', true);
        }
        else{
            $("#primary-send-btn").text('Send SMS').css('background-color', 'royalblue');
            $("#ssf-fitem-template-placeholder-holder").hide();
            $('#inp-ssf-main-message').removeAttr('readonly');
        }
        $('#inp-ssf-main-message').val(curTemplateItem.message).focus();
    },
    
    _getServiceTemplates: async function(){
        var templatesResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "/", "post");
        if(templatesResponse.code === 401 || templatesResponse.data.code === 401 || templatesResponse.data.message === "Authentication failed"){
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return;
        }
        return  templatesResponse;
    },
    
    _getServiceWATemplates: async function(silenceAuthError = false, page =1){
        var templatesResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://rcsgui.karix.solutions/api/v1.0/template/${UA_TPA_FEATURES.APP_SAVED_CONFIG.waba_id}?status=APPROVED`, "get");
        if(templatesResponse.code === 401 || templatesResponse.data.code === 401 || templatesResponse.data.message === "Authentication failed"){
            if(silenceAuthError){
                return {
                    data:{
                        data: []
                    }
                };
            }
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return;
        }
        return  templatesResponse;
    },
    
    showReAuthorizeERROR: function(showCloseOption = false){
        if($('#inp_tpa_api_key').length > 0){
            console.log('already showing reautherr');
            return;
        }
        showErroWindow("Authentication needed!", `You need to authorize your Karix Account to proceed. <br><br> 
        <div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
            <div style="
                font-size: 24px;
                font-weight: bold;
                margin-top: 20px;
                margin-bottom: 15px;
            ">API Configuration</div>
            <div style="margin-left:20px">
                <div style="font-size: 16px;margin-bottom: 5px;margin-top: 10px;color: rgb(80,80,80);">Karix API Key</div>
                <input id="inp_tpa_api_key" type="text" placeholder="Enter your API Key..." style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
            </div>
            <div style="margin-left:20px;display:none">
                <div style="font-size: 16px;margin-bottom: 0px;margin-top: 10px;color: rgb(80,80,80);">Karix SMS Sender IDs</div>
                <div style="font-size:14px;color:silver;margin-bottom:5px">Provide comma separated in case of multiple sender IDs</div>
                <input id="inp_tpa_senders" type="text" placeholder="Enter Karix Sender IDs..." style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
            </div>
            
            <div style="
                font-size: 24px;
                font-weight: bold;
                margin-top: 20px;
                margin-bottom: 15px;
            ">WhatsApp Channel Configuration</div>
            <div style="margin-left:20px">
                <div style="font-size: 16px;margin-bottom: 0px;margin-top: 10px;color: rgb(80,80,80);">Karix API Domain</div>
                <div style="font-size:14px;color:silver;margin-bottom:5px">For UAE users: <b>rcmapi.karix.ae</b></div>
                <input id="inp_tpa_api_domain" type="text" placeholder="Enter Karix Domain" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;" value="rcmapi.instaalerts.zone">
            </div>
            <div style="margin-left:20px;margin-bottom:5px">
            <div style="font-size: 16px;margin-bottom: 5px;margin-top: 10px;color: rgb(80,80,80);">Karix WhatsApp WABA ID</div>
            <input id="inp_tpa_waba_id" type="text" placeholder="Enter WABA ID" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
            </div>
            <button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.saveTPAAPIKey(UA_APP_UTILITY.reloadWindow)">Authorize Now</button>
        </div>`, showCloseOption, false, 500);
        $('#inp_tpa_api_key').focus();
        UA_LIC_UTILITY.showExistingInvitedAdminDDHTML(true);
    },
    
    showReAuthorizeERRORWithClose: function(){
        UA_TPA_FEATURES.showReAuthorizeERROR();
    },
    
    saveTPAAPIKey: async function(callback){
        let authtoken = $("#inp_tpa_api_key").val();
        if(!valueExists(authtoken)){
            showErroMessage("Please fill API Key");
            return;
        }
        let smsSenders = $("#inp_tpa_senders").val();
//        if(!valueExists(smsSenders)){
//            showErroMessage("Please fill SMS Senders");
//            return;
//        }
        let whatsAppWABAID = $("#inp_tpa_waba_id").val();
        let waDomain = $("#inp_tpa_api_domain").val().trim();
        
//        if(!valueExists(whatsAppWABAID)){
//            showErroMessage("Please fill WhatsApp API WABA ID");
//            return;
//        }
        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', '__multiple_keys__', {
            'authtoken': authtoken,
            'senders': smsSenders,
            'waba_id': whatsAppWABAID ? whatsAppWABAID : '',
            'api_domain': waDomain
        },
        callback);
    },
    
    saveTPAWABAAPIKey: async function(callback){
        let authtoken = $("#inp_tpa_waba_api_key").val();
        if(!valueExists(authtoken)){
            showErroMessage("Please fill API Key");
            return;
        }
        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', 'waba_authtoken', authtoken, callback);
    },
    
    
    
    prepareAndSendBulkWhatsApp : function(){
        
        if(!UA_APP_UTILITY.currentSender){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var senderId = UA_APP_UTILITY.currentSender.value;
        if(!valueExists(senderId)){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var messageText = $("#inp-ssf-main-message").val().trim();
        if(!valueExists(messageText)){
            showErroWindow('Message is empty!', "Kindly fill message to proceed.");
            return false;
        }
        var recipNumbers = UA_APP_UTILITY.getCurrentMessageRecipients();
        if(recipNumbers.length === 0){
            showErroWindow('Recipient list is empty!', "Kindly add recipients to send SMS.");
            return false;
        }
        if(!UA_APP_UTILITY.fileUploadComplete){
            showErroWindow('File upload is not complete!', "Please wait while your file is uploading and try again.");
            return;
        }
        if(UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.MESSAGE_FORM){
            $('#sms-prog-window').show();
        }
            $('#msgItemsProgHolder').html('');
          var totalCount = recipNumbers.length;
          $('#totMsgDisp').text(`Sending messages 0/${totalCount}...`);
          var messagesSentCount = 0;
          var messageStatusMap = {
              'success': 0,
              'failed' : 0
          };
        recipNumbers.forEach(item=>{
            let templateDetails = null;
            let resolvedMessageText = null;
            let contactRecordId = UA_APP_UTILITY.storedRecipientInventory[item.id] ? item.id : null;
            if(UA_TPA_FEATURES.chosenMessgeTemplate){
                templateDetails = {
                    "templateId": UA_TPA_FEATURES.chosenMessgeTemplate.tempid
                };
                if(UA_TPA_FEATURES.chosenMessgeTemplate.head_mediatype && UA_TPA_FEATURES.chosenMessgeTemplate.head_mediatype!=="" && UA_APP_UTILITY.currentAttachedFile && UA_APP_UTILITY.currentAttachedFile.mediaUrl){
                    templateDetails.media = {
                        url : UA_APP_UTILITY.currentAttachedFile.mediaUrl,
                        type: UA_TPA_FEATURES.chosenMessgeTemplate.head_mediatype.toLowerCase()
                    };
                }
                resolvedMessageText = messageText + (templateDetails.media ? ' '+templateDetails.media.url : '');
                if(UA_TPA_FEATURES.chosenMessgeTemplate.placeholders && UA_TPA_FEATURES.chosenMessgeTemplate.placeholders.length > 0){
                    let parameterValues = {};
                    UA_TPA_FEATURES.chosenMessgeTemplate.placeholders.forEach(item=>{
                        let placeHolderValue = $('#ssf-templ-place-input-'+item).val();
                        //if(contactRecordId){
                            placeHolderValue = UA_APP_UTILITY.getTemplateAppliedMessage(placeHolderValue, UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[contactRecordId]);
                        //}
                        resolvedMessageText = resolvedMessageText.replace('{{'+item+'}}', placeHolderValue);
                        parameterValues[""+(parseInt(item)-1)] = (placeHolderValue);
                    });
                    if(templateDetails.media){
                        templateDetails.bodyParameterValues = parameterValues;
                    }
                    else{
                        templateDetails.parameterValues = parameterValues;
                    }
                }
            }
            let messageContent = null;
            if(templateDetails){
                if(templateDetails.media){
                  messageContent = {
                            "preview_url": false,
                            "type": "MEDIA_TEMPLATE",
                            "mediaTemplate": templateDetails
                    };
                }
                else{
                    messageContent = {
                            "preview_url": false,
                            "type": "TEMPLATE",
                            "template": templateDetails
                    };
                }
            }
            else{
                resolvedMessageText = UA_APP_UTILITY.getTemplateAppliedMessage(messageText, UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id]);
                messageContent = {
                    "preview_url": false,
                    "text": resolvedMessageText,
                    "type": "AUTO_TEMPLATE"
                };
            }
            let messagePayload = {
                "message": {
                    "channel": "WABA",
                    "content": messageContent,
                    "recipient":{
                        "to": item.number,
                        "recipient_type": "individual"
                    },
                    "sender": {
                        "from": senderId
                    }
                },
                "metaData":{
                    "version":" v1.0.9" 
                }
            };
            if(!contactRecordId && UA_APP_UTILITY.isConvChatView()){
                let fetchedKeys = Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
                contactRecordId = fetchedKeys.length === 1 ? fetchedKeys[0] : null;
            }
            let messageHistoryMap = {
                'contact':{
                    'name': UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].Full_Name : item.number
                },
                'from': senderId,
                'to': item.number,
                'message': resolvedMessageText,
                'status': 'NOT_SENT',
                'module': UA_SAAS_SERVICE_APP.widgetContext.module,
                'moduleId': item.id,
                'channel': 'WhatsApp'
            };
            $('#msgItemsProgHolder').append(`
              <div id="msg-resp-item-${parseInt(item.number)}" class="msgRespItem" title="${getSafeString(resolvedMessageText)}">
                  <div class="mri-label"><b>${$('.msgRespItem').length+1}</b>. WhatsApp to ${UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].name+'-' : ''} ${item.number}</div>
                  <div class="mri-status">Sending...</div>
              </div>
          `);
            UA_TPA_FEATURES._proceedToSendWAAndExecuteCallback(messagePayload, async function(response){
                console.log(response);
                messagesSentCount++;
                $(`#msg-resp-item-${parseInt(item.number)} .mri-status`).text(response.data.statusDesc).css({'color': response.data.statusCode === "200" ? 'green' : 'crimson'});
                response.data.statusCode === "200" ? messageStatusMap.success++ : messageStatusMap.failed++;
                if(messagesSentCount === totalCount){
                    $('#totMsgDisp').text(`All messages have been processed. TOTAL: ${totalCount}, SENT: ${messageStatusMap.success}, FAILED: ${messageStatusMap.failed}`);
                }else{
                    $('#totMsgDisp').text(`Sending messages ${messagesSentCount}/${totalCount}...`);
                }
                messageHistoryMap.status = response.data.statusCode === "200" ? "SUCCESS" : response.data.statusCode;
                await UA_SAAS_SERVICE_APP.addSentSMSAsRecordInHistory({
                    "message1": messageHistoryMap
                });
                if(UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CHAT && response.data && response.data.statusCode){
                    messageHistoryMap["id"] = new Date().getTime();
                    messageHistoryMap["date_created"] = new Date();
                    messageHistoryMap["body"] = messageHistoryMap.message;
                    // UA_TPA_FEATURES.fetchMessagesAndAddToChatBox(true);
                    UA_APP_UTILITY.addMessageInBox(UA_TPA_FEATURES.convertMessageToUASchema(messageHistoryMap), true);
                }
            });
        });
        if(UA_APP_UTILITY.currentMessagingView !== UA_APP_UTILITY.appPrefinedUIViews.MESSAGE_FORM){
            UA_APP_UTILITY.resetMessageFormFields()
        }
        
        
    },
    
    sendSMS : function(sender, number, text, callback){
        
        var messagePayload = {
                "ver": "1.0",
                "send": sender,
                "dest": number,
                "text": text,
                "type": "PM"
            };
        
        UA_TPA_FEATURES._proceedToSendSMSAndExecuteCallback(messagePayload, callback);
        
    },
    
    sendBulkSMS : function(sender, numberMessageMapArray, callback){
        
        numberMessageMapArray.forEach(item => {
            var messagePayload = {
                "ver": "1.0",
                "send": sender,
                "dest": item.number,
                "text": item.text,
                "type": "PM"
            };

            UA_TPA_FEATURES._proceedToSendSMSAndExecuteCallback(messagePayload, callback);
        });
        
        
    },
    
    _proceedToSendSMSAndExecuteCallback: async function(messagePayload, callback){
        if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        var sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://japi.instaalerts.zone/httpapi/QueryStringReceiver?"+new URLSearchParams(messagePayload).toString(), "GET");
        callback(sentAPIResponse);
    },
    
    _proceedToSendWAAndExecuteCallback: async function(messagePayload, callback){
        if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        let apiDomain = UA_TPA_FEATURES.APP_SAVED_CONFIG && UA_TPA_FEATURES.APP_SAVED_CONFIG.api_domain && UA_TPA_FEATURES.APP_SAVED_CONFIG.api_domain.trim()!=="" ? UA_TPA_FEATURES.APP_SAVED_CONFIG.api_domain : 'rcmapi.instaalerts.zone';
        var sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://${apiDomain}/services/rcm/sendMessage`, "POST", messagePayload);
        callback(sentAPIResponse);
    },
    
    getAPIResponse: async function (extensionName, url, method, data) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = {"Content-Type": "application/json"};
        await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, TPAService: "karix",url: url, method: method, data: data, headers: headers, _ua_lic_adminUserID: UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID}).then((response) => {
            response = response.data;
            returnResponse = response;
            removeTopProgressBar(credAdProcess3);
            return true;
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
        return returnResponse;
    },
    
    getCurrentAccountOrBalanceInfo: async function(callback){
        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "--checkbalance", "GET", {});
        if((response.code === 401 || response.message === "Authentication failed") || (response.data && (response.data.code === 401 || response.data.message === "Authentication failed"))){
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return;
        }
        callback(response);
    },
    
    showIncomingWebhookDialog: function(){
        showErroWindow("Sync Incoming messages","Provide this URL to Karix to receive incoming WhatsApp messages <br><br> <textarea style='width:100%;height:100px;padding:10px;border:1px solid silver'>https://us-central1-ulgebra-license.cloudfunctions.net/ulgebraGlobalWebhookRouter?a="+extensionName+"&o="+encodeURIComponent(appsConfig.UA_DESK_ORG_ID)+"&t="+encodeURIComponent(UA_TPA_FEATURES.workflowCode.incoming.ulgebra_webhook_authtoken)+"</textarea>", false, true);
    },
    
    fetchMessagesAndAddToChatBox: async function(isOnlyForNewMID=false){
        var senderId = "";
        var isWhatsAppChannel = false;
        if(UA_APP_UTILITY.currentSender){
            senderId = UA_APP_UTILITY.currentSender.value;
            isWhatsAppChannel = UA_APP_UTILITY.currentSender.channel === UA_APP_UTILITY.MESSAGING_CHANNELS.WHATSAPP;
            if (isWhatsAppChannel) {
                senderId = senderId.replace(/\D/g, '');
            }
        }
        var recipientId = "";
        if(UA_APP_UTILITY.chatCurrentRecipient){
            recipientId = UA_APP_UTILITY.chatCurrentRecipient.address;
        }
        if ((!senderId || !recipientId)) {
            return;
        }
        if(senderId && recipientId){
            UA_APP_UTILITY.CURRENT_CONVERSATION_ID = UA_APP_UTILITY.getCleanStringForHTMLAttribute(senderId+'::'+recipientId);
            UA_APP_UTILITY.initiateRTListeners(senderId+'::'+recipientId);
       }
       if(!isOnlyForNewMID){
            $("#chatbox-message-holder").html("");
            UA_TPA_FEATURES.LOCAL_MESSAGE_STORAGE = {};
       }
       UA_TPA_FEATURES.fetchMessagesAPIAndAddTemp(senderId, recipientId, UA_APP_UTILITY.currentSender.channel.id, isOnlyForNewMID);
    },
    newRTEventReceived: function(CID, MID){ //this only for karix. others: go to twilio or messagebird ref (If you wanna original function method)
        if(!UA_APP_UTILITY.FETCHED_MESSAGES[MID.a]){
            // UA_TPA_FEATURES.fetchMessageByIDAndAddToChatBox(CID, MID.a);
            let oldCount = Object.keys(UA_TPA_FEATURES.LOCAL_MESSAGE_STORAGE).length+1;
            var intervalId = setInterval(()=>{ 
                if(oldCount <= Object.keys(UA_TPA_FEATURES.LOCAL_MESSAGE_STORAGE).length) clearInterval(intervalId);
                UA_TPA_FEATURES.fetchMessagesAndAddToChatBox(true); 
            }, 1000*30);
        }
    },

    runAfterSIDResolved: [],
    
    LOCAL_MESSAGE_STORAGE: {},
    
    fetchMessagesAPIAndAddTemp: async function(senderId, recipientId, channel, isOnlyForNewMID=false){
        // var resp = await UA_TPA_FEATURES.getAPIResponse(extensionName, url, "GET");
        var resp = await UA_SAAS_SERVICE_APP.getRecipientHistorysList(senderId, recipientId, channel);
        if(resp.error){
            showErroWindow('Unable to fetch Twilio conversations', resp.error.message);
        }
        resp.data.messages.forEach(item=>{
            UA_TPA_FEATURES.LOCAL_MESSAGE_STORAGE[new Date(item.date_created).getTime()] = item;
        });
        var sortedItems = Object.keys(UA_TPA_FEATURES.LOCAL_MESSAGE_STORAGE).sort().reverse();
        if(isOnlyForNewMID){
            sortedItems = Object.keys(UA_TPA_FEATURES.LOCAL_MESSAGE_STORAGE).sort().reverse()
        }
        sortedItems.forEach(item=>{
            if(UA_TPA_FEATURES.LOCAL_MESSAGE_STORAGE[item].status && UA_TPA_FEATURES.LOCAL_MESSAGE_STORAGE[item].status.toLowerCase() == "received") {
                UA_TPA_FEATURES.LOCAL_MESSAGE_STORAGE[item].direction = "inbound";
                // item.to = [item.from, item.from = item.to][0];
            }
            else {
                UA_TPA_FEATURES.LOCAL_MESSAGE_STORAGE[item].direction = "outbound";
            }
            if($(`#msgitem-${UA_TPA_FEATURES.LOCAL_MESSAGE_STORAGE[item].id}`).length < 1){
                UA_APP_UTILITY.addMessageInBox(UA_TPA_FEATURES.convertMessageToUASchema(UA_TPA_FEATURES.LOCAL_MESSAGE_STORAGE[item]), isOnlyForNewMID);
            }
        });
        UA_APP_UTILITY.scrollChatBoxToLatest();
        return resp;
    },
    
    convertMessageToUASchema: function(msgObj){
        var messageChannel = UA_APP_UTILITY.MESSAGING_CHANNELS[msgObj.channel.toUpperCase()];
        let originalFrom = msgObj.from;
        let originalTo = msgObj.to;
        msgObj.from = msgObj.from.indexOf('whatsapp') > -1 ? msgObj.from.split(":")[1] : msgObj.from;
        msgObj.to = msgObj.to.indexOf('whatsapp') > -1 ? msgObj.to.split(":")[1] : msgObj.to;
        var attachmentsArr = [];
        if (msgObj.body && (msgObj.body.indexOf("https://rcmmedia.instaalerts.zone/services/media/") > -1 || msgObj.body.indexOf("https://firebasestorage.googleapis.com/") > -1)) {
            let link = msgObj.body.indexOf("https://firebasestorage.googleapis.com/") > -1?"https://firebasestorage.googleapis.com/" + msgObj.body.split("https://firebasestorage.googleapis.com/")[1]:"https://rcmmedia.instaalerts.zone/services/media/" + msgObj.body.split("https://rcmmedia.instaalerts.zone/services/media/")[1];
            msgObj.body = msgObj.body.indexOf("https://firebasestorage.googleapis.com/") > -1?msgObj.body.split("https://firebasestorage.googleapis.com/")[0]:msgObj.body.split("https://rcmmedia.instaalerts.zone/services/media/")[0];
            attachmentsArr.push({
                'type': 'file',
                'url': link
            });
        }
        var modifiedMsgObj = {
            'id': msgObj.id,
            'text': this.getSafeStringForChat(msgObj.body ? msgObj.body : ""),
            'channel': messageChannel,
            'isIncoming': msgObj.direction === "inbound",
            'from': msgObj.from,
            'to': msgObj.to,
            'createdTime': new Date(msgObj.date_sent?msgObj.date_sent:msgObj.date_created).getTime(),
            'status': msgObj.status,
            'isUnRead': msgObj.isUnRead?msgObj.isUnRead: false,
            'attachments': attachmentsArr,
            'conversationID': (originalFrom+"::"+originalTo)
            // 'conversationID': msgObj.direction === "inbound" ? (originalTo+"::"+originalFrom) : (originalFrom+"::"+originalTo)
        };
        // this.addMsgMediaURLAfterResolving(msgObj);
        return modifiedMsgObj;
    },
    
    getSafeStringForChat: function (rawStr) {
        return getSafeString(rawStr);
    },
};