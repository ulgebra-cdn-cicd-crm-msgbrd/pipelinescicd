


// var UA_TPA_FEATURES = {
//     APP_NOTIFICATIONS_COUNT: {},
//     UA_APPS_LIST_FOR_USER: [],
//     UA_SMS_APPS_LIST: ["whatcetra", "messagebird", "twilio", "pinnacle", "telebu", "msg91", "clickatell", "ringcentral", "grapevine", "karix", "mtalkz", "rivet", "routemobile", "sinch", "telnyx", "textlocal", "vonage"],

//     renderInitialElements: async () => {
//         $(".pageContentHolder").html("")
//         $("#suo-item-workflow-nav").hide()
//         $("#ac_name_label_tpa").hide();
//         UA_TPA_FEATURES.startListeningNotificationsCountForUser(console.log);
//         UA_TPA_FEATURES.UA_APPS_LIST_FOR_USER = await UA_TPA_FEATURES.getUserAuthorizedExtentions(currentUser.uid);
//         let appId = localStorage.getItem("ua-allInOneSms-appCode-"+appCodeSAAS);
//         if(appId && appId.indexOf(appCodeSAAS) > -1) UA_TPA_FEATURES.redirectByAppId(null, appId);
//         else {
//             $("#ua-all-in-one-sms-container").show();
//             UA_TPA_FEATURES.renderUserAppsList(UA_TPA_FEATURES.UA_APPS_LIST_FOR_USER, appCodeSAAS);
//         }
//     },

//     getUserAuthorizedExtentions: async function(userId){
//         var userAppsList = [];
//         await db.collection("ulgebraUsers").doc(userId).collection("extensions").get().then(async function(docs){
//             await docs.forEach(doc => {
//                 userAppsList.push(doc.id);
//             });
//             return;
//         });
//         return userAppsList;
//     },

//     startListeningNotificationsCountForUser: async function(callback){
//         if(!firebase.auth().currentUser) return;
//         let NOTIFICATIONS_COUNT_RT_LISTENER = firebase.database().ref(`user_unread_notifications_count/${firebase.auth().currentUser.uid}`);
//         await NOTIFICATIONS_COUNT_RT_LISTENER.on('value', function(snapshot){
//             UA_TPA_FEATURES.APP_NOTIFICATIONS_COUNT = snapshot.val();
//             return callback(UA_TPA_FEATURES.APP_NOTIFICATIONS_COUNT);
//         });
//     },

//     renderUserAppsList: async function(appList, saasName){
//         // if(!appList && appList.length < 1){
//             UA_TPA_FEATURES.renderUltraAppsList(saasName);
//             return;
//         // }
//         appList.push("addNewfor"+saasName);
//         appList.forEach((appId)=>{
//             let tpaName = appId.split("for")[0];
//             if(appId.indexOf(saasName) > -1 && ["calendly", "acuityscheduling", "lookup", "anysms", saasName].includes(tpaName) != true){
//                 UA_TPA_FEATURES.showAppIconInUI(appId, UA_TPA_FEATURES.APP_NOTIFICATIONS_COUNT[appId], "UA_TPA_FEATURES.redirectByAppId");
//             }
//         });
//     },

//     renderUltraAppsList: async function(saasName){        
//         $(".ua-all-in-one-sms-ttl").text("SMS Providers");
//         $("#ua-allInOneSms-apps-holder").empty()
//         UA_TPA_FEATURES.UA_SMS_APPS_LIST.forEach((tpaName)=>{
//             let appId = tpaName + "for" + saasName;
//             UA_TPA_FEATURES.showAppIconInUI(appId, UA_TPA_FEATURES.APP_NOTIFICATIONS_COUNT[appId], "UA_TPA_FEATURES.redirectByAppId");
//         });
//     },

//     showAppIconInUI: async function(appId, notifCount, onclickFunc){
//         notifCount = notifCount && notifCount > 0 ? notifCount : 0;
//         let iconHtmlText = "";
//         let isPlus = false;
//         let tpaName = appId.split("for")[0];
//         if(tpaName && tpaName != "addNew"){            
//             iconHtmlText = `<img class="ua-allInOneSms-appItem-icon" src="https://sms.ulgebra.com/js/chrome-extentions/floating-app/apps_logo/${tpaName}.png">`;
//         }
//         else{
//             isPlus = true;
//             onclickFunc = "UA_TPA_FEATURES.renderUltraAppsList";
//             iconHtmlText = `<div class="ua-allInOneSms-appItem-add">+</div>`;
//         }
//         $("#ua-allInOneSms-apps-holder").append(`<div class="ua-allInOneSms-appItem" onclick="${onclickFunc}(event, '${appId}')">
//                                                     <div class="ua-allInOneSms-appItem-notif" style="display:${notifCount?"block":"none"}">
//                                                         <div class="">${notifCount}</div>
//                                                     </div>
//                                                     <div class="ua-allInOneSms-appItem-icon-holder">
//                                                         ${iconHtmlText}
//                                                     </div>
//                                                     <div class="ua-allInOneSms-appItem-name">${isPlus?"Add":appPrettyNameMap[tpaName]}</div>
//                                                 </div>`);
//     },

//     redirectByAppId: async function(event, appId){
//         localStorage.setItem("ua-allInOneSms-appCode-"+appCodeSAAS, appId);
//         let loc = location.href.toString().replace(extensionName, appId);
//         location.href = loc + "&anysms=true";
//     }

// }