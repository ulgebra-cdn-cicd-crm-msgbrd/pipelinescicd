var UA_TPA_FEATURES = {
    chosenMessgeTemplate : null,
    'clientIds': {
        'pinnacleforhubspotcrm' : "f30b42b5-9aca-43df-8da5-fc91ac136ed0",
        'pinnacleforbitrix24' : "app.628ca9f370b123.27655410",
        'pinnacleforpipedrive': '7e31a48b6f3c4606',
        'pinnacleforshopify': '8e760b4d84de29b76018581e4c0f2598'
    },
    workflowCode : {
        "SMS": {
            "sender": "FILL_HERE",
            "messagetype": "TXT",
            "message":[
                    {
                        "number": "FILL_HERE",
                        "text":"FILL_HERE" 
                    }
            ],
            "module": "FILL_HERE",
            "recordId": "FILL_HERE",
            "channel": "SMS",
            "ulgebra_webhook_authtoken": null
        },
        "WhatsApp":{
            "from": "FILL_HERE",
            "to": "FILL_HERE",
            "type": "template",
            "message": {
              "templateid": "FILL_HERE",
              "url": "FILL_HERE",
              "placeholders": [
                "FILL_HERE",
                "FILL_HERE"
              ]
            },
            "module": "FILL_HERE",
            "recordId": "FILL_HERE",
            "channel": "WhatsApp",
            "ulgebra_webhook_authtoken": null
        }
    },
    getSupportedChannels: function(){
        return [ UA_APP_UTILITY.MESSAGING_CHANNELS.SMS, UA_APP_UTILITY.MESSAGING_CHANNELS.WHATSAPP ];
    },
    SMS_AUTHENTICATION_FAILED: false,
    renderInitialElements: async function(){
        UA_APP_UTILITY._feature_showUAMsgTemplates = false;
        var approvedTemplateDDId = null;
        $("#ac_name_label_tpa .anl_servicename").text('Pinnacle');

        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "http://api.pinnacle.in/index.php/checkbalance", "GET", {});
        if((response.code === 401 || response.message === "Authentication failed") || (response.data && (response.data.code === 401 || response.data.message === "Authentication failed" || response.data.status === "Authentication failed"))){
            UA_TPA_FEATURES.SMS_AUTHENTICATION_FAILED = true;
        }
        else{
            $('#ac_name_label_tpa .ac_name_id').text('Pinnacle Balance : '+response.data.data.balance);
        }
        
        if(!UA_TPA_FEATURES.SMS_AUTHENTICATION_FAILED){
        var fetchedTemplatesResponse = await UA_TPA_FEATURES._getServiceTemplates();
        if((fetchedTemplatesResponse.code === 401 || fetchedTemplatesResponse.message === "Authentication failed") || (fetchedTemplatesResponse.data && (fetchedTemplatesResponse.data.code === 401 || fetchedTemplatesResponse.data.message === "Authentication failed"))){
            if(UA_TPA_FEATURES.SMS_AUTHENTICATION_FAILED){
                UA_TPA_FEATURES.showReAuthorizeERROR();
            }
            else{
                fetchedTemplatesCollection = {
                    data: {
                        data: []
                    }
                }
            }
        }
        // console.log('fetchedtemoktes ', fetchedTemplatesResponse);
        fetchedTemplatesCollection = fetchedTemplatesResponse.data.data;
        
        var sendersArray = [];
        var sendersUniqueArray = [];
        var templatesArray = [];
        fetchedTemplatesCollection.forEach(item=>{
            UA_APP_UTILITY.fetchedTemplates[item.id] = {
                'message': item.message
            };
            templatesArray.push({
                'label': item.name + ' (SMS)',
                'value': item.id
            });
            if(sendersUniqueArray.indexOf(item.senderid) === -1){
                sendersUniqueArray.push(item.senderid);
                UA_APP_UTILITY.addMessaegSender(item.senderid, UA_APP_UTILITY.MESSAGING_CHANNELS.SMS, item.senderid);
                sendersArray.push({
                    'label': item.senderid,
                    'value': item.senderid
                });
            }
        });
        
        approvedTemplateDDId = UA_APP_UTILITY.renderSelectableDropdown('#ssf-fitem-template-var-holder', 'Insert Pinnacle template', templatesArray, 'UA_TPA_FEATURES.insertTemplateContentInMessageInput', false, false);
        }
        UA_APP_UTILITY.TPA_SENDERS_FETCH_COMPLETED();
        
        
        UA_APP_UTILITY.getPersonalWebhookAuthtoken();
       
        var countryCallCodeArray = [];
        UA_APP_UTILITY.countryCallingCodeArray.forEach(item=>{
            countryCallCodeArray.push({
                'label': `${item.name} (${item.dial_code})`,
                'value': item.dial_code
            });
        });
        
        UA_APP_UTILITY.renderSelectableDropdown('#ssf-new-recip-countrycode', 'Select country', countryCallCodeArray, 'UA_APP_UTILITY.log');
   
        var fetchedTemplatesWAResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://console.pinbot.ai/api/fetch-template", "post");
        if((fetchedTemplatesWAResponse.code === 401 || fetchedTemplatesWAResponse.message === "Authentication failed") || (fetchedTemplatesWAResponse.data && (fetchedTemplatesWAResponse.data.code === 401 || fetchedTemplatesWAResponse.data.message === "Authentication failed"))){
            if(UA_TPA_FEATURES.SMS_AUTHENTICATION_FAILED){
                UA_TPA_FEATURES.showReAuthorizeERROR();
                return false;
            }
            else{
                fetchedTemplatesCollection = {
                    data: {
                        data: []
                    }
                }
            }
        }
        if(UA_TPA_FEATURES.SMS_AUTHENTICATION_FAILED && fetchedTemplatesWAResponse.data.data){
            $('#ac_name_label_tpa .ac_name_id').text(fetchedTemplatesWAResponse.data.data.length+ ' WhatsApp templates found');
        }
        // console.log('wa fetchedtemoktes ', fetchedTemplatesWAResponse);
         var waTemplatesArray = [];
         if(fetchedTemplatesWAResponse.data && fetchedTemplatesWAResponse.data.data){
        fetchedTemplatesWAResponse.data.data.forEach(item=>{
            UA_APP_UTILITY.fetchedTemplates[item.tempid] = {
                'message': item.body_message,
                'templateType': 'placeholder_template',
                'placeholders': item.placeholders ? item.placeholders.split(',') : [],
                'tempid': item.tempid,
                'head_media_url': item.head_media_url,
                'head_mediatype': item.head_mediatype
            };
            waTemplatesArray.push({
                'label': item.temptitle + ' (WhatsApp)',
                'value': item.tempid
            });
        });
        
        UA_APP_UTILITY.renderSelectableDropdown('#ssf-fitem-template-var-holder', 'Insert Pinnacle template', waTemplatesArray, 'UA_TPA_FEATURES.insertTemplateContentInMessageInput', false, false, approvedTemplateDDId);
    }
        
        
    },
    
    renderWorkflowBodyCode :function(accessToken){
        
        UA_TPA_FEATURES.workflowCode.SMS.ulgebra_webhook_authtoken = accessToken.saas;
        UA_TPA_FEATURES.workflowCode.WhatsApp.ulgebra_webhook_authtoken = accessToken.saas;
        
        UA_APP_UTILITY.addWorkflowBodyCode('sms', 'For Sending SMS', UA_TPA_FEATURES.workflowCode.SMS);
        
        UA_APP_UTILITY.addWorkflowBodyCode('whatsapp', 'For Sending WhatsApp', UA_TPA_FEATURES.workflowCode.WhatsApp);
    },
    
    
    
    prepareAndSendSMS: function(){
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        if((UA_APP_UTILITY.currentSender && UA_APP_UTILITY.currentSender.channel && UA_APP_UTILITY.currentSender.channel.id == "WHATSAPP") || (UA_TPA_FEATURES.chosenMessgeTemplate && UA_TPA_FEATURES.chosenMessgeTemplate.templateType === "placeholder_template")){
            return UA_TPA_FEATURES.prepareAndSendBulkWhatsApp();
        }
        if(!UA_APP_UTILITY.currentSender){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var senderId = UA_APP_UTILITY.currentSender.value;
        if(!valueExists(senderId)){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var recipNumbers = UA_APP_UTILITY.getCurrentMessageRecipients();
        if(recipNumbers.length === 0){
            showErroWindow('Recipient list is empty!', "Kindly add recipients to send SMS.");
            return false;
        }
        var messageText = $("#inp-ssf-main-message").val().trim();
        var messageNumberMapArray = [];
        var messageHistoryMap = {};
        recipNumbers.forEach(item=>{
            let resolvedMessageText = UA_APP_UTILITY.getTemplateAppliedMessage(messageText, UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id]);
            let historyUID = item.id+''+new Date().getTime()+''+Math.round(Math.random(100000,99999)*1000000);
            messageNumberMapArray.push({
                'number': item.number,
                'text': resolvedMessageText,
                'clientuid': historyUID
            });
            messageHistoryMap[historyUID] = {
                'contact':{
                    'name': UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].Full_Name : item.number
                },
                'from': senderId,
                'to': item.number,
                'message': resolvedMessageText,
                'status': 'NOT_SENT',
                'module': UA_SAAS_SERVICE_APP.widgetContext.module,
                'moduleId': item.id,
                'channel': "SMS"
            };
        });
        
        if(!valueExists(messageText)){
            showErroWindow('Message is empty!', "Kindly fill message to proceed.");
            return false;
        }
        
        if(recipNumbers.length === 0){
            showErroWindow('Recipient list is empty!', "Kindly add recipients to send SMS.");
            return false;
        }
        
        UA_TPA_FEATURES.sendBulkSMS(senderId, messageNumberMapArray, (function(apiResponse){
            if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
                   return false;
               }
            if(apiResponse.data.code !== 200){
                showErroWindow('Unable to send message', apiResponse.data.status);
                return;
            }
            else{
                showErroWindow('Completed', `Your ${apiResponse.data.data.length}/${messageNumberMapArray.length} messages has been sent successfully.`, false, true);
                UA_APP_UTILITY.resetMessageFormFields();
                // console.log(apiResponse);
                apiResponse.data.data.forEach(item=>{
                    messageHistoryMap[item.clientuid].status = "SENT";
                });
                UA_SAAS_SERVICE_APP.addSentSMSAsRecordInHistory(messageHistoryMap);
            }
        }));
        
    },
    
    
    
    
    
    insertTemplateContentInMessageInput :function(templateId){
        var curTemplateItem = UA_APP_UTILITY.fetchedTemplates[templateId];
        UA_TPA_FEATURES.chosenMessgeTemplate = curTemplateItem;
        $("#messageAttachmentInputHolder").hide();
        UA_APP_UTILITY.currentAttachedFile = null;
        $("#attachedfile").text('Attach File');
        $('#inputFileAttach').val('');
        if(curTemplateItem.templateType === "placeholder_template"){
            $("#primary-send-btn").text('Send WhatsApp').css('background-color', 'green');
            if(curTemplateItem.head_mediatype){
                switch (curTemplateItem.head_mediatype) {
                    case "0":
                        $("#attachedfile").text('Attach Document');
                        break;
                    case "1":
                        $("#attachedfile").text('Attach Image');
                        break;
                    case "2":
                        $("#attachedfile").text('Attach Video');
                        break;
                    default:
                        $("#attachedfile").text('Attach File');
                        break;
                }
            }
            if(curTemplateItem.head_media_url){
                $("#messageAttachmentInputHolder").show();
            }
            $("#ssf-fitem-template-placeholder-holder-listholder").html("");
            curTemplateItem.placeholders.forEach(item=>{
                $("#ssf-fitem-template-placeholder-holder-listholder").append(`
                    <div class="ssf-temp-placehold-item">
                            <span class="ssf-temp-placehold-item-label">{{${item}}}</span> <span class="material-icons ssf-temp-placehold-item-icon">double_arrow</span> <input onblur="UA_APP_UTILITY.lastFocusedInputElemForPlaceholderInsert = 'ssf-templ-place-input-${item}'" id="ssf-templ-place-input-${item}" data-placeholder-id="${item}" class="ssf-temp-placehold-item-input" type="text" placeholder="Type or choose field from above">
                        </div>
                `);
            });
            if(curTemplateItem.placeholders.length > 0){
                $("#ssf-fitem-template-placeholder-holder").show();
            }else{
                $("#ssf-fitem-template-placeholder-holder").hide();
            }
            $('#inp-ssf-main-message').attr('readonly', true);
        }
        else{
            $("#primary-send-btn").text('Send SMS').css('background-color', 'royalblue');
            $("#ssf-fitem-template-placeholder-holder").hide();
            $('#inp-ssf-main-message').removeAttr('readonly');
        }
        $('#inp-ssf-main-message').val(curTemplateItem.message).focus();
    },
    
    _getServiceTemplates: async function(){
        var templatesResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://api.pinnacle.in/index.php/getdlttemplate", "post");
        if(templatesResponse.code === 401 || templatesResponse.data.code === 401 || templatesResponse.data.message === "Authentication failed"){
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return;
        }
        return  templatesResponse;
    },
    
    _getServiceWATemplates: async function(silenceAuthError = false){
        var templatesResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://console.pinbot.ai/api/fetch-template", "post");
        if(templatesResponse.code === 401 || templatesResponse.data.code === 401 || templatesResponse.data.message === "Authentication failed"){
            if(silenceAuthError){
                return {
                    data:{
                        data: []
                    }
                };
            }
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return;
        }
        return  templatesResponse;
    },
    
    showReAuthorizeERROR: function(noCloseOption = false){
        if($('#inp_tpa_api_key').length > 0){
//            console.log('already showing reautherr');
            return;
        }
        showErroWindow("Authorization needed!", `You need to authorize your Pinnacle Account to proceed. <br><br> <div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
        <div style="font-size: 18px;margin-bottom: 5px;margin-top: 20px;">Pinnacle SMS API Key</div>
        <div class="help_apikey_tip" style="cursor: default;"><a>Contact : </b>support@pinnacle.in</a></div>
        <input id="inp_tpa_api_key" type="text" placeholder="Enter your API Key" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
        
        <div style="font-size: 18px;margin-bottom: 10px;margin-top: 10px;">Pinnacle WABA API Key</div>
        <div class="help_apikey_tip" style="cursor: default;"><a>Contact : </b>support@pinnacle.in</a></div>
        <input id="inp_tpa_waba_api_key" type="text" placeholder="Enter your WABA API Key" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
        <button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.saveTPAAPIKey(UA_APP_UTILITY.reloadWindow)">Authorize</button>
    </div>`, true, true, 500);
        $('#inp_tpa_api_key').focus();
        UA_LIC_UTILITY.showExistingInvitedAdminDDHTML(true);
    },
    
    openWABAAPIKey: function(showCloseOption = true){
        showErroWindow("Authorization needed!", `Enter your WABA API key. <br><br> <div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
        <div style="font-size: 18px;margin-bottom: 5px;margin-top: 20px;">Pinnacle WABA ID</div>
        <div class="help_apikey_tip" style="cursor: default;"><a>Contact : </b>support@pinnacle.in</a></div>
        <input id="inp_tpa_waba_api_key" type="text" placeholder="Enter your WABA API Key" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;"><button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.saveTPAWABAAPIKey(UA_APP_UTILITY.reloadWindow)">Authorize Now</button>
    </div>`, showCloseOption);
        $('#inp_tpa_api_key').focus();
    },
    
    showReAuthorizeERRORWithClose: function(){
        UA_TPA_FEATURES.showReAuthorizeERROR();
    },
    
    saveTPAAPIKey: async function(callback){
        let authtoken = $("#inp_tpa_api_key").val();
        // if(!valueExists(authtoken)){
        //     showErroMessage("Please Enter API Key");
        //     return;
        // }
        let whatsAppWABAID = $("#inp_tpa_waba_api_key").val();
       if(!valueExists(authtoken) && !valueExists(whatsAppWABAID) ){
           showErroMessage("Please fill SMS API Key or Pinnacle WABA ID");
           return;
       }
        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', '__multiple_keys__', {
            'authtoken': authtoken,
            'waba_authtoken': whatsAppWABAID ? whatsAppWABAID : ''
        }, callback);
    },
    
    saveTPAWABAAPIKey: async function(callback){
        let authtoken = $("#inp_tpa_waba_api_key").val();
        if(!valueExists(authtoken)){
            showErroMessage("Please Enter WABA ID");
            return;
        }
        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', 'waba_authtoken', authtoken, callback);
    },
    
    
    
    prepareAndSendBulkWhatsApp : function(){
        
        if(!UA_APP_UTILITY.currentSender){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var senderId = UA_APP_UTILITY.currentSender.value;
        if(!valueExists(senderId)){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var messageText = $("#inp-ssf-main-message").val().trim();
        if(!valueExists(messageText)){
            showErroWindow('Message is empty!', "Kindly fill message to proceed.");
            return false;
        }
        if(!UA_TPA_FEATURES.chosenMessgeTemplate || !UA_TPA_FEATURES.chosenMessgeTemplate.tempid) {
            showErroWindow('You are not allowed to send free form messages!', "Kindly select a approved WhatsApp template to proceed.");
            return false;
        }
        var recipNumbers = UA_APP_UTILITY.getCurrentMessageRecipients();
        if(recipNumbers.length === 0){
            showErroWindow('Recipient list is empty!', "Kindly add recipients to send SMS.");
            return false;
        }
        if(!UA_APP_UTILITY.fileUploadComplete){
            showErroWindow('File upload is not complete!', "Please wait while your file is uploading and try again.");
            return;
        }
        var messageNumberMapArray = [];
        var messageHistoryMap = {};
        $('#sms-prog-window').show();
            $('#msgItemsProgHolder').html('');
          var totalCount = recipNumbers.length;
          $('#totMsgDisp').text(`Sending messages 0/${totalCount}...`);
          var messagesSentCount = 0;
          var messageStatusMap = {
              'success': 0,
              'failed' : 0
          };
        recipNumbers.forEach(item=>{
            let message = {
                "templateid": UA_TPA_FEATURES.chosenMessgeTemplate.tempid
            };
            if(UA_APP_UTILITY.currentAttachedFile && UA_APP_UTILITY.currentAttachedFile.mediaUrl){
                message.url = UA_APP_UTILITY.currentAttachedFile.mediaUrl;
            }
            let contactRecordId = UA_APP_UTILITY.storedRecipientInventory[item.id] ? item.id : null;
            let resolvedMessageText = messageText + (message.url ? ' '+message.url : '');
            if(UA_TPA_FEATURES.chosenMessgeTemplate.placeholders){
                message.placeholders = [];
                UA_TPA_FEATURES.chosenMessgeTemplate.placeholders.forEach(item=>{
                    let placeHolderValue = $('#ssf-templ-place-input-'+item).val();
                    //if(contactRecordId){
                        placeHolderValue = UA_APP_UTILITY.getTemplateAppliedMessage(placeHolderValue, UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[contactRecordId]);
                    //}
                    resolvedMessageText = resolvedMessageText.replace('{{'+item+'}}', placeHolderValue);
                    message.placeholders.push(placeHolderValue);
                });
            }
            let messagePayload = {
                "from": senderId,
                "to": item.number,
                "type": "template",
                "message": message
            };
            messageHistoryMap = {
                'contact':{
                    'name': UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].Full_Name : item.number
                },
                'from': senderId,
                'to': item.number,
                'message': resolvedMessageText,
                'status': 'NOT_SENT',
                'module': UA_SAAS_SERVICE_APP.widgetContext.module,
                'moduleId': item.id,
                'channel': 'WhatsApp'
            };
            $('#msgItemsProgHolder').append(`
              <div id="msg-resp-item-${parseInt(item.number)}" class="msgRespItem" title="${getSafeString(resolvedMessageText)}">
                  <div class="mri-label"><b>${$('.msgRespItem').length+1}</b>. WhatsApp to ${UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].name+'-' : ''} ${item.number}</div>
                  <div class="mri-status">Sending...</div>
              </div>
          `);
            UA_TPA_FEATURES._proceedToSendWAAndExecuteCallback(messagePayload, function(response){
                if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
                   return false;
               }
                // console.log(response);
                messagesSentCount++;
                if(!response)
                {
                    $('#totMsgDisp').text(`Unable to send messages, Kindly retry.`);
                    showErroMessage(`Kindly ensure the given Sender ID is approved to send the given Message/Template in Pinnacle.`);
                    return ;
                }
                if(response.data && response.data.code == "WA1003" && (message.placeholders && message.placeholders.includes('') == true)) { 
                    $(`#msg-resp-item-${parseInt(item.number)} .mri-status`).text(response.data.status+' - Missing placeholders value( '+response.data.message+ ')').css({'color': response.data.status === "SUCCESS" ? 'green' : 'crimson'});
                }
                else {
                    $(`#msg-resp-item-${parseInt(item.number)} .mri-status`).text(response.data.status+' - '+response.data.message).css({'color': response.data.status === "SUCCESS" ? 'green' : 'crimson'});
                }
                response.data.status === "SUCCESS" ? messageStatusMap.success++ : messageStatusMap.failed++;
                if(messagesSentCount === totalCount){
                    $('#totMsgDisp').text(`All messages have been processed. TOTAL: ${totalCount}, SENT: ${messageStatusMap.success}, FAILED: ${messageStatusMap.failed}`);
                }else{
                    $('#totMsgDisp').text(`Sending messages ${messagesSentCount}/${totalCount}...`);
                }
                messageHistoryMap.status = response.data.status === "SUCCESS" ? "SUCCESS" : response.data.status+"-"+response.data.message;
                UA_SAAS_SERVICE_APP.addSentSMSAsRecordInHistory({
                    "message1": messageHistoryMap
                });
            });
        });
        
        
    },
    
    sendSMS : function(sender, number, text, callback){
        
        var messageArray = [{
            "number": number,
            "text": text
        }];
        
        var messagePayload = {
            "sender": sender,
            "message": messageArray,
            "messagetype": "TXT"
        };
        
        UA_TPA_FEATURES._proceedToSendSMSAndExecuteCallback(messagePayload, callback);
        
    },
    
    sendBulkSMS : function(sender, numberMessageMapArray, callback){
        
        var messagePayload = {
            "sender": sender,
            "message": numberMessageMapArray,
            "messagetype": "TXT"
        };
        
        UA_TPA_FEATURES._proceedToSendSMSAndExecuteCallback(messagePayload, callback);
        
    },
    
    _proceedToSendSMSAndExecuteCallback: async function(messagePayload, callback){
        if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        var sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "http://api.pinnacle.in/index.php/sms/json", "POST", messagePayload);
        callback(sentAPIResponse);
    },
    
    _proceedToSendWAAndExecuteCallback: async function(messagePayload, callback){
        if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        var sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://api.pinbot.ai/v1/wamessage/sendMessage", "POST", messagePayload);
        callback(sentAPIResponse);
    },
    
    getAPIResponse: async function (extensionName, url, method, data) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = {"Content-Type": "application/json"};
        if(url.startsWith("https://console.pinbot.ai") || url.startsWith("https://api.pinbot.ai")){
            headers.apikey = "{{WABA_APIKEY}}";
        }
        await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, TPAService: "pinnacle",url: url, method: method, data: data, headers: headers, _ua_lic_adminUserID: UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID}).then((response) => {
            response = response.data;
            returnResponse = response;
            removeTopProgressBar(credAdProcess3);
            return true;
        }).catch(err => {
//            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
        return returnResponse;
    },
    
    getCurrentAccountOrBalanceInfo: async function(callback){
        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "http://api.pinnacle.in/index.php/checkbalance", "GET", {});
        if((response.code === 401 || response.message === "Authentication failed") || (response.data && (response.data.code === 401 || response.data.message === "Authentication failed"))){
            UA_TPA_FEATURES.SMS_AUTHENTICATION_FAILED = true;
            callback(null);
        }
        callback(response);
    }
    
};