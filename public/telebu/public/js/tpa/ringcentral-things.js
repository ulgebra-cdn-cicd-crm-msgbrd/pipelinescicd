var UA_TPA_FEATURES = {
    chosenMessgeTemplate : null,
    'clientIds': {
        'ringcentralforhubspotcrm' : "91ef3af9-c7b9-40a7-bbe9-650fd7ab5323",
        'ringcentralforbitrix24' : "app.62bb49940a51d9.75471153",
        'ringcentralforpipedrive' : "07e6bff456896e53"
    },
    getSupportedChannels: function(){
        return [ UA_APP_UTILITY.MESSAGING_CHANNELS.SMS ];
    },
    "ringcentralClient" :{
        "clientId" : "bCvr43liSMGvmExjTPbZug"
    },
    tpaSortName: "RingCentral",
    workflowCode : {
        "SMS": {
            "from": {"phoneNumber": "FILL_HERE"},
            "to":[
                {"phoneNumber": "FILL_HERE"}
            ],
            "text": "FILL_HERE",
            "module": "FILL_HERE",
            "recordId": "FILL_HERE",
            "channel": "SMS",
            "ulgebra_webhook_authtoken": null
        }
    },
    renderInitialElements: async function() {

        // await UA_TPA_FEATURES.checkFormViewResize();

        $(".incoming-config-tpa-entity").text('messages');
        
        $("#ac_name_label_tpa .anl_servicename").text(UA_TPA_FEATURES.tpaSortName);
        UA_TPA_FEATURES.getCurrentAccountOrBalanceInfo(function(resp){
            $('#ac_name_label_tpa .ac_name_id').text('Service : '+resp.data.serviceInfo.servicePlan.name);
        });
        
        var fetchedPhoneNumbersList = await UA_TPA_FEATURES._getfetchedPhoneNumbersList();
        
        var sendersArray = [];
        var sendersUniqueArray = [];
        fetchedPhoneNumbersList.forEach(item=>{
            if(sendersUniqueArray.indexOf(item.phoneNumber) === -1){
                sendersUniqueArray.push(item.phoneNumber);
                sendersArray.push({
                    'label': item.usageType,
                    'value': item.phoneNumber
                });
                UA_APP_UTILITY.addMessaegSender(item.phoneNumber, UA_APP_UTILITY.MESSAGING_CHANNELS.SMS, item.usageType+" ("+item.phoneNumber+")");
            }
        });
        UA_APP_UTILITY.TPA_SENDERS_FETCH_COMPLETED();
        UA_APP_UTILITY.getPersonalWebhookAuthtoken();
        
        var countryCallCodeArray = [];
        UA_APP_UTILITY.countryCallingCodeArray.forEach(item=>{
            countryCallCodeArray.push({
                'label': `${item.name} (${item.dial_code})`,
                'value': item.dial_code
            });
        });
        
        UA_APP_UTILITY.renderSelectableDropdown('#ssf-new-recip-countrycode', 'Select country', countryCallCodeArray, 'UA_APP_UTILITY.log');
   
        
    },
    checkFormViewResize:async function() {
        try{
            if(!Object.keys(UA_SAAS_SERVICE_APP.CURRENT_USER_INFO).length) {
                UA_TPA_FEATURES.checkFormViewResize();
                return;
            }

            if(extensionName === 'ringcentralforzohocrm') {
                UA_APP_UTILITY.currentMessagingView = UA_APP_UTILITY.appPrefinedUIViews.MESSAGE_FORM
                await UA_SAAS_SERVICE_APP.changedCurrentView();
            }
        }
        catch(ex){console.log(ex);}
    },
    renderWorkflowBodyCode :async function(accessToken){
        
        UA_TPA_FEATURES.workflowCode.SMS.ulgebra_webhook_authtoken = accessToken.saas;
        //UA_TPA_FEATURES.workflowCode.WhatsApp.ulgebra_webhook_authtoken = accessToken.saas;
        
        UA_APP_UTILITY.addWorkflowBodyCode('sms', 'For Sending SMS', UA_TPA_FEATURES.workflowCode.SMS);
        //UA_APP_UTILITY.addWorkflowBodyCode('whatsapp', 'For Sending WhatsApp', UA_TPA_FEATURES.workflowCode.WhatsApp);

    },
    ulgON_OFFToggleDiv : async function(ulgToggle, ulgToggleParent, toggleButtSize, toggleActionFunctionOn, toggleActionFunctionOff, isOff) {      

        let toggleOn = `<path xmlns="http://www.w3.org/2000/svg" d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm0 16H5V5h14v14zM17.99 9l-1.41-1.42-6.59 6.59-2.58-2.57-1.42 1.41 4 3.99z"/>` 
        let toggleOff = `<path style="fill: #727f8c;" d="M19 5v14H5V5h14m0-2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2z"/>` 
        
        $(ulgToggleParent).append(`<svg class="${ulgToggle}" xmlns="http://www.w3.org/2000/svg" height="${toggleButtSize}" viewBox="0 0 24 24" width="${toggleButtSize}" fill="#000000" style="transition: 0.3s; cursor: pointer; fill: #1e5f9f; display: inline-block; margin-right: -10px; position: relative; right: 10px;">
            ${isOff ? toggleOff : toggleOn}
        </svg>`);

        $('<script>').attr('type', 'text/javascript').text(`
async function ${ulgToggle}ActFunction(selected) {
    if($(selected).attr('toggle') == 'on') {
        UA_TPA_FEATURES.wcConfirm('<div style="width: 100%; height: 100px; display: flex; align-items: center; justify-content: center;flex-direction: column; justify-content: center;"><div class="tpaIncomingLoderSet"></div><div class="tpaIncomingTextSet" style="font-size: 13px; height: 0px; position: relative; top: 8px;">Webhook Removing...</div></div>','','Okay',false,true);
        UA_TPA_FEATURES.ulgloaderDiv(".tpaIncomingLoderSet", "30px");
        if(await ${toggleActionFunctionOff}) {
            $(selected).attr('toggle', 'off');
            $('.incomingWebhookStatus').text('Disabled');
            $('.incomingCreateModuleDiv').hide();
            $(selected).find('.${ulgToggle}').html('${toggleOff}').hide().show(100);
            $('.tpaIncomingTextSet').text('Webhook removed');
            setTimeout(function() {UA_TPA_FEATURES.wcConfirmHide();}, 1500);
        }
        else {
            $('.incomingCreateModuleDiv').show();
            $('.tpaIncomingTextSet').text('Webhook delete failed');
            setTimeout(function() {UA_TPA_FEATURES.wcConfirmHide();}, 1500);
        }
    }
    else {
        UA_TPA_FEATURES.wcConfirm('<div style="width: 100%; height: 100px; display: flex; align-items: center; justify-content: center;flex-direction: column; justify-content: center;"><div class="tpaIncomingLoderSet"></div><div class="tpaIncomingTextSet" style="font-size: 13px; height: 0px; position: relative; top: 8px;">Webhook Creating...</div></div>','','Okay',false,true);
        UA_TPA_FEATURES.ulgloaderDiv(".tpaIncomingLoderSet", "30px");
        if(await ${toggleActionFunctionOn}) {
            $(selected).attr('toggle', 'on');
            $('.incomingWebhookStatus').text('Enabled');
            $('.incomingCreateModuleDiv').show();
            $(selected).find('.${ulgToggle}').html('${toggleOn}').hide().show(100);
            $('.tpaIncomingTextSet').text('Webhook Created');
            setTimeout(function() {UA_TPA_FEATURES.wcConfirmHide();}, 1500);
        }
        else {
            $('.incomingCreateModuleDiv').hide();
            $('.tpaIncomingTextSet').text('Webhook Create failed');
            setTimeout(function() {UA_TPA_FEATURES.wcConfirmHide();}, 1500);
        }
    }
}`).prependTo(ulgToggleParent);

    },
    ulgloaderDiv : async function(ulgLoaderParent, ulgLoaderSize) {

        $(ulgLoaderParent).append(`<div class="loaderOuterDiv"><div class="wcMsgLoadingInner" title="loading…"><svg class="wcMsgLoadingSVG" width="calc(${ulgLoaderSize}/2)" height="calc(${ulgLoaderSize}/2)" viewBox="0 0 46 46" role="status"><circle class="wcMsgLoadingSvgCircle" cx="23" cy="23" r="20" fill="none" stroke-width="6" style="stroke: rgb(37 162 94);"></circle></svg></div></div>`);

        let ulg_loader_popup_css = `/* message loading style */

.wcMsgLoadingInner {
    margin: 0 auto;
    background-color: #fff;
    border-radius: 50%;
    box-shadow: 0 1px 1px 0 rgba(0,0,0,.06),0 2px 5px 0 rgba(0,0,0,.2);
    display: flex;
    align-items: center;
    justify-content: center;
    width: ${ulgLoaderSize};
    height: ${ulgLoaderSize};
    color: rgba(0,0,0,0.25);
}

.wcMsgLoadingSvgCircle {
    stroke: #ccc;
    stroke-dasharray: 1,150;
    stroke-dashoffset: 0;
    stroke-linecap: round;
    animation: wcMsgLoadingSvgCircle 1.5s ease-in-out infinite;
}

.wcMsgLoadingSVG {
    animation: wcMsgLoadingSVG 2s linear infinite;
}

/* message loading style */

@keyframes wcMsgLoadingSVG{
    to{transform:rotate(1turn)}
}

@keyframes wcMsgLoadingSvgCircle{
    0%{stroke-dasharray:1,150;stroke-dashoffset:0}
    50%{stroke-dasharray:90,150;stroke-dashoffset:-35}
    to{stroke-dasharray:90,150;stroke-dashoffset:-124}
}`;

        $('<style>').attr('type', 'text/css').text(ulg_loader_popup_css).prependTo('.loaderOuterDiv');        

    },
    wcConfirm : async function(htmlText, runFunc, confirmTitle, alart, buttonHide) {

        $('.wcConfirmOuter').remove();
        $('body').append(`<div class="wcConfirmOuter">
            <div class="wcConfirmInner divPopUpShow">
                <div class="wcConfirmBody">
                    ${htmlText}
                </div>
                <div class="wcConfirmButt" style="display:${buttonHide?'none':'flex'};">
                    <div class="wcConfirmCancel" style="display:${alart?'none':'block'};" onclick="ulg_style_things.wcConfirmHide()">Cancel</div>
                    <div class="wcConfirmYes" onclick="UA_TPA_FEATURES.wcConfirmHide();${runFunc}">${confirmTitle}</div>
                </div>
            </div>
        </div>`);

        let ulg_confirm_popup_css = `/* Alart function style */

.wcConfirmOuter {
    position: fixed;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    background-color: #36363680;
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    align-content: center;
    justify-content: center;
    align-items: center;
    z-index: 200;
}

.wcConfirmInner {
    position: absolute;
    min-width: 417px;
    max-width: calc(100% - 500px);
    min-height: 100px;
    max-height: max-content;
    border-radius: 5px;
    background-color: #f7f7f7;
    display: flex;
    justify-content: flex-start;
    align-content: space-between;
    align-items: flex-end;
    flex-wrap: nowrap;
    flex-direction: column;
    box-shadow: 0px 2px 12px #767676;
}

.wcConfirmBody {
    position: relative;
    width: 100%;
    height: max-content;
    min-height: 50px;
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    align-content: center;
    align-items: center;
    box-sizing: border-box;
    padding: 20px 30px;
    justify-content: flex-start;
}

.wcConfirmButt {
    position: relative;
    display: flex;
    justify-content: flex-end;
    width: 100%;
    align-items: flex-start;
    flex-wrap: nowrap;
    align-content: center;
    flex-direction: row;
    height: 54px;
    /* right: 20px; */
    padding-right: 20px;
    /* bottom: 20px; */
    box-sizing: border-box;
}

.wcConfirmYes {
    background-color: #3f4fcc;
    color: white;
    box-sizing: border-box;
    padding: 2px 10px;
    border-radius: 3px;
    margin: 5px 5px;
    box-shadow: inset 0px 0px 11px #4244707a;
    cursor: pointer;
}

.wcConfirmCancel {
    background-color: #d6d6d6;
    color: #525252;
    box-sizing: border-box;
    padding: 2px 10px;
    border-radius: 3px;
    margin: 5px 5px;
    box-shadow: inset 0px 0px 11px #c5c5c57a;
    cursor: pointer;
}

/* Alart function style */`;

        $('<style>').attr('type', 'text/css').text(ulg_confirm_popup_css).prependTo('.wcConfirmOuter');

        $('<script>').attr('type', 'text/javascript').text(`
        $(document).keypress(function(e) {

            if($('.wcConfirmYes').is(':visible'))
            if(e.which == 13) {
                $('.wcConfirmYes').click();
                e.preventDefault();
            }

        });`).prependTo('.wcConfirmOuter');
        

    },
    wcConfirmHide : async function() {

        $('.wcConfirmInner').removeClass('divPopUpHide').removeClass('divPopUpShow').addClass('divPopUpHide').hide();
        $('.wcConfirmOuter').fadeOut(0);
        await setTimeout(function() { $('.wcConfirmInner').removeClass('divPopUpHide').removeClass('divPopUpShow').html('').hide().fadeOut(); $('.wcConfirmOuter').remove(); }, 300);

    },
    
    prepareAndSendSMS: function(){
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        if(UA_TPA_FEATURES.chosenMessgeTemplate && UA_TPA_FEATURES.chosenMessgeTemplate.templateType === "placeholder_template"){
            return UA_TPA_FEATURES.prepareAndSendBulkWhatsApp();
        }
        if(!UA_APP_UTILITY.currentSender){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var senderId = UA_APP_UTILITY.currentSender.value;
        if(!valueExists(senderId)){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var recipNumbers = UA_APP_UTILITY.getCurrentMessageRecipients();

        if(recipNumbers.length === 0){
            showErroWindow('Recipient list is empty!', "Kindly add recipients to send SMS.");
            return false;
        }

        var messageText = $("#inp-ssf-main-message").val().trim();

        if(!valueExists(messageText)){
            showErroWindow('Message is empty!', "Kindly fill message to proceed.");
            return false;
        }

        var messageNumberMapArray = [];
        var messageHistoryMap = {};
        

        $('#sms-prog-window').show();
        $('#msgItemsProgHolder').html('');
        $('#totMsgDisp').text(`Sending messages 0/${recipNumbers.length}...`);

        recipNumbers.forEach(item=>{
            let resolvedMessageText = UA_APP_UTILITY.getTemplateAppliedMessage(messageText, UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id]);
            var historyUID = item.id+''+new Date().getTime()+''+Math.round(Math.random(100000,99999)*1000000);
            messageNumberMapArray.push({
                'number': item.number,
                'text': resolvedMessageText, 
                'clientuid': historyUID
            });
            messageHistoryMap[historyUID] = {
                'contact':{
                    'name': UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].Full_Name : item.number
                },
                'from': senderId,
                'to': item.number,
                'message': resolvedMessageText,
                'status': 'NOT_SENT',
                'module': UA_SAAS_SERVICE_APP.widgetContext.module,
                'moduleId': item.id,
                'channel': "SMS"
            };

            $('#msgItemsProgHolder').append(`
              <div id="msg-resp-item-${parseInt(item.number)}" class="msgRespItem" title="${getSafeString(resolvedMessageText)}">
                  <div class="mri-label"><b>${$('.msgRespItem').length+1}</b>. SMS  ${senderId}  to ${UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].name+'-' : ''} ${item.number}</div>
                  <div class="mri-status">Sending...</div>
              </div>
          `);
        });
        
        UA_TPA_FEATURES.sendBulkSMS(senderId, messageNumberMapArray, (function(successSentMsgRes, successSentMsgCount){
            if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
                   return false;
               }
            if(successSentMsgRes && typeof(successSentMsgRes.data) == 'object' && successSentMsgRes.data && !successSentMsgRes.error) {
                $('#totMsgDisp').text(`All messages have been processed. TOTAL: ${messageNumberMapArray.length}, SENT: ${successSentMsgCount}, FAILED: ${messageNumberMapArray.length-successSentMsgCount}`);
                UA_APP_UTILITY.resetMessageFormFields(); 
            }
            else{
                //showErroWindow('Unable to send message', '');
                return;              
            }
        }), messageHistoryMap);
        
    },

    insertTemplateContentInMessageInput :function(templateId){
        var curTemplateItem = UA_APP_UTILITY.fetchedTemplates[templateId];
        UA_TPA_FEATURES.chosenMessgeTemplate = curTemplateItem;
        $("#messageAttachmentInputHolder").hide();
        UA_APP_UTILITY.currentAttachedFile = null;
        $("#attachedfile").text('Attach file');
        $('#inputFileAttach').val('');
        if(curTemplateItem.templateType === "placeholder_template"){
            $("#primary-send-btn").text('Send WhatsApp').css('background-color', 'green');
            if(curTemplateItem.head_mediatype){
                switch (curTemplateItem.head_mediatype) {
                    case "0":
                        $("#attachedfile").text('Attach document');
                        break;
                    case "1":
                        $("#attachedfile").text('Attach image');
                        break;
                    case "2":
                        $("#attachedfile").text('Attach video');
                        break;
                    default:
                        $("#attachedfile").text('Attach file');
                        break;
                }
            }
            if(curTemplateItem.head_media_url){
                $("#messageAttachmentInputHolder").show();
            }
            $("#ssf-fitem-template-placeholder-holder-listholder").html("");
            curTemplateItem.placeholders.forEach(item=>{
                $("#ssf-fitem-template-placeholder-holder-listholder").append(`
                    <div class="ssf-temp-placehold-item">
                            <span class="ssf-temp-placehold-item-label">{{${item}}}</span> <span class="material-icons ssf-temp-placehold-item-icon">double_arrow</span> <input onblur="UA_APP_UTILITY.lastFocusedInputElemForPlaceholderInsert = 'ssf-templ-place-input-${item}'" id="ssf-templ-place-input-${item}" data-placeholder-id="${item}" class="ssf-temp-placehold-item-input" type="text" placeholder="Type or choose field from above">
                        </div>
                `);
            });
            if(curTemplateItem.placeholders.length > 0){
                $("#ssf-fitem-template-placeholder-holder").show();
            }else{
                $("#ssf-fitem-template-placeholder-holder").hide();
            }
            $('#inp-ssf-main-message').attr('readonly', true);
        }
        else{
            $("#primary-send-btn").text('Send SMS').css('background-color', 'royalblue');
            $("#ssf-fitem-template-placeholder-holder").hide();
            $('#inp-ssf-main-message').removeAttr('readonly');
        }
        $('#inp-ssf-main-message').val(curTemplateItem.message).focus();
    },
    
    _getfetchedPhoneNumbersList : async function(){
        responseData = [];
        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://platform.ringcentral.com/restapi/v1.0/account/~/phone-number", "GET");
        if(response.code === 401 || response.data.code === 401 || response.data.message === "Authentication failed"){
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return responseData;
        }
        responseData = response.data.records;
        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://platform.ringcentral.com/restapi/v1.0/account/~/extension/~/phone-number", "GET");
        responseData.concat(response.data.records);
        return responseData;
    },
    
    showReAuthorizeERROR: function(showCloseOption = false){
       showErroWindow("Authorization needed!", `You need to authorize your ${UA_TPA_FEATURES.tpaSortName} Account to proceed <br><br> <button id="UA_TPA_AUTH_BTN" class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.initiateAuth()">Authorize</button>`, false, false, 1000);
       UA_LIC_UTILITY.showExistingInvitedAdminDDHTML(true);
    },

    initiateAuth: function () {
        if(!extensionName){
            showErroWindow('Application error!', 'Unable to load app type. Kindly <a target="_blank" href="https://apps.ulgebra.com/contact">contact developer</a>');
            return;
        }
        window.open(`https://platform.ringcentral.com/restapi/oauth/authorize?response_type=code&client_id=${UA_TPA_FEATURES.ringcentralClient["clientId"]}&state=${currentUser.uid}:::${extensionName}&redirect_uri=https://us-central1-ulgebra-license.cloudfunctions.net/oauthCallbackRingcentral`, 'Authorize', 'popup');
    },
    
    showReAuthorizeERRORWithClose: function(){
        UA_TPA_FEATURES.showReAuthorizeERROR();
    },
    
    saveTPAAPIKey: async function(callback){
        let authtoken = $("#inp_tpa_api_key").val();
        if(!valueExists(authtoken)){
            showErroMessage("Please fill API Key");
            return;
        }
        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', 'authtoken', authtoken, callback);
    },
    
    saveTPAWABAAPIKey: async function(callback){
        let authtoken = $("#inp_tpa_waba_api_key").val();
        if(!valueExists(authtoken)){
            showErroMessage("Please fill API Key");
            return;
        }
        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', 'waba_authtoken', authtoken, callback);
    },
        
    prepareAndSendBulkWhatsApp : function(){
        
        if(!UA_APP_UTILITY.currentSender){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var senderId = UA_APP_UTILITY.currentSender.value;
        if(!valueExists(senderId)){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var messageText = $("#inp-ssf-main-message").val().trim();
        if(!valueExists(messageText)){
            showErroWindow('Message is empty!', "Kindly fill message to proceed.");
            return false;
        }
        var recipNumbers = UA_APP_UTILITY.getCurrentMessageRecipients();
        if(recipNumbers.length === 0){
            showErroWindow('Recipient list is empty!', "Kindly add recipients to send SMS.");
            return false;
        }
        if(!UA_APP_UTILITY.fileUploadComplete){
            showErroWindow('File upload is not complete!', "Please wait while your file is uploading and try again.");
            return;
        }
        var messageNumberMapArray = [];
        var messageHistoryMap = {};
        $('#sms-prog-window').show();
            $('#msgItemsProgHolder').html('');
          var totalCount = recipNumbers.length;
          $('#totMsgDisp').text(`Sending messages 0/${totalCount}...`);
          var messagesSentCount = 0;
          var messageStatusMap = {
              'success': 0,
              'failed' : 0
          };
        recipNumbers.forEach(item=>{
            var message = {
                "templateid": UA_TPA_FEATURES.chosenMessgeTemplate.tempid
            };
            if(UA_APP_UTILITY.currentAttachedFile && UA_APP_UTILITY.currentAttachedFile.mediaUrl){
                message.url = UA_APP_UTILITY.currentAttachedFile.mediaUrl;
            }
            var contactRecordId = UA_APP_UTILITY.storedRecipientInventory[item.id] ? item.id : null;
            var resolvedMessageText = messageText + (message.url ? ' '+message.url : '');
            if(UA_TPA_FEATURES.chosenMessgeTemplate.placeholders){
                message.placeholders = [];
                UA_TPA_FEATURES.chosenMessgeTemplate.placeholders.forEach(item=>{
                    var placeHolderValue = $('#ssf-templ-place-input-'+item).val();
                    //if(contactRecordId){
                        placeHolderValue = UA_APP_UTILITY.getTemplateAppliedMessage(placeHolderValue, UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[contactRecordId]);
                    //}
                    resolvedMessageText = resolvedMessageText.replace('{{'+item+'}}', placeHolderValue);
                    message.placeholders.push(placeHolderValue);
                });
            }
            var messagePayload = {
                "from": senderId,
                "to": item.number,
                "type": "template",
                "message": message
            };
            messageHistoryMap = {
                'contact':{
                    'name': UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].Full_Name : item.number
                },
                'from': senderId,
                'to': item.number,
                'message': resolvedMessageText,
                'status': 'NOT_SENT',
                'module': UA_SAAS_SERVICE_APP.widgetContext.module,
                'moduleId': item.id,
                'channel': 'WhatsApp',
                'tpa': 'RingCentral'
            };
            $('#msgItemsProgHolder').append(`
              <div id="msg-resp-item-${parseInt(item.number)}" class="msgRespItem" title="${getSafeString(resolvedMessageText)}">
                  <div class="mri-label"><b>${$('.msgRespItem').length+1}</b>. WhatsApp to ${UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].name+'-' : ''} ${item.number}</div>
                  <div class="mri-status">Sending...</div>
              </div>
          `);
            UA_TPA_FEATURES._proceedToSendWAAndExecuteCallback(messagePayload, function(response){
                if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
                   return false;
               }
                console.log(response);
                messagesSentCount++;
                $(`#msg-resp-item-${parseInt(item.number)} .mri-status`).text(response.data.status+' - '+response.data.message).css({'color': response.data.status === "SUCCESS" ? 'green' : 'crimson'});
                response.data.status === "SUCCESS" ? messageStatusMap.success++ : messageStatusMap.failed++;
                if(messagesSentCount === totalCount){
                    $('#totMsgDisp').text(`All messages have been processed. TOTAL: ${totalCount}, SENT: ${messageStatusMap.success}, FAILED: ${messageStatusMap.failed}`);
                }else{
                    $('#totMsgDisp').text(`Sending messages ${messagesSentCount}/${totalCount}...`);
                }
                messageHistoryMap.status = response.data.status === "SUCCESS" ? "SUCCESS" : response.data.status+"-"+response.data.message;
                UA_SAAS_SERVICE_APP.addSentSMSAsRecordInHistory({
                    "message1": messageHistoryMap
                });
            });
        });
        
        
    },
    
    sendSMS : function(sender, number, text, callback){
        var messagePayload = {
            "from": {"phoneNumber": sender},
            "to":[
                {"phoneNumber": number}
            ],
            "text": text
        };
        UA_TPA_FEATURES._proceedToSendSMSAndExecuteCallback(messagePayload, callback);    
    },
    
    sendBulkSMS : async function(sender, numberMessageMapArray, callback, messageHistoryMap) {

        let successSentMsgCount = 0;
        
        for(let MsgMapArr=0; MsgMapArr < numberMessageMapArray.length; MsgMapArr++) {

            var messagePayload = {
                "from": {"phoneNumber": sender},
                "to": [{"phoneNumber": numberMessageMapArray[MsgMapArr].number}],
                "text": numberMessageMapArray[MsgMapArr].text
            };
            
            let successSentMsgRes = await UA_TPA_FEATURES._proceedToSendSMSAndExecuteCallback(messagePayload, '');
            if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
                return false;
            }
            if(successSentMsgRes && typeof(successSentMsgRes.data) == 'object' && successSentMsgRes.data && !successSentMsgRes.error) {
                successSentMsgCount++;
                messageHistoryMap[numberMessageMapArray[MsgMapArr].clientuid].status = "SENT";
                $(`#msg-resp-item-${parseInt(numberMessageMapArray[MsgMapArr].number)} .mri-status`).text(`Sent`).css({'color': 'green'});
            }
            else {
                $(`#msg-resp-item-${parseInt(numberMessageMapArray[MsgMapArr].number)} .mri-status`).text(`Failed ${successSentMsgRes.error? "- "+successSentMsgRes.error.message:""}`).css({'color': 'crimson'});
            }

            $('#totMsgDisp').text(`Sending messages ${MsgMapArr+1}/${numberMessageMapArray.length}...`);
            if(MsgMapArr == numberMessageMapArray.length-1) {
                if(extensionName.endsWith('desk')) {
                    await UA_SAAS_SERVICE_APP.addSentSMSAsRecordInHistory(messageHistoryMap);
                }
                await callback(successSentMsgRes, successSentMsgCount);
            }

        }        
        
    },
    
    _proceedToSendSMSAndExecuteCallback: async function(messagePayload, callback){
        if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        var sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://platform.ringcentral.com/restapi/v1.0/account/~/extension/~/sms", "POST", messagePayload);
        if(sentAPIResponse.code === 401 || ( sentAPIResponse.data && (sentAPIResponse.data.code === 401 || sentAPIResponse.data.message === "Authentication failed"))){
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return;
        }
        return sentAPIResponse;
    },
    
    getAPIResponse: async function (extensionName, url, method, data) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = {"Content-Type": "application/json"};
        await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, TPAService: "ringcentral",url: url, method: method, data: data, headers: headers, _ua_lic_adminUserID: UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID}).then((response) => {
            response = response.data;
            returnResponse = response;
            removeTopProgressBar(credAdProcess3);
            return true;
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
        return returnResponse;
    },
    
    getCurrentAccountOrBalanceInfo: async function(callback){
        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://platform.ringcentral.com/restapi/v1.0/account/~", "GET", {});
        if(!response || (response.code === 401 || response.message === "Authentication failed") || (response.data && (response.data.code === 401 || response.data.message === "Authentication failed"))){
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return;
        }
        callback(response);
    },
    removeIncomingWebHookFunction: async function(tpaWebhookId) {

        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://platform.ringcentral.com/restapi/v1.0/subscription", "GET", {});
        console.log(response);

        if(response && response.data.records && response.data.records.length) {

            let webhookArr = response.data.records;
            let ulgIncomWebHook = webhookArr.find(o => o.deliveryMode.address === `https://api.ulgebra.com/v1/webhooks?a=${extensionName}&o=${appsConfig.UA_DESK_ORG_ID}&t=${tpaWebhookId}`);
            
            let apiURL = "https://platform.ringcentral.com/restapi/v1.0/subscription";

            apiURL = apiURL+'/'+ulgIncomWebHook.id;
            let webhookRes = await UA_TPA_FEATURES.getAPIResponse(extensionName, apiURL, 'DELETE', {});
            if(webhookRes && typeof(webhookRes) == 'object' && webhookRes.status === 204) {
                console.log(webhookRes);
                return true;
            }
            else {
                return false;
            }

        }
        else {
            return false;
        }
        
    },
    showIfErrorInAPIResult: function(response){
        if((response.code === 401 || response.message === "Authentication failed") || (response.error && response.error.code === 401) || (response.data && (response.data.code === 401 || response.data.message === "Authentication failed"))){
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return true;
        }
        if(response.error && response.error.message){
            showErroWindow('Error from RingCentral!', response.error.message+` <br><br> <a target="_blank" href="${response.error.more_info}">Know more</a>`);
            return true;
        }
        return false;
    },
    EXISTING_WEBHOOK_ID: null,
    isWebhookAlreadyExists: async function(){
        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://platform.ringcentral.com/restapi/v1.0/subscription", "GET", {});
        console.log(response);
        if(UA_TPA_FEATURES.showIfErrorInAPIResult(response)){
            return;
        }
        let existingServiceID = null;
        response.data.records.forEach((item)=>{
            try{
                let url = item.deliveryMode.address;
                if(url && url.startsWith(UA_APP_UTILITY.getWebhookBaseURL())){
                    existingServiceID = item.id;
                }
            }
            catch(ex){console.log(ex)}
        });
        UA_TPA_FEATURES.EXISTING_WEBHOOK_ID = existingServiceID;
        return existingServiceID !== null;
    },
    
    addNewWebhook: async function(){

        if(!UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS){
            UA_APP_UTILITY.CALL_AFTER_PERSONAL_COMMON_SETTING_RESOLVED.push(UA_TPA_FEATURES.addNewWebhook);
            return false;
        }
        if(UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.hasOwnProperty('ua_incoming_enable_capture_'+currentUser.uid) && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS['ua_incoming_enable_capture_'+currentUser.uid] === false){
            return;
        }
        
        UA_TPA_FEATURES.EXISTING_WEBHOOK_ID = true;
        UA_TPA_FEATURES.incomingCaptureStatusChanged();

    },

    actualPersonalCommonSettingResolved: function() {
        UA_TPA_FEATURES.addNewWebhook();
        UA_APP_UTILITY.insertIncomingWD_supportedModules();
        UA_TPA_FEATURES.insertIncomingWebhookDialog();
    },
    incomingCaptureStatusChanged: function(){
        if(UA_TPA_FEATURES.EXISTING_WEBHOOK_ID){
            document.getElementById('tpa-switch-incoming-enable-capture').checked = true;
            $('.tpaichan-devider').fadeIn();
        }
        else{
            document.getElementById('tpa-switch-incoming-enable-capture').checked = false;
            $('.tpaichan-devider:not(#master-tpai-incom-chan-item)').fadeOut();
        }
    },
    deleteExistingWebhook: async function(){        
        UA_TPA_FEATURES.EXISTING_WEBHOOK_ID = null;
        UA_TPA_FEATURES.incomingCaptureStatusChanged();
        return true;
    },
    showIncomingWebhookDialog: function(){
        $("#error-window-tpa_service_senders").show();
        $("#incoming-channel-config-item").parent().remove();
    },
    insertIncomingWebhookDialog: async function() {
        
        if(!UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS){
            UA_APP_UTILITY.CALL_AFTER_PERSONAL_COMMON_SETTING_RESOLVED.push(UA_TPA_FEATURES.addNewWebhook);
            return false;
        }
        if(UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.hasOwnProperty('ua_incoming_enable_capture_'+currentUser.uid) && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS['ua_incoming_enable_capture_'+currentUser.uid] === false){
            return;
        }
        var webhookExists = await UA_TPA_FEATURES.isWebhookAlreadyExists();
        if(!webhookExists){

            let webhookAddress = UA_APP_UTILITY.getAuthorizedWebhookURL();
            let webhookData = {"eventFilters":["/restapi/v1.0/account/~/extension/~/presence?detailedTelephonyState=true&sipData=true","/restapi/v1.0/account/~/extension/~/message-store","/restapi/v1.0/account/~/extension/~/presence/line","/restapi/v1.0/account/~/extension"],"deliveryMode":{"transportType":"WebHook","address":webhookAddress.toString()}};
            var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://platform.ringcentral.com/restapi/v1.0/subscription", "POST", webhookData);
            if(UA_TPA_FEATURES.showIfErrorInAPIResult(response)){
                return false;
            }
            return response.data && (response.data.sid !== null);
        }
        return true;

    }
    
};