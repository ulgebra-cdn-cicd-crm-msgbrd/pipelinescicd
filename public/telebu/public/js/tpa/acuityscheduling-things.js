var UA_TPA_FEATURES = {
    'clientIds': {
        'acuityschedulingforpipedrive': '00c72ed9d967386d',
        'acuityschedulingforbitrix24': 'app.618df22406aa95.14782903',
        'acuityschedulingforhubspotcrm': '106961a0-ec87-4d08-8188-f08f154adbb0'
    },
    renderInitialElements: async () => {
        $(".pageContentHolder").html("")
        $("#suo-item-workflow-nav").hide()
        $("#ac_name_label_tpa .anl_servicename").text('Acuity Scheduling');

        $(".incoming-config-tpa-entity").text('events');
        $('#incoming-channel-display-text').text('Choose which event type appointments should be captured in this integration.');
        const me = (await UA_TPA_FEATURES._getAboutMe()).data
        if (!me?.email) {
            UA_TPA_FEATURES.showReAuthorizeERROR()
            return;
        }
        UA_TPA_FEATURES.me = me
        $('#ac_name_label_tpa .ac_name_id').text(me.email);
        $("#inp-penea-cur-invitee-email").after(`<select id="inp-penea-cur-invitee-contact-number" style="border-radius: 40px;border: none;box-shadow: 0px 0px 3px inset rgb(0 0 0 / 30%);padding: 7px 15px;font-size: 12px;vertical-align: middle;"><option value="">All Contacts</option></select>
            <input onkeyup="UA_APP_UTILITY.eventHistorySearchContactInput(event)" id="inp-penea-cur-invitee-calendarID" type="text" placeholder="Calendar id (optional)" style="border-radius: 40px;border: none;box-shadow: 0px 0px 3px inset rgb(0 0 0 / 30%);padding: 7px 15px;font-size: 12px;vertical-align: middle;">
        `)
        $("#peana-appointment-type").css("display", "flex")
        UA_APP_UTILITY.getPersonalWebhookAuthtoken();
        UA_TPA_FEATURES.getMyOrgAcuityAppointmentTypes();
    },
    showReAuthorizeERROR: function (showCloseOption = false) {
        if ($('#inp_tpa_api_key').length > 0) {
            console.log('already showing reautherr');
            return;
        }
        showErroWindow("Authorization needed!", `You need to authorize your Acuity Scheduling Account to proceed. <br><br> 
    <div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
        <div style="
            font-size: 24px;
            font-weight: bold;
            margin-top: 20px;
            margin-bottom: 15px;
        ">Acuity Scheduling Configuration</div>
        <div style="margin-left:20px">
            <div style="font-size: 16px;margin-bottom: 5px;margin-top: 10px;color: rgb(80,80,80);">Acuity Scheduling User ID</div>
            <div class="help_apikey_tip"><a target="_blank" href="https://secure.acuityscheduling.com/app.php?action=settings&key=api">Get Here</a></div>
            <input id="inp_tpa_user_id" type="text" placeholder="Enter your User ID..." style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
        </div>
        <div style="margin-left:20px">
            <div style="font-size: 16px;margin-bottom: 5px;margin-top: 10px;color: rgb(80,80,80);">Acuity Scheduling API Key</div>
            <div class="help_apikey_tip"><a target="_blank" href="https://secure.acuityscheduling.com/app.php?action=settings&key=api">Get Here</a></div>
            <input id="inp_tpa_api_key" type="text" placeholder="Enter your API Key..." style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
        </div>
        <button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.saveTPAAPIKey(UA_APP_UTILITY.reloadWindow)">Authorize</button>
    </div>`, showCloseOption, false, 500);
        $('#inp_tpa_api_key').focus();
        UA_LIC_UTILITY.showExistingInvitedAdminDDHTML(true);
    },
    showReAuthorizeERRORWithClose: function () {
        UA_TPA_FEATURES.showReAuthorizeERROR();
    },
    saveTPAAPIKey: async function (callback) {
        let userid = $("#inp_tpa_user_id").val();
        let apikey = $("#inp_tpa_api_key").val();
        if (!valueExists(userid)) {
            showErroMessage("Please fill User ID");
            return;
        }
        if (!valueExists(apikey)) {
            showErroMessage("Please fill API Key");
            return;
        }
        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', '__multiple_keys__', { userid, apikey }, callback);
    },
    _getAboutMe: async function () {
        sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://acuityscheduling.com/api/v1/me", "GET");
        return sentAPIResponse
    },
    _getAppointmentTypes: async function () {
        sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://acuityscheduling.com/api/v1/appointment-types", "GET");
        return sentAPIResponse
    },
    APPOINTMENT_TYPES: [],
    CURRENT_SELECTED_APPOINTMENT_TYPE: null,
    ALL_APPOINTMENT_TYPES_SELECTED: true,
    renderAppointmentTypes: async function () {
        const sentAPIResponse = (await UA_TPA_FEATURES._getAppointmentTypes()).data
        sentAPIResponse.forEach((event, i) => {
            UA_TPA_FEATURES.APPOINTMENT_TYPES[event.id] = event
        })
        let optionsArray = sentAPIResponse.map(event => ({ label: event.name, value: event.id.toString() }))
        optionsArray = [{ label: "ALL APPOINTMENT TYPES", value: "allAgents", selected: true }, ...optionsArray]
        UA_APP_UTILITY.renderSelectableDropdown("#peana-appointment-type", "Appointment Types", optionsArray, "UA_TPA_FEATURES.renderNewMeetingIframeForEventTypes")

        return true
    },
    CURRENT_EVENT_SEARCH_HISTORY_CONTACT_EMAIL: null,
    CURRENT_EVENT_SEARCH_HISTORY_CONTACT_PHONE: null,
    CURRENT_EVENT_SEARCH_HISTORY_CONTACT_CALENDER_ID: null,
    getAppointments: async function () {
        let url = "https://acuityscheduling.com/api/v1/appointments?max=50&direction=DESC";
        if (UA_TPA_FEATURES.CURRENT_SELECTED_APPOINTMENT_TYPE) {
            url += "&appointmentTypeID=" + UA_TPA_FEATURES.CURRENT_SELECTED_APPOINTMENT_TYPE.id
        }
        let searchInviteeEmail = $('#inp-penea-cur-invitee-email').val().trim();
        let searchInviteePhone = $("#inp-penea-cur-invitee-contact-number").val().trim();
        let searchInviteeCalendarID = $("#inp-penea-cur-invitee-calendarID").val().trim();

        if (searchInviteeEmail) {
            UA_TPA_FEATURES.CURRENT_EVENT_SEARCH_HISTORY_CONTACT_EMAIL = searchInviteeEmail;
            url += "&email=" + searchInviteeEmail
        }
        if (searchInviteePhone) {
            UA_TPA_FEATURES.CURRENT_EVENT_SEARCH_HISTORY_CONTACT_PHONE = searchInviteePhone;
            url += "&phone=" + searchInviteePhone
        }
        if (searchInviteeCalendarID) {
            UA_TPA_FEATURES.CURRENT_EVENT_SEARCH_HISTORY_CONTACT_CALENDER_ID = searchInviteeCalendarID;
            url += "&calendarID=" + searchInviteeCalendarID
        }
        sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, url, "GET", null);
        return (sentAPIResponse)
    },
    updateQueryStringParameter: function (uri, key, value) {
        var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";
        if (uri.match(re)) {
            return uri.replace(re, '$1' + key + "=" + value + '$2');
        }
        else {
            return uri + separator + key + "=" + value;
        }
    },
    getAPIResponse: async function (extensionName, url, method, data) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = { "Content-Type": "application/json" };
        await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({ appCode: extensionName, TPAService: "acuityscheduling", url: url, method: method, data: data, headers: headers, _ua_lic_adminUserID: UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID }).then((response) => {
            response = response.data;
            returnResponse = response;
            removeTopProgressBar(credAdProcess3);
            return true;
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
        return returnResponse;
    },
    callOnEntityDetailFetched: async function (data) {
        let firstEndityId = Object.keys(UA_APP_UTILITY.storedRecipientInventory)[0]
        let contact = data || UA_APP_UTILITY.storedRecipientInventory[firstEndityId] || { email: null }
        if (contact) {
            Object.keys(contact).forEach(key => {
                contact[key.toLowerCase()] = contact[key];
            })
        }
        try{
            if(contact.email && (typeof contact.email === "array" || typeof contact.email === "object")){
                contact.emails = contact.email;
                let primaryEmail = null;
                let anyValidEmail = null;
                contact.emails.forEach(item=>{
                    if(item.primary){
                        primaryEmail = item.value;
                    }
                    if(valueExists(item.value)){
                        anyValidEmail = item.value;
                    }
                })
                contact.email = primaryEmail ? primaryEmail : anyValidEmail;
            }
            if(contact.phone && (typeof contact.phone === "array" || typeof contact.phone === "object")){
                contact.phones = contact.phone;
                let primaryPhone = null;
                let anyValidPhone = null;
                contact.phones.forEach(item=>{
                    if(item.primary){
                        primaryPhone = item.value;
                    }
                    if(valueExists(item.value)){
                        anyValidPhone = item.value;
                    }
                })
                contact.phone = primaryPhone ? primaryPhone : anyValidPhone;
            }
        }
        catch(ex){console.log(ex)};
        contact.email = contact.Email || contact.email
        UA_TPA_FEATURES.contact = contact
        $('#inp-penea-cur-invitee-email').val(contact.email);
        UA_TPA_FEATURES.CURRENT_EVENT_SEARCH_HISTORY_CONTACT_EMAIL = contact.email;

        if (contact.mobile || contact.phone) {
            let options = ""
            let selected = false
            if (contact.mobile) {
                options += ` <option value="${contact.mobile}" selected>${contact.mobile}</option>`
                selected = true
            }
            if (contact.phone) {
                options += ` <option value="${contact.phone}" ${!selected ? "selected" : ""}>${contact.phone}</option>`
            }
            $("#inp-penea-cur-invitee-contact-number").append(options)

        }
        await UA_TPA_FEATURES.renderAppointmentTypes()
        UA_TPA_FEATURES.renderNewMeetingIframeForEventTypes("allAgents");
    },
    MY_FETCHED_CALENDLY_EVENT_TYPES: {},
    MY_ORG_FETCHED_CALENDLY_EVENT_TYPES: {},

    IS_NON_ADMIN_ACCOUNT: false,
    getMyOrgAcuityAppointmentTypes: async function () {
        let sentAPIResponse = (await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://acuityscheduling.com/api/v1/appointment-types`, "GET")).data;
        if (sentAPIResponse.error) {
            UA_TPA_FEATURES.IS_NON_ADMIN_ACCOUNT = true;
            return false;
        }
        sentAPIResponse.forEach(item => {
            let chanId = item.id
            UA_TPA_FEATURES.MY_ORG_FETCHED_CALENDLY_EVENT_TYPES[chanId] = item;
        });
        UA_TPA_FEATURES.insertIncomingWD_channels();
    },
    actualOrgCommonSettingResolved: function () {
        UA_TPA_FEATURES.insertIncomingWD_supportedModules();
        UA_TPA_FEATURES.addNewWebhook();
    },
    prepareNewEvent: async function () {
        let appointmentTypes = []
        if (UA_TPA_FEATURES.ALL_APPOINTMENT_TYPES_SELECTED) {
            appointmentTypes = UA_TPA_FEATURES.APPOINTMENT_TYPES
        } else {
            appointmentTypes = UA_TPA_FEATURES.APPOINTMENT_TYPES.filter(item => item.id === UA_TPA_FEATURES.CURRENT_SELECTED_APPOINTMENT_TYPE.id)
        }
        $('#peana-event-types-links').html("")
        appointmentTypes.forEach(elm => {
            $('#peana-event-types-links').append(
                `<div class="events-links eventTypes ${!elm.active ? 'disabled' : ''}" 
                style="border: 1px solid ${elm.color};" 
                data-url="${elm.scheduling_url}" data-active="${elm.active}">
                ${elm.active ? '<div class="eventStatus" style="color: #248750">active</div>' : ''}
                <div class="field-title" style="font-size:13px">
                    ${elm.name}&nbsp; ${elm.secret ? secret : ''}<br>
                    ( ${elm.duration} minutes )
                </div>
                <a target="_blank" href="${elm.schedulingUrl}" class="event-field content_copy copy-link"  data-url="${elm.schedulingUrl}" style="cursor:pointer;float:left;">
                    <span class="material-icons" >open_in_new</span>
                    &nbsp;Open
                </a>
                <div onclick="prepareToCopyToClipBoard(this)"  class="event-field content_copy copy-link" data-url="${elm.schedulingUrl}" style="cursor:pointer;float:left;">
                    <span class="material-icons">content_copy</span>
                    &nbsp;Copy
                </div>

                <div class='createdAt' >Price: ${elm.price}</div>
            </div>`
            )
        })
    },
    loadSeduledEvents: async function () {
        UA_TPA_FEATURES.events = (await UA_TPA_FEATURES.getAppointments()).data
        const now = new Date()
        const past = []
        const upcoming = []
        UA_TPA_FEATURES.events.forEach(function (event) {
            if (new Date(event.datetime) < now) {
                past.push(event)
                return
            }
            upcoming.push(event)
        })
        $('#upcommingCount').text(upcoming.length)
        $('#pastCount').text(past.length)
        $('#upcomming').html("")
        $('#past').html("")
        past.forEach(event => $('#past').append(UA_TPA_FEATURES.eventElement(event)))
        upcoming.reverse().forEach(event => $('#upcomming').append(UA_TPA_FEATURES.eventElement(event)))
        let searchInviteeEmail = $('#inp-penea-cur-invitee-email').val().trim();
        let searchInviteePhone = $("#inp-penea-cur-invitee-contact-number").val().trim();//invitee-calendarID
        let searchInviteeCalendarID = $("#inp-penea-cur-invitee-calendarID").val().trim();
        let msg = past.length || upcoming.length ? "Showing events " : "No Appointments "
        msg += "for "
        msg += UA_TPA_FEATURES.ALL_APPOINTMENT_TYPES_SELECTED ? "Acuityscheduling Appointment Types " : ""
        msg += UA_TPA_FEATURES.CURRENT_SELECTED_APPOINTMENT_TYPE ? UA_TPA_FEATURES.CURRENT_SELECTED_APPOINTMENT_TYPE.name + " " : ""
        msg += searchInviteeEmail ? " & " + searchInviteeEmail : ""
        msg += searchInviteePhone ? " & " + searchInviteePhone : ""
        msg += searchInviteeCalendarID ? " & Calendar ID " + searchInviteeCalendarID : ""
        $('#showing').html(msg)
    },
    eventElement: function (event) {
        const { canceled, confirmationPage, firstName, lastName, date, time, endTime, duration, dateCreated, id, datetime, timezone } = event
        return (`
            <div class="events" data-event-id="${id}" data-uri="${confirmationPage}" onclick="UA_TPA_FEATURES.expandEvent(this)">
                <div class='eventStatus' style='color: ${!canceled ? '#248750' : 'red'}' >${!canceled ? "Active" : "Canceled"}</div>
                <div class="field-title"> ${getSafeString(firstName)} ${getSafeString(lastName)} </div>
                <div class="event-field">
                    ${shortDate(date)}
                    ${time.split(time.slice(-2))[0] + " " + time.slice(-2)} -  
                    ${endTime.split(endTime.slice(-2))[0] + " " + endTime.slice(-2)} <br>
                    (${duration} Minutes) <br>
                    <span style="display:none;">${new Date(datetime).toLocaleString()}</span>
                </div>
                <div class='createdAt' title="Created at ${getTimeString(dateCreated)}">${getDateString(dateCreated)}</div>
            </div>
            `)
    },

    expandEvent: async function (e) {
        let confirmationPage = $(e).attr("data-uri")
        myModel(`<iframe src="${confirmationPage}" style="border:none;height:100vh">Loading</iframe>`)
    },

    FETCHED_EVENT_AGENTS: {},

    cacheUserDetailsIfNotPresentInOrgSetting: function (userItem) {
        if (!UA_APP_UTILITY.ACTUAL_ORG_COMMON_SETTINGS) {
            UA_APP_UTILITY.CALL_AFTER_ORG_COMMON_SETTING_RESOLVED.push(() => { UA_TPA_FEATURES.cacheUserDetailsIfNotPresentInOrgSetting(userItem); });
            return;
        }
        let userId = userItem.uri.split('/')[userItem.uri.split('/').length - 1];
        UA_APP_UTILITY.addToOrgCacheIfNotExists('calendly_my_org_members_' + userId, userItem);
    },
    CURRENT_SELECTED_AGENT: null,
    ALL_AGENT_VIEW_SELECTED: false,
    renderNewMeetingIframeForEventTypes: function (appointmentTypeId) {
        if (appointmentTypeId) {
            if (appointmentTypeId === "allAgents") {
                UA_TPA_FEATURES.ALL_APPOINTMENT_TYPES_SELECTED = true
                UA_TPA_FEATURES.CURRENT_SELECTED_APPOINTMENT_TYPE = null
                $('#peaneai-allAgents').toggleClass('selected', UA_TPA_FEATURES.ALL_APPOINTMENT_TYPES_SELECTED);
                $('.peana-userlist').toggleClass('allAgents', UA_TPA_FEATURES.ALL_APPOINTMENT_TYPES_SELECTED);
            } else {
                appointmentTypeId = parseInt(appointmentTypeId)
                UA_TPA_FEATURES.ALL_APPOINTMENT_TYPES_SELECTED = false;
                $('.peana-u-item').removeClass('selected');
                $('.peana-userlist').removeClass('allAgents');
                $('#peaneai-' + appointmentTypeId).addClass('selected');
                //            $('.peana-meetin-iframe-loader').show();
                $("#peanea-main-new-meeting-iframe").css('opacity', '0.2');
                UA_TPA_FEATURES.CURRENT_SELECTED_APPOINTMENT_TYPE = UA_TPA_FEATURES.APPOINTMENT_TYPES[appointmentTypeId];

            }

        }
        // else {
        //     debugger
        //     let default_SelectedTypeID = Object.keys( UA_TPA_FEATURES.APPOINTMENT_TYPES)[0]
        //     $(`#peaneai-${default_SelectedTypeID}`).trigger("click")
        // }
        let contact = UA_TPA_FEATURES.contact
        let firstName = ""
        let lastName = ""
        let fullName = valueExists((contact.name)) ? encodeURIComponent(contact.name) : "";
        let contact_email = valueExists((contact.email)) ? encodeURIComponent(contact.email) : "";
        let contact_phone = "";
        if (contact.phone) {
            contact_phone = encodeURIComponent(contact.phone);
        }
        if (contact.mobile) {
            contact_phone = encodeURIComponent(contact.mobile);
        }
        try { //bitrix24 phone,email if an object it will take that first object's value
            if (!contact_phone && contact?.PHONE?.length) {
                contact_phone = encodeURIComponent(contact.PHONE[0]["VALUE"])
            }
            if (!contact_email && contact?.EMAIL?.length) {
                contact_email = encodeURIComponent(contact.EMAIL[0]["VALUE"])
            }
        }
        catch (err) {
            console.log(err)
        }
        
        try{
            //zohocrm || bitrix || freshworkscrm
            firstName = contact?.First_Name || contact?.NAME || contact?.first_name || ""
        }catch(err){console.log(err) }
        try{
            lastName = contact?.Last_Name || contact?.LAST_NAME || contact?.last_name || ""
        }catch(err){console.log(err) }

        if (UA_APP_UTILITY.CURRENT_EVENT_PAGE_VIEW === "new") {
            let iframeurl = null
            if (UA_TPA_FEATURES.ALL_APPOINTMENT_TYPES_SELECTED) {
                iframeurl = UA_TPA_FEATURES.me.acuityScheduler
            } else {
                iframeurl = UA_TPA_FEATURES.CURRENT_SELECTED_APPOINTMENT_TYPE.schedulingUrl;
            }
            $("#peanea-main-new-meeting-iframe").attr('src',`${iframeurl}&firstName=${firstName || fullName}&lastName=${lastName || fullName}&phone=${contact_phone}&email=${contact_email}`);
        }
        UA_TPA_FEATURES.reAdjustNewBookingIframeView();
        if (UA_APP_UTILITY.CURRENT_EVENT_PAGE_VIEW === "links") {
            UA_TPA_FEATURES.prepareNewEvent();
        }
        if (UA_APP_UTILITY.CURRENT_EVENT_PAGE_VIEW === "history") {
            UA_TPA_FEATURES.loadSeduledEvents();
        }
    },
    reAdjustNewBookingIframeView: function () {
        let iframeLength = $('#peanea-main-new-meeting-iframe').outerWidth();
        if (iframeLength < 650) {
            $('#peanea-main-new-meeting-iframe').removeClass('reduceTopCalendlyPadding');
        }
        else {
            $('#peanea-main-new-meeting-iframe').addClass('reduceTopCalendlyPadding');
        }
    },
    EXISTING_WEBHOOK_ID: null,
    WEBHOOK_PERMISSION_DENIED: false,
    isWebhookAlreadyExists: async function () {
        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://acuityscheduling.com/api/v1/webhooks`, "GET", {});

        if ((response.error && response.error.code === 403) || UA_TPA_FEATURES.showIfErrorInAPIResult(response)) {
            UA_TPA_FEATURES.WEBHOOK_PERMISSION_DENIED = true;
            UA_TPA_FEATURES.IS_NON_ADMIN_ACCOUNT = true;
            return false;
        }
        let existingServiceID = null;

        response.data.forEach((item) => {
            let url = item.target;
            if (url && url.startsWith(UA_APP_UTILITY.getWebhookBaseURL())) {
                existingServiceID = existingServiceID? existingServiceID+","+ item.id: item.id;
            }
        });

        UA_TPA_FEATURES.EXISTING_WEBHOOK_ID = existingServiceID;
        UA_TPA_FEATURES.serviceSID = existingServiceID;
        return existingServiceID !== null;
    },

    showIfErrorInAPIResult: function (response) {
        if ((response.code === 401 || response.message === "Authentication failed") || (response.error && response.error.code === 401) || (response.data && (response.data.code === 401 || response.data.message === "Authentication failed"))) {
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return true;
        }
        if (response.error && response.error.message) {
            showErroWindow('Error from Acuity Scheduling!', response.error.message);
            return true;
        }
        return false;
    },

    addNewWebhook: async function () {
        if(!UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS){
            UA_APP_UTILITY.CALL_AFTER_PERSONAL_COMMON_SETTING_RESOLVED.push(UA_TPA_FEATURES.addNewWebhook);
            return false;
        }
        // doc: https://developers.acuityscheduling.com/page/webhooks-webhooks-webhooks#webhooks
        if (UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.hasOwnProperty('ua_incoming_enable_capture_' + currentUser.uid) && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS['ua_incoming_enable_capture_' + currentUser.uid] === false) {
            UA_TPA_FEATURES.incomingCaptureStatusChanged();
            return;
        }
        if (UA_TPA_FEATURES.IS_NON_ADMIN_ACCOUNT) {
            UA_TPA_FEATURES.incomingCaptureStatusChanged();
            return false;
        }
        if (UA_APP_UTILITY.PERSONAL_WEBHOOK_TOKEN && appsConfig.UA_DESK_ORG_ID) {
            var webhookExists = await UA_TPA_FEATURES.isWebhookAlreadyExists();
            if (!webhookExists && !UA_TPA_FEATURES.WEBHOOK_PERMISSION_DENIED) {
                var responseIds = "";
                try{
                    ["appointment.scheduled","appointment.canceled", "appointment.rescheduled"].forEach(async (event)=>{
                        var webhookData = {
                            "event": event,
                            "target": UA_APP_UTILITY.getAuthorizedWebhookURL()
                        }
                        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://acuityscheduling.com/api/v1/webhooks`, "POST", webhookData);
                        if (UA_TPA_FEATURES.showIfErrorInAPIResult(response)) {
                            return false;
                        }
                        if(response && response.data && response.data.id){
                            responseIds = responseIds + (responseIds?",":"") + response.data.id;
                        }
                    });
                }
                catch(err){
                    console.log(err);
                }
                UA_TPA_FEATURES.EXISTING_WEBHOOK_ID = responseIds;
                UA_TPA_FEATURES.incomingCaptureStatusChanged();
                UA_TPA_FEATURES.showIncomingWebhookDialog();
                return responseIds? true:false;
            }
            UA_TPA_FEATURES.incomingCaptureStatusChanged();
            return true;
        }
        return false;
    },
    incomingCaptureStatusChanged: function () {
        if (UA_TPA_FEATURES.EXISTING_WEBHOOK_ID) {
            document.getElementById('tpa-switch-incoming-enable-capture').checked = true;
        }
        else {
            document.getElementById('tpa-switch-incoming-enable-capture').checked = false;
        }
    },
    deleteExistingWebhook: async function () {
        try{
            if (UA_TPA_FEATURES.IS_NON_ADMIN_ACCOUNT || !UA_TPA_FEATURES.EXISTING_WEBHOOK_ID) {
                return false;
            }
            var ids = [UA_TPA_FEATURES.EXISTING_WEBHOOK_ID];
            if(UA_TPA_FEATURES.EXISTING_WEBHOOK_ID && typeof UA_TPA_FEATURES.EXISTING_WEBHOOK_ID == "string" && UA_TPA_FEATURES.EXISTING_WEBHOOK_ID.indexOf(',') > -1){
                ids = UA_TPA_FEATURES.EXISTING_WEBHOOK_ID.split(",");
            }
            ids.forEach(async (id)=>{
                var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://acuityscheduling.com/api/v1/webhooks/"+id, "DELETE", {});   
                if (UA_TPA_FEATURES.showIfErrorInAPIResult(response)) {
                    return false;
                }
            });
            UA_TPA_FEATURES.EXISTING_WEBHOOK_ID = null;
            UA_TPA_FEATURES.incomingCaptureStatusChanged();
            return true;
        }
        catch(err){
            console.log(err);
        }
    },
    pageEventNavItemSelected: function (viewType) {
        UA_TPA_FEATURES.renderNewMeetingIframeForEventTypes()
    },
    insertAllAgentViewIfNotPresent: function(){
        if($('#peaneai-allAgents').length > 0){
            return false;
        }
        $('.peana-userlist').prepend(`<div data-agentemail="allAgents" id="peaneai-allAgents" onclick="UA_TPA_FEATURES.renderNewMeetingIframeForEventTypes('allAgents')" class="peana-u-item selected" style="padding-left: 20px;padding-bottom: 8px;">
                            <div class="peanai-det">
                                <div class="peanai-top">
                                    All agents
                                </div>
                            </div>
                        </div>`);
    },
    renderWorkflowBodyCode: function(){
        UA_TPA_FEATURES.addNewWebhook();
    },
    showIncomingWebhookDialog: function(){
        $("#error-window-tpa_service_senders").show();
        // UA_TPA_FEATURES.refreshCalendlyEventPhoneFieldsDataInCache();
    },
    insertIncomingWD_supportedModules: function(){
        UA_APP_UTILITY.insertIncomingWD_supportedModules();
    },
    insertIncomingWD_channels: function(){
        if (!UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS) {
            UA_APP_UTILITY.CALL_AFTER_PERSONAL_COMMON_SETTING_RESOLVED.push(() => {
                UA_TPA_FEATURES.insertIncomingWD_channels();
            });
            return;
        }
        let supportedChannelDD = ``;
        for(var i in UA_TPA_FEATURES.MY_ORG_FETCHED_CALENDLY_EVENT_TYPES){
            let ignored = false;
            let chanItem = UA_TPA_FEATURES.MY_ORG_FETCHED_CALENDLY_EVENT_TYPES[i];
            let chanId = chanItem.id;
            if (UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.hasOwnProperty('ua_incoming_tpa_ignored_channels')) {
                ignored = UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.ua_incoming_tpa_ignored_channels.includes(chanId);
            }
            supportedChannelDD += `<div class="tpaichan-item"> <label class="switch"><input ${!ignored ? 'checked' : ''} onchange="UA_TPA_FEATURES.toggleIncomingChannelConfig('${chanId}')" type="checkbox" id="tpa-switch-incoming-channel-${chanId}"><div class="slider round"></div></label> <span class="tpaichani-name">${'<b>' + chanItem.name + '</b>' + ' - ' + chanItem.category + ' - ' + chanItem.duration + ' mins'}</span> </div>`;
            UA_FIELD_MAPPPING_UTILITY.renderIncomingFieldMappingChannelOptions(`Event: ${'<b>'+chanItem.name+'</b>'+ ' - '+ chanItem.type+ ' - '+chanItem.duration+' mins'}`, 'eventtype='+chanId);
        }
        $('#error-window-tpa_service_senders #incoming-channel-config-item').html(supportedChannelDD);
    },
    toggleIncomingChannelConfig: function(chanId){
        if(!UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.ua_incoming_tpa_ignored_channels){
            UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.ua_incoming_tpa_ignored_channels = [];
        }
        if(UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.ua_incoming_tpa_ignored_channels.includes(chanId)){
            UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.ua_incoming_tpa_ignored_channels.remove_by_value(chanId);
        }
        else{
            UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.ua_incoming_tpa_ignored_channels.push(chanId);
        }
        UA_APP_UTILITY.saveSettingInCredDoc('ua_incoming_tpa_ignored_channels', UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.ua_incoming_tpa_ignored_channels);
    },
    insertIncomingConfigItems: function(){
        $('#incoming-module-config-item').append(`
            <div class="tpaichan-item" style="display:none">
                <label class="switch">
                    <input disabled readonly checked onchange="UA_TPA_FEATURES.updateIncomingActivitiesToggle()" type="checkbox" id="tpa-switch-incoming-enable-capture">
                    <div class="slider round"></div>
                </label> Create activities in ${saasServiceName}
            </div>`);
    }
};




function fullScreenIframe(url) {
    myModel(`<iframe style="height:100vh;width:75vh;border:none;"  src="${url}"></iframe>`)
}



function safeLink(text, url, clr) {
    return (`<a nofollow noopener noreferrer href="${url}" class="btn btn-${clr} btn-sm active" target="_blank" role="button" aria-pressed="true">${text}</a>    `)
}
function shortDate(str) {
    const date = new Date(str)
    const dateArr = date.toDateString().split(' ')
    const year = parseInt(dateArr.pop())
    const textDate = (dateArr + ',' + year).replaceAll(',', ', ')
    const textDateArr = textDate.split(' ')
    textDateArr[1] = textDateArr[1].replace(',', '')
    return textDateArr.join(' ')
}
function formatAMPM(str) {
    const date = new Date(str)
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}
function getTimeString(previous) {
    previous = new Date(previous);
    var msPerMinute = 60 * 1000;
    var msPerHour = msPerMinute * 60;
    var msPerDay = msPerHour * 24;
    var msPerMonth = msPerDay * 30;
    var msPerYear = msPerDay * 365;
    var elapsed = new Date() - previous;
    if (elapsed < msPerMinute) {
        return Math.round(elapsed / 1000) + ' seconds ago';
    }
    else if (elapsed < msPerHour) {
        return Math.round(elapsed / msPerMinute) + ' minutes ago';
    }
    else if (elapsed < msPerDay) {
        return Math.round(elapsed / msPerHour) + ' hours ago';
    }
    else if (elapsed < msPerMonth) {
        return ' ' + Math.round(elapsed / msPerDay) + ' days ago';
    }
    else if (elapsed < msPerYear) {
        return ' ' + Math.round(elapsed / msPerMonth) + ' months ago';
    }
    else {
        return ' ' + Math.round(elapsed / msPerYear) + ' years ago';
    }
}
function getSafeString(rawStr) {
    if (!rawStr || rawStr.trim() === "") {
        return "";
    }
    return $('<textarea/>').text(rawStr).html();
}
function prepareToCopyToClipBoard(e) {
    const TextToCopy = $(e).attr('data-url')
    if (copyToClipBoard(TextToCopy)) {
        const x = $(e).html()
        $(e).html(`<span class="material-icons" >done</span>&nbsp;Copied`)
        setTimeout(function () {
            $(e).html(x)
        }, 2000)
    }
}

function copyToClipBoard(TextToCopy) {
    const TempText = document.createElement("input");
    TempText.value = TextToCopy;
    document.body.appendChild(TempText);
    TempText.select();
    isCopied = document.execCommand("copy");
    document.body.removeChild(TempText);
    if (isCopied) {
        return true;
    } else {
        document.body.removeChild(TempText);
        console.log('err to copy clipboard')
    }
}




function myModel(data) {
    $('body').append(
        `<div class="newCalendlyEventIframe">
        <button onclick="$('.newCalendlyEventIframe').remove()" style="
            position: absolute;
            right: 15px;
            top: 10px;
            background-color: crimson;
            color: white;
            font-size: 19px !important;
            border-radius: 50px;
            border: none;
            padding: 4px 11px 6px;
            cursor: pointer;
            text-align: center;
            z-index:2;
            ">x
        </button>
        <div style="display: inline-block;margin-top: 45px;" class="myModel">
            <div style="
                padding:10px;
                border-radius:2px;
                min-width:70vw;
                ">
                <div>${data}</div>
            </div>
        </div>
    </div>`
    );
}
function getDateString(date) {
    const givenDate = new Date(date).toDateString()
    const today = new Date().toDateString()
    const tomorrow = new Date(new Date().setDate(new Date().getDate() + 1)).toDateString()
    const yestreday = new Date(new Date().setDate(new Date().getDate() - 1)).toDateString()
    const currentYear = new Date().getFullYear()
    const givenYear = new Date(date).getFullYear()
    switch (givenDate) {
        case today:
            return 'today'
        case tomorrow:
            return 'tomorrow'
        case yestreday:
            return 'yestreday'
        default:
            return givenDate
    }
}
