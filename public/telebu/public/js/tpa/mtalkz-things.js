var UA_TPA_FEATURES = {
    chosenMessgeTemplate : null,
    'clientIds': {
        'pinnacleforhubspotcrm' : "f30b42b5-9aca-43df-8da5-fc91ac136ed0"
    },
    getSupportedChannels: function(){
        return [ UA_APP_UTILITY.MESSAGING_CHANNELS.SMS ];
    },
    workflowCode : {
        "SMS": {
            "senderId": "FILL_HERE",
            "to": "FILL_HERE",
            "text": "FILL_HERE",
            "module": "FILL_HERE",
            "recordId": "FILL_HERE",
            "channel": "SMS",
            "ulgebra_webhook_authtoken": null
        }

    },
    renderInitialElements: async function(){
        
        $("#ac_name_label_tpa .anl_servicename").text('Mtalkz');
        UA_TPA_FEATURES.getCurrentAccountOrBalanceInfo(async function(resp) {

            setTimeout(function() { $('.isActionBtn').parent().parent().remove(); }, 2000);
            $('#inp-ssf-main-message').attr('readonly', 'readonly');

            $('#ac_name_label_tpa .ac_name_id').text('Balance : '+resp.balance);
            
            let fetchedTemplatesResponse = await UA_TPA_FEATURES._getServiceTemplates();
            console.log('fetchedtemoktes ', fetchedTemplatesResponse);
            fetchedTemplatesCollection = fetchedTemplatesResponse.data;
        
            var sendersArray = [];
            var templatesArray = [];

            fetchedTemplatesCollection.forEach(item=>{
                if(item.status == "Active") {
                    let placeHoldersArr = [];
                    for(let count=0; count < item.templateData.split('{#var#}').length;count++) {
                        if(count != 0) {
                            placeHoldersArr.push(count);
                        }
                    }
                    UA_APP_UTILITY.fetchedTemplates[item.templateName] = {
                        'message': item.templateData,
                        'templateType': 'placeholder_template',
                        'placeholders': placeHoldersArr
                    };
                    templatesArray.push({
                        'label': item.templateName,
                        'value': item.templateName
                    });
                }
            });
            
            if(templatesArray.length > 0) {
                let approvedTemplateDDId = UA_APP_UTILITY.renderSelectableDropdown('#ssf-fitem-template-var-holder', 'Insert Mtalkz template', templatesArray, 'UA_TPA_FEATURES.insertTemplateContentInMessageInput', false, false);
            }
        
            var fetchedSenderResponse = await UA_TPA_FEATURES._getServiceSenders();
            console.log('fetchedtemoktes ', fetchedSenderResponse);
            fetchedSendersCollection = fetchedSenderResponse.data;
            
            fetchedSendersCollection.forEach(item=>{
                UA_APP_UTILITY.addMessaegSender(item.senderId, UA_APP_UTILITY.MESSAGING_CHANNELS.SMS, item.senderId);
                sendersArray.push({
                    'label': item.senderId,
                    'value': item.senderId
                });            
            });
            
            
            UA_APP_UTILITY.getPersonalWebhookAuthtoken();
           
            var countryCallCodeArray = [];
            UA_APP_UTILITY.countryCallingCodeArray.forEach(item=>{
                countryCallCodeArray.push({
                    'label': `${item.name} (${item.dial_code})`,
                    'value': item.dial_code
                });
            });
            
            UA_APP_UTILITY.renderSelectableDropdown('#ssf-new-recip-countrycode', 'Select country', countryCallCodeArray, 'UA_APP_UTILITY.log');

        });
        
    },
    
    renderWorkflowBodyCode :function(accessToken){
        
        UA_TPA_FEATURES.workflowCode.SMS.ulgebra_webhook_authtoken = accessToken.saas;
        //UA_TPA_FEATURES.workflowCode.WhatsApp.ulgebra_webhook_authtoken = accessToken.saas;
        
        UA_APP_UTILITY.addWorkflowBodyCode('sms', 'For Sending SMS', UA_TPA_FEATURES.workflowCode.SMS);
        
        //UA_APP_UTILITY.addWorkflowBodyCode('whatsapp', 'For Sending WhatsApp', UA_TPA_FEATURES.workflowCode.WhatsApp);
    },
    
    
    
    prepareAndSendSMS: function(){
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        if(UA_TPA_FEATURES.chosenMessgeTemplate && UA_TPA_FEATURES.chosenMessgeTemplate.templateType === "placeholder_template"){
            return UA_TPA_FEATURES.prepareAndSendBulkWhatsApp();
        }
        if(!UA_APP_UTILITY.currentSender){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var senderId = UA_APP_UTILITY.currentSender.value;
        if(!valueExists(senderId)){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var recipNumbers = UA_APP_UTILITY.getCurrentMessageRecipients();
        if(recipNumbers.length === 0){
            showErroWindow('Recipient list is empty!', "Kindly add recipients to send SMS.");
            return false;
        }
        var messageText = $("#inp-ssf-main-message").val().trim();
        var messageNumberMapArray = [];
        var messageHistoryMap = {};
        recipNumbers.forEach(item=>{
            let resolvedMessageText = UA_APP_UTILITY.getTemplateAppliedMessage(messageText, UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id]);
            var historyUID = item.id+''+new Date().getTime()+''+Math.round(Math.random(100000,99999)*1000000);
            messageNumberMapArray.push({
                'number': item.number,
                'text': resolvedMessageText,
                'clientuid': historyUID
            });
            messageHistoryMap[historyUID] = {
                'contact':{
                    'name': UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].Full_Name : item.number
                },
                'from': senderId,
                'to': item.number,
                'message': resolvedMessageText,
                'status': 'NOT_SENT',
                'module': UA_SAAS_SERVICE_APP.widgetContext.module,
                'moduleId': item.id,
                'channel': "SMS"
            };
        });
        
        if(!valueExists(messageText)){
            showErroWindow('Message is empty!', "Kindly fill message to proceed.");
            return false;
        }
        
        if(recipNumbers.length === 0){
            showErroWindow('Recipient list is empty!', "Kindly add recipients to send SMS.");
            return false;
        }
        
        UA_TPA_FEATURES.sendBulkSMS(senderId, messageNumberMapArray, (function(successSentMsgRes, successSentMsgCount){
            if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
                return false;
            }
            if(successSentMsgRes && typeof(successSentMsgRes.data) == 'object' && successSentMsgRes.data && typeof(successSentMsgRes.data) == 'object' && successSentMsgRes.data.Success && successSentMsgRes.data.Success == 'True' && successSentMsgRes.data.MessageUUID) {
                showErroWindow('Completed', `Your ${successSentMsgCount}/${messageNumberMapArray.length} messages has been sent successfully.`, false, true);
                UA_APP_UTILITY.resetMessageFormFields(); 
            }
            else{
                //showErroWindow('Unable to send message', apiResponse.data.status);
                return;              
            }
        }), messageHistoryMap);
        
    },
    
    
    
    
    
    insertTemplateContentInMessageInput :function(templateId){
        var curTemplateItem = UA_APP_UTILITY.fetchedTemplates[templateId];
        UA_TPA_FEATURES.chosenMessgeTemplate = curTemplateItem;
        $("#messageAttachmentInputHolder").hide();
        UA_APP_UTILITY.currentAttachedFile = null;
        $("#attachedfile").text('Attach file');
        $('#inputFileAttach').val('');
        if(curTemplateItem.templateType === "placeholder_template"){
            $("#primary-send-btn").text('Send SMS').css('background-color', 'green');
            if(curTemplateItem.head_mediatype){
                switch (curTemplateItem.head_mediatype) {
                    case "0":
                        $("#attachedfile").text('Attach document');
                        break;
                    case "1":
                        $("#attachedfile").text('Attach image');
                        break;
                    case "2":
                        $("#attachedfile").text('Attach video');
                        break;
                    default:
                        $("#attachedfile").text('Attach file');
                        break;
                }
            }
            if(curTemplateItem.head_media_url){
                $("#messageAttachmentInputHolder").show();
            }
            $("#ssf-fitem-template-placeholder-holder-listholder").html("");
            curTemplateItem.placeholders.forEach(item=>{
                $("#ssf-fitem-template-placeholder-holder-listholder").append(`
                    <div class="ssf-temp-placehold-item">
                            <span class="ssf-temp-placehold-item-label">{{${item}}}</span> <span class="material-icons ssf-temp-placehold-item-icon">double_arrow</span> <input onblur="UA_APP_UTILITY.lastFocusedInputElemForPlaceholderInsert = 'ssf-templ-place-input-${item}'" id="ssf-templ-place-input-${item}" data-placeholder-id="${item}" class="ssf-temp-placehold-item-input" type="text" placeholder="Type or choose field from above">
                        </div>
                `);
            });
            if(curTemplateItem.placeholders.length > 0){
                $("#ssf-fitem-template-placeholder-holder").show();
            }else{
                $("#ssf-fitem-template-placeholder-holder").hide();
            }
            $('#inp-ssf-main-message').attr('readonly', true);
        }
        else{
            $("#primary-send-btn").text('Send SMS').css('background-color', 'royalblue');
            $("#ssf-fitem-template-placeholder-holder").hide();
            $('#inp-ssf-main-message').removeAttr('readonly');
        }
        $('#inp-ssf-main-message').val(curTemplateItem.message).focus();
    },
    
    _getServiceSenders: async function(){

        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "http://msg.mtalkz.com/V2/http-senderid-api.php?apikey={{apikey}}", "GET");
        if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.data && typeof(response.data.data) == 'object' && response.data.data.length > 0 ) {
            return  response.data;
        }
        else {
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return false;
        }
        
    },
    
    _getServiceTemplates: async function() {

        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "http://msg.mtalkz.com/V2/http-template-api.php?apikey={{apikey}}", "GET");
        if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.data && typeof(response.data.data) == 'object' && response.data.data.length > 0 ) {
            return  response.data;
        }
        else {
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return false;
        }
        
    },
    
    _getServiceWATemplates: async function(silenceAuthError = false){
        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://restapi.smscountry.com/v0.1/Accounts/"+UA_TPA_FEATURES.tpaCredentials.authId+"/WhatsappTemplate/"+UA_TPA_FEATURES.tpaCredentials.ChannelUUID, "GET");
        if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.Message && response.data.Message == 'Success.' && response.data.Table) {
            return  response;
        }
        else {
            if(silenceAuthError){
                return {
                    data:{
                        data: []
                    }
                };
            }
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return;
        }
    },
    
    showReAuthorizeERROR: function(showCloseOption = false){
        if($('#inp_tpa_api_key').length > 0){
            console.log('already showing reautherr');
            return;
        }
        showErroWindow("Authentication needed!", `You need to authorize your Mtalkz Account to proceed. <br><br> <div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
        <div style="font-size: 18px;margin-bottom: 5px;margin-top: 20px;">Mtalkz API Key</div>
        <div class="help_apikey_tip"><a target="_blank" href="http://msg.mtalkz.com/main.php?page=apikey&u=u0">Get Here</a></div>
        <input id="inp_tpa_api_key" type="text" placeholder="Enter your API Key..." style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
        <button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.saveTPAAPIKey(UA_APP_UTILITY.reloadWindow)">Authorize Now</button>
    </div>`, showCloseOption, false, 500);
        $('#inp_tpa_api_key').focus();
        UA_LIC_UTILITY.showExistingInvitedAdminDDHTML(true);
    },
    
    openWABAAPIKey: function(showCloseOption = false){
        showErroWindow("WhatsApp Authentication needed!", `Enter your WABA API key. <br><br> <div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
        <div style="font-size: 18px;margin-bottom: 5px;margin-top: 20px;">Mtalkz ChannelUUID</div>
        <input id="inp_tpa_waba_api_key" type="text" placeholder="Enter your ChannelUUID..." style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
        <button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.saveTPAWABAAPIKey(UA_APP_UTILITY.reloadWindow)">Authorize Now</button>
    </div>`, showCloseOption, false, 500);
        $('#inp_tpa_waba_api_key').focus();
    },
    
    showReAuthorizeERRORWithClose: function(){
        UA_TPA_FEATURES.showReAuthorizeERROR();
    },
    
    saveTPAAPIKey: async function(callback){
        let authkey = $("#inp_tpa_api_key").val();
        if(!valueExists(authkey)){
            showErroMessage("Please fill API Key");
            return;
        }

        let isTpaAccountAutherized = await UA_TPA_FEATURES.isTpaAccountAutherized(extensionName, "http://msg.mtalkz.com/V2/http-balance-api.php?apikey="+authkey+"&format=json", "GET", {}, authkey);
        
        if(isTpaAccountAutherized) {
            await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', 'authtoken', authkey, callback);
        }
        else {
            showErroMessage("<b>Please provide Authorization, API Key is Incorrect.</b>");
            return;
        }

    },
    isTpaAccountAutherized: async function (extensionName, url, method, data, authid) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = {"Content-Type": "application/json"};
        return await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, url: url, method: method, data: data, headers: headers}).then(async (response) => {
            if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.data && typeof(response.data.data) == 'object' && response.data.data.data && typeof(response.data.data.data) == 'object' && response.data.data.data.balance) {
                removeTopProgressBar(credAdProcess3);
                return true;
            }
            else {
                removeTopProgressBar(credAdProcess3);
                return false;
            }
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
    },
    saveTPAWABAAPIKey: async function(callback){

        let authkey = $("#inp_tpa_waba_api_key").val();
        if(!valueExists(authkey)){
            showErroMessage("Please fill ChannelUUID");
            return;
        }
        let authtoken = $("#inp_tpa_waba_api_token").val();
        if(!valueExists(authtoken)){
            showErroMessage("Please fill WABA Id");
            return;
        }

        let isTpaWaBaAccountAutherized = await UA_TPA_FEATURES.isTpaWaBaAccountAutherized(extensionName, "https://restapi.smscountry.com/v0.1/Accounts/"+UA_TPA_FEATURES.tpaCredentials.authId+"/WhatsappTemplate/", "GET", {}, authkey);
        
        if(isTpaWaBaAccountAutherized) {
            await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', '__multiple_keys__', {'ChannelUUID' : authkey, 'WABAId': authtoken}, callback);
        }
        else {
            showErroMessage("<b>Please provide Authorization</b></br></br>ChannelUUID or WABA Id is Incorrect.");
            return;
        }
    },
    isTpaWaBaAccountAutherized: async function (extensionName, url, method, data, authid) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = {"Content-Type": "application/json"};
        return await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, TPAService: "mtalkz",url: url, method: method, data: data, headers: headers}).then(async (response) => {
            if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.data && typeof(response.data.data) == 'object' && response.data.data.Channels && response.data.data.Channels.filter(item=> item.ChannelUUID === authid).length) {
                console.log(response);
                removeTopProgressBar(credAdProcess3);
                return true;
            }
            else {
                removeTopProgressBar(credAdProcess3);
                return false;
            }
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
    },
    
    
    
    prepareAndSendBulkWhatsApp : function(){
        
        if(!UA_APP_UTILITY.currentSender){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var senderId = UA_APP_UTILITY.currentSender.value;
        if(!valueExists(senderId)){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var messageText = $("#inp-ssf-main-message").val().trim();
        if(!valueExists(messageText)){
            showErroWindow('Message is empty!', "Kindly fill message to proceed.");
            return false;
        }
        var recipNumbers = UA_APP_UTILITY.getCurrentMessageRecipients();
        if(recipNumbers.length === 0){
            showErroWindow('Recipient list is empty!', "Kindly add recipients to send SMS.");
            return false;
        }
        if(!UA_APP_UTILITY.fileUploadComplete){
            showErroWindow('File upload is not complete!', "Please wait while your file is uploading and try again.");
            return;
        }
        var messageNumberMapArray = [];
        var messageHistoryMap = {};
        $('#sms-prog-window').show();
            $('#msgItemsProgHolder').html('');
          var totalCount = recipNumbers.length;
          $('#totMsgDisp').text(`Sending messages 0/${totalCount}...`);
          var messagesSentCount = 0;
          var messageStatusMap = {
              'success': 0,
              'failed' : 0
          };
        recipNumbers.forEach(item=>{

            var contactRecordId = UA_APP_UTILITY.storedRecipientInventory[item.id] ? item.id : null;
            var historyUID = item.id+''+new Date().getTime()+''+Math.round(Math.random(100000,99999)*1000000);
            var resolvedMessageText = messageText;
            if(UA_TPA_FEATURES.chosenMessgeTemplate.placeholders){
                UA_TPA_FEATURES.chosenMessgeTemplate.placeholders.forEach(item=>{
                    var placeHolderValue = document.getElementById('ssf-templ-place-input-'+item).value;
                    placeHolderValue = UA_APP_UTILITY.getTemplateAppliedMessage(placeHolderValue, UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[contactRecordId]);
                    resolvedMessageText = resolvedMessageText.replace('{#var#}', placeHolderValue);
                });
            }
            messageNumberMapArray.push({
                'number': item.number,
                'text': resolvedMessageText,
                'clientuid': historyUID
            });
            messageHistoryMap[historyUID] = {
                'contact':{
                    'name': UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].Full_Name : item.number
                },
                'from': senderId,
                'to': item.number,
                'message': resolvedMessageText,
                'status': 'NOT_SENT',
                'module': UA_SAAS_SERVICE_APP.widgetContext.module,
                'moduleId': item.id,
                'channel': 'SMS'
            };
            $('#msgItemsProgHolder').append(`
              <div id="msg-resp-item-${parseInt(item.number)}" class="msgRespItem" title="${getSafeString(resolvedMessageText)}">
                  <div class="mri-label"><b>${$('.msgRespItem').length+1}</b>. SMS to ${UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].name+'-' : ''} ${item.number}</div>
                  <div class="mri-status">Sending...</div>
              </div>
          `);
    
        });

        UA_TPA_FEATURES.sendBulkSMS(senderId, messageNumberMapArray, (function(successSentMsgRes, successSentMsgCount){
            if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
                   return false;
               }
            if(successSentMsgRes && successSentMsgRes.data && typeof(successSentMsgRes.data) == 'object' && successSentMsgRes.data.message && successSentMsgRes.data.message == "message Submitted successfully" && successSentMsgRes.data.msgid && successSentMsgRes.data.data && typeof(successSentMsgRes.data.data) == 'object') {
                $('#totMsgDisp').text(`All messages have been processed. TOTAL: ${messageNumberMapArray.length}, SENT: ${successSentMsgCount}, FAILED: ${messageNumberMapArray.length-successSentMsgCount}`);
                UA_APP_UTILITY.resetMessageFormFields(); 
            }
            else{
                showErroWindow('Unable to send message', '');
                return;              
            }
        }), messageHistoryMap);
        
        
    },
    
    sendSMS : function(sender, number, text, callback){
        
        // var messageArray = [{
        //     "number": number,
        //     "text": text
        // }];
        
        // var messagePayload = {
        //     "sender": sender,
        //     "message": messageArray,
        //     "messagetype": "TXT"
        // };

        var messagePayload = {
            "Text" : text,
            "Number" : number,
            "SenderId" : sender,
            "DRNotifyHttpMethod" : "POST",
            "Tool":"API"
        };
        
        UA_TPA_FEATURES._proceedToSendSMSAndExecuteCallback(messagePayload, callback);
        
    },
    
    sendBulkSMS : async function(sender, numberMessageMapArray, callback, messageHistoryMap) {

        let successSentMsgCount = 0;
        
        for(let MsgMapArr=0; MsgMapArr < numberMessageMapArray.length; MsgMapArr++) {

            var messagePayload = {
                "message" : numberMessageMapArray[MsgMapArr].text,
                "number" : numberMessageMapArray[MsgMapArr].number,
                "senderid" : sender,
                "apikey": '{{apikey}}',
                "format": "json",
                'clientuid': numberMessageMapArray[MsgMapArr].clientuid
            };
            
            let successSentMsgRes = await UA_TPA_FEATURES._proceedToSendWAAndExecuteCallback(messagePayload, '');
            if(successSentMsgRes && successSentMsgRes.data && typeof(successSentMsgRes.data) == 'object' && successSentMsgRes.data.message && successSentMsgRes.data.message == "message Submitted successfully" && successSentMsgRes.data.msgid && successSentMsgRes.data.data && typeof(successSentMsgRes.data.data) == 'object') {
                successSentMsgCount++;                
                messageHistoryMap[messagePayload.clientuid].status = "SENT";
                $(`#msg-resp-item-${parseInt(numberMessageMapArray[MsgMapArr].number)} .mri-status`).text(successSentMsgRes.data.data[0].status).css({'color': 'green'});
            }
            else {
                $(`#msg-resp-item-${parseInt(numberMessageMapArray[MsgMapArr].number)} .mri-status`).text(successSentMsgRes.data.message).css({'color': 'crimson'});
            }

            $('#totMsgDisp').text(`Sending messages ${MsgMapArr+1}/${numberMessageMapArray.length}...`);
            if(MsgMapArr == numberMessageMapArray.length-1) {
                await UA_SAAS_SERVICE_APP.addSentSMSAsRecordInHistory(messageHistoryMap);
                await callback(successSentMsgRes, successSentMsgCount);
            }

        }
        
    },
    
    _proceedToSendSMSAndExecuteCallback: async function(messagePayload, callback) {
        if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        var sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://restapi.smscountry.com/v0.1/Accounts/${UA_TPA_FEATURES.tpaCredentials.authId}/SMSes/`, "POST", messagePayload);
        return sentAPIResponse;
    },
    
    _proceedToSendWAAndExecuteCallback: async function(messagePayload, callback){
        if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        var sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://msg.mtalkz.com/V2/http-api-post.php", "POST", messagePayload);
        return sentAPIResponse;
    },
    
    getAPIResponse: async function (extensionName, url, method, data) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = {"Content-Type": "application/json"};
        await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, TPAService: "mtalkz",url: url, method: method, data: data, headers: headers, _ua_lic_adminUserID: UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID}).then((response) => {
            response = response.data;
            returnResponse = response;
            removeTopProgressBar(credAdProcess3);
            return true;
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
        return returnResponse;
    },

    getCurrentAccountInfo: async function() {
        var headers = {"Content-Type": "application/json"};
        return await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, TPAService: "mtalkz",url: '--checkbalance', method: 'get', data: {}, headers: headers}).then(async (response) => {
            if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.status && response.data.status == 'success' && response.data.ulgebraAppConfiguration) {
                return response.data.ulgebraAppConfiguration;
            }
            else {
                return false;
            }
        }).catch(err => {
            console.log(err);
            return false;
        });
    },    
    tpaCredentials: false,
    getCurrentAccountOrBalanceInfo: async function(callback) {

        UA_TPA_FEATURES.tpaCredentials = await UA_TPA_FEATURES.getCurrentAccountInfo();

        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);

        if(!UA_TPA_FEATURES.tpaCredentials) {
            UA_TPA_FEATURES.showReAuthorizeERROR();
            removeTopProgressBar(credAdProcess3);
            return false;
        }
        else {

            console.log(UA_TPA_FEATURES.tpaCredentials);            
            var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "http://msg.mtalkz.com/V2/http-balance-api.php?apikey={{apikey}}&format=json", "GET", {});
            
            if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.data && typeof(response.data.data) == 'object' && response.data.data.balance) {
                callback(response.data.data);
                removeTopProgressBar(credAdProcess3);
                return true;
            }
            else {
                UA_TPA_FEATURES.showReAuthorizeERROR();
                removeTopProgressBar(credAdProcess3);
                return false;
            }
            

            return true;
        }
        
        
    }
    
};