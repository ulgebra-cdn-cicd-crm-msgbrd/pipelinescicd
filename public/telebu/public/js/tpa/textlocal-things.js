var UA_TPA_FEATURES = {
    chosenMessgeTemplate : null,
    'clientIds': {
        'textlocalforhubspotcrm' : "217e8bc7-73b3-4b0b-9a2f-e5e8eb756f3d",
        'textlocalforbitrix24' : "app.62fe43e60fe999.41270105",
        'textlocalforpipedrive': "79eb4474d9705365"
    },
    tpaSortName: 'TextLocal',
    getSupportedChannels: function(){
        return [ UA_APP_UTILITY.MESSAGING_CHANNELS.SMS ];
    },
    workflowCode : {
        "SMS": {
            "to": "FILL_HERE",
            "text": "FILL_HERE",
            "senderId": "FILL_HERE",
            "module": "FILL_HERE",
            "recordId": "FILL_HERE",
            "channel": "SMS",
            "ulgebra_webhook_authtoken": null
        }
    },
    renderInitialElements: async function(){
        
        $("#ac_name_label_tpa .anl_servicename").text(UA_TPA_FEATURES.tpaSortName);
        UA_TPA_FEATURES.getCurrentAccountOrBalanceInfo(async function(resp) {
            
            $('#ac_name_label_tpa .ac_name_id').text(`Balance : ${resp.balance.sms}`);

            var sendersArray = [];
            var sendersUniqueArray = [];
            
            var fetchedSenderNameListResponse = await UA_TPA_FEATURES._getServiceSenderNameList();
            if(fetchedSenderNameListResponse) {
                fetchedSenderNameListResponse.forEach(item=>{
                    if(item) {
                        sendersUniqueArray.push(item);
                        UA_APP_UTILITY.addMessaegSender(item, UA_APP_UTILITY.MESSAGING_CHANNELS.SMS, item);
                        sendersArray.push({
                            'label': item,
                            'value': item
                        });
                    }
                });
            }           
            UA_APP_UTILITY.TPA_SENDERS_FETCH_COMPLETED();
            var fetchedTemplatesResponse = await UA_TPA_FEATURES._getServiceTemplates();
            if(fetchedTemplatesResponse) {
                console.log('fetchedtemoktes ', fetchedTemplatesResponse);
                
                var templatesArray = [];
                fetchedTemplatesResponse.forEach((item, key)=>{
                    UA_APP_UTILITY.fetchedTemplates[key + ''] = {
                        'message': UA_TPA_FEATURES.getSafeString(item.body+'')
                    };
                    templatesArray.push({
                        'label': UA_TPA_FEATURES.getSafeString(item.title.replaceAll(' ', '_')),
                        'value': key + ''
                    });
                });
                
                var approvedTemplateDDId = UA_APP_UTILITY.renderSelectableDropdown('#ssf-fitem-template-var-holder', `Insert ${UA_TPA_FEATURES.tpaSortName} template`, templatesArray, 'UA_TPA_FEATURES.insertTemplateContentInMessageInput', false, false);
            }

            UA_APP_UTILITY.getPersonalWebhookAuthtoken();
           
            var countryCallCodeArray = [];
            UA_APP_UTILITY.countryCallingCodeArray.forEach(item=>{
                countryCallCodeArray.push({
                    'label': `${item.name} (${item.dial_code})`,
                    'value': item.dial_code
                });
            });
            
            UA_APP_UTILITY.renderSelectableDropdown('#ssf-new-recip-countrycode', 'Select country', countryCallCodeArray, 'UA_APP_UTILITY.log');
   
        
        });
        
    },
    
    renderWorkflowBodyCode :function(accessToken){
        
        UA_TPA_FEATURES.workflowCode.SMS.ulgebra_webhook_authtoken = accessToken.saas;
        //UA_TPA_FEATURES.workflowCode.WhatsApp.ulgebra_webhook_authtoken = accessToken.saas;
        
        UA_APP_UTILITY.addWorkflowBodyCode('sms', 'For Sending SMS', UA_TPA_FEATURES.workflowCode.SMS);
        
        //UA_APP_UTILITY.addWorkflowBodyCode('whatsapp', 'For Sending WhatsApp', UA_TPA_FEATURES.workflowCode.WhatsApp);
    },
    
    
    
    prepareAndSendSMS: function(){
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        if(UA_TPA_FEATURES.chosenMessgeTemplate && UA_TPA_FEATURES.chosenMessgeTemplate.templateType === "placeholder_template"){
            return UA_TPA_FEATURES.prepareAndSendBulkWhatsApp();
        }
        if(!UA_APP_UTILITY.currentSender){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var senderId = UA_APP_UTILITY.currentSender.value;
        if(!valueExists(senderId)){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }

        var recipNumbers = UA_APP_UTILITY.getCurrentMessageRecipients();
        
        if(recipNumbers.length === 0){
            showErroWindow('Recipient list is empty!', "Kindly add recipients to send SMS.");
            return false;
        }

        var messageText = $("#inp-ssf-main-message").val().trim();

        if(!valueExists(messageText)){
            showErroWindow('Message is empty!', "Kindly fill message to proceed.");
            return false;
        }

        $('#sms-prog-window').show();
        $('#msgItemsProgHolder').html('');
        $('#totMsgDisp').text(`Sending messages 0/${recipNumbers.length}...`);

        var messageNumberMapArray = [];
        var messageHistoryMap = {};
        recipNumbers.forEach(item=>{
            let resolvedMessageText = UA_APP_UTILITY.getTemplateAppliedMessage(messageText, UA_APP_UTILITY.storedRecipientInventory[item.id]);
            var historyUID = item.id+''+new Date().getTime()+''+Math.round(Math.random(100000,99999)*1000000);
            messageNumberMapArray.push({
                'number': item.number,
                'text': resolvedMessageText,
                'clientuid': historyUID
            });
            messageHistoryMap[historyUID] = {
                'contact':{
                    'name': UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].Full_Name : item.number
                },
                'from': senderId,
                'to': item.number,
                'message': resolvedMessageText,
                'status': 'NOT_SENT',
                'module': UA_SAAS_SERVICE_APP.widgetContext.module,
                'moduleId': item.id,
                'channel': 'SMS'
            };

            $('#msgItemsProgHolder').append(`
                <div id="msg-resp-item-${parseInt(item.number)}" class="msgRespItem" title="${getSafeString(resolvedMessageText)}">
                  <div class="mri-label"><b>${$('.msgRespItem').length+1}</b>. SMS to ${UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].name+'-' : ''} ${item.number}</div>
                  <div class="mri-status">Sending...</div>
                </div>
            `);

        });
    
        
        UA_TPA_FEATURES.sendBulkSMS(senderId, messageNumberMapArray, (function(successSentMsgRes, successSentMsgCount){
            if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
                    return false;
                }
            if(successSentMsgRes && typeof(successSentMsgRes) == 'object' && successSentMsgRes.data && typeof(successSentMsgRes.data) == 'object' && successSentMsgRes.data.messages && typeof(successSentMsgRes.data.messages) == 'object' && successSentMsgRes.data.messages[0] && typeof(successSentMsgRes.data.messages[0]) == 'object' && successSentMsgRes.data.messages[0].id && successSentMsgRes.data.status && successSentMsgRes.data.status === "success") {
                $('#totMsgDisp').text(`All messages have been processed. TOTAL: ${messageNumberMapArray.length}, SENT: ${successSentMsgCount}, FAILED: ${messageNumberMapArray.length-successSentMsgCount}`);
                UA_APP_UTILITY.resetMessageFormFields(); 
            }
            else{
                let errorMessage = 'Please try again later or check your textlocal balance.';
                try{
                    if (successSentMsgRes && typeof (successSentMsgRes) == 'object' && successSentMsgRes.data && typeof (successSentMsgRes.data) == 'object' && successSentMsgRes.data.messages_not_sent && typeof (successSentMsgRes.data.messages_not_sent) == 'object' && successSentMsgRes.data.messages_not_sent[0] && typeof (successSentMsgRes.data.messages_not_sent[0]) == 'object' && successSentMsgRes.data.messages_not_sent[0].error && typeof (successSentMsgRes.data.messages_not_sent[0].error) == 'object' && successSentMsgRes.data.messages_not_sent[0].error.message){
                        errorMessage = successSentMsgRes.data.messages_not_sent[0].error.message;
                    }
                }
                catch(ex){ console.log(ex);}
                showErroWindow('Error from TextLocal!', errorMessage);
                return;              
            }
        }), messageHistoryMap);
        
    },
    
    
    
    
    
    insertTemplateContentInMessageInput :function(templateId){
        var curTemplateItem = UA_APP_UTILITY.fetchedTemplates[templateId];
        UA_TPA_FEATURES.chosenMessgeTemplate = curTemplateItem;
        $("#messageAttachmentInputHolder").hide();
        UA_APP_UTILITY.currentAttachedFile = null;
        $("#attachedfile").text('Attach file');
        $('#inputFileAttach').val('');
        if(curTemplateItem.templateType === "placeholder_template"){
            $("#primary-send-btn").text('Send WhatsApp').css('background-color', 'green');
            if(curTemplateItem.head_mediatype){
                switch (curTemplateItem.head_mediatype) {
                    case "0":
                        $("#attachedfile").text('Attach document');
                        break;
                    case "1":
                        $("#attachedfile").text('Attach image');
                        break;
                    case "2":
                        $("#attachedfile").text('Attach video');
                        break;
                    default:
                        $("#attachedfile").text('Attach file');
                        break;
                }
            }
            if(curTemplateItem.head_media_url){
                $("#messageAttachmentInputHolder").show();
            }
            $("#ssf-fitem-template-placeholder-holder-listholder").html("");
            curTemplateItem.placeholders.forEach(item=>{
                $("#ssf-fitem-template-placeholder-holder-listholder").append(`
                    <div class="ssf-temp-placehold-item">
                            <span class="ssf-temp-placehold-item-label">{{${item}}}</span> <span class="material-icons ssf-temp-placehold-item-icon">double_arrow</span> <input onblur="UA_APP_UTILITY.lastFocusedInputElemForPlaceholderInsert = 'ssf-templ-place-input-${item}'" id="ssf-templ-place-input-${item}" data-placeholder-id="${item}" class="ssf-temp-placehold-item-input" type="text" placeholder="Type or choose field from above">
                        </div>
                `);
            });
            if(curTemplateItem.placeholders.length > 0){
                $("#ssf-fitem-template-placeholder-holder").show();
            }else{
                $("#ssf-fitem-template-placeholder-holder").hide();
            }
            $('#inp-ssf-main-message').attr('readonly', true);
        }
        else{
            $("#primary-send-btn").text('Send SMS').css('background-color', 'royalblue');
            $("#ssf-fitem-template-placeholder-holder").hide();
            $('#inp-ssf-main-message').removeAttr('readonly');
        }
        $('#inp-ssf-main-message').val(curTemplateItem.message).focus();
    },
    
    _getServiceSenderNameList: async function() {
        var senderListResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://{{domain}}/get_sender_names/", "post",{});
        if(senderListResponse && typeof(senderListResponse) == 'object' && senderListResponse.data && typeof(senderListResponse.data) == 'object' && senderListResponse.data.sender_names && typeof(senderListResponse.data.sender_names) == 'object' && senderListResponse.data.sender_names.length){
            return  senderListResponse.data.sender_names;
        }
        else {
            return false;
        }
    },
    
    _getServiceTemplates: async function(){
        var templatesResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://{{domain}}/get_templates/", "post",{});
        if(templatesResponse && typeof(templatesResponse) == 'object' && templatesResponse.data && typeof(templatesResponse.data) == 'object' && templatesResponse.data.templates && typeof(templatesResponse.data.templates) == 'object' && templatesResponse.data.templates.length){
            return  templatesResponse.data.templates;
        }
        else {
            return false;
        }
    },
    
    _getServiceChannels: async function(silenceAuthError = false){
        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://api.nexmo.com/beta/chatapp-accounts", "GET");
        if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data._embedded && typeof(response.data._embedded) == 'object' && response.data._embedded.length) {
            return response.data._embedded;
        }
        else {
            return false;
        }
    },
    
    showReAuthorizeERROR: function(showCloseOption = false){
        if($('#inp_tpa_api_key').length > 0){
            console.log('already showing reautherr');
            return;
        }
        showErroWindow("Authorization needed!", `You need to authorize your TextLocal Account to proceed. <br><br> <div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
        <div style="font-size: 18px;margin-bottom: 5px;margin-top: 20px;">TextLocal API Key</div>
        <div class="help_apikey_tip"><a target="_blank" href="${Intl.DateTimeFormat().resolvedOptions().timeZone === 'Asia/Calcutta' ? 'https://control.textlocal.in/settings/apikeys/' : 'https://control.txtlocal.co.uk/settings/apikeys/'}">Get Here</a></div>
        <input id="inp_tpa_api_key" type="text" placeholder="Enter your API Key" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">

        <button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.saveTPAAPIKey(UA_APP_UTILITY.reloadWindow)">Authorize</button>
    </div>`, showCloseOption, false, 500);
        $('#inp_tpa_api_key').focus();
        UA_LIC_UTILITY.showExistingInvitedAdminDDHTML(true);
    },
    
    openWABAAPIKey: function(showCloseOption = false){
        showErroWindow("Brand Authorization needed!", `<div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
        <div style="font-size: 18px;margin-bottom: 5px;margin-top: 20px;"> ${UA_TPA_FEATURES.tpaSortName} From</div>
        <input id="inp_tpa_waba_api_key" type="text" placeholder="Enter your From Name" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
        <button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.saveTPAWABAAPIKey(UA_APP_UTILITY.reloadWindow)">Save</button>
    </div>`, showCloseOption, false, 500);
        $('#inp_tpa_waba_api_key').focus();
    },
    
    showReAuthorizeERRORWithClose: function(){
        UA_TPA_FEATURES.showReAuthorizeERROR();
    },
    
    saveTPAAPIKey: async function(callback){
        let authtoken = $("#inp_tpa_api_key").val();
        if(!valueExists(authtoken)){
            showErroMessage("Please Enter API Key");
            return;
        }

        let isTpaAccountAutherized = await UA_TPA_FEATURES.isTpaAccountAutherized(extensionName, ``, "POST", {'apikey': authtoken});
        
        if(isTpaAccountAutherized) {
            await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', '__multiple_keys__', {'authtoken' : authtoken, 'api_domain': isTpaAccountAutherized}, callback);
        }
        else {
            showErroMessage("<b>Please provide Authorization</b></br></br>API Key is Incorrect.");
            return;
        }

    },
    isTpaAccountAutherized: async function (extensionName, url, method, data) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = {"Content-Type": "application/x-www-form-urlencoded"};
        url = `https://api.txtlocal.com/balance`;
        return await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, url: url, method: method, data: data, headers: headers}).then(async (response) => {
            if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.data && typeof(response.data.data) == 'object' && response.data.data.balance && typeof(response.data.data.balance) == 'object' && String(response.data.data.balance.sms)) {
                removeTopProgressBar(credAdProcess3);
                return 'api.txtlocal.com';
            }
            else {
                url = `https://api.textlocal.in/balance`;
                return await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, url: url, method: method, data: data, headers: headers}).then(async (response) => {
                    if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.data && typeof(response.data.data) == 'object' && response.data.data.balance && typeof(response.data.data.balance) == 'object' && String(response.data.data.balance.sms)) {
                        removeTopProgressBar(credAdProcess3);
                        return 'api.textlocal.in';
                    }
                    else {
                        removeTopProgressBar(credAdProcess3);
                        return false;
                    }
                }).catch(err => {
                    console.log(err);
                    removeTopProgressBar(credAdProcess3);
                    return false;
                });
            }
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
    },
    saveTPAWABAAPIKey: async function(callback){

        let authkey = $("#inp_tpa_waba_api_key").val();
        if(!valueExists(authkey)){
            showErroMessage("Please fill Sender Id");
            return;
        }

        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', 'senderId', authkey, callback);

    },
    isTpaWaBaAccountAutherized: async function (extensionName, url, method, data, authid) {
        let authkey = $("#inp_tpa_waba_api_key").val();
        if(!valueExists(authkey)){
            showErroMessage("Please fill From Name");
            return;
        }

        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', 'senderId', authkey, callback);
    },
    
    
    
    prepareAndSendBulkWhatsApp : function(){
        
        if(!UA_APP_UTILITY.currentSender){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var senderId = UA_APP_UTILITY.currentSender.value;
        if(!valueExists(senderId)){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var messageText = $("#inp-ssf-main-message").val().trim();
        if(!valueExists(messageText)){
            showErroWindow('Message is empty!', "Kindly fill message to proceed.");
            return false;
        }
        var recipNumbers = UA_APP_UTILITY.getCurrentMessageRecipients();
        if(recipNumbers.length === 0){
            showErroWindow('Recipient list is empty!', "Kindly add recipients to send SMS.");
            return false;
        }
        if(!UA_APP_UTILITY.fileUploadComplete){
            showErroWindow('File upload is not complete!', "Please wait while your file is uploading and try again.");
            return;
        }
        var messageNumberMapArray = [];
        var messageHistoryMap = {};
        $('#sms-prog-window').show();
            $('#msgItemsProgHolder').html('');
          var totalCount = recipNumbers.length;
          $('#totMsgDisp').text(`Sending messages 0/${totalCount}...`);
          var messagesSentCount = 0;
          var messageStatusMap = {
              'success': 0,
              'failed' : 0
          };
        recipNumbers.forEach(item=>{
            var message = {
                "templateid": UA_TPA_FEATURES.chosenMessgeTemplate.tempid
            };
            if(UA_APP_UTILITY.currentAttachedFile && UA_APP_UTILITY.currentAttachedFile.mediaUrl){
                message.url = UA_APP_UTILITY.currentAttachedFile.mediaUrl;
            }
            var contactRecordId = UA_APP_UTILITY.storedRecipientInventory[item.id] ? item.id : null;
            var resolvedMessageText = messageText + (message.url ? ' '+message.url : '');
            if(UA_TPA_FEATURES.chosenMessgeTemplate.placeholders){
                message.placeholders = [];
                UA_TPA_FEATURES.chosenMessgeTemplate.placeholders.forEach(item=>{
                    var placeHolderValue = $('#ssf-templ-place-input-'+item).val();
                    //if(contactRecordId){
                        placeHolderValue = UA_APP_UTILITY.getTemplateAppliedMessage(placeHolderValue, UA_APP_UTILITY.storedRecipientInventory[contactRecordId]);
                    //}
                    resolvedMessageText = resolvedMessageText.replace('{{'+item+'}}', placeHolderValue);
                    message.placeholders.push(placeHolderValue);
                });
            }
            var messagePayload = {
                "Text" : message,
                "Number" : item.number,
                "SenderId" : senderId,
                "DRNotifyHttpMethod" : "POST",
                "Tool":"API"
            };
            messageHistoryMap = {
                'contact':{
                    'name': UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].Full_Name : item.number
                },
                'from': senderId,
                'to': item.number,
                'message': resolvedMessageText,
                'status': 'NOT_SENT',
                'module': UA_SAAS_SERVICE_APP.widgetContext.module,
                'moduleId': item.id,
                'channel': 'WhatsApp'
            };

            $('#sms-prog-window').show();
            $('#msgItemsProgHolder').append(`
              <div id="msg-resp-item-${parseInt(item.number)}" class="msgRespItem" title="${getSafeString(resolvedMessageText)}">
                  <div class="mri-label"><b>${$('.msgRespItem').length+1}</b>. WhatsApp to ${UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].name+'-' : ''} ${item.number}</div>
                  <div class="mri-status">Sending...</div>
              </div>
          `);
            UA_TPA_FEATURES._proceedToSendWAAndExecuteCallback(messagePayload, function(response){
                if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
                    return false;
                }
                console.log(response);
                messagesSentCount++;
                $(`#msg-resp-item-${parseInt(item.number)} .mri-status`).text(response.data.status+' - '+response.data.message).css({'color': response.data.status === "SUCCESS" ? 'green' : 'crimson'});
                response.data.status === "SUCCESS" ? messageStatusMap.success++ : messageStatusMap.failed++;
                if(messagesSentCount === totalCount){
                    $('#totMsgDisp').text(`All messages have been processed. TOTAL: ${totalCount}, SENT: ${messageStatusMap.success}, FAILED: ${messageStatusMap.failed}`);
                }else{
                    $('#totMsgDisp').text(`Sending messages ${messagesSentCount}/${totalCount}...`);
                }
                messageHistoryMap.status = response.data.status === "SUCCESS" ? "SUCCESS" : response.data.status+"-"+response.data.message;
                UA_SAAS_SERVICE_APP.addSentSMSAsRecordInHistory({
                    "message1": messageHistoryMap
                });
            });
        });
        
        
    },
    
    sendSMS : function(sender, number, text, callback){
        
        // var messageArray = [{
        //     "number": number,
        //     "text": text
        // }];
        
        // var messagePayload = {
        //     "sender": sender,
        //     "message": messageArray,
        //     "messagetype": "TXT"
        // };

        var messagePayload = {
            "Text" : text,
            "Number" : number,
            "SenderId" : sender,
            "DRNotifyHttpMethod" : "POST",
            "Tool":"API"
        };
        
        UA_TPA_FEATURES._proceedToSendSMSAndExecuteCallback(messagePayload, callback);
        
    },
    
    sendBulkSMS : async function(sender, numberMessageMapArray, callback, messageHistoryMap) {

        let successSentMsgCount = 0;
        
        for(let MsgMapArr=0; MsgMapArr < numberMessageMapArray.length; MsgMapArr++) {

            
            //let msgUrl = 'https://{{domain}}/bulk_json';
            let msgText = numberMessageMapArray[MsgMapArr].text;
            let msgNumber = numberMessageMapArray[MsgMapArr].number.replace(/\D/g,'');
            // var messagePayload = {
            //         "sender": sender,
            //         "messages": [
            //             {
            //                 "number": msgNumber,
            //                 "text": encodeURIComponent(msgText)
            //             }
            //         ]
            //     };
            let msgUrl = `https://{{domain}}/send/?apikey={{apikey}}&sender=${sender}&numbers=${msgNumber}&message=${encodeURIComponent(msgText)}`;
                
        if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        
            
            //let successSentMsgRes = await UA_TPA_FEATURES.getAPIResponse(extensionName, msgUrl, "POST", {data:JSON.stringify(messagePayload)});
            let successSentMsgRes = await UA_TPA_FEATURES.getAPIResponse(extensionName, msgUrl, "GET", {});
            if(successSentMsgRes && typeof(successSentMsgRes) == 'object' && successSentMsgRes.data && typeof(successSentMsgRes.data) == 'object' && successSentMsgRes.data.messages && typeof(successSentMsgRes.data.messages) == 'object' && successSentMsgRes.data.messages[0] && typeof(successSentMsgRes.data.messages[0]) == 'object' && successSentMsgRes.data.messages[0].id && successSentMsgRes.data.status && successSentMsgRes.data.status === "success") {
                successSentMsgCount++;                
                messageHistoryMap[numberMessageMapArray[MsgMapArr].clientuid].status = "SENT";
                $(`#msg-resp-item-${parseInt(numberMessageMapArray[MsgMapArr].number)} .mri-status`).text(`Sent`).css({'color': 'green'});
            }
            else {
                $(`#msg-resp-item-${parseInt(numberMessageMapArray[MsgMapArr].number)} .mri-status`).text(`Failed`).css({'color': 'crimson'});
            }

            $('#totMsgDisp').text(`Sending messages ${MsgMapArr+1}/${numberMessageMapArray.length}...`);
            if(MsgMapArr == numberMessageMapArray.length-1) {
                await UA_SAAS_SERVICE_APP.addSentSMSAsRecordInHistory(messageHistoryMap);
                await callback(successSentMsgRes, successSentMsgCount);
            }

        }
        
    },
    
    _proceedToSendSMSAndExecuteCallback: async function(messagePayload, callback) {
        if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        var sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://api.rmlconnect.net/bulksms/bulksms?username=${credential.apikey}&password=${credential.senderId}&type=${msgType}&dlr=2&destination=${requestMap.to}&source=${credential.extraId}&message=${msgText}`, "POST", messagePayload);
        return sentAPIResponse;
    },
    
    _proceedToSendWAAndExecuteCallback: async function(messagePayload, callback){
        if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        var sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://api.pinbot.ai/v1/wamessage/sendMessage", "POST", messagePayload);
        callback(sentAPIResponse);
    },
    
    getAPIResponse: async function (extensionName, url, method, data) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = {"Content-Type": "application/x-www-form-urlencoded"};
        await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, TPAService: "textlocal",url: url, method: method, data: data, headers: headers, _ua_lic_adminUserID: UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID}).then((response) => {
            response = response.data;
            returnResponse = response;
            removeTopProgressBar(credAdProcess3);
            return true;
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
        return returnResponse;
    },

    getCurrentAccountInfo: async function() {
        var headers = {"Content-Type": "application/json"};
        return await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, TPAService: "textlocal",url: '--checkbalance', method: 'get', data: {}, headers: headers}).then(async (response) => {
            if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.status && response.data.status == 'success' && response.data.ulgebraAppConfiguration) {
                return response.data.ulgebraAppConfiguration;
            }
            else {
                return false;
            }
        }).catch(err => {
            console.log(err);
            return false;
        });
    },    
    tpaCredentials: false,
    getCurrentAccountOrBalanceInfo: async function(callback) {

        UA_TPA_FEATURES.tpaCredentials = await UA_TPA_FEATURES.getCurrentAccountInfo();

        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);

        if(!UA_TPA_FEATURES.tpaCredentials) {
            UA_TPA_FEATURES.showReAuthorizeERROR();
            removeTopProgressBar(credAdProcess3);
            return false;
        }
        else {

            console.log(UA_TPA_FEATURES.tpaCredentials);         
            var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://{{domain}}/balance", "POST", {});
            if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.balance && typeof(response.data.balance) == 'object' && String(response.data.balance.sms)) {
                callback(response.data);
                removeTopProgressBar(credAdProcess3);
                return true;
            }
            else {
                UA_TPA_FEATURES.showReAuthorizeERROR();
                removeTopProgressBar(credAdProcess3);
                return false;
            }            

            return true;
        }
        
        
    },
    getSafeString: function(rawStr) {
        if (!rawStr || rawStr+"".trim() === "") {
            return "";
        }

        return $('<textarea/>').text(rawStr).html();
    }
    
};
