var UA_TPA_FEATURES = {
    chosenMessgeTemplate : null,
    'clientIds': {
        'whatcetraforhubspotcrm' : "2b90ed06-f4fc-4ef7-bb1e-89346dea1461",
        'whatcetraforbitrix24' : "app.5fca3756b0c5c2.28053584",
        'whatcetraforpipedrive': 'd688695f917bf352'
    },
    tpaSortName: 'WhatCetra',
    getSupportedChannels: function(){
        return [ UA_APP_UTILITY.MESSAGING_CHANNELS.SMS, UA_APP_UTILITY.MESSAGING_CHANNELS.MMS ];
    },
    workflowCode : {
        "WHATSAPP": {
            "to": "FILL_HERE",
            "text": "FILL_HERE",
            "mediaUrl": "FILL_HERE (Upload Link from https://app.whatcetra.com/#Files to send attachment. ! Not Mandatory )",
            "module": "FILL_HERE",
            "recordId": "FILL_HERE",
            "channel": "WHATSAPP",
            "ulgebra_webhook_authtoken": null
        }
    },
    renderInitialElements: async function() {

        $('#suo-item-workflow-nav').after(`<div id="suo-item-workflow-chrome" class="suo-item suo-item-ac-labels" onclick="UA_TPA_FEATURES.initiateAuth()">
                                        <span class="material-icons">get_app</span> Re-Install ${UA_TPA_FEATURES.tpaSortName} <span class="anl_servicename"></span><div class="ac_name_id">Click to configure</span>
                                    </div></div>`);
        
        $(".incoming-config-tpa-entity").text('messages');
        $("#primary-send-btn").html('<span class="material-icons ssf-fitem-ttl-icon">send</span> Send WhatsApp');
        
        $("#ac_name_label_tpa .anl_servicename").text(UA_TPA_FEATURES.tpaSortName);
        $('#WHATSAPP_CONV_WINDOW_POPUP').hide();
        
        if(extensionName.endsWith("forhubspotcrm")){
            UA_TPA_FEATURES.fetchLICDetails(UA_TPA_FEATURES.executeAfterBalanceCheck);
        }
        else{
            UA_TPA_FEATURES.getCurrentAccountOrBalanceInfo(UA_TPA_FEATURES.executeAfterBalanceCheck);
        } 
        
    },
    executeAfterBalanceCheck: async function(resp) {
        
            let name = 'Whatcetra';
            if(resp.name) {
                name = resp.name;
            }
            
            $('#ac_name_label_tpa .ac_name_id').html(`<b>${name}</b>, Balance : ${resp.balance}`);

            // $('#DD_HOLDER_SENDER_LIST').before(`<div class="ssf-fitem-ans" id="DD_HOLDER_MsgType_LIST"><div class="ssf-fitem-label">Channel</div></div>`)
            // UA_APP_UTILITY.renderSelectableDropdown('#DD_HOLDER_MsgType_LIST', 'Select a Channel', [{'label': 'SMS', 'value': 'SMS'}], 'UA_APP_UTILITY.log', true);
            // $('#DD_HOLDER_MsgType_LIST .dropdownInner div').first().click();

            // $('#DD_HOLDER_MsgType_LIST').find('.dropdownOuter .dropdownInner.focusCancellerOverlaySupported').children().last().text('Group ID   ');
            // let groupIdText = $('#DD_HOLDER_MsgType_LIST').find('.dropdownOuter .dropdownInner.focusCancellerOverlaySupported').children().last().attr('onclick');
            // $('#DD_HOLDER_MsgType_LIST').find('.dropdownOuter .dropdownInner.focusCancellerOverlaySupported').children().last().attr('onclick', groupIdText.replace(`'Other'`, `'Group ID'`));
            
            $('#DD_HOLDER_SENDER_LIST').hide();
            $('#DD_HOLDER_SENDER_LIST').parent().find('.ssf-fitem-label').first().show();

            $('#DD_HOLDER_SENDER_LIST').before(`<div class="ssf-fitem-ans" id="DD_HOLDER_MsgType_LIST" style="top: 0px;">
                    <div style="width: max-content;">
                        <div class="DD_HOLDER_SENDER_LIST_inner" style="display: flex;padding: 10px 20px 10px 15px; box-shadow: 0px 2px 5px rgb(0 0 0 / 30%); border-radius: 5px; margin: 10px 0px; color: rgb(50,50,50);">
                            <div style="width: 25px; border-radius: 100%; overflow: hidden; height: 25px;">
                                <img title="${name}" src="${resp.pic}" onload="$(this).css({'display': 'block'});" onerror="UA_TPA_FEATURES.wcProfileImgFileError(this);" style="width: 100%; height: 100%;"/>
                            </div>
                            <div style="padding-left: 10px;color:grey;cursor:default;padding-top: 3px;" id="connectedWhatsAppDiv">
                                ${name}
                            </div>
                        </div>
                    </div>
                </div>`);

            $('#messageAttachmentInputHolder').show();

            UA_APP_UTILITY.getPersonalWebhookAuthtoken();
           
            var countryCallCodeArray = [];
            UA_APP_UTILITY.countryCallingCodeArray.forEach(item=>{
                countryCallCodeArray.push({
                    'label': `${item.name} (${item.dial_code})`,
                    'value': item.dial_code
                });
            });
            
            UA_APP_UTILITY.renderSelectableDropdown('#ssf-new-recip-countrycode', 'Select country', countryCallCodeArray, 'UA_APP_UTILITY.log');
   
            $('#nav_viewTypeSwitch').find('.dropdownInner').find('.ddi-item').each(function() {
                $(this).attr('onclick', $(this).attr('onclick')+';UA_TPA_FEATURES.conversationViewChangeStyle(this);');
            });

            UA_TPA_FEATURES.tpaOnlyConversationView();

            if(extensionName === 'whatcetraforzohocrm') {

                await UA_SAAS_SERVICE_APP.searchRecord(UA_SAAS_SERVICE_APP.APP_API_NAME[extensionName].API_NAME + "WhatsApp_Templates", `(${UA_SAAS_SERVICE_APP.APP_API_NAME[extensionName].API_NAME + "Module_Name"}:equals:${UA_SAAS_SERVICE_APP.widgetContext.module})`).then(function(resp) {           

                    var fetchedTemplatesResponse = resp;
                    if(fetchedTemplatesResponse) {
                        console.log('fetchedtemoktes ', fetchedTemplatesResponse);
                        let fetchedTemplatesCollection = fetchedTemplatesResponse;

                        //UA_APP_UTILITY._feature_showUAMsgTemplates = false;
                        
                        var templatesArray = [];
                        fetchedTemplatesCollection.forEach((item, key)=>{
                            let tempMsgText = item[UA_SAAS_SERVICE_APP.APP_API_NAME[extensionName].API_NAME + "WhatsApp_Message"].split(UA_SAAS_SERVICE_APP.widgetContext.module+'.');
                            for(let i=0; i<tempMsgText.length;i++) {
                                if(i!==0) {
                                    tempMsgText[i] = tempMsgText[i].split('}')[0].replaceAll(' ', '_')+"}"+tempMsgText[i].split('}')[1];
                                }
                                else {
                                    tempMsgText[i] = tempMsgText[i]+UA_SAAS_SERVICE_APP.widgetContext.module+'.';
                                }
                            }
                            tempMsgText = tempMsgText.join('');
                            UA_APP_UTILITY.fetchedTemplates[key+''] = {
                                'message': tempMsgText
                            };
                            templatesArray.push({
                                'label': item['Name'],
                                'value': key+''
                            });
                        });
                        
                        var approvedTemplateDDId = UA_APP_UTILITY.renderSelectableDropdown('#ssf-fitem-template-var-holder', `Insert Old ${UA_TPA_FEATURES.tpaSortName} template`, templatesArray, 'UA_TPA_FEATURES.insertTemplateContentInMessageInput', false, false);
                    }

                });

            }

        
    },
    tpaOnlyConversationView: async function() {

        if(!Object.keys(UA_APP_UTILITY.storedRecipientInventory).length) {
            setTimeout(function() {UA_TPA_FEATURES.tpaOnlyConversationView();}, 3000);
            return;
        }

        let queryParams = new URLSearchParams(window.location.search);
        if(queryParams.get('view') && queryParams.get('view') === 'conversation') {
            $('#nav_viewTypeSwitch').find('.dropdownInner').find('.ddi-item').each(async function() {
                if($(this).text().trim() === "Conversations View") {
                    $(this).click();
                    $('#nav_viewTypeSwitch').hide();
                    $('.messagePeopleHeaderOuter').hide();
                    UA_TPA_FEATURES.loadWhatcetra($(this).text().trim());
                }
            });
            return;
        }
        
        $('#nav_viewTypeSwitch').find('.dropdownInner').find('.ddi-item').each(function() {
            $(this).attr('onclick', $(this).attr('onclick')+';UA_TPA_FEATURES.conversationViewChangeStyle(this);');
        });

        return;

    },
    loadWhatcetra: async function(view) {

        $('#uaCHATConvListHolder').css({'width': '100%', 'z-index': '10'}).show();
        $('#DD_HOLDER_RECIPIENT_LIST').addClass('DD_HOLDER_RECIPIENT_LIST_WA');
        $('.messagePeopleHeaderOuter').addClass('messagePeopleHeaderOuter_WA');

        if(view === "Chat View") {

            $('#uaCHATConvListHolder').css({'height': 'calc(100% - 112px)', 'top': '112px'});

            $('#DD_HOLDER_MsgType_LIST').css({'height': '0px', 'top': '-20px'});
            $('#DD_HOLDER_MsgType_LIST').find('.DD_HOLDER_SENDER_LIST_inner').css({'box-shadow': 'none'});
            
            $('.messagePeopleHeader').find('.ssf-fitem').each(function() {
                let headTitle = $(this).find('.ssf-fitem-label').text().trim().replaceAll(' ', '').replaceAll('\n', '');
                if(headTitle.indexOf("Recipients+AddRecipients") >= 0) {
                    $(this).show().css({'position': 'relative', 'right': '6px', 'justify-content': 'flex-end', 'display': 'flex'});
                    $(this).find('.ssf-fitem-label').hide();
                    $(this).find('.dropdownInner').css({'left': 'auto', 'right': '0px'});
                }
                else if(headTitle === "Sender") {
                    $(this).show();
                }
                else {
                    $(this).hide();
                }
            });
            
            let fetchedNumbers = {};
        
            await UA_APP_UTILITY.getCurrentMessageRecipients(true).forEach(async function (fetchNumbers) {

                fetchedNumbers[fetchNumbers.id+'-_-'+fetchNumbers.number.replace(/\D/g,'')] = fetchNumbers.number.replace(/\D/g,'');

            });      

            let selectedPhoneType = $('.ssfrpt-item.selected').text().trim().replaceAll(' ', '').replaceAll('\n', '_').split('_').filter(function (v) {
              return v != '';
            });

            $('#DD_HOLDER_RECIPIENT_LIST').find('.dropdownInner').find('.ddi-item').each(function() {
                let thisnumberPhoneType = $(this).attr('data-ddlabel').trim().split(' ');
                thisnumberPhoneType = thisnumberPhoneType[thisnumberPhoneType.length-1];
                let fetchedNumberId = $(this).attr('onclick').split('-_-')[0].split("', '")[2];            
                let fetched_Number = $(this).attr('onclick').split('-_-')[1].split("', '")[0];
                if(fetchedNumbers[fetchedNumberId+'-_-'+fetched_Number] && fetchedNumbers[fetchedNumberId+'-_-'+fetched_Number] === fetched_Number) {
                    let newFuncSet = $(this).attr('onclick').replace('UA_APP_UTILITY.dropDownOptionSelected', 'UA_TPA_FEATURES.chatViewFuncChange');
                    $(this).show().attr('onclick', `${newFuncSet}`);
                }
                else {
                    $(this).hide();
                }
            }); 

            $('#DD_HOLDER_RECIPIENT_LIST').find('.dropdownInner').find('.ddi-item').first().click();

        }
        else {

            $('#uaCHATConvListHolder').css({'height': 'calc(100% - 0px)', 'top': '50px'});

            if(extensionName === 'whatcetraforzohocrm') {
                await UA_SAAS_SERVICE_APP.changedCurrentView();
            }
            let fetchedNumbers = '';
            let getFetchedNumbers = await UA_APP_UTILITY.getCurrentMessageRecipients(true);

            for(let i=0; i < getFetchedNumbers.length; i++) {
                if(fetchedNumbers) {
                    fetchedNumbers = fetchedNumbers+','+getFetchedNumbers[i].number.replace(/\D/g,'');
                }
                else {
                    fetchedNumbers = getFetchedNumbers[i].number.replace(/\D/g,'');
                }
                if(i === getFetchedNumbers.length-1) {
                    $('#uaCHATConvListHolder').html('<iframe frameBorder="0" src="https://app.whatcetra.com/?contactNumber='+fetchedNumbers+'&embed=true" width="100%" height="100%"></iframe>');
                }
            }
        }

    
    },
    conversationViewChangeStyle: async function(selected) {
        if($(selected).text().trim() !== "Form View") {

            $('#uaCHATConvListHolder').html('');
            UA_TPA_FEATURES.ulgloaderDiv("#uaCHATConvListHolder", "30px");
            $('#uaCHATConvListHolder').find('.loaderOuterDiv').css({'width': '100%', 'height': '100%', 'display': 'flex', 'align-items': 'center'});

            if($(selected).text().trim() === "Chat View") {
                $('.messagePeopleHeaderOuter').show();
            }
            else {
                $('.messagePeopleHeaderOuter').hide();
            }
            UA_TPA_FEATURES.loadWhatcetra($(selected).text().trim());

        }
        else {
            $('#uaCHATConvListHolder').hide();
            $('.messagePeopleHeaderOuter').show();

            $('#DD_HOLDER_MsgType_LIST').css({'height': 'auto', 'top': '0px'});
            $('#DD_HOLDER_MsgType_LIST').find('.DD_HOLDER_SENDER_LIST_inner').css({'box-shadow': '0px 2px 5px rgb(0 0 0 / 30%)'});
            
            $('.messagePeopleHeader').find('.ssf-fitem').each(function() {
                let headTitle = $(this).find('.ssf-fitem-label').text().trim().replaceAll(' ', '').replaceAll('\n', '');
                if(headTitle.indexOf("Recipients+AddRecipients") >= 0) {
                    $(this).show().css({'position': 'relative', 'right': '0px', 'justify-content': 'normal', 'display': 'block'});
                    $(this).find('.ssf-fitem-label').show();
                    $(this).find('.dropdownInner').css({'left': '0px'});
                }
                else if(headTitle === "Sender") {
                    $(this).show();
                }
                else {
                    $(this).hide();
                }
            });

        }
    },
    chatViewFuncChange: function(event, ddElemId, label, value, callbackFunc, optionInvokeFunction) {

        $('#uaCHATConvListHolder').html('');
        UA_TPA_FEATURES.ulgloaderDiv("#uaCHATConvListHolder", "30px");
        $('#uaCHATConvListHolder').find('.loaderOuterDiv').css({'width': '100%', 'height': '100%', 'display': 'flex', 'align-items': 'center'});

        $('#uaCHATConvListHolder').html('<iframe frameBorder="0" src="https://app.whatcetra.com/?contactNumber='+value.split('-_-')[1].replace(/\D/g,'')+'&embed=true" width="100%" height="100%"></iframe>');
        console.log(event, ddElemId, label, value, callbackFunc, optionInvokeFunction);
        $('#DD_HOLDER_RECIPIENT_LIST').find('.dropdownDisplayText').text(label);
        UA_APP_UTILITY.removeFocusCancellerOverlay();

    },
    wcProfileImgFileError(selected) {
        $(selected).parent().append('<svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v1c0 .55.45 1 1 1h14c.55 0 1-.45 1-1v-1c0-2.66-5.33-4-8-4z"/></svg>');
        $(selected).css({'display': 'none'});
    },
    textCopyInCommand: function(selected) {
        let copyFrom = $('<textarea/>');
        copyFrom.text($(selected).attr('data-copyText'));
        $('body').append(copyFrom);
        copyFrom.select();
        document.execCommand('copy');
        copyFrom.remove();
    },
    renderWorkflowBodyCode :function(accessToken){
        
        //UA_TPA_FEATURES.workflowCode.WhatsApp.ulgebra_webhook_authtoken = accessToken.saas;
        UA_TPA_FEATURES.workflowCode.WHATSAPP.ulgebra_webhook_authtoken = accessToken.saas;
        
        //UA_APP_UTILITY.addWorkflowBodyCode('whatsapp', 'For Sending WhatsApp Text Messages', UA_TPA_FEATURES.workflowCode.WhatsApp);
        
        UA_APP_UTILITY.addWorkflowBodyCode('whatsapp', 'For Sending WhatsApp Messages with Attachment', UA_TPA_FEATURES.workflowCode.WHATSAPP);

    }, 
    prepareAndSendSMS: function(){
        
        document.body.scrollTop = document.documentElement.scrollTop = 0;

        if(!UA_APP_UTILITY.fileUploadComplete){
            showErroWindow('File upload is not complete!', "Please wait while your file is uploading and try again.");
            return;
        }
        
        let channel = 'WHATSAPP';
        // let channel = $('#DD_HOLDER_MsgType_LIST .dropdownOuter').attr('data-selectedval');
        
        // if(channel === "_-_other_-_") {
        //     channel = $('#DD_HOLDER_MsgType_LIST .dd-item-other-input').val().trim();
        // }

        // if(!valueExists(channel)){
        //     showErroWindow('Channel is empty!', "Kindly choose Channel to proceed.");
        //     return false;
        // }

        var recipNumbers = UA_APP_UTILITY.getCurrentMessageRecipients();
        
        if(recipNumbers.length === 0){
            showErroWindow('Recipient list is empty!', "Kindly add recipients to send WhatsApp.");
            return false;
        }

        var messageText = $("#inp-ssf-main-message").val().trim();

        if(!valueExists(messageText)){
            showErroWindow('Message is empty!', "Kindly fill message to proceed.");
            return false;
        }

        var mediaUrl = "";
        if(UA_APP_UTILITY.currentAttachedFile && UA_APP_UTILITY.currentAttachedFile.mediaUrl){
            mediaUrl = UA_APP_UTILITY.currentAttachedFile.mediaUrl;
        }

        $('#sms-prog-window').show();
        $('#msgItemsProgHolder').html('');
        $('#totMsgDisp').text(`Sending messages 0/${recipNumbers.length}...`);

        var messageNumberMapArray = [];
        var messageHistoryMap = {};
        recipNumbers.forEach(item=>{
            let resolvedMessageText = UA_APP_UTILITY.getTemplateAppliedMessage(messageText, UA_APP_UTILITY.storedRecipientInventory[item.id]);
            var historyUID = item.id+''+new Date().getTime()+''+Math.round(Math.random(100000,99999)*1000000);
            messageNumberMapArray.push({
                'number': item.number,
                'text': resolvedMessageText,
                'mediaUrl': mediaUrl,
                'channel': channel,
                'clientuid': historyUID
            });
            messageHistoryMap[historyUID] = {
                'contact':{
                    'name': UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].Full_Name : item.number
                },
                'from': channel,
                'to': item.number,
                'message': mediaUrl ? resolvedMessageText+' '+mediaUrl : resolvedMessageText,
                'status': 'NOT_SENT',
                'module': UA_SAAS_SERVICE_APP.widgetContext.module,
                'moduleId': item.id,
                'channel': 'WHATSAPP'
            };
            
            if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
                let messagePayload = {
                            "to": item.number.replace(/\D/g,''),
                            "text": resolvedMessageText,
                            "channel": "WHATSAPP"
                        };
                if(mediaUrl) {
                    messagePayload["mediaUrl"] = mediaUrl;
                }
                UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            }

            $('#msgItemsProgHolder').append(`
                <div id="msg-resp-item-${parseInt(item.number)}" class="msgRespItem" title="${getSafeString(resolvedMessageText)}">
                  <div class="mri-label"><b>${$('.msgRespItem').length+1}</b>. WhatsApp to ${UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].name+'-' : ''} ${item.number}</div>
                  <div class="mri-status">Sending...</div>
                </div>
            `);

        });
        
        if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }    
        
        UA_TPA_FEATURES.sendBulkSMS(messageNumberMapArray, (function(successSentMsgRes, successSentMsgCount){
            if(successSentMsgRes && typeof(successSentMsgRes) == 'object' && successSentMsgRes.data && typeof(successSentMsgRes.data) == 'object' && successSentMsgRes.data.id && successSentMsgRes.data.status && successSentMsgRes.data.status === "queued") {
                $('#totMsgDisp').text(`All messages have been processed. TOTAL: ${messageNumberMapArray.length}, SENT: ${successSentMsgCount}, FAILED: ${messageNumberMapArray.length-successSentMsgCount}`);
                UA_APP_UTILITY.resetMessageFormFields(); 
            }
            else{
                //showErroWindow('Unable to send message', '');
                return;              
            }
        }), messageHistoryMap);
        
    },  
    showMessagingWorkflowCode: async function(messagePayload) {
        
        if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        
    },    
    insertTemplateContentInMessageInput :function(templateId){
        var curTemplateItem = UA_APP_UTILITY.fetchedTemplates[templateId];
        UA_TPA_FEATURES.chosenMessgeTemplate = curTemplateItem;
        //$("#messageAttachmentInputHolder").hide();
        UA_APP_UTILITY.currentAttachedFile = null;
        $("#attachedfile").text('Attach file');
        $('#inputFileAttach').val('');
        if(curTemplateItem.templateType === "placeholder_template"){
            $("#primary-send-btn").text('Send WhatsApp').css('background-color', 'green');
            if(curTemplateItem.head_mediatype){
                switch (curTemplateItem.head_mediatype) {
                    case "0":
                        $("#attachedfile").text('Attach document');
                        break;
                    case "1":
                        $("#attachedfile").text('Attach image');
                        break;
                    case "2":
                        $("#attachedfile").text('Attach video');
                        break;
                    default:
                        $("#attachedfile").text('Attach file');
                        break;
                }
            }
            if(curTemplateItem.head_media_url){
                $("#messageAttachmentInputHolder").show();
            }
            $("#ssf-fitem-template-placeholder-holder-listholder").html("");
            curTemplateItem.placeholders.forEach(item=>{
                $("#ssf-fitem-template-placeholder-holder-listholder").append(`
                    <div class="ssf-temp-placehold-item">
                            <span class="ssf-temp-placehold-item-label">{{${item}}}</span> <span class="material-icons ssf-temp-placehold-item-icon">double_arrow</span> <input onblur="UA_APP_UTILITY.lastFocusedInputElemForPlaceholderInsert = 'ssf-templ-place-input-${item}'" id="ssf-templ-place-input-${item}" data-placeholder-id="${item}" class="ssf-temp-placehold-item-input" type="text" placeholder="Type or choose field from above">
                        </div>
                `);
            });
            if(curTemplateItem.placeholders.length > 0){
                $("#ssf-fitem-template-placeholder-holder").show();
            }else{
                $("#ssf-fitem-template-placeholder-holder").hide();
            }
            $('#inp-ssf-main-message').attr('readonly', true);
        }
        else{
            $("#primary-send-btn").text('Send WhatsApp').css('background-color', 'royalblue');
            $("#ssf-fitem-template-placeholder-holder").hide();
            $('#inp-ssf-main-message').removeAttr('readonly');
        }
        $('#inp-ssf-main-message').val(curTemplateItem.message).focus();
    },
    
    _getServiceSenderNameList: async function() {
        var senderListResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://{{domain}}/get_sender_names/", "post",{});
        if(senderListResponse && typeof(senderListResponse) == 'object' && senderListResponse.data && typeof(senderListResponse.data) == 'object' && senderListResponse.data.sender_names && typeof(senderListResponse.data.sender_names) == 'object' && senderListResponse.data.sender_names.length){
            return  senderListResponse.data.sender_names;
        }
        else {
            return false;
        }
    },
    
    _getServiceTemplates: async function(){
        var templatesResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://{{domain}}/get_templates/", "post",{});
        if(templatesResponse && typeof(templatesResponse) == 'object' && templatesResponse.data && typeof(templatesResponse.data) == 'object' && templatesResponse.data.templates && typeof(templatesResponse.data.templates) == 'object' && templatesResponse.data.templates.length){
            return  templatesResponse.data.templates;
        }
        else {
            return false;
        }
    },
    
    _getServiceChannels: async function(silenceAuthError = false){
        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://api.nexmo.com/beta/chatapp-accounts", "GET");
        if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data._embedded && typeof(response.data._embedded) == 'object' && response.data._embedded.length) {
            return response.data._embedded;
        }
        else {
            return false;
        }
    },
    
    showReAuthorizeERROR: function(showCloseOption = false){
        if($('#inp_tpa_api_key').length > 0){
            console.log('already showing reautherr');
            return;
        }
        showErroWindow("Authorization needed!", `You need to authorize your ${UA_TPA_FEATURES.tpaSortName} Account to proceed. <br><br> <div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
        
        <div style="font-size: 18px;margin-bottom: 5px;margin-top: 20px;">${UA_TPA_FEATURES.tpaSortName} User ID</div>
        <div class="help_apikey_tip"><a target="_blank" href="https://app.whatcetra.com/#Integration">Get Here</a></div>
        <input id="inp_tpa_user_id" type="text" placeholder="Enter your User ID" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
        
        <div style="font-size: 18px;margin-bottom: 5px;margin-top: 20px;">${UA_TPA_FEATURES.tpaSortName} API Key</div>
        <div class="help_apikey_tip"><a target="_blank" href="https://app.whatcetra.com/#Integration">Get Here</a></div>
        <input id="inp_tpa_api_key" type="text" placeholder="Enter your API Key" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">

        <button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.saveTPAAPIKey(UA_APP_UTILITY.reloadWindow)">Authorize</button>
    </div>`, showCloseOption, false, 500);
        $('#inp_tpa_api_key').focus();
        UA_LIC_UTILITY.showExistingInvitedAdminDDHTML(true);
    },
    
    openWABAAPIKey: function(showCloseOption = false){
        showErroWindow("Brand Authentication needed!", `<div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
        <div style="font-size: 18px;margin-bottom: 5px;margin-top: 20px;"> ${UA_TPA_FEATURES.tpaSortName} From</div>
        <input id="inp_tpa_waba_api_key" type="text" placeholder="Enter your From Name" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
        <button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.saveTPAWABAAPIKey(UA_APP_UTILITY.reloadWindow)">Save</button>
    </div>`, showCloseOption, false, 500);
        $('#inp_tpa_waba_api_key').focus();
    },
    
    showReAuthorizeERRORWithClose: function(){
        UA_TPA_FEATURES.showReAuthorizeERROR();
    },
    
    saveTPAAPIKey: async function(callback){
        
        let wcuserId = $("#inp_tpa_user_id").val();
        if(!valueExists(wcuserId)){
            showErroMessage("Please Enter User ID");
            return;
        }
        
        let authtoken = $("#inp_tpa_api_key").val();
        if(!valueExists(authtoken)){
            showErroMessage("Please Enter API Key");
            return;
        }

        let isTpaAccountAutherized = await UA_TPA_FEATURES.isTpaAccountAutherized(extensionName, `https://us-central1-app-whatcetra.cloudfunctions.net/getBalance`, "POST", {'_apikey': authtoken, '_userId': wcuserId});
        
        if(isTpaAccountAutherized) {
            await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', '__multiple_keys__', {'authtoken' : authtoken, 'wcuserId': wcuserId}, callback);
        }
        else {
            showErroMessage("<b>Please provide Authorization</b></br></br>API Key or User ID is Incorrect.");
            return;
        }

    },
    isTpaAccountAutherized: async function (extensionName, url, method, data) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = {"Content-Type": "application/json"};
        return await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, url: url, method: method, data: data, headers: headers, _ua_lic_adminUserID: UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID}).then(async (response) => {
            if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.data && typeof(response.data.data) == 'object' && response.data.data.data && typeof(response.data.data.data) == 'object' && String(response.data.data.data.balance)) {
                removeTopProgressBar(credAdProcess3);
                return true;
            }
            else {
                removeTopProgressBar(credAdProcess3);
                return false;
            }
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
    },
    saveTPAWABAAPIKey: async function(callback){

        let authkey = $("#inp_tpa_waba_api_key").val();
        if(!valueExists(authkey)){
            showErroMessage("Please fill Sender Id");
            return;
        }

        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', 'senderId', authkey, callback);

    },
    isTpaWaBaAccountAutherized: async function (extensionName, url, method, data, authid) {
        let authkey = $("#inp_tpa_waba_api_key").val();
        if(!valueExists(authkey)){
            showErroMessage("Please fill From Name");
            return;
        }

        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', 'senderId', authkey, callback);
    },
    
    sendBulkSMS : async function(numberMessageMapArray, callback, messageHistoryMap) {

        let successSentMsgCount = 0;
        
        for(let MsgMapArr=0; MsgMapArr < numberMessageMapArray.length; MsgMapArr++) {

            
            let msgUrl = 'https://us-central1-app-whatcetra.cloudfunctions.net/sendWAMessage';
            let msgText = numberMessageMapArray[MsgMapArr].text;
            let msgNumber = numberMessageMapArray[MsgMapArr].number.replace(/\D/g,'');
            let msgMediaUrl = numberMessageMapArray[MsgMapArr].mediaUrl;
            let msgChannel = numberMessageMapArray[MsgMapArr].channel;
            let messagePayload = { 
                                    "text": msgText, 
                                    "contactNumber": msgNumber 
                                };
            if(msgMediaUrl) {
                messagePayload.mediaUrl = msgMediaUrl;
            }
            let successSentMsgRes = await UA_TPA_FEATURES.getAPIResponse(extensionName, msgUrl, "POST", messagePayload);
            if(successSentMsgRes && typeof(successSentMsgRes) == 'object' && successSentMsgRes.data && typeof(successSentMsgRes.data) == 'object' && successSentMsgRes.data.id && successSentMsgRes.data.status && successSentMsgRes.data.status === "queued") {
                successSentMsgCount++;                
                messageHistoryMap[numberMessageMapArray[MsgMapArr].clientuid].status = "queued";
                $(`#msg-resp-item-${parseInt(numberMessageMapArray[MsgMapArr].number)} .mri-status`).text(`Sent`).css({'color': 'green'});
                messageHistoryMap[numberMessageMapArray[MsgMapArr].clientuid].id = successSentMsgRes.data.id+"";
            }
            else {
                $(`#msg-resp-item-${parseInt(numberMessageMapArray[MsgMapArr].number)} .mri-status`).text(`Failed`).css({'color': 'crimson'});
                messageHistoryMap[numberMessageMapArray[MsgMapArr].clientuid].id = "";
            }

            $('#totMsgDisp').text(`Sending messages ${MsgMapArr+1}/${numberMessageMapArray.length}...`);
            if(MsgMapArr == numberMessageMapArray.length-1) {
                if(extensionName.endsWith('desk')) {
                    await UA_SAAS_SERVICE_APP.addSentSMSAsRecordInHistory(messageHistoryMap);
                }
                await callback(successSentMsgRes, successSentMsgCount);
            }

        }
        
    },
    
    _proceedToSendSMSAndExecuteCallback: async function(messagePayload, callback) {
        if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        var sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://api.rmlconnect.net/bulksms/bulksms?username=${credential.apikey}&password=${credential.senderId}&type=${msgType}&dlr=2&destination=${requestMap.to}&source=${credential.extraId}&message=${msgText}`, "POST", messagePayload);
        return sentAPIResponse;
    },
    
    _proceedToSendWAAndExecuteCallback: async function(messagePayload, callback){
        if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        var sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://api.pinbot.ai/v1/wamessage/sendMessage", "POST", messagePayload);
        callback(sentAPIResponse);
    },
    
    getAPIResponse: async function (extensionName, url, method, data) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = {"Content-Type": "application/json"};
        await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, TPAService: "whatcetra",url: url, method: method, data: data, headers: headers, _ua_lic_adminUserID: UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID}).then((response) => {
            response = response.data;
            returnResponse = response;
            removeTopProgressBar(credAdProcess3);
            return true;
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
        return returnResponse;
    },

    getCurrentAccountInfo: async function() {
        var headers = {"Content-Type": "application/json"};
        return await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, TPAService: "whatcetra",url: '--checkbalance', method: 'get', data: {}, headers: headers, _ua_lic_adminUserID: UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID}).then(async (response) => {
            if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.status && response.data.status == 'success' && response.data.ulgebraAppConfiguration) {
                return response.data.ulgebraAppConfiguration;
            }
            else {
                return false;
            }
        }).catch(err => {
            console.log(err);
            return false;
        });
    },    
    tpaCredentials: false,
    getCurrentAccountOrBalanceInfo: async function(callback) {

        await UA_TPA_FEATURES.checkVersion(callback);
        
    },
    extensionVersion: null,
    extensionVersionId: 'bjnjgehlejjebdcpfcdickaaiignnijd',
    extensionVersionIdforHubspot: 'oaioeafiobhcpckkockfndiekjkdklji',
    checkVersion: async function(callback) {
        if(window.chrome && chrome.runtime && chrome.runtime.sendMessage){
            return await chrome.runtime.sendMessage(UA_TPA_FEATURES.extensionVersionId, { "message": "version" },function (reply){
                console.log(reply);
                UA_TPA_FEATURES.extensionVersion = reply;
                if(UA_TPA_FEATURES.extensionVersion){
                    $('#suo-item-workflow-chrome').find('.ac_name_id').html(`<b>Version : </b>${UA_TPA_FEATURES.extensionVersion.version}`);
                    UA_TPA_FEATURES.fetchLICDetails(callback);
                    return true;
                }
                else{
                    if(!localStorage.getItem('chromExtensionPopupHideCheck') || localStorage.getItem('chromExtensionPopupHideCheck') !== 'true') {
                       UA_TPA_FEATURES.showReAuthorizeERRORforWhatcetra();
                    }
                    return false;
                }
            }); 
        }else{
            if(!localStorage.getItem('chromExtensionPopupHideCheck') || localStorage.getItem('chromExtensionPopupHideCheck') !== 'true') {
               UA_TPA_FEATURES.showReAuthorizeERRORforWhatcetra();
            }
        }       
    },
    fetchLICDetails: async function(callback) {

        let queryParams = new URLSearchParams(window.location.search);
        if(extensionName.indexOf('hubspotcrm') > -1) {

            $('#suo-item-workflow-chrome').after(`<div id="suo-item-workflow-hubspot"   class="suo-item suo-item-ac-labels" onclick="UA_TPA_FEATURES.initiateAuth('hubspot')">
                                        <span class="material-icons">get_app</span> Re-Install ${UA_TPA_FEATURES.tpaSortName} for Hubspot <span class="anl_servicename"></span><div class="ac_name_id">Click to configure</span>
                                    </div></div>`);

            if(window.chrome && chrome.runtime && chrome.runtime.sendMessage){
                await chrome.runtime.sendMessage(UA_TPA_FEATURES.extensionVersionIdforHubspot, { "message": "version" },function (reply){
                    console.log(reply);
                    if(reply) {
                        $('#suo-item-workflow-hubspot').find('.ac_name_id').html(`<b>Version : </b>${reply.version}`);
                    }
                    else {
                        if(!localStorage.getItem('chromExtensionPopupHideCheck') || localStorage.getItem('chromExtensionPopupHideCheck') !== 'true') {
                           UA_TPA_FEATURES.showReAuthorizeERRORforHubspot();
                        }
                        return false;
                    }
                }); 
            }
            else {
                if(!localStorage.getItem('chromExtensionPopupHideCheck') || localStorage.getItem('chromExtensionPopupHideCheck') !== 'true') {
                    UA_TPA_FEATURES.showReAuthorizeERRORforWhatcetra();
                 }
            }

        }

        UA_TPA_FEATURES.tpaCredentials = await UA_TPA_FEATURES.getCurrentAccountInfo();

        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);

        if(!UA_TPA_FEATURES.tpaCredentials) {
            UA_TPA_FEATURES.showReAuthorizeERROR();
            removeTopProgressBar(credAdProcess3);
            return false;
        }
        else {

            console.log(UA_TPA_FEATURES.tpaCredentials);         
            var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://us-central1-app-whatcetra.cloudfunctions.net/getBalance", "POST", {});
            if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.data && typeof(response.data.data) == 'object' && String(response.data.data.balance)) {
                callback(response.data.data);
                removeTopProgressBar(credAdProcess3);
                return true;
            }
            else {
                UA_TPA_FEATURES.showReAuthorizeERROR();
                removeTopProgressBar(credAdProcess3);
                return false;
            }            
            return true;

        }
    
    },
    showReAuthorizeERRORforHubspot: function(showCloseOption = false) {
        showErroWindow("Install Google Chrome Extension!", `Our Google Chrome extension is needed to Send Messages in <br>Background and to Send Bulk WhatsApp Messages. <br><br>You need to purchase <a target="_blank" style="font-weight: 900; color: #1557af;" href="https://apps.ulgebra.com/zoho/crm/whatsapp-web/pricing?src=crmwabulkpop">one of our plans to send Bulk Messages.</a> <br><br> 
                        <div style="display: inline-block;">
                            <div style="display: inline-block;box-sizing: border-box;padding-right: 10px;">
                                <button id="UA_TPA_AUTH_BTN" class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.initiateAuth('hubspot')">Install Google Chrome Extension</button>
                            </div>
                            <div style="display: inline-block;">
                                <button id="UA_TPA_AUTH_BTN" class="ua_service_login_btn ua_primary_action_btn" onclick="removeIDElem('#error-window-1000')" style="background-color: #6e6e6e;">Cancel</button>
                            </div>
                        </div>
                        <div style=" position: relative; top: 20px; width: max-content; "> <script>if(localStorage.getItem('chromExtensionPopupHideCheck') === 'true') {$('#chromExtensionPopupHideCheck').prop('checked', true);}</script> <label style=" cursor: pointer; " onclick="localStorage.setItem('chromExtensionPopupHideCheck', $('#chromExtensionPopupHideCheck').is(':checked'))"><input type="checkbox" id="chromExtensionPopupHideCheck"><span style=" box-sizing: border-box; padding-left: 5px; text-shadow: 0px 0px 1px; ">Don't show again.</span></label> </div>
                        `, false, showCloseOption, 1000);
    },
    showReAuthorizeERRORforWhatcetra: function(showCloseOption = false) {
        showErroWindow("Install Google Chrome Extension!", `Our Google Chrome extension is needed to Send Messages in <br>Background and to Send Bulk WhatsApp Messages. <br><br>You need to purchase <a target="_blank" style="font-weight: 900; color: #1557af;" href="https://apps.ulgebra.com/zoho/crm/whatsapp-web/pricing?src=crmwabulkpop">one of our plans to send Bulk Messages.</a> <br><br> 
                        <div style="display: inline-block;">
                            <div style="display: inline-block;box-sizing: border-box;padding-right: 10px;">
                                <button id="UA_TPA_AUTH_BTN" class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.initiateAuth()">Install Google Chrome Extension</button>
                            </div>
                            <div style="display: inline-block;">
                                <button id="UA_TPA_AUTH_BTN" class="ua_service_login_btn ua_primary_action_btn" onclick="removeIDElem('#error-window-1000')" style="background-color: #6e6e6e;">Cancel</button>
                            </div>
                        </div>
                        <div style=" position: relative; top: 20px; width: max-content; "> <script>if(localStorage.getItem('chromExtensionPopupHideCheck') === 'true') {$('#chromExtensionPopupHideCheck').prop('checked', true);}</script> <label style=" cursor: pointer; " onclick="localStorage.setItem('chromExtensionPopupHideCheck', $('#chromExtensionPopupHideCheck').is(':checked'))"><input type="checkbox" id="chromExtensionPopupHideCheck"><span style=" box-sizing: border-box; padding-left: 5px; text-shadow: 0px 0px 1px; ">Don't show again.</span></label> </div>
                        `, false, showCloseOption, 1000);
    },
    initiateAuth: function (saas) {
        if(!extensionName){
            showErroWindow('Application error!', 'Unable to load app type. Kindly <a target="_blank" href="https://apps.ulgebra.com/contact">contact developer</a>');
            return;
        }
        if(saas && saas === 'hubspot') {
            window.open(`https://chrome.google.com/webstore/detail/wa-web-for-zoho-crm-bulk/${UA_TPA_FEATURES.extensionVersionIdforHubspot}`, 'Authorize', 'popup');
        }
        else {
            window.open(`https://chrome.google.com/webstore/detail/wa-web-for-zoho-crm-bulk/${UA_TPA_FEATURES.extensionVersionId}`, 'Authorize', 'popup');
        }
    },
    getSafeString: function(rawStr) {
        if (!rawStr || rawStr+"".trim() === "") {
            return "";
        }

        return $('<textarea/>').text(rawStr).html();
    },
    actualPersonalCommonSettingResolved: function() {
        UA_TPA_FEATURES.addNewWebhook();
        UA_APP_UTILITY.insertIncomingWD_supportedModules();
        UA_TPA_FEATURES.insertIncomingWebhookDialog();
    },
    incomingCaptureStatusChanged: function(){
        if(UA_TPA_FEATURES.EXISTING_WEBHOOK_ID){
            document.getElementById('tpa-switch-incoming-enable-capture').checked = true;
            $('.tpaichan-devider').fadeIn();
        }
        else{
            document.getElementById('tpa-switch-incoming-enable-capture').checked = false;
            $('.tpaichan-devider:not(#master-tpai-incom-chan-item)').fadeOut();
        }
    },
    deleteExistingWebhook: async function(){        
        UA_TPA_FEATURES.EXISTING_WEBHOOK_ID = null;
        UA_TPA_FEATURES.incomingCaptureStatusChanged();
        return true;
    },
    showIncomingWebhookDialog: function(){
        $("#error-window-tpa_service_senders").show();
        $("#incoming-channel-config-item").parent().remove();
    },
    addNewWebhook: async function(){
        
        if(!UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS){
            UA_APP_UTILITY.CALL_AFTER_PERSONAL_COMMON_SETTING_RESOLVED.push(UA_TPA_FEATURES.addNewWebhook);
            return false;
        }
        if(UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.hasOwnProperty('ua_incoming_enable_capture_'+currentUser.uid) && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS['ua_incoming_enable_capture_'+currentUser.uid] === false){
            return;
        }
        
        UA_TPA_FEATURES.EXISTING_WEBHOOK_ID = true;
        UA_TPA_FEATURES.incomingCaptureStatusChanged();
        
    },
    EXISTING_WEBHOOK_ID: null,
    isWebhookAlreadyExists: async function(){
        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://us-central1-app-whatcetra.cloudfunctions.net/integrationsActions", "POST", { "action": "getIntegration" });
        console.log(response);
        if(response && response.data && response.data.data && response.data.data.length){
            let existingServiceID = null;
            response.data.data.forEach((item)=>{
                try{
                    let url = item.webhookUrl;
                    if(url && url.startsWith(UA_APP_UTILITY.getWebhookBaseURL())){
                        existingServiceID = true;
                    }
                }
                catch(ex){console.log(ex)}
            });
            UA_TPA_FEATURES.EXISTING_WEBHOOK_ID = existingServiceID;
            return existingServiceID !== null;
        }
        
    },
    insertIncomingWebhookDialog: async function() {
        
        var webhookExists = await UA_TPA_FEATURES.isWebhookAlreadyExists();
        if(!webhookExists){

            let webhookAddress = UA_APP_UTILITY.getAuthorizedWebhookURL();
            let webhookData = { "action": "addIntegration", "webhook_url": webhookAddress };
            var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://us-central1-app-whatcetra.cloudfunctions.net/integrationsActions", "POST", webhookData);
            console.log(response);         
        }
        return true;

    },
    ulgloaderDiv : async function(ulgLoaderParent, ulgLoaderSize) {

        $(ulgLoaderParent).append(`<div class="loaderOuterDiv"><div class="wcMsgLoadingInner" title="loading…"><svg class="wcMsgLoadingSVG" width="calc(${ulgLoaderSize}/2)" height="calc(${ulgLoaderSize}/2)" viewBox="0 0 46 46" role="status"><circle class="wcMsgLoadingSvgCircle" cx="23" cy="23" r="20" fill="none" stroke-width="6" style="stroke: rgb(37 162 94);"></circle></svg></div></div>`);

        let ulg_loader_popup_css = `/* message loading style */

        .wcMsgLoadingInner {
            margin: 0 auto;
            background-color: #fff;
            border-radius: 50%;
            box-shadow: 0 1px 1px 0 rgba(0,0,0,.06),0 2px 5px 0 rgba(0,0,0,.2);
            display: flex;
            align-items: center;
            justify-content: center;
            width: ${ulgLoaderSize};
            height: ${ulgLoaderSize};
            color: rgba(0,0,0,0.25);
        }

        .wcMsgLoadingSvgCircle {
            stroke: #ccc;
            stroke-dasharray: 1,150;
            stroke-dashoffset: 0;
            stroke-linecap: round;
            animation: wcMsgLoadingSvgCircle 1.5s ease-in-out infinite;
        }

        .wcMsgLoadingSVG {
            animation: wcMsgLoadingSVG 2s linear infinite;
        }

        /* message loading style */

        @keyframes wcMsgLoadingSVG{
            to{transform:rotate(1turn)}
        }

        @keyframes wcMsgLoadingSvgCircle{
            0%{stroke-dasharray:1,150;stroke-dashoffset:0}
            50%{stroke-dasharray:90,150;stroke-dashoffset:-35}
            to{stroke-dasharray:90,150;stroke-dashoffset:-124}
        }`;

        $('<style>').attr('type', 'text/css').text(ulg_loader_popup_css).prependTo('.loaderOuterDiv');        

    }
    
};
