var UA_TPA_FEATURES = {
    workflowCode: {
        "SMS": {
            "to": "FILL_HERE",
            "from": "FILL_HERE",
            "body": "FILL_HERE",
            "channel": "SMS",
            "module": "FILL_HERE",
            "recordId": "FILL_HERE",
            "ulgebra_webhook_authtoken": null
        }
    },
    TPAServiceName: "Clicksend",
    chosenMessgeTemplate: null,
    'clientIds': {},
    APP_SAVED_CONFIG: null,
    currentSelectedRoute: "MKT",
    getSupportedChannels: function () {
        return [UA_APP_UTILITY.MESSAGING_CHANNELS.SMS];
    },
    renderInitialElements: async function () {
        $("#ac_name_label_tpa .anl_servicename").text(UA_TPA_FEATURES.TPAServiceName);
        UA_TPA_FEATURES.getCurrentAccountOrBalanceInfo(async function (response) {
            if (response?.status === 200) {
                $('#ac_name_label_tpa .ac_name_id').html(`Balance: ${response.data.data._currency.currency_prefix_d}${response.data.data.balance}`);
            }else{
                UA_TPA_FEATURES.showReAuthorizeERROR();
                return
            }
        }, 1);

        UA_APP_UTILITY.getPersonalWebhookAuthtoken();

        var countryCallCodeArray = [];
        UA_APP_UTILITY.countryCallingCodeArray.forEach(item => {
            countryCallCodeArray.push({
                'label': `${item.name} (${item.dial_code})`,
                'value': item.dial_code
            });
        });

        UA_APP_UTILITY.renderSelectableDropdown('#ssf-new-recip-countrycode', 'Select country', countryCallCodeArray, 'UA_APP_UTILITY.log');
        $("#messageAttachmentInputHolder").remove() //removing wapp image upload element
        UA_TPA_FEATURES.getSenders(function(response){
            if(response.data.data.total){
                response.data.data.forEach(item=>{
                    let number = item.dedicated_number.toString()
                    UA_APP_UTILITY.addMessaegSender(number,{id:number,label:"SMS"},number)
                })
            }
        })
    },
    renderWorkflowBodyCode: function (accessToken) {
        UA_TPA_FEATURES.getSupportedChannels().forEach(channel => {
            if (channel.id === "SMS") {
                UA_TPA_FEATURES.workflowCode.SMS.ulgebra_webhook_authtoken = accessToken.saas;
                UA_APP_UTILITY.addWorkflowBodyCode('sms', 'For Sending SMS', UA_TPA_FEATURES.workflowCode.SMS);
            }
            if (channel.id === "WHATSAPP") {
                UA_TPA_FEATURES.workflowCode.WHATSAPP.ulgebra_webhook_authtoken = accessToken.saas;
                UA_APP_UTILITY.addWorkflowBodyCode('whatsapp', 'For Sending Whatsapp', UA_TPA_FEATURES.workflowCode.WHATSAPP);
            }
        })
    },
    insertTemplateContentInMessageInput: function (templateId) {
        var curTemplateItem = UA_APP_UTILITY.fetchedTemplates[templateId];
        UA_TPA_FEATURES.chosenMessgeTemplate = curTemplateItem;
        $("#messageAttachmentInputHolder").hide();
        UA_APP_UTILITY.currentAttachedFile = null;
        $("#attachedfile").text('Attach file');
        $('#inputFileAttach').val('');
        if (curTemplateItem.templateType === "placeholder_template") {
            $("#primary-send-btn").text('Send WhatsApp').css('background-color', 'green');
            if (curTemplateItem.head_mediatype && curTemplateItem.head_mediatype !== "") {
                $("#attachedfile").text('Attach ' + curTemplateItem.head_mediatype);
                $("#messageAttachmentInputHolder").show();
            }
            //            if(curTemplateItem.head_media_url){
            //                $("#messageAttachmentInputHolder").show();
            //            }
            $("#ssf-fitem-template-placeholder-holder-listholder").html("");
            curTemplateItem.placeholders.forEach(item => {
                $("#ssf-fitem-template-placeholder-holder-listholder").append(`
                    <div class="ssf-temp-placehold-item">
                            <span class="ssf-temp-placehold-item-label">{{${item}}}</span> <span class="material-icons ssf-temp-placehold-item-icon">double_arrow</span> <input onblur="UA_APP_UTILITY.lastFocusedInputElemForPlaceholderInsert = 'ssf-templ-place-input-${item}'" id="ssf-templ-place-input-${item}" data-placeholder-id="${item}" class="ssf-temp-placehold-item-input" type="text" placeholder="Type or choose field from above">
                        </div>
                `);
            });
            if (curTemplateItem.placeholders.length > 0) {
                $("#ssf-fitem-template-placeholder-holder").show();
            } else {
                $("#ssf-fitem-template-placeholder-holder").hide();
            }
            $('#inp-ssf-main-message').attr('readonly', true);
        }
        else {
            $("#primary-send-btn").text('Send SMS').css('background-color', 'royalblue');
            $("#ssf-fitem-template-placeholder-holder").hide();
            $('#inp-ssf-main-message').removeAttr('readonly');
        }
        $('#inp-ssf-main-message').val(curTemplateItem.message).focus();
    },

    _getServiceTemplates: async function () {
        var templatesResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "/", "post");
        if (templatesResponse.code === 401 || templatesResponse.data.code === 401 || templatesResponse.data.message === "Authentication failed") {
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return;
        }
        return templatesResponse;
    },
    showReAuthorizeERROR: function (showCloseOption = false) {
        if ($('#inp_tpa_service_p_id').length > 0) {
            console.log('already showing reautherr');
            return;
        }
        showErroWindow("Authorization needed!", `You need to authorize your ${UA_TPA_FEATURES.TPAServiceName}  Account to proceed. <br><br> 
        <div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
            <div style="
                font-size: 24px;
                font-weight: bold;
                margin-top: 20px;
                margin-bottom: 15px;
            ">SMS Channel Configuration</div>
            <div style="margin-left:20px">
                <div style="font-size: 16px;margin-bottom: 5px;margin-top: 10px;color: rgb(80,80,80);">${UA_TPA_FEATURES.TPAServiceName} UserName</div>
                <div class="help_apikey_tip"><a target="_blank" href="https://dashboard.clicksend.com/account/subaccounts">Get Here</a></div>
                <input id="help_apiSecret_tip" type="text" placeholder="Enter your UserName" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
            </div>
            <div style="margin-left:20px">
                <div style="font-size: 16px;margin-bottom: 5px;margin-top: 10px;color: rgb(80,80,80);">${UA_TPA_FEATURES.TPAServiceName} API Key</div>
                <div class="help_apikey_tip"><a target="_blank" href="https://dashboard.clicksend.com/account/subaccounts">Get Here</a></div>
                <input id="inp_tpa_api_key" type="text" placeholder="Enter your API Key" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
            </div>
            <button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.saveTPAAPIKey(UA_APP_UTILITY.reloadWindow)">Authorize</button>
        </div>`, showCloseOption, false, 500);
        $('#inp_tpa_authkey').focus();
        UA_LIC_UTILITY.showExistingInvitedAdminDDHTML(true);
    },

    showReAuthorizeERRORWithClose: function () {
        UA_TPA_FEATURES.showReAuthorizeERROR();
    },

    saveTPAAPIKey: async function (callback) {
        let username = $("#help_apiSecret_tip").val();
        let api_key = $("#inp_tpa_api_key").val();
        if (!valueExists(username)) {
            showErroMessage("Please Enter Enter your UserName");
            $("#help_apiSecret_tip").focus();
            return;
        }
        if (!valueExists(api_key)) {
            showErroMessage("Please Enter API Key");
            $("#inp_tpa_api_key").focus();
            return;
        }
        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', '__multiple_keys__', {username,api_key}, callback);
    },

    prepareAndSendSMS: function () {
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        if (!UA_APP_UTILITY.currentSender) {
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var senderId = UA_APP_UTILITY.currentSender.value;
        if (!valueExists(senderId)) {
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var recipNumbers = UA_APP_UTILITY.getCurrentMessageRecipients();
        var messageText = $("#inp-ssf-main-message").val().trim();
        if (!valueExists(messageText)) {
            showErroWindow('Message is empty!', "Kindly fill message to proceed.");
            return false;
        }
        if (recipNumbers.length === 0) {
            showErroWindow('Recipient list is empty!', "Kindly add recipients to send SMS.");
            return false;
        }
        if (messageText.length >= 160) {
            showErroWindow('Message is too long!', "Since your message contains more than 160 characters, message may fail in clickatell. <br>  <br>Anyway we're attempting to send the message... ");
        }
        if (!UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE) {
            showErroWindow("<div id='sending-title' class='info'>Sending</div>", `<div id='sending-content'></div>`, false, true);
        }
        const sendedMsgStatus = []
        const resultColorCorrection = () => {
            $(".sent-pan").each(function () {
                $(this).css("border-color", $(this).find(".sent-icon").css("color"))
                $(this).css("box-shadow", "1px 1px 3px " + $(this).find(".sent-icon").css("color"))
            })
        }
        recipNumbers.forEach(function (item, i) {
            item.number = item.number.replace("+", "")
            const templateAppliedMsg = UA_APP_UTILITY.getTemplateAppliedMessage(messageText, UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id])
            $("#sending-content").append(`<div class="sent-pan" id="sending-pan-${i}">
                <div class="sent-to">${item.number}</div>
                <div class="sent-msg info"><span class="material-icons sent-icon">more_horiz</span></div>
            </div>`)
            resultColorCorrection()
            let messageHistoryMap = {
                'contact': {
                    'name': UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].Full_Name : item.number
                },
                'from': senderId,
                'to': item.number,
                'message': templateAppliedMsg,
                'status': 'NOT_SENT',
                'module': UA_SAAS_SERVICE_APP.widgetContext.module,
                'moduleId': item.id,
                'channel': UA_APP_UTILITY.currentSender.channel.label
            };
            let callback = function (apiResponse) {
                if (apiResponse.status === 200 && apiResponse.data.data.messages[0].status === "SUCCESS") {
                        sendedMsgStatus.push({
                            success: true
                        })
                        $(`#sending-pan-${i}`).find(".sent-msg").toggleClass("info success").html(`Sent successfully <span class='material-icons sent-icon' title="${apiResponse.data.data.messages[0].message_id || ""}">check_circle</span>`)
                        messageHistoryMap.status = "SENT";
                        UA_APP_UTILITY.resetMessageFormFields();                    
                } else {
                    sendedMsgStatus.push({
                        success: false
                    })
                    const errMsg = (apiResponse?.data?.data?.messages[0]?.status || "Something went wrong").replaceAll('"', '\"')
                    $(`#sending-pan-${i}`).find(".sent-msg").toggleClass("info danger").html(`Unable to send <span class='material-icons sent-icon' title="${errMsg}">error</span>`).slideDown()
                }
                resultColorCorrection()
                UA_SAAS_SERVICE_APP.addSentSMSAsRecordInHistory({ "message1": messageHistoryMap });

                const totalProcessedCount = recipNumbers.length
                const successCount = sendedMsgStatus.filter(status => status.success).length
                const failedCount = sendedMsgStatus.filter(status => !status.success).length
                $("#sending-title").text(`Sending messages ${recipNumbers.length}/${sendedMsgStatus.length}...`)
                if (failedCount === totalProcessedCount) {
                    $("#sending-title").text("Unable to send messages.").toggleClass("info danger")
                }
                if (successCount === totalProcessedCount) {
                    $("#sending-title").text("Sent successfully").toggleClass("info success")
                }
                if (successCount && failedCount) {
                    $("#sending-title").text(`All messages have been processed. total: ${totalProcessedCount}, sent: ${successCount}, failed: ${failedCount}`)
                }
                return apiResponse
            }
            UA_TPA_FEATURES.sendSMS(senderId, item.number, templateAppliedMsg, callback)
        })


    },
    sendSMS: function (sender, recipNumber, messageText, callback) {
        messagePayload = {
            "to": recipNumber,
            "from": sender,
            "body": messageText
        }
        if (UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE) {
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        UA_TPA_FEATURES._proceedToSendSMSAndExecuteCallback(messagePayload, callback);
    },

    _proceedToSendSMSAndExecuteCallback: async function (messagePayload, callback) {
        var sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://rest.clicksend.com/v3/sms/send", "POST", messagePayload);
        callback(sentAPIResponse);
    },

    getAPIResponse: async function (extensionName, url, method, data) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = { "Content-Type": "application/json" };
        let TPAService = UA_TPA_FEATURES.TPAServiceName.toLocaleLowerCase()
        await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({ appCode: extensionName, TPAService, url: url, method: method, data: data, headers: headers, _ua_lic_adminUserID: UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID }).then((response) => {
            response = response.data;
            returnResponse = response;
            removeTopProgressBar(credAdProcess3);
            return true;
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
        return returnResponse;
    },

    getCurrentAccountOrBalanceInfo: async function (callback) {
        const promotionalBalance = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://rest.clicksend.com/v3/account", "GET", {});
        callback(promotionalBalance);
    },
    getSenders: async function (callback) {
        let sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://rest.clicksend.com/v3/numbers?q=type:sms", "GET", {});
        callback(sentAPIResponse);
    },
};

