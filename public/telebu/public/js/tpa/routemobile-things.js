var UA_TPA_FEATURES = {
    chosenMessgeTemplate : null,
    'clientIds': {
        'routemobileforhubspotcrm' : "19a87901-c92c-4901-9b96-d7775db63b59"
    },
    tpaSortName: 'Route Mobile',
    getSupportedChannels: function(){
        return [ UA_APP_UTILITY.MESSAGING_CHANNELS.SMS ];
    },
    workflowCode : {
        "SMS": {
            "senderId": "FILL_HERE",
            "to": "FILL_HERE",
            "text": "FILL_HERE",
            "contentType": "FILL_HERE",
            "module": "FILL_HERE",
            "recordId": "FILL_HERE",
            "channel": "SMS",
            "ulgebra_webhook_authtoken": null
        }
    },
    renderInitialElements: async function(){

        $('#ac_name_label_tpa').after(`<div id="ac_name_label_saas" class="suo-item suo-item-ac-labels" onclick="UA_TPA_FEATURES.openWABAAPIKey()">
                                        <span class="material-icons">restart_alt</span> Update ${UA_TPA_FEATURES.tpaSortName} Source
                                        <div class="ac_wa_name_id" style="display: none; font-size: 12px; text-align: right; display: block; color: grey; "></div>
                                    </div>`);
        
        $("#ac_name_label_tpa .anl_servicename").text(UA_TPA_FEATURES.tpaSortName);
        UA_TPA_FEATURES.getCurrentAccountOrBalanceInfo(async function(resp) {
            
            $('#ac_name_label_tpa .ac_name_id').text(`Balance : ${resp.replace('BALANCE:', '')}`);
            
            var sendersArray = [];
            if(UA_TPA_FEATURES.tpaCredentials.senderId) {

                $('.ac_wa_name_id').text('Sender : '+UA_TPA_FEATURES.tpaCredentials.senderId).show();

                sendersArray.push({
                    'label': UA_TPA_FEATURES.tpaCredentials.senderId,
                    'value': UA_TPA_FEATURES.tpaCredentials.senderId
                });
                UA_APP_UTILITY.addMessaegSender(UA_TPA_FEATURES.tpaCredentials.senderId, UA_APP_UTILITY.MESSAGING_CHANNELS.SMS, UA_TPA_FEATURES.tpaCredentials.senderId);
        
            }
            else {
                UA_TPA_FEATURES.openWABAAPIKey();
            }
            UA_APP_UTILITY.TPA_SENDERS_FETCH_COMPLETED();
            //$('#DD_HOLDER_SENDER_LIST .dropdownInner div').first().click();
            
            $('#DD_HOLDER_SENDER_LIST').after(`<div class="ssf-fitem-ans" id="DD_HOLDER_MsgType_LIST"><div class="ssf-fitem-label">Language</div></div>`)
            UA_APP_UTILITY.renderSelectableDropdown('#DD_HOLDER_MsgType_LIST', 'Select a Language', [{'label': 'English', 'value': '0'}, {'label': 'Unicode', 'value': '2'}], 'UA_APP_UTILITY.log', false);
            $('#DD_HOLDER_MsgType_LIST .dropdownInner div').first().click();

            var fetchedTemplatesResponse = await UA_TPA_FEATURES._getServiceTemplates();
            if(fetchedTemplatesResponse) {
                console.log('fetchedtemoktes ', fetchedTemplatesResponse);
                fetchedTemplatesCollection = fetchedTemplatesResponse;

                UA_APP_UTILITY._feature_showUAMsgTemplates = false;
                
                var templatesArray = [];
                fetchedTemplatesCollection.forEach((item, key)=>{
                    UA_APP_UTILITY.fetchedTemplates[key+''] = {
                        'message': UA_TPA_FEATURES.getSafeString(item.messagetext)
                    };
                    templatesArray.push({
                        'label': UA_TPA_FEATURES.getSafeString(item.messagetext).replaceAll(' ',"&nbsp;").replaceAll(/[\r\n]+/g,"&nbsp;").replaceAll('"', '&#8243;').replaceAll("'", '&#8242;'),
                        'value': key+''
                    });
                });
                
                var approvedTemplateDDId = UA_APP_UTILITY.renderSelectableDropdown('#ssf-fitem-template-var-holder', `Insert ${UA_TPA_FEATURES.tpaSortName} template`, templatesArray, 'UA_TPA_FEATURES.insertTemplateContentInMessageInput', false, false);
            }

        UA_APP_UTILITY.getPersonalWebhookAuthtoken();
       
        var countryCallCodeArray = [];
        UA_APP_UTILITY.countryCallingCodeArray.forEach(item=>{
            countryCallCodeArray.push({
                'label': `${item.name} (${item.dial_code})`,
                'value': item.dial_code
            });
        });
        
        UA_APP_UTILITY.renderSelectableDropdown('#ssf-new-recip-countrycode', 'Select country', countryCallCodeArray, 'UA_APP_UTILITY.log');
   
        
        });
        
    },
    
    renderWorkflowBodyCode :function(accessToken){
        
        UA_TPA_FEATURES.workflowCode.SMS.ulgebra_webhook_authtoken = accessToken.saas;
        //UA_TPA_FEATURES.workflowCode.WhatsApp.ulgebra_webhook_authtoken = accessToken.saas;
        
        UA_APP_UTILITY.addWorkflowBodyCode('sms', 'For Sending SMS', UA_TPA_FEATURES.workflowCode.SMS);
        
        //UA_APP_UTILITY.addWorkflowBodyCode('whatsapp', 'For Sending WhatsApp', UA_TPA_FEATURES.workflowCode.WhatsApp);
    },
    
    
    
    prepareAndSendSMS: function(){
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        if(UA_TPA_FEATURES.chosenMessgeTemplate && UA_TPA_FEATURES.chosenMessgeTemplate.templateType === "placeholder_template"){
            return UA_TPA_FEATURES.prepareAndSendBulkWhatsApp();
        }
        if(!UA_APP_UTILITY.currentSender){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var senderId = UA_APP_UTILITY.currentSender.value;
        if(!valueExists(senderId)){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }

        var msgType = $('#DD_HOLDER_MsgType_LIST .dropdownOuter').attr('data-selectedval');
        if(!valueExists(msgType)){
            showErroWindow('Message Content Type is empty!', "Kindly choose Message Language to proceed.");
            return false;
        }

        var recipNumbers = UA_APP_UTILITY.getCurrentMessageRecipients();
        
        if(recipNumbers.length === 0){
            showErroWindow('Recipient list is empty!', "Kindly add recipients to send SMS.");
            return false;
        }

        var messageText = $("#inp-ssf-main-message").val().trim();

        if(!valueExists(messageText)){
            showErroWindow('Message is empty!', "Kindly fill message to proceed.");
            return false;
        }

        $('#sms-prog-window').show();
        $('#msgItemsProgHolder').html('');
        $('#totMsgDisp').text(`Sending messages 0/${recipNumbers.length}...`);

        var messageNumberMapArray = [];
        var messageHistoryMap = {};
        recipNumbers.forEach(item=>{
            let resolvedMessageText = UA_APP_UTILITY.getTemplateAppliedMessage(messageText, UA_APP_UTILITY.storedRecipientInventory[item.id]);
            var historyUID = item.id+''+new Date().getTime()+''+Math.round(Math.random(100000,99999)*1000000);
            messageNumberMapArray.push({
                'number': item.number,
                'text': resolvedMessageText,
                'clientuid': historyUID
            });
            messageHistoryMap[historyUID] = {
                'contact':{
                    'name': UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].Full_Name : item.number
                },
                'from': senderId,
                'to': item.number,
                'message': resolvedMessageText,
                'status': 'NOT_SENT',
                'module': UA_SAAS_SERVICE_APP.widgetContext.module,
                'moduleId': item.id,
                'channel': 'SMS'
            };

            $('#msgItemsProgHolder').append(`
                <div id="msg-resp-item-${parseInt(item.number)}" class="msgRespItem" title="${getSafeString(resolvedMessageText)}">
                  <div class="mri-label"><b>${$('.msgRespItem').length+1}</b>. SMS to ${UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].name+'-' : ''} ${item.number}</div>
                  <div class="mri-status">Sending...</div>
                </div>
            `);

        });
    
        
        UA_TPA_FEATURES.sendBulkSMS(senderId, msgType, messageNumberMapArray, (function(successSentMsgRes, successSentMsgCount){
            if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
                   return false;
               }
            if(successSentMsgRes && typeof(successSentMsgRes) == 'object' && successSentMsgRes.data && typeof(successSentMsgRes.data) == 'string') {
                $('#totMsgDisp').text(`All messages have been processed. TOTAL: ${messageNumberMapArray.length}, SENT: ${successSentMsgCount}, FAILED: ${messageNumberMapArray.length-successSentMsgCount}`);
                UA_APP_UTILITY.resetMessageFormFields(); 
            }
            else{
                //showErroWindow('Unable to send message', '');
                return;              
            }
        }), messageHistoryMap);
        
    },
    
    
    
    
    
    insertTemplateContentInMessageInput :function(templateId){
        var curTemplateItem = UA_APP_UTILITY.fetchedTemplates[templateId];
        UA_TPA_FEATURES.chosenMessgeTemplate = curTemplateItem;
        $("#messageAttachmentInputHolder").hide();
        UA_APP_UTILITY.currentAttachedFile = null;
        $("#attachedfile").text('Attach file');
        $('#inputFileAttach').val('');
        if(curTemplateItem.templateType === "placeholder_template"){
            $("#primary-send-btn").text('Send WhatsApp').css('background-color', 'green');
            if(curTemplateItem.head_mediatype){
                switch (curTemplateItem.head_mediatype) {
                    case "0":
                        $("#attachedfile").text('Attach document');
                        break;
                    case "1":
                        $("#attachedfile").text('Attach image');
                        break;
                    case "2":
                        $("#attachedfile").text('Attach video');
                        break;
                    default:
                        $("#attachedfile").text('Attach file');
                        break;
                }
            }
            if(curTemplateItem.head_media_url){
                $("#messageAttachmentInputHolder").show();
            }
            $("#ssf-fitem-template-placeholder-holder-listholder").html("");
            curTemplateItem.placeholders.forEach(item=>{
                $("#ssf-fitem-template-placeholder-holder-listholder").append(`
                    <div class="ssf-temp-placehold-item">
                            <span class="ssf-temp-placehold-item-label">{{${item}}}</span> <span class="material-icons ssf-temp-placehold-item-icon">double_arrow</span> <input onblur="UA_APP_UTILITY.lastFocusedInputElemForPlaceholderInsert = 'ssf-templ-place-input-${item}'" id="ssf-templ-place-input-${item}" data-placeholder-id="${item}" class="ssf-temp-placehold-item-input" type="text" placeholder="Type or choose field from above">
                        </div>
                `);
            });
            if(curTemplateItem.placeholders.length > 0){
                $("#ssf-fitem-template-placeholder-holder").show();
            }else{
                $("#ssf-fitem-template-placeholder-holder").hide();
            }
            $('#inp-ssf-main-message').attr('readonly', true);
        }
        else{
            $("#primary-send-btn").text('Send SMS').css('background-color', 'royalblue');
            $("#ssf-fitem-template-placeholder-holder").hide();
            $('#inp-ssf-main-message').removeAttr('readonly');
        }
        $('#inp-ssf-main-message').val(curTemplateItem.message).focus();
    },
    
    _getServiceSenders: async function(){

        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://restapi.smscountry.com/v0.1/Accounts/"+UA_TPA_FEATURES.tpaCredentials.authId+"/SenderIDs", "GET");
        if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.Success && response.data.Success == 'True' && response.data.SenderIds) {
            return  response;
        }
        else {
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return false;
        }
        
    },
    
    _getServiceTemplates: async function() {

        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://crm.rmlconnect.net/smsapis/v1/userauth", "GET");
        if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.data && typeof(response.data.data) == 'object' && response.data.data.usertype) {
            if(response.data.data.usertype != "live") {
                var tempResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://crm.rmlconnect.net/smsapis/v1/smstemplates", "GET");
                if(tempResponse && typeof(tempResponse) == 'object' && tempResponse.data && typeof(tempResponse.data) == 'object' && tempResponse.data.data && typeof(tempResponse.data.data) == 'object' && tempResponse.data.data.templates && typeof(tempResponse.data.data.templates) == 'object' && tempResponse.data.data.templates.templates && typeof(tempResponse.data.data.templates.templates) == 'object') {
                    return tempResponse.data.data.templates.templates;
                }
                else {
                    UA_TPA_FEATURES.showReAuthorizeERROR();
                    return false;
                }
            }
            else {
                return false;
            }
        }
        else {
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return false;
        }
        
    },
    
    _getServiceWATemplates: async function(silenceAuthError = false){
        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://restapi.smscountry.com/v0.1/Accounts/"+UA_TPA_FEATURES.tpaCredentials.authId+"/WhatsappTemplate/"+UA_TPA_FEATURES.tpaCredentials.ChannelUUID, "GET");
        if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.Message && response.data.Message == 'Success.' && response.data.Table) {
            return response;
        }
        else {
            if(silenceAuthError){
                return {
                    data:{
                        data: []
                    }
                };
            }
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return;
        }
    },
    
    showReAuthorizeERROR: function(showCloseOption = false){
        if($('#inp_tpa_api_key').length > 0){
            console.log('already showing reautherr');
            return;
        }
        showErroWindow("Authorization needed!", `You need to authorize your ${UA_TPA_FEATURES.tpaSortName} Account to proceed. <br><br> <div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
        <div style="font-size: 18px;margin-bottom: 5px;margin-top: 20px;">${UA_TPA_FEATURES.tpaSortName} Username</div>
        <div class="help_apikey_tip"><a target="_blank" href="https://client.rmlconnect.net/userprofile/viewuserprofile">Get Here</a></div>
        <input id="inp_tpa_api_key" type="text" placeholder="Enter your Username" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
        
        <div style="font-size: 18px;margin-bottom: 5px;margin-top: 20px;">${UA_TPA_FEATURES.tpaSortName} Password</div>
        <div class="help_apikey_tip"><a target="_blank" href="https://client.rmlconnect.net/userprofile/viewuserprofile">Get Here</a></div>
        <input id="inp_tpa_api_token" type="text" placeholder="Enter your Password" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
        <button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.saveTPAAPIKey(UA_APP_UTILITY.reloadWindow)">Authorize</button>
    </div>`, showCloseOption, false, 500);
        $('#inp_tpa_api_key').focus();
        UA_LIC_UTILITY.showExistingInvitedAdminDDHTML(true);
    },
    
    openWABAAPIKey: function(showCloseOption = false){
        showErroWindow("Source Authentication needed!", `<div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
        <div style="font-size: 18px;margin-bottom: 5px;margin-top: 20px;"> ${UA_TPA_FEATURES.tpaSortName} Sender</div>
        <div class="help_apikey_tip" style="cursor: default;"><a>Contact : </b>alliances@routemobile.com</a></div>
        <input id="inp_tpa_waba_api_key" type="text" placeholder="Enter your Sender Id" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
        <button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.saveTPAWABAAPIKey(UA_APP_UTILITY.reloadWindow)">Save</button>
    </div>`, showCloseOption, false, 500);
        $('#inp_tpa_waba_api_key').focus();
    },
    
    showReAuthorizeERRORWithClose: function(){
        UA_TPA_FEATURES.showReAuthorizeERROR();
    },
    
    saveTPAAPIKey: async function(callback){
        let authkey = $("#inp_tpa_api_key").val();
        if(!valueExists(authkey)){
            showErroMessage("Please Enter Username");
            return;
        }
        let authtoken = $("#inp_tpa_api_token").val();
        if(!valueExists(authtoken)){
            showErroMessage("Please Enter Password");
            return;
        }

        let isTpaAccountAutherized = await UA_TPA_FEATURES.isTpaAccountAutherized(extensionName, `https://api.rmlconnect.net/CreditCheck/checkcredits?username=${authkey}&password=${authtoken}`, "GET", {}, authkey, authtoken);
        
        if(isTpaAccountAutherized) {
            await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', '__multiple_keys__', {'username' : authkey, 'authtoken': authtoken}, callback);
        }
        else {
            showErroMessage("<b>Please provide Authorization</b></br></br>Username or Password is Incorrect.");
            return;
        }

    },
    isTpaAccountAutherized: async function (extensionName, url, method, data, authid, authtoken) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = {"Content-Type": "application/json"};
        return await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, url: url, method: method, data: data, headers: headers}).then(async (response) => {
            if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.data && (response.data.data.indexOf('BALANCE') > -1 || response.data.data == "PERMISSION DENIED")) {
                removeTopProgressBar(credAdProcess3);
                return true;
            }
            else {
                removeTopProgressBar(credAdProcess3);
                return false;
            }
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
    },
    saveTPAWABAAPIKey: async function(callback){

        let authkey = $("#inp_tpa_waba_api_key").val();
        if(!valueExists(authkey)){
            showErroMessage("Please fill Sender Id");
            return;
        }

        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', 'senderId', authkey, callback);

    },
    isTpaWaBaAccountAutherized: async function (extensionName, url, method, data, authid) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = {"Content-Type": "application/json"};
        return await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, TPAService: "routemobile",url: url, method: method, data: data, headers: headers}).then(async (response) => {
            if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.data && typeof(response.data.data) == 'object' && response.data.data.Channels && response.data.data.Channels.filter(item=> item.ChannelUUID === authid).length) {
                console.log(response);
                removeTopProgressBar(credAdProcess3);
                return true;
            }
            else {
                removeTopProgressBar(credAdProcess3);
                return false;
            }
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
    },
    
    
    
    prepareAndSendBulkWhatsApp : function(){
        
        if(!UA_APP_UTILITY.currentSender){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var senderId = UA_APP_UTILITY.currentSender.value;
        if(!valueExists(senderId)){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var messageText = $("#inp-ssf-main-message").val().trim();
        if(!valueExists(messageText)){
            showErroWindow('Message is empty!', "Kindly fill message to proceed.");
            return false;
        }
        var recipNumbers = UA_APP_UTILITY.getCurrentMessageRecipients();
        if(recipNumbers.length === 0){
            showErroWindow('Recipient list is empty!', "Kindly add recipients to send SMS.");
            return false;
        }
        if(!UA_APP_UTILITY.fileUploadComplete){
            showErroWindow('File upload is not complete!', "Please wait while your file is uploading and try again.");
            return;
        }
        var messageNumberMapArray = [];
        var messageHistoryMap = {};
        $('#sms-prog-window').show();
            $('#msgItemsProgHolder').html('');
          var totalCount = recipNumbers.length;
          $('#totMsgDisp').text(`Sending messages 0/${totalCount}...`);
          var messagesSentCount = 0;
          var messageStatusMap = {
              'success': 0,
              'failed' : 0
          };
        recipNumbers.forEach(item=>{
            var message = {
                "templateid": UA_TPA_FEATURES.chosenMessgeTemplate.tempid
            };
            if(UA_APP_UTILITY.currentAttachedFile && UA_APP_UTILITY.currentAttachedFile.mediaUrl){
                message.url = UA_APP_UTILITY.currentAttachedFile.mediaUrl;
            }
            var contactRecordId = UA_APP_UTILITY.storedRecipientInventory[item.id] ? item.id : null;
            var resolvedMessageText = messageText + (message.url ? ' '+message.url : '');
            if(UA_TPA_FEATURES.chosenMessgeTemplate.placeholders){
                message.placeholders = [];
                UA_TPA_FEATURES.chosenMessgeTemplate.placeholders.forEach(item=>{
                    var placeHolderValue = $('#ssf-templ-place-input-'+item).val();
                    //if(contactRecordId){
                        placeHolderValue = UA_APP_UTILITY.getTemplateAppliedMessage(placeHolderValue, UA_APP_UTILITY.storedRecipientInventory[contactRecordId]);
                    //}
                    resolvedMessageText = resolvedMessageText.replace('{{'+item+'}}', placeHolderValue);
                    message.placeholders.push(placeHolderValue);
                });
            }
            var messagePayload = {
                "Text" : message,
                "Number" : item.number,
                "SenderId" : senderId,
                "DRNotifyHttpMethod" : "POST",
                "Tool":"API"
            };
            messageHistoryMap = {
                'contact':{
                    'name': UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].Full_Name : item.number
                },
                'from': senderId,
                'to': item.number,
                'message': resolvedMessageText,
                'status': 'NOT_SENT',
                'module': UA_SAAS_SERVICE_APP.widgetContext.module,
                'moduleId': item.id,
                'channel': 'WhatsApp'
            };

            $('#sms-prog-window').show();
            $('#msgItemsProgHolder').append(`
              <div id="msg-resp-item-${parseInt(item.number)}" class="msgRespItem" title="${getSafeString(resolvedMessageText)}">
                  <div class="mri-label"><b>${$('.msgRespItem').length+1}</b>. WhatsApp to ${UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].name+'-' : ''} ${item.number}</div>
                  <div class="mri-status">Sending...</div>
              </div>
          `);
            UA_TPA_FEATURES._proceedToSendWAAndExecuteCallback(messagePayload, function(response){
                console.log(response);
                messagesSentCount++;
                $(`#msg-resp-item-${parseInt(item.number)} .mri-status`).text(response.data.status+' - '+response.data.message).css({'color': response.data.status === "SUCCESS" ? 'green' : 'crimson'});
                response.data.status === "SUCCESS" ? messageStatusMap.success++ : messageStatusMap.failed++;
                if(messagesSentCount === totalCount){
                    $('#totMsgDisp').text(`All messages have been processed. TOTAL: ${totalCount}, SENT: ${messageStatusMap.success}, FAILED: ${messageStatusMap.failed}`);
                }else{
                    $('#totMsgDisp').text(`Sending messages ${messagesSentCount}/${totalCount}...`);
                }
                messageHistoryMap.status = response.data.status === "SUCCESS" ? "SUCCESS" : response.data.status+"-"+response.data.message;
                UA_SAAS_SERVICE_APP.addSentSMSAsRecordInHistory({
                    "message1": messageHistoryMap
                });
            });
        });
        
        
    },
    
    sendSMS : function(sender, number, text, callback){
        
        // var messageArray = [{
        //     "number": number,
        //     "text": text
        // }];
        
        // var messagePayload = {
        //     "sender": sender,
        //     "message": messageArray,
        //     "messagetype": "TXT"
        // };

        var messagePayload = {
            "Text" : text,
            "Number" : number,
            "SenderId" : sender,
            "DRNotifyHttpMethod" : "POST",
            "Tool":"API"
        };
        
        UA_TPA_FEATURES._proceedToSendSMSAndExecuteCallback(messagePayload, callback);
        
    },
    
    sendBulkSMS : async function(sender, msgType, numberMessageMapArray, callback, messageHistoryMap) {

        let successSentMsgCount = 0;

        let ErrorType = {
            '1702': 'Invalid URL. This means that one of the parameters was not provided or left blank.',
            '1703': 'Invalid value in username or password field.',
            '1704': 'nvalid message type.',
            '1705': 'Invalid message.',
            '1706': 'Invalid destination.',
            '1707': 'Invalid source (Sender ID).',
            '1708': 'Invalid dlr value.',
            '1709': 'User validation has failed.',
            '1710': 'Internal error.',
            '1715': 'Response timeout.',
            '1025': 'Insufficient credit.',
            '1032': 'DND reject.',
            '1028': 'Spam message.',
            '1033': 'You can send only approved template messages'
        };
        
        for(let MsgMapArr=0; MsgMapArr < numberMessageMapArray.length; MsgMapArr++) {

            var messagePayload = {
                'clientuid': numberMessageMapArray[MsgMapArr].clientuid
            };

            let msgText = encodeURIComponent(numberMessageMapArray[MsgMapArr].text);
            let msgNumber = numberMessageMapArray[MsgMapArr].number.replace(/\D/g,'');

            if(msgType == '2') {
                msgText = numberMessageMapArray[MsgMapArr].text.split("").map( char => ("0000" + char.charCodeAt(0).toString(16)).slice(-4) ).join("");
            }
            if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
                return false;
            }
            let successSentMsgRes = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://api.rmlconnect.net/bulksms/bulksms?username={{authkey}}&password={{authtoken}}&type=${msgType}&dlr=2&destination=${msgNumber}&source=${sender}&message=${msgText}`, "POST", messagePayload);
            if(successSentMsgRes && typeof(successSentMsgRes) == 'object' && successSentMsgRes.data && typeof(successSentMsgRes.data) == 'string') {
                if(successSentMsgRes.data.split('|')[0] == '1701') {
                    successSentMsgCount++;                
                    messageHistoryMap[messagePayload.clientuid].status = "SENT";
                    $(`#msg-resp-item-${parseInt(numberMessageMapArray[MsgMapArr].number)} .mri-status`).text(`Sent`).css({'color': 'green'});
                }
                else {
                    messageHistoryMap[messagePayload.clientuid].status = ErrorType[successSentMsgRes.data.split('|')[0]];
                    $(`#msg-resp-item-${parseInt(numberMessageMapArray[MsgMapArr].number)} .mri-status`).text(ErrorType[successSentMsgRes.data.split('|')[0]]).css({'color': 'crimson'});
                }
            }
            else {
                $(`#msg-resp-item-${parseInt(numberMessageMapArray[MsgMapArr].number)} .mri-status`).text(`Failed`).css({'color': 'crimson'});
            }

            $('#totMsgDisp').text(`Sending messages ${MsgMapArr+1}/${numberMessageMapArray.length}...`);
            if(MsgMapArr == numberMessageMapArray.length-1) {
                await UA_SAAS_SERVICE_APP.addSentSMSAsRecordInHistory(messageHistoryMap);
                await callback(successSentMsgRes, successSentMsgCount);
            }

        }
        
    },
    
    _proceedToSendSMSAndExecuteCallback: async function(messagePayload, callback) {
        if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        var sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://api.rmlconnect.net/bulksms/bulksms?username=${credential.apikey}&password=${credential.senderId}&type=${msgType}&dlr=2&destination=${requestMap.to}&source=${credential.extraId}&message=${msgText}`, "POST", messagePayload);
        return sentAPIResponse;
    },
    
    _proceedToSendWAAndExecuteCallback: async function(messagePayload, callback){
        if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        var sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://api.pinbot.ai/v1/wamessage/sendMessage", "POST", messagePayload);
        callback(sentAPIResponse);
    },
    
    getAPIResponse: async function (extensionName, url, method, data) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = {"Content-Type": "application/json"};
        await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, TPAService: "routemobile",url: url, method: method, data: data, headers: headers, _ua_lic_adminUserID: UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID}).then((response) => {
            response = response.data;
            returnResponse = response;
            removeTopProgressBar(credAdProcess3);
            return true;
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
        return returnResponse;
    },

    getCurrentAccountInfo: async function() {
        var headers = {"Content-Type": "application/json"};
        return await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, TPAService: "routemobile",url: '--checkbalance', method: 'get', data: {}, headers: headers}).then(async (response) => {
            if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.status && response.data.status == 'success' && response.data.ulgebraAppConfiguration) {
                return response.data.ulgebraAppConfiguration;
            }
            else {
                return false;
            }
        }).catch(err => {
            console.log(err);
            return false;
        });
    },    
    tpaCredentials: false,
    getCurrentAccountOrBalanceInfo: async function(callback) {

        UA_TPA_FEATURES.tpaCredentials = await UA_TPA_FEATURES.getCurrentAccountInfo();

        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);

        if(!UA_TPA_FEATURES.tpaCredentials) {
            UA_TPA_FEATURES.showReAuthorizeERROR();
            removeTopProgressBar(credAdProcess3);
            return false;
        }
        else {

            console.log(UA_TPA_FEATURES.tpaCredentials);            
            var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://api.rmlconnect.net/CreditCheck/checkcredits?username={{authkey}}&password={{authtoken}}`, "GET", {});
            
            if(response && typeof(response) == 'object' && response.data && (response.data.indexOf('BALANCE') > -1 || response.data == "PERMISSION DENIED")) {
                callback(response.data);
                removeTopProgressBar(credAdProcess3);
                return true;
            }
            else {
                UA_TPA_FEATURES.showReAuthorizeERROR();
                removeTopProgressBar(credAdProcess3);
                return false;
            }
            

            return true;
        }
        
        
    },
    getSafeString: function(rawStr) {
        if (!rawStr || rawStr+"".trim() === "") {
            return "";
        }

        return $('<textarea/>').text(rawStr).html();
    }
    
};