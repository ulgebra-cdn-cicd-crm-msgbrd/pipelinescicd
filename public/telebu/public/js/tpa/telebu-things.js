var UA_TPA_FEATURES = {
    chosenMessgeTemplate : null,
    'clientIds': {
        'pinnacleforhubspotcrm' : "f30b42b5-9aca-43df-8da5-fc91ac136ed0"
    },
    getSupportedChannels: function(){
        return [ UA_APP_UTILITY.MESSAGING_CHANNELS.SMS ];
    },
    workflowCode : {
        "SMS": {
            "SenderId": "FILL_HERE",
            "Number": "FILL_HERE",
            "Text": "FILL_HERE",
            "module": "FILL_HERE",
            "recordId": "FILL_HERE",
            "channel": "SMS",
            "ulgebra_webhook_authtoken": null
        }
    },
    renderInitialElements: async function(){
        
        if(extensionName.endsWith('zohocrm')){
            UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED.remove_by_value('ZohoCRM.modules.ALL');
            UA_SAAS_SERVICE_APP.CURRENT_OAUTH_SCOPE_NEEDED.push('ZohoCRM.modules.READ', 'ZohoCRM.modules.UPDATE', 'ZohoCRM.modules.CREATE');
        }
            
        $('#ac_name_label_tpa').after(`<div id="ac_name_label_saas" class="suo-item suo-item-ac-labels" onclick="UA_TPA_FEATURES.openWABAAPIKey()">
                                        <span class="material-icons">restart_alt</span> Update WABA Account
                                        <div class="ac_wa_name_id" style="display: none; font-size: 12px; text-align: right; display: block; color: grey; "></div>
                                    </div>`);
        
        $("#ac_name_label_tpa .anl_servicename").text('Telebu');
        UA_TPA_FEATURES.getCurrentAccountOrBalanceInfo(async function(resp) {
            $('#ac_name_label_tpa .ac_name_id').text('Telebu Balance : '+resp['Accounts'][0].Amount);
            if(UA_TPA_FEATURES.tpaCredentials.WABAId) {
                $('.ac_wa_name_id').text('Telebu WABA Id : '+UA_TPA_FEATURES.tpaCredentials.WABAId).show();
            }
        
        
        var fetchedTemplatesResponse = await UA_TPA_FEATURES._getServiceTemplates();
        console.log('fetchedtemoktes ', fetchedTemplatesResponse);
        fetchedTemplatesCollection = fetchedTemplatesResponse.data.Templates;
        
        if(fetchedTemplatesCollection.length) {
            var templatesArray = [];
            fetchedTemplatesCollection.forEach(item=>{
                UA_APP_UTILITY.fetchedTemplates[item.Name] = {
                    'message': item.Template
                };
                templatesArray.push({
                    'label': item.Name,
                    'value': item.Name
                });
            });
            var approvedTemplateDDId = UA_APP_UTILITY.renderSelectableDropdown('#ssf-fitem-template-var-holder', 'Insert Telebu template', templatesArray, 'UA_TPA_FEATURES.insertTemplateContentInMessageInput', false, false);
        }
        var fetchedSenderResponse = await UA_TPA_FEATURES._getServiceSenders();
        console.log('fetchedtemoktes ', fetchedSenderResponse);
        fetchedSendersCollection = fetchedSenderResponse.data.SenderIds;
        
        if(fetchedSendersCollection.length) {
            var sendersArray = [];
            var sendersUniqueArray = [];
            fetchedSendersCollection.forEach(item=>{
                if(sendersUniqueArray.indexOf(item.SenderId) === -1){
                    sendersUniqueArray.push(item.SenderId);
                    UA_APP_UTILITY.addMessaegSender(item.SenderId, UA_APP_UTILITY.MESSAGING_CHANNELS.SMS, item.SenderId);
                    sendersArray.push({
                        'label': item.SenderId,
                        'value': item.SenderId
                    });
                }
            });
        }
        
        UA_APP_UTILITY.TPA_SENDERS_FETCH_COMPLETED();
        UA_APP_UTILITY.getPersonalWebhookAuthtoken();
       
        var countryCallCodeArray = [];
        UA_APP_UTILITY.countryCallingCodeArray.forEach(item=>{
            countryCallCodeArray.push({
                'label': `${item.name} (${item.dial_code})`,
                'value': item.dial_code
            });
        });
        
        UA_APP_UTILITY.renderSelectableDropdown('#ssf-new-recip-countrycode', 'Select country', countryCallCodeArray, 'UA_APP_UTILITY.log');
   
        // var fetchedTemplatesWAResponse = await UA_TPA_FEATURES._getServiceWATemplates(true);
        // console.log('wa fetchedtemoktes ', fetchedTemplatesWAResponse);
        //  var waTemplatesArray = [];
        // fetchedTemplatesWAResponse.data.forEach(item=>{
        //     UA_APP_UTILITY.fetchedTemplates[item.tempid] = {
        //         'message': item.body_message,
        //         'templateType': 'placeholder_template',
        //         'placeholders': item.placeholders ? item.placeholders.split(',') : [],
        //         'tempid': item.tempid,
        //         'head_media_url': item.head_media_url,
        //         'head_mediatype': item.head_mediatype
        //     };
        //     waTemplatesArray.push({
        //         'label': item.temptitle + ' (WhatsApp)',
        //         'value': item.tempid
        //     });
        // });
        
        // UA_APP_UTILITY.renderSelectableDropdown('#ssf-fitem-template-var-holder', 'Insert Telebu template', waTemplatesArray, 'UA_TPA_FEATURES.insertTemplateContentInMessageInput', false, false, approvedTemplateDDId);
        
        });
        
    },
    
    renderWorkflowBodyCode :function(accessToken){
        
        UA_TPA_FEATURES.workflowCode.SMS.ulgebra_webhook_authtoken = accessToken.saas;
        //UA_TPA_FEATURES.workflowCode.WhatsApp.ulgebra_webhook_authtoken = accessToken.saas;
        
        UA_APP_UTILITY.addWorkflowBodyCode('sms', 'For Sending SMS', UA_TPA_FEATURES.workflowCode.SMS);
        
        //UA_APP_UTILITY.addWorkflowBodyCode('whatsapp', 'For Sending WhatsApp', UA_TPA_FEATURES.workflowCode.WhatsApp);
    },
    
    
    
    prepareAndSendSMS: function(){
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        if(UA_TPA_FEATURES.chosenMessgeTemplate && UA_TPA_FEATURES.chosenMessgeTemplate.templateType === "placeholder_template"){
            return UA_TPA_FEATURES.prepareAndSendBulkWhatsApp();
        }
        if(!UA_APP_UTILITY.currentSender){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var senderId = UA_APP_UTILITY.currentSender.value;
        if(!valueExists(senderId)){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var recipNumbers = UA_APP_UTILITY.getCurrentMessageRecipients();
        
        if(recipNumbers.length === 0){
            showErroWindow('Recipient list is empty!', "Kindly add recipients to send SMS.");
            return false;
        }

        var messageText = $("#inp-ssf-main-message").val().trim();

        if(!valueExists(messageText)){
            showErroWindow('Message is empty!', "Kindly fill message to proceed.");
            return false;
        }
        if(messageText.length > 600){
            showErroWindow('Message is Large!', "Message should be within 600 characters.");
            return false;
        }

        $('#sms-prog-window').show();
        $('#msgItemsProgHolder').html('');
        $('#totMsgDisp').text(`Sending messages 0/${recipNumbers.length}...`);

        var messageNumberMapArray = [];
        var messageHistoryMap = {};
        recipNumbers.forEach(item=>{
            let resolvedMessageText = UA_APP_UTILITY.getTemplateAppliedMessage(messageText, UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id]);
            var historyUID = item.id+''+new Date().getTime()+''+Math.round(Math.random(100000,99999)*1000000);
            messageNumberMapArray.push({
                'number': item.number,
                'text': resolvedMessageText,
                'clientuid': historyUID
            });
            messageHistoryMap[historyUID] = {
                'contact':{
                    'name': UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].Full_Name : item.number
                },
                'from': senderId,
                'to': item.number,
                'message': resolvedMessageText,
                'status': 'NOT_SENT',
                'module': UA_SAAS_SERVICE_APP.widgetContext.module,
                'moduleId': item.id,
                'channel': "SMS"
            };

            $('#msgItemsProgHolder').append(`
                <div id="msg-resp-item-${parseInt(item.number)}" class="msgRespItem" title="${getSafeString(resolvedMessageText)}">
                  <div class="mri-label"><b>${$('.msgRespItem').length+1}</b>. SMS to ${UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].name+'-' : ''} ${item.number}</div>
                  <div class="mri-status">Sending...</div>
                </div>
            `);

        });
        
        UA_TPA_FEATURES.sendBulkSMS(senderId, messageNumberMapArray, (function(successSentMsgRes, successSentMsgCount){
            if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
                return false;
            }
            if(successSentMsgRes && typeof(successSentMsgRes.data) == 'object' && successSentMsgRes.data && typeof(successSentMsgRes.data) == 'object' && successSentMsgRes.data.Success && successSentMsgRes.data.Success == 'True' && successSentMsgRes.data.MessageUUID) {
                $('#totMsgDisp').text(`All messages have been processed. TOTAL: ${messageNumberMapArray.length}, SENT: ${successSentMsgCount}, FAILED: ${messageNumberMapArray.length-successSentMsgCount}`);
                UA_APP_UTILITY.resetMessageFormFields(); 
            }
            else{
                if(successSentMsgRes && typeof(successSentMsgRes) == 'object' && successSentMsgRes.error && typeof(successSentMsgRes.error) == 'object' && successSentMsgRes.error.Message) {
                    $('#totMsgDisp').text(`All messages have been processed. TOTAL: ${messageNumberMapArray.length}, SENT: ${successSentMsgCount}, FAILED: ${messageNumberMapArray.length-successSentMsgCount}`);
                    UA_APP_UTILITY.resetMessageFormFields(); 
                }
                else {
                    //showErroWindow('Unable to send message', '');
                    return;
                }              
            }
        }), messageHistoryMap);
        
    },
    
    
    
    
    
    insertTemplateContentInMessageInput :function(templateId){
        var curTemplateItem = UA_APP_UTILITY.fetchedTemplates[templateId];
        UA_TPA_FEATURES.chosenMessgeTemplate = curTemplateItem;
        $("#messageAttachmentInputHolder").hide();
        UA_APP_UTILITY.currentAttachedFile = null;
        $("#attachedfile").text('Attach file');
        $('#inputFileAttach').val('');
        if(curTemplateItem.templateType === "placeholder_template"){
            $("#primary-send-btn").text('Send WhatsApp').css('background-color', 'green');
            if(curTemplateItem.head_mediatype){
                switch (curTemplateItem.head_mediatype) {
                    case "0":
                        $("#attachedfile").text('Attach document');
                        break;
                    case "1":
                        $("#attachedfile").text('Attach image');
                        break;
                    case "2":
                        $("#attachedfile").text('Attach video');
                        break;
                    default:
                        $("#attachedfile").text('Attach file');
                        break;
                }
            }
            if(curTemplateItem.head_media_url){
                $("#messageAttachmentInputHolder").show();
            }
            $("#ssf-fitem-template-placeholder-holder-listholder").html("");
            curTemplateItem.placeholders.forEach(item=>{
                $("#ssf-fitem-template-placeholder-holder-listholder").append(`
                    <div class="ssf-temp-placehold-item">
                            <span class="ssf-temp-placehold-item-label">{{${item}}}</span> <span class="material-icons ssf-temp-placehold-item-icon">double_arrow</span> <input onblur="UA_APP_UTILITY.lastFocusedInputElemForPlaceholderInsert = 'ssf-templ-place-input-${item}'" id="ssf-templ-place-input-${item}" data-placeholder-id="${item}" class="ssf-temp-placehold-item-input" type="text" placeholder="Type or choose field from above">
                        </div>
                `);
            });
            if(curTemplateItem.placeholders.length > 0){
                $("#ssf-fitem-template-placeholder-holder").show();
            }else{
                $("#ssf-fitem-template-placeholder-holder").hide();
            }
            $('#inp-ssf-main-message').attr('readonly', true);
        }
        else{
            $("#primary-send-btn").text('Send SMS').css('background-color', 'royalblue');
            $("#ssf-fitem-template-placeholder-holder").hide();
            $('#inp-ssf-main-message').removeAttr('readonly');
        }
        $('#inp-ssf-main-message').val(curTemplateItem.message).focus();
    },
    
    _getServiceSenders: async function(){

        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://restapi.smscountry.com/v0.1/Accounts/"+UA_TPA_FEATURES.tpaCredentials.authId+"/SenderIDs", "GET");
        if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.Success && response.data.Success == 'True' && response.data.SenderIds) {
            return  response;
        }
        else {
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return false;
        }
        
    },
    
    _getServiceTemplates: async function() {

        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://restapi.smscountry.com/v0.1/Accounts/"+UA_TPA_FEATURES.tpaCredentials.authId+"/Templates", "GET");
        if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.Success && response.data.Success == 'True' && response.data.Templates) {
            return response;
        }
        else {
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return false;
        }
        
    },
    
    _getServiceWATemplates: async function(silenceAuthError = false){
        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://restapi.smscountry.com/v0.1/Accounts/"+UA_TPA_FEATURES.tpaCredentials.authId+"/WhatsappTemplate/"+UA_TPA_FEATURES.tpaCredentials.ChannelUUID, "GET");
        if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.Message && response.data.Message == 'Success.' && response.data.Table) {
            return  response;
        }
        else {
            if(silenceAuthError){
                return {
                    data:{
                        data: []
                    }
                };
            }
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return;
        }
    },
    
    showReAuthorizeERROR: function(showCloseOption = false){
        if($('#inp_tpa_api_key').length > 0){
            console.log('already showing reautherr');
            return;
        }
        showErroWindow("Authentication needed!", `You need to authorize your Telebu Account to proceed. <br><br> <div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
        <div style="font-size: 18px;margin-bottom: 5px;margin-top: 20px;">Telebu Auth ID</div>
        <div class="help_apikey_tip"><a target="_blank" href="">Contact Us : contactus@smscountry.com</a></div>
        <input id="inp_tpa_api_key" type="text" placeholder="Enter your Auth Id..." style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
        
        <div style="font-size: 18px;margin-bottom: 5px;margin-top: 20px;">Telebu Auth Token</div>
        <div class="help_apikey_tip"><a target="_blank" href="">Contact Us : contactus@smscountry.com</a></div>
        <input id="inp_tpa_api_token" type="text" placeholder="Enter your Auth Token..." style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
        <button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.saveTPAAPIKey(UA_APP_UTILITY.reloadWindow)">Authorize Now</button>
    </div>`, showCloseOption, false, 500);
        $('#inp_tpa_api_key').focus();
        UA_LIC_UTILITY.showExistingInvitedAdminDDHTML(true);
    },
    
    openWABAAPIKey: function(showCloseOption = false){
        showErroWindow("WhatsApp Authentication needed!", `<div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
        <div style="font-size: 18px;margin-bottom: 5px;margin-top: 20px;">Telebu Channel UUID</div>
        <div class="help_apikey_tip" style="cursor: default;"><a>Contact : </b>Telebu</a></div>
        <input id="inp_tpa_waba_api_key" type="text" placeholder="Enter your Channel UUID..." style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
        
        <div style="font-size: 18px;margin-bottom: 5px;margin-top: 20px;">Telebu WABA Id</div>
        <div class="help_apikey_tip" style="cursor: default;"><a>Contact : </b>Telebu</a></div>
        <input id="inp_tpa_waba_api_token" type="text" placeholder="Enter your WABA Id..." style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
        <button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.saveTPAWABAAPIKey(UA_APP_UTILITY.reloadWindow)">Authorize Now</button>
    </div>`, showCloseOption, false, 500);
        $('#inp_tpa_waba_api_key').focus();
    },
    
    showReAuthorizeERRORWithClose: function(){
        UA_TPA_FEATURES.showReAuthorizeERROR();
    },
    
    saveTPAAPIKey: async function(callback){
        let authkey = $("#inp_tpa_api_key").val();
        if(!valueExists(authkey)){
            showErroMessage("Please fill Auth Id");
            return;
        }
        let authtoken = $("#inp_tpa_api_token").val();
        if(!valueExists(authtoken)){
            showErroMessage("Please fill Auth Token");
            return;
        }

        let isTpaAccountAutherized = await UA_TPA_FEATURES.isTpaAccountAutherized(extensionName, "https://restapi.smscountry.com/v0.1/Accounts/"+authkey+"/", "GET", {}, authkey, authtoken);
        
        if(isTpaAccountAutherized) {
            await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', '__multiple_keys__', {'authId' : authkey, 'authtoken': authtoken}, callback);
        }
        else {
            showErroMessage("<b>Please provide Authorization</b></br></br>Auth ID or Auth Token is Incorrect.");
            return;
        }

    },
    isTpaAccountAutherized: async function (extensionName, url, method, data, authid, authtoken) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = {"Content-Type": "application/json", 'Authorization':'Basic '+btoa(authid+':'+authtoken)};
        return await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, url: url, method: method, data: data, headers: headers}).then(async (response) => {
            if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.data && typeof(response.data.data) == 'object' && response.data.data.Success && response.data.data.Success == 'True' && response.data.data.Accounts) {
                removeTopProgressBar(credAdProcess3);
                return true;
            }
            else {
                removeTopProgressBar(credAdProcess3);
                return false;
            }
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
    },
    saveTPAWABAAPIKey: async function(callback){

        let authkey = $("#inp_tpa_waba_api_key").val();
        if(!valueExists(authkey)){
            showErroMessage("Please fill Channel UUID");
            return;
        }
        let authtoken = $("#inp_tpa_waba_api_token").val();
        if(!valueExists(authtoken)){
            showErroMessage("Please fill WABA Id");
            return;
        }

        let isTpaWaBaAccountAutherized = await UA_TPA_FEATURES.isTpaWaBaAccountAutherized(extensionName, "https://restapi.smscountry.com/v0.1/Accounts/"+UA_TPA_FEATURES.tpaCredentials.authId+"/WhatsappTemplate/", "GET", {}, authkey);
        
        if(isTpaWaBaAccountAutherized) {
            await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', '__multiple_keys__', {'ChannelUUID' : authkey, 'WABAId': authtoken}, callback);
        }
        else {
            showErroMessage("<b>Please provide Authorization</b></br></br>Channel UUID or WABA Id is Incorrect.");
            return;
        }
    },
    isTpaWaBaAccountAutherized: async function (extensionName, url, method, data, authid) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = {"Content-Type": "application/json"};
        return await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, TPAService: "telebu",url: url, method: method, data: data, headers: headers}).then(async (response) => {
            if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.data && typeof(response.data.data) == 'object' && response.data.data.Channels && response.data.data.Channels.filter(item=> item.ChannelUUID === authid).length) {
                console.log(response);
                removeTopProgressBar(credAdProcess3);
                return true;
            }
            else {
                removeTopProgressBar(credAdProcess3);
                return false;
            }
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
    },
    
    
    
    prepareAndSendBulkWhatsApp : function(){
        
        if(!UA_APP_UTILITY.currentSender){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var senderId = UA_APP_UTILITY.currentSender.value;
        if(!valueExists(senderId)){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var messageText = $("#inp-ssf-main-message").val().trim();
        if(!valueExists(messageText)){
            showErroWindow('Message is empty!', "Kindly fill message to proceed.");
            return false;
        }
        var recipNumbers = [];
        $('.ssf-recip-item.selected').each((i, item)=>{
            recipNumbers.push({
                'id': $(item).attr('data-recip-id'),
                'number': $(item).attr('data-recip-number')
            });
        });
        if(recipNumbers.length === 0){
            showErroWindow('Recipient list is empty!', "Kindly add recipients to send SMS.");
            return false;
        }
        if(!UA_APP_UTILITY.fileUploadComplete){
            showErroWindow('File upload is not complete!', "Please wait while your file is uploading and try again.");
            return;
        }
        var messageNumberMapArray = [];
        var messageHistoryMap = {};
        $('#sms-prog-window').show();
            $('#msgItemsProgHolder').html('');
          var totalCount = recipNumbers.length;
          $('#totMsgDisp').text(`Sending messages 0/${totalCount}...`);
          var messagesSentCount = 0;
          var messageStatusMap = {
              'success': 0,
              'failed' : 0
          };
        recipNumbers.forEach(item=>{
            var message = {
                "templateid": UA_TPA_FEATURES.chosenMessgeTemplate.tempid
            };
            if(UA_APP_UTILITY.currentAttachedFile && UA_APP_UTILITY.currentAttachedFile.mediaUrl){
                message.url = UA_APP_UTILITY.currentAttachedFile.mediaUrl;
            }
            var contactRecordId = UA_APP_UTILITY.storedRecipientInventory[item.id] ? item.id : null;
            var resolvedMessageText = messageText + (message.url ? ' '+message.url : '');
            if(UA_TPA_FEATURES.chosenMessgeTemplate.placeholders){
                message.placeholders = [];
                UA_TPA_FEATURES.chosenMessgeTemplate.placeholders.forEach(item=>{
                    var placeHolderValue = $('#ssf-templ-place-input-'+item).val();
                    //if(contactRecordId){
                        placeHolderValue = UA_APP_UTILITY.getTemplateAppliedMessage(placeHolderValue, UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[contactRecordId]);
                    //}
                    resolvedMessageText = resolvedMessageText.replace('{{'+item+'}}', placeHolderValue);
                    message.placeholders.push(placeHolderValue);
                });
            }
            var messagePayload = {
                "Text" : message,
                "Number" : item.number,
                "SenderId" : senderId,
                "DRNotifyHttpMethod" : "POST",
                "Tool":"API"
            };
            messageHistoryMap = {
                'contact':{
                    'name': UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].Full_Name : item.number
                },
                'from': senderId,
                'to': item.number,
                'message': resolvedMessageText,
                'status': 'NOT_SENT',
                'module': UA_SAAS_SERVICE_APP.widgetContext.module,
                'moduleId': item.id,
                'channel': 'WhatsApp'
            };
            $('#msgItemsProgHolder').append(`
              <div id="msg-resp-item-${parseInt(item.number)}" class="msgRespItem" title="${getSafeString(resolvedMessageText)}">
                  <div class="mri-label"><b>${$('.msgRespItem').length+1}</b>. WhatsApp to ${UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].name+'-' : ''} ${item.number}</div>
                  <div class="mri-status">Sending...</div>
              </div>
          `);
            UA_TPA_FEATURES._proceedToSendWAAndExecuteCallback(messagePayload, function(response){
                if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
                    return false;
                }
                console.log(response);
                messagesSentCount++;
                $(`#msg-resp-item-${parseInt(item.number)} .mri-status`).text(response.data.status+' - '+response.data.message).css({'color': response.data.status === "SUCCESS" ? 'green' : 'crimson'});
                response.data.status === "SUCCESS" ? messageStatusMap.success++ : messageStatusMap.failed++;
                if(messagesSentCount === totalCount){
                    $('#totMsgDisp').text(`All messages have been processed. TOTAL: ${totalCount}, SENT: ${messageStatusMap.success}, FAILED: ${messageStatusMap.failed}`);
                }else{
                    $('#totMsgDisp').text(`Sending messages ${messagesSentCount}/${totalCount}...`);
                }
                messageHistoryMap.status = response.data.status === "SUCCESS" ? "SUCCESS" : response.data.status+"-"+response.data.message;
                UA_SAAS_SERVICE_APP.addSentSMSAsRecordInHistory({
                    "message1": messageHistoryMap
                });
            });
        });
        
        
    },
    
    sendSMS : function(sender, number, text, callback){
        
        // var messageArray = [{
        //     "number": number,
        //     "text": text
        // }];
        
        // var messagePayload = {
        //     "sender": sender,
        //     "message": messageArray,
        //     "messagetype": "TXT"
        // };

        var messagePayload = {
            "Text" : text,
            "Number" : number,
            "SenderId" : sender,
            "DRNotifyHttpMethod" : "POST",
            "Tool":"API"
        };
        
        UA_TPA_FEATURES._proceedToSendSMSAndExecuteCallback(messagePayload, callback);
        
    },
    
    sendBulkSMS : async function(sender, numberMessageMapArray, callback, messageHistoryMap) {

        let successSentMsgCount = 0;
        
        for(let MsgMapArr=0; MsgMapArr < numberMessageMapArray.length; MsgMapArr++) {

            var messagePayload = {
                "Text" : numberMessageMapArray[MsgMapArr].text,
                "Number" : numberMessageMapArray[MsgMapArr].number.replace(/\D/g,''),
                "SenderId" : sender,
                "DRNotifyHttpMethod" : "POST",
                "Tool":"API",
                'clientuid': numberMessageMapArray[MsgMapArr].clientuid
            };
            
            let successSentMsgRes = await UA_TPA_FEATURES._proceedToSendSMSAndExecuteCallback(messagePayload, '');
            if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
                return false;
            }
            if(successSentMsgRes && typeof(successSentMsgRes) == 'object' && successSentMsgRes.data && typeof(successSentMsgRes.data) == 'object' && successSentMsgRes.data.Success && successSentMsgRes.data.Success == 'True' && successSentMsgRes.data.MessageUUID) {
                successSentMsgCount++;                
                messageHistoryMap[messagePayload.clientuid].status = "SENT";
                $(`#msg-resp-item-${parseInt(numberMessageMapArray[MsgMapArr].number)} .mri-status`).text(`Sent`).css({'color': 'green'});
            }
            else {
                if(successSentMsgRes && typeof(successSentMsgRes) == 'object' && successSentMsgRes.error && typeof(successSentMsgRes.error) == 'object' && successSentMsgRes.error.Message) {
                    messageHistoryMap[messagePayload.clientuid].status = successSentMsgRes.error.Message;
                }
                $(`#msg-resp-item-${parseInt(numberMessageMapArray[MsgMapArr].number)} .mri-status`).text('Failed').css({'color': 'crimson'});
            }

            $('#totMsgDisp').text(`Sending messages ${MsgMapArr+1}/${numberMessageMapArray.length}...`);
            if(MsgMapArr == numberMessageMapArray.length-1) {
                await UA_SAAS_SERVICE_APP.addSentSMSAsRecordInHistory(messageHistoryMap);
                await callback(successSentMsgRes, successSentMsgCount);
            }

        }
        
    },
    
    _proceedToSendSMSAndExecuteCallback: async function(messagePayload, callback) {
        messagePayload.Number = messagePayload.Number.replace(/\D/g,'');
        if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        var sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://restapi.smscountry.com/v0.1/Accounts/${UA_TPA_FEATURES.tpaCredentials.authId}/SMSes/`, "POST", messagePayload);
        return sentAPIResponse;
    },
    
    _proceedToSendWAAndExecuteCallback: async function(messagePayload, callback){
        if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        var sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://api.pinbot.ai/v1/wamessage/sendMessage", "POST", messagePayload);
        callback(sentAPIResponse);
    },
    
    getAPIResponse: async function (extensionName, url, method, data) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = {"Content-Type": "application/json"};
        // if(url.startsWith("https://console.pinbot.ai") || url.startsWith("https://api.pinbot.ai")){
        //     headers.apikey = "{{WABA_APIKEY}}";
        // }
        await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, TPAService: "telebu",url: url, method: method, data: data, headers: headers, _ua_lic_adminUserID: UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID}).then((response) => {
            response = response.data;
            returnResponse = response;
            removeTopProgressBar(credAdProcess3);
            return true;
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
        return returnResponse;
    },

    getCurrentAccountInfo: async function() {
        var headers = {"Content-Type": "application/json"};
        return await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, TPAService: "telebu",url: '--checkbalance', method: 'get', data: {}, headers: headers}).then(async (response) => {
            if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.status && response.data.status == 'success' && response.data.ulgebraAppConfiguration) {
                return response.data.ulgebraAppConfiguration;
            }
            else {
                return false;
            }
        }).catch(err => {
            console.log(err);
            return false;
        });
    },    
    tpaCredentials: false,
    getCurrentAccountOrBalanceInfo: async function(callback) {

        UA_TPA_FEATURES.tpaCredentials = await UA_TPA_FEATURES.getCurrentAccountInfo();

        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);

        if(!UA_TPA_FEATURES.tpaCredentials) {
            UA_TPA_FEATURES.showReAuthorizeERROR();
            removeTopProgressBar(credAdProcess3);
            return false;
        }
        else {

            console.log(UA_TPA_FEATURES.tpaCredentials);            
            var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://restapi.smscountry.com/v0.1/Accounts/"+UA_TPA_FEATURES.tpaCredentials.authId+"/", "GET", {});
            
            if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && (response.data.Success && response.data.Success == 'True' && response.data.Accounts)) {
                callback(response.data);
                removeTopProgressBar(credAdProcess3);
                return true;
            }
            else {
                UA_TPA_FEATURES.showReAuthorizeERROR();
                removeTopProgressBar(credAdProcess3);
                return false;
            }
            

            return true;
        }
        
        
    }
    
};
