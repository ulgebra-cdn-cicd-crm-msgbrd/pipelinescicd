var UA_TPA_FEATURES = {
    chosenMessgeTemplate : null,
    'clientIds': {
        'messagebirdforhubspotcrm' : "9be1eb84-88d7-4bbe-972b-f507b913f504",
        'messagebirdforbitrix24': 'app.62e2625a500219.63455613',
        'messagebirdforpipedrive': '89e0cf9aefc3fdae'
    },
    APP_SAVED_CONFIG : null,
    getSupportedChannels: function(){
        return [ UA_APP_UTILITY.MESSAGING_CHANNELS.SMS, UA_APP_UTILITY.MESSAGING_CHANNELS.WHATSAPP, UA_APP_UTILITY.MESSAGING_CHANNELS.VIBER ];
    },
    workflowCode : {
        "Channel_TEXT": {
            "channelId": "FILL_HERE_SMS_WHATSAPP_VIBER_Channel_ID",
            "to": "FILL_HERE_CONTACT_NUMBER_WITH_COUNTRY_CODE",
            "type": "text",
            "content":{
                "text": "FILL_HERE_Message_Text"
            },
            "source":{
                "type":"uaapp-workflow"
            },
            "module": "FILL_HERE",
            "recordId": "FILL_HERE",
            "channel": "SMS",
            "ulgebra_webhook_authtoken": null
        },
        "Channel_MEDIA": {
            "channelId": "FILL_HERE_SMS_WHATSAPP_VIBER_Channel_ID",
            "to": "FILL_HERE_CONTACT_NUMBER_WITH_COUNTRY_CODE",
            "type": "image",
            "content":{
                "image": {
                    "url": "FILL_HERE_Attachment_URL",
                    "caption": "FILL_HERE_YOUR_CAPTION"
                }
            },
            "source":{
                "type":"uaapp-workflow"
            },
            "module": "FILL_HERE",
            "recordId": "FILL_HERE",
            "channel": "WhatsApp",
            "ulgebra_webhook_authtoken": null
        },
        "WhatsApp_Template":{
            "channelId": "FILL_HERE_WhatsApp_Channel_ID",
            "to": "FILL_HERE_CONTACT_NUMBER_WITH_COUNTRY_CODE",
            "type": "hsm",
            "content":{
                "hsm": {
                    "namespace": "FILL_HERE_YOUR_WHATSAPP_NAMESPACE_ID",
                    "templateName": "FILL_HERE_YOUR_WHATSAPP_TEMPLATE_NAME",
                    "language": {
                        "policy": "deterministic",
                        "code": "en"
                    },
                    "params": [
                        {
                            "default": "FILL_HERE_PARAM_{{1}}_VALUE"
                        },
                        {
                            "default": "FILL_HERE_PARAM_{{2}}_VALUE"
                        }
                    ]
                }
            },
            "source":{
                "type":"uaapp-workflow"
            },
            "module": "FILL_HERE",
            "recordId": "FILL_HERE",
            "channel": "WhatsApp",
            "ulgebra_webhook_authtoken": null
        },
        "WhatsApp_Media_Template":{
            "channelId": "FILL_HERE_WhatsApp_Channel_ID",
            "to": "FILL_HERE_CONTACT_NUMBER_WITH_COUNTRY_CODE",
            "type": "hsm",
            "content":{
                "hsm": {
                    "namespace":"FILL_HERE_YOUR_WHATSAPP_NAMESPACE_ID",
                    "templateName":"FILL_HERE_YOUR_WHATSAPP_MEDIA_TEMPLATE_NAME",
                    "language":{
                        "policy":"deterministic",
                        "code":"en"
                    },
                    "components":[
                        {
                            "type":"header",
                            "parameters":[
                                {
                                    "type":"image",
                                    "image":{
                                        "url":"FILL_HERE_Attachment_URL"
                                    }
                                }
                            ]
                        },
                        {
                          "type":"body",
                          "parameters":[
                                {
                                    "type":"text",
                                    "text":"FILL_HERE_PARAM_{{1}}_VALUE"
                                },
                                {
                                    "type":"text",
                                    "text":"FILL_HERE_PARAM_{{2}}_VALUE"
                                }
                            ]
                        }
                    ]
                },
                "source":{
                    "type":"uaapp-workflow"
                },
                "module": "FILL_HERE",
                "recordId": "FILL_HERE",
                "channel": "WhatsApp",
                "ulgebra_webhook_authtoken": null
            }
        }
    },
    accountSID: null,
    serviceSID : null,
    incomingDataSchemaJSON: {
        "message": {
            "AccountSid": {
                example: "ACaf4d4f141ceaf613a1743b42ed6c602c"
            },
            "ApiVersion": {
                example: "2010-04-01"
            },
            "Body": {
                example: "Hi, how are you?"
            },
            "From": {
                example: "+919994411345"
            },
            "MessageSid": {
                example: "SMd85014e10671da8bce767a5eba08b289"
            },
            "NumSegments": {
                example: "1"
            },
            "ProfileName": {
                example: "Vijay"
            },
            "SmsMessageSid": {
                example: "SMd85014e10671da8bce767a5eba08b289"
            },
            "SmsSid": {
                example: "SMd85014e10671da8bce767a5eba08b289"
            },
            "SmsStatus": {
                example: "received"
            },
            "To": {
                example: "+14155238886"
            },
            "WaId": {
                example: "919994411345"
            },
            "ReferralNumMedia": {
                example: "0"
            },
            "MediaContentType0":{
                example: "image/jpeg"
            },
            "MediaUrl0": {
                example: "https://api.twilio.com/2010-04-01/Accounts/ACaf4d4f141ceaf613a1743b42ed6c602c/Messages/MM879f4f89bdf3b475a5ed414bcd5b29e2/Media/ME8c3c58035abe73f7ca7746f317c2fc37"
            },
            "NumMedia": {
                example: "0"
            },
            "_UA_Channel": {
                example: "SMS"
            }
        }
    },
    FETCHED_CHANNELS : {},
    renderInitialElements: async function(){
        UA_APP_UTILITY.addRTNotificationNavigation();
        $("#ac_name_label_tpa .anl_servicename").text('MessageBird');
        
        $(".incoming-config-tpa-entity").text('messages');
        $('#incoming-channel-display-text').text(`Choose which channel messages should be captured in this integration.`);
        UA_TPA_FEATURES.renderFieldMappingCriteriaExtra();
        //UA_TPA_FEATURES.getCurrentAccountOrBalanceInfo(async function(resp){
           // $('#ac_name_label_tpa .ac_name_id').text(resp.data.balance);
            
            //if(!resp.data.balance){
              //  return;
           // }
            UA_TPA_FEATURES.fetchTemplatesAndRenderInBackground();
            UA_APP_UTILITY.getPersonalWebhookAuthtoken();
            var numbersAPIResp = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://integrations.messagebird.com/v1/public/integrations`, "GET");
            UA_TPA_FEATURES.showIfErrorInAPIResult(numbersAPIResp);
            if(numbersAPIResp.data.count === 0){
                numbersAPIResp.data.items = [];
            }
            $('#ac_name_label_tpa .ac_name_id').text(numbersAPIResp.data.items.length+' channels found.');
            let ignorableChannels = ["livechat", 'events'];
            numbersAPIResp.data.items.forEach(item=>{
                if(item.slug === "events"){
                    return;
                }
                item.value = item.id;
                UA_TPA_FEATURES.FETCHED_CHANNELS[item.id] = item;
                let channel = UA_APP_UTILITY.MESSAGING_CHANNELS[item.slug.toUpperCase()];
                UA_APP_UTILITY.addMessaegSender(item.id, channel, item.name, item);
                if(ignorableChannels.includes(item.slug)){
                    let selectorSK = `#DD_HOLDER_SENDER_LIST [data-ddlabel="${(channel? channel.label+"-"+item.name : item.name).replace(/[\W_]+/g," ")}"]`;
                    $(selectorSK).addClass('hideOnFormView hideOnChatView');
                    console.log(selectorSK);
                    return;
                }
            });
            UA_TPA_FEATURES.insertIncomingWD_channels();
            numbersAPIResp = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://integrations.messagebird.com/v1/public/integrations/whatsapp`, "GET");
            UA_TPA_FEATURES.showIfErrorInAPIResult(numbersAPIResp);
            if(numbersAPIResp.data.count === 0){
                numbersAPIResp.data.items = [];
            }
            $('#ac_name_label_tpa .ac_name_id').text(numbersAPIResp.data.items.length+' channels found.');
            numbersAPIResp.data.items.forEach(item=>{
                item.value = item.id;
                if(UA_APP_UTILITY.fetchedSenders.hasOwnProperty(item.id)){
                    item.channel = UA_APP_UTILITY.MESSAGING_CHANNELS[item.slug.toUpperCase()];
                    UA_APP_UTILITY.fetchedSenders[item.id] = item;
                    return;
                }
                UA_APP_UTILITY.addMessaegSender(item.id, UA_APP_UTILITY.MESSAGING_CHANNELS[item.slug.toUpperCase()], item.name, item);
            });
            UA_APP_UTILITY.TPA_SENDERS_FETCH_COMPLETED();
//            var sendersArray = [];
//            UA_TPA_FEATURES.APP_SAVED_CONFIG.senders.split(',').forEach(item=>{
//                    sendersArray.push({
//                        'label': item,
//                        'value': item
//                    });
//            });
//            UA_APP_UTILITY.addMessaegSender('919994411345', UA_APP_UTILITY.MESSAGING_CHANNELS.SMS, "919994411345");
            
        //});
        
       
   
//        var fetchedTemplatesWAResponse = await UA_TPA_FEATURES._getServiceWATemplates(true);
//        console.log('wa fetchedtemoktes ', fetchedTemplatesWAResponse);
//         var waTemplatesArray = [];
//        fetchedTemplatesWAResponse.data.data.forEach(item=>{
//            UA_APP_UTILITY.fetchedTemplates[item.tempid] = {
//                'message': item.body_message,
//                'templateType': 'placeholder_template',
//                'placeholders': item.placeholders ? item.placeholders.split(',') : [],
//                'tempid': item.tempid,
//                'head_media_url': item.head_media_url,
//                'head_mediatype': item.head_mediatype
//            };
//            waTemplatesArray.push({
//                'label': item.temptitle + ' (WhatsApp)',
//                'value': item.tempid
//            });
//        });
//        
//        UA_APP_UTILITY.renderSelectableDropdown('#ssf-fitem-template-var-holder', 'Insert Pinnacle template', waTemplatesArray, 'UA_TPA_FEATURES.insertTemplateContentInMessageInput', false, false, approvedTemplateDDId);
        
        
        
    },
    
    actualOrgCommonSettingResolved: function(){
        UA_TPA_FEATURES.insertIncomingWD_supportedModules();
        UA_TPA_FEATURES.addNewWebhook();
    },
    
    fetchTemplatesAndRenderInBackground: async function(){
         var fetchedTemplatesResponse = await UA_TPA_FEATURES._getServiceTemplates();
        console.log('fetchedtemoktes ', fetchedTemplatesResponse);
        UA_TPA_FEATURES.showIfErrorInAPIResult(fetchedTemplatesResponse);
        let fetchedTemplatesCollection = fetchedTemplatesResponse.data;
        if(!fetchedTemplatesCollection){
            return false;
        }
        var templatesArray = [];
        fetchedTemplatesCollection.forEach(item=>{
            if(item.status !== "APPROVED"){
                return;
            }
            UA_APP_UTILITY.fetchedTemplates[item.name] = {
                'message': item.content,
                'id': item.name,
                "templateObj": item
            };
            UA_APP_UTILITY.fetchedTemplates[item.name+'-_-_-'+item.language] = {
                'message': item.content,
                'id': item.name+'-_-_-'+item.language,
                "templateObj": item
            };
            UA_TPA_FEATURES.getDetailedTemplateMessageDetailAndStore(item.name+'-_-_-'+item.language);
            templatesArray.push({
                'label': `${item.name} - ${item.language}`,
                'value': item.name+'-_-_-'+item.language,
                'templateType': "placeholder_template"
            });
        });
        
        var approvedTemplateDDId = UA_APP_UTILITY.renderSelectableDropdown('#ssf-fitem-template-var-holder', 'Insert WhatsApp template', templatesArray, 'UA_TPA_FEATURES.insertTemplateContentInMessageInput', false, false);
       
        var countryCallCodeArray = [];
        UA_APP_UTILITY.countryCallingCodeArray.forEach(item=>{
            countryCallCodeArray.push({
                'label': `${item.name} (${item.dial_code})`,
                'value': item.dial_code
            });
        });
        
        UA_APP_UTILITY.renderSelectableDropdown('#ssf-new-recip-countrycode', 'Select country', countryCallCodeArray, 'UA_APP_UTILITY.log');
    },
    
    appsConfigHasBeenResolved: function(){
        //UA_TPA_FEATURES.addNewWebhook();
    },
    
    renderWorkflowBodyCode :function(accessToken){
        UA_TPA_FEATURES.workflowCode.Channel_TEXT.ulgebra_webhook_authtoken = accessToken.saas;
        UA_TPA_FEATURES.workflowCode.Channel_MEDIA.ulgebra_webhook_authtoken = accessToken.saas;
        UA_TPA_FEATURES.workflowCode.WhatsApp_Template.ulgebra_webhook_authtoken = accessToken.saas;
        UA_TPA_FEATURES.workflowCode.WhatsApp_Media_Template.ulgebra_webhook_authtoken = accessToken.saas;
        
        UA_APP_UTILITY.addWorkflowBodyCode('channel_text', 'For Sending Text Message', UA_TPA_FEATURES.workflowCode.Channel_TEXT);
        
        UA_APP_UTILITY.addWorkflowBodyCode('channel_media', 'For Sending Attachment Message', UA_TPA_FEATURES.workflowCode.Channel_MEDIA);
        
        UA_APP_UTILITY.addWorkflowBodyCode('whatsapp_template', 'For Sending WhatsApp Template Message', UA_TPA_FEATURES.workflowCode.WhatsApp_Template);
        
        UA_APP_UTILITY.addWorkflowBodyCode('whatsapp_media_template', 'Sending WhatsApp Media Template Message', UA_TPA_FEATURES.workflowCode.WhatsApp_Media_Template);
    },
    
    insertTemplateContentInMessageInput :function(templateId){
        try{   
            UA_APP_UTILITY.removeCurrentUploadedAttachment();
        }catch(er){ console.log(er); }
        var curTemplateItem = UA_APP_UTILITY.fetchedTemplates[templateId];
        UA_TPA_FEATURES.chosenMessgeTemplate = curTemplateItem;
        $("#ssf-fitem-template-placeholder-holder-listholder").html("");
        var placeHolderItems = UA_TPA_FEATURES.getMatchingMsgTemplateList(curTemplateItem.message);
        curTemplateItem.placeholders = [];
        placeHolderItems.forEach(item => {
            item  = item.replace(/[^0-9]/g, '');
            curTemplateItem.placeholders.push(item);
            $("#ssf-fitem-template-placeholder-holder-listholder").append(`
                    <div class="ssf-temp-placehold-item">
                            <span class="ssf-temp-placehold-item-label">${item}</span> <span class="material-icons ssf-temp-placehold-item-icon">double_arrow</span> <input onblur="UA_APP_UTILITY.lastFocusedInputElemForPlaceholderInsert = 'ssf-templ-place-input-${item}'" id="ssf-templ-place-input-${item}" data-placeholder-id="${item}" class="ssf-temp-placehold-item-input" type="text" placeholder="Type or choose field from above">
                        </div>
                `);
        });
        if (curTemplateItem.placeholders.length > 0) {
            $("#ssf-fitem-template-placeholder-holder").show();
        } else {
            $("#ssf-fitem-template-placeholder-holder").hide();
        }
        $('#inp-ssf-main-message').val(curTemplateItem.message);
        let isContentOnlyTemplate = UA_TPA_FEATURES.chosenMessgeTemplate && UA_TPA_FEATURES.chosenMessgeTemplate.templateType !== undefined && UA_TPA_FEATURES.chosenMessgeTemplate.templateType === "content_only_template";
        if(templateId.indexOf("ua_msg_template_item") === 0 || isContentOnlyTemplate){
            $('#inp-ssf-main-message').removeAttr('readonly');
        }
        else{
            $('#inp-ssf-main-message').attr('readonly', true);
        }
        try{
            if(curTemplateItem && curTemplateItem.attachmentData && curTemplateItem.attachmentData.isForNewTemp){
                UA_APP_UTILITY.currentAttachedFile = curTemplateItem.attachmentData;
                $('.cht-attachmentNameHolder.inForm').css({'display': 'inline-block'});
                $('.cht-attachmentNameHolder.inForm .cht-attch-close').hide();
                $('.cht-attachmentNameHolder.inForm .cht-attch-prog').hide();
                $('.cht-attachmentNameHolder.inForm .cht-attch-status').text(getSafeString(curTemplateItem.attachmentData.fileMeta.name));
            }
        }
        catch(err){
            console.log(err);
        }
    },
    
    _getServiceTemplates: async function(){
        var templatesResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://integrations.messagebird.com/v1/public/whatsapp/templates", "get");
        if(templatesResponse.code === 401 || (templatesResponse.data && (templatesResponse.data.code === 401 || templatesResponse.data.message === "Authentication failed"))){
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return;
        }
        return  templatesResponse;
    },
    
    _getServiceWATemplates: async function(silenceAuthError = false){
        var templatesResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "/", "post");
        if(templatesResponse.code === 401 || templatesResponse.data.code === 401 || templatesResponse.data.message === "Authentication failed"){
            if(silenceAuthError){
                return {
                    data:{
                        data: []
                    }
                };
            }
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return;
        }
        return  templatesResponse;
    },
    
    getMatchingMsgTemplateList: function(messageText){
        var matches = messageText.match(/\{\{[A-Za-z0-9._\-]+\}\}*/g);
        return matches ? matches : [];
    },
    
    showReAuthorizeERROR: function(showCloseOption = false){
        if($('#inp_tpa_authtoken').length > 0){
            console.log('already showing reautherr');
            return;
        }
        showErroWindow("Authorization needed!", `${UA_LIC_UTILITY.getAskSharedAccessAdminTip()} You need to authorize your MessageBird Account to proceed. <br><br> 
        <div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
            
            <div style="margin-left:20px">
                <div style="font-size: 16px;margin-bottom: 5px;margin-top: 10px;color: rgb(80,80,80);font-weight:bold">MessageBird Live Access Key</div>
        <div class="help_apikey_tip"><a target="_blank" href="https://dashboard.messagebird.com/en/developers/access">Get Here</a></div>
                <input id="inp_tpa_authtoken" type="text" placeholder="Enter MessageBird AccessKey" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
            </div>
            
            <button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.saveTPAAPIKey(UA_APP_UTILITY.reloadWindow)">Authorize</button>
        </div>`, showCloseOption, false, 500);
        $('#inp_tpa_api_key').focus();
        UA_LIC_UTILITY.showExistingInvitedAdminDDHTML(true);
    },
    
    showReAuthorizeERRORWithClose: function(){
        UA_TPA_FEATURES.showReAuthorizeERROR();
    },
    
    saveTPAAPIKey: async function(callback){
        let authtoken = $("#inp_tpa_authtoken").val();
        if(!valueExists(authtoken)){
            showErroMessage("Please Enter MessageBird AccessKey");
            return;
        }
        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', 'authtoken', authtoken, callback);
    },
    
    saveTPAWABAAPIKey: async function(callback){
        let authtoken = $("#inp_tpa_waba_api_key").val();
        if(!valueExists(authtoken)){
            showErroMessage("Please Enter API Key");
            return;
        }
        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', 'waba_authtoken', authtoken, callback);
    },
    
    
    
    prepareAndSendSMS : function(){
        let isConversationMessage = (UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CONVERSATIONS || UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CHAT) && UA_APP_UTILITY.CURRENT_CONVERSATION_ID;
        if (!UA_APP_UTILITY.currentSender) {
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var senderId = UA_APP_UTILITY.currentSender.value;
        if (senderId === "_-_other_-_") {
            senderId = $('#DD_HOLDER_SENDER_LIST .dd-item-other-input').val().trim();
        }
        if (!valueExists(senderId)) {
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var currentConversation = UA_APP_UTILITY.FETCHED_CONVERSATIONS[UA_APP_UTILITY.CURRENT_CONVERSATION_ID];
        let recipNumbers = [];
        try{
            recipNumbers = UA_APP_UTILITY.getCurrentMessageRecipients();
            UA_SAAS_SERVICE_APP.recipNumbers = recipNumbers;
        }
        catch(ex){ console.log(ex); }
        if(recipNumbers.length === 0) {
            if(isConversationMessage){
                let convRecordId = currentConversation.saasModuleId;
                try{
                    if(!convRecordId && UA_SAAS_SERVICE_APP.widgetContext.entityId){
                        if(Array.isArray(UA_SAAS_SERVICE_APP.widgetContext.entityId)){
                            convRecordId = UA_SAAS_SERVICE_APP.widgetContext.entityId[0] 
                        }
                        else{
                            convRecordId = UA_SAAS_SERVICE_APP.widgetContext.entityId.split(',')[0];
                        }
                    }
                }
                catch(e){ console.log(e); }   
                recipNumbers.push({
                    id: convRecordId,
                    number: currentConversation.to
                });
            }
        }
        if (recipNumbers.length === 0) {
            showErroWindow('Recipient list is empty!', "Kindly add recipients to send SMS.");
            return false;
        }
        var messageText = $("#inp-ssf-main-message").val().trim();
        if (!valueExists(messageText)) {
            showErroWindow('Message is empty!', "Kindly fill message to proceed.");
            return false;
        }
        if(!UA_APP_UTILITY.fileUploadComplete){
            showErroWindow('File upload is not complete!', "Please wait while your file is uploading and try again.");
            return;
        }
        if(UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.MESSAGE_FORM){
            $('#sms-prog-window').show();
        }
        $('#msgItemsProgHolder').html('');
        var totalCount = recipNumbers.length;
        $('#totMsgDisp').text(`Sending messages 0/${totalCount}...`);
        var messagesSentCount = 0;
        var messageStatusMap = {
            'success': 0,
            'failed': 0
        };
        recipNumbers.forEach(item=>{
            let messageURL = null;
            if(UA_APP_UTILITY.currentAttachedFile && UA_APP_UTILITY.currentAttachedFile.mediaUrl){
                messageURL = UA_APP_UTILITY.currentAttachedFile.mediaUrl;
            }
            let contactRecordId = UA_APP_UTILITY.storedRecipientInventory[item.id] ? item.id : null;
            let resolvedMessageText = UA_APP_UTILITY.getTemplateAppliedMessage(messageText, UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id]);
            let parameterValues = [];
            if(UA_TPA_FEATURES.chosenMessgeTemplate && UA_TPA_FEATURES.chosenMessgeTemplate.placeholders){
                UA_TPA_FEATURES.chosenMessgeTemplate.placeholders.forEach(item=>{
                    let placeHolderValue = $('#ssf-templ-place-input-'+item).val();
                    //if(contactRecordId){
                        placeHolderValue = UA_APP_UTILITY.getTemplateAppliedMessage(placeHolderValue, UA_APP_UTILITY.storedRecipientInventory[contactRecordId]);
                    //}
                    parameterValues[(parseInt(item)-1)] = {
                        'default': placeHolderValue
                    };
                    resolvedMessageText = resolvedMessageText.replace('{{'+item+'}}', placeHolderValue);
                });
            }
            item.number = "+"+item.number.replace(/\D/g,'');
            var content = {
                "text": resolvedMessageText
            };
            if(UA_APP_UTILITY.currentSender.channel === UA_APP_UTILITY.MESSAGING_CHANNELS.WHATSAPP || UA_APP_UTILITY.currentSender.channel === UA_APP_UTILITY.MESSAGING_CHANNELS.WHATSAPP_SANDBOX) {
                if(resolvedMessageText.length > 2950 && messageURL){
                    UA_APP_UTILITY.currentAttachedExtraFiles[UA_APP_UTILITY.currentAttachedFile.randomFileRequestID] = UA_APP_UTILITY.currentAttachedFile;
                    UA_APP_UTILITY.currentAttachedFile = null;
                    messageURL = null;
                    console.log('Sending text separately since caption more than 3000');
                }
            }
            let type = "text";
            let isContentOnlyTemplate = UA_TPA_FEATURES.chosenMessgeTemplate && UA_TPA_FEATURES.chosenMessgeTemplate.templateType !== undefined && UA_TPA_FEATURES.chosenMessgeTemplate.templateType === "content_only_template";
            if((UA_APP_UTILITY.currentSender.channel === UA_APP_UTILITY.MESSAGING_CHANNELS.WHATSAPP || UA_APP_UTILITY.currentSender.channel === UA_APP_UTILITY.MESSAGING_CHANNELS.WHATSAPP_SANDBOX) && UA_TPA_FEATURES.chosenMessgeTemplate && UA_TPA_FEATURES.chosenMessgeTemplate.id.indexOf('ua_msg_template_')!==0 && !isContentOnlyTemplate){
                type = "hsm";
                let templateNameSpace = UA_APP_UTILITY.currentSender.settings.namespace;
                if(!templateNameSpace){
                    templateNameSpace = UA_TPA_FEATURES.chosenMessgeTemplate.templateObj.namespace;
                }
                let chosenTemplateName = UA_TPA_FEATURES.chosenMessgeTemplate.id;
                if(chosenTemplateName.indexOf("-_-_-") > 1){
                    chosenTemplateName = chosenTemplateName.split("-_-_-")[0];
                }
                content = {
                    "hsm": {
                        "namespace": templateNameSpace,
                        "templateName": chosenTemplateName,
                        "language": {
                            "policy": "deterministic",
                            "code": UA_TPA_FEATURES.chosenMessgeTemplate.templateObj.language
                        },
                        "params": parameterValues
                    }
                };
                if(UA_TPA_FEATURES.chosenMessgeTemplate.templateObj.attachment){
                    let attachmentType = UA_TPA_FEATURES.chosenMessgeTemplate.templateObj.attachment.type.toLowerCase();
                    let attachmentObj = {
                        'type': attachmentType
                    };
                    attachmentObj[attachmentType] =  {
                        "url": messageURL
                    };
                    let updatedParamValues = [];
                    parameterValues.forEach(paramItem=>{
                       updatedParamValues.push({
                           'type': 'text',
                           'text': paramItem.default
                       }); 
                    });
                    content.hsm.components = [
                        {
                            "type": "header",
                            "parameters": [attachmentObj]
                        },
                        {
                          "type": "body",
                          "parameters": updatedParamValues
                        }
                    ];
                    delete content.hsm.params;
                }
            }
            else{
                if (messageURL) {
                    let fileMeta = UA_APP_UTILITY.currentAttachedFile.fileMeta;
                    var types = ["image", "audio", "video"];
                    type = fileMeta.type.substring(0, fileMeta.type.indexOf("/"));
                    if (!types.includes(type)) {
                        type = "file";
                    }
                    content[type] = {"url": messageURL};
                    if(valueExists(resolvedMessageText)){
                        content[type].caption = resolvedMessageText;
                    }
                }
            }
            let messagePayload = {
                "channelId": senderId,
                "to": item.number,
                "type": type,
                "content": content,
                "source": {
                    "type": "uaapp",
                    "email": currentUser.email,
                    "name": currentUser.displayName,
                    "id": currentUser.uid,
                    "pic": firebase.auth().currentUser.photoURL ? firebase.auth().currentUser.photoURL : '',
                    "uaApp": extensionName,
                    "uaAppSaaSOrgID": appsConfig.UA_DESK_ORG_ID,
                    "uaAppSaaSUserId": UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.id ? UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.id : ''
                }
            };
            
            if(UA_APP_UTILITY.isConvChatView() && UA_APP_UTILITY.CURRENT_CONVERSATION_ID!==null){
                delete messagePayload.to;
                UA_TPA_FEATURES.FETCHED_CONVERSATIONS[UA_APP_UTILITY.CURRENT_CONVERSATION_ID].lastUsedChannelId = senderId;
            }
            
            if(!contactRecordId && UA_APP_UTILITY.isConvChatView()){
                let fetchedKeys = Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS);
                contactRecordId = fetchedKeys.length === 1 ? fetchedKeys[0] : null;
            }
            let messageHistoryMap = {
                'contact':{
                    'name': UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].Full_Name : item.number
                },
                'from': UA_APP_UTILITY.currentSender.channel.id === "WHATSAPP" ? UA_APP_UTILITY.currentSender.settings.msisdn : UA_TPA_FEATURES.FETCHED_CHANNELS[senderId].name+' - '+UA_TPA_FEATURES.FETCHED_CHANNELS[senderId].slug.toUpperCase(),
                'to': item.number,
                'message': resolvedMessageText,
                'status': 'NOT_SENT',
                'module': UA_SAAS_SERVICE_APP.widgetContext.module,
                'moduleId': item.id,
                'channel': UA_APP_UTILITY.currentSender.channel.label
            };
            if(UA_APP_UTILITY.isFormView()){
                $('#msgItemsProgHolder').append(`
                  <div id="msg-resp-item-${parseInt(item.number)}" class="msgRespItem" title="${getSafeString(resolvedMessageText)}">
                      <div class="mri-label"><b>${$('.msgRespItem').length+1}</b>. WhatsApp to ${UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].name+'-' : ''} ${item.number}</div>
                      <div class="mri-status">Sending...</div>
                  </div>
              `);
            }
            UA_TPA_FEATURES._proceedToSendWAAndExecuteCallback(messagePayload, function(response){
                if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
                   return false;
               }
               if(Object.keys(UA_APP_UTILITY.currentAttachedExtraFiles).length === 0){
                    UA_APP_UTILITY.resetMessageFormFields();
               }
               else{
                    if(UA_APP_UTILITY.currentAttachedFile){
                        UA_APP_UTILITY.removeCurrentUploadedAttachment(UA_APP_UTILITY.currentAttachedFile.randomFileRequestID);
                    }
                        let nextFileKey = Object.keys(UA_APP_UTILITY.currentAttachedExtraFiles)[0];
                        let nextFileObj = UA_APP_UTILITY.currentAttachedExtraFiles[nextFileKey];
                        UA_APP_UTILITY.currentAttachedFile = nextFileObj;
                        UA_TPA_FEATURES.chosenMessgeTemplate = null;
                        $("#inp-ssf-main-message").val(UA_APP_UTILITY.currentAttachedFile.fileMeta.name);
                        delete UA_APP_UTILITY.currentAttachedExtraFiles[nextFileKey];
                        UA_TPA_FEATURES.prepareAndSendSMS();
                    
               }
                console.log(response);
                let isError = response.error;
                let errorMessage = isError ? response.error.errors[0].description : '';
                if(isError && UA_APP_UTILITY.isConvChatView()){
                    showErroWindow('Error from MessageBird', errorMessage);
                    return false;
                }
                let pos = Number(item.id);
            

                messagesSentCount++;
                $(`#msg-resp-item-${parseInt(item.number)} .mri-status`).html(isError ? ('Unable to send message. '+errorMessage): UA_APP_UTILITY.currentSender.channel.label+' Message Sent.').css({'color': isError ? 'crimson' : 'green'});
                isError ?  messageStatusMap.failed++ : messageStatusMap.success++;
                if(messagesSentCount === totalCount){
                    $('#totMsgDisp').text(`All messages have been processed. TOTAL: ${totalCount}, SENT: ${messageStatusMap.success}, FAILED: ${messageStatusMap.failed}`);
                }else{
                    $('#totMsgDisp').text(`Sending messages ${messagesSentCount}/${totalCount}...`);
                }
                
                messageHistoryMap.status = isError ? "FAILED - "+errorMessage : "SUCCESS" ;
                messageHistoryMap.statusText = isError ? "FAILED" : "SUCCESS" ;
                messageHistoryMap.errorMessage = isError ? errorMessage : "" ;
                messageHistoryMap.index = pos;
                messageHistoryMap.isError = isError;

                UA_SAAS_SERVICE_APP.addSentSMSAsRecordInHistory({
                    "message1": messageHistoryMap,
                });
            });
        });
        
        
    },
    
    sendSMS : function(sender, number, text, callback){
        
        var messageArray = [{
            "number": number,
            "text": text
        }];
        
        var messagePayload = {
            "sender": sender,
            "message": messageArray,
            "messagetype": "TXT"
        };
        
        UA_TPA_FEATURES._proceedToSendSMSAndExecuteCallback(messagePayload, callback);
        
    },
    
    _proceedToSendWAAndExecuteCallback: async function(messagePayload, callback){
        if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        var sentAPIResponse = null;
        if(UA_APP_UTILITY.CURRENT_CONVERSATION_ID){
            sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://conversations.messagebird.com/v1/conversations/${UA_APP_UTILITY.CURRENT_CONVERSATION_ID}/messages`, "POST", messagePayload);
        }
        else{
            sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://conversations.messagebird.com/v1/conversations/start`, "POST", messagePayload);
        }
        callback(sentAPIResponse);
    },
    
    getAPIResponse: async function (extensionName, url, method, data) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = {'content-type': 'application/json'};
        await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, TPAService: "messagebird",url: url, method: method, data: data, headers: headers, _ua_lic_adminUserID: UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID}).then((response) => {
            response = response.data;
            returnResponse = response;
            removeTopProgressBar(credAdProcess3);
            return true;
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
        return returnResponse;
    },
    
    getCurrentAccountOrBalanceInfo: async function(callback){
        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://rest.messagebird.com/balance", "GET", {});
        if((response.code === 401 || response.message === "Authentication failed") || (response.error && response.error.code === 401) || (response.data && (response.data.code === 401 || response.data.message === "Authentication failed"))){
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return;
        }
        callback(response);
    },
    EXISTING_WEBHOOK_ID: null,
    isWebhookAlreadyExists: async function(){
        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://conversations.messagebird.com/v1/webhooks", "GET", {});
        if(UA_TPA_FEATURES.showIfErrorInAPIResult(response)){
            return;
        }
        let existingServiceID = null;
        response.data.items.forEach((item)=>{
            let url = item.url;
            if(url && url.startsWith(UA_APP_UTILITY.getWebhookBaseURL())){
                existingServiceID = item.id;
            }
        });
        UA_TPA_FEATURES.EXISTING_WEBHOOK_ID = existingServiceID;
        UA_TPA_FEATURES.incomingCaptureStatusChanged();
        UA_TPA_FEATURES.serviceSID = existingServiceID;
        return existingServiceID !== null;
    },
    
    showIfErrorInAPIResult: function(response){
        console.log(response);
        if((response.code === 401 || response.message === "Authentication failed") || (response.error && response.error.code === 401) || (response.data && (response.data.code === 401 || response.data.message === "Authentication failed"))){
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return true;
        }
        if(response.error && response.error.errors && response.error.errors[0] && response.error.errors[0].description){
            let errorTitle = `Error from MessageBird!`;
            if(response.error.errors[0].code === 21){
                    errorTitle = 'Incoming messages sync error!';
                    response.error.errors[0].description += `.<br><br> Kindly delete unused webhooks below and refresh this page. Until then incoming messages will not sync in this integration<br><br><a href="https://ulgebra.com/messagebird-cliq-help" target="_blank" style="
    margin-top: 15px;
    display: inline-block;
    background-color: crimson;
    color: white;
    padding: 4px 10px;
    border-radius: 4px;
">
    Delete Unwanted Webhooks Now</a> <a onclick="window.location.reload()" href="#" style="
    margin-top: 15px;
    display: inline-block;
    background-color: royalblue;
    color: white;
    padding: 4px 10px;
    border-radius: 4px;
">
    I have deleted, retry now!</a> `;
            }
            showErroWindow(errorTitle, response.error.errors[0].description);
            return true;
        }
        else{
            if(response.error && response.error.message){
                showErroWindow('Error from messagebird', response.error.message);
            }
        }
        return false;
    },
    
    showNumbersFromService: function(){
        
    },
    
    addNewWebhook: async function(){
        if(!UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS){
            UA_APP_UTILITY.CALL_AFTER_PERSONAL_COMMON_SETTING_RESOLVED.push(UA_TPA_FEATURES.addNewWebhook);
            return false;
        }
        if(UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.hasOwnProperty('ua_incoming_enable_capture_'+currentUser.uid) && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS['ua_incoming_enable_capture_'+currentUser.uid] === false){
            return;
        }
        if(UA_APP_UTILITY.PERSONAL_WEBHOOK_TOKEN && appsConfig.UA_DESK_ORG_ID){
            var webhookExists = await UA_TPA_FEATURES.isWebhookAlreadyExists();
            if(!webhookExists){
                var webhookData = {
                        "events": ["message.created", "message.updated"],
                        "url": UA_APP_UTILITY.getAuthorizedWebhookURL()
                    };
                var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://conversations.messagebird.com/v1/webhooks", "POST", webhookData);
                if(UA_TPA_FEATURES.showIfErrorInAPIResult(response)){
                    return false;
                }
                UA_TPA_FEATURES.serviceSID = response.data.id;
                UA_TPA_FEATURES.EXISTING_WEBHOOK_ID = UA_TPA_FEATURES.serviceSID;
                UA_TPA_FEATURES.incomingCaptureStatusChanged();
                UA_TPA_FEATURES.showIncomingWebhookDialog();
                return response.data && (response.data.id !== null);
            }
            return true;
        }
        return false;
    },
    
    fetchConversationsAndAddToChatBox: async function(number, autoLoadMessagesIfSingle = false){
        if(!number){
            number = UA_APP_UTILITY.chatCurrentRecipient ? UA_APP_UTILITY.chatCurrentRecipient.address : null;
        }
        if(!number){
            return false;
        }
        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://contacts.messagebird.com/v2/contacts?identifierExact="+parseInt(number), "get", {});
        if(!response.data.items){
            return false;
        }
        response.data.items.forEach(async contactItem => {
            let contactConversationResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://conversations.messagebird.com/v1/conversations/contact/"+contactItem.id, "get", {});
            if(contactConversationResponse && contactConversationResponse.data.count > 0){
                if(contactConversationResponse.data.count === 1){
                    let contactConversationFetchComplete = await UA_TPA_FEATURES.getConversationsList("&ids="+contactConversationResponse.data.items.join(','), autoLoadMessagesIfSingle);
                }
                else{
                        contactConversationResponse.data.items.forEach(convIdItem=>{
                            UA_TPA_FEATURES.getConversationsList("/"+convIdItem, false);
                        });
                        UA_APP_UTILITY.changeCurrentView('CONVERSATIONS', false);
                }
            }
        });
    },
    
    newRTEventReceived: function(CID, MID){
        if(!UA_APP_UTILITY.FETCHED_MESSAGES[MID.a]){
            UA_TPA_FEATURES.fetchMessageByIDAndAddToChatBox(CID, MID.a);
        }
    },
    fetchMessageByIDAndAddToChatBox: async function(convID, messageID){
       if(convID === UA_APP_UTILITY.CURRENT_CONVERSATION_ID){
            var messageAPI = `https://conversations.messagebird.com/v1/messages/${messageID}`;
            var resp = await UA_TPA_FEATURES.getAPIResponse(extensionName, messageAPI, "GET");
            if(resp.error){
                console.log('Unable to fetch messagbird message', resp.error.message);
            }
            resp.data.isUnRead = true;
            resp.data.conversationID = convID;
            UA_APP_UTILITY.addMessageInBox(UA_TPA_FEATURES.convertMessageToUASchema(resp.data), true);
        }
        else{
            console.log('ignoring new message since current conv id is different');
        }
    },
    
    LOCAL_MESSAGE_STORAGE: {},
    
    getDetailedTemplateMessageDetailAndStore: async function(templateName){
            let queryTemplateName = templateName;
            if(templateName.indexOf('-_-_-') > 1){
                queryTemplateName = queryTemplateName.split('-_-_-')[0];
            }
            var mediaAPIResp = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://integrations.messagebird.com/v2/platforms/whatsapp/templates/"+ queryTemplateName, "GET");
            let templateLanguage = UA_APP_UTILITY.fetchedTemplates[templateName].templateObj.language;
            let templateID = UA_APP_UTILITY.fetchedTemplates[templateName].templateObj.id;
            UA_APP_UTILITY.fetchedTemplates[templateName].templateObj = mediaAPIResp.data[0];
            UA_APP_UTILITY.fetchedTemplates[templateName].templateObj.language = templateLanguage;
            UA_APP_UTILITY.fetchedTemplates[templateName].templateObj.id = templateID;
            let attachObj = null;
            UA_APP_UTILITY.fetchedTemplates[templateName].templateObj.components.forEach(compItem=>{
                if(compItem.type === "HEADER" && ["IMAGE", "VIDEO", "DOCUMENT"].includes(compItem.format)){
                    attachObj = {
                        type: compItem.format
                    }
                }
            });
            UA_APP_UTILITY.fetchedTemplates[templateName].templateObj.attachment = attachObj;
            UA_TPA_FEATURES.cacheWATemplateIfNotPresentInOrgSetting(templateName, UA_APP_UTILITY.fetchedTemplates[templateName].templateObj);
    },
    
    convertMessageToUASchema: function(msgObj){
        try{
        var messageChannel = UA_APP_UTILITY.MESSAGING_CHANNELS[msgObj.platform.toUpperCase()];
        var attachmentsArr = [];
        var msgType = msgObj.type;
        var content = msgObj.content.text ? msgObj.content.text : '';
        if (msgType !== "text") {
            if (msgType === "location") {
                content = "http://maps.google.com/?ll=" + msgObj.content[msgType].latitude + "," + msgObj.content[msgType].longitude;
            }
            else if (msgType === "hsm") {
                let templateNameRaw = msgObj.content.hsm.templateName;
                if(UA_APP_UTILITY.fetchedTemplates[msgObj.content.hsm.templateName]){
                    let templateLanguage = msgObj.content.hsm.language.code;
                    if(templateLanguage){
                        let updatedTemplateName = templateNameRaw+'-_-_-'+templateLanguage;
                        if(UA_APP_UTILITY.fetchedTemplates.hasOwnProperty(updatedTemplateName)){
                            msgObj.content.hsm.templateName = updatedTemplateName;
                        }
                    }
                }
                content = (UA_APP_UTILITY.fetchedTemplates[msgObj.content.hsm.templateName] ? UA_APP_UTILITY.fetchedTemplates[msgObj.content.hsm.templateName].message : `TEMPLATE_NOT_FOUND : Template with name ${msgObj.content.hsm.templateName} does not exist in your messagebird account.`);
                if(msgObj.content.hsm.params){
                    let i = 1;
                    msgObj.content.hsm.params.forEach(paramItem=>{
                        let paramValue = paramItem.default;
                        if(!UA_APP_UTILITY.fetchedTemplates[msgObj.content.hsm.templateName]){
                            content+=` PARAM-${i}: {{${i}}}`;
                        }
                        content = content.replaceAll('{{'+i+'}}', paramValue);
                        i++;
                    });
                }
                if(msgObj.content.hsm.components) {
                    let headerParam = null;
                    let bodyParam = null;
                    msgObj.content.hsm.components.forEach(compItem=>{
                        if(compItem.type === "header"){
                            headerParam = compItem;
                        }
                        if(compItem.type === "body"){
                            bodyParam = compItem;
                        }
                    });
                    if (headerParam && headerParam.parameters) {
                        headerParam.parameters.forEach(paramItem => {
                            let url = paramItem[paramItem.type].url;
                            attachmentsArr.push({
                                url: url,
                                type: paramItem.type
                            });
                        });
                    }
                    let i = 1;
                    if(bodyParam && bodyParam.parameters) {
                        bodyParam.parameters.forEach(paramItem => {
                            if(paramItem.type === "text"){
                                let paramValue = paramItem.text;
                                if(!UA_APP_UTILITY.fetchedTemplates[msgObj.content.hsm.templateName]){
                                    content+=` PARAM-${i}: {{${i}}}`;
                                }
                                content = content.replaceAll('{{'+i+'}}', paramValue);
                                i++;
                            }
                        });
                    }
                }
            }
            else {
                if (["image", "video", "audio", "file"].includes(msgType)) {
                    attachmentsArr.push({
                        'url' :msgObj.content[msgType].url,
                        'type': msgType
                    });
                }
                if (msgObj.content[msgType] && msgObj.content[msgType].caption) {
                    content += " " + msgObj.content[msgType].caption;
                }
            }
        }
        let authorObj = null;
        try{
            if(msgObj.source && msgObj.source.type === "uaapp"){
                authorObj = msgObj.source;
            }
        }
        catch(e){ console.log(e);}
        var modifiedMsgObj = {
            'id': msgObj.id,
            'text': content,
            'channel': messageChannel,
            'isIncoming': msgObj.direction === "received",
            'from': msgObj.from,
            'to': msgObj.to,
            'createdTime': msgObj.createdDatetime,
            'status': msgObj.status,
            'isUnRead': msgObj.isUnRead,
            'attachments': attachmentsArr,
            'conversationID': msgObj.conversationID,
            'author': authorObj
        };
        if(msgObj.error && msgObj.error.description){
            modifiedMsgObj.error = {
                'message': msgObj.error.description
            };
        }
        //this.addMsgMediaURLAfterResolving(msgObj);
        return modifiedMsgObj;
        }catch(ex){
            console.log(ex);
        }
    },
    
    getSafeStringForChat: function (rawStr) {
        return getSafeString(rawStr);
//        if (!rawStr || rawStr.trim() === ""){
//            return "";
//        }
//
//        let urlRegex = /(https?:\/\/[^\s]+)/g;
//        let phoneNum = /\s*(?:\+?(\d{1,3}))?([- (]*(\d{3})[- )]*)?((\d{3})[- ]*(\d{2,4})(?:[-x ]*(\d+))?)/gm;
//        let urlArr = [];
//
//        return $('<textarea/>').text(rawStr).html().replace(urlRegex, function (url) {
//            urlArr.push(url);
//            return `${ `<a href="${url}" target="_blank" class="${'addChatUrl'}">${url}</a>` }`;
//        }).replace(phoneNum, function (num) {
//            let number = num + "";
//
//            let itIsUrl = false;
//            if (number.length >= 6 && number.length <= 15) {
//                urlArr.forEach(function (url) {
//                    if (url.includes(number)) {
//                        itIsUrl = true;
//                    }
//                });
//
//                if (itIsUrl) {
//                    return num;
//                } else
//                    return ``;
//            } else
//                return num;
//        });
    },
    showIncomingWebhookDialog: function(){
        $("#error-window-tpa_service_senders").show();
    },
    insertIncomingWD_supportedModules: function(){
        UA_APP_UTILITY.insertIncomingWD_supportedModules();
    },
    incomingCaptureStatusChanged: function(){
        if(UA_TPA_FEATURES.EXISTING_WEBHOOK_ID){
            document.getElementById('tpa-switch-incoming-enable-capture').checked = true;
             $('.tpaichan-devider').fadeIn();
        }
        else{
            document.getElementById('tpa-switch-incoming-enable-capture').checked = false;
            $('.tpaichan-devider:not(#master-tpai-incom-chan-item)').fadeOut();
        }
    },
    insertIncomingWD_channels: function(){
        if (!UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS) {
            UA_APP_UTILITY.CALL_AFTER_PERSONAL_COMMON_SETTING_RESOLVED.push(() => {
                UA_TPA_FEATURES.insertIncomingWD_channels();
            });
            return;
        }
        UA_TPA_FEATURES.refreshChannelsDataInCache();
        let supportedChannelDD = ``;
        for(var i in UA_TPA_FEATURES.FETCHED_CHANNELS){
            let ignored = false;
            let chanItem = UA_TPA_FEATURES.FETCHED_CHANNELS[i];
            if (UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.hasOwnProperty('ua_incoming_tpa_ignored_channels')) {
                ignored = UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.ua_incoming_tpa_ignored_channels.includes(chanItem.id);
            }
            supportedChannelDD += `<div class="tpaichan-item"> <label class="switch"><input ${!ignored ? 'checked' : ''} onchange="UA_TPA_FEATURES.toggleIncomingChannelConfig('${chanItem.id}')" type="checkbox" id="tpa-switch-incoming-channel-${chanItem.id}"><div class="slider round"></div></label> <span class="tpaichani-name">${'<b>'+chanItem.name+'</b>'+ ' - '+ chanItem.slug}</span> </div>`;
        }
        $('#error-window-tpa_service_senders #incoming-channel-config-item').html(supportedChannelDD);
    },
    
    toggleIncomingChannelConfig: function(chanId){
        if(!UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.ua_incoming_tpa_ignored_channels){
            UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.ua_incoming_tpa_ignored_channels = [];
        }
        if(UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.ua_incoming_tpa_ignored_channels.includes(chanId)){
            UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.ua_incoming_tpa_ignored_channels.remove_by_value(chanId);
        }
        else{
            UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.ua_incoming_tpa_ignored_channels.push(chanId);
        }
        UA_APP_UTILITY.saveSettingInCredDoc('ua_incoming_tpa_ignored_channels', UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.ua_incoming_tpa_ignored_channels);
    },
    
    addNewConversationByIDInConversationBox: async function(cid, mid){
        if(UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CONVERSATIONS){
            if(UA_APP_UTILITY.FETCHED_CONVERSATIONS.hasOwnProperty(cid)){
                if(mid){
                    var messageAPI = "https://conversations.messagebird.com/v1/messages/"+mid;
                    var resp = await UA_TPA_FEATURES.getAPIResponse(extensionName, messageAPI, "GET");
                    let convertedMsg = UA_TPA_FEATURES.convertMessageToUASchema(resp.data);
                    UA_APP_UTILITY.FETCHED_CONVERSATIONS[cid].message = convertedMsg.text;
                    UA_APP_UTILITY.FETCHED_CONVERSATIONS[cid].time = new Date(resp.data.createdDatetime).getTime();
                    UA_APP_UTILITY.FETCHED_CONVERSATIONS[cid].channel = UA_APP_UTILITY.MESSAGING_CHANNELS[resp.data.platform.toUpperCase()];
                    UA_TPA_FEATURES.FETCHED_CONVERSATIONS[cid].lastUsedChannelId  = resp.data.channelId;
                    UA_TPA_FEATURES.FETCHED_CONVERSATIONS[cid].channel  = UA_APP_UTILITY.FETCHED_CONVERSATIONS[cid].channel;
                    let convChannel = UA_TPA_FEATURES.FETCHED_CHANNELS[resp.data.channelId];
                    UA_APP_UTILITY.FETCHED_CONVERSATIONS[cid].from = convChannel.name + ' - '+ convChannel.slug.toUpperCase();
                    UA_APP_UTILITY.addConversationItemToConversationBox(UA_APP_UTILITY.FETCHED_CONVERSATIONS[cid]);
                    UA_TPA_FEATURES.fetchMessageByIDAndAddToChatBox(cid, mid);
                }
            }
            else{
                var messageAPI = "https://conversations.messagebird.com/v1/conversations/"+cid;
                var resp = await UA_TPA_FEATURES.getAPIResponse(extensionName, messageAPI, "GET");
                let convItem = UA_TPA_FEATURES.convertConvItemToUAConversationSchema(resp.data);
                UA_APP_UTILITY.addConversationItemToConversationBox(convItem);
            }
        }
    },
    FETCHED_CONVERSATIONS : {},
    getConversationsList: async function(apiParams = '&limit=20', autoLoadMessagesIfSingle = false){
        UA_APP_UTILITY.LOCAL_FETCHED_CONVERSATION_IDS = [];
        let isSingleConvByIDRequest = apiParams.startsWith('/');
        var messageAPI = isSingleConvByIDRequest ? "https://conversations.messagebird.com/v1/conversations"+apiParams :  "https://conversations.messagebird.com/v1/conversations?"+apiParams;
        var resp = await UA_TPA_FEATURES.getAPIResponse(extensionName, messageAPI, "GET");
        if(isSingleConvByIDRequest){
            resp.data = {
                'items': [resp.data]
            };
        }
        if(resp.data.items.length === 0){
            return false;
        }
        resp.data.items.forEach(conversationItem=>{
            let convItem = UA_TPA_FEATURES.convertConvItemToUAConversationSchema(conversationItem);
            UA_TPA_FEATURES.FETCHED_CONVERSATIONS[convItem.id] = conversationItem;
            UA_APP_UTILITY.addConversationItemToConversationBox(convItem);
        });
        if(autoLoadMessagesIfSingle && resp.data.items.length === 1){
            UA_APP_UTILITY.changeCurrentView(UA_APP_UTILITY.appPrefinedUIViews.CHAT.value, false);
        }
        if(autoLoadMessagesIfSingle && resp.data.items.length <= 1){
            UA_APP_UTILITY.selectConversationToLoad(resp.data.items[0].id);
        }
        else{
            if(autoLoadMessagesIfSingle){
                if(UA_APP_UTILITY.isChatView()){
                    UA_APP_UTILITY.changeCurrentView(UA_APP_UTILITY.appPrefinedUIViews.CONVERSATIONS.value, false);
                }
            }
        }
        return true;
    },
    convertConvItemToUAConversationSchema: function(convItem){
        let channel = UA_APP_UTILITY.MESSAGING_CHANNELS[convItem.lastUsedPlatformId.toUpperCase()];
        //messageItem.from = messageItem.from.replace("whatsapp:", '');
        //messageItem.to = messageItem.to.replace("whatsapp:", '');
        let convMsg = convItem.messages.totalCount+ " messages from";
        convItem.channels.forEach(chanItem=>{
            convMsg+=" "+chanItem.name;
        });
        let convName = "";
        if(convItem.contact.displayName){
            convName = convItem.contact.displayName;
        }
        else{
            if(convItem.contact.lastName){
                convName = convItem.contact.lastName;
            }
            if(convItem.contact.firstName){
                convName += " "+convItem.contact.lastName;
            }
        }
        if(!convName && convItem.contact.msisdn){
            convName +=  " +"+convItem.contact.msisdn;
        }
        let convChannel = UA_TPA_FEATURES.FETCHED_CHANNELS[convItem.lastUsedChannelId];
        convName += " - "+convChannel.name;
        return {
            'id': convItem.id,
            'name':  convName,
            'status': "received",
            'channel': channel,
            'message': convMsg,
            'time': new Date(convItem.lastReceivedDatetime ? convItem.lastReceivedDatetime : convItem.updatedDatetime).getTime(),
            'from': convChannel.name + ' - '+ convChannel.slug.toUpperCase(),
            'to': convItem.contact.msisdn ? "+"+convItem.contact.msisdn : convName
        };
    },
    loadConversationInChatBox: function(convID, refreshConversationList = true){
        if(!UA_TPA_FEATURES.FETCHED_CONVERSATIONS.hasOwnProperty(convID)){
            UA_TPA_FEATURES.getConversationsList("/"+convID, true);
            return false;
        }
        UA_APP_UTILITY.chooseMessageSender(UA_TPA_FEATURES.FETCHED_CONVERSATIONS[convID].lastUsedChannelId, refreshConversationList);
        $("#DD_HOLDER_SENDER_LIST .dropdownDisplayText").text(UA_APP_UTILITY.FETCHED_CONVERSATIONS[convID].from);
        $("#DD_HOLDER_RECIPIENT_LIST .dropdownDisplayText").text(UA_APP_UTILITY.FETCHED_CONVERSATIONS[convID].to);
        $('.ua-conv-list-item.selected').removeClass('selected');
        $('#CONV-ITEM-'+UA_APP_UTILITY.getCleanStringForHTMLAttribute(convID)).addClass('selected');
        $("#primary-send-btn").html(`<span class="material-icons ssf-fitem-ttl-icon">send</span> Send ${getSafeString(UA_APP_UTILITY.FETCHED_CONVERSATIONS[convID].channel.label)}`);
        $("#messageAttachmentInputHolder").toggle(UA_APP_UTILITY.FETCHED_CONVERSATIONS[convID].channel.attachmentSupported);
        $('.messageActionFooter').show();
        UA_APP_UTILITY.refreshMessageBox();
    },
    
    messageChannelSelected: function(){
        
    },
    
    messageSenderSelected: function(refreshConversationList = true){
        if (UA_APP_UTILITY.isChatView()) {
            UA_APP_UTILITY.refreshMessageBox();
        }
        if (UA_APP_UTILITY.isConvView() && refreshConversationList) {
            UA_APP_UTILITY.refreshConversationBox();
        }
    },
    
    performConversationSearchOnNumber: function(val){
        if(!valueExists(val)){
            return;
        }
        $("#DD_HOLDER_RECIPIENT_LIST .dropdownDisplayText").text(getSafeString(val));
        UA_APP_UTILITY.chatCurrentRecipient = {
            'channel': UA_APP_UTILITY.MESSAGING_CHANNELS.SMS,
            'address': val,
            'label': val,
            'value': val
        };
        $("#uaCHATConvListHolder").html("");
        $("#chatbox-message-holder").html("");
        UA_TPA_FEATURES.fetchConversationsAndAddToChatBox(val);
    },
     
    fetchMessagesAndAddToChatBox: async function(params = '&limit=20'){
        if (!UA_APP_UTILITY.CURRENT_CONVERSATION_ID) {
            if(UA_APP_UTILITY.chatCurrentRecipient){
                $("#uaCHATConvListHolder").html("");
                $("#chatbox-message-holder").html("");
                UA_TPA_FEATURES.fetchConversationsAndAddToChatBox(UA_APP_UTILITY.chatCurrentRecipient.address, true);
                return;
            }
        }
        if(!UA_APP_UTILITY.CURRENT_CONVERSATION_ID){
            console.log('no current conv id found');
            return false;
        }
       var messagesAPI = `https://conversations.messagebird.com/v1/conversations/${UA_APP_UTILITY.CURRENT_CONVERSATION_ID}/messages?${params}`;
       var resp = await UA_TPA_FEATURES.getAPIResponse(extensionName, messagesAPI, "GET");
       UA_APP_UTILITY.initiateRTListeners(UA_APP_UTILITY.CURRENT_CONVERSATION_ID);
       $("#chatbox-message-holder").html("");
       UA_TPA_FEATURES.messageDirectionsFetched = 0;
       UA_TPA_FEATURES.LOCAL_MESSAGE_STORAGE = {};
       let isFindLastRecievedMsg = false;
       resp.data.items.forEach((msgItem)=>{
           if(msgItem.type === "event"){
               return;
           }
            let convertedMsgItem = UA_TPA_FEATURES.convertMessageToUASchema(msgItem);
            convertedMsgItem.conversationID = UA_APP_UTILITY.CURRENT_CONVERSATION_ID;
            UA_APP_UTILITY.addMessageInBox(convertedMsgItem, false);
        });
        UA_APP_UTILITY.scrollChatBoxToLatest();
    },
    
    fetchConversationsForAllCurrentRecipients: async function(){
        if(UA_APP_UTILITY.getCurrentMessageRecipients(true).length === 0){
            return false;
        }
        $("#uaCHATConvListHolder").html("");
        UA_APP_UTILITY.getCurrentMessageRecipients(true).splice(0,5).forEach(recipItem=>{
            UA_TPA_FEATURES.fetchConversationsAndAddToChatBox(recipItem.number);
        });
    },
    
    getMessageContentAndAttachmentForMessageBird: function (messagePayload) {
        let attachments = [];
        let messageText = "";
        try {
            if (messagePayload.type === "text") {
                messageText = messagePayload.content.text + ' ';
            } else if (messagePayload.type === "hsm") {
                messageText = ' WhatsApp Template Name: ' + messagePayload.content.hsm.templateName + ' ';
                if (messagePayload.content.hsm.components) {
                    let headerParam = messagePayload.content.hsm.components[0];
                    let bodyParam = messagePayload.content.hsm.components[1];
                    if (headerParam) {
                        headerParam.parameters.forEach(paramItem => {
                            let url = paramItem[paramItem.type].url;
                            messageText += ' ' + paramItem.type + ' : ' + url;
                            attachments.push({
                                url: url,
                                type: paramItem.type
                            });
                        });
                    }
                    if (bodyParam) {
                        let paramId = 1;
                        bodyParam.parameters.forEach(paramItem => {
                            messageText += ` PARAM {{${paramId}}}: ${paramItem.default} `;
                            paramId++;
                        });
                    }
                }
                if (messagePayload.content.hsm.params) {
                    let paramId = 1;
                    messagePayload.content.hsm.params.forEach(paramItem => {
                        messageText += ` PARAM {{${paramId}}}: ${paramItem.default} `;
                        paramId++;
                    });
                }
            } else if (messagePayload.type !== "text") {
                let url = messagePayload.content[messagePayload.type].url;
                attachments.push({
                    url: url,
                    type: messagePayload.type
                });
                let caption = messagePayload.content[messagePayload.type].caption;
                messageText += ' ' + messagePayload.type + ' : ' + url + ' ';
                if (caption) {
                    messageText += ' ' + caption + ' ';
                }
            }
        } catch (ex) {
            console.log(JSON.stringify(ex));
        }
        if (messageText) {
            messageText = messageText.trim();
        }
        return {
            "text": messageText,
            "attachments": attachments
        };
    },
    
    deleteExistingWebhook: async function(){
        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, 'https://conversations.messagebird.com/v1/webhooks/'+UA_TPA_FEATURES.EXISTING_WEBHOOK_ID, "DELETE", {});
        if (UA_TPA_FEATURES.showIfErrorInAPIResult(response)){
            return false;
        }
        UA_TPA_FEATURES.EXISTING_WEBHOOK_ID = null;
        UA_TPA_FEATURES.incomingCaptureStatusChanged();
        return true;
    },
    
    refreshChannelsDataInCache: async function(){
         for(var i in UA_TPA_FEATURES.FETCHED_CHANNELS){
            let chanItem = UA_TPA_FEATURES.FETCHED_CHANNELS[i];
            if(chanItem.slug === "livechat" || chanItem.slug === "events"){
                continue;
            }
            UA_APP_UTILITY.saveSettingInOrgCommonCredDoc('_ua_app_cache_tpa_channels_'+chanItem.id, chanItem);
        }
    },
    
    cacheWATemplateIfNotPresentInOrgSetting: function(templateID, templateObj){
        UA_APP_UTILITY.addToOrgCacheIfNotExists('tpa_wa_templates_'+templateID, templateObj);
    },
    
    checkAndLoadLatestConversationIfMessageBirdContactFound: async function(){
        try{
            if(Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS).length === 1){
                let firstRecord = UA_APP_UTILITY.storedRecipientInventory[Object.keys(UA_APP_UTILITY.storedRecipientInventory)[0]];
                let calculatedMessgeBirdContactEmail = null;
                if(firstRecord.email && typeof firstRecord.email === "string" && firstRecord.email.endsWith("@contacts.messagebird.com")){
                    calculatedMessgeBirdContactEmail = firstRecord.email;
                }
                else{
                    if(firstRecord.email && Array.isArray(firstRecord.email)){
                        firstRecord.email.forEach(emailItem=>{
                            if(emailItem.value && emailItem.value.endsWith("@contacts.messagebird.com")){
                                calculatedMessgeBirdContactEmail = emailItem.value;
                            }
                        });
                    }
                }
                let messagebirdContactId = null;
                if(calculatedMessgeBirdContactEmail){
                    messagebirdContactId = calculatedMessgeBirdContactEmail.split("@")[0];
                }
                if(messagebirdContactId){
                    let contactConversationResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://conversations.messagebird.com/v1/conversations/contact/"+messagebirdContactId, "get", {});
                    if(contactConversationResponse && contactConversationResponse.data.count > 0){
                        if(contactConversationResponse.data.count === 1){
                            let contactConversationFetchComplete = await UA_TPA_FEATURES.getConversationsList("&ids="+contactConversationResponse.data.items.join(','), true);
                        }
                        else{
                            contactConversationResponse.data.items.forEach(convIdItem=>{
                                UA_TPA_FEATURES.getConversationsList("/"+convIdItem, false);
                            });
                            UA_APP_UTILITY.changeCurrentView('CONVERSATIONS', false);
                        }
                    }
                }
            }
        }
        catch(ex){
            console.log(ex);
        }
    },
     
    callOnEntityDetailFetched: function(){
        UA_TPA_FEATURES.checkAndLoadLatestConversationIfMessageBirdContactFound();
    },

    getConvRelatedRecordsFromSaas: async function(convObj){
        try{
            if(!UA_SAAS_SERVICE_APP.searchRelatedRecordsToAllModules || typeof UA_SAAS_SERVICE_APP.searchRelatedRecordsToAllModules !== 'function') return;
            var relatedRocords = [];
            if(extensionName.indexOf("forzohodesk") > -1){                
                relatedRocords = await UA_SAAS_SERVICE_APP.searchRelatedRecordsToAllModules(convObj.id);
            }
            else{
                if(convObj.to && !isNaN(convObj.to)){
                    relatedRocords = await UA_SAAS_SERVICE_APP.searchRelatedRecordsToAllModules(convObj.to, "phone");
                }
                else if(convObj.to && convObj.to.includes('-')){
                    let convContactData = UA_TPA_FEATURES.FETCHED_CONVERSATIONS[convObj.id].contact;
                    let contactemail = convContactData.id + "@contacts.messagebird.com";
                    relatedRocords = await UA_SAAS_SERVICE_APP.searchRelatedRecordsToAllModules(contactemail, "email");
                }
            }
            return relatedRocords;    
        }
        catch(err){
            return [];
        }    
    },

    renderFieldMappingCriteriaExtra: function(){
        UA_FIELD_MAPPPING_UTILITY.renderIncomingFieldMappingChannelOptions(`When message platform is `, 'message-platform=');
        UA_FIELD_MAPPPING_UTILITY.renderIncomingFieldMappingChannelOptions(`When message channel id is `, 'message-channelId=');
        UA_FIELD_MAPPPING_UTILITY.renderIncomingFieldMappingChannelOptions(`When incoming message received to `, 'direction-in-to=');
    }

    
};