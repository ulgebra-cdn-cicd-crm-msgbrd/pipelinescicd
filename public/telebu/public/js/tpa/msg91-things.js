var UA_TPA_FEATURES = {
    workflowCode: {
        "SMS": {
            "sender": "FILL_HERE", //"MSGIND",
            "route": "FILL_HERE (1 for promotional,4 for transactional SMS)",
            "message": "FILL_HERE",
            "to": "FILL_HERE",
            "channel": "SMS",
            "module": "FILL_HERE",
            "recordId": "FILL_HERE",
            "ulgebra_webhook_authtoken": null
        },
        "WHATSAPP": {
            "integrated_number": "FILL_HERE",//Intergated WhatsApp Number, with country code",
            "recipient_number": "FILL_HERE",
            "content_type": "text",
            "text": "FILL_HERE",
            "channel": "WHATSAPP",
            "module": "FILL_HERE",
            "recordId": "FILL_HERE",
            "ulgebra_webhook_authtoken": null
        }

    },
    chosenMessgeTemplate: null,
    'clientIds': {},
    APP_SAVED_CONFIG: null,
    currentSelectedRoute: 1,
    getSupportedChannels: function () {
        return [UA_APP_UTILITY.MESSAGING_CHANNELS.SMS, UA_APP_UTILITY.MESSAGING_CHANNELS.WHATSAPP];
    },
    renderInitialElements: async function () {
        $("#ac_name_label_tpa .anl_servicename").text('MSG91');
        let balanceText = ""
        UA_TPA_FEATURES.getCurrentAccountOrBalanceInfo(async function (promotional, transactional ,sendotp) {
            if (promotional.status === 200) {
                balanceText += `<div>Promotional Balance: ${promotional.data}</div>`
            } else {
                UA_TPA_FEATURES.showReAuthorizeERROR();
                return false
            }
            if (transactional.status === 200) {
                balanceText += `<div>Transactional Balance: ${transactional.data}</div>`
            }
            if(sendotp.status === 200){
                balanceText += `<div>OTP Balance: ${sendotp.data}</div>`

            }
            $('#ac_name_label_tpa .ac_name_id').html(balanceText);

        }, 1);

        UA_APP_UTILITY.getPersonalWebhookAuthtoken();

        var countryCallCodeArray = [];
        UA_APP_UTILITY.countryCallingCodeArray.forEach(item => {
            countryCallCodeArray.push({
                'label': `${item.name} (${item.dial_code})`,
                'value': item.dial_code
            });
        });

        UA_APP_UTILITY.renderSelectableDropdown('#ssf-new-recip-countrycode', 'Select country', countryCallCodeArray, 'UA_APP_UTILITY.log');
        $("#messageAttachmentInputHolder").remove() //removing wapp image upload element
    },
    messageSenderSelected: function(){
        UA_TPA_FEATURES.smsRouteHandler()
    },
    smsRouteHandler: function (route) {
        const routeElementRendered = $("#sms-form-item-item-route")[0]
        if (!routeElementRendered) {
            $("#sms-form-item-item-sender").after(`
            <div class="ssf-fitem" id="sms-form-item-item-route" style="display:none;">
                <div class="ssf-fitem-label">
                    <span class="material-icons ssf-fitem-ttl-icon" title="SMS Route">route</span> <span class="ssf-fitem-ttl-label">Route</span>
                </div>
                <div class="ssf-fitem-ans ssf-fitem-radio" id="DD_HOLDER_SENDER_ROUTE">
                    <div class="ssf-fitem-radio-item">
                        <input style="margin:0;" type="radio" name="sms_route" value="1" onchange="UA_TPA_FEATURES.smsRouteHandler(1)" checked>
                        <label onclick="$(this).siblings().trigger('click')" for="promotional">Promotional</label>
                    </div>
                    <div class="ssf-fitem-radio-item">
                        <input style="margin:0;" type="radio" name="sms_route" value="4" onchange="UA_TPA_FEATURES.smsRouteHandler(4)">
                        <label for="transactional" onclick="$(this).siblings().trigger('click')">Transactional</label>
                    </div>
                    <div class="ssf-fitem-radio-item">
                        <input style="margin:0;" type="radio" name="sms_route" value="106" onchange="UA_TPA_FEATURES.smsRouteHandler(106)">
                        <label for="OTP SMS" onclick="$(this).siblings().trigger('click')">OTP SMS</label>
                    </div>
                </div>
            </div>`)
        }
        if (UA_APP_UTILITY?.currentSender?.channel?.label === "SMS") {
            $("#sms-form-item-item-route").show()
        } else {
            $("#sms-form-item-item-route").hide()
        }
        if (route) {
            UA_TPA_FEATURES.currentSelectedRoute = route
        }
    },

    renderWorkflowBodyCode: function (accessToken) {

        UA_TPA_FEATURES.workflowCode.SMS.ulgebra_webhook_authtoken = accessToken.saas;
        UA_TPA_FEATURES.workflowCode.WHATSAPP.ulgebra_webhook_authtoken = accessToken.saas;
        UA_APP_UTILITY.addWorkflowBodyCode('sms', 'For Sending SMS', UA_TPA_FEATURES.workflowCode.SMS);
        UA_APP_UTILITY.addWorkflowBodyCode('whatsapp', 'For Sending Whatsapp', UA_TPA_FEATURES.workflowCode.WHATSAPP);
    },
    insertTemplateContentInMessageInput: function (templateId) {
        var curTemplateItem = UA_APP_UTILITY.fetchedTemplates[templateId];
        UA_TPA_FEATURES.chosenMessgeTemplate = curTemplateItem;
        $("#messageAttachmentInputHolder").hide();
        UA_APP_UTILITY.currentAttachedFile = null;
        $("#attachedfile").text('Attach file');
        $('#inputFileAttach').val('');
        if (curTemplateItem.templateType === "placeholder_template") {
            $("#primary-send-btn").text('Send WhatsApp').css('background-color', 'green');
            if (curTemplateItem.head_mediatype && curTemplateItem.head_mediatype !== "") {
                $("#attachedfile").text('Attach ' + curTemplateItem.head_mediatype);
                $("#messageAttachmentInputHolder").show();
            }
            //            if(curTemplateItem.head_media_url){
            //                $("#messageAttachmentInputHolder").show();
            //            }
            $("#ssf-fitem-template-placeholder-holder-listholder").html("");
            curTemplateItem.placeholders.forEach(item => {
                $("#ssf-fitem-template-placeholder-holder-listholder").append(`
                    <div class="ssf-temp-placehold-item">
                            <span class="ssf-temp-placehold-item-label">{{${item}}}</span> <span class="material-icons ssf-temp-placehold-item-icon">double_arrow</span> <input onblur="UA_APP_UTILITY.lastFocusedInputElemForPlaceholderInsert = 'ssf-templ-place-input-${item}'" id="ssf-templ-place-input-${item}" data-placeholder-id="${item}" class="ssf-temp-placehold-item-input" type="text" placeholder="Type or choose field from above">
                        </div>
                `);
            });
            if (curTemplateItem.placeholders.length > 0) {
                $("#ssf-fitem-template-placeholder-holder").show();
            } else {
                $("#ssf-fitem-template-placeholder-holder").hide();
            }
            $('#inp-ssf-main-message').attr('readonly', true);
        }
        else {
            $("#primary-send-btn").text('Send SMS').css('background-color', 'royalblue');
            $("#ssf-fitem-template-placeholder-holder").hide();
            $('#inp-ssf-main-message').removeAttr('readonly');
        }
        $('#inp-ssf-main-message').val(curTemplateItem.message).focus();
    },

    _getServiceTemplates: async function () {
        var templatesResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "/", "post");
        if (templatesResponse.code === 401 || templatesResponse.data.code === 401 || templatesResponse.data.message === "Authentication failed") {
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return;
        }
        return templatesResponse;
    },

    _getServiceWATemplates: async function (silenceAuthError = false, page = 1) {
        var templatesResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://rcsgui.karix.solutions/api/v1.0/template/${UA_TPA_FEATURES.APP_SAVED_CONFIG.waba_id}?status=APPROVED&page=` + page, "get");
        if (templatesResponse.code === 401 || templatesResponse.data.code === 401 || templatesResponse.data.message === "Authentication failed") {
            if (silenceAuthError) {
                return {
                    data: {
                        data: []
                    }
                };
            }
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return;
        }
        return templatesResponse;
    },

    showReAuthorizeERROR: function (showCloseOption = false) {
        if ($('#inp_tpa_service_p_id').length > 0) {
            console.log('already showing reautherr');
            return;
        }
        showErroWindow("Authorization needed!", `You need to authorize your MSG91 Account to proceed. <br><br> 
        <div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
            <div style="
                font-size: 24px;
                font-weight: bold;
                margin-top: 20px;
                margin-bottom: 15px;
            ">SMS Channel Configuration</div>
            <div style="margin-left:20px">
                <div style="font-size: 16px;margin-bottom: 5px;margin-top: 10px;color: rgb(80,80,80);">MSG91 Authkey</div>
                <div class="help_apikey_tip"><a target="_blank" href="https://control.msg91.com/user/index.php#developer/api">Get Here</a></div>
                <input id="inp_tpa_authkey" type="text" placeholder="Enter your Authkey" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
            </div>
            <button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.saveTPAAPIKey(UA_APP_UTILITY.reloadWindow)">Authorize</button>
        </div>`, showCloseOption, false, 500);
        $('#inp_tpa_authkey').focus();
        UA_LIC_UTILITY.showExistingInvitedAdminDDHTML(true);
    },

    showReAuthorizeERRORWithClose: function () {
        UA_TPA_FEATURES.showReAuthorizeERROR();
    },

    saveTPAAPIKey: async function (callback) {
        let authkey = $("#inp_tpa_authkey").val();
        if (!valueExists(authkey)) {
            showErroMessage("Please Enter Authkey");
            $("#inp_tpa_authkey").focus();
            return;
        }
        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', 'authkey', authkey, callback);
    },

    saveTPAWABAAPIKey: async function (callback) {
        let authtoken = $("#inp_tpa_waba_api_key").val();
        if (!valueExists(authtoken)) {
            showErroMessage("Please Enter API Key");
            return;
        }
        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', 'waba_authtoken', authtoken, callback);
    },

    prepareAndSendSMS: function () {
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        if (!UA_APP_UTILITY.currentSender) {
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var senderId = UA_APP_UTILITY.currentSender.value;
        if (!valueExists(senderId)) {
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var recipNumbers = UA_APP_UTILITY.getCurrentMessageRecipients();
        var messageText = $("#inp-ssf-main-message").val().trim();
        if (!valueExists(messageText)) {
            showErroWindow('Message is empty!', "Kindly fill message to proceed.");
            return false;
        }
        if (recipNumbers.length === 0) {
            showErroWindow('Recipient list is empty!', "Kindly add recipients to send SMS.");
            return false;
        }
        if (messageText.length >= 160) {
            showErroWindow('Message is too long!', "Since your message contains more than 160 characters, message may fail in clickatell. <br>  <br>Anyway we're attempting to send the message... ");
        }
        if (!UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE) {
            showErroWindow("<div id='sending-title' class='info'>Sending</div>", `<div id='sending-content'></div>`, false, true);
        }
        const sendedMsgStatus = []
        const resultColorCorrection = () => {
            $(".sent-pan").each(function () {
                $(this).css("border-color", $(this).find(".sent-icon").css("color"))
                $(this).css("box-shadow", "1px 1px 3px " + $(this).find(".sent-icon").css("color"))
            })
        }
        recipNumbers.forEach(function (item, i) {
            item.number = item.number.replace("+","")
            const templateAppliedMsg = UA_APP_UTILITY.getTemplateAppliedMessage(messageText, UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id])
            $("#sending-content").append(`<div class="sent-pan" id="sending-pan-${i}">
                <div class="sent-to">${item.number}</div>
                <div class="sent-msg info"><span class="material-icons sent-icon">more_horiz</span></div>
            </div>`)
            resultColorCorrection()
            let messageHistoryMap = {
                'contact': {
                    'name': UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].Full_Name : item.number
                },
                'from': senderId,
                'to': item.number,
                'message': templateAppliedMsg,
                'status': 'NOT_SENT',
                'module': UA_SAAS_SERVICE_APP.widgetContext.module,
                'moduleId': item.id,
                'channel': UA_APP_UTILITY.currentSender.channel.label
            };
            let callback = function (apiResponse) {
                
                try {
                    if (UA_APP_UTILITY.currentSender.channel.label === "SMS") {
                        if (apiResponse.status === 200) {
                            sendedMsgStatus.push({
                                success: true
                            })
                            $(`#sending-pan-${i}`).find(".sent-msg").toggleClass("info success").html(`Sent successfully <span class='material-icons sent-icon' title="${apiResponse?.data?.message || ""}">check_circle</span>`)
                            messageHistoryMap.status = "SENT";
                            UA_APP_UTILITY.resetMessageFormFields();
                        } else {
                            sendedMsgStatus.push({
                                success: false
                            })
                            const errMsg = (apiResponse?.result?.error?.errors || apiResponse?.error?.message || "Something went wrong").replaceAll('"', '\"')
                            $(`#sending-pan-${i}`).find(".sent-msg").toggleClass("info danger").html(`Unable to send <span class='material-icons sent-icon' title="${errMsg}">error</span>`).slideDown()
                        }
                    } else {
                        if (apiResponse.code === 200) {
                            sendedMsgStatus.push({
                                success: true
                            })
                            $(`#sending-pan-${i}`).find(".sent-msg").toggleClass("info success").html(`Sent successfully <span class='material-icons sent-icon' title="${apiResponse.data}">check_circle</span>`)
                            messageHistoryMap.status = "SENT";
                            UA_APP_UTILITY.resetMessageFormFields();
                        } else {
                            sendedMsgStatus.push({
                                success: false
                            })
                            const errMsg = (apiResponse?.error?.errors || "Something went wrong")
                            $(`#sending-pan-${i}`).find(".sent-msg").toggleClass("info danger").html(`Unable to send <span class='material-icons sent-icon' title="${errMsg}">error</span>`).slideDown()
                        }
                    }

                    resultColorCorrection()
                    UA_SAAS_SERVICE_APP.addSentSMSAsRecordInHistory({ "message1": messageHistoryMap });
                } catch (err) {
                    console.log(err)
                }
                const totalProcessedCount = recipNumbers.length
                const successCount = sendedMsgStatus.filter(status => status.success).length
                const failedCount = sendedMsgStatus.filter(status => !status.success).length
                $("#sending-title").text(`Sending messages ${recipNumbers.length}/${sendedMsgStatus.length}...`)
                if (failedCount === totalProcessedCount) {
                    $("#sending-title").text("Unable to send messages.").toggleClass("info danger")
                }
                if (successCount === totalProcessedCount) {
                    $("#sending-title").text("Sent successfully").toggleClass("info success")
                }
                if (successCount && failedCount) {
                    $("#sending-title").text(`All messages have been processed. total: ${totalProcessedCount}, sent: ${successCount}, failed: ${failedCount}`)
                }
                return apiResponse
            }
            const senderFunc = UA_APP_UTILITY.currentSender.channel.label === "SMS" ? "sendSMS" : "sendWhatsappMessage";

            UA_TPA_FEATURES[senderFunc](senderId, item.number, templateAppliedMsg, callback)
        })


    },
    sendSMS: function (sender, recipNumber, messageText, callback) {
        messagePayload = {
            "sender": sender,//"MSGIND",
            "route": UA_TPA_FEATURES.currentSelectedRoute,
            "sms": [
                {
                    "message": messageText,
                    "to": [recipNumber]
                }
            ]
        }
        if (UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE) {
            const worlflowPayload = {
                "sender": sender,//"MSGIND",
                "route": UA_TPA_FEATURES.currentSelectedRoute,
                "message": messageText,
                "to": recipNumber
            }
            UA_APP_UTILITY.showMessagingWorkflowCode(worlflowPayload);
            return false;
        }
        UA_TPA_FEATURES._proceedToSendSMSAndExecuteCallback(messagePayload, callback);
    },
    sendWhatsappMessage: function (sender, recipNumber, messageText, callback) {
        //https://docs.msg91.com/p/tf9GTextN/e/9yF5DNqR1/MSG91
        let messagePayload = {
            "integrated_number": sender, //"Intergated WhatsApp Number, with country code"
            "recipient_number": recipNumber,
            "content_type": "text",
            "text": messageText
        }
        if (UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE) {
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        UA_TPA_FEATURES._proceedToSendWhatsappMessageAndExecuteCallback(messagePayload, callback);
    },
    _proceedToSendWhatsappMessageAndExecuteCallback: async function (messagePayload, callback) {
        var sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, 'https://api.msg91.com/api/v5/whatsapp/whatsapp-outbound-message/', "POST", messagePayload);
        callback(sentAPIResponse);
    },
    _proceedToSendSMSAndExecuteCallback: async function (messagePayload, callback) {
        var sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "http://api.msg91.com/api/v2/sendsms", "POST", messagePayload);
        callback(sentAPIResponse);
    },

    getAPIResponse: async function (extensionName, url, method, data) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = { "Content-Type": "application/json" };
        await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({ appCode: extensionName, TPAService: "msg91", url: url, method: method, data: data, headers: headers, _ua_lic_adminUserID: UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID }).then((response) => {
            response = response.data;
            returnResponse = response;
            removeTopProgressBar(credAdProcess3);
            return true;
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
        return returnResponse;
    },

    getCurrentAccountOrBalanceInfo: async function (callback) {
        // https://control.msg91.com/api/balance.php?authkey=YourAuthKey&type=1
        const promotionalBalance = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://api.msg91.com/api/balance.php?authkey={{authkey}}&type=1", "GET", {});
        const transactionalBalance = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://api.msg91.com/api/balance.php?authkey={{authkey}}&type=4", "GET", {});
        const sendOtpBalance = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://api.msg91.com/api/balance.php?authkey={{authkey}}&type=106", "GET", {});
        callback(promotionalBalance, transactionalBalance,sendOtpBalance);
    },
};

