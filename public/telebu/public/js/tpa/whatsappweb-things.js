window.addEventListener('storage', async (event) => {
    console.log(event);
    if(event && event.key && event.key.indexOf("zww_chatsLoaded") == 0 && localStorage.getItem("zww_chatsLoaded") && localStorage.getItem("zww_chats")){
        UA_TPA_FEATURES.whatsAppWebConversationObj = JSON.parse(localStorage.getItem("zww_chats"));
        let to = UA_TPA_FEATURES.whatsAppWebConversationObj[0].id.split('@')[0].split('_')[1];
        UA_APP_UTILITY.selectConversationToLoad(`whatsapp:+${UA_APP_UTILITY.currentSender.value}::whatsapp:+${to}`);
    }
});

var UA_TPA_FEATURES = {
    chosenMessgeTemplate : null,
    'clientIds': {
        'whatsappwebforhubspotcrm' : "",
        'whatsappwebforbitrix24' : ""
    },
    tpaSortName: 'WhatsApp Web',
    getSupportedChannels: function(){
        return [ UA_APP_UTILITY.MESSAGING_CHANNELS.SMS, UA_APP_UTILITY.MESSAGING_CHANNELS.MMS ];
    },
    workflowCode : {
        "SMS": {
            "to": "FILL_HERE",
            "text": "FILL_HERE",
            "module": "FILL_HERE",
            "recordId": "FILL_HERE",
            "channel": "SMS",
            "ulgebra_webhook_authtoken": null
        }
    },
    renderInitialElements: async function(){

        let setQueryParams = new URLSearchParams(window.location.search);
        if(setQueryParams.get('view') && setQueryParams.get('view') === 'settings') {
            window.location.href = `https://sms.ulgebra.com/whatsappweb_zohocrm_settings`;
            return;
        }
        
        $("#ac_name_label_tpa .anl_servicename").text(UA_TPA_FEATURES.tpaSortName);
        $('#primary-send-btn').text('Send WhatsApp');
        $('#WHATSAPP_CONV_WINDOW_POPUP').remove();
        UA_TPA_FEATURES.getCurrentAccountOrBalanceInfo(async function() {
            
            $('#ac_name_label_tpa .ac_name_id').html(`<b>${UA_TPA_FEATURES.subscriptionPlan}</b>, Credits : ${UA_TPA_FEATURES.subscriptionCredits}`);

            UA_TPA_FEATURES._getServiceSender();
            
            //UA_APP_UTILITY.getPersonalWebhookAuthtoken();
            $('#suo-item-workflow-nav').hide();
           
            var countryCallCodeArray = [];
            UA_APP_UTILITY.countryCallingCodeArray.forEach(item=>{
                countryCallCodeArray.push({
                    'label': `${item.name} (${item.dial_code})`,
                    'value': item.dial_code
                });
            });
            
            UA_APP_UTILITY.renderSelectableDropdown('#ssf-new-recip-countrycode', 'Select country', countryCallCodeArray, 'UA_APP_UTILITY.log');

            $('#nav_viewTypeSwitch').find('.dropdownInner').find('.ddi-item').each(function() {
                $(this).attr('onclick', $(this).attr('onclick')+';UA_TPA_FEATURES.conversationViewChangeStyle(this);');
            });

            let queryParams = new URLSearchParams(window.location.search);
            if(queryParams.get('view') && queryParams.get('view') === 'conversation') {
                UA_TPA_FEATURES.tpaOnlyConversationView();
                return;
            }

            if(extensionName === 'whatsappwebforzohocrm') {
                await UA_SAAS_SERVICE_APP.searchRecord(UA_SAAS_SERVICE_APP.APP_API_NAME[extensionName].API_NAME + "WhatsApp_Templates", `(${UA_SAAS_SERVICE_APP.APP_API_NAME[extensionName].API_NAME + "Module_Name"}:equals:${UA_SAAS_SERVICE_APP.widgetContext.module})`).then(function(resp) {           

                    var fetchedTemplatesResponse = resp;
                    if(fetchedTemplatesResponse) {
                        console.log('fetchedtemoktes ', fetchedTemplatesResponse);
                        let fetchedTemplatesCollection = fetchedTemplatesResponse;

                        //UA_APP_UTILITY._feature_showUAMsgTemplates = false;
                        
                        var templatesArray = [];
                        fetchedTemplatesCollection.forEach((item, key)=>{
                            let tempMsgText = item[UA_SAAS_SERVICE_APP.APP_API_NAME[extensionName].API_NAME + "WhatsApp_Message"].split(UA_SAAS_SERVICE_APP.widgetContext.module+'.');
                            for(let i=0; i<tempMsgText.length;i++) {
                                if(i!==0) {
                                    tempMsgText[i] = tempMsgText[i].split('}')[0].replaceAll(' ', '_')+"}"+tempMsgText[i].split('}')[1];
                                }
                                else {
                                    tempMsgText[i] = tempMsgText[i]+UA_SAAS_SERVICE_APP.widgetContext.module+'.';
                                }
                            }
                            tempMsgText = tempMsgText.join('');
                            UA_APP_UTILITY.fetchedTemplates[key+''] = {
                                'message': tempMsgText
                            };
                            templatesArray.push({
                                'label': item['Name'],
                                'value': key+''
                            });
                        });
                        
                        var approvedTemplateDDId = UA_APP_UTILITY.renderSelectableDropdown('#ssf-fitem-template-var-holder', `Insert Old ${UA_TPA_FEATURES.tpaSortName} template`, templatesArray, 'UA_TPA_FEATURES.insertTemplateContentInMessageInput', false, false);
                    }

                });
            }

        });
        
    },
    tpaOnlyConversationView: async function() {

        if(!Object.keys(UA_APP_UTILITY.storedRecipientInventory).length) {
            setTimeout(function() {UA_TPA_FEATURES.tpaOnlyConversationView();}, 3000);
            return;
        }
        
        $('#nav_viewTypeSwitch').find('.dropdownInner').find('.ddi-item').each(async function() {
            if($(this).text().trim() === "Conversations View") {
                $(this).click();
                $('#nav_viewTypeSwitch').hide();
                if(extensionName === 'whatsappwebforzohocrm') {
                    await UA_SAAS_SERVICE_APP.changedCurrentView();
                }
            }
        });
        return;

    },
    whatsAppWebConversationObj: [],
    conversationViewChangeStyle: function(selected) {
        if($(selected).text().trim() !== "Form View") {

            $('#DD_HOLDER_MsgType_LIST').css({'height': '0px', 'top': '-20px'});
            $('#DD_HOLDER_MsgType_LIST').find('.DD_HOLDER_SENDER_LIST_inner').css({'box-shadow': 'none'});
            $('#messageAttachmentInputHolder').remove();
            $('.ssf-fitem-attach-checkbox').css({'position': 'relative', 'top': '-12px'});
            $('#primary-send-btn').css({'position': 'relative', 'top': '8px'});

            $('.show-on-view-conversation-view').removeClass('show-on-view-conversation-view');

            if($(selected).text().trim() === "Conversations View") {
                $('.messagePeopleHeader').find('.ssf-fitem').each(function() {
                    if($(this).find('.ssf-fitem-label').text().trim().replaceAll(' ', '').replaceAll('\n', '').indexOf("Recipients+AddRecipients") >= 0) {
                        $(this).hide();
                    }
                });

                if(UA_TPA_FEATURES.isAllowedwithAttach) {
                    $('#wa-attach-tip').html('You will be redirected to WhatsApp tab for sending attachments after you click Send.');
                }
                else {
                    $('#wa-attach-tip').html('You must upgrade to <b>Business</b> or <b>Premium</b> plan for sending attachments.');
                }

            }
            else {

                if(UA_TPA_FEATURES.isAllowedwithAttach) {
                    $('#wa-attach-tip').html('You will be redirected to WhatsApp tab<br>for sending attachments after you click Send.');
                }
                else {
                    $('#wa-attach-tip').html('You must upgrade to <b>Business</b> or <b>Premium</b> plan<br>for sending attachments.');
                }

                UA_TPA_FEATURES.getConversationsList(); 

                $('.messagePeopleHeader').find('.ssf-fitem').each(function() {
                    let headTitle = $(this).find('.ssf-fitem-label').text().trim().replaceAll(' ', '').replaceAll('\n', '');
                    if(headTitle.indexOf("Recipients+AddRecipients") >= 0) {
                        $(this).show().css({'position': 'relative', 'right': '6px', 'justify-content': 'flex-end', 'display': 'flex'});
                        $(this).find('.ssf-fitem-label').hide();
                        $(this).find('.dropdownInner').css({'left': 'auto', 'right': '0px'});
                    }
                    else if(headTitle === "Sender") {
                        $(this).show();
                    }
                    else {
                        $(this).hide();
                    }
                });
            }

        }
        else {

            if(UA_TPA_FEATURES.isAllowedwithAttach) {
                $('#wa-attach-tip').html('You will be redirected to WhatsApp tab for sending attachments after you click Send.');
            }
            else {
                $('#wa-attach-tip').html('You must upgrade to <b>Business</b> or <b>Premium</b> plan for sending attachments.');
            }

            $('#DD_HOLDER_MsgType_LIST').css({'height': 'auto', 'top': '0px'});
            $('#DD_HOLDER_MsgType_LIST').find('.DD_HOLDER_SENDER_LIST_inner').css({'box-shadow': '0px 2px 5px rgb(0 0 0 / 30%)'});
            $('#messageAttachmentInputHolder').remove();
            $('.ssf-fitem-attach-checkbox').css({'position': 'relative', 'top': '0px'});
            $('#primary-send-btn').css({'position': 'relative', 'top': '0px'});

            $('.messagePeopleHeader').find('.ssf-fitem').each(function() {
                let headTitle = $(this).find('.ssf-fitem-label').text().trim().replaceAll(' ', '').replaceAll('\n', '');
                if(headTitle.indexOf("Recipients+AddRecipients") >= 0) {
                    $(this).show().css({'position': 'relative', 'right': '0px', 'justify-content': 'normal', 'display': 'block'});
                    $(this).find('.ssf-fitem-label').show();
                    $(this).find('.dropdownInner').css({'left': '0px'});
                }
                else if(headTitle === "Sender") {
                    $(this).show();
                }
                else {
                    $(this).hide();
                }
            });
        }
    },
    renderWorkflowBodyCode :function(accessToken){
        
        UA_TPA_FEATURES.workflowCode.SMS.ulgebra_webhook_authtoken = accessToken.saas;
        UA_TPA_FEATURES.workflowCode.MMS.ulgebra_webhook_authtoken = accessToken.saas;
        
        UA_APP_UTILITY.addWorkflowBodyCode('sms', 'For Sending SMS', UA_TPA_FEATURES.workflowCode.SMS);
        
        UA_APP_UTILITY.addWorkflowBodyCode('mms', 'For Sending MMS', UA_TPA_FEATURES.workflowCode.MMS);
    },
    prepareAndSendSMS: function(){
        
        document.body.scrollTop = document.documentElement.scrollTop = 0;

        if(!UA_TPA_FEATURES.fromNumber) {
            UA_TPA_FEATURES.showErroWindowforWhatsAppWebConnect();
            return;
        }

        var recipNumbers = UA_APP_UTILITY.getCurrentMessageRecipients();
        
        if(recipNumbers.length === 0){
            showErroWindow('Recipient list is empty!', "Kindly add recipients to send WhatsApp.");
            return false;
        }

        var messageText = $("#inp-ssf-main-message").val().trim();

        if(!valueExists(messageText)){
            showErroWindow('Message is empty!', "Kindly fill message to proceed.");
            return false;
        }

        if(UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.MESSAGE_FORM) {
            $('#sms-prog-window').show();
            $('#msgItemsProgHolder').html('');
            $('#totMsgDisp').text(`Sending messages 0/${recipNumbers.length}...`);
        }

        var messageNumberMapArray = [];
        var messageHistoryMap = {};
        recipNumbers.forEach(item=>{
            let resolvedMessageText = UA_APP_UTILITY.getTemplateAppliedMessage(messageText, UA_APP_UTILITY.storedRecipientInventory[item.id]);
            var historyUID = item.id+''+new Date().getTime()+''+Math.round(Math.random(100000,99999)*1000000);
            messageNumberMapArray.push({
                'number': item.number,
                'text': resolvedMessageText,
                'clientuid': historyUID
            });
            messageHistoryMap[historyUID] = {
                'contact':{
                    'name': UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].Full_Name : item.number
                },
                'from': UA_TPA_FEATURES.fromNumber,
                'to': item.number,
                'message': resolvedMessageText,
                'status': 'NOT_SENT',
                'module': UA_SAAS_SERVICE_APP.widgetContext.module,
                'moduleId': item.id,
                'channel': 'WHATSAPP'
            };

            if(UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.MESSAGE_FORM) {
                $('#msgItemsProgHolder').append(`
                    <div id="msg-resp-item-${parseInt(item.number)}" class="msgRespItem" title="${this.getSafeString(resolvedMessageText)}">
                      <div class="mri-label"><b>${$('.msgRespItem').length+1}</b>. WhatsApp to ${UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].name+'-' : ''} ${item.number}</div>
                      <div class="mri-status">Sending...</div>
                    </div>
                `);
            }

        });
    
        
        UA_TPA_FEATURES.sendBulkSMS(messageNumberMapArray, (function(successSentMsgRes, successSentMsgCount){
            if(UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.MESSAGE_FORM) {
                if(successSentMsgRes && typeof(successSentMsgRes) == 'object' && successSentMsgRes.status && successSentMsgRes.status == "success") {
                    $('#totMsgDisp').text(`All messages have been processed. TOTAL: ${messageNumberMapArray.length}, SENT: ${successSentMsgCount}, FAILED: ${messageNumberMapArray.length-successSentMsgCount}`);
                    //UA_APP_UTILITY.resetMessageFormFields(); 
                }
                else{
                    //showErroWindow('Unable to send message', '');
                    return;              
                }
            }
        }), messageHistoryMap);
        
    },    
    
    insertTemplateContentInMessageInput :function(templateId){
        var curTemplateItem = UA_APP_UTILITY.fetchedTemplates[templateId];
        UA_TPA_FEATURES.chosenMessgeTemplate = curTemplateItem;
        $("#messageAttachmentInputHolder").hide();
        UA_APP_UTILITY.currentAttachedFile = null;
        $("#attachedfile").text('Attach file');
        $('#inputFileAttach').val('');
        if(curTemplateItem.templateType === "placeholder_template"){
            $("#primary-send-btn").text('Send WhatsApp').css('background-color', 'green');
            if(curTemplateItem.head_mediatype){
                switch (curTemplateItem.head_mediatype) {
                    case "0":
                        $("#attachedfile").text('Attach document');
                        break;
                    case "1":
                        $("#attachedfile").text('Attach image');
                        break;
                    case "2":
                        $("#attachedfile").text('Attach video');
                        break;
                    default:
                        $("#attachedfile").text('Attach file');
                        break;
                }
            }
            if(curTemplateItem.head_media_url){
                $("#messageAttachmentInputHolder").show();
            }
            $("#ssf-fitem-template-placeholder-holder-listholder").html("");
            curTemplateItem.placeholders.forEach(item=>{
                $("#ssf-fitem-template-placeholder-holder-listholder").append(`
                    <div class="ssf-temp-placehold-item">
                            <span class="ssf-temp-placehold-item-label">{{${item}}}</span> <span class="material-icons ssf-temp-placehold-item-icon">double_arrow</span> <input onblur="UA_APP_UTILITY.lastFocusedInputElemForPlaceholderInsert = 'ssf-templ-place-input-${item}'" id="ssf-templ-place-input-${item}" data-placeholder-id="${item}" class="ssf-temp-placehold-item-input" type="text" placeholder="Type or choose field from above">
                        </div>
                `);
            });
            if(curTemplateItem.placeholders.length > 0){
                $("#ssf-fitem-template-placeholder-holder").show();
            }else{
                $("#ssf-fitem-template-placeholder-holder").hide();
            }
            $('#inp-ssf-main-message').attr('readonly', true);
        }
        else{
            $("#primary-send-btn").text('Send WhatsApp').css('background-color', 'royalblue');
            $("#ssf-fitem-template-placeholder-holder").hide();
            $('#inp-ssf-main-message').removeAttr('readonly');
        }
        $('#inp-ssf-main-message').val(curTemplateItem.message).focus();
    },
    
    _getServiceSender: async function() {

        $('#DD_HOLDER_SENDER_LIST').hide();
        $('#DD_HOLDER_SENDER_LIST').parent().find('.ssf-fitem-label').first().show();

        $('#DD_HOLDER_SENDER_LIST').before(`<div class="ssf-fitem-ans" id="DD_HOLDER_MsgType_LIST" style="top: 0px;">
                <div style="width: max-content;">
                    <div class="DD_HOLDER_SENDER_LIST_inner" style="padding: 10px 15px; box-shadow: 0px 2px 5px rgb(0 0 0 / 30%); border-radius: 5px; margin: 10px 0px; color: rgb(50,50,50);">
                        <div style="color:grey;cursor:default;padding-top: 3px;" id="connectedWhatsAppDiv">
                            Checking connection...
                        </div>
                    </div>
                </div>
            </div>`);

        await UA_TPA_FEATURES.checkWhatsAppConnected();

    },
    
    _getServiceTemplates: async function(){
        var templatesResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://{{domain}}/get_templates/", "post",{});
        if(templatesResponse && typeof(templatesResponse) == 'object' && templatesResponse.data && typeof(templatesResponse.data) == 'object' && templatesResponse.data.templates && typeof(templatesResponse.data.templates) == 'object' && templatesResponse.data.templates.length){
            return  templatesResponse.data.templates;
        }
        else {
            return false;
        }
    },
    
    _getServiceChannels: async function(silenceAuthError = false){
        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://api.nexmo.com/beta/chatapp-accounts", "GET");
        if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data._embedded && typeof(response.data._embedded) == 'object' && response.data._embedded.length) {
            return response.data._embedded;
        }
        else {
            return false;
        }
    },
    
    showReAuthorizeERROR: function(showCloseOption = false){
        showErroWindow("Install Google Chrome Extension!", `Our Google Chrome extension is needed to Send Messages in <br>Background and to Send Bulk WhatsApp Messages. <br><br>You need to purchase <a target="_blank" style="font-weight: 900; color: #1557af;" href="https://apps.ulgebra.com/zoho/crm/whatsapp-web/pricing?src=crmwabulkpop">one of our plans to send Bulk Messages.</a> <br><br> <button id="UA_TPA_AUTH_BTN" class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.initiateAuth()">Install Google Chrome Extension</button>`, false, false, 1000);
    },

    initiateAuth: function () {
        if(!extensionName){
            showErroWindow('Application error!', 'Unable to load app type. Kindly <a target="_blank" href="https://apps.ulgebra.com/contact">contact developer</a>');
            return;
        }
        window.open(`https://chrome.google.com/webstore/detail/wa-web-for-zoho-crm-bulk/haphhpfcpfeagcmannpebjjjpdlbhflh`, 'Authorize', 'popup');
    },
    
    openWABAAPIKey: function(showCloseOption = false){
        showErroWindow("Brand Authentication needed!", `<div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
        <div style="font-size: 18px;margin-bottom: 5px;margin-top: 20px;"> ${UA_TPA_FEATURES.tpaSortName} From</div>
        <input id="inp_tpa_waba_api_key" type="text" placeholder="Enter your From Name..." style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
        <button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.saveTPAWABAAPIKey(UA_APP_UTILITY.reloadWindow)">Save</button>
    </div>`, showCloseOption, false, 500);
        $('#inp_tpa_waba_api_key').focus();
    },
    
    showReAuthorizeERRORWithClose: function(){
        UA_TPA_FEATURES.showReAuthorizeERROR();
    },
    
    saveTPAAPIKey: async function(callback){
        let authtoken = $("#inp_tpa_api_key").val();
        if(!valueExists(authtoken)){
            showErroMessage("Please fill API Key");
            return;
        }

        let wcuserId = $("#inp_tpa_user_id").val();
        if(!valueExists(wcuserId)){
            showErroMessage("Please fill API Key");
            return;
        }

        let isTpaAccountAutherized = await UA_TPA_FEATURES.isTpaAccountAutherized(extensionName, `https://us-central1-app-whatcetra.cloudfunctions.net/getBalance`, "POST", {'_apikey': authtoken, '_userId': wcuserId});
        
        if(isTpaAccountAutherized) {
            await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', '__multiple_keys__', {'authtoken' : authtoken, 'wcuserId': wcuserId}, callback);
        }
        else {
            showErroMessage("<b>Please provide Authorization</b></br></br>API Key or User ID is Incorrect.");
            return;
        }

    },
    isTpaAccountAutherized: async function (extensionName, url, method, data) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = {"Content-Type": "application/json"};
        return await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, url: url, method: method, data: data, headers: headers}).then(async (response) => {
            if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.data && typeof(response.data.data) == 'object' && response.data.data.data && typeof(response.data.data.data) == 'object' && String(response.data.data.data.balance)) {
                removeTopProgressBar(credAdProcess3);
                return true;
            }
            else {
                removeTopProgressBar(credAdProcess3);
                return false;
            }
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
    },
    saveTPAWABAAPIKey: async function(callback){

        let authkey = $("#inp_tpa_waba_api_key").val();
        if(!valueExists(authkey)){
            showErroMessage("Please fill Sender Id");
            return;
        }

        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', 'senderId', authkey, callback);

    },
    isTpaWaBaAccountAutherized: async function (extensionName, url, method, data, authid) {
        let authkey = $("#inp_tpa_waba_api_key").val();
        if(!valueExists(authkey)){
            showErroMessage("Please fill From Name");
            return;
        }

        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', 'senderId', authkey, callback);
    },
    
    WHA_CHROME_SELECTOR: {"pcs":'._3GlyB',"be":"#app .app-wrapper-web span","Re":"[data-icon=send]","In":'._3J6wB:not(._2ctZG)',"StartChat":'._3J6wB:not(._2ctZG) svg',"Im":"#side header img","at":'[data-icon="clip"]',"atIm":'[data-icon="attach-image-old"]',"atd":'[data-icon="attach-document-old"]',"om":".message-out","fb":'[data-icon="forward-chat"]',
                            "mb":"._1gL0z","fd":"._2Nr6U .zoWT4 .ggj6brxn.gfz4du6o.r7fjleex.g0rxnol2.lhj4utae.le5p0ye3.l7jjieqr.i0jNr","fi":"input[type=file]","bl":"._1YfMk ._31enr","Ig":"._3ExzF","cw":"._1bR5a","if":".message-in","ic":"_31gEB","cwa":'._3XpKm._20zqk',"asr":'._1-59F',"cl":".GDTQm","ma":"._17Osw","ht":"#main header ._21nHd span","cr":"._210SC"},
    
    sendBulkSMS : async function(numberMessageMapArray, callback, messageHistoryMap) {

        if(!UA_TPA_FEATURES.isAllowed) {
            showErroWindow('No Subscription Plan', `<div onclick="window.open('https://store.ulgebra.com/?product_id=prod_IZibGrC2KlZfyE')" style=" width: 150px; position: relative; left: calc(100% - 230px); top: 3px; height: 30px; display: flex; align-items: center; justify-content: center; background-color: #247642; border-radius: 2px; box-shadow: 0px 0px 2px #001e04; color: whitesmoke; cursor: pointer; ">Upgrade Plan</div>`);
            return;
        }

        if(Number(UA_TPA_FEATURES.subscriptionCredits) && Number(UA_TPA_FEATURES.subscriptionCredits) < numberMessageMapArray.length) {
            showErroWindow('No Subscription Plan', `<div onclick="window.open('https://store.ulgebra.com/?product_id=prod_IZibGrC2KlZfyE')" style=" width: 150px; position: relative; left: calc(100% - 230px); top: 3px; height: 30px; display: flex; align-items: center; justify-content: center; background-color: #247642; border-radius: 2px; box-shadow: 0px 0px 2px #001e04; color: whitesmoke; cursor: pointer; ">Upgrade Plan</div>`);
            return;
        }

        UA_TPA_FEATURES.logSUBActivityLog(numberMessageMapArray.length);

        let successSentMsgCount = 0;

        let dt = new Date().getTime();
        let attachmentId = new Date().getTime();

        chrome.runtime.sendMessage(UA_TPA_FEATURES.extensionVersionId,{"message":"setSelectors","s":UA_TPA_FEATURES.WHA_CHROME_SELECTOR,"url":''},function (response){
            console.log(response);
        });

        let messagePayload = {};

        for(let MsgMapArr=0; MsgMapArr < numberMessageMapArray.length; MsgMapArr++) {            

            let msgUrl = '';
            let msgText = numberMessageMapArray[MsgMapArr].text;
            let msgNumber = numberMessageMapArray[MsgMapArr].number.replace(/\D/g,'');
            
            messagePayload[++dt] = {
                                "n":msgNumber,
                                "m":msgText,
                                "u":"",
                                "dt":dt,
                                "t":attachmentId
                            };

            successSentMsgCount++;  
            messageHistoryMap[numberMessageMapArray[MsgMapArr].clientuid].status = "SENT";
            $(`#msg-resp-item-${parseInt(numberMessageMapArray[MsgMapArr].number)} .mri-status`).text(`Sent`).css({'color': 'green'});
            $('#totMsgDisp').text(`Sending messages ${MsgMapArr+1}/${numberMessageMapArray.length}...`);

            if(UA_APP_UTILITY.currentMessagingView !== UA_APP_UTILITY.appPrefinedUIViews.MESSAGE_FORM) {
                
                let sentMsgItem = {
                    id: "true_"+msgNumber+"@c.us",
                    src: "",
                    m: msgText,
                    t: String(new Date().getTime()),
                    ty: "chat"
                };

                UA_TPA_FEATURES.LOCAL_MESSAGE_STORAGE[sentMsgItem.t] = sentMsgItem;
                let convData = UA_TPA_FEATURES.convertMessageToUASchema(sentMsgItem);
                UA_APP_UTILITY.addMessageInBox(convData, true);

                let convItem = UA_TPA_FEATURES.convertMsgItemToUAConversationSchema(sentMsgItem);
                UA_APP_UTILITY.addConversationItemToConversationBox(convItem);

                UA_APP_UTILITY.scrollChatBoxToLatest();

                $('.msgmedia-holdr').css({'height': 'max-content'});

            }

        }

        if(UA_TPA_FEATURES.isAllowedwithAttach && UA_TPA_FEATURES.isGetAttachment) {
            messagePayload = {"message": "getAttachments","t":attachmentId,"sendBulkWhatsAppList":messagePayload};
        }

        await chrome.runtime.sendMessage(UA_TPA_FEATURES.extensionVersionId, messagePayload, async function (successSentMsgRes){
            
            console.log('response >>--->',successSentMsgRes);
            if(successSentMsgRes && typeof(successSentMsgRes) == 'object' && successSentMsgRes.status && successSentMsgRes.status == "success") {
                await UA_SAAS_SERVICE_APP.addSentSMSAsRecordInHistory(messageHistoryMap);
                await callback(successSentMsgRes, successSentMsgCount);
            }

        });
        
    },
    subscriptionPlans: {
"price_1IdrL2Ho9p6j6FgR5qyjJOo0":{
 "type": "premium"
},"price_1IP0aDHo9p6j6FgRibv4isf0":{
"type": "premium"
},"price_1I6CrpHo9p6j6FgRISpRviN4":{
"type": "premium"
},"price_1HyZblHo9p6j6FgRQzDdcl81":{
"type": "premium"
},"price_1HyZbTHo9p6j6FgRGfOwxuwQ":{
"type": "premium"
},"price_1HyZbFHo9p6j6FgRVHU989Un":{
"type": "premium"
},"price_1IP0ZkHo9p6j6FgRqHyLCI4z":{
"type": "business"
},"price_1I6CrFHo9p6j6FgRhnRWESX1":{
"type": "business"
},"price_1HyZaxHo9p6j6FgRnWjTs3we":{
"type": "business"
},"price_1HyZadHo9p6j6FgRe0Ai3LrT":{
"type": "business"
},"price_1HyZEtHo9p6j6FgRAvpfOvqM":{
"type": "business"
},"price_1HyZAgHo9p6j6FgReqwmnUp7":{
"type": "classic"
},"price_1HyZAgHo9p6j6FgRqQGZavms":{
"type": "classic"
},"price_1IP0YzHo9p6j6FgRg15L9O7H":{
"type": "classic"
},"price_1I6CmKHo9p6j6FgRQv6Cpet2":{
"type": "classic"
},"price_1HyeEsHo9p6j6FgRoSkOo1kP":{
"type": "classic"
},"price_1HyeESHo9p6j6FgRy1Pwz7FH":{
"type": "classic"
},"price_freeInstall":{
"type": "classic"
}},
subscriptionsArr: [],
subscriptionPlanId: null,
subscriptionOwnerId: null,
isAllowed: null,
isAllowedwithAttach: null,
subscriptionCredits: `No Credits <div onclick="window.open('https://store.ulgebra.com/?product_id=prod_IZibGrC2KlZfyE')" style=" width: 90px; position: relative; left: calc(100% - 90px); top: 3px; height: 25px; display: flex; align-items: center; justify-content: center; background-color: #247642; border-radius: 2px; box-shadow: 0px 0px 2px #001e04; color: whitesmoke; cursor: pointer; ">Upgrade Plan</div>`,

    fetchLICDetails: function(callback){

    if (!UA_SAAS_SERVICE_APP.CURRENT_USER_INFO || !UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.email) {
        console.log('firebase not initialized, retrying...');
        setTimeout(function() {UA_TPA_FEATURES.fetchLICDetails(callback);}, 2000);
        return;
    }

    fetch("https://us-central1-ulgebra-license.cloudfunctions.net/getUALicensedUserSubs?userId="+UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.email)
        .then(function (response) {  
            return response.json();
        })
        .then(function (subscrObj) {
            console.log(subscrObj);
            let subscrPriceOwnerArr = {};
            if(subscrObj && Object.keys(subscrObj.subscriptions).length > 0){
                UA_TPA_FEATURES.subscriptionsArr = subscrObj.subscriptions;
                for(var item in UA_TPA_FEATURES.subscriptionsArr){
                    if(UA_TPA_FEATURES.subscriptionPlans.hasOwnProperty(UA_TPA_FEATURES.subscriptionsArr[item].priceId)){
                        subscrPriceOwnerArr[UA_TPA_FEATURES.subscriptionsArr[item].priceId] = {'priceId': UA_TPA_FEATURES.subscriptionsArr[item].priceId, 'ownerId': UA_TPA_FEATURES.subscriptionsArr[item].licOwner};
                    }
                }
            }
            else {
                UA_TPA_FEATURES.subscriptionsArr = {'basic' : {
                                                        licOwner: currentUser.uid,
                                                        modifiedTime: new Date().getTime(),
                                                        priceId: "price_freeInstall",
                                                        subscriptionId: "basic",
                                                        subscriptionStatus: "active"
                                                    }};
                for(var item in UA_TPA_FEATURES.subscriptionsArr){
                    if(UA_TPA_FEATURES.subscriptionPlans.hasOwnProperty(UA_TPA_FEATURES.subscriptionsArr[item].priceId)){
                        subscrPriceOwnerArr[UA_TPA_FEATURES.subscriptionsArr[item].priceId] = {'priceId': UA_TPA_FEATURES.subscriptionsArr[item].priceId, 'ownerId': UA_TPA_FEATURES.subscriptionsArr[item].licOwner};
                    }
                }
            }
            for(var item in UA_TPA_FEATURES.subscriptionPlans){
              if(subscrPriceOwnerArr[item]){
                    UA_TPA_FEATURES.subscriptionPlanId = item;
                    UA_TPA_FEATURES.subscriptionOwnerId = subscrPriceOwnerArr[item].ownerId;
                    UA_TPA_FEATURES.subscriptionPlan = UA_TPA_FEATURES.subscriptionPlans[UA_TPA_FEATURES.subscriptionPlanId].type.toUpperCase();
                    UA_TPA_FEATURES.isAllowed = UA_TPA_FEATURES.subscriptionPlans[UA_TPA_FEATURES.subscriptionPlanId].type === "basic" || UA_TPA_FEATURES.subscriptionPlans[UA_TPA_FEATURES.subscriptionPlanId].type === "classic";
                    if(UA_TPA_FEATURES.subscriptionPlans[UA_TPA_FEATURES.subscriptionPlanId].type === "business" || UA_TPA_FEATURES.subscriptionPlans[UA_TPA_FEATURES.subscriptionPlanId].type === "premium"){
                        UA_TPA_FEATURES.isAllowedwithAttach = true;
                        UA_TPA_FEATURES.isAllowed = true;
                        $('.messageInputHolder').append(`<div class="ssf-fitem-attach-checkbox" style="display: block;"> <br> 
                            <input onchange="UA_TPA_FEATURES.getAttachments()" type="checkbox" id="wwebattach" name="wwebattach" style=" height: 18px; width: 18px; cursor: pointer; vertical-align: middle; margin-top: -1px; ">
                            <label id="wa-attach-tipLabel" for="wwebattach" style=" margin-left: 5px; cursor: pointer; ">Include attachments with this message</label>
                            <span id="wa-attach-tip" style="cursor: default;display: block;display: none;margin: 0px;border: 1px dotted green;padding: 5px 10px;background-color: rgba(0,200,0,0.1);word-break: unset;border-radius: 3px;font-size: 14px;color: green;width: max-content;position: relative;left: 34px;top: 3px;">
                                You will be redirected to WhatsApp tab for sending attachments after you click Send. 
                            </span>
                            <style>#wa-attach-tip {display: block;}</style>
                        </div>`);
                    }
                    else {
                        $('.messageInputHolder').append(`<div class="ssf-fitem-attach-checkbox" style="display: block;"> <br> 
                            <input onclick="UA_TPA_FEATURES.getAttachments()" type="checkbox" id="wwebattach" name="wwebattach" style=" height: 18px; width: 18px; cursor: pointer; vertical-align: middle; margin-top: -1px; ">
                            <label id="wa-attach-tipLabel" for="wwebattach" style=" margin-left: 5px; cursor: pointer; ">Include attachments with this message</label>
                            <span id="wa-attach-tip" style="cursor: default;display: block;display: none;margin: 0px;border: 1px dotted #004880;padding: 5px 10px;background-color: rgb(0 106 200 / 10%);word-break: unset;border-radius: 3px;font-size: 14px;color: #004b80;width: max-content;position: relative;top: 3px;left: 34px;">You must upgrade to <b>Business</b> or <b>Premium</b> plan for sending attachments.</span>
                            <style>#wa-attach-tip {display: block;}</style>
                        </div>`);
                    }

                    firebase.database().ref('ualic_user_credits/' + UA_TPA_FEATURES.subscriptionOwnerId).on('value', function(snapshot){
                        console.log(snapshot.val());
                        if(snapshot.val() > 0) {
                            UA_TPA_FEATURES.subscriptionCredits = snapshot.val();
                            $('#ac_name_label_tpa .ac_name_id').html(`<b>${UA_TPA_FEATURES.subscriptionPlan}</b>, Credits : ${UA_TPA_FEATURES.subscriptionCredits}`);
                        }
                    });

                    callback();

                  break;
              }
            }
        })
        .catch(function (error) {
            console.log("Error: ", error);
        });
},
fromNumber: null,
checkWhatsAppConnected: async function() {

    chrome.runtime.sendMessage(UA_TPA_FEATURES.extensionVersionId,{"message":"setSelectors","s":UA_TPA_FEATURES.WHA_CHROME_SELECTOR,"url":''},function (response){
        console.log(response);
    });

    $('#DD_HOLDER_MsgType_LIST #connectedWhatsAppDiv').html(`Checking connection...`);

    await chrome.runtime.sendMessage(UA_TPA_FEATURES.extensionVersionId,{"message":"checkState"}, function (response){
        if(response && response.connected == true && response.ownerNo){
            UA_TPA_FEATURES.fromNumber = response.ownerNo;
            $('#DD_HOLDER_MsgType_LIST #connectedWhatsAppDiv').html(`<b>${response.ownerNo}</b> (<span style="color: green;font-size: 14px;">Connected via</span> <a href="https://web.whatsapp.com" target="_blank" style="color: #007bff;text-decoration: none; ">WhatsApp Web</a>)`).css({'cursor': 'default'});
            $('<style>').attr('type', 'text/css').text(`#connectedWhatsAppDiv a:hover{color: #0056b3 !important; text-decoration: underline !important;}`).prependTo('#connectedWhatsAppDiv');
            UA_APP_UTILITY.currentSender = {
                    channel: {id: 'WHATSAPP', label: 'WhatsApp', attachmentSupported: false},
                    label: "WhatsApp",
                    value: UA_TPA_FEATURES.fromNumber
            };
            UA_TPA_FEATURES.getConversationsList();
            if($('.whatsappwebConnectError').is(':visible')) {
                $('.error-window-outer ').hide();
            }
        }
        else {
            UA_TPA_FEATURES.fromNumber = null;
            $('#DD_HOLDER_MsgType_LIST #connectedWhatsAppDiv').html(`WhatsApp Web is not connected<button onclick="UA_TPA_FEATURES.checkWhatsAppConnected()" style=" padding: 2px 12px; margin-left: 10px; border-radius: 25px; background-color: green; color: white; border: none; box-shadow: 0px 2px 5px rgb(0 0 0 / 30%); font-size: 15px; ">Fix now</button>`).css({'cursor': 'not-allowed'});
            if(!$('.whatsappwebConnectError').is(':visible')) {
                UA_TPA_FEATURES.showErroWindowforWhatsAppWebConnect();
            }
            else {
                setTimeout(function() {
                    $('.whatsappwebConnectErrorButt').find('.loaderOuterDiv').hide();
                    $('.whatsappwebConnectErrorButt').css({'width': 'auto'});
                }, 1000)
            }
        }
    });

},
showErroWindowforWhatsAppWebConnect: function() {
    showErroWindow('Please make sure', `<ul class="whatsappwebConnectError" style="line-height: 1.6;text-shadow: 0px 0px 1px;"><li>WhatsApp Web tab is connected with your phone</li><li>Only one WhatsApp Web tab is open.</li></ul><br><button class="whatsappwebConnectErrorButt" onclick="UA_TPA_FEATURES.checkWhatsAppConnected(); $('.whatsappwebConnectErrorButt').css({'width': '150px'});$('.whatsappwebConnectErrorButt').find('.loaderOuterDiv').show();" style="display: flex;align-items: center;width: auto;justify-content: space-between; padding: 5px 12px; margin-left: 0px; border-radius: 3px; background-color: green; color: white; border: none; box-shadow: 0px 2px 5px rgb(0 0 0 / 30%); font-size: 15px; ">Check Again</button><br><br><span style="font-size: 14px;font-style: italic;">If it's connected properly and this error shown, <br><b>refresh WhatsApp Web</b> and try or restart your chrome browser <br>and try again or Contact Us.</span>`);
    UA_TPA_FEATURES.ulgloaderDiv(".whatsappwebConnectErrorButt", "15px");
    $('.whatsappwebConnectErrorButt').find('.loaderOuterDiv').hide().find('.wcMsgLoadingInner').css({'box-shadow': 'none', 'background-color': '#008000'}).find('.wcMsgLoadingSVG').css({'width': '12px', 'height': '12px'}).find('.wcMsgLoadingSvgCircle').css({'stroke': 'rgb(197 216 205)'});
},
ulgloaderDiv : async function(ulgLoaderParent, ulgLoaderSize) {

        $(ulgLoaderParent).append(`<div class="loaderOuterDiv"><div class="wcMsgLoadingInner" title="loading…"><svg class="wcMsgLoadingSVG" width="calc(${ulgLoaderSize}/2)" height="calc(${ulgLoaderSize}/2)" viewBox="0 0 46 46" role="status"><circle class="wcMsgLoadingSvgCircle" cx="23" cy="23" r="20" fill="none" stroke-width="6" style="stroke: rgb(37 162 94);"></circle></svg></div></div>`);

        let ulg_loader_popup_css = `/* message loading style */

.wcMsgLoadingInner {
    margin: 0 auto;
    background-color: #fff;
    border-radius: 50%;
    box-shadow: 0 1px 1px 0 rgba(0,0,0,.06),0 2px 5px 0 rgba(0,0,0,.2);
    display: flex;
    align-items: center;
    justify-content: center;
    width: ${ulgLoaderSize};
    height: ${ulgLoaderSize};
    color: rgba(0,0,0,0.25);
}

.wcMsgLoadingSvgCircle {
    stroke: #ccc;
    stroke-dasharray: 1,150;
    stroke-dashoffset: 0;
    stroke-linecap: round;
    animation: wcMsgLoadingSvgCircle 1.5s ease-in-out infinite;
}

.wcMsgLoadingSVG {
    animation: wcMsgLoadingSVG 2s linear infinite;
}

/* message loading style */

@keyframes wcMsgLoadingSVG{
    to{transform:rotate(1turn)}
}

@keyframes wcMsgLoadingSvgCircle{
    0%{stroke-dasharray:1,150;stroke-dashoffset:0}
    50%{stroke-dasharray:90,150;stroke-dashoffset:-35}
    to{stroke-dasharray:90,150;stroke-dashoffset:-124}
}`;

    $('<style>').attr('type', 'text/css').text(ulg_loader_popup_css).prependTo('.loaderOuterDiv');        

}
,
logSUBActivityLog: async function(priority){
    await fetch("https://us-central1-ulgebra-license.cloudfunctions.net/uaAPPSubActivity?behalf="+UA_TPA_FEATURES.subscriptionOwnerId+"&priority="+priority+"&digi="+Date.now())
      .then(function (response) {
          return response.json();
      })
      .then(function (myJson) {
          console.log(myJson);
      })
      .catch(function (error) {
          console.log("Error: ", error);
       });
},
isGetAttachment: false,
getAttachments: function() {
    if($('#wwebattach').is(":checked") && UA_TPA_FEATURES.isAllowedwithAttach) {
        $("#wa-attach-tip").css({'display': 'block'}).show();
        UA_TPA_FEATURES.isGetAttachment = true;
    }
    else if(UA_TPA_FEATURES.isAllowedwithAttach) {
        $("#wa-attach-tip").hide();
        UA_TPA_FEATURES.isGetAttachment = false;
    }
    else {
        $('#wwebattach').removeAttr("checked");
        $('#wa-attach-tip').remove();
        $('#wa-attach-tipLabel').append(`<span id="wa-attach-tip" style="cursor: default;display: none;margin: 0px;border: 1px dotted #004880;padding: 5px 10px;background-color: rgb(0 106 200 / 10%);word-break: unset;border-radius: 3px;font-size: 14px;color: #004b80;width: max-content;position: relative;top: 3px;left: 34px;">You must upgrade to <b>Business</b> or <b>Premium</b> plan for sending attachments.</span>`);
        $('#wa-attach-tip').show().fadeOut(5000);
    }
},
    
    getAPIResponse: async function (extensionName, url, method, data) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = {"Content-Type": "application/json"};
        await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, TPAService: "whatcetra",url: url, method: method, data: data, headers: headers}).then((response) => {
            response = response.data;
            returnResponse = response;
            removeTopProgressBar(credAdProcess3);
            return true;
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
        return returnResponse;
    },

    getCurrentAccountInfo: async function() {
        var headers = {"Content-Type": "application/json"};
        return await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, TPAService: "whatcetra",url: '--checkbalance', method: 'get', data: {}, headers: headers}).then(async (response) => {
            if(response && typeof(response) == 'object' && response.data && typeof(response.data) == 'object' && response.data.status && response.data.status == 'success' && response.data.ulgebraAppConfiguration) {
                return response.data.ulgebraAppConfiguration;
            }
            else {
                return false;
            }
        }).catch(err => {
            console.log(err);
            return false;
        });
    },    
    tpaCredentials: false,
    getCurrentAccountOrBalanceInfo: async function(callback) {

        await UA_TPA_FEATURES.checkVersion(callback);
        
    },
    extensionVersion: null,
    extensionVersionId: 'haphhpfcpfeagcmannpebjjjpdlbhflh',
    checkVersion: async function(callback) {
        if(window.chrome && chrome.runtime && chrome.runtime.sendMessage){
            return await chrome.runtime.sendMessage(UA_TPA_FEATURES.extensionVersionId, { "message": "version" },function (reply){
                console.log(reply);
                UA_TPA_FEATURES.extensionVersion = reply;
                if(UA_TPA_FEATURES.extensionVersion && parseFloat(UA_TPA_FEATURES.extensionVersion.version) > 9){
                    UA_TPA_FEATURES.fetchLICDetails(callback);
                    return true;
                }
                else{
                    UA_TPA_FEATURES.showReAuthorizeERROR();
                    return false;
                }
            }); 
        }else{
            UA_TPA_FEATURES.showReAuthorizeERROR();
        }       
    },
    getSafeString: function(rawStr) {
        if (!rawStr || rawStr+"".trim() === "") {
            return "";
        }
        return $('<textarea/>').text(rawStr).html();
    },
    fetchMessagesAndAddToChatBox: async function(){
        var senderId = "";
        var isWhatsAppChannel = false;
        if(UA_APP_UTILITY.currentSender){
            senderId = UA_APP_UTILITY.currentSender.value;
            isWhatsAppChannel = UA_APP_UTILITY.currentSender.channel === UA_APP_UTILITY.MESSAGING_CHANNELS.WHATSAPP;
            if (isWhatsAppChannel) {
                senderId = "whatsapp:+" + senderId.replace(/\D/g, '');
            }
       }
       else if(!UA_APP_UTILITY.currentSender) {
            UA_TPA_FEATURES.showErroWindowforWhatsAppWebConnect();
            return;
        }
       var recipientId = "";
       if(UA_APP_UTILITY.chatCurrentRecipient){
            recipientId = UA_APP_UTILITY.chatCurrentRecipient.address;
            if(valueExists(recipientId)){
                recipientId = (isWhatsAppChannel ? "whatsapp:+" : (recipientId.indexOf('+')!==0 ? "+" : '')) +recipientId;
            }
        }
        if ((!senderId || !recipientId)) {
            return;
        }
       
       if(senderId && recipientId){
            UA_APP_UTILITY.CURRENT_CONVERSATION_ID = UA_APP_UTILITY.getCleanStringForHTMLAttribute(senderId+'::'+recipientId);
            UA_APP_UTILITY.initiateRTListeners(senderId+'::'+recipientId);
       }
       $("#chatbox-message-holder").html("");
       UA_TPA_FEATURES.messageDirectionsFetched = 0;
       UA_TPA_FEATURES.LOCAL_MESSAGE_STORAGE = {};
       UA_TPA_FEATURES.fetchMessagesAPIAndAddTemp();
    },
    messageDirectionsFetched: 0,    
    LOCAL_MESSAGE_STORAGE: {},    
    fetchMessagesAPIAndAddTemp: async function(url){

        UA_TPA_FEATURES.whatsAppWebConversationObj.forEach(item=>{
            UA_TPA_FEATURES.LOCAL_MESSAGE_STORAGE[item.t] = item;
        });
        
        Object.keys(UA_TPA_FEATURES.LOCAL_MESSAGE_STORAGE).sort().reverse().forEach(item=>{
            
            if(UA_TPA_FEATURES.messageDirectionsFetched === 0) {
                UA_TPA_FEATURES.messageDirectionsFetched++;
                let convItem = UA_TPA_FEATURES.convertMsgItemToUAConversationSchema(UA_TPA_FEATURES.LOCAL_MESSAGE_STORAGE[item]);
                UA_APP_UTILITY.addConversationItemToConversationBox(convItem);
            }

            let convData = UA_TPA_FEATURES.convertMessageToUASchema(UA_TPA_FEATURES.LOCAL_MESSAGE_STORAGE[item]);
            UA_APP_UTILITY.addMessageInBox(convData, false);

        });
        UA_APP_UTILITY.scrollChatBoxToLatest();

        $('.msgmedia-holdr').css({'height': 'max-content'});
        
        return;
    },
    
    convertMessageToUASchema: function(msgObj){

        let inComing = msgObj.id.indexOf("false_") != -1;
        let to = 'whatsapp:+'+msgObj.id.split('@')[0].split('_')[1];
        let from = 'whatsapp:+'+UA_APP_UTILITY.currentSender.value;
        let customerNumber = !inComing ? to : from;
        let conversationID = from + "::" + to;
        customerNumber = customerNumber.replace("whatsapp:", '');
        let channelName = from.indexOf("whatsapp") === 0 ? "whatsapp" : "sms";
        
        let content = msgObj.m ? this.getSafeString(msgObj.m) : "";
        content = content.replace("\n","<br>");
        let mediaHTML = "";
        if(msgObj.ty !== "chat"){
            mediaHTML = {url: msgObj.src, type: msgObj.ty};
        }
        let status = inComing?"received":"delivered";
        
        let messageChannel = channelName === "whatsapp" ? UA_APP_UTILITY.MESSAGING_CHANNELS.WHATSAPP : UA_APP_UTILITY.MESSAGING_CHANNELS.SMS;

        var attachmentsArr = [];

        if(mediaHTML !== "") {
            attachmentsArr = [mediaHTML];
            content = "";
        }

        var modifiedMsgObj = {
            'id': msgObj.id.replaceAll('@', '_').replaceAll('.', '_'),
            'text': this.getSafeString(content),
            'channel': messageChannel,
            'isIncoming': inComing,
            'from': from,
            'to': to,
            'createdTime': Number(msgObj.t),
            'status': status,
            'isUnRead': false,
            'attachments': attachmentsArr,
            'conversationID': conversationID
        };
        
        return modifiedMsgObj;
    },
    getConversationsList: async function(){

        if(!UA_APP_UTILITY.currentSender) {
            UA_TPA_FEATURES.checkWhatsAppConnected();
            return;
        }

        UA_APP_UTILITY.LOCAL_FETCHED_CONVERSATION_IDS = [];

        UA_TPA_FEATURES.whatsAppWebConversationObj = [];

        let fetchedNumbers = {};
        
        await UA_APP_UTILITY.getCurrentMessageRecipients(true).forEach(async function (fetchNumbers) {

            fetchedNumbers[fetchNumbers.id+'-_-'+fetchNumbers.number.replace(/\D/g,'')] = fetchNumbers.number.replace(/\D/g,'');

            let sentMsgItem = {
                id: "true_"+fetchNumbers.number.replace(/\D/g,'')+"@c.us",
                src: "",
                m: "Start Conversation",
                t: String(new Date().getTime()),
                ty: "chat"
            };

            let convItem = await UA_TPA_FEATURES.convertMsgItemToUAConversationSchema(sentMsgItem);
            await UA_APP_UTILITY.addConversationItemToConversationBox(convItem);

            let convIDClean = UA_APP_UTILITY.getCleanStringForHTMLAttribute(convItem.id);
            if($(`#CONV-ITEM-${convIDClean}`).length !== 0){
                $(`#CONV-ITEM-${convIDClean}`).attr('onclick', `UA_TPA_FEATURES.conversationViewChatsGetFunction('${convItem.name}');`)
            }

        });

        let selectedPhoneType = $('.ssfrpt-item.selected').text().trim().replaceAll(' ', '').replaceAll('\n', '_').split('_').filter(function (v) {
                                  return v != '';
                                });

        $('#DD_HOLDER_RECIPIENT_LIST').find('.dropdownInner').find('.ddi-item').each(function() {
            let thisnumberPhoneType = $(this).attr('data-ddlabel').trim().split(' ');
            thisnumberPhoneType = thisnumberPhoneType[thisnumberPhoneType.length-1];
            let fetchedNumberId = $(this).attr('onclick').split('-_-')[0].split("', '")[2];            
            let fetched_Number = $(this).attr('onclick').split('-_-')[1].split("', '")[0];
            if(fetchedNumbers[fetchedNumberId+'-_-'+fetched_Number] && fetchedNumbers[fetchedNumberId+'-_-'+fetched_Number] === fetched_Number && selectedPhoneType.indexOf(thisnumberPhoneType) >= 0) {
                let newFuncSet = $(this).attr('onclick').replace('UA_APP_UTILITY.dropDownOptionSelected', 'UA_TPA_FEATURES.chatViewFuncChange');
                $(this).show().attr('onclick', `${newFuncSet}`);
            }
            else {
                $(this).hide();
            }
        });         
        

    },
    conversationViewChatsGetFunction: function(to) {
        localStorage.setItem("zww_id",to.replace(/\D/g,'')+"@c.us");
        localStorage.setItem("zww_action","getChats_"+Date.now());

        let convID = `whatsapp:+${UA_APP_UTILITY.currentSender.value}::whatsapp:+${to.replace(/\D/g,'')}`;
        let convIDClean = UA_APP_UTILITY.getCleanStringForHTMLAttribute(convID);
        UA_APP_UTILITY.CURRENT_CONVERSATION_ID = convIDClean;
        $(`#CONV-ITEM-${convIDClean} .ucli-unread`).text('0').fadeOut();
        $(`#CONV-ITEM-${convIDClean}`).addClass('selected');
        UA_TPA_FEATURES.loadConversationInChatBox(convID);

    },
    chatViewFuncChange: function(event, ddElemId, label, value, callbackFunc, optionInvokeFunction) {
        UA_TPA_FEATURES.conversationViewChatsGetFunction(value.split('-_-')[1].replace(/\D/g,''));
        console.log(event, ddElemId, label, value, callbackFunc, optionInvokeFunction);
        $('#DD_HOLDER_RECIPIENT_LIST').find('.dropdownDisplayText').text(label);
        UA_APP_UTILITY.removeFocusCancellerOverlay();
    },
    convertMsgItemToUAConversationSchema: function(msgObj) {

        let inComing = msgObj.id.indexOf("false_") != -1;
        let to = 'whatsapp:+'+msgObj.id.split('@')[0].split('_')[1];
        let from = 'whatsapp:+'+UA_APP_UTILITY.currentSender.value;
        let customerNumber = to; //!inComing ? to : from;
        let conversationID = from + "::" + to; //!inComing ? from + "::" + to : to + "::" + from;
        customerNumber = customerNumber.replace("whatsapp:", '');
        let channelName = from.indexOf("whatsapp") === 0 ? "whatsapp" : "sms";
        
        let content = msgObj.m ? this.getSafeString(msgObj.m) : "";
        content = content.replace("\n","<br>");
        let mediaHTML = "";
        if(msgObj.ty === "image"){
            content="";
            mediaHTML = `<img src="${msgObj.src}" style="height: 20;"/>`;
        }
        let status = inComing?"received":"delivered";

        return {
                'id': conversationID,
                'name': customerNumber,
                'status': status,
                'channel': channelName === "whatsapp" ? UA_APP_UTILITY.MESSAGING_CHANNELS.WHATSAPP : UA_APP_UTILITY.MESSAGING_CHANNELS.SMS,
                'message': mediaHTML+content,
                'time': Number(msgObj.t)
            };

    },
    loadConversationInChatBox: function(convID){

        if(!UA_APP_UTILITY.currentSender) {
            UA_TPA_FEATURES.showErroWindowforWhatsAppWebConnect();
            return;
        }

        let customerNumber = convID.split("::")[1];
        let sender = convID.split("::")[0];
        let channel = customerNumber.indexOf("whatsapp") === 0 ? UA_APP_UTILITY.MESSAGING_CHANNELS.WHATSAPP : UA_APP_UTILITY.MESSAGING_CHANNELS.SMS;
        UA_APP_UTILITY.currentSender = {
            'channel': channel,
            'value': sender.replace("whatsapp:+", ''),
            'label': "WhatsApp "+sender.replace("whatsapp:+", '')
        };
        UA_APP_UTILITY.chatCurrentRecipient = {
            'channel': channel,
            'value': customerNumber.replace("whatsapp:+", ''),
            'address': customerNumber.replace("whatsapp:+", ''),
            'label': "WhatsApp "+customerNumber.replace("whatsapp:+", '')
        };
        $('.ua-conv-list-item.selected').removeClass('selected');
        $('#CONV-ITEM-'+UA_APP_UTILITY.getCleanStringForHTMLAttribute(convID)).addClass('selected').attr('data-last-msg-time', '0');
        $("#primary-send-btn").text('Send ' + UA_APP_UTILITY.currentSender.channel.label);
        $("#messageAttachmentInputHolder").toggle(UA_APP_UTILITY.currentSender.channel.attachmentSupported);
        if (typeof UA_TPA_FEATURES.messageChannelSelected === "function") {
            UA_TPA_FEATURES.messageChannelSelected();
        }
        $('.messageActionFooter').show();
        UA_APP_UTILITY.refreshMessageBox();
    }
    
};


