var UA_TPA_FEATURES = {
    workflowCode: {
        "SMS": {
            "to": "FILL_HERE",
            "sender": "FILL_HERE",
            "type": "FILL_HERE",
            "body": "FILL_HERE",
            "channel": "SMS",
            "module": "FILL_HERE",
            "recordId": "FILL_HERE",
            "ulgebra_webhook_authtoken": null
        },
        "WHATSAPP": {
            'from': "FILL_HERE",
            'to': "FILL_HERE",
            'type': 'text',
            'body': "FILL_HERE",
            "channel": "WhatsApp",
            "module": "FILL_HERE",
            "recordId": "FILL_HERE",
            "ulgebra_webhook_authtoken": null
        }

    },
    chosenMessgeTemplate: null,
    'clientIds': {},
    APP_SAVED_CONFIG: null,
    currentSelectedRoute: "MKT",
    getSupportedChannels: function () {
        return [UA_APP_UTILITY.MESSAGING_CHANNELS.SMS, UA_APP_UTILITY.MESSAGING_CHANNELS.WHATSAPP];
    },
    renderInitialElements: async function () {
        $("#ac_name_label_tpa .anl_servicename").text('Kaleyra');
        let balanceText = ""
        UA_TPA_FEATURES.getCurrentAccountOrBalanceInfo(async function (response) {
            if (response.code === 400 || response.code === 200) {
                // response.data.data.forEach(function (item) {
                //     balanceText += `Balance: ${item.usd_currency_symbol + item.value_in_usd}`
                // })
                $('#ac_name_label_tpa .ac_name_id').html("Activated");
            }else if(response.error.code === "RBC001"){
                UA_TPA_FEATURES.showReAuthorizeERROR();
                return
            }else if(response.error.code === "RBC212"){
                showErroWindow("Kaleyra Error", response.error.message,true)
                return
            }

            
        }, 1);

        UA_APP_UTILITY.getPersonalWebhookAuthtoken();

        var countryCallCodeArray = [];
        UA_APP_UTILITY.countryCallingCodeArray.forEach(item => {
            countryCallCodeArray.push({
                'label': `${item.name} (${item.dial_code})`,
                'value': item.dial_code
            });
        });

        UA_APP_UTILITY.renderSelectableDropdown('#ssf-new-recip-countrycode', 'Select country', countryCallCodeArray, 'UA_APP_UTILITY.log');
        $("#messageAttachmentInputHolder").remove() //removing wapp image upload element
    },
    messageSenderSelected: function () {
        UA_TPA_FEATURES.smsRouteHandler()
    },
    smsRouteHandler: function (route) {
        const routeElementRendered = $("#sms-form-item-item-route")[0]
        if (!routeElementRendered) {
            const routes = ["MKT", "OTP", "TXN", "DEFAULT"]
            let routeElm = ""
            routes.forEach(function (route, i) {
                routeElm += `<div class="ssf-fitem-radio-item">
                    <input style="margin:0;" type="radio" name="sms_route" value="${route}" onchange="UA_TPA_FEATURES.smsRouteHandler('${route}')" ${i === 0 ? "checked" : ""}>
                    <label onclick="$(this).siblings().trigger('click')" for="${route}">${route}</label>
                </div>`
            })
            $("#sms-form-item-item-sender").after(`
            <div class="ssf-fitem" id="sms-form-item-item-route" style="display:none;">
                <div class="ssf-fitem-label">
                    <span class="material-icons ssf-fitem-ttl-icon" title="SMS Route">route</span> <span class="ssf-fitem-ttl-label">Route</span>
                </div>
                <div class="ssf-fitem-ans ssf-fitem-radio" id="DD_HOLDER_SENDER_ROUTE">
                    ${routeElm}
                </div>
            </div>`)
        }
        if (UA_APP_UTILITY?.currentSender?.channel?.label === "SMS") {
            $("#sms-form-item-item-route").show()
        } else {
            $("#sms-form-item-item-route").hide()
        }
        if (route) {
            UA_TPA_FEATURES.currentSelectedRoute = route
        }
    },

    renderWorkflowBodyCode: function (accessToken) {
        UA_TPA_FEATURES.workflowCode.SMS.ulgebra_webhook_authtoken = accessToken.saas;
        UA_TPA_FEATURES.workflowCode.WHATSAPP.ulgebra_webhook_authtoken = accessToken.saas;
        UA_APP_UTILITY.addWorkflowBodyCode('sms', 'For Sending SMS', UA_TPA_FEATURES.workflowCode.SMS);
        UA_APP_UTILITY.addWorkflowBodyCode('whatsapp', 'For Sending Whatsapp', UA_TPA_FEATURES.workflowCode.WHATSAPP);
    },
    insertTemplateContentInMessageInput: function (templateId) {
        var curTemplateItem = UA_APP_UTILITY.fetchedTemplates[templateId];
        UA_TPA_FEATURES.chosenMessgeTemplate = curTemplateItem;
        $("#messageAttachmentInputHolder").hide();
        UA_APP_UTILITY.currentAttachedFile = null;
        $("#attachedfile").text('Attach file');
        $('#inputFileAttach').val('');
        if (curTemplateItem.templateType === "placeholder_template") {
            $("#primary-send-btn").text('Send WhatsApp').css('background-color', 'green');
            if (curTemplateItem.head_mediatype && curTemplateItem.head_mediatype !== "") {
                $("#attachedfile").text('Attach ' + curTemplateItem.head_mediatype);
                $("#messageAttachmentInputHolder").show();
            }
            //            if(curTemplateItem.head_media_url){
            //                $("#messageAttachmentInputHolder").show();
            //            }
            $("#ssf-fitem-template-placeholder-holder-listholder").html("");
            curTemplateItem.placeholders.forEach(item => {
                $("#ssf-fitem-template-placeholder-holder-listholder").append(`
                    <div class="ssf-temp-placehold-item">
                            <span class="ssf-temp-placehold-item-label">{{${item}}}</span> <span class="material-icons ssf-temp-placehold-item-icon">double_arrow</span> <input onblur="UA_APP_UTILITY.lastFocusedInputElemForPlaceholderInsert = 'ssf-templ-place-input-${item}'" id="ssf-templ-place-input-${item}" data-placeholder-id="${item}" class="ssf-temp-placehold-item-input" type="text" placeholder="Type or choose field from above">
                        </div>
                `);
            });
            if (curTemplateItem.placeholders.length > 0) {
                $("#ssf-fitem-template-placeholder-holder").show();
            } else {
                $("#ssf-fitem-template-placeholder-holder").hide();
            }
            $('#inp-ssf-main-message').attr('readonly', true);
        }
        else {
            $("#primary-send-btn").text('Send SMS').css('background-color', 'royalblue');
            $("#ssf-fitem-template-placeholder-holder").hide();
            $('#inp-ssf-main-message').removeAttr('readonly');
        }
        $('#inp-ssf-main-message').val(curTemplateItem.message).focus();
    },

    _getServiceTemplates: async function () {
        var templatesResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "/", "post");
        if (templatesResponse.code === 401 || templatesResponse.data.code === 401 || templatesResponse.data.message === "Authentication failed") {
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return;
        }
        return templatesResponse;
    },
    showReAuthorizeERROR: function (showCloseOption = false) {
        if ($('#inp_tpa_service_p_id').length > 0) {
            console.log('already showing reautherr');
            return;
        }
        showErroWindow("Authorization needed!", `You need to authorize your Kaleyra  Account to proceed. <br><br> 
        <div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
            <div style="
                font-size: 24px;
                font-weight: bold;
                margin-top: 20px;
                margin-bottom: 15px;
            ">SMS Channel Configuration</div>
            <div style="margin-left:20px">
                <div style="font-size: 16px;margin-bottom: 5px;margin-top: 10px;color: rgb(80,80,80);">Kaleyra SID</div>
                <div class="help_apikey_tip"><a target="_blank" href="https://in.kaleyra.io/api-keys">Get Here</a></div>
                <input id="inp_tpa_SID" type="text" placeholder="Enter your SID (Security Identifier)" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
            </div>
            <div style="margin-left:20px">
                <div style="font-size: 16px;margin-bottom: 5px;margin-top: 10px;color: rgb(80,80,80);">Kaleyra API Key</div>
                <div class="help_apikey_tip"><a target="_blank" href="https://in.kaleyra.io/api-keys">Get Here</a></div>
                <input id="inp_tpa_api_key" type="text" placeholder="Enter your API Key" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
            </div>
            <button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.saveTPAAPIKey(UA_APP_UTILITY.reloadWindow)">Authorize</button>
        </div>`, showCloseOption, false, 500);
        $('#inp_tpa_authkey').focus();
        UA_LIC_UTILITY.showExistingInvitedAdminDDHTML(true);
    },

    showReAuthorizeERRORWithClose: function () {
        UA_TPA_FEATURES.showReAuthorizeERROR();
    },

    saveTPAAPIKey: async function (callback) {
        let sid = $("#inp_tpa_SID").val();
        let api_key = $("#inp_tpa_api_key").val();
        if (!valueExists(sid)) {
            showErroMessage("Please Enter Enter your SID (Security Identifier)");
            $("#inp_tpa_SID").focus();
            return;
        }
        if (!valueExists(api_key)) {
            showErroMessage("Please Enter API Key");
            $("#inp_tpa_api_key").focus();
            return;
        }

        let data = {
            api_key,
            SID: sid
        }
        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', '__multiple_keys__', data, callback);
    },

    prepareAndSendSMS: function () {
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        if (!UA_APP_UTILITY.currentSender) {
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var senderId = UA_APP_UTILITY.currentSender.value;
        if (!valueExists(senderId)) {
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var recipNumbers = UA_APP_UTILITY.getCurrentMessageRecipients();
        var messageText = $("#inp-ssf-main-message").val().trim();
        if (!valueExists(messageText)) {
            showErroWindow('Message is empty!', "Kindly fill message to proceed.");
            return false;
        }
        if (recipNumbers.length === 0) {
            showErroWindow('Recipient list is empty!', "Kindly add recipients to send SMS.");
            return false;
        }
        if (messageText.length >= 160) {
            showErroWindow('Message is too long!', "Since your message contains more than 160 characters, message may fail in clickatell. <br>  <br>Anyway we're attempting to send the message... ");
        }
        if (!UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE) {
            showErroWindow("<div id='sending-title' class='info'>Sending</div>", `<div id='sending-content'></div>`, false, true);
        }
        const sendedMsgStatus = []
        const resultColorCorrection = () => {
            $(".sent-pan").each(function () {
                $(this).css("border-color", $(this).find(".sent-icon").css("color"))
                $(this).css("box-shadow", "1px 1px 3px " + $(this).find(".sent-icon").css("color"))
            })
        }
        recipNumbers.forEach(function (item, i) {
            item.number = item.number.replace("+", "")
            const templateAppliedMsg = UA_APP_UTILITY.getTemplateAppliedMessage(messageText, UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id])
            $("#sending-content").append(`<div class="sent-pan" id="sending-pan-${i}">
                <div class="sent-to">${item.number}</div>
                <div class="sent-msg info"><span class="material-icons sent-icon">more_horiz</span></div>
            </div>`)
            resultColorCorrection()
            let messageHistoryMap = {
                'contact': {
                    'name': UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].Full_Name : item.number
                },
                'from': senderId,
                'to': item.number,
                'message': templateAppliedMsg,
                'status': 'NOT_SENT',
                'module': UA_SAAS_SERVICE_APP.widgetContext.module,
                'moduleId': item.id,
                'channel': UA_APP_UTILITY.currentSender.channel.label
            };
            let callback = function (apiResponse) {
                if (apiResponse.status === 202) {
                    sendedMsgStatus.push({
                        success: true
                    })
                    $(`#sending-pan-${i}`).find(".sent-msg").toggleClass("info success").html(`Sent successfully <span class='material-icons sent-icon' title="${apiResponse?.data?.id || ""}">check_circle</span>`)
                    messageHistoryMap.status = "SENT";
                    UA_APP_UTILITY.resetMessageFormFields();
                } else {
                    sendedMsgStatus.push({
                        success: false
                    })
                    const errMsg = (apiResponse.error.message || apiResponse.error.error.error || "Something went wrong")
                    $(`#sending-pan-${i}`).find(".sent-msg").toggleClass("info danger").html(`Unable to send <span class='material-icons sent-icon' title="${errMsg}">error</span>`).slideDown()
                }
                resultColorCorrection()
                UA_SAAS_SERVICE_APP.addSentSMSAsRecordInHistory({ "message1": messageHistoryMap });

                const totalProcessedCount = recipNumbers.length
                const successCount = sendedMsgStatus.filter(status => status.success).length
                const failedCount = sendedMsgStatus.filter(status => !status.success).length
                $("#sending-title").text(`Sending messages ${recipNumbers.length}/${sendedMsgStatus.length}...`)
                if (failedCount === totalProcessedCount) {
                    $("#sending-title").text("Unable to send messages.").toggleClass("info danger")
                }
                if (successCount === totalProcessedCount) {
                    $("#sending-title").text("Sent successfully").toggleClass("info success")
                }
                if (successCount && failedCount) {
                    $("#sending-title").text(`All messages have been processed. total: ${totalProcessedCount}, sent: ${successCount}, failed: ${failedCount}`)
                }
                return apiResponse
            }
            const senderFunc = UA_APP_UTILITY.currentSender.channel.label === "SMS" ? "sendSMS" : "sendWhatsappMessage";

            UA_TPA_FEATURES[senderFunc](senderId, item.number, templateAppliedMsg, callback)
        })


    },
    sendSMS: function (sender, recipNumber, messageText, callback) {
        messagePayload = {
            "to": recipNumber,
            "sender": sender,
            "type": UA_TPA_FEATURES.currentSelectedRoute,
            "body": messageText
        }
        if (UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE) {
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        UA_TPA_FEATURES._proceedToSendSMSAndExecuteCallback(messagePayload, callback);
    },
    sendWhatsappMessage: function (sender, recipNumber, messageText, callback) {
        let messagePayload = {
            'to': recipNumber,
            'from': sender,
            'type': 'text',
            'body': messageText,
            'channel': 'WhatsApp'
        }
        if (UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE) {
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        UA_TPA_FEATURES._proceedToSendWhatsappMessageAndExecuteCallback(messagePayload, callback);
    },
    _proceedToSendWhatsappMessageAndExecuteCallback: async function (messagePayload, callback) {
        var sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, 'https://api.kaleyra.io/v1/{{SID}}/messages', "POST", messagePayload);
        callback(sentAPIResponse);
    },
    _proceedToSendSMSAndExecuteCallback: async function (messagePayload, callback) {
        var sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://api.kaleyra.io/v1/{{SID}}/messages", "POST", messagePayload);
        callback(sentAPIResponse);
    },

    getAPIResponse: async function (extensionName, url, method, data) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = { "Content-Type": "application/json" };
        await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({ appCode: extensionName, TPAService: "kaleyra", url: url, method: method, data: data, headers: headers, _ua_lic_adminUserID: UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID }).then((response) => {
            response = response.data;
            returnResponse = response;
            removeTopProgressBar(credAdProcess3);
            return true;
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
        return returnResponse;
    },

    getCurrentAccountOrBalanceInfo: async function (callback) {
        const promotionalBalance = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://api.kaleyra.io/v1/{{SID}}/messages", "POST", {});
        callback(promotionalBalance);
    },
};

