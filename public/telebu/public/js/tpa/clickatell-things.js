var UA_TPA_FEATURES = {
    workflowCode: {
        "SMS": {
            "messages": [
                {
                    "content": "FILL_HERE",
                    "to": "FILL_HERE",
                    "from": "FILL_HERE (optional)",
                }
            ],
            "channel": "SMS",
            "module": "FILL_HERE",
            "recordId": "FILL_HERE",
            "ulgebra_webhook_authtoken": null
        },
        "WHATSAPP": {
            "messages": [
                {
                    "content": "FILL_HERE",
                    "to": "FILL_HERE",
                    "from": "FILL_HERE (optional)",
                }
            ],
            "channel": "WHATSAPP",
            "module": "FILL_HERE",
            "recordId": "FILL_HERE",
            "ulgebra_webhook_authtoken": null
        }

    },
    chosenMessgeTemplate : null,
    'clientIds': {
        'clickatellforpipedrive': "75e9bb9450844c03",
        'clickatellforbitrix24' : "app.618df632e1e316.12060277",
        'clickatellforhubspotcrm' : "a1fc08b9-ceb5-4fff-98ef-6c1af7220e68"
    },
    APP_SAVED_CONFIG : null,
    getSupportedChannels: function(){
        return [ UA_APP_UTILITY.MESSAGING_CHANNELS.SMS, UA_APP_UTILITY.MESSAGING_CHANNELS.WHATSAPP ];
    },
    renderInitialElements: async function(){
        $("#ac_name_label_tpa .anl_servicename").text('Clickatell');
        UA_TPA_FEATURES.getCurrentAccountOrBalanceInfo(async function(resp){
            $('#ac_name_label_tpa .ac_name_id').text(appPrettyName + ' Balance : ' + resp.data.balance);
            UA_APP_UTILITY.addMessaegSender("default", UA_APP_UTILITY.MESSAGING_CHANNELS.SMS, "default");
            UA_APP_UTILITY.TPA_SENDERS_FETCH_COMPLETED();
        });        
        UA_APP_UTILITY.getPersonalWebhookAuthtoken();
       
        var countryCallCodeArray = [];
        UA_APP_UTILITY.countryCallingCodeArray.forEach(item=>{
            countryCallCodeArray.push({
                'label': `${item.name} (${item.dial_code})`,
                'value': item.dial_code
            });
        });
        
        UA_APP_UTILITY.renderSelectableDropdown('#ssf-new-recip-countrycode', 'Select country', countryCallCodeArray, 'UA_APP_UTILITY.log');
        $("#messageAttachmentInputHolder").remove() //removing wapp image upload element
    },
    
    renderWorkflowBodyCode :function(accessToken){
        
        UA_TPA_FEATURES.workflowCode.SMS.ulgebra_webhook_authtoken = accessToken.saas;        
        UA_TPA_FEATURES.workflowCode.WHATSAPP.ulgebra_webhook_authtoken = accessToken.saas;        
        UA_APP_UTILITY.addWorkflowBodyCode('sms', 'For Sending SMS', UA_TPA_FEATURES.workflowCode.SMS);
        UA_APP_UTILITY.addWorkflowBodyCode('whatsapp', 'For Sending Whatsapp', UA_TPA_FEATURES.workflowCode.WHATSAPP);
    },
    
    
    
    prepareAndSendSMS: function(){
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        if(!UA_APP_UTILITY.currentSender){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        var senderId = UA_APP_UTILITY.currentSender.value;
        if(!valueExists(senderId)){
            showErroWindow('Sender is empty!', "Kindly choose sender to proceed.");
            return false;
        }
        senderId = senderId=== "default"?"":senderId
        var recipNumbers = UA_APP_UTILITY.getCurrentMessageRecipients();
        var messageText = $("#inp-ssf-main-message").val().trim();
        if(!valueExists(messageText)){
            showErroWindow('Message is empty!', "Kindly fill message to proceed.");
            return false;
        }
        if(recipNumbers.length === 0){
            showErroWindow('Recipient list is empty!', "Kindly add recipients to send SMS.");
            return false;
        }
        if(messageText.length >= 160){
            showErroWindow('Message is too long!', "Since your message contains more than 160 characters, message may fail in clickatell. <br>  <br>Anyway we're attempting to send the message... ");
        }
        let messageHistoryMaps = recipNumbers.map(item => {
            return{
                'contact':{
                    'name': UA_APP_UTILITY.storedRecipientInventory[item.id] ? UA_APP_UTILITY.storedRecipientInventory[item.id].Full_Name : item.number
                },
                'from': senderId,
                'to': item.number,
                'message': UA_APP_UTILITY.getTemplateAppliedMessage(messageText, UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id]),
                'status': 'NOT_SENT',
                'module': UA_SAAS_SERVICE_APP.widgetContext.module,
                'moduleId': item.id,
                'channel': UA_APP_UTILITY.currentSender.channel.label
            };
        })

        UA_TPA_FEATURES.sendSMS(senderId, recipNumbers,messageText, function (apiResponse) {
            apiResponse = apiResponse.data || apiResponse.error
            showErroWindow("<div id='sending-title'>Sending</div>", `<div id='sending-content'></div>`, false, true);
            if (apiResponse.messages) {
                apiResponse.messages.forEach(msg => {
                    $("#sending-content").append(`<div class="sent-pan">
                            <div class="sent-to">${msg.to}</div>
                            <div class="sent-msg ${msg.accepted?"success":"danger"}">${msg.accepted?"Sent":msg.error.description} <span class="material-icons sent-icon">${msg.accepted?"check_circle":"error"}</span></div>
                        </div>`)
                    $(".sent-pan").each(function(){
                        $(this).css("border-color",$(this).find(".sent-icon").css("color"))
                        $(this).css("box-shadow","1px 1px 3px "+$(this).find(".sent-icon").css("color"))
                    })
                    let messageHistoryMap = messageHistoryMaps.filter((item)=>item.to.replace("+","") === msg.to.replace("+",""))[0]
                    messageHistoryMap.status = msg.accepted?"SENT":"NOT_SENT";
                    UA_SAAS_SERVICE_APP.addSentSMSAsRecordInHistory({"message1": messageHistoryMap});
                })
                $("#sending-title").html("Completed")
                UA_APP_UTILITY.resetMessageFormFields();
            }
            if(apiResponse.errorCode){
                $("#sending-title").html("Failed").addClass("danger")
                $("#sending-content").text(apiResponse.errorDescription).addClass("danger")
            }
            return apiResponse
        })
    },
    
    
    
    
    insertTemplateContentInMessageInput :function(templateId){
        var curTemplateItem = UA_APP_UTILITY.fetchedTemplates[templateId];
        UA_TPA_FEATURES.chosenMessgeTemplate = curTemplateItem;
        $("#messageAttachmentInputHolder").hide();
        UA_APP_UTILITY.currentAttachedFile = null;
        $("#attachedfile").text('Attach file');
        $('#inputFileAttach').val('');
        if(curTemplateItem.templateType === "placeholder_template"){
            $("#primary-send-btn").text('Send WhatsApp').css('background-color', 'green');
            if(curTemplateItem.head_mediatype && curTemplateItem.head_mediatype!==""){
                $("#attachedfile").text('Attach '+curTemplateItem.head_mediatype);
                $("#messageAttachmentInputHolder").show();
            }
//            if(curTemplateItem.head_media_url){
//                $("#messageAttachmentInputHolder").show();
//            }
            $("#ssf-fitem-template-placeholder-holder-listholder").html("");
            curTemplateItem.placeholders.forEach(item=>{
                $("#ssf-fitem-template-placeholder-holder-listholder").append(`
                    <div class="ssf-temp-placehold-item">
                            <span class="ssf-temp-placehold-item-label">{{${item}}}</span> <span class="material-icons ssf-temp-placehold-item-icon">double_arrow</span> <input onblur="UA_APP_UTILITY.lastFocusedInputElemForPlaceholderInsert = 'ssf-templ-place-input-${item}'" id="ssf-templ-place-input-${item}" data-placeholder-id="${item}" class="ssf-temp-placehold-item-input" type="text" placeholder="Type or choose field from above">
                        </div>
                `);
            });
            if(curTemplateItem.placeholders.length > 0){
                $("#ssf-fitem-template-placeholder-holder").show();
            }else{
                $("#ssf-fitem-template-placeholder-holder").hide();
            }
            $('#inp-ssf-main-message').attr('readonly', true);
        }
        else{
            $("#primary-send-btn").text('Send SMS').css('background-color', 'royalblue');
            $("#ssf-fitem-template-placeholder-holder").hide();
            $('#inp-ssf-main-message').removeAttr('readonly');
        }
        $('#inp-ssf-main-message').val(curTemplateItem.message).focus();
    },
    
    _getServiceTemplates: async function(){
        var templatesResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "/", "post");
        if(templatesResponse.code === 401 || templatesResponse.data.code === 401 || templatesResponse.data.message === "Authentication failed"){
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return;
        }
        return  templatesResponse;
    },
    
    _getServiceWATemplates: async function(silenceAuthError = false, page =1){
        var templatesResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, `https://rcsgui.karix.solutions/api/v1.0/template/${UA_TPA_FEATURES.APP_SAVED_CONFIG.waba_id}?status=APPROVED&page=`+page, "get");
        if(templatesResponse.code === 401 || templatesResponse.data.code === 401 || templatesResponse.data.message === "Authentication failed"){
            if(silenceAuthError){
                return {
                    data:{
                        data: []
                    }
                };
            }
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return;
        }
        return  templatesResponse;
    },
    
    showReAuthorizeERROR: function(showCloseOption = false){
        if($('#inp_tpa_api_key').length > 0){
            console.log('already showing reautherr');
            return;
        }
        showErroWindow("Authorization needed!", `You need to authorize your Clickatell Account to proceed. <br><br> 
        <div style="background-color: white;padding: 10px 20px 20px;border-radius: 5px;"> 
            <div style="
                font-size: 24px;
                font-weight: bold;
                margin-top: 20px;
                margin-bottom: 15px;
            ">SMS Channel Configuration</div>
            <div style="margin-left:20px">
                <div style="font-size: 16px;margin-bottom: 5px;margin-top: 10px;color: rgb(80,80,80);">Clickatell API Key</div>
                <div class="help_apikey_tip"><a target="_blank" href="https://portal.clickatell.com/#/integrations/omni">Create or Get Here</a></div>
                <input id="inp_tpa_api_key" type="text" placeholder="Enter your API Key" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
            </div>
            <!--div style="margin-left:20px">
                <div style="font-size: 16px;margin-bottom: 0px;margin-top: 10px;color: rgb(80,80,80);">Clickatell SMS Sender IDs</div>
                <div style="font-size:14px;color:silver;margin-bottom:5px">Provide comma separated in case of multiple sender IDs</div>
                <input id="inp_tpa_senders" type="text" placeholder="Enter Clickatell SMS Sender IDs" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
            </div-->
            
            <!--div style="
                font-size: 24px;
                font-weight: bold;
                margin-top: 20px;
                margin-bottom: 15px;
            ">WhatsApp Channel Configuration</div>
            <div style="margin-left:20px;margin-bottom:5px">
            <div style="font-size: 16px;margin-bottom: 5px;margin-top: 10px;color: rgb(80,80,80);">Karix WhatsApp WABA ID</div>
            <input id="inp_tpa_waba_id" type="text" placeholder="Enter WABA ID" style="padding: 5px 10px;border: 1px solid silver;border-radius: 3px;width: 100%;margin-bottom: 10px;font-size: 14px;box-sizing: border-box;">
            </div-->
            <button class="ua_service_login_btn ua_primary_action_btn" onclick="UA_TPA_FEATURES.saveTPAAPIKey(UA_APP_UTILITY.reloadWindow)">Authorize</button>
        </div>`, showCloseOption, false, 500);
        $('#inp_tpa_api_key').focus();
        UA_LIC_UTILITY.showExistingInvitedAdminDDHTML(true);
    },
    
    showReAuthorizeERRORWithClose: function(){
        UA_TPA_FEATURES.showReAuthorizeERROR();
    },
    
    saveTPAAPIKey: async function(callback){
        let authtoken = $("#inp_tpa_api_key").val();
        if(!valueExists(authtoken)){
            showErroMessage("Please Enter API Key");
            return;
        }
        // let smsSenders = $("#inp_tpa_senders").val();
//        if(!valueExists(smsSenders)){
//            showErroMessage("Please fill SMS Senders");
//            return;
//        }
        // let whatsAppWABAID = $("#inp_tpa_waba_id").val();
//        if(!valueExists(whatsAppWABAID)){
//            showErroMessage("Please fill WhatsApp API WABA ID");
//            return;
//        }
        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', 'authtoken', 
        authtoken,callback);
    },
    
    saveTPAWABAAPIKey: async function(callback){
        let authtoken = $("#inp_tpa_waba_api_key").val();
        if(!valueExists(authtoken)){
            showErroMessage("Please Enter API Key");
            return;
        }
        await UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', 'waba_authtoken', authtoken, callback);
    },

    
    sendSMS: function (sender, recipNumbers,messageText, callback) {
        let messagePayload = {
            messages: recipNumbers.map(item => {
                return {
                    "content": UA_APP_UTILITY.getTemplateAppliedMessage(messageText, UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[item.id]),
                    "to": item.number,
                    "from": sender,
                    "channel": UA_APP_UTILITY.currentSender.channel.label
                }
            })
        }
        UA_TPA_FEATURES._proceedToSendSMSAndExecuteCallback(messagePayload, callback);
    },   
    _proceedToSendSMSAndExecuteCallback: async function(messagePayload, callback){
        if(UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE){
            UA_APP_UTILITY.showMessagingWorkflowCode(messagePayload);
            return false;
        }
        var sentAPIResponse = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://platform.clickatell.com/v1/message", "POST",messagePayload);
        callback(sentAPIResponse);
    },

    getAPIResponse: async function (extensionName, url, method, data) {
        var returnResponse = null;
        var credAdProcess3 = curId++;
        showTopProgressBar(credAdProcess3);
        var headers = {"Content-Type": "application/json"};
        await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({appCode: extensionName, TPAService: "clickatell",url: url, method: method, data: data, headers: headers, _ua_lic_adminUserID: UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID}).then((response) => {
            response = response.data;
            returnResponse = response;
            removeTopProgressBar(credAdProcess3);
            return true;
        }).catch(err => {
            console.log(err);
            removeTopProgressBar(credAdProcess3);
            return false;
        });
        return returnResponse;
    },
    
    getCurrentAccountOrBalanceInfo: async function(callback){
        var response = await UA_TPA_FEATURES.getAPIResponse(extensionName, "https://platform.clickatell.com/v1/balance", "GET", {});
        if((response.code === 401 || response.code === 403 || response.message === "Authentication failed") || (response.data && (response.data.code === 401 || response.data.message === "Authentication failed"))){
            UA_TPA_FEATURES.showReAuthorizeERROR();
            return;
        }
        callback(response);
    },    
};