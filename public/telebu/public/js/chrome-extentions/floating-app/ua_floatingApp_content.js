

///////////////////////////////////////////////////////    UA_FLOAT_APP_CONTENT    ///////////////////////////////////////////////////////////////////////////

var UA_FLOAT_APP_CONTENT = {
    CURRENT_USER_DETAILS: {},
    UA_USER_APPS_LIST: [],
    CURRENT_SAAS_NAME: "",
    CURRENT_SAAS_MODULE: "",
    CURRENT_SAAS_MODULE_TYPE: "",
    CURRENT_SAAS_RECORDID: "",
    CURRENT_SAAS_APPS_LIST: [],
    CONTEXT: {},
    MINIMIZED_IFRAME_REFS: {},
    minimize_ua_iframe_count: 1,
    notificationsData: {},

    sendToFrame: async function(iframeId,data){
        document.querySelector(iframeId).contentWindow.postMessage(data,"*");
    },

    getCurrentUserDetails: async function(){
        UA_FLOAT_APP_CONTENT.sendToFrame("#ua_floating_config_frame",{action:"currentUserDetails"});
    },

    reloadParentWindow: async function(){
        window.location.reload();
    },

    uaSignOut: async function(){
        UA_FLOAT_APP_CONTENT.sendToFrame("#ua_floating_config_frame",{action:"uaSignOut"});
    },

    getSafeString: function (rawStr){
        if(typeof rawStr === "number"){
            rawStr = rawStr+"";
        }
        if (!rawStr || rawStr.trim() === "") {
            return "";
        }
        let elem = document.createElement("textarea");
        elem.innerText = rawStr+"";
        return elem.innerHTML;
    },
    
    getTimeString: function (previous, timeForToday = false) {

        previous = new Date(previous);
        var msPerMinute = 60 * 1000;
        var msPerHour = msPerMinute * 60;
        var msPerDay = msPerHour * 24;
        var msPerMonth = msPerDay * 30;
        var msPerYear = msPerDay * 365;

        var elapsed = new Date() - previous;
        var onlyTime = new Date(previous).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }).toLowerCase();
        if (elapsed < msPerMinute) {
            if(timeForToday){
                return onlyTime;
            }
            return Math.round(elapsed / 1000) + ' seconds ago';
        }
        else if (elapsed < msPerHour) {
            if(timeForToday){
                return onlyTime;
            }
            return Math.round(elapsed / msPerMinute) + ' minutes ago';
        }
        else if (elapsed < msPerDay) {
            if(timeForToday && Math.round(elapsed / msPerHour) < 5){
                return onlyTime;
            }
            return Math.round(elapsed / msPerHour) + ' hours ago';
        } 
        else if (elapsed < msPerMonth) {
            return Math.round(elapsed / msPerDay) + ' days ago';
        }
        else if (elapsed < msPerYear) {
            return Math.round(elapsed / msPerMonth) + ' months ago';
        }
        else {
            return Math.round(elapsed / msPerYear) + ' years ago';
        }
    },

    isCurrentUser: async function(data={}){

                //////////////////////////////////////////////////                         \\
                //                                              //                 \\              \\
                //                                              //                   
                UA_FLOAT_APP_CONTENT.uaFALoadFloatAppElement();     //=====>                        
                //                                              //                  \\              \\
                //                                              //                          \\
                //////////////////////////////////////////////////

        if(!data.userId){
            // document.querySelector("#ua_floating_config_frame").style.display = "block";
            UA_FLOAT_APP_CONTENT.loadUASignInBtn("#ua_addedApps_div");
        }
        else{
            document.querySelector("#ua_floating_config_frame").style.display = "none";
            UA_FLOAT_APP_CONTENT.CURRENT_USER_DETAILS = data.currentUserDetails;
            UA_FLOAT_APP_CONTENT.handleUAFloatingFlow(data);
        }
    },

    loadUASignInBtn: async function(parentId){
        if(!document.querySelector(parentId)) return;
        document.querySelector(parentId).innerHTML = "";
        var html_text =`<div id="uaFloatingAppsHolder" style="position: relative;display: flex;width: max-content;margin-right: 50px;height: max-content;">
                            <div class="ua-floating-app-item ua-init" onclick="document.querySelector('#ua_floating_config_frame').style.display == 'block'?document.querySelector('#ua_floating_config_frame').style.display = 'none':document.querySelector('#ua_floating_config_frame').style.display = 'block'" style="position: relative; width: 60px; height: 20px; text-align: center; background: royalblue; margin-left: 5px; margin-top: 2px; border-radius: 50px; padding: 10px 10px; cursor: pointer;color: white; font-size: 15px;">
                                <div class="ua-floating-app-item-icon">Sign In</div>
                            </div>
                        </div>`
        document.querySelector(parentId).innerHTML = html_text;
    
    },

    handleUAFloatingFlow: async function(data){
        var userAppsList = data.userAppsList;
        await userAppsList.forEach((appId)=>{
            if(UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME && appId.indexOf("for"+UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME) > -1 && UA_FLOAT_APP_CONTENT.CURRENT_SAAS_APPS_LIST.includes(appId) == false){
                UA_FLOAT_APP_CONTENT.CURRENT_SAAS_APPS_LIST.push(appId);
            }
        });
        UA_FLOAT_APP_CONTENT.renderUserAddedApps("#ua_addedApps_div");
    },

    renderUserAddedApps: async function(parentId){
        if(!document.querySelector(parentId)) return;
        document.querySelector(parentId).innerHTML = "";
        var html_text = ""
        ///// settings content holder
        html_text = html_text + `<div class="ua-floating-app-settings-content ua-init" style="display: none; position: absolute; background: white; width: max-content; height: max-content; top: 55px; box-shadow: 0px 0px 5px 1px #aaa; border-radius: 4px;">
                                    <div></div>
                                </div>`;
        ///// settings icon
        html_text = html_text +`<div id="ua-floating-app-settings" class="ua-floating-app-item ua-init" onclick="document.querySelector('.ua-floating-app-settings-content').style.display == 'block'?document.querySelector('.ua-floating-app-settings-content').style.display = 'none':document.querySelector('.ua-floating-app-settings-content').style.display = 'block'" style="position: relative; width: 30px; height: 30px; text-align: center; margin-left: 2px; left: 3px; margin-top: 3px; border-radius: 50px; padding: 5px 5px; cursor: pointer;">
                                    <div class="ua-floating-app-item-icon" style="position: relative; width: 100%; height: 100%;">
                                        <img class="ua-floating-app-settings-icon" src="https://img.icons8.com/external-others-inmotus-design/67/000000/external-Settings-basic-elements-others-inmotus-design.png" style="position: relative; width: 100%; height: 100%;">
                                    </div>
                                </div>`;
        ///// Apps List
        html_text = html_text + `<div id="uaFloatingAppsHolder" style="position: relative;display: flex;width: max-content;margin-right: 50px;height: max-content;">`;
        UA_FLOAT_APP_CONTENT.CURRENT_SAAS_APPS_LIST.forEach((appId)=>{
        html_text = html_text + `<div class="ua-floating-app-item ua-tooltip ua-init ${appId}" style="position: relative; width: 33px; height: 33px; text-align: center; /*background: #8f7373;*/ margin-left: 5px; margin-top: 3px; border-radius: 50px; padding: 2px 2px; border: 2px solid #aaa0; /*box-shadow: inset 0px 0px 1px 0px #aaa;*/">
                                        <div style="position: absolute; top: -5px;right: -7px;z-index: 10;">
                                            <span class="ua-app-alert-count ua-init ${appId}" onclick="UA_FLOAT_APP_CONTENT.renderAndShowFloatingAppNotif(event, '${appId}')" style="display: none;position: absolute;top: 0px;right: 0px;width: 10px; height: 10px;z-index: 1;font-size: 9px; color: white; background: crimson;padding: 3px 3px 3px 3px;border-radius: 50px; text-overflow: ellipsis;white-space: nowrap; overflow: hidden;cursor: pointer;">0</span>
                                            <div class="ua-init ua-floating-app-notif-holder"  style="display: none;position: relative;background: white; top: 17px; left: 45%; z-index: 1000;width: 170px;max-height: 250px;box-shadow: rgb(170 170 170) 0px 0px 5px 0px;font-size: 11px;font-family: system-ui;border-radius: 4px;">
                                                <div class="ua-init ua-floating-app-notif-top-actions" style="position: relative;width: 90%;left: 5%;flex-direction: row;font-size: 12px;padding: 15px 2px;border-bottom: 1px solid #e2e2e2;display: flex;">
                                                    <div class="ua-floating-app-notif-ttl" style="color: #332d2d;font-family: system-ui;font-size: 11px;">New notifications</div>
                                                    <div class="ua-floating-app-notif-clear-all" onclick="UA_FLOAT_APP_CONTENT.clearAllNewNotifications(event, '${appId}')" style="position: absolute;right: 5px;padding: 4px 4px;border-radius: 10px;color: crimson;font-size: 10px;cursor: pointer;">Clear all</div>
                                                </div>
                                                <div class="ua-init ua-floating-app-notif-items" style="position: relative;display: flex;flex-direction: column;width: 100%;left: 0%;font-size: 12px;white-space: normal;overflow: auto;max-height: 200px;"></div>
                                            </div>
                                        </div>
                                        <div class="ua-floating-app-item-icon ua-init" onclick="UA_FLOAT_APP_CONTENT.showFloatingAppFrame('${appId}')" style="position: relative; width: 100%; height: 100%; background: white; border-radius: 50px; cursor: pointer;">
                                            <img src="https://sms.ulgebra.com/js/chrome-extentions/floating-app/apps_logo/${appId.split("for")[0]}.png" style="position: relative; width: 100%; height: 100%; border-radius: 50px;">
                                        </div>
                                        <span class="ua-tooltiptext">${appId.split("for"+UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME)[0]}</span>
                                    </div>`        
        });

        html_text = html_text + "</div>"
        document.querySelector(parentId).innerHTML = html_text;
        ///// Load Settings Options
        UA_FLOAT_APP_CONTENT.renderSettingsContent();
        ///// LoadMinimizedCacheItems
        UA_FLOAT_APP_CONTENT.renderMinimizedCacheItems();
        ///// loadDirectUAppBtnsInSaas
        if(UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME){
            UA_FLOAT_APP_CONTENT.loadDirectUAppBtnsInSaas();
            if(UA_FLOAT_APP_CONTENT.SAAS_THINGS[UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME] && UA_FLOAT_APP_CONTENT.SAAS_THINGS[UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME].setMutationsInSaas){
                UA_FLOAT_APP_CONTENT.SAAS_THINGS[UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME].setMutationsInSaas();
            }
        }


    },

    renderSettingsContent: async function(){
        var html_text = "";
        if(UA_FLOAT_APP_CONTENT.CURRENT_USER_DETAILS){

            html_text = `<div style="position: relative; display: flex; padding: 5px 10px; width: max-content; height: max-content; align-items: center; justify-content: center;">
                            <div class="ua-floating-app-item-icon ua-init" style="position: relative; width: 30px; height: 30px; text-align: center; border-radius: 50px; padding: 5px 5px;">
                                <img class="ua-floating-app-settings-icon" src="${UA_FLOAT_APP_CONTENT.CURRENT_USER_DETAILS.photoURL?UA_FLOAT_APP_CONTENT.CURRENT_USER_DETAILS.photoURL:"https://img.icons8.com/external-gliphyline-royyan-wijaya/32/000000/external-account-nightlife-business-glyph-gliphyline-royyan-wijaya.png"}" style="position: relative; width: 100%; height: 100%; border-radius: 50px;">
                            </div>
                            <div class="ua-floating-app-item ua-init" onclick="UA_FLOAT_APP_CONTENT.uaSignOut()" style="position: relative; width: 60px; height: 10px; text-align: center; background: crimson; border-radius: 50px; padding: 10px 10px; cursor: pointer; color: white; font-size: 11px;">
                                <div class="ua-floating-app-item-icon">Sign Out</div>
                            </div>
                        </div>`

        }
        document.querySelector(".ua-floating-app-settings-content").innerHTML = html_text;
    },

    ListeningToNotificationsCountForUser: async function(data={}){
        if(!data) return;
        let notificationCountData = data.snapshot;
        UA_FLOAT_APP_CONTENT.APP_NOTIFICATIONS_COUNT = notificationCountData;
        Object.keys(notificationCountData).forEach((appId)=>{
            let count = notificationCountData[appId];
            count = count ? Math.ceil(parseInt(count) * 10)/10 : 0;
            document.querySelectorAll(`.ua-floating-app-item.${appId}`).forEach((elem)=>{
                elem.querySelector(".ua-app-alert-count").innerText = count;
                if(count == 0){
                    elem.querySelector(".ua-app-alert-count").style.display = "none";
                    elem.querySelector(".ua-init.ua-floating-app-notif-holder").style.display = "none";
                }
                else elem.querySelector(".ua-app-alert-count").style.display = "block";
            });
        });
    },

    renderAndShowFloatingAppNotif: async function(event, appId){
        return;
        if(!event) return;
        let notif_elem = event.target.parentElement.querySelector(".ua-init.ua-floating-app-notif-holder");
        if(notif_elem.style.display == "block"){
            notif_elem.style.display = "none";
            return;
        }
        
        UA_FLOAT_APP_CONTENT.notificationsData[appId] = [];
        document.querySelector("#ua_floating_config_frame").contentWindow.postMessage({action: "getUserAppNotificationsData", appId: appId},"*");
        
        let intervalCount = 0;
        let intervalId = await setInterval(function() {
            console.log(' in Interval callback with count ' + intervalCount);
            intervalCount++;
            if (intervalCount > 5 || UA_FLOAT_APP_CONTENT.notificationsData[appId].length > 0) {
                clearInterval(intervalId);
                console.log(UA_FLOAT_APP_CONTENT.notificationsData[appId]);
                UA_FLOAT_APP_CONTENT.renderUANotificationItemsUI(event, appId);
            }
        }, 500);
        notif_elem.style.display = "block";
    },

    renderUANotificationItemsUI: async function(event, appId){
        if(!event || !appId || !UA_FLOAT_APP_CONTENT.notificationsData[appId] || UA_FLOAT_APP_CONTENT.notificationsData[appId].length < 1) return;
        let items = UA_FLOAT_APP_CONTENT.notificationsData[appId];
        let container = event.target.parentElement.querySelector(".ua-init.ua-floating-app-notif-items");
        // items.forEach((item)=>{
        for(var i=items.length-1; i > -1; i--){
            let item = items[i];
            if(container.querySelector(`#uanotifid-${item.id}`)){
                continue;
            }
            isUnReadNotication = true;
            let notifItemHTML = `<div class="nspc-item ${isUnReadNotication ? 'unread': ''}" id="uanotifid-${item.id}" style="position: relative;width: 100%;box-shadow: 0px 0px 4px 0px #bdbdbd;margin: 3px 0px 3px 0px;border-radius: 2px;padding: 5px 0px;font-size: 11px;">
                                    <div class="nspc-item-time" style="position: relative;width: 34%;left: 33%;text-align: center;background: #eee;top: -5px;border-radius: 0px 0px 3px 3px;padding: 2px 0px;font-size: 9.5px;">
                                        ${UA_FLOAT_APP_CONTENT.getTimeString(item.ct, true)}
                                    </div>
                                    <a target="_blank" href="https://sms.ulgebra.com/app-notification-click?notificationId=${item.id}&appCode=${appId}&notificationTitle=${encodeURIComponent(item.nTtl)}&notificationContent=${encodeURIComponent(item.nMsg)}" style="color: #822e83;font-family: system-ui;"><div class="nspc-item-ttl">
                                        ${UA_FLOAT_APP_CONTENT.getSafeString(item.nTtl)}
                                    </div></a>
                                    <div class="nspc-item-content" style="position: relative;max-height: 60px;text-overflow: ellipsis;overflow: hidden;margin-top: 5px;color: #555;font-family: system-ui;font-size: 10px;padding: 2px 10px;">
                                        ${UA_FLOAT_APP_CONTENT.getSafeString(item.nMsg)}
                                    </div>
                                </div> `;
            container.insertAdjacentHTML("afterbegin", notifItemHTML);
        }
    },

    clearAllNewNotifications: async function(event, appId){
        event.target.parentElement.parentElement.style.display = "none";
        event.target.parentElement.parentElement.querySelector(".ua-init.ua-floating-app-notif-items").innerHTML = "";
        UA_FLOAT_APP_CONTENT.sendToFrame("#ua_floating_config_frame",{action:"markAllMyNotificationsAsTrue", appId: appId});
        return;
    },

    showFloatingAppFrame: async function(appId){
        // var context = await UA_FLOAT_APP_CONTENT["getCurrentSaasModule_"+UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME](console.log);
        let getModuleName = await UA_FLOAT_APP_CONTENT.SAAS_THINGS[UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME].getSaasModule();
        let entityId = await UA_FLOAT_APP_CONTENT.SAAS_THINGS[UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME].getSaasRecordIds();
        let getExtraParams = await UA_FLOAT_APP_CONTENT.SAAS_THINGS[UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME].getExtraParams()
        UA_FLOAT_APP_CONTENT.CONTEXT = {
            openMode: "modal",
            module: getModuleName,
            module_type: UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE,
            view_type: UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE, 
            ids_count: entityId.length,
            entityId: entityId
        };
        
        var params = `appCode=${appId}&disableSAASSdk=true`;
        Object.keys(UA_FLOAT_APP_CONTENT.CONTEXT).forEach((key)=>{
            params = params + `&${key}=${UA_FLOAT_APP_CONTENT.CONTEXT[key]}`;
        });
        
        params = params+getExtraParams;

        if(Object.keys(UA_FLOAT_APP_CONTENT.MINIMIZED_IFRAME_REFS).length > 0){
            let arr = Object.values(UA_FLOAT_APP_CONTENT.MINIMIZED_IFRAME_REFS);
            let obj = arr.find(o => o.url === `https://sms.ulgebra.com/send-sms?${params}`);
            if(obj && obj.id && document.querySelector(obj.id)){
                document.querySelector(obj.id).style.display = "block";
                return;
            }
        }

        if(!document.getElementById(`ua-float-app-div-${UA_FLOAT_APP_CONTENT.minimize_ua_iframe_count}`)){
            UA_FLOAT_APP_CONTENT.renderUAFIframeUi(`https://sms.ulgebra.com/send-sms?${params}`, appId, UA_FLOAT_APP_CONTENT.minimize_ua_iframe_count);
        }
        else{
            document.getElementById(`ua-float-app-iframe-${UA_FLOAT_APP_CONTENT.minimize_ua_iframe_count}`).style.display = "block";
        }
    },

    getAppPrettyNames: function(appCodeArray){
        var appPrettyNamesList =  []; 
        try{
            appCodeArray.forEach(appCode=>{
                appPrettyNamesList.push(UA_FLOAT_APP_CONTENT.capitalize(appCode.split("for")[0]) +" for "+ UA_FLOAT_APP_CONTENT.capitalize(appCode.split("for")[1]));
            });
        }
        catch(ex){
            console.log(ex);
        }
        return appPrettyNamesList;
    },

    capitalize: function(string){
        var c = '';
        var s = string.split(' ');
        for (var i = 0; i < s.length; i++) {
            c+= s[i].charAt(0).toUpperCase() + s[i].slice(1) + ' ';
        }
        return c.trim();
    },

    renderUAFIframeUi: async function(iframeUrl, appId, refId){
        var div = document.createElement('div');
            div.setAttribute('id', `ua-float-app-div-${refId}`);
            div.setAttribute('class', "ua-init ua-float-app-div");
            div.style.cssText = `position: fixed;top: 0px;left: 0px;width: 100%;height: 100%;background: rgba(0, 0, 0, 0.75);flex-flow: column wrap;z-index: 10000;`;
            
            div.innerHTML =`<div id="ua-float-app-iframe-holder-${refId}" class="ua-float-app-frame-holder" style="position: relative;top: 20px;width: 50%;height: 80%;margin-left: 25%;background: white;padding: 0px 0px 0px 0px;z-index: 1;font-weight: 600;color: #555151;display: flex;flex-direction: column;border-radius: 5px;box-shadow: 0px 0px 5px 0px #565656;">
                                    <div class="ua-floating-frame-actions" style="position: relative;width: 100%;height: 40px;color: #dfedf3;padding-top: 10px;background-color: #607d8b;border-radius: 5px 5px 0px 0px;">
                                    <div class="ua-floating-frame-actions-leftalign" style="position: absolute;display: flex;left: 20px;padding: 5px;font-weight: normal;font-size: 18px;">
                                        AppCursor - ${UA_FLOAT_APP_CONTENT.getAppPrettyNames([appId])[0]}
                                    </div>
                                    <div class="ua-floating-frame-actions-rightalign" style="position: absolute;display: flex; right: 0px;">
                                        <div class="ua-floating-frame-open" onclick="UA_FLOAT_APP_CONTENT.openNewTab_ua_iframe(event, ${refId})" style="position: relative; margin-right: 5px;padding: 5px;cursor: pointer;"> 
                                            <img style="position: relative; width: 13px;height: 13px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABmJLR0QA/wD/AP+gvaeTAAAA0klEQVRIie3WPw6CMBiH4VfjFSBewNHEaxDjeVyd8H4yuIsLiQfQweBAaxDaUsJXNYFf0oS/ffha0hSmjC1b4AqUwi0HEhecB0B1u9ShWQMuPUZlSN7ePDBkzc/gheV6cwqGpjWF4xvqCf47eAM8+VyJbpIfojs1ZU97GZTot/OBGCi+DcdApu4VjudE4TqaqfMD1ZyLwvcOVGctCT+AnTqOgJN64Qwse0C9Yb1LcFUaBAbZSr3hCNlKveGjAU2ofjiJfZc1KyClqlwaDb2fm2LOC7t2mvEPj5b+AAAAAElFTkSuQmCC">
                                        </div>
                                        <div class="ua-floating-frame-minimize" onclick="UA_FLOAT_APP_CONTENT.minimize_ua_iframe(event, ${refId}, '${appId}')" style="position: relative; margin-right: 5px;padding: 5px;cursor: pointer;"> 
                                            <img style="position: relative; width: 13px;height: 13px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABmJLR0QA/wD/AP+gvaeTAAAASElEQVRIie3QQQrAIBQD0aGn095/rVC8h64KXRULX6EwD7IOCUiSNOkEKtCDU4H8VnwtKL3TnkXH10tWyUAhfm0B0sYdkqQ/G5gTRCkVjqWEAAAAAElFTkSuQmCC">
                                        </div>
                                        <div class="ua-floating-frame-close" onclick="UA_FLOAT_APP_CONTENT.close_ua_iframe(event, ${refId})" style="position: relative; margin-right: 10px;padding: 5px;cursor: pointer;"> 
                                            <img style="position: relative; width: 13px;height: 13px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABmJLR0QA/wD/AP+gvaeTAAAAsklEQVRIie2WSwqAMAxER+9g0fsfR1e68Di6aUHFTyYdKKgDWbXjSyNNA/z6qgKAIUbI8PeMvwEwAlhizAA6Aur2DxtTigm2zEPce/T3XrAFfgU1g4+l2sZV2Z48rQXMwmVQBi6HJt39t+lhzXMNd7o7lfykXrgUaoVT0FqcXCX+HlVqpr1KoFJ4ketkaQ6e9poNte41wz1tUAIv9iwWGwTORhemDWb5Q8ySGtaE/l8v0ArSo+eVYaS+0wAAAABJRU5ErkJggg==">
                                        </div>
                                    </div>
                                </div> 
                                <iframe id="ua-float-app-iframe-${refId}" class="ua-float-app-iframe" src="${iframeUrl}" style="${UA_FLOAT_APP_CONTENT.ua_cssTexts["ua-float-app-iframe"]}"></iframe>
                            </div>`;
            document.body.appendChild(div);
    },

    close_ua_iframe: async function(event, id){
        console.log(id);
        document.getElementById("ua-float-app-div-" + id).remove();
        if(id && UA_FLOAT_APP_CONTENT.MINIMIZED_IFRAME_REFS[id]) delete UA_FLOAT_APP_CONTENT.MINIMIZED_IFRAME_REFS[id];
        localStorage.setItem(`uaFloatAppMinimizedCacheRef_${UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME}`, JSON.stringify(UA_FLOAT_APP_CONTENT.MINIMIZED_IFRAME_REFS));
    },

    openNewTab_ua_iframe: async function(event, id){
        console.log(id);
        let iframe_href = document.getElementById("ua-float-app-iframe-" + id).getAttribute("src");
        window.open(iframe_href, "_blank");
    },

    minimize_ua_iframe: async function(event, id, appId){
        console.log(id);
        let iframe_url = document.querySelector(`#ua-float-app-iframe-${id}`).getAttribute('src');
        let query = new URLSearchParams(new URL(iframe_url).search);
        // let query = JSON.parse('{"' + decodeURI(new URL(iframe_url).search.replace(/&/g, "\",\"").replace(/=/g,"\":\"")) + '"}')
        UA_FLOAT_APP_CONTENT.MINIMIZED_IFRAME_REFS[id] = {
            url: iframe_url,
            appId: appId,
            module: query.get("module"),
            module_type: query.get("module_type"),
            entityId: query.get("entityId")?query.get("entityId"):(query.get("recordId")?query.get("recordId"):(query.get("selectedIds")?query.get("selectedIds"):"")),
            ids_count: query.get("ids_count")
        };

        // var uaFloatAppMinimizedCacheRef = localStorage.getItem("uaFloatAppMinimizedCacheRef")?JSON.parse(localStorage.getItem("uaFloatAppMinimizedCacheRef")):[];
        localStorage.setItem(`uaFloatAppMinimizedCacheRef_${UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME}`, JSON.stringify(UA_FLOAT_APP_CONTENT.MINIMIZED_IFRAME_REFS));

        document.getElementById("ua-float-app-div-" + id).style.display = "none";
        UA_FLOAT_APP_CONTENT.minimize_ua_iframe_count += 1;
        UA_FLOAT_APP_CONTENT.insertMinimizedFrameItem(id);
    },

    insertMinimizedFrameItem: async function(id){
        if(!document.querySelector("#ua-minimized-contain-outer")) UA_FLOAT_APP_CONTENT.renderMinimizeFrameHolder();
        let frame_data = UA_FLOAT_APP_CONTENT.MINIMIZED_IFRAME_REFS[id];
        let html_text = `<div id="ua-minimized-item-${id}" class="ua-minimized-item ua-init" onclick="UA_FLOAT_APP_CONTENT.showUAMinimizedFrame(event, ${id})" style="position: relative;display: flex;padding: 5px 5px;width: max-content;height: max-content;align-items: center;justify-content: center;text-overflow: ellipsis;white-space: nowrap;box-shadow: 1px 1px 5px 0px #aaa;border-radius: 4px;margin: 0px 0px 0px 5px;margin-top: 1px;cursor: pointer;">
                            <div class="ua-minimized-item-icon ua-init" style="position: relative; width: 30px; height: 30px; text-align: center; border-radius: 50px; padding: 5px 5px;">
                                <img class="ua-minimized-app-icon" src="https://sms.ulgebra.com/js/chrome-extentions/floating-app/apps_logo/${frame_data.appId.split("for")[0]}.png" style="position: relative; width: 30px; height: 30px; border-radius: 50px;">
                            </div>
                            <div class="ua-minimized-item-details ua-init" style="position: relative;width: max-conten;text-align: left;border-radius: 50px;padding: 5px 10px;font-size: 11px;">
                                <div style="position: relative; display: flex;">
                                    <div style="position: relative; font-weight: bold;margin-right: 10px;color: #525050;font-size: 12px;">${frame_data.module?frame_data.module.replace("_"," "):""}</div>
                                    ${frame_data.module_type?`<div style="position: relative;padding: 1px 8px;outline: 1px solid #aaa;border-radius: 2px;box-shadow: 1px 1px 3px 0px #aaa;">${frame_data.module_type}</div>`:""}
                                </div>
                                <div style="position: relative;display: inline-flex;margin-top: 3px;">
                                    <div style="width: max-content;position: relative;text-overflow: ellipsis;overflow: hidden;">${frame_data.ids_count} records</div>
                                </div>
                            </div>
                            <div data-id="${id}" class="ua-init" style="position: relative;width: 13px;height: 13px;padding: 2px 10px;text-align: center;font-size: 10px;font-weight: 900;color: #333;font-family: sans-serif;cursor: pointer;">
                                <div id="ua-minimized-item-close-${id}" class="ua-minimized-item-close" style=""> &#x2715 </div>
                            </div>
                        </div>`;
        document.querySelector("#ua-minimized-contain-inner").insertAdjacentHTML('afterbegin', html_text);
        document.querySelector(`#ua-minimized-item-close-${id}`).addEventListener("click", async function(event){
            document.getElementById("ua-minimized-item-" + id).remove();
            UA_FLOAT_APP_CONTENT.close_ua_iframe(event, id);
        });
    },

    renderMinimizeFrameHolder: async function(){
        var div = document.createElement('div');
            div.setAttribute('id', `ua-minimized-contain-outer`);
            div.setAttribute('class', "ua-init ua-minimized-container-outer");
            div.style.cssText = `position: fixed;bottom: 0px; left: 0px;width: 100%; min-height: 0px; height: max-content;background: rgba(170, 170, 170, 0.35);z-index: 10000;`;
            div.innerHTML =`<div class="ua-minimize-dragElem" style="position: absolute;top: -20px;left: 50%;height: 20px;background: white;box-shadow: 0px -3px 5px 0px #aaa;padding: 0px 8px;border-radius: 8px 8px 0px 0px;z-index: 1;cursor: pointer;">
                                <img class="ua-minimize-expand-img" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABmJLR0QA/wD/AP+gvaeTAAAAbElEQVRIie2TQQqAIBBFn9C9PE676Cq2qS6bbUaQEJmYbDUPPoLKf4oIjvM3QTKsPAHHCEkpz5JPJXX5JckyZ5Y8yxdgriSmm7TKC2ZJr/yVZOpIyoOuwNZYP2XcVUduEICo2BcZ+Dccx1FwA31qIBfHhxayAAAAAElFTkSuQmCC">
                            </div>
                            <div id="ua-minimized-contain-inner" style="display: none; position: relative;width: 100%;height: max-content;padding: 3px;background: white;box-shadow: 0px 0px 5px 1px #aaa;overflow-x: auto;"></div>`;
        document.body.appendChild(div);

        UA_FLOAT_APP_CONTENT.ua_dragElement(document.querySelector(".ua-minimize-dragElem"), document.querySelector(".ua-minimize-dragElem"), false);
        document.querySelector("#ua-minimized-contain-outer").addEventListener("click", async function(){
            if(document.querySelector("#ua-minimized-contain-inner").style.display == "none"){
                document.querySelector("#ua-minimized-contain-inner").style.display = "flex";
                document.querySelector(".ua-minimize-expand-img").style.transform = "rotate(180deg)";
            }
            else{
                document.querySelector("#ua-minimized-contain-inner").style.display = "none";
                document.querySelector(".ua-minimize-expand-img").style.transform = "rotate(0deg)";
            }
        });        
    },

    renderMinimizedCacheItems: async function(){
        var uaFloatAppMinimizedCacheRef = localStorage.getItem(`uaFloatAppMinimizedCacheRef_${UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME}`)?JSON.parse(localStorage.getItem(`uaFloatAppMinimizedCacheRef_${UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME}`)):{};
        Object.keys(uaFloatAppMinimizedCacheRef).forEach((item)=>{
            UA_FLOAT_APP_CONTENT.MINIMIZED_IFRAME_REFS[UA_FLOAT_APP_CONTENT.minimize_ua_iframe_count] = uaFloatAppMinimizedCacheRef[item];
            UA_FLOAT_APP_CONTENT.insertMinimizedFrameItem(UA_FLOAT_APP_CONTENT.minimize_ua_iframe_count);
            UA_FLOAT_APP_CONTENT.minimize_ua_iframe_count += 1;
        });
    },

    showUAMinimizedFrame: async function(event, id){
        if(event.target.id.includes("ua-minimized-item-close")) return;
        if(document.getElementById("ua-float-app-div-" + id)) document.getElementById("ua-float-app-div-" + id).style.display = "block";
        else if(UA_FLOAT_APP_CONTENT.MINIMIZED_IFRAME_REFS[id]){
            let iframeRef = UA_FLOAT_APP_CONTENT.MINIMIZED_IFRAME_REFS[id];
            UA_FLOAT_APP_CONTENT.renderUAFIframeUi(iframeRef.url, iframeRef.appId, id);
        }
        document.getElementById("ua-minimized-item-" + id).remove();
        delete UA_FLOAT_APP_CONTENT.MINIMIZED_IFRAME_REFS[id];
    },

    loadDirectUAppBtnsInSaas: async function(){
        if(!UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME){
            return;
        }
        // var processRef = UA_FLOAT_APP_CONTENT.SAAS_THINGS[saasId][moduleId];
        var processRef = {};
        possibleForDuplicatesRef = ["pipedrive"];
        if(UA_FLOAT_APP_CONTENT.SAAS_THINGS[UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME].getRenderConfiguation && typeof UA_FLOAT_APP_CONTENT.SAAS_THINGS[UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME].getRenderConfiguation === "function"){
            processRef = await UA_FLOAT_APP_CONTENT.SAAS_THINGS[UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME].getRenderConfiguation();
            if(!processRef || !processRef.place_id || !document.querySelector(processRef.place_id)){
                return;
            }
        }
        var uaFloatingAppsHolderNode = document.getElementById("uaFloatingAppsHolder");
        var uaFloatingAppsHolderClone = uaFloatingAppsHolderNode.cloneNode(true);
            uaFloatingAppsHolderClone.style["margin-right"] = "0px";

        var addedApps_div = document.createElement('div')
            addedApps_div.setAttribute('id',"ua_addedApps_btn");

        if(processRef.parent_addClassName && processRef.parent_addClassName.target && processRef.parent_addClassName.className && document.querySelector(processRef.parent_addClassName.target)){
            if(possibleForDuplicatesRef.includes(UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME)){
                document.querySelectorAll(processRef.parent_addClassName.target).forEach((prnEle)=>{
                    prnEle.classList.add(processRef.parent_addClassName.className);
                })
            }
            else{
                document.querySelector(processRef.parent_addClassName.target).classList.add(processRef.parent_addClassName.className);
            }
        }
        if(processRef.parent_addCss && processRef.parent_addCss.target && processRef.parent_addCss.css && document.querySelector(processRef.parent_addCss.target)){
            if(possibleForDuplicatesRef.includes(UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME)){
                document.querySelectorAll(processRef.parent_addCss.target).forEach((prnEle)=>{
                    prnEle.style.cssText = prnEle.style.cssText + processRef.parent_addCss.css;
                })
            }
            else{
                document.querySelector(processRef.parent_addCss.target).style.cssText = document.querySelector(processRef.parent_addCss.target).style.cssText + processRef.parent_addCss.css;
            }
        }
        addedApps_div.style.cssText = processRef.css;
        addedApps_div.appendChild(uaFloatingAppsHolderClone);

        if(possibleForDuplicatesRef.includes(UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME) && document.querySelector("#ua_addedApps_btn")){
            let elem = document.querySelector("#ua_addedApps_btn");
            let elemRefs = document.querySelectorAll(processRef.place_id);
            elemRefs.forEach((elemRef)=>{
                if(processRef.position && (processRef.position == "beforebegin"|| processRef.position == "afterend") && !elemRef.parentElement.querySelector("#ua_addedApps_btn")){
                    elemRef.insertAdjacentElement(processRef.position, addedApps_div);
                }
                else if(processRef.position && (processRef.position == "beforeend" || processRef.position == "afterbegin") && !elemRef.querySelector("#ua_addedApps_btn")){
                    elemRef.insertAdjacentElement(processRef.position, addedApps_div);
                }
            });
        }
        else if(!document.querySelector("#ua_addedApps_btn") && processRef.place_id){
            UA_FLOAT_APP_CONTENT.uaInsertFloatAppBtns(processRef.place_id, addedApps_div, processRef.position)
        }

        // UA_FLOAT_APP_CONTENT.uaInsertFloatAppBtns(processRef.place_id, addedApps_div, processRef.position)

    },

    uaInsertFloatAppBtns: async function(placedRef, node, position){
        if(position) document.querySelector(placedRef).insertAdjacentElement(position, node);
        else document.querySelector(placedRef).appendChild(node);
    },



///////////////////////////////////////////////////////    FLOAT APP ICON REACTIONS THINGS    ///////////////////////////////////////////////////////////////////////////

    ua_cssTexts: {
        "ulg-float-app-btn-dragzone": `position: relative;display: flex;width: 35px;height: 35px;right: 0px;top: 1.5px;z-index: 10000;padding: 5px;cursor: pointer;background: url("https://sms.ulgebra.com/js/chrome-extentions/floating-app/apps_logo/ulgebra_trans_logo.png") 7px 7px / 30px 30px no-repeat rgb(255, 255, 255);border: 1px solid rgb(170, 170, 170);box-shadow: 0px 0px 5px 1px #6f6d6d;border-radius: 50px;background-repeat: no-repeat;animation: spin 1.5s cubic-bezier(0.26, 0.01, 0.38, 1.01);`,
                                        
        "ulg-float-app-btn-dragable": `display: flex;position: fixed;z-index: 10000;bottom: 15%;right: 8.6%;border: none;box-shadow: 2px 4px 18px rgba(0, 0, 0, 0.2);transition: border-color 0.2s, box-shadow 0.2s;background: #ffffff14;width: max-content;height: max-content;border-radius: 20px;`,

        "ua_addedApps_div": `display: none;padding: 2px 5px 4px 0px;position: absolute;width: 45px;height: max-conten;background: rgb(255 255 255);border-radius: 50px;transition-property: width;transition-duration: 2s;right: 0px;box-shadow: 0px 0px 10px 0px #aaa;`,

        "ua-send-float-app-close": `position: relative;right: 0px;top: 0px;width: 95%;text-align: right;height: 30px;padding: 6px 9px;font-weight: 600;color: #555151;cursor: pointer;`,

        "ua-float-app-iframe": `position: relative;width: 100%;height: calc(100% - 30px);border: none;border-radius: 5px;border-top: 1px solid #aaa;`
    },

    uaFALoadFloatAppElement: async function (params = null){
        params = {
            "id": "ulg-float-app-btn-dragzone",
            "class": "ua-init",
            "css": UA_FLOAT_APP_CONTENT.ua_cssTexts["ulg-float-app-btn-dragzone"],
            "text": "",
            "parent-id": "body",
            "elemType": "div",
            "url": `https://sms.ulgebra.com/index.html?parent_url=${encodeURIComponent(location.href)}`
        };  

        if(!document.getElementById("ulg-float-app-btn-dragable"))
        {
            var dragable_div = document.createElement('div');
                dragable_div.setAttribute('id', "ulg-float-app-btn-dragable");
                dragable_div.style.cssText = UA_FLOAT_APP_CONTENT.ua_cssTexts["ulg-float-app-btn-dragable"];

            var addedApps_div = document.createElement('div')
                addedApps_div.setAttribute('id',"ua_addedApps_div");
                addedApps_div.style.cssText = UA_FLOAT_APP_CONTENT.ua_cssTexts["ua_addedApps_div"];

            var div = document.createElement('div');
                div.setAttribute('id', params.id);
                div.setAttribute('class', params.class);
                div.innerHTML = params.text;
                div.style.cssText = params.css;
            
            var defaultLoc = localStorage.getItem("uaFloatAppDefaultLocaion_XY")?JSON.parse(localStorage.getItem("uaFloatAppDefaultLocaion_XY")):{};
            if(defaultLoc && defaultLoc.x && defaultLoc.y){
                dragable_div.style.top = defaultLoc.x;
                dragable_div.style.left = defaultLoc.y;
            }

            dragable_div.appendChild(addedApps_div);
            dragable_div.appendChild(div);
            document.querySelector(params['parent-id']).appendChild(dragable_div);

            const dragable = document.getElementById("ulg-float-app-btn-dragable"),
            dragzone = document.getElementById("ulg-float-app-btn-dragzone");
            UA_FLOAT_APP_CONTENT.ua_dragElement(dragable, dragzone);

            document.getElementById( params.id).addEventListener('click',function(event){
                UA_FLOAT_APP_CONTENT.ua_rotateUlgLogo("start_rotate");   
                UA_FLOAT_APP_CONTENT.ua_floatingAppToggle();         
                setTimeout(()=> UA_FLOAT_APP_CONTENT.ua_rotateUlgLogo("stop_rotate"),1500);
            });
        }
    },

    ua_rotateUlgLogo: async function(action) {
        if(action == "start_rotate") {
            document.getElementById("ulg-float-app-btn-dragzone").style.animation = "spin 1.5s cubic-bezier(0.26, 0.01, 0.38, 1.01) infinite";
        }
        else if(action == "stop_rotate") {
            document.getElementById("ulg-float-app-btn-dragzone").style.animation = "spin 1.5s cubic-bezier(0.26, 0.01, 0.38, 1.01)";
        }
    },

    ua_floatingAppToggle: async function(){
        //////// extra loadDirectUAppBtnsInSaas render func
        if(UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME){
            UA_FLOAT_APP_CONTENT.loadDirectUAppBtnsInSaas();
        }
        ////////
        if(!document.getElementById("ua_addedApps_div").style.width || document.getElementById("ua_addedApps_div").style.width == "max-content" || document.getElementById("ua_addedApps_div").style.width != "0px") {
            document.getElementById("ua_addedApps_div").style.width = "0px";
            document.getElementById("ua_addedApps_div").style.display = "none";
        }
        else {
            document.getElementById("ua_addedApps_div").style.display = "flex";
            document.getElementById("ua_addedApps_div").style.width = "max-content";
            
        }
    },

    ua_dragElement: async function(element, dragzone, isGlob = true){
        let pos1 = 0,
        pos2 = 0,
        pos3 = 0,
        pos4 = 0;
        const ua_dragMouseUp = () => {
            document.onmouseup = null;
            document.onmousemove = null;
            element.classList.remove("drag");
        };

        const ua_dragMouseMove = (event) => {
            event.preventDefault();
            pos1 = pos3 - event.clientX;
            pos2 = pos4 - event.clientY;
            pos3 = event.clientX;
            pos4 = event.clientY;
            if(isGlob){
                element.style.top = `${element.offsetTop - pos2}px`;
                element.style.left = `${element.offsetLeft - pos1}px`;
                localStorage.setItem("uaFloatAppDefaultLocaion_XY",JSON.stringify({x:element.style.top,y:element.style.left}));
            }
            else{
                element.style.bottom = `${element.offsetTop - pos2}px`;
                element.style.left = `${element.offsetLeft - pos1}px`;
            }
        };

        const ua_dragMouseDown = (event) => {
            event.preventDefault();
            pos3 = event.clientX;
            pos4 = event.clientY;
            element.classList.add("drag");
            document.onmouseup = ua_dragMouseUp;
            document.onmousemove = ua_dragMouseMove;
        };
        
        if(isGlob){
            element.style["padding"] = "0px";
            element.style["padding-left"] = "0px";
        }
        dragzone.onmousedown = ua_dragMouseDown;
    }
};

    
///////////////////////////////////////////////////////     Mutation Listeners     ///////////////////////////////////////////////////////////
    
    UA_FLOAT_APP_CONTENT.isMutationObserverRunning = false;
    UA_FLOAT_APP_CONTENT.runMutationObserver = async function(type, matchData = null, delay_sec = 2000){
        if(type == "MutationObserver"){
            UA_FLOAT_APP_CONTENT.isMutationObserverRunning = true;
            var observer = new MutationObserver(function(mutations) {
                mutations.forEach(function(mutationRecord) {
                    // console.log('style changed!');
                    let url = location.href;
                    if (url !== window_location_href) {
                        window_location_href = window.location.href;
                        if(UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME){
                            if(matchData.callback) setTimeout(()=>(matchData.callback(2000)), delay_sec);
                            if(!matchData.callback) setTimeout(()=>(UA_FLOAT_APP_CONTENT.loadDirectUAppBtnsInSaas(2000)), delay_sec);
                        }  
                    }
                });    
            });
            
            var targetElem = document.querySelector(matchData.target);
            observer.observe(targetElem, matchData.options);
        }
        if(type == "historyChange"){
            UA_FLOAT_APP_CONTENT.isMutationObserverRunning = true;
            (function(history){
                var pushState = history.pushState;
                history.pushState = function(state) {
                    // console.log('I am called from pushStateHook');
                    pushState.apply(history, arguments);
                    setTimeout(()=>(UA_FLOAT_APP_CONTENT.loadDirectUAppBtnsInSaas(2000)), delay_sec);
                    return ;
                };
            })(window.history);
        }
        if(type == "DOMSubtreeModified"){
            UA_FLOAT_APP_CONTENT.isMutationObserverRunning = true;
            document.body.addEventListener('DOMSubtreeModified', function () {
                if(UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME && UA_FLOAT_APP_CONTENT["getCurrentSaasModule_"+UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME] && window_location_href != window.location.href){
                    window_location_href = window.location.href;
                    // setTimeout(()=>(UA_FLOAT_APP_CONTENT["getCurrentSaasModule_"+UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME](UA_FLOAT_APP_CONTENT.loadDirectUAppBtnsInSaas)), 2000);
                }
            }, false);
        }
    };



////////////////////////////////////////////////////// END OF THE UA_FLOAT_APP_CONTENT OBJECT //////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





///////////////////////////////////////////////////////    LOAD INIT IFRAME FOR CONFIG    //////////////////////////////////////////////////////
console.log(window.location.href);

if(window.location.href){
    var window_location_href = window.location.href;
    if(window.location.href.includes("https://crm.zoho.")) UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME = "zohocrm";
    if(window.location.href.includes("https://desk.zoho.")) UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME = "zohodesk";
    if(window.location.href.includes("https://books.zoho.")) UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME = "zohobooks";
    if(window.location.href.includes("https://app.hubspot.")) UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME = "hubspotcrm";
    if(window.location.href.includes(".pipedrive.")) UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME = "pipedrive";
    if(window.location.href.includes("https://app.futuresimple.com/")) UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME = "zendesksell";
    if(window.location.href.includes(".myfreshworks.com/crm/sales/")) UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME = "freshworkscrm";
    if(window.location.href.includes(".bitrix24.")) UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME = "bitrix24";
    if(window.location.href.includes(".leadsquared.")) UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME = "leadsquared";
    if(window.location.href.includes(".freshdesk.")) UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME = "freshdesk";
    if(window.location.href.includes("https://recruit.zoho.")) UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME = "zohorecruit";
    if(window.location.href.includes("https://bigin.zoho.")) UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME = "zohobigin";
    if(window.location.href.includes(".dynamics.com")) UA_FLOAT_APP_CONTENT.CURRENT_SAAS_NAME = "dynamicscrm";
}

if(!document.getElementById("ua_floating_config_frame")){
    var config_iframe = document.createElement('iframe');
    config_iframe.setAttribute("id", "ua_floating_config_frame");
    config_iframe.style.cssText = "display: none; position: fixed; width: 300px; height: 340px; top: 1rem; z-index: 100; right: 0px; border: none; box-shadow: -1px 0px 4px 0px #aaa; border-radius: 10px 0px 0px 10px;";
    config_iframe.setAttribute('src', "https://sms.ulgebra.com/js/chrome-extentions/floating-app/ua-floating-config-main.html");
    document.body.appendChild(config_iframe);
}

window.addEventListener('message', function(event){
    if (event.data && event.data.ulgId && event.data.ulgId == "UA_FLOAT_APP_CONTENT") {
        var data = event.data;
        if(data.action && typeof UA_FLOAT_APP_CONTENT[data.action] === "function"){
            UA_FLOAT_APP_CONTENT[data.action](data);
        }
        else if(data.action && data.action == "setVariable" && typeof UA_FLOAT_APP_CONTENT[data.action] !== "function"){
            if(data.key) UA_FLOAT_APP_CONTENT[data.variableName][data.key] = data.value;
            else UA_FLOAT_APP_CONTENT[data.variableName] = data.value;
        }
    }
}, false);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////   SAAS FUNCTIONS ( Usage - DETAILS , LISTENER , BUTTONS )   //////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    UA_FLOAT_APP_CONTENT.SAAS_THINGS = {

////////////////////////////////////=>
        zohocrm: {
            "url_module_ref": ["Contacts", "Leads", "Potentials", "Accounts"],
            "css": "padding: 2px 5px 4px 0px; position: relative; width: max-content; background: rgba(255, 255, 255, 0); border-radius: 50px; /*box-shadow: rgb(170 170 170) 0px 0px 10px 0px;*/ display: inline-block; right: 15px; top: 12px;",
            "Contacts": {
                detail:{"css": "","place_id": ".pL10.dvmo.send_mailbtndetailpage.alignright","position": "afterbegin"},
                list:{"css": "padding: 0px;top: -8px;","place_id": "lyte-td.alignRight","position": "afterbegin",
                    "parent_addCss": {"target": "lyte-td.alignRight", "css":"display: flex; position: absolute; right: 0;"}
                },
            },
            "Leads": {
                detail:{"css": "","place_id": ".pL10.dvmo.send_mailbtndetailpage.alignright","position": "afterbegin"},
                list:{"css": "","place_id": "lyte-td.alignRight","position": "afterbegin"}
            },
            "Deals": {
                detail:{"css": "","place_id": ".pL10.dvmo.send_mailbtndetailpage.alignright","position": "afterbegin"},
                list:{"css": "","place_id": "lyte-td.alignRight","position": "afterbegin"}
            },
            "Accounts": {
                detail:{"css": "","place_id": ".pL10.dvmo.send_mailbtndetailpage.alignright","position": "afterbegin"},
                list:{"css": "","place_id": "lyte-td.alignRight","position": "afterbegin"}
            },

            getRenderConfiguation: async function(){    /* Get Zoho CRM RenderConfiguration Ref By Module */
                let module = await this.getSaasModule(); 
                if(!module){
                    return null;
                }
                renderConfig = this[module][UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE];
                renderConfig['css'] = this.css + renderConfig['css'];
                return renderConfig;
            },
            getExtraParams: async function(){   
                let loc = window.location.href;
                let extraParams = "";
                if ($zoho && $zoho.salesiq && $zoho.salesiq.values && $zoho.salesiq.values.email) {
                    extraParams = $zoho.salesiq.values.email;
                }
                return '&currentLoggedInSAASUser='+extraParams;
            },

            getSaasModule: async function(){    /* Get Zoho CRM Current Module */
                let loc = window.location.href;
                let selectedModule = "";
                UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "";
                if(loc.indexOf('/custom-view/') > -1) {
                    selectedModule = loc.split('/custom-view/')[0].split('/tab/')[1];
                    UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "list";
                }
                else {
                    selectedModule = loc.split("/tab/")[1].split('/')[0];
                    UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "detail";
                }
                if(selectedModule === "Potentials") {
                    selectedModule = "Deals";
                }
                return selectedModule;
            },

            getSaasRecordIds: async function(){  /* Get Zoho CRM Current Module And RecordIds */
                let loc = window.location.href;
                let entityId = [];
                if(loc.indexOf('/custom-view/') > -1) {
                    if(document.getElementsByClassName('listViewRowSelected').length) {
                        document.getElementsByClassName('listViewRowSelected').forEach(function(element) {
                            let selectId = element.attributes.id.value;
                            if(selectId && selectId.replace(/\D/g, '')) {
                                entityId.push(selectId.replace(/\D/g, ''));
                            }
                        });
                    }
                }
                else {
                    entityId.push(loc.split("/tab/")[1].split('/')[1]);
                }
                return entityId;
            },

            setMutationsInSaas: async function(){   /* Activate Mutation Observer For Zoho CRM */
                if(!UA_FLOAT_APP_CONTENT.isMutationObserverRunning){
                    if(document.querySelector("#ajax_load_tab")) UA_FLOAT_APP_CONTENT.runMutationObserver("MutationObserver", {target: "#ajax_load_tab", options:{attributes : true, attributeFilter : ['style']}}, 2000);
                }
            }
        },
        
////////////////////////////////////=>
        hubspotcrm: {
            "url_module_ref": ["contact/", "company/", "deal/", "ticket/"],
            "css": "padding: 2px 5px 4px 0px; position: relative; width: max-content; background: rgba(255, 255, 255, 0); border-radius: 50px; /*box-shadow: rgb(170 170 170) 0px 0px 10px 0px;*/ display: inline-block; right: 15px;top: -8px;",
            "contact": {
                detail:{"css": "","place_id": ".p-left-2.flex-shrink-0.m-left-auto","position": "afterbegin"}
            },
            "company": {
                detail:{"css": "","place_id": ".p-left-2.flex-shrink-0.m-left-auto","position": "afterbegin"}
            },
            "deal": {
                detail:{"css": "","place_id": ".p-left-2.flex-shrink-0.m-left-auto","position": "afterbegin"}
            },
            "ticket": {
                detail:{"css": "","place_id": ".p-left-2.flex-shrink-0.m-left-auto","position": "afterbegin"}
            },

            getRenderConfiguation: async function(){    /* Get Hubspot CRM RenderConfiguration Ref By Module */
                let module = await this.getSaasModule(); 
                if(!module){
                    return null;
                }
                renderConfig = this[module][UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE];
                renderConfig['css'] = this.css + renderConfig['css'];
                return renderConfig;
            },
            getExtraParams: async function(){   
                let loc = window.location.href;
                let extraParams = document.querySelectorAll('.user-info-email')[0].innerText;
                return '&currentLoggedInSAASUser='+extraParams;
            },

            getSaasModule: async function(){    /* Get Hubspot CRM Current Module */
                let loc = window.location.href;
                let selectedModule = "";
                UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "";
                if(loc.indexOf('/objects/') > -1) {
                    selectedModule = $('.private-dropdown__item__label').first().text().trim().toLowerCase().slice(0, -1);
                    UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "list";
                }
                else {
                    selectedModule = loc.split('/')[5];
                    UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "detail";
                }
                if(selectedModule === "companie") {
                    selectedModule = "company";
                }
                return selectedModule;
            },

            getSaasRecordIds: async function(){  /* Get Hubspot CRM Current Module And RecordIds */
                let loc = window.location.href;
                let entityId = [];
                if(loc.indexOf('/objects/') > -1) {
                    if(document.querySelectorAll('.private-checkbox__input:checked').length) {
                        document.querySelectorAll('.private-checkbox__input:checked').forEach(function(element) {
                            if(element.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.attributes['data-test-id']) {
                                let selectId = element.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.attributes['data-test-id'].value;
                                if(selectId && selectId.replace(/\D/g, '')) {
                                    entityId.push(selectId.replace(/\D/g, ''));
                                }   
                            }
                        });
                    }
                }
                else {
                    entityId.push(loc.split('/')[6]);
                }
                return entityId;
            },

            setMutationsInSaas: async function(){   /* Activate Mutation Observer For Hubspot CRM */
                return;
            }
        },
                
////////////////////////////////////=>
        pipedrive: {
            "url_module_ref": ["person/", "leads/inbox", "deal/"],
            "css": "padding: 2px 5px 4px 0px; position: relative; width: max-content; background: rgba(255, 255, 255, 0); border-radius: 50px; /*box-shadow: rgb(170 170 170) 0px 0px 10px 0px;*/ display: inline-block;",
            "person": {
                detail:{"css": "right: 15px;top: 0px;","place_id": ".actions .ownerView","position": "beforebegin"}
            },
            "lead": {
                detail:{"css": "margin-left: 30%; top: -8px;","place_id": "div[data-testid='DetailWrapper'][class='Detail__Wrapper-sc-1qbdaoj-0 jJyTXz']","position": "afterbegin"}
            },
            "deal": {
                detail:{"css": "right: 15px;top: 0px;","place_id": ".actions .ownerView","position": "beforebegin","parent_addCss": {"target": ".content.actionsContent .actions", "css":"display: flex; flex-direction: row; align-items: center;"}}
            },

            getRenderConfiguation: async function(){    /* Get Pipedrive CRM RenderConfiguration Ref By Module */
                let module = await this.getSaasModule(); 
                if(!module){
                    return null;
                }
                renderConfig = this[module][UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE];
                renderConfig['css'] = this.css + renderConfig['css'];
                return renderConfig;
            },
            getExtraParams: async function(){   
                let loc = window.location.href;
                let extraParams = "";
                if(loc.indexOf('/user/') > -1) {
                    extraParams = {};
                    if(extraParams === 'everyone') {
                        extraParams = JSON.stringify({everyone : extraParams});
                    }
                    else {
                        extraParams[loc.split('/user/')[1]] = loc.split('/user/')[1];
                        extraParams = JSON.stringify(extraParams);
                    }
                }
                else if(loc.indexOf('/filter/') > -1) {
                    extraParams = {};
                    extraParams[loc.split('/filter/')[1]] = loc.split('/filter/')[1];
                    extraParams = JSON.stringify(extraParams);
                }
                if (app && app.config && app.config.menuConfig && app.config.menuConfig.user && app.config.menuConfig.user.email) {
                    extraParams = extraParams +'&currentLoggedInSAASUser='+app.config.menuConfig.user.email;
                }
                return `&filter=${extraParams}`;
            },
            getSaasModule: async function(){    /* Get Pipedrive CRM Current Module */
                let loc = window.location.href;
                let selectedModule = "";
                UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "";
                if(loc.indexOf('/list/') > -1 || loc.indexOf('/deals') > -1 || loc.indexOf('/leads/inbox') > -1) {
                    selectedModule = loc.split('/list/')[0].split('/')[3].slice(0, -1);
                    UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "list";
                }
                else {
                    selectedModule = loc.split("/")[3];
                    UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "detail";
                }
                return selectedModule;
            },

            getSaasRecordIds: async function(){  /* Get Pipedrive CRM Current Module And RecordIds */
                let loc = window.location.href;
                let entityId = [];
                if(loc.indexOf('/list/') > -1 || loc.indexOf('/deals') > -1) {
                    if(document.querySelectorAll('.gridContent__table input:checked').length) {
                        document.querySelectorAll('.gridContent__table input:checked').forEach(function(element) {
                            let selectId = element.attributes['data-id'].value;
                            if(selectId && selectId.replace(/\D/g, '')) {
                                entityId.push(selectId.replace(/\D/g, ''));
                            }
                        });
                    }
                }
                else if(loc.indexOf('/leads/inbox') > -1) {
                    entityId.push(loc.split("/")[5]);
                }
                else {
                    entityId.push(loc.split("/")[4]);
                }
                return entityId;
            },

            setMutationsInSaas: async function(){   /* Activate Mutation Observer For Pipedrive CRM */
                if(!UA_FLOAT_APP_CONTENT.isMutationObserverRunning){
                    UA_FLOAT_APP_CONTENT.runMutationObserver("historyChange", {}, 3000);
                }
            }
        },
                
////////////////////////////////////=>
        freshworkscrm: {
            "url_module_ref": ["contacts/", "deals/", "accounts/"],
            "css": "padding: 2px 5px 4px 0px; position: relative; width: max-content; background: rgba(255, 255, 255, 0); border-radius: 50px; /*box-shadow: rgb(170 170 170) 0px 0px 10px 0px;*/ display: inline-block; right: 15px;top: -8px;",
            "contact": {
                detail:{"css": "","place_id": "#pageaction-area .flex","position": "afterbegin","parent_addCss": {"target": "#pageaction-area .flex", "css": "align-items: flex-start;margin-top: 15px;"}}
            },
            "deal": {
                detail:{"css": "","place_id": "#pageaction-area .flex","position": "afterbegin","parent_addCss": {"target": "#pageaction-area .flex", "css": "align-items: flex-start;margin-top: 15px;"}}
            },
            "sales_account": {
                detail:{"css": "","place_id": "#pageaction-area .flex","position": "afterbegin","parent_addCss": {"target": "#pageaction-area .flex", "css": "align-items: flex-start;margin-top: 15px;"}}
            },

            getRenderConfiguation: async function(){    /* Get Freshworks CRM RenderConfiguration Ref By Module */
                let module = await this.getSaasModule(); 
                if(!module){
                    return null;
                }
                renderConfig = this[module][UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE];
                renderConfig['css'] = this.css + renderConfig['css'];
                return renderConfig;
            },
            getExtraParams: async function(){   
                let loc = window.location.href;
                let extraParams = document.querySelectorAll('.user-email')[0].innerText;
                return '&currentLoggedInSAASUser='+extraParams;
            },

            getSaasModule: async function(){    /* Get Freshworks CRM Current Module */
                let loc = window.location.href;
                let selectedModule = "";
                UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "";
                if(loc.indexOf('/view/') > -1) {
                    selectedModule = loc.split('/view/')[0].split('/')[5].slice(0, -1);
                    UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "list";
                }
                else {
                    selectedModule = loc.split('/')[5].slice(0, -1);
                    UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "detail";
                }
                if(selectedModule === "account") {
                    selectedModule = "sales_account";
                }
                return selectedModule;
            },

            getSaasRecordIds: async function(){  /* Get Freshworks CRM Current Module And RecordIds */
                let loc = window.location.href;
                let entityId = [];
                if(loc.indexOf('/view/') > -1) {
                    if(document.querySelectorAll('.fsa-checkbox-wrap input:checked').length) {
                        document.querySelectorAll('.fsa-checkbox-wrap input:checked').forEach(function(element) {
                            if(element.attributes['name'].value !== 'allChecked') {
                                let selectId = element.attributes['name'].value;
                                if(selectId && selectId.replace(/\D/g, '')) {
                                    entityId.push(selectId.replace(/\D/g, ''));
                                }  
                            }
                        });
                    }
                }
                else {
                    entityId.push(loc.split('/')[6].split('?')[0].replace(/\D/g, ''));
                }
                return entityId;
            },

            setMutationsInSaas: async function(){   /* Activate Mutation Observer For Freshworks CRM */
                if(!UA_FLOAT_APP_CONTENT.isMutationObserverRunning){
                    UA_FLOAT_APP_CONTENT.runMutationObserver("historyChange", {}, 2000);
                }
            }
        },
                
////////////////////////////////////=>
        zendesksell: {
            "url_module_ref": ["contacts/", "leads/", "deals/"],
            "css": "padding: 2px 5px 4px 0px; position: relative; width: max-content; background: rgba(255, 255, 255, 0); border-radius: 50px; /*box-shadow: rgb(170 170 170) 0px 0px 10px 0px;*/ display: inline-block; right: 15px;top: -8px;",
            "contact": {
                detail:{"css": "","place_id": ".react-actions","position": "afterbegin","parent_addCss": {"target": ".react-actions", "css": "display: flex; margin-top: 25px;"}}
            },
            "deal": {
                detail:{"css": "","place_id": ".react-actions","position": "afterbegin","parent_addCss": {"target": ".react-actions", "css": "display: flex; margin-top: 25px;"}}
            },
            "lead": {
                detail:{"css": "","place_id": ".react-actions","position": "afterbegin","parent_addCss": {"target": ".react-actions", "css": "display: flex; margin-top: 25px;"}}
            },

            getRenderConfiguation: async function(){    /* Get Zendesk Sell RenderConfiguration Ref By Module */
                let module = await this.getSaasModule(); 
                if(!module){
                    return null;
                }
                renderConfig = this[module][UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE];
                renderConfig['css'] = this.css + renderConfig['css'];
                return renderConfig;
            },
            getExtraParams: async function(){   
                let loc = window.location.href;
                let extraParams = "";
                if (App && App.instance && App.instance.user && App.instance.user.attributes && App.instance.user.attributes.email) {
                    extraParams = App.instance.user.attributes.email;
                }
                return '&currentLoggedInSAASUser=' + extraParams;
            },

            getSaasModule: async function(){    /* Get Zendesk Sell Current Module */
                let loc = window.location.href;
                let selectedModule = "";
                UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "";
                if(loc.indexOf('/working/') > -1) {
                    selectedModule = loc.split('/working/')[1].split('/')[0].slice(0, -1);
                    UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "list";
                }
                else {
                    if(loc.indexOf('/leads/') > -1) {
                        selectedModule = loc.split('/')[3].slice(0, -1);
                    }
                    else {
                        selectedModule = loc.split('/')[4].slice(0, -1);
                    }
                    UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "detail";
                }
                return selectedModule;
            },

            getSaasRecordIds: async function(){  /* Get Zendesk Sell Current Module And RecordIds */
                let loc = window.location.href;
                let entityId = [];
                if(loc.indexOf('/working/') > -1) {
                    if(document.querySelectorAll('svg').length) {
                        document.querySelectorAll('svg').forEach(function(element) {
                            if(element.attributes['name'] && element.attributes['name'].value === 'checkmark' && element.parentNode.parentNode.parentNode.parentNode.parentNode.attributes['data-row'] && element.parentNode.parentNode.parentNode.parentNode.parentNode.attributes['data-row'].value && element.parentNode.parentNode.attributes['class'].value && element.parentNode.parentNode.attributes['class'].value.indexOf('selected') > -1) {
                                let selectId = element.parentNode.parentNode.parentNode.parentNode.parentNode.attributes['data-row'].value;
                                if(selectId && selectId.replace(/\D/g, '')) {
                                    entityId.push(selectId.replace(/\D/g, ''));
                                }
                            }
                        });
                    }
                }
                else {
                    if(loc.indexOf('/leads/') > -1) {
                        entityId.push(loc.split('/')[4]);
                    }
                    else {
                        entityId.push(loc.split('/')[5]);
                    }
                }
                return entityId;
            },

            setMutationsInSaas: async function(){   /* Activate Mutation Observer For Zendesk Sell */
                if(!UA_FLOAT_APP_CONTENT.isMutationObserverRunning){
                    UA_FLOAT_APP_CONTENT.runMutationObserver("historyChange", {}, 2000);
                }
            }
        },
                
////////////////////////////////////=>
        bitrix24: {
            "url_module_ref": ["contact/", "lead/", "deal/"],
            "css": "padding: 2px 5px 4px 0px; position: relative; width: max-content; background: rgba(255, 255, 255, 0); border-radius: 50px; /*box-shadow: rgb(170 170 170) 0px 0px 10px 0px;*/ display: inline-block; right: 15px;",
            "contact": {
                detail:{"css": "","place_id": "#toolbar_contact_details_","position": "afterbegin","parent_addCss": {"target": ".pagetitle-menu>div", "css": "display: inline-flex;"}}
            },
            "deal": {
                detail:{"css": "","place_id": "#toolbar_deal_details_","position": "afterbegin","parent_addCss": {"target": ".pagetitle-menu>div", "css": "display: inline-flex;"}}
            },
            "lead": {
                detail:{"css": "","place_id": "#toolbar_lead_details_","position": "afterbegin","parent_addCss": {"target": ".pagetitle-menu>div", "css": "display: inline-flex;"}}
            },

            getRenderConfiguation: async function(){    /* Get Bitrix24 CRM RenderConfiguration Ref By Module */
                let module = await this.getSaasModule(); 
                if(!module){
                    return null;
                }
                renderConfig = this[module][UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE];
                renderConfig['css'] = this.css + renderConfig['css'];
                return renderConfig;
            },
            getExtraParams: async function(){   
                let loc = window.location.href;
                let extraParams = "";
                if (BXIM && BXIM.userEmail) {
                    extraParams = BXIM.userEmail;
                }
                return '&currentLoggedInSAASUser=' + extraParams;
            },

            getSaasModule: async function(){    /* Get Bitrix24 CRM Current Module */
                let loc = window.location.href;
                let selectedModule = "";
                UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "";
                if(loc.indexOf('/list/') > -1) {
                    selectedModule = loc.split('/list/')[0].split('/')[4];
                    UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "list";
                }
                else {
                    selectedModule = loc.split('/')[4];
                    UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "detail";
                }
                return selectedModule;
            },

            getSaasRecordIds: async function(){  /* Get Bitrix24 CRM Current Module And RecordIds */
                let loc = window.location.href;
                let entityId = [];
                if(loc.indexOf('/list/') > -1) {
                    if(document.querySelectorAll('.main-grid-container tbody .main-grid-row-checkbox:checked').length) {
                        document.querySelectorAll('.main-grid-container tbody .main-grid-row-checkbox:checked').forEach(function(element) {
                            if(element.offsetParent !== null) {
                                let selectId = element.attributes['value'].value;
                                if(selectId && selectId.replace(/\D/g, '')) {
                                    entityId.push(selectId.replace(/\D/g, ''));
                                }
                            }
                        });
                    }
                }
                else {
                    entityId.push(loc.split('/')[6]);
                }
                return entityId;
            },

            setMutationsInSaas: async function(){   /* Activate Mutation Observer For Bitrix24 CRM */
                if(!UA_FLOAT_APP_CONTENT.isMutationObserverRunning){
                    UA_FLOAT_APP_CONTENT.runMutationObserver("historyChange", {}, 2000);
                }
            }
        },
                
////////////////////////////////////=>
        freshdesk: {
            
            
            getExtraParams: async function(){   
                let loc = window.location.href;
                let extraParams = document.querySelectorAll('.text__infotext')[0].innerText;
                return '&currentLoggedInSAASUser='+extraParams;
            },

            getSaasModule: async function(){    /* Get freshdesk Current Module */
                let loc = window.location.href;
                let selectedModule = "";
                UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "";
                if(loc.indexOf('/filters/') > -1) {
                    selectedModule = loc.split('/filters/')[0].split('/')[4].slice(0, -1);
                    UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "list";
                }
                else {
                    selectedModule = loc.split('/')[4].slice(0, -1);
                    UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "detail";
                }
                if(selectedModule === "companie") {
                    selectedModule = "company";
                }
                return selectedModule;
            },

            getSaasRecordIds: async function(){  /* Get freshdesk Current Module And RecordIds */
                let loc = window.location.href;
                let entityId = [];
                if(loc.indexOf('/filters/') > -1) {
                    if(document.querySelectorAll('.__ui-form__custom-checkbox input:checked').length) {
                        document.querySelectorAll('.__ui-form__custom-checkbox input:checked').forEach(function(element) {
                            if(element.attributes['id'].value !== 'top-navigation') {
                                let selectId = element.attributes['id'].value;
                                if(selectId && selectId.replace(/\D/g, '')) {
                                    entityId.push(selectId.replace(/\D/g, ''));
                                 }  
                            }
                        });
                    }
                }
                else {
                    entityId.push(loc.split('/')[5]);
                }
                return entityId;
            }
        },
                
////////////////////////////////////=>
        zohobooks: {
            
            
            getExtraParams: async function(){   
                let loc = window.location.href;
                let extraParams = "";
                if ($zoho && $zoho.salesiq && $zoho.salesiq.values && $zoho.salesiq.values.email) {
                    extraParams = $zoho.salesiq.values.email;
                }
                return '&currentLoggedInSAASUser=' + extraParams;
            },

            getSaasModule: async function(){    /* Get zohobooks CRM Current Module */
                let loc = window.location.href;
                let selectedModule = "";
                UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "";
                if(!loc.split('?')[0].split('#/')[1].split('/')[1]) {
                    selectedModule = loc.split('?')[0].split('#/')[1].split('/')[0];
                    UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "list";
                }
                else {
                    selectedModule = loc.split('?')[0].split('#/')[1].split('/')[0];
                    UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "detail";
                }
                return selectedModule;
            },

            getSaasRecordIds: async function(){  /* Get zohobooks CRM Current Module And RecordIds */
                let loc = window.location.href;
                let entityId = [];
                if(!loc.split('?')[0].split('#/')[1].split('/')[1]) {
                    if(document.querySelectorAll('tbody .ember-checkbox:checked').length) {     
                        document.querySelectorAll('tbody .ember-checkbox:checked').forEach(function(element) {
                            if(element.parentNode.parentNode.querySelectorAll('a').length) {           
                                element.parentNode.parentNode.querySelectorAll('a').forEach(function(subElement) {
                                    if((subElement.attributes['class'].value.trim() === 'ember-view' || subElement.attributes['class'].value.trim() === 'ember-view text-break') && subElement.attributes['href'].value) {           
                                        let selectRecord = subElement.attributes['href'].value.split('?')[0].split('/');
                                        let selectId = selectRecord[selectRecord.length-1];   
                                        if(selectId && selectId.replace(/\D/g, '')) {
                                            entityId.push(selectId.replace(/\D/g, ''));
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
                else {
                    entityId.push(loc.split('?')[0].split('#/')[1].split('/')[1]);
                }
                return entityId;
            }
        },
                
////////////////////////////////////=>
        shopify: {
                      
            
            getExtraParams: async function(){   
                let loc = window.location.href;
                let extraParams = "";
                document.querySelectorAll('script').forEach(function (selected) {
                    if (selected && selected.attributes && selected.attributes['data-serialized-id'] && selected.attributes['data-serialized-id'].value && selected.attributes['data-serialized-id'].value === 'request-details') {
                        extraParams = JSON.parse(selected.innerText).shop.shopEmail;
                        return '&currentLoggedInSAASUser='+extraParams;
                    }
                });
                return '&currentLoggedInSAASUser='+extraParams;
            },
            getSaasModule: async function(){    /* Get shopify CRM Current Module */
                let loc = window.location.href;
                let selectedModule = "";
                UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "";
                if(!loc.split('?')[0].split('/admin/')[1].split('/')[1]) {
                    selectedModule = loc.split('?')[0].split('/admin/')[1].split('/')[0];
                    UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "list";
                }
                else {
                    selectedModule = loc.split('?')[0].split('/admin/')[1].split('/')[0];
                    UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "detail";
                }
                return selectedModule;
            },

            getSaasRecordIds: async function(){  /* Get shopify CRM Current Module And RecordIds */
                let loc = window.location.href;
                let entityId = [];
                if(!loc.split('?')[0].split('/admin/')[1].split('/')[1]) {
                    if(document.querySelectorAll('input').length) {
                        document.querySelectorAll('input').forEach(function(element) {
                            if(element.attributes['class'] && element.attributes['class'].value.indexOf('Checkbox') > -1 && element.attributes['aria-checked'] && element.attributes['aria-checked'].value === 'true' && element.attributes['id'] && element.attributes['id'].value && element.attributes['id'].value.indexOf('gid') > -1) {
                                let selectRecord = element.attributes['id'].value.split('/')
                                let selectId = selectRecord[selectRecord.length-1]; 
                                if(selectId && selectId.replace(/\D/g, '')) {
                                    entityId.push(selectId.replace(/\D/g, ''));
                                }                   
                            }
                        });
                    }
                }
                else {
                    entityId.push(loc.split('?')[0].split('/admin/')[1].split('/')[1]);
                }
                return entityId;
            }
            
        },
                
////////////////////////////////////=>
        leadsquared: {
            
            
            getExtraParams: async function(){   
                let loc = window.location.href;
                let extraParams = document.querySelectorAll('.user-email')[0].innerText;
                return '&currentLoggedInSAASUser='+extraParams;
            },

            getSaasModule: async function(){    /* Get leadsquared CRM Current Module */
                let loc = window.location.href;
                let selectedModule = "";
                UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "";
                if(loc.indexOf('/LeadManagement') > -1 && !(loc.indexOf('LeadID=') > -1)) {
                    selectedModule = "leads";
                    UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "list";
                }
                else {
                    selectedModule = "leads";
                    UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "detail";
                }
                return selectedModule;
            },

            getSaasRecordIds: async function(){  /* Get leadsquared CRM Current Module And RecordIds */
                let loc = window.location.href;
                let entityId = [];
                if(loc.indexOf('/LeadManagement') > -1 && !(loc.indexOf('LeadID=') > -1)) {
                    if(document.querySelectorAll('.lead-checkbox').length) {
                        document.querySelectorAll('.lead-checkbox').forEach(function(element) {
                            if(element.querySelectorAll('input') && element.querySelectorAll('input')[0] && element.querySelectorAll('input')[0].checked) {
                                let selectId = element.querySelectorAll('input')[0].attributes['value'].value;
                                if(selectId) {
                                    entityId.push(selectId);
                                }  
                            }
                        });
                    }
                }
                else {
                    entityId.push(new URLSearchParams(window.location.search).get('LeadID'));
                }
                return entityId;
            }
            
        },
                
////////////////////////////////////=>
        zohorecruit: {
            
            
            getExtraParams: async function(){   
                let loc = window.location.href;
                let extraParams = "";
                if ($zoho && $zoho.salesiq && $zoho.salesiq.values && $zoho.salesiq.values.email) {
                    extraParams = $zoho.salesiq.values.email;
                }
                return '&currentLoggedInSAASUser=' + extraParams;
            },

            getSaasModule: async function(){    /* Get zohorecruit CRM Current Module */
                let loc = window.location.href;
                let selectedModule = "";
                UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "";
                if(loc.indexOf('/ShowTab.do') > -1) {
                    selectedModule = new URLSearchParams(window.location.search).get('module');
                    UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "list";
                }
                else {
                    selectedModule = new URLSearchParams(window.location.search).get('module');
                    UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "detail";
                }
                return selectedModule;
            },

            getSaasRecordIds: async function(){  /* Get zohorecruit CRM Current Module And RecordIds */
                let loc = window.location.href;
                let entityId = [];
                if(loc.indexOf('/ShowTab.do') > -1) {
                    if(document.querySelectorAll('#listViewTable tbody .customCheckBoxChecked').length) {
                        document.querySelectorAll('#listViewTable tbody .customCheckBoxChecked').forEach(function(element) {
                            let selectId = element.attributes['data-params'].value;
                            if(selectId) {
                                entityId.push(selectId);
                            } 
                        });
                    }
                }
                else {
                    entityId.push(new URLSearchParams(window.location.search).get('id'));
                }
                return entityId;
            }
            
        },
        ////////////////////////////////////=>
        zohobigin: {
            
            getExtraParams: async function(){   
                let loc = window.location.href;
                let extraParams = "";
                if (zgssearch && zgssearch.GSConstant && zgssearch.GSConstant.cue) {
                    extraParams = zgssearch.GSConstant.cue;
                }
                return '&currentLoggedInSAASUser=' + extraParams;
            },

            getSaasModule: async function(){    /* Get zohorecruit CRM Current Module */
                let loc = window.location.href;
                let selectedModule = "";
                UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "";
                let urlSegments = loc.split('/');
                
                if(loc.indexOf('/bigin/') > -1) {
                
                    selectedModule = urlSegments[5];
                    selectedModule = selectedModule.substring(0,1).toUpperCase() + selectedModule.substring(1);
                    UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE =  urlSegments[6] == 'list' ? "list" : "";
                } 
               
                return selectedModule;
            },

            getSaasRecordIds: async function(){  /* Get zohorecruit CRM Current Module And RecordIds */
                let loc = window.location.href;
                let entityId = [];
                let urlSegments = loc.split('/');
                let module =  urlSegments[6] === 'list' ? "list" : "";
                if(module === 'list') {
                    if(document.querySelectorAll('lyte-tr.lv-row').length) {
                        document.querySelectorAll('lyte-tr.lv-row').forEach(function(element) {
                            let selected = $(element).find('.lyteCheckbox input[type="checkbox"]:checked');
                            if(selected.length) {
                                let id = $(element).find('lyte-yield[ux-row-id]').attr('ux-row-id');
                                entityId.push(id);
                            }
                        });
                    }
                } else {
                    let entity = urlSegments[6] ? urlSegments[6].split('?')[0] : false;
                    entityId.push(entity);
                }
                return entityId;
            }
            
        },
        ////////////////////////////////////=>
        dynamicscrm: {
            
            getExtraParams: async function(){   
                let loc = window.location.href;
                let extraParams = "";
                return extraParams;
            },

            getSaasModule: async function(){    /* Get zohorecruit CRM Current Module */
                let loc = window.location.href;
                let selectedModule = "";
                UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE = "";
                let urlSegments = loc.split('/');
                let params = new URLSearchParams(window.location.search);
                if(params.get("etn")){
                    selectedModule = params.get("etn");
                    if(params.get("pagetype") && params.get("pagetype") == "entityrecord") {
                        UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE =  "detail";
                    }
                    else if(params.get("pagetype") && params.get("pagetype") == "entitylist"){
                        UA_FLOAT_APP_CONTENT.CURRENT_SAAS_MODULE_TYPE =  "list";
                    }
                } 
               
                return selectedModule;
            },

            getSaasRecordIds: async function(){  /* Get zohorecruit CRM Current Module And RecordIds */
                let loc = window.location.href;
                let entityId = [];
                let urlSegments = loc.split('/');
                let params = new URLSearchParams(window.location.search);
                if(params.get("pagetype") && params.get("pagetype") == "entitylist") {
                    if(document.querySelector(".ag-center-cols-container")) {
                        document.querySelector(".ag-center-cols-container").querySelectorAll(".ag-row.ag-row.ag-row-position-absolute.ag-row-selected").forEach(function(element) {
                            let id = element.getAttribute('row-id');
                            entityId.push(id);
                        });
                    }
                } 
                else if(params.get("pagetype") && params.get("pagetype") == "entityrecord" && params.get("id")) {
                    let entity = params.get("id");
                    entityId.push(entity);
                }
                return entityId;
            }
            
        }
        
    }
    

//////////////////////////////////////////////////////   END SAAS FUNCTIONS   ///////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


var styles = `
.ua-init.ua-floating-app-notif-holder:after {
    content: "";
    position: absolute;
    top: 0%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #555 transparent transparent transparent;
}

.ua-init::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    border-radius: 10px;
    background-color: #F5F5F5;
}

.ua-init::-webkit-scrollbar {
    width: 7px;
    background-color: #F5F5F5;
}

.ua-init::-webkit-scrollbar-thumb {
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, .3);
    background-color: #cccccc;
}

div.ua-minimized-item:hover {
    background: #d6d2d224;
    box-shadow: 0px 0px 3px 1px #aaa;
    outline: 1px solid #aaaaaaab;
}

.ua-minimized-item-close:hover {
    color: crimson;
    font-weight: 900;
    font-size: 13px;
    margin-top: -3px;
}
`;

var styleSheet = document.createElement("style");
styleSheet.innerText = styles;
document.head.appendChild(styleSheet);