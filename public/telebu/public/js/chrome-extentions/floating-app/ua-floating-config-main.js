
function handleauthoriztion(){
    if(currentUser){
        console.log(currentUser);
    }
}

var handleSignedInUser = function (user){
    $("#credential_picker_container").hide();
    currentUser = firebase.auth().currentUser;
    UA_FLOAT_APP_CONFIG.currentUserDetails();
}

var handleSignedOutUser = function () {
    document.getElementById("notSignedInError").querySelector("b").remove();
    $("#notSignedInError").show();
    $(".signedIn").hide();
    $('#loader').hide();
    $(".signedOut").show();
    $("#firebaseui-auth-container").show();
    $(".showAfterSignIn").hide();
    // $("#credential_picker_container").show();
    UA_FLOAT_APP_CONFIG.currentUserDetails();
};

window.addEventListener('message', function(event){
    if (event.data) {
        var data = event.data;
        if(data.action && typeof UA_FLOAT_APP_CONFIG[data.action] === "function"){
            UA_FLOAT_APP_CONFIG[data.action](data);
        }
    }
}, false);


///////////////////////////////////////////       UA_FLOAT_APP_CONFIG       /////////////////////////////////////////////////////////////////////

var UA_FLOAT_APP_CONFIG = {
    sendToParent: async function(data={}){
        data.ulgId = "UA_FLOAT_APP_CONTENT";
        window.parent.postMessage(data,"*");
    },

    currentUserDetails: async function(){
        var currentUserId = "";
        var userAppsList = [];
        var currentUserDetails = {};
        if(firebase && firebase.auth().currentUser && firebase.auth().currentUser.uid){
            currentUserId = firebase.auth().currentUser.uid;
            currentUserDetails = {
                id: currentUserId,
                name: firebase.auth().currentUser.displayName,
                email: firebase.auth().currentUser.email,
                photoURL: firebase.auth().currentUser.photoURL
            }
        }
        if(currentUserId){
            userAppsList = await UA_FLOAT_APP_CONFIG.getUserAuthorizedExtentions(currentUserId);
        }
        UA_FLOAT_APP_CONFIG.sendToParent({action:"isCurrentUser",userId:currentUserId,userAppsList:userAppsList,currentUserDetails: currentUserDetails});
        UA_FLOAT_APP_CONFIG.startListeningNotificationsCountForUser();
    },

    startListeningNotificationsCountForUser: async function(){
        if(!firebase.auth().currentUser) return;
        let NOTIFICATIONS_COUNT_RT_LISTENER = firebase.database().ref(`user_unread_notifications_count/${firebase.auth().currentUser.uid}`);
        NOTIFICATIONS_COUNT_RT_LISTENER.on('value', function(snapshot){
            UA_FLOAT_APP_CONFIG.APP_NOTIFICATIONS_COUNT = snapshot.val();
            UA_FLOAT_APP_CONFIG.sendToParent({action:"ListeningToNotificationsCountForUser", snapshot: snapshot.val()});
        });
    },

    getUserAuthorizedExtentions: async function(userId){
        var userAppsList = [];
        await db.collection("ulgebraUsers").doc(userId).collection("extensions").get().then(async function(docs){
            await docs.forEach(doc => {
                userAppsList.push(doc.id);
            });
            return;
        });
        return userAppsList;
    },

    uaSignOut: async function(){
        firebase.auth().signOut().then(() => {
            // Sign-out successful.
            UA_FLOAT_APP_CONFIG.sendToParent({action:"reloadParentWindow"});
        }).catch((error) => {
            // An error happened.
        });
    },

    getUserAppNotificationsData: async function(data){
        if(!data || !data.appId) return;
        var valueData = [];
        var wantedCount = UA_FLOAT_APP_CONFIG.APP_NOTIFICATIONS_COUNT[data.appId];
        wantedCount = wantedCount > 20 ? 20:wantedCount;
        var resp = await db.collection("ulgebraUsers").doc(currentUser.uid).collection('extensions').doc(data.appId).collection("notifications").orderBy('ct', 'desc').limit(wantedCount).get();
        if(resp.docs){
            await resp.docs.forEach(item =>{
                let itemData = item.data();
                itemData.id = item.id;
                valueData.push(itemData);
            });
        }
        UA_FLOAT_APP_CONFIG.sendToParent({action:"setVariable", variableName: "notificationsData", key: data.appId, value: valueData});
        return;
    },
    
    markAllMyNotificationsAsTrue: function(data){
        if(!data || !data.appId) return;
        firebase.database().ref(`user_unread_notifications_count/${currentUser.uid}/${data.appId}`).set(0);
    }
};
