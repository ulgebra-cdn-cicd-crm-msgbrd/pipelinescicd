var hubIndex = window.location.href.indexOf("#https://app.hubspot.com");
var APP_ENV_CLIENT_URL = "https://sms.ulgebra.com";
var APP_ENV_SERVER_URL = "https://us-central1-ulgebra-license.cloudfunctions.net";
var APP_LOAD_FROM_LIVE = true;
var appsConfig = {};
if (hubIndex != -1 && window.location.href.indexOf("/settings") != -1) {
    $("body").append(`<div style="position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0,0,0,0.5);
  z-index: 2;
  cursor: pointer;"><a  style="position: absolute;
  top: 50%;
  left: 50%;
  font-size: 25px;
  color: white;
  transform: translate(-50%,-50%);
  -ms-transform: translate(-50%,-50%);" href="${window.location.href.substring(0, hubIndex)}" target="_blank">Go To Settings</a>
  </div>`)
}
if (document.getElementById("Error")) {
    $("#Error").append(`<div style="float:right;margin:30px;margin-top:50px;cursor: pointer;" onclick='$("#Error").hide()'>
        <svg xmlns="http://www.w3.org/2000/svg" width="29" height="29" viewBox="0 0 24 24" fill="none" stroke="#f8f4f4" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg></div>`);
}
if (typeof UA_SAAS_SERVICE_APP === "undefined") {
    UA_SAAS_SERVICE_APP = {};
}
if (typeof UA_TPA_FEATURES === "undefined") {
    UA_TPA_FEATURES = {};
}
const regionNamesInEnglish = new Intl.DisplayNames(['en'], { type: 'region' });
var curId = 1000;
var errorId = 1000;
var initTries = 0;
var initProcessId = 1;
var storageRef = null;
var appPrettyNameMap = {
    "pinnacle": "Pinnacle",
    "karix": "Karix",
    "clickatell": "Clickatell",
    "telebu": "Telebu",
    "rivet": "Rivet",
    "mtalkz": "Mtalkz",
    "routemobile": "Route Mobile",
    "grapevine": "Grapevine",
    "vonage": "Vonage",
    "ringcentral": "RingCentral",
    "twilio": "Twilio",
    "calendly": "Calendly",
    "textlocal": "Textlocal",
    "messagebird": "MessageBird",
    "whatcetra": "WhatCetra",
    "whatsappweb": "WhatsApp Web",
    "acuityscheduling": "Acuity",
    "telnyx": "Telnyx",
    "sinch": "Sinch",
    "msg91": "MSG91",
    "kaleyra": "Kaleyra",
    "rivet": "Rivet",
    "exotel": "Exotel",
    "clicksend": "ClickSend",
    "burstsms": "BurstSMS",
    "bulksms": "BulkSMS",
    "plivo": "Plivo",
    "gupshup": "Gupshup",
    "textmagic": "TextMagic",
    "stripe": "Stripe",
    "xero": "Xero"
};
var saasPrettyNameMap = {
    "zoho": "Zoho",
    "zohodesk": "Zoho Desk",
    "zohocrm": "Zoho CRM",
    "pipedrive": "Pipedrive",
    "pipedrivecrm": "Pipedrive",
    "bitrix24": "Bitrix24",
    "bitrix24crm": "Bitrix24",
    "hubspot": "HubSpot",
    "hubspotcrm": "HubSpot CRM",
    "gorgias": "Gorgias",
    "freshdesk": "Freshdesk",
    "freshsales": "Freshsales",
    "freshworkscrm": "Freshsales",
    "shopify": "Shopify",
    "zendesk": "Zendesk",
    "zendesksell": "Zendesk Sell",
    "salesporcecrm": "SalesForce",
    "zendesksupport": "Zendesk Support",
    "leadsquared": "LeadSquared",
    "dynamicscrm": "Dynamics CRM"
};
var eventApps = ["calendly", "acuityscheduling"];
var lookupApps = ["lookup"];
var appTPACode = "undefined";
var appPrettyName = "Ulgebra";
var saasServiceName = "CRM";
var appCodeTPA = null;
var appCodeSAAS = null;
var isUASMSApp = true;
var appCodeVsNameMap = {
    "twilioforzohocrm": "Twilio for Zoho CRM - Ulgebra",
    "twilioforhubspotcrm": "Twilio for HubSpot - Ulgebra",
    "calendlyformonday": "Calendly for monday.com"
};
var UA_STAND_ALONE_APP = false;
try {
    if (typeof extensionName === "undefined" || !extensionName) {
        try {
            queryParams = new URLSearchParams(window.location.search);
            extensionName = queryParams.get('appCode');
        }
        catch (ex) {
            console.log(ex);
        }
    }
    appCodeTPA = extensionName.split("for")[0];
    appCodeSAAS = extensionName.split("for")[1];
    UA_STAND_ALONE_APP = extensionName.startsWith("lookup") || (queryParams && queryParams.get("disableSAASSdk"));
    appTPACode = extensionName ? extensionName.split('for')[0] : "undefined";
    appPrettyName = appPrettyNameMap[appTPACode] ? appPrettyNameMap[appTPACode] : "Ulgebra";
    let appKindName = "";
    switch (location.host.split('.')[0]) {
        case "sms":
            appKindName = " - Messaging";
            break;
        case "events":
            appKindName = " - Meetings";
            break;
    }
    saasServiceName = saasPrettyNameMap.hasOwnProperty(extensionName.split("for")[1]) ? saasPrettyNameMap[extensionName.split("for")[1]] : extensionName.split("for")[1].toUpperCase();
    document.title = appPrettyName + " - " + saasServiceName + appKindName + " - Ulgebra";
    $('.saasServiceName').text(saasServiceName);
    eventApps.forEach(eventAppName => {
        if (extensionName.indexOf(eventAppName) === 0) {
            isUASMSApp = false;
            $(".pageContentHolder").html('');
            $(".pageEventAppNewAppointmentHolder").show();
        }
    });
    lookupApps.forEach(eventAppName => {
        if (extensionName.indexOf(eventAppName) === 0) {
            isUASMSApp = false;
            $(".pageContentHolder").html('');
            $("body").addClass('ua-app-type-lookup');
            $(".pageLookUpPageHolder").show();
        }
    });
    if (extensionName.indexOf("forzohobooks") > 0) {
        $('.pageContentHolder').css({ 'padding-bottom': '150px' });
    }
    if (queryParams.get("actionType") === "fieldMapping") {
        $('.pageEventAppNewAppointmentHolder, .pageContentHolder').hide();
        $("#fieldConfigAppFullScreen").show();
    }
    //field mapping enabling here
    if ((!["calendly", "twilio", "acuityscheduling", "messagebird"].includes(appCodeTPA) || !["zohocrm", "zohodesk", "pipedrive", "bitrix24", "hubspotcrm", "freshdesk", "dynamicscrm"].includes(appCodeSAAS))) {
        $("#suo-item-fieldmapping-nav").remove();
    } else {
        $("#suo-item-fieldmapping-nav").show();
    }
    if (!extensionName.startsWith("calendly")) {
        $("#tpaichan-user-mapping").remove();
    }
    if (extensionName.endsWith("shopify")) {
        $("#saasAuthIDCredit").remove();
        $('#signedProfileUserLink_subs').remove();
    }
    $("#ua-ran-out-credit-link").attr('href', './store?appCode=' + extensionName);
}
catch (ex) {
    console.log(ex);
}
var APP = {
    initialize: function () {
        APP.renderEngine.header();
        if (!window.location.href.startsWith('http://localhost:') || APP_LOAD_FROM_LIVE) {
            APP_ENV_CLIENT_URL = "https://sms.ulgebra.com";
            APP_ENV_SERVER_URL = "https://us-central1-ulgebra-license.cloudfunctions.net";
        }
        else {
            APP_ENV_CLIENT_URL = "http://localhost:5003";
            APP_ENV_SERVER_URL = "http://localhost:5001/ulgebra-license/us-central1";
        }
        if (["messagebird", "twilio", "whatcetra"].includes(appCodeTPA)) {
            $("#suo-item-chat-inbox").show();
        }
        else {
            $("#suo-item-chat-inbox").remove();
        }
        if (extensionName.startsWith("lookup")) {
            $('.userInfoBtn').css({ 'right': '80px !important', 'background-color': 'white' });
            let unsupportedNavItems = ["suo-item-lic-users-manage-users", "suo-item-incoming-nav", "suo-item-fieldmapping-nav", "suo-item-workflow-nav"];
            unsupportedNavItems.forEach(item => {
                $("#" + item).remove();
            });
        }
    },
    renderEngine: {
        header: function () {
            $('.displayAppPrettyName').text(appPrettyName);
            $("body").prepend(`
            <header>
                <div class="mainSearchBarHolder">
                    <div class="siteName">
                        <div class="siteLogo"></div> <a style="color:black" href="${location.href.toString().replace("&disableSAASSdk=false", "") + "&disableSAASSdk=true"}" target="_blank"> ${appPrettyName} </a>
                    </div>
                    <div id="nav_viewTypeSwitch" style="display:inline-block"></div>
                    <div id="nav_notificationIcon" class="authInfoSlip" title="Notifications" onclick="UA_APP_UTILITY.renderAndShowNotifications()">
                        <span id="ua_current_app_alert_count">0</span>
                        <span class="material-icons">notifications</span>
                    </div>
                    <div id="saasAuthIDCredit" class="authInfoSlip" title="Ulgebra Credits">
                        <a href="./store?appCode=${extensionName}" target="_blank">
                            <span class="credicon" style="display: inline-block;float: left;margin-right: 5px;margin-top: -3px;" title="Ulgebra Credits">
                                <b style="font-size: 18px;font-weight: normal;color: darkorange;">U</b>
                                <b style="font-size: 18px;margin-left: -11px;font-weight: normal;color: #03ae03;">C</b>
                            </span> <b class="ua-lic-credit-amount">0</b>
                        </a>
                    </div>
                    <div class="signUserInfoHolder signedIn">
                        <div class="userInfoBtn">
                            <img id="signInUserImg"/>
                        </div>
                        <div class="aa-ualic_sub_status_tip">
                ...
            </div>
                        <div class="signedUserOptions">
                            <div id="signedInUserAppAdminHolder" onclick="UA_LIC_UTILITY.showExistingInvitedAdminDDHTML(false, true)"></div>
                            <div class="signedUserOptionsInr">
                                <div class="ua_settings_header">
                                    <div class="suo-item suo-item-ac-labels" id="suo-item-ua-profile-nav">
                                        <span class="material-icons">account_circle</span> <span id="signInUserName"> My Account </span><span id="signInUserEmail">...</span>
                                    </div>
                                </div>
                                <div class="ua_settings_outer">

                                    <div id="ua_settings_feature" class="ua_settings_divisions">

                                        <div class="ua_settings_subTitle">Features</div>

                                        <div class="ua_settings_subBody">
                                        
                                            <div class="suo-item suo-item-ac-labels" id="suo-item-ua-app-lic-plan" onclick="showProductPlanDetails()" onmouseover = "showUaSettingsSubDivDetails(this)" onmouseout = "hideUaSettingsSubDivDetails(this)">
                                                <div class="ac_DetailMain_id">
                                                    <div class="ac_subName_logo"><span class="material-icons">shopping_cart</span></div>
                                                    <div class="ac_subDetail_id">
                                                        <div class="ac_subName_id"> <span>App Subscription</span> <span class="anl_servicename"></span> </div>
                                                        <div class="ac_name_id">Using <b>Free trial</b>. Click to Buy & Activate all features.</div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="suo-item-support-help" class="suo-item suo-item-ac-labels" style="color: #07aee6;background-color: white;cursor: default;" onmouseover = "showUaSettingsSubDivDetails(this)" onmouseout = "hideUaSettingsSubDivDetails(this)">
                                                <div class="ac_DetailMain_id">
                                                    <div class="ac_subName_logo"><span class="material-icons" style="color: #07aee6;">help</span></div>
                                                    <div class="ac_subDetail_id">
                                                        <div class="ac_subName_id"><span>How to use app?</span><span class="anl_servicename"></span></div>
                                                        <div class="ac_name_id" style="color: darkgoldenrod;overflow: hidden;display: inline-block;bottom: 12px;">
                                                            <a id="suo-item-support-help-guide-link" target="_blank" href="https://apps.ulgebra.com/_/search?query=${appPrettyName}%20for%20${saasServiceName}&amp;scope=site&amp;showTabs=false">Help Guide</a>
                                                            <a target="_blank" href="https://apps.ulgebra.com/contact">Contact Us</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <a style="color:unset" onclick="this.href='?appCode='+extensionName+'&disableSAASSdk=true&force-ui-view=CONVERSATIONS';" onload="this.href='?appCode='+extensionName+'&disableSAASSdk=true&force-ui-view=CONVERSATIONS';" href="#" target="_blank">
                                                <div id="suo-item-chat-inbox" style="display:none;background-color: white !important;" class="suo-item suo-item-ac-labels" onclick="UA_APP_UTILITY.showWorkFlowInstructionDialog();" onmouseover = "showUaSettingsSubDivDetails(this)" onmouseout = "hideUaSettingsSubDivDetails(this)">

                                                    <div class="ac_DetailMain_id">
                                                        <div class="ac_subName_logo"><span class="material-icons">forum</span></div>
                                                        <div class="ac_subDetail_id">
                                                            <div class="ac_subName_id"> <span>${appPrettyName} Inbox</span> <span class="anl_servicename"></span> </div>
                                                            <div class="ac_name_id">
                                                                <i style=" background-color: darkgoldenrod; color: white; font-style: normal; font-size: 9px; padding: 1px 7px 2px; border-radius: 12px;position: relative; top: -2px;">New</i>
                                                                View all conversations in real-time
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </a>

                                            <div class="suo-item suo-item-ac-labels" onclick="window.open('${location.href.toString().replace("&disableSAASSdk=false", "") + "&disableSAASSdk=true"}');" onmouseover = "showUaSettingsSubDivDetails(this)" onmouseout = "hideUaSettingsSubDivDetails(this)">
                                                
                                                <div class="ac_DetailMain_id">
                                                    <div class="ac_subName_logo"><span class="material-icons">open_in_new</span></div>
                                                    <div class="ac_subDetail_id">
                                                        <div class="ac_subName_id"> <span>Open in New Tab</span> <span class="anl_servicename"></span> </div>
                                                        <div class="ac_name_id">
                                                            Open this page in new tab
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <a style="color:unset" href="https://chrome.google.com/webstore/detail/appcursor-all-app-wigets/lgjaomhkknkffgpfbpngnbcinhpddbpp" target="_blank">
                                                <div class="suo-item suo-item-ac-labels" style="color: darkgoldenrod;background-color: white !important;" onmouseover = "showUaSettingsSubDivDetails(this)" onmouseout = "hideUaSettingsSubDivDetails(this)">

                                                    <div class="ac_DetailMain_id">
                                                        <div class="ac_subName_logo"><span class="material-icons" style="color: darkgoldenrod;">auto_awesome</span></div>
                                                        <div class="ac_subDetail_id">
                                                            <div class="ac_subName_id"> <span>Install Ulgebra AppCursor</span> <span class="anl_servicename"></span> </div>
                                                            <div class="ac_name_id" style="color: darkgoldenrod;">
                                                                <i style=" background-color: darkgoldenrod; color: white; font-style: normal; font-size: 9px; padding: 1px 7px 2px; border-radius: 12px;position: relative; top: -2px;">New</i>
                                                                Access ${UA_APP_UTILITY.getAppPrettyName(extensionName)} instantly &amp; our other apps.
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </a>
                                        
                                        </div>

                                    </div>
                                    <div id="ua_settings_forAdmin" class="ua_settings_divisions">
                                    
                                        <div class="ua_settings_subTitle">For Admin</div>

                                        <div class="ua_settings_subBody">
                                        
                                            <div id="suo-item-lic-users-manage-users" class="suo-item suo-item-ac-labels" onclick="UA_LIC_UTILITY.showUserPermissionLICDialog();" onmouseover = "showUaSettingsSubDivDetails(this)" onmouseout = "hideUaSettingsSubDivDetails(this)">

                                                <div class="ac_DetailMain_id">
                                                    <div class="ac_subName_logo"><span class="material-icons">groups</span></div>
                                                    <div class="ac_subDetail_id">
                                                        <div class="ac_subName_id"> <span>Manage App Users</span> <span class="anl_servicename"></span> </div>
                                                        <div class="ac_name_id">
                                                            Activate app for all org users (For admins)
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div id="suo-item-incoming-nav" style="display: none" class="suo-item suo-item-ac-labels" onclick="UA_TPA_FEATURES.showIncomingWebhookDialog();" onmouseover = "showUaSettingsSubDivDetails(this)" onmouseout = "hideUaSettingsSubDivDetails(this)">

                                                <div class="ac_DetailMain_id">
                                                    <div class="ac_subName_logo"><span class="material-icons">sync</span></div>
                                                    <div class="ac_subDetail_id">
                                                        <div class="ac_subName_id"> <span>Incoming events sync</span> <span class="anl_servicename"></span> </div>
                                                        <div class="ac_name_id">
                                                            To receive incoming messages
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div id="suo-item-workflow-nav"   class="suo-item suo-item-ac-labels" onclick="UA_APP_UTILITY.showWorkFlowInstructionDialog();" onmouseover = "showUaSettingsSubDivDetails(this)" onmouseout = "hideUaSettingsSubDivDetails(this)">

                                                <div class="ac_DetailMain_id">
                                                    <div class="ac_subName_logo"><span class="material-icons">auto_mode</span></div>
                                                    <div class="ac_subDetail_id">
                                                        <div class="ac_subName_id"> <span>Workflows</span> <span class="anl_servicename"></span> </div>
                                                        <div class="ac_name_id">
                                                            Send automated messages with workflows
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <a style="color:unset" onclick="this.href='?appCode='+extensionName+'&disableSAASSdk=true&actionType=fieldMapping';" onload="this.href='?appCode='+extensionName+'&disableSAASSdk=true&actionType=fieldMapping';" href="#" target="_blank">
                                            
                                                <div id="suo-item-fieldmapping-nav" class="suo-item suo-item-ac-labels" onmouseover = "showUaSettingsSubDivDetails(this)" onmouseout = "hideUaSettingsSubDivDetails(this)">

                                                    <div class="ac_DetailMain_id">
                                                        <div class="ac_subName_logo"><span class="material-icons">signpost</span></div>
                                                        <div class="ac_subDetail_id">
                                                            <div class="ac_subName_id"> <span>Fields Mapping for Sync</span> <span class="anl_servicename"></span> </div>
                                                            <div class="ac_name_id">
                                                                Map ${appPrettyName} - ${saasServiceName} fields for incoming events
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>

                                            </a>

                                            <div class="suo-item suo-item-ac-labels" onclick="UA_APP_UTILITY.appUsageAnalyticsMain();" onmouseover = "showUaSettingsSubDivDetails(this)" onmouseout = "hideUaSettingsSubDivDetails(this)">

                                                <div class="ac_DetailMain_id">
                                                    <div class="ac_subName_logo"><span class="material-icons">insights</span></div>
                                                    <div class="ac_subDetail_id">
                                                        <div class="ac_subName_id"> <span>App Usage Analytics</span> <span class="anl_servicename"></span> </div>
                                                        <div class="ac_name_id">
                                                            View app usage history
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                        <span id="suo-item-notification-nav-insertafter"></span>

                                    </div>
                                    <div id="ua_settings_advanced" class="ua_settings_divisions">

                                        <div class="ua_settings_subTitle">Advanced</div>

                                        <div class="ua_settings_subBody">
                                        
                                            <div id="ac_name_label_saas" class="suo-item suo-item-ac-labels" onclick="UA_SAAS_SERVICE_APP.initiateAuthFlow();" onmouseover = "showUaSettingsSubDivDetails(this)" onmouseout = "hideUaSettingsSubDivDetails(this)">

                                                <div class="ac_DetailMain_id">
                                                    <div class="ac_subName_logo"><span class="material-icons">restart_alt</span></div>
                                                    <div class="ac_subDetail_id">
                                                        <div class="ac_subName_id"> <span>Re-Authorize</span> <span class="anl_servicename">...</span> </div>
                                                        <div class="ac_name_id">
                                                            fetching...
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                            </div>

                                            <div id="ac_name_label_tpa"  class="suo-item suo-item-ac-labels" onclick="UA_APP_UTILITY.showReAuthorizeERRORWithClose();" onmouseover = "showUaSettingsSubDivDetails(this)" onmouseout = "hideUaSettingsSubDivDetails(this)">

                                                <div class="ac_DetailMain_id">
                                                    <div class="ac_subName_logo"><span class="material-icons">restart_alt</span></div>
                                                    <div class="ac_subDetail_id">
                                                        <div class="ac_subName_id"> <span>Re-Authorize</span> <span class="anl_servicename">...</span> </div>
                                                        <div class="ac_name_id">
                                                            fetching...
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                            </div>                                     

                                            <div class="suo-item suo-item-ac-labels" style="color: red;" onclick="$('#UA_APP_UNINSTALL_MODLE').show();$('#ua-app-uninstall-confirm-text').val('');" onmouseover = "showUaSettingsSubDivDetails(this)" onmouseout = "hideUaSettingsSubDivDetails(this)">

                                                <div class="ac_DetailMain_id">
                                                    <div class="ac_subName_logo"><span class="material-icons">delete_sweep</span></div>
                                                    <div class="ac_subDetail_id">
                                                        <div class="ac_subName_id"> <span>Reset App Configurations</span> <span class="anl_servicename">...</span> </div>
                                                        <div class="ac_name_id">
                                                            Click to reset or remove
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        
                                        </div>

                                    </div>
                                    

                                </div>

                                <div class="ua_settings_footer">
                                
                                    <div><hr style="top: 0; width: calc(100% - 24.5px); left: 3.5px;"></div>

                                    <div class="uaSetting_footerOuter">

                                        <div class="uaSetting_Subfooter">
                                            <a id="signedProfileUserLink_subs" class="listVidLink" href="./store" target="_blank">
                                                <div class="suo-item">
                                                    <span class="material-icons">payments</span>
                                                    <span class="uaSettingFootSubTitle">My Subscriptions</span> 
                                                </div>
                                            </a>
                                        </div>

                                        <div class="uaSetting_Subfooter">
                                            <a id="signedProfileUserLink" class="listVidLink" href="https://apps.ulgebra.com/contact" target="_blank">
                                                <div class="suo-item">
                                                    <span class="material-icons" style="width: calc(100% - 25px) !important;">contact_support</span>
                                                    <span class="uaSettingFootSubTitle">Contact Developers</span>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="uaSetting_Subfooter">
                                            <div class="suo-item" id="signoutBtn">
                                                <span class="material-icons">login</span>
                                                <span class="uaSettingFootSubTitle">Sign out</span> 
                                            </div>
                                        </div>

                                    </div>
                                    
                                    
                                        
                                    
                                
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="pageTitle">
                    ..
                </div>
                <div style="display:none" id="lic-sub-allowed-user-holder" class="pmtw-outer">
                    <div class="pmtw-holder">
                        <div class="pmt-ttl">
                            Configure license users <button class="pmtw-close-btn" onclick="$('#lic-sub-allowed-user-holder').hide()">X</button> <button class="lcpi-buymore-lic-btn" onclick="$('#lic-sub-allowed-user-holder').hide();showProductPlanDetails()">Buy More License</button>
                        </div>
                        <div class="pmt-content">
                        <div class="lcpi-item-fetch-status">
                        Loading...</div>
                        </div>
                    </div>
                </div>

                <div class="signedOut signOutbackWindow"></div>
                <div id="notSignedInError">
                    <div class="signTtl" ><span id="signin-text">Sign in to Ulgebra</span><div>
                    <div class="signInHelpText" style="
                        font-size: 14px;
                        margin-top: 10px;
                        color: green;
                        margin-bottom: -15px;
                        line-height: 1.65;
                        padding: 10px 15px;
                        /* text-align: left; */
                    ">Only Use <span class="sign-in-suggest-email">Email which you login to ${saasServiceName}</span> in below options, even if you don't have an account already.<b style="
                    color: #8b8b8b; position: absolute; bottom: 0px; right: 0px; left: 0px; background-color: whitesmoke; padding: 7px 10px; font-weight: normal;
                    ">Note: This page may not work in incognito browser mode</b></div>
                    <div id="loader"><div class="fetchingAudioBtn"><span class="spinnow material-icons">sync</span></div></div> <br>
                    <div  style="display: none" id="firebaseui-auth-container"></div>
                        </div>
                    </div>
            
                </div>
            </header>
            `);
        }
    }
};

function showUaSettingsSubDivDetails(selected) {
    $(selected).addClass('settingPageHoverMainDiv');
    $(selected).find('.ac_DetailMain_id').addClass('ac_DetailMain_idHover');
    $(selected).find('.ac_subName_id').addClass('ac_subName_idHover');
    $(selected).find('.ac_name_id').addClass('ac_name_idHover');
}

function hideUaSettingsSubDivDetails(selected) {
    $(selected).removeClass('settingPageHoverMainDiv');
    $(selected).find('.ac_DetailMain_id').removeClass('ac_DetailMain_idHover');
    $(selected).find('.ac_subName_id').removeClass('ac_subName_idHover');
    $(selected).find('.ac_name_id').removeClass('ac_name_idHover');
}

function showProductPlanDetails() {
    $('#ua-lic-product-plans').show();
}
function renderProductPlanDetails() {
    if ($('#ua-lic-product-plans').length > 0) {
        return;
    }
    document.body.insertAdjacentHTML('beforeend', `<div style="display:none" id="ua-lic-product-plans" class="pmtw-outer">
    <div onclick="$('#ua-lic-product-plans').hide()" class="pmt-ttl-close" style=" position: absolute; right: 12px; top: 7px; background-color: grey; color: whitesmoke; font-size: 20px; border-radius: 26px; cursor: pointer; height: 30px; width: 30px; padding-top: 2px; box-sizing: border-box; ">x</div>  
    <div style="text-align: right;padding: 10px 20px;background-color: rgba(0,0,0,0.5);backdrop-filter: blur(1px);">
    <div id="trial-expire-subscription-cur-ac-signout" style="color: white;">
            <span style="font-size: 12px;color: silver;">${currentUser.email}</span> 
            <div id="ua-lic-use-shared-ac-btn" onclick="UA_LIC_UTILITY.showExistingInvitedAdminDDHTML(false, true)" style="background-color: grey;border: none;border-radius: 10px;margin-left: 5px;font-size: 12px;color: white;display: inline-block;padding: 3px 8px;cursor: pointer;">Use Shared Access</div>
            <div onclick="signOut()" style="background-color: grey;border: none;border-radius: 10px;margin-left: 5px;font-size: 12px;color: white;display: inline-block;padding: 3px 8px;cursor: pointer;">Sign out</div>
    </div>

</div>
        <div>
            <a href="./store?appCode=${extensionName}&src=credit-low-alert" target="_blank">
                <div id="trial-expire-subscription-dynamic-island" class="pmtw-sec-holder">
                    <div class="pmtw-sec-top-text">
                        Your free trial is expired.
                    </div>
                    <div class="pmtw-sec-bottom-text">
                        Please purchase to continue.
                    </div>
                </div>
            </a>
        </div>
        <div>    
            <div id="active-subscription-dynamic-island" class="pmtw-sec-holder" onclick="$('#ua-lic-product-plans').hide();$('#lic-sub-allowed-user-holder').show()">
                <div class="pmtw-sec-top-text">
                    ...
                </div>
                <div class="pmtw-sec-bottom-text">
                    Click to manage plans & users
                </div>
            </div>
        </div>
        <div class="pmtw-holder">
            <div class="pmt-ttl">
                <a style="color:royalblue" href="./store?appCode=${extensionName}&src=sms-app-page-nav" target="_blank">
                Start subscription
                </a>
                <div class="pmt-chc-holder">
                    <select id="pmtp-currencySelector" onchange="ualic_changeCurrency()">
                        <option value="usd">Select Currency</option>
                        <option value="usd">USD ($)</option>
                        <option value="eur">EURO (€)</option>
                        <option value="inr">INR (₹)</option>
                        <option value="gbp">GBP (£)</option>
                        <option value="aud">AUD (A$)</option>
                    </select>
                </div>
            </div>
            <div class="pmt-content">
                <div class="pmt-plansholder">
                </div>
                <div class="pmt-userquantityholder" style="display: none">
                    <div class="pmtqn-inner">
                        <div class="pmtqn-back" onclick="$('.pmt-userquantityholder').hide()">
                        < Back
                    </div>
                    <div class="pmtqn-ttl">
                        Users / Quantity
                    </div>
                    <div class="pmtqn-subttl">
                        Number of users or agents using this application
                    </div>
                    <div class="pmtqn-content">
                        <div class="pmtqn-inc-btn" onclick="ualic_adjustQuantity(false)">
                            -
                        </div>
                        <div class="pmtqn-inc-inp">
                            <input value="1" id="quan-count" min="1" type="number" step="1"/>
                        </div>
                        <div class="pmtqn-inc-btn" onclick="ualic_adjustQuantity(true)">
                            +
                        </div>
                    </div>
                        <div class="pmtp-plan-actions">
                            <button class="pmtppa-btn" onclick="ualic_proceedToPayment()">Proceed</button>
                        </div>
                </div>
                </div>
            </div>
            <div class="ua-lic-process-loading"><span class="material-icons spinnow processing">
hourglass_top
</span> Processing, Please Wait...

    <div style="display:none" class="ualic_manual_pay_redir_window">
    <a id="ualic_manual_pay_redir_window_url" style="margin-top: 30px;font-size: 16px;display: block;color: blue;text-decoration: underline;cursor: pointer;" target="_blank" href="https://app.ulgebra.com">Click here if you're not redirected to payment</a>
     <div style="margin-top: 30px;font-size: 16px;display: block;color: white;cursor: pointer;background-color: royalblue;display: inline-block;padding: 5px 15px;border-radius: 5px;" onclick="window.location.reload()">I have made the payment, refresh now</div></div>
</div>
        </div>
    </div>`);
    $('#pmtp-currencySelector').val(selectedCurrency);
    UA_LIC_UTILITY.startListeningCreditsForUser(UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID ? UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID : currentUser.uid);
}

async function uaAppUninstallConfirmTextCheck(value) {
    if (value != 'confirm') {
        $('#ua-app-uninstall-reset').attr('disabled', true);
        $('#ua-app-uninstall-reset').css({ cursor: "not-allowed", color: "#6c6666", background: "#f5f8fa" });
    }
    else {
        $('#ua-app-uninstall-reset').attr('disabled', false);
        $('#ua-app-uninstall-reset').css({ cursor: "pointer", color: "white", background: "#f04b51" });
    }
}

async function appUninstallationHandler() {
    if ($("#ua-app-uninstall-confirm-text").val() != "confirm") return;
    $("#ua-app-uninstall-reset").text("Loading...")
    firebase.functions().httpsCallable('new_appUninstallationHandler')({
        appCode: extensionName,
        appOrgScope: extensionName.split("for")[1] + "_orgs",
        orgId: appsConfig.UA_DESK_ORG_ID ? appsConfig.UA_DESK_ORG_ID : "",
        secContext: appsConfig.APP_UNIQUE_ID ? appsConfig.APP_UNIQUE_ID : ""
    }).then((response) => {
        console.log("deleted", response);
        $("#ua-app-uninstall-reset").text("Done")
        location.reload();
    }).catch(err => { console.log(err); });
}

function addProductPlan(priceID, priceData) {
    var featureHTML = '';
    if (!priceData.active || priceData.type === "one_time") {
        console.log("inactive or one_time");
        return;
    }
    Object.keys(UALIC_APP_PROD_DATA).forEach(item => {
        if (item.startsWith('stripe_metadata_plan_feat')) {
            var stripeFeatPlanName = item.split('stripe_metadata_plan_feat_')[1];
            if (priceData.description.toLowerCase().replaceAll(/[\W_]+/g, '') === stripeFeatPlanName) {
                UALIC_APP_PROD_DATA[item].split('\n').forEach(featLineITEM => {
                    if (featLineITEM.trim().length < 1) {
                        return;
                    }
                    featureHTML += `<div class="pmtppf-item">${featLineITEM}</div>`;
                });
            }
        }
    });
    $('.pmt-plansholder').append(`<div ${selectedCurrency !== priceData.currency ? 'style="display:none"' : ''} class="pmtp-item price-item ${priceData.currency}">
                        <div class="pmtp-planname">
                            ${priceData.description}
                        </div>
                        <div class="pmtp-planprice">
                            ${currencyMap[priceData.currency]}${priceData.unit_amount / 100}
                        </div>
                        <div class="pmtp-planperhow">
                            /${UALIC_APP_PROD_DATA.stripe_metadata_amount_per_display ? UALIC_APP_PROD_DATA.stripe_metadata_amount_per_display + ' /' + priceData.interval : ''}
                        </div>
                        <div class="pmtp-plan-features">
                            ${featureHTML}
                        </div>
                        <div class="pmtp-plan-actions">
                            <button class="pmtppa-btn" onclick="ualic_selectPaymentPlan('${priceID}')">${priceData.trial_period_days > 0 ? 'Start Free Trial' : 'Select Plan'}</button>
                        </div>
                    </div>`);
}

var GoogleAuth;
var currentUser = null;
var db = null;
let isMobileView = mobileCheck();
var initTrackingId = null;
var enableTrialModesForPay = true;
var fbAnalytics = {};
window.onload = (function () {
    /// $('body').append("<audio autoplay src='ZHpyi6oDmZM/vWvTRTsVMiZbhRUPeHLmqbC25mI3/1593485308379.mpeg'></audio>");
    var firebaseConfig = {
        apiKey: "AIzaSyDsvl4lleRa8k-3UuDqltueXZy1V27f0Sw",
        authDomain: "ulgebra-license.firebaseapp.com",
        databaseURL: "https://ulgebra-license.firebaseio.com",
        projectId: "ulgebra-license",
        storageBucket: "ulgebra-license.appspot.com",
        messagingSenderId: "356364764905",
        appId: "1:356364764905:web:4bea988d613e73e1805278",
        measurementId: "G-54FN510HDW"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    storageRef = firebase.storage().ref();
    db = firebase.firestore();
    fbAnalytics = firebase.analytics();
    if (window.location.href.startsWith('http://localhost:') && !APP_LOAD_FROM_LIVE) {
        firebase.functions().useFunctionsEmulator('http://localhost:5001');
        firebase.firestore().settings({
            host: 'localhost:8080',
            ssl: false
        });
    }

    // handleClientLoad();
    var uiConfig = getFirebaseAuthConfig();

    var fbaseauth = firebase.auth();
    fbaseauth.onAuthStateChanged(function (user) {
        $('.waitTillAuthCheck').removeClass('waitToResolve');
        user ? handleSignedInUser(user) : handleSignedOutUser();
        if (user) {
            UA_LIC_UTILITY.initialize();
        }
        handleauthoriztion();
    });
    /*try{
        if(extensionName && extensionName.indexOf('hubspot') < 0){
            uiConfig.credentialHelper = firebaseui.auth.CredentialHelper.GOOGLE_YOLO;
        }
        fbaseauth.setPersistence(firebase.auth.Auth.Persistence.LOCAL);
        // uiConfig.credentialHelper = firebaseui.auth.CredentialHelper.GOOGLE_YOLO;
    }
    catch(ex){
        console.log(ex);
        try{
            fbaseauth.setPersistence(firebase.auth.Auth.Persistence.SESSION);
            // uiConfig.credentialHelper = firebaseui.auth.CredentialHelper.GOOGLE_YOLO;
        }catch(ex){
            console.log(ex);
            fbaseauth.setPersistence(firebase.auth.Auth.Persistence.NONE);
            uiConfig.credentialHelper = firebaseui.auth.CredentialHelper.NONE;
        }
    }*/
    var ui = new firebaseui.auth.AuthUI(fbaseauth);
    ui.start('#firebaseui-auth-container', uiConfig);

    $("#signoutBtn").click((function () {
        firebase.auth().signOut();
        window.location.reload();
    }));

});

function signOut() {
    showErroWindow('Logging out..', 'Please wait...');
    firebase.auth().signOut();
    window.location.reload();
}

function getFirebaseAuthConfig() {
    if (window.location.href.indexOf("hubspot") > -1) {
        return {
            callbacks: {
                // Called when the user has been successfully signed in.
                'signInSuccessWithAuthResult': function (authResult, redirectUrl) {
                    if (currentUser == null && authResult.user) {
                        handleSignedInUser(authResult.user);
                        handleauthoriztion();
                    }
                    if (authResult.additionalUserInfo) {
                        document.getElementById('is-new-user').textContent =
                            authResult.additionalUserInfo.isNewUser ?
                                'New User' : 'Existing User';
                    }
                    // Do not redirect.
                    return false;
                },
                uiShown: function () {
                    $("#credential_picker_container").hide();
                    $("#notSignedInError").hide();
                    $(".loader").hide();
                    $(".showAfterSignIn").show();
                }
            },
            'credentialHelper': firebaseui.auth.CredentialHelper.NONE,
            signInFlow: 'popup',
            signInOptions: [
                {
                    "provider": firebase.auth.EmailAuthProvider.PROVIDER_ID
                },
                {
                    provider: firebase.auth.GoogleAuthProvider.PROVIDER_ID,
                    clientId: '356364764905-ua06urrrgp9qh0u61qqtb0tk6cv40jmm.apps.googleusercontent.com'
                }
            ],
            // credentialHelper: firebaseui.auth.CredentialHelper.GOOGLE_YOLO,
            tosUrl: 'https://apps.ulgebra.com/terms',
            privacyPolicyUrl: 'https://apps.ulgebra.com/privacy-policy'

        };
    }
    else {
        return {
            "callbacks": {
                // Called when the user has been successfully signed in.
                'signInSuccessWithAuthResult': function (authResult, redirectUrl) {
                    if (currentUser == null && authResult.user) {
                        handleSignedInUser(authResult.user);
                        handleauthoriztion();
                    }
                    if (authResult.additionalUserInfo) {
                        document.getElementById('is-new-user').textContent =
                            authResult.additionalUserInfo.isNewUser ?
                                'New User' : 'Existing User';
                    }
                    // Do not redirect.
                    return false;
                },
                uiShown: function () {
                    $("#credential_picker_container").hide();
                    $("#notSignedInError").hide();
                    $(".loader").hide();
                    $(".showAfterSignIn").show();
                }
            },
            // Opens IDP Providers sign-in flow in a popup.
            signInFlow: 'popup',
            signInOptions: [
                // Leave the lines as is for the providers you want to offer your users.
                {
                    "provider": firebase.auth.EmailAuthProvider.PROVIDER_ID
                },
                {
                    provider: firebase.auth.GoogleAuthProvider.PROVIDER_ID,
                    clientId: '356364764905-ua06urrrgp9qh0u61qqtb0tk6cv40jmm.apps.googleusercontent.com'
                }
            ],
            credentialHelper: firebaseui.auth.CredentialHelper.GOOGLE_YOLO,
            // tosUrl and privacyPolicyUrl accept either url string or a callback
            // function.
            // Terms of service url/callback.
            tosUrl: 'https://apps.ulgebra.com/terms',
            privacyPolicyUrl: 'https://apps.ulgebra.com/privacy-policy'
        };
    }

}

var handleSignedOutUser = function () {
    $("#notSignedInError").show();
    $(".signedIn").hide();
    $('#loader').hide();
    $(".signedOut").show();
    $("#firebaseui-auth-container").show();
    $(".showAfterSignIn").hide();
    $("#credential_picker_container").show();
    if (UA_SAAS_SERVICE_APP && typeof UA_SAAS_SERVICE_APP.provideSuggestedCurrentEnvLoginEmailID === "function") {
        UA_SAAS_SERVICE_APP.provideSuggestedCurrentEnvLoginEmailID();
    }
};

function getSafeString(rawStr) {
    if (typeof rawStr === "number") {
        rawStr = rawStr + "";
    }
    if (!rawStr || rawStr.trim() === "") {
        return "";
    }
    return $('<textarea/>').text(rawStr).html();
}

function doPageSignAction() {

}

var handleSignedInUser = function (user) {
    $("#credential_picker_container").hide();
    currentUser = firebase.auth().currentUser;
    $("#notSignedInError").hide();
    $(".showAfterSignIn").show();
    $(".signedIn").show();
    $(".signedOut").hide();
    var photoURL = currentUser.photoURL ? currentUser.photoURL : "https://app.azurna.com/images/profile_icon.png";
    $("#signInUserImg").attr({
        "src": photoURL
    });
    $("#signInUserName").text(currentUser.displayName);
    $("#signInUserEmail").text('(' + currentUser.email + ')');

    // $("#signedProfileUserLink").attr({
    //     "href": "/user?id=" + currentUser.uid
    // });
    currentUser = firebase.auth().currentUser;
    if (!currentUser.emailVerified) {
        showErroWindow('Verify Ulgebra Account', `You need to verify your email for Ulgebra account. <br><br>Kindly check your mailbox (${currentUser.email}) including spam & notifications folder.<br><br><button class="ua_service_login_btn ua_primary_action_btn ua_email_verify_err_send_btn" onclick="currentUser.sendEmailVerification();$('.ua_email_verify_err_send_btn').text('Verification mail sent');$('#ua-contact-if-no-verify-mail-tip').show()">Send verification email now</button> <div style="display:none;color:green" id="ua-contact-if-no-verify-mail-tip"><br><br> If you did not receive any mail, <a href="https://apps.ulgebra.com/contact" target="_blank">Contact Us here</a></di>`, false, false, 10000);
    }
    if (UA_SAAS_SERVICE_APP && typeof UA_SAAS_SERVICE_APP.provideSuggestedCurrentEnvLoginEmailID === "function") {
        UA_SAAS_SERVICE_APP.provideSuggestedCurrentEnvLoginEmailID();
    }
    ualic_getAPPProdDetails();
    doPageSignAction(true);
    ua_doServiceCommonActions();
    UA_APP_UTILITY.startListeningNotificationsCountForUser(currentUser.uid);
};

var ua_doServiceCommonActions_retries = 0;
async function ua_doServiceCommonActions() {
    return true;
    ua_doServiceCommonActions_retries++;
    try {
        if (extensionName && extensionName.indexOf("pipedrive") > 0) {
            if (!appsConfig.APP_UNIQUE_ID && ua_doServiceCommonActions_retries < 6) {
                setTimeout(ua_doServiceCommonActions, 3000);
                return false;
            }
            firebase.functions().httpsCallable('makePipedriveHTTPCall')(JSON.stringify({ extension: extensionName, url: '/v1/users', method: 'GET' })).then((response) => {
                var authorizedPipedriveUser = null;
                response.data.data.forEach(item => { if (item.id === parseInt(appsConfig.APP_UNIQUE_ID)) { authorizedPipedriveUser = item; return; } });
                $('#WTR-INTEG-STATUS').append(`<div style="margin-top: 15px; color: rgb(86,86,86); font-size: 14px; background-color: whitesmoke; padding: 7px 20px; text-align: center; border-radius: 5px; width: 100%; box-sizing: border-box;">Integration authorized for pipedrive account : <b>${authorizedPipedriveUser.name} (${authorizedPipedriveUser.email})</b></div>`);
            }).catch(err => { console.log(err) });
        }
        else if (extensionName && extensionName.indexOf("hubspot") > 0) {
            var apiRes = await makeHubspotcall(JSON.stringify({
                "url": "https://api.hubapi.com/oauth/v1/access-tokens/",
                "method": "GET",
                "extension": extensionName,
                "reason_type": "accountName"
            })).then(function (resp) {
                // console.log(resp);
                return resp.data;
            });
            if (apiRes !== "accesstoken_not_found") {
                $('#WTR-INTEG-STATUS').append(`<div style="margin-top: 15px; color: rgb(86,86,86); font-size: 14px; background-color: whitesmoke; padding: 7px 20px; text-align: center; border-radius: 5px; width: 100%; box-sizing: border-box;">Integration authorized for HubSpot account : <b>${apiRes.hub_domain.split("-")[0]}</b> (${apiRes.hub_id}) by <i>${apiRes.user}</i></div>`);
            }
            // else{
            //     $('#WTR-INTEG-STATUS').append(`<div style="margin-top: 15px; color: rgb(86,86,86); font-size: 14px; background-color: whitesmoke; padding: 7px 20px; text-align: center; border-radius: 5px; width: 100%; box-sizing: border-box;">Integration authorized for HubSpot account : <i>Unable to fetch, Contact Ulgebra</i></div>`);
            // }
        }
        else if (extensionName && extensionName.indexOf("gorgias") > 0) {
            if (!appsConfig.APP_UNIQUE_ID && ua_doServiceCommonActions_retries < 6) {
                setTimeout(ua_doServiceCommonActions, 3000);
                return false;
            }
            db.collection("ulgebraUsers").doc(firebase.auth().currentUser.uid).get().then(function (doc) {
                if (doc.exists && doc.data().gorgiasPortalId) {
                    $('#WTR-INTEG-STATUS').append(`<div style="margin-top: 15px; color: rgb(86,86,86); font-size: 14px; background-color: whitesmoke; padding: 7px 20px; text-align: center; border-radius: 5px; width: 100%; box-sizing: border-box;">Integration authorized for gorgias account : <b>${doc.data().gorgiasPortalId}.gorgias.com</b></div>`);
                    return doc.data().gorgiasPortalId;
                }
                else {
                    return null;
                }
            });
        }
        else if (extensionName && extensionName.indexOf("formonday") > 0) {
            firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({ appCode: extensionName, url: 'https://api.monday.com/v2', method: 'POST', data: { "query": 'query { me { name email } account {name slug}}' }, headers: { "Authorization": "Bearer {{AUTHTOKEN}}", "Content-Type": "application/json" } }).then((response) => {
                response = response.data.data;
                $("#saveAndGoToServiceBtn").attr({ "data-serviceUrl": `https://${response.data.account.slug}.monday.com` }).show();
                $('#WTR-INTEG-STATUS').append(`<div style="margin-top: 15px; color: rgb(86,86,86); font-size: 14px; background-color: whitesmoke; padding: 7px 20px; text-align: center; border-radius: 5px; width: 100%; box-sizing: border-box;">Integration authorized for monday.com account : <b>${response.data.me.name} (${response.data.me.email}) with <a href="https://${response.data.account.slug}.monday.com" target="_blank">${response.data.account.name}</a></b></div>`);
            }).catch(err => { console.log(err); });
        }
        else if (extensionName && extensionName.indexOf("forbitrix24") > 0) {
            db.collection("bitrix24Users").doc(firebase.auth().currentUser.uid).get().then(function (doc) {
                if (doc.exists && doc.data().portalId) {
                    $('#WTR-INTEG-STATUS').append(`<div style="margin-top: 15px; color: rgb(86,86,86); font-size: 14px; background-color: whitesmoke; padding: 7px 20px; text-align: center; border-radius: 5px; width: 100%; box-sizing: border-box;">Integration authorized for Bitrix24 account domain : <b>${doc.data().portalId}</b></div>`);
                    return doc.data().portalId;
                }
                else {
                    return null;
                }
            });
        }
    }
    catch (ex) {
        console.log(ex);
    }
}

$(function () {

    APP.initialize();

    var isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;

    if (isFirefox) {
        $('body').append(`
            <style type="text/css">
                .process-window-outer, .error-window-outer {
                    background-color : rgba(256, 256, 256, 0.85);
                }
            </style>`);
    }

    $('.userInfoBtn').click(function () {
        toggleUserNavSideBar();
        $(".notification-side-panel-outer").hide();
    });

});

function toggleUserNavSideBar() {
    // var hidden = $(".signedUserOptions");
    //     if (hidden.hasClass('visible')) {
    //         hidden.animate({"right": "-750px"}).removeClass('visible');
    //     } else {
    //         hidden.animate({"right": "0px"}).addClass('visible');
    //     }
    $('body').toggleClass('settingPageView');
}

function hideUserNavSideBar() {
    $('body').removeClass('settingPageView');
    //$(".signedUserOptions").animate({"right": "-750px"}).removeClass('visible');
}

function makeHubspotcall(data) {
    var makeHubspotHTTPCall = firebase.functions().httpsCallable('makeHubspotHTTPCall');
    return makeHubspotHTTPCall(data).then(function (result) {
        //   console.log(result);
        return result;
    }).catch(function (error) {
        var code = error.code;
        var message = error.message;
        var details = error.details;
        console.log(error)
        return null;
        // ...
    });
}


function showElem(elem) {
    if (isMobileView) {
        elem.slideDown();
    } else {
        elem.show();
    }
}

function hideElem(elem) {
    if (isMobileView) {
        elem.slideUp();
    } else {
        elem.hide();
    }
}

function removeElem(elem) {
    elem.remove();
}

function removeIDElem(elemID) {
    $(elemID).remove();
}

function hideElemNoAnimation(elem) {
    elem.hide();
}

function mobileCheck() {
    let check = false;
    (function (a) {
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4)))
            check = true;
    })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
}

function getQueryParamValue(q) {
    let query = new URLSearchParams(window.location.search);
    return query.get(q);
}

function showProcess(text, id) {
    showTopProgressBar(id);
    $(".process-window-outer").show();
    $("#process-window-items").append(`<div id="process-item-${id}" class="process-window-item"> <i class="material-icons spinnow">hourglass_top</i> ${text}</div>`);
}

function processCompleted(id) {
    removeTopProgressBar(id);
    $(`#process-item-${id}`).remove();
    if (($("#process-window-items").children().length) === 0) {
        $(".process-window-outer").hide();
    }
}

function showTryAgainError() {
    return showErroMessage('Try again later');
}

function showInvalidCredsError() {
    return showErroMessage('Given Plivo Auth ID or Authtoken is Invalid <br><br> Try again with proper Plivo credentials from <a href="https://console.plivo.com/dashboard/" title="Click to go to plivo dashboard" target="_blank" noopener nofollow>Plivo dashboard</a>.');
}

function showErroMessage(html) {
    return showErroWindow('Unable to process your request', html);
}

function showErroWindow(title, html, noCloseOption = false, isPositive = false, zIndex = null) {
    hideUserNavSideBar();
    if (UA_APP_UTILITY.isFormView()) {
        if (title === 'Sender is empty!') {
            UA_APP_UTILITY.toggleFieldSenderInvalidErrorMessage(true);
            return;
        }
        if (title === 'Recipient list is empty!') {
            UA_APP_UTILITY.toggleFieldRecipientInvalidErrorMessage(true);
            return;
        }
        if (title === 'Message is empty!') {
            UA_APP_UTILITY.toggleFieldMessageInvalidErrorMessage(true);
            return;
        }
    }
    if (!zIndex) {
        zIndex = curId;
    }
    var id = errorId++;
    let errorWindowID = `error-window-${id}`;
    $('body').append(`<div class="error-window-outer ${isPositive ? ' success-window-outer ' : ''}" style="${zIndex ? 'z-index:' + zIndex : ''}" id="${errorWindowID}">
            <div class="error-window-inner">
                <div class="error-window-title">
                    ${title}
                </div>
                <div class="error-window-detail">
                    ${html}
                </div>
                <div class="error-window-close" ${noCloseOption ? 'style="display:none"' : ''} onclick="removeIDElem('#error-window-${id}')">
                   <i class="material-icons">close</i> Close
                </div>
            </div>
        </div>`);
    return errorWindowID;
}

function valueExists(val) {
    return val !== null && val !== undefined && val !== 'undefined' && val.length > 0 && val !== "null";
}

function closeAllErrorWindows() {
    $('.error-window-outer').remove();
}

String.prototype.capitalize = function () {
    var c = '';
    var s = this.split(' ');
    for (var i = 0; i < s.length; i++) {
        c += s[i].charAt(0).toUpperCase() + s[i].slice(1) + ' ';
    }
    return c.trim();
};

/*var licSupportedPlans = {
"price_1IdrL2Ho9p6j6FgR5qyjJOo0":{
 "type": "premium"
},"price_1IP0aDHo9p6j6FgRibv4isf0":{
"type": "premium"
},"price_1I6CrpHo9p6j6FgRISpRviN4":{
"type": "premium"
},"price_1HyZblHo9p6j6FgRQzDdcl81":{
"type": "premium"
},"price_1HyZbTHo9p6j6FgRGfOwxuwQ":{
"type": "premium"
},"price_1HyZbFHo9p6j6FgRVHU989Un":{
"type": "premium"
},"price_1IP0ZkHo9p6j6FgRqHyLCI4z":{
"type": "business"
},"price_1I6CrFHo9p6j6FgRhnRWESX1":{
"type": "business"
},"price_1HyZaxHo9p6j6FgRnWjTs3we":{
"type": "business"
},"price_1HyZadHo9p6j6FgRe0Ai3LrT":{
"type": "business"
},"price_1HyZEtHo9p6j6FgRAvpfOvqM":{
"type": "business"
},"price_1HyZAgHo9p6j6FgReqwmnUp7":{
"type": "classic"
},"price_1HyZAgHo9p6j6FgRqQGZavms":{
"type": "classic"
},"price_1IP0YzHo9p6j6FgRg15L9O7H":{
"type": "classic"
},"price_1I6CmKHo9p6j6FgRQv6Cpet2":{
"type": "classic"
},"price_1HyeEsHo9p6j6FgRoSkOo1kP":{
"type": "classic"
},"price_1HyeESHo9p6j6FgRy1Pwz7FH":{
"type": "classic"
}};*/

const STRIPE_PUBLISHABLE_KEY = 'pk_live_vZ8o9dQoM2zharvDfdExWgZf00GIVF1VvZ';

var taxRates = ['txr_1Hv2S0Ho9p6j6FgR0qRizWPw'];
var selectedProductId = null;
var selectedCurrency = new Date().toString().indexOf("India") > 0 ? "inr" : "usd";
var selectedPaymentPlanId = null;
var currencyMap = {
    "inr": "₹",
    "usd": "$",
    "eur": "€",
    "gbp": "£",
    "aud": "A$"
};
var selectedSubType = "recurring";

var pricesObj = {};

const functionLocation = 'us-central1';

function viewMySubcribed(selected) {

    if ($(selected).attr('data-checked') == 1) {
        $(selected).attr('data-checked', 0);
        $(selected).removeClass('subcribed-portal-buttonTrue');
        $('#selectProductsContainer').show();
        $('#subcribedProductsContainer').hide();
    }
    else {
        $(selected).attr('data-checked', 1);
        $(selected).addClass('subcribed-portal-buttonTrue');
        $('#selectProductsContainer').hide();
        $('#subcribedProductsContainer').show();
    }

}

Array.prototype.remove_by_value = function (val) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] === val) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};

function subcribedProductId(quantity, subcribeId) {

    db.collection('customers').doc(currentUser).collection('subscriptions').doc(subcribeId).collection('users').get().then(function (querySnapshot) {

        if (!querySnapshot.docs.length) {
            for (i = 0; i < quantity; i++) {
                $('.userEmailAddOuter').append(`<div class="addUserEmailIdOut">
                      <div class="addUserEmailIdBox" style=""><input type="email" data-added="0" placeholder="Add User Email" class="addUserEmailIdInput"></div>
                      <div class="addedUserEmailIdOut" style="display: none;">
                          <div class="addedUserEmailId" style=""></div>
                          <div class="addUserEmailIdRemove">×</div>
                      </div>
                  </div>`);
            }
            $('.addUserEmailIdInput').first().focus();
            return true;
        }

        let allQuantityCheck = 0;
        querySnapshot.forEach(async function (doc) {

            allQuantityCheck++;
            $('.userEmailAddOuter').append(`<div class="addUserEmailIdOut">
                      <div class="addUserEmailIdBox"  style="display: none;"><input type="email" data-added="1" placeholder="Add User Email" class="addUserEmailIdInput"></div>
                      <div class="addedUserEmailIdOut">
                          <div class="addedUserEmailId" style="">${doc.data().email}</div>
                          <div class="addUserEmailIdRemove">×</div>
                      </div>
                  </div>`);

        });

        if (allQuantityCheck < quantity) {
            quantity = quantity - allQuantityCheck;
            for (i = 0; i < quantity; i++) {
                $('.userEmailAddOuter').append(`<div class="addUserEmailIdOut">
                      <div class="addUserEmailIdBox" style=""><input type="email" data-added="0" onkeypress="$(this).removeClass('addUserEmailIdInputErr')" onfocus="$(this).css({'border-color': '#2172f6'})" onfocusout="$(this).css({'border-color': 'black'})" placeholder="Add User Email" class="addUserEmailIdInput"></div>
                      <div class="addedUserEmailIdOut" style="display: none;">
                          <div class="addedUserEmailId" style=""></div>
                          <div class="addUserEmailIdRemove" onclick="ualic_removeEditUserEmail(this)">×</div>
                      </div>
                  </div>`);
            }
        }
        else {
            $('.addUsersEmailIdSaveButt').hide();
        }

    });

    function removeEditUserEmail(selected) {
        $('.addUsersEmailIdSaveButt').show();
        $(this).parent().parent().find('.addedUserEmailIdOut').hide();
        $(this).parent().parent().find('.addUserEmailIdBox').show();
        $(this).parent().parent().find('.addUserEmailIdInput').attr('data-added', '0');
    }

    function addUsersEmailIdSaveButt() {
        $('.addUserEmailIdInput').each(function () {

            if ($(this).attr('data-added') == '0') {

                let reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                if ($(this).val().trim() == '' || reg.test(int_mail) == false) {
                    $(this).addClass('addUserEmailIdInputErr').focus();
                    return false;
                }

                db.collection('customers').doc(currentUser).collection('subscriptions').doc(subcribeId).collection('users').doc($(this).val().trim()).set({
                    email: $(this).val().trim(),
                    createdTime: Date.now()
                }, { merge: true });

                $(this).parent().parent().find('.addedUserEmailIdOut').show();
                $(this).parent().parent().find('.addedUserEmailId').text($(this).val().trim());
                $(this).val('');
                $(this).parent().parent().find('.addUserEmailIdInput').attr('data-added', '1');
                $(this).parent().parent().find('.addUserEmailIdBox').hide();

            }

        });
    }

}

var existingLicenseOwnerList = {};
async function ualic_fetchExistingLicenseOwnersList() {
    /* firebase.database().ref(`ualic_user_credits/${currentUser}`).on('value', function(snapshot){
         $('#ua-lic-credit-amount').text(snapshot.val() ? snapshot.val() : 0);
       });*/

    await db.collection('UALicenseOwners').doc(currentUser.uid).collection('subscriptions').get().then(snapshot => {
        if (snapshot.empty) {
            return false;
        }
        snapshot.docs.forEach(item => {
            existingLicenseOwnerList[item.id] = item.data();
        });
        return true;
    });
    //ualic_renderSubscribeUserList();
}
var UALIC_APP_PRODUCT_ID = null;
var UALIC_APP_PROD_DATA = {};
var UALIC_APP_PRICE_DATA = {};

async function ualic_getAPPProdDetails() {
    if (extensionName.startsWith('pinnacle') || extensionName.startsWith('telebu') || extensionName.startsWith('karix') || extensionName.startsWith('rivet') || extensionName.startsWith('telnyx')) {
        return false;
    }
    if (typeof extensionName === "undefined" || !extensionName) {
        $('#suo-item-ua-app-lic-plan').hide();
        return;
    }
    $('#suo-item-ua-app-lic-plan .ac_name_id').text('Fetching product details...');
    ualic_fetchExistingLicenseOwnersList();
    let appProductResp = await db.collection(`products`).where('stripe_metadata_appcode', '==', extensionName).get();
    if (!appProductResp || appProductResp.docs.length === 0) {
        $('#suo-item-ua-app-lic-plan').hide();
        return false;
    }
    UALIC_APP_PROD_DATA = appProductResp.docs[0].data();
    if (UALIC_APP_PROD_DATA) {
        if (UALIC_APP_PROD_DATA.stripe_metadata_document) {
            $("#suo-item-support-help-guide-link").attr("href", UALIC_APP_PROD_DATA.stripe_metadata_document);
        }
        if (UALIC_APP_PROD_DATA.stripe_metadata_playlist) {
            $("#suo-item-support-help .ac_name_id").append(`<a target="blank" href="${UALIC_APP_PROD_DATA.stripe_metadata_playlist}">Videos</a>`);
        }
        // if(UALIC_APP_PROD_DATA.stripe_metadata_playlist){
        //     // $("#suo-item-support-help .ac_name_id").append(`<a target="blank" href="${UALIC_APP_PROD_DATA.stripe_metadata_playlist}"> - Demo Videos</a>`);
        // }
    }
    UALIC_APP_PRODUCT_ID = appProductResp.docs[0].id;
    let priceDataResp = await db.collection(`products/${UALIC_APP_PRODUCT_ID}/prices`).get();
    if (priceDataResp && priceDataResp.docs.length > 0) {
        licSupportedPlans = {};
        priceDataResp.docs.forEach(priceData => {
            UALIC_APP_PRICE_DATA[priceData.id] = priceData.data();
            licSupportedPlans[priceData.id] = priceData.data();
        });
    }
    console.log("All price data fetch Done!");
    ualic_renderSubscribeUserList();
    renderProductPlanDetails();
    $('.pmt-plansholder').html('');
    for (var item in UALIC_APP_PRICE_DATA) {
        addProductPlan(item, UALIC_APP_PRICE_DATA[item]);
    }
    $('.pmt-plansholder').append(`<div class="pmtp-descalim-tip" style="font-size: 12px;color: green;text-align: right;position: absolute;bottom: 0px;left: 0px;right: 0px;background-color: #e1ebf4;padding: 8px 15px;line-height:1.65">
                    
                    <a target="_blank" href="https://apps.ulgebra.com/cancel-refund-policy">Refund policy</a> |  
            <a target="_blank" href="https://apps.ulgebra.com/contact">Contact Us</a>
                </div>`);
}
var existingLicenseOwnerCountForCurrentSubs = 0;
async function ualic_renderSubscribeUserList() {
    try {
        $('#suo-item-ua-app-lic-plan .ac_name_id').text('Fetching subscription details...');
        //$('.license-configure-panel').show();
        $('.lcpi-item-fetch-status').text('Checking for active subscriptions...');
        $('.lcp-lic-item').remove();
        var planPurchaseUsersListFilled = await db.collection('customers').doc(currentUser.uid).collection('subscriptions').get().then(async (snapshot) => {
            //   if (snapshot.empty) {
            //     $('.lcpi-item-fetch-status').text("You don't have any active subscriptions, purchase below then configure users here.");
            //     return false;
            //   }
            enableTrialModesForPay = false;
            var activeSubsCount = 0;

            for (var i = 0; i < snapshot.docs.length; i++) {
                try {
                    const subscription = snapshot.docs[i].data();
                    if (subscription.product.id !== UALIC_APP_PRODUCT_ID) {
                        continue;
                    }
                    var productId = UALIC_APP_PRODUCT_ID;
                    var productData = UALIC_APP_PROD_DATA;
                    $('.lcpi-item-fetch-status').hide();
                    var priceData = UALIC_APP_PRICE_DATA[subscription.price.id];
                    var priceId = subscription.price.id;
                    var subscriptionId = snapshot.docs[i].id;
                    var isActive = subscription.status === "active" || subscription.status === "trialing";
                    if ((!isActive && !includeInActiveSubs)) {
                        continue;
                    }
                    planPurchaseUsersListFilled = true;
                    if (!priceData) {
                        console.log(' priceData not found ' + priceId);
                        return;
                    }
                    if (!productData) {
                        console.log(' productData not found ' + productId);
                        return;
                    }
                    activeSubsCount++;
                    $('#lic-sub-allowed-user-holder .pmt-content').append(`
        <div class="lcp-lic-item" id="license-item-${subscriptionId}">
            <div class="lcpi-itemmeta">
            <div class="lcpi-icon">
              <img src="${productData.images ? productData.images[0] : ''}"/>
            </div>
            <div class="lcpi-ttl-meta">
              <div class="lcp-lic-item-ttl">
                ${productData.name}
              </div>
              <div class="lcpi-quantity">
                <div class="lcpi-plantype">${priceData.description}</div> <div class="lcpi-plantype">${subscription.quantity} Users</div>

                <span style="color: grey;margin-left: 15px;"> ${subscription.status.toUpperCase()} ${isActive ? " from <b>" + (subscription.current_period_start ? subscription.current_period_start.toDate().toLocaleDateString() : "") + "</b> to <b>" + (subscription.current_period_end ? subscription.current_period_end.toDate().toLocaleDateString() : "") + "</b>" : "<span class='c-red' style='margin-right:10px'>Subscription is not active</span>" + " Cancelled on <b>" + (subscription.canceled_at ? subscription.canceled_at.toDate().toLocaleDateString() : "") + "</b>"} </span>

              </div>
            </div>
          </div>
            <div class="lcpi-user-tip">
              ${productData.stripe_metadata_userIdText && productData.stripe_metadata_userIdText !== "" ? productData.stripe_metadata_userIdText : 'Provide EMail Ids of your ' + saasServiceName + ' users to allow them to use this license.'}
            </div>
            <div class="lcpi-users">

            </div>
            <div class="lcpi-action">
              <button class="lcpi-action-save" ${!isActive ? "style='display:none'" : ""} onclick="ualic_updateLICUsers('${subscriptionId}','${priceId}')">Save</button>
              <button class="lcpi-action-save" ${!isActive ? "style='display:none'" : "style='float: right;background-color: green;'"} onclick="$('#lic-sub-allowed-user-holder').hide();showProductPlanDetails()">Add more users</button>
            </div>
          </div>`);

                    for (var j = 0; j < subscription.quantity; j++) {
                        var value = existingLicenseOwnerList[subscriptionId] && existingLicenseOwnerList[subscriptionId].userIds.length > j ? existingLicenseOwnerList[subscriptionId].userIds[j] : "";
                        if (value) {
                            existingLicenseOwnerCountForCurrentSubs++;
                        }
                        $(`#license-item-${subscriptionId} .lcpi-users`).append(`<div class="lcpi-user-item">
                <div class="lcpi-u-index">${j + 1}</div><div class="lcpi-u-inp"><input class="inp_lcpi-user" type="text" ${!isActive ? "disabled" : ""} value="${value}" ${value && value.trim() !== "" ? "disabled readonly" : ""}/></div>
              </div>`);
                    }
                } catch (ex) {
                    console.log(ex);
                }
            }
            if (activeSubsCount === 0) {
                $('#suo-item-ua-app-lic-plan .ac_name_id').html("Using <b>free trial</b>. Click to Buy & Activate all features");
                try {
                    if (typeof fetchLICDetails !== "undefined" && typeof licSupportedPlans === "object" && licSupportedPlans) {
                        var isPlanAdded = await fetchLICDetails(currentUser.email);
                        if (isPlanAdded) {
                            UA_LIC_UTILITY.CURRENT_USER_DOES_HAVE_ANY_VALID_PLAN = true;
                            $('#suo-item-ua-app-lic-plan .ac_name_id').html("Active <b>valid subscription</b>.");
                            return;
                        }
                        else {
                            UA_LIC_UTILITY.CURRENT_USER_DOES_HAVE_ANY_VALID_PLAN = false;
                            $('#suo-item-ua-app-lic-plan .ac_name_id').html("Using <b>free trial</b>. Click to Buy & Activate all features");
                        }
                    }
                }
                catch (e) {
                    console.log(e);
                }
                //showProductPlanDetails();
            }
            else {
                $('#ua-lic-product-plans').hide();
                if (existingLicenseOwnerCountForCurrentSubs === 0) {
                    $('#lic-sub-allowed-user-holder').show();
                }
                $("#active-subscription-dynamic-island").css('display', 'inline-block');
                $("#active-subscription-dynamic-island .pmtw-sec-top-text").text(`${activeSubsCount} Subscription${activeSubsCount > 1 ? 's' : ''} Active`);
                if (activeSubsCount === 1) {
                    $('#suo-item-ua-app-lic-plan .ac_name_id').html("Subscription is <b>Active</b>. Manage users");
                    //    $('.ualic_sub_status_tip').html(`<button onclick="$('#lic-sub-allowed-user-holder').show()">Configure users</button>`);
                }
                else {
                    $('#suo-item-ua-app-lic-plan .ac_name_id').html(`<b>${activeSubsCount} Active</b> Subscriptions. Manage users`);
                    // $('.ualic_sub_status_tip').html(`Active licenses : ${activeSubsCount} <button onclick="$('#lic-sub-allowed-user-holder').show()">Configure users</button>`);
                }
            }
        });
    } catch (exc) {
        console.log(exc);
        return false;
    }
    if (!planPurchaseUsersListFilled) {

    }
}

var impersonUserId = null;
var includeInActiveSubs = false;

// Checkout handler
async function ualic_proceedToPayment() {
    if (selectedCurrency !== "inr") {
        taxRates = ["txr_1IlvlbHo9p6j6FgRG8iwtor9"];
    }
    $(".ua-lic-process-loading").show();
    const docRef = await db
        .collection('customers')
        .doc(currentUser.uid)
        .collection('checkout_sessions')
        .add({
            price: selectedPaymentPlanId,
            allow_promotion_codes: true,
            tax_rates: taxRates,
            success_url: window.self !== window.top ? 'https://sms.ulgebra.com/store' : window.location.href,
            cancel_url: window.self !== window.top ? 'https://sms.ulgebra.com/store' : window.location.href,
            quantity: parseInt($("#quan-count").val()),
            trial_from_plan: enableTrialModesForPay,
            metadata: {
                tax_rate: taxRates.length > 0 ? '18% GST exclusive' : '',
                payment_made_from: window.location.href
            }
        });
    // Wait for the CheckoutSession to get attached by the extension
    docRef.onSnapshot((snap) => {
        const { error, sessionId } = snap.data();
        if (error) {
            // Show an error to your customer and then inspect your function logs.
            alert(`An error occured: ${error.message}`);
            document.querySelectorAll('button').forEach((b) => (b.disabled = false));
        }
        if (sessionId) {
            // We have a session, let's redirect to Checkout
            // Init Stripe
            if (window.self !== window.top) {
                var paymentURL = 'https://sms.ulgebra.com/redirect-to-checkout?stripe_session_id=' + sessionId;
                $('.ualic_manual_pay_redir_window').show();
                $('#ualic_manual_pay_redir_window_url').attr({ 'href': paymentURL });
                window.open(paymentURL, '_blank').focus();
            }
            else {
                const stripe = Stripe(STRIPE_PUBLISHABLE_KEY);
                stripe.redirectToCheckout({ sessionId });
            }
        }
    });
}

function showProductPlanBuyModal() {

}

// Billing portal handler
async function ualic_redirectToBillingPortal() {
    try {
        $(".processing").show();
        //document.querySelectorAll('button').forEach((b) => (b.disabled = true));

        // Call billing portal function
        const functionRef = firebase
            .app()
            .functions(functionLocation)
            .httpsCallable('ext-firestore-stripe-subscriptions-createPortalLink');
        const { data } = await functionRef({ returnUrl: window.location.origin });
        window.location.assign(data.url);
    }
    catch (exc) {
        console.log(exc);
        alert("Unable to retrive your subscriptions or you don't have any subscriptions.");
        $(".processing").hide();
    }
}

function ualic_selectPaymentPlan(planId) {
    selectedPaymentPlanId = planId;
    $('.pmt-userquantityholder').show()
}

function ualic_adjustQuantity(increase) {
    var currentVal = parseInt($('#quan-count').val());
    var nextVal = increase ? currentVal + 1 : currentVal - 1;
    if (nextVal < 1) {
        return;
    }
    $('#quan-count').val(nextVal);
    if (nextVal < 2) {
        $("#quan-decrease").attr({ 'disabled': true });
    }
    else {
        $("#quan-decrease").removeAttr("disabled");
    }
}

function ualic_changeCurrency() {
    selectedCurrency = $("#pmtp-currencySelector").val();
    $('.price-item').hide();
    $('.price-item.' + selectedCurrency).show();
}

function ualic_updateLICUsers(subscriptionId, priceId) {
    var updatableUserIds = [];
    var updateCallFunction = firebase.functions().httpsCallable('saveLICUsersList');
    $(`#license-item-${subscriptionId} .inp_lcpi-user`).each(function (index) {
        var idItem = $(this).val().trim();
        if (idItem !== "") {
            if (!updatableUserIds.includes(idItem)) {
                updatableUserIds.push(idItem);
            }
        }
    });
    $(`#license-item-${subscriptionId} .lcpi-action`).append('<b class="licpi-cs-status" style="color:royalblue"> Saving user details...</b>');
    updateCallFunction({
        "priceId": priceId,
        "subscriptionId": subscriptionId,
        "userIds": updatableUserIds
    }).then(res => {
        $(`#license-item-${subscriptionId} .licpi-cs-status`).css({ 'color': 'green' }).text("User details has been saved");
        setTimeout(function () {
            $(`#license-item-${subscriptionId} .licpi-cs-status`).remove();
            $('#lic-sub-allowed-user-holder').hide();
        }, 2000);
        console.log(res);
    }).catch(ex => {
        console.log(ex);
    });
}

if (window.location.href.endsWith("settings")) {
    var $zoho = $zoho || {}; $zoho.salesiq = $zoho.salesiq || { widgetcode: "60a8f0da5aa7a99f8a5753dda46416ff23048a3378d0734a7519b71fde7ff34c", values: {}, ready: function () { } }; var d = document; s = d.createElement("script"); s.type = "text/javascript"; s.id = "zsiqscript"; s.defer = true; s.src = "https://salesiq.zoho.com/widget"; t = d.getElementsByTagName("script")[0]; t.parentNode.insertBefore(s, t);
}

// (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
// new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
// j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
// 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
// })(window,document,'script','dataLayer','GTM-TS7N79N');

var licDetJSON = {};
var licPlanArray = [];
var licCurCredit = 0;
var licChosenSubId = null;
var licChosenPlanId = null;
let licSupportedPlans = null;
function fetchLICDetails(userId, callbackFunc) {
    var planVSsubmap = {};
    return fetch("https://us-central1-ulgebra-license.cloudfunctions.net/getUALicensedUserSubs?userId=" + userId)
        .then(function (response) {
            return response.json();
        })
        .then(function (myJson) {
            licDetJSON = myJson;
            if (licDetJSON && Object.keys(licDetJSON.subscriptions).length > 0) {
                for (var item in licDetJSON.subscriptions) {
                    if (licSupportedPlans.hasOwnProperty(licDetJSON.subscriptions[item].priceId)) {
                        licPlanArray.push(licDetJSON.subscriptions[item].priceId);
                        planVSsubmap[licDetJSON.subscriptions[item].priceId] = item;
                    }
                }
            }
            for (var item in licSupportedPlans) {
                if (licPlanArray.includes(item)) {
                    licChosenPlanId = item;
                    licChosenSubId = planVSsubmap[item];
                    break;
                }
            }
            if (licChosenSubId) {
                return true;
            }
            return false;
        })
        .catch(function (error) {
            console.log("Error: ", error);
        });
}

function serviceSupportsDirectInstallFromAzurna() {
    return extensionName.indexOf('forhubspot') > -1 || extensionName.indexOf('formonday') > -1;
}

function saveSettingInOrgCredDoc(property, value, processId = 0) {
    var obj = {};
    obj[property] = value;
    db.collection(extensionName.split("for")[1] + "_orgs").doc(appsConfig.UA_DESK_ORG_ID).collection(extensionName).doc(appsConfig.APP_UNIQUE_ID).set(obj,
        { merge: true }).then((res) => {
            processCompleted(processId);
        });
}

function showTopProgressBar(progressId) {
    $('body').append(`<div id="UApageTopProgressBar-${progressId}" class="UApageTopProgressBar"><div class="UApageTopProgressBarInner"></div></div>`);
}

function removeTopProgressBar(progressId) {
    $('#UApageTopProgressBar-' + progressId).remove();
}

function syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + (match === '"FILL_HERE"' ? '"<span class="_fill_here">FILL_HERE</span>"' : match) + '</span>';
    });
}
function emptyFunction() {

}
var UA_APP_UTILITY = {
    chosenMessgeTemplate: null,
    currentAttachedFile: null,
    fileUploadComplete: true,
    ORG_COMMON_SETTINGS: null,
    fetchedTemplates: {},
    fetchedSenders: {},
    availableRecipientFieldTypes: {},
    storedRecipientInventory: {},
    selectedRecipFieldTypes: new Set(),
    PERSONAL_WEBHOOK_TOKEN: null,
    lastFocusedInputElemForPlaceholderInsert: "inp-ssf-main-message",
    _feature_showUAMsgTemplates: true,
    currentSender: null,
    senderDropdownID: null,
    FETCHED_MESSAGES: {},
    CALL_AFTER_APP_CONFIG_RESOLVED: [],
    MESSAGING_CHANNELS: {
        "SMS": {
            "id": "SMS",
            "label": "SMS",
            "attachmentSupported": false
        },
        "MMS": {
            "id": "MMS",
            "label": "MMS",
            "attachmentSupported": true
        },
        "WHATSAPP": {
            "id": "WHATSAPP",
            "label": "WhatsApp",
            "attachmentSupported": true
        },
        "WHATSAPP_SANDBOX": {
            "id": "WHATSAPP_SANDBOX",
            "label": "WhatsApp Sandbox",
            "attachmentSupported": true
        },
        "MESSENGER": {
            "id": "MESSENGER",
            "label": "Messenger",
            "attachmentSupported": true
        },
        "FACEBOOK": {
            "id": "FACEBOOK",
            "label": "Messenger",
            "attachmentSupported": true
        },
        "FACEBOOK_MESSENGER": {
            "id": "FACEBOOK_MESSENGER",
            "label": "Messenger",
            "attachmentSupported": true
        },
        "VIBER": {
            "id": "VIBER",
            "label": "Viber",
            "attachmentSupported": true
        },
        "LINE": {
            "id": "LINE",
            "label": "LINE",
            "attachmentSupported": true
        },
        "INSTAGRAM": {
            "id": "INSTAGRAM",
            "label": "Instagram",
            "attachmentSupported": true
        },
        "WECHAT": {
            "id": "WECHAT",
            "label": "WeChat",
            "attachmentSupported": true
        },
        "TELEGRAM": {
            "id": "TELEGRAM",
            "label": "Telegram",
            "attachmentSupported": true
        }
    },
    initiateAPPUtility: function () {
        $("#inp-new-recip").on('keyup', function (e) {
            if (e.keyCode === 13) {
                UA_APP_UTILITY.addRecipientViaManualInput();
            }
        });
        $("#ua-msg-conv-search-number").on('keyup', function (e) {
            if (e.keyCode === 13) {
                UA_APP_UTILITY.performConversationSearchOnNumber();
            }
        });
        if (localStorage.getItem('ua_ui_config_msg_def_countrycode')) {
            $("#ssf-rec-def-countrycode input").val(localStorage.getItem('ua_ui_config_msg_def_countrycode'));
        }
        if (localStorage.getItem('ua_ui_config_msg_def_countrycode_criteria')) {
            $("#ssf-rec-def-countrycode-criteria select").val(localStorage.getItem('ua_ui_config_msg_def_countrycode_criteria'));
        }
    },
    renderSupportedViewsDDForNavigation: function () {
        if (extensionName.indexOf("twilio") !== 0 && extensionName.indexOf("messagebird") !== 0 && extensionName.indexOf("whatsappweb") !== 0 && extensionName.indexOf("whatcetra") !== 0 && (extensionName.indexOf("karixforzohocrm") !== 0) && (extensionName.indexOf("ringcentral") !== 0)) {
            return;
        }
        if (!isUASMSApp) {
            return;
        }
        if (extensionName.endsWith("forzohobooks")) {
            return;
        }
        if (extensionName.indexOf("twilio") !== 0 && extensionName.indexOf("messagebird") !== 0 && extensionName.indexOf("whatcetra") !== 0 && extensionName.indexOf("whatsappweb") !== 0 && extensionName.indexOf("ringcentral") !== 0) {
            delete UA_APP_UTILITY.appPrefinedUIViews.CONVERSATIONS;
        }
        let viewOptionsArray = [];
        Object.keys(UA_APP_UTILITY.appPrefinedUIViews).forEach(viewKey => {
            let optionItem = UA_APP_UTILITY.appPrefinedUIViews[viewKey];
            optionItem.value = viewKey;
            viewOptionsArray.push(optionItem);
        });
        UA_APP_UTILITY.renderSelectableDropdown("#nav_viewTypeSwitch", "Form View", viewOptionsArray, 'UA_APP_UTILITY.changeCurrentView');
    },
    appsConfigHasBeenResolved: function () {
        UA_ULGEBRA_CRM_UTILITY.initialize();
        UA_APP_UTILITY.showAppCursorInstallIfNotInstalled();
        UA_APP_UTILITY.getSettingFromOrgCommonCredDoc('');
        if (UA_APP_UTILITY._feature_showUAMsgTemplates) {
            UA_APP_UTILITY.renderSavedTemplatesInDropdowns();
        }
        UA_APP_UTILITY.renderSavedSendersInDropdowns();
        UA_APP_UTILITY.renderSupportedViewsDDForNavigation();
        let stopSupportedViewsForApps = [];
        if (!stopSupportedViewsForApps.includes(extensionName) && !extensionName.endsWith("forzohobooks")) {
            let lastOpenedView = null;//localStorage.getItem('ua_app_ui_last_view_'+extensionName);
            if (queryParams.get('force-ui-view')) {
                lastOpenedView = queryParams.get('force-ui-view').toUpperCase();
            }
            if (queryParams.get('force-ui-view-events')) {
                let lastOpenedEventView = queryParams.get('force-ui-view-events').toLowerCase();
                UA_APP_UTILITY.pageEventNavItemSelected(lastOpenedEventView);
            }
            if (lastOpenedView) {
                try {
                    if (lastOpenedView === "MESSAGE-FORM") {
                        lastOpenedView = "MESSAGE_FORM";
                    }
                    $('#nav_viewTypeSwitch .dropdownDisplayText').text(UA_APP_UTILITY.appPrefinedUIViews[lastOpenedView].label);
                    UA_APP_UTILITY.changeCurrentView(lastOpenedView);
                } catch (e) { console.log(e); }
            }
        }
        if (typeof UA_TPA_FEATURES.appsConfigHasBeenResolved === "function") {
            UA_TPA_FEATURES.appsConfigHasBeenResolved();
        }
        if (queryParams.get("actionType") === "fieldMapping") {
            UA_FIELD_MAPPPING_UTILITY.initialize();
        }
        try {
            if (UA_SAAS_SERVICE_APP.widgetContext.entityId && ((Array.isArray(UA_SAAS_SERVICE_APP.widgetContext.entityId) && UA_SAAS_SERVICE_APP.widgetContext.entityId.length === 1) || UA_SAAS_SERVICE_APP.widgetContext.entityId.split(',').length === 1)) {
                UA_APP_UTILITY.appCollisionPresenceHandler(UA_SAAS_SERVICE_APP.widgetContext.module + '_-_-_' + UA_SAAS_SERVICE_APP.widgetContext.entityId);
            }
        }
        catch (ex) { console.log(ex); }
        try {
            if (queryParams.get('openChatId')) {
                if (currentUser && currentUser.uid && extensionName && UA_SAAS_SERVICE_APP.CURRENT_ORG_INFO) {
                    UA_APP_UTILITY.openChatIdToChatBox(queryParams.get('openChatId'));
                }
            }
        }
        catch (ex) {
            console.log(ex);
        }
    },
    addRTNotificationNavigation: function () {
        $('#suo-item-notification-nav-insertafter').after(`<div id="suo-item-rt-notif-nav"   class="suo-item suo-item-ac-labels" onclick="UA_APP_UTILITY.redirectToRTNotificationConfigPage()">
                                        <span class="material-icons">notifications</span> Message Notifications <span class="anl_servicename"></span><div class="ac_name_id">Get desktop & in-app notifcations</span>
                                    </div>`);
    },
    redirectToRTNotificationConfigPage: function () {
        if (appsConfig.UA_DESK_ORG_ID && appsConfig.APP_UNIQUE_ID) {
            $("#suo-item-rt-notif-nav .ac_name_id").text('Click to enable');
            window.open(`https://sms.ulgebra.com/app-notification-settings?appCode=${extensionName}&oid=${appsConfig.UA_DESK_ORG_ID}&sec=${currentUser.uid}`, '_blank');
        }
        else {
            $("#suo-item-rt-notif-nav .ac_name_id").text('Try again in few seconds.');
        }
    },
    getTimeString: function (previous, timeForToday = false) {

        previous = new Date(previous);
        var msPerMinute = 60 * 1000;
        var msPerHour = msPerMinute * 60;
        var msPerDay = msPerHour * 24;
        var msPerMonth = msPerDay * 30;
        var msPerYear = msPerDay * 365;

        var elapsed = new Date() - previous;
        var onlyTime = new Date(previous).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }).toLowerCase();
        if (elapsed < msPerMinute) {
            if (timeForToday) {
                return onlyTime;
            }
            return Math.round(elapsed / 1000) + ' seconds ago';
        }
        else if (elapsed < msPerHour) {
            if (timeForToday) {
                return onlyTime;
            }
            return Math.round(elapsed / msPerMinute) + ' minutes ago';
        }
        else if (elapsed < msPerDay) {
            if (timeForToday && Math.round(elapsed / msPerHour) < 5) {
                return onlyTime;
            }
            return Math.round(elapsed / msPerHour) + ' hours ago';
        }
        else if (elapsed < msPerMonth) {
            return Math.round(elapsed / msPerDay) + ' days ago';
        }
        else if (elapsed < msPerYear) {
            return Math.round(elapsed / msPerMonth) + ' months ago';
        }
        else {
            return Math.round(elapsed / msPerYear) + ' years ago';
        }
    },
    openUploader: function (argument) {
        $('#chatbox-input-file').click();
    },
    showFullScreenPleaseWait: function () {
        $('.fullscreenPleaseWait').fadeIn(500);
    },
    hideFullScreenPleaseWait: function () {
        $('.fullscreenPleaseWait').fadeOut(250);
    },
    autoGrowTextArea: function (element) {
        if (element.id === "inp-ssf-main-message" && element.scrollHeight - 20 <= 60) {
            return false;
        }
        element.style.height = 'unset';
        element.style.height = (element.value.trim().length > 0 ? (element.scrollHeight + 25) : (element.scrollHeight - 25)) + "px";
        if (element.id === "inp-ssf-main-message") {
            UA_APP_UTILITY.reCalcluateChatBoxSize();
            if (element.value.length > 0) {
                UA_APP_UTILITY.toggleFieldMessageInvalidErrorMessage(false);
            }
        }
    },
    proceedToSearchOnEnter: function () {

    },
    ddActionMap: {
        "edit": {
            "icon": "edit",
            "label": "Edit"
        },
        "delete": {
            "icon": "delete_forever",
            "label": "Delete"
        },
        "select_by_default": {
            "icon": "push_pin",
            "label": "Select by default"
        },
        "hide": {
            "icon": "visibility_off",
            "label": "Hide Visibility"
        }
    },
    renderSelectableDropdown: function (targetElem, placeHolder, optionsArray, callback, showOtherField = false, noReplaceValueOnDisplaySlip = false, ddID = null, optionActions = []) {
        try {
            var ddElemId = ddID ? ddID : `dd-item-${new Date().getTime()}-${Math.round(Math.random(1000, 9899898) * 1000000)}`;
            let ddTargetIconMap = {
                "#ssf-recip-list-add-holdr": "filter_list"
            };
            if (ddTargetIconMap.hasOwnProperty(targetElem)) {
                placeHolder = `<i class="material-icons">${ddTargetIconMap[targetElem]}</i> ` + placeHolder;
            }
            if (!ddID) {
                $(targetElem).append(`<div id="${ddElemId}" data-noReplaceDisplaySlipValue="${noReplaceValueOnDisplaySlip.toString()}" class="dropdownOuter">
                                <div class="dropdownDisplaySlip roundedCorner" onclick="UA_APP_UTILITY.showDropDownItemsOnDisplaySlipClick('${ddElemId}')">
                                    <div class="dropdownDisplayText">
                                        ${placeHolder ? placeHolder : 'Select an option'}
                                    </div>
                                    <div class="dropdownDisplayArrow"></div>
                                </div>
                                <div class="dropdownInner focusCancellerOverlaySupported">
                                    <input oninput="UA_APP_UTILITY.filterDropdownItemsOnSearch(this, '${ddElemId}')" class="inpDropdownFilter" type="text" placeholder="Filter..." />
                                </div>
                                ${showOtherField ? `<input oninput="UA_APP_UTILITY.showFieldInBLSForLaterUse('${targetElem}', $(this).val())" onblur="UA_APP_UTILITY.callwindowCallback('${callback}', $(this).val())" class="dd-item-other-input" type="text" style=""><div class="dd-item-other-save" onclick="UA_APP_UTILITY.saveFieldInBLSForLaterUse('${targetElem}')">Save for Later</div>` : ''}
                            </div>`);
            }
            optionsArray.length > 10 ? $(`#${ddElemId} .inpDropdownFilter`).removeClass('hideThisInSpecific') : $(`#${ddElemId} .inpDropdownFilter`).addClass('hideThisInSpecific');
            if (showOtherField) {
                var existingVals = localStorage.getItem('fieldValAutoComplete_' + targetElem.replace('#', ''));
                existingVals = existingVals ? JSON.parse(existingVals) : [];
                existingVals.forEach(item => {
                    optionsArray.push({
                        'label': item,
                        'value': item ? item.trim() : item
                    });
                });
                optionsArray.push({
                    'label': 'Other',
                    'value': '_-_other_-_'
                });
            }
            optionsArray.forEach(item => {
                try {
                    if (typeof item.label === "number") {
                        item.label = item.label + "";
                    }
                    if (typeof item.value === "number") {
                        item.value = item.value + "";
                    }
                    if (!item.label) {
                        return;
                    }
                    let actionHTML = `<div class="ddopt-act-item-holder">`;
                    let dditemid = `dditemid-${new Date().getTime()}-${item.label.replace(/[\W_]+/g, "")}`;
                    if (item.value !== "_-_other_-_" && !item.isActionBtn) {
                        optionActions.forEach(actionItem => {
                            if (actionItem.ddActionMapType === UA_APP_UTILITY.ddActionMap.delete && !item.value.startsWith('ua_msg_')) {
                                return;
                            }
                            actionHTML += `<div title="${actionItem.ddActionMapType.label}" class="ddopt-act-item" onclick="UA_APP_UTILITY.ddOptionItemClicked(event, '${actionItem.callback}', '${item.value + "".replaceAll('\'', '\\')}', true)"><span class="material-icons">${actionItem.ddActionMapType.icon}</span></div>`;
                        });
                    }
                    actionHTML += "</div>";
                    $(`#${ddElemId} .dropdownInner`).append(`<div id="${dditemid}" title="${item.label.replace(/[\W_]+/g, " ") + " (" + item.value.replace(/[\W_]+/g, " ")})" data-ddlabel="${item.label.replace(/[\W_]+/g, " ")}" onclick="UA_APP_UTILITY.dropDownOptionSelected(event, '${ddElemId}', '${getSafeString(item.label).replaceAll('\'', '\\')}', '${getSafeString(item.value).replaceAll('\'', '\\')}', '${callback}', '${item.function}')" class="ddi-item ${item.isActionBtn ? 'isActionBtn' : ''} ${item.hidden ? ' isHiddenDDItem ' : ''}">${getSafeString(item.label)} ${item.selected ? '<span class="material-icons dditem-pin-def">push_pin</span>' : ''} ${actionHTML} </div>`);
                    if (item.selected) {
                        UA_APP_UTILITY.dropDownOptionSelected(event, ddElemId, getSafeString(item.label), getSafeString(item.value), callback, item.function);
                    }
                }
                catch (ex) {
                    console.log(ex);
                }
            });
            return ddElemId;
        } catch (ex) { console.log(ex); }
    },
    ddOptionItemClicked: function (event, callback, value) {
        UA_APP_UTILITY.callwindowCallback(callback, value);
    },
    getSafeString: function (rawStr) {
        if (typeof rawStr === "number") {
            rawStr = rawStr + "";
        }
        if (!rawStr || rawStr.trim() === "") {
            return "";
        }
        return $('<textarea/>').text(rawStr).html();
    },
    saveFieldInBLSForLaterUse: function (targetElem) {
        var fieldVal = $(targetElem + ' .dd-item-other-input').val();
        var existingVals = localStorage.getItem('fieldValAutoComplete_' + targetElem.replace('#', ''));
        existingVals = existingVals ? JSON.parse(existingVals) : [];
        if (existingVals.indexOf(fieldVal) > -1) {
            return;
        }
        existingVals.push(fieldVal);
        localStorage.setItem('fieldValAutoComplete_' + targetElem.replace('#', ''), JSON.stringify(existingVals));
        $(targetElem + ' .dd-item-other-save').hide();
    },

    showFieldInBLSForLaterUse: function (targetElem, fieldVal) {
        if (!fieldVal) {
            $(targetElem + ' .dd-item-other-save').hide();
            return;
        }
        var existingVals = localStorage.getItem('fieldValAutoComplete_' + targetElem.replace('#', ''));
        existingVals = existingVals ? JSON.parse(existingVals) : [];
        if (existingVals.indexOf(fieldVal) > -1) {
            $(targetElem + ' .dd-item-other-save').hide();
            return;
        }
        $(targetElem + ' .dd-item-other-save').css('display', 'inline-block');
    },

    filterDropdownItemsOnSearch: function (ev, ddElemId) {
        var val = ev.value.toLowerCase();
        if (val !== "") {
            document.querySelectorAll(`#${ddElemId} .ddi-item`).forEach(item => {
                if (item.getAttribute('data-ddlabel').toLowerCase().indexOf(val) < 0) {
                    item.style.display = 'none';
                } else {
                    item.style.display = 'block';
                }
            }
            );
        }
        else {
            $(`#${ddElemId} .ddi-item`).show();
        }
    },

    dropDownOptionSelected: function (event, ddElemId, label, value, callbackFunc, optionInvokeFunction) {
        if (event && (event.currentTarget && event.target !== event.currentTarget)) {
            return;
        }
        if ($(`#${ddElemId}`).attr('data-noReplaceDisplaySlipValue') === "false") {
            $(`#${ddElemId} .dropdownDisplayText`).text(label);
        }
        $(`#${ddElemId}`).attr('data-selectedval', value);
        UA_APP_UTILITY.removeFocusCancellerOverlay();
        if (value === "_-_other_-_") {
            $(`#${ddElemId} .dd-item-other-input`).show().focus();
            return;
        }
        else {
            $(`#${ddElemId} .dd-item-other-input`).hide();
        }
        if (optionInvokeFunction && optionInvokeFunction !== "undefined") {
            UA_APP_UTILITY.callwindowCallback(optionInvokeFunction);
        }
        else {
            UA_APP_UTILITY.callwindowCallback(callbackFunc, value);
        }

    },
    callwindowCallback: function (callbackFunc, value) {
        if (callbackFunc === "UA_TPA_FEATURES.insertTemplateContentInMessageInput") {
            UA_APP_UTILITY.autoGrowTextArea(document.getElementById('inp-ssf-main-message'));
            $('#ssf-fitem-template-var-holder').css('margin-bottom', '10px');
            $("#ssf-recip-chosen-template-holder").html(`Selected Template ID: <b>${value}</b>`).show();
        }
        if (callbackFunc.indexOf('.') > 0) {
            window[callbackFunc.split('.')[0]][callbackFunc.split('.')[1]](value);
        } else {
            window[callbackFunc](value);
        }
    },
    refreshSelectedRecipCountInUI: function () {
        let recipNumberCount = $('.ssf-recip-item.selected').length;
        if (recipNumberCount > 0) {
            UA_APP_UTILITY.toggleFieldRecipientInvalidErrorMessage(false);
        }
        $('#recip-count-holder').text('Selected ' + recipNumberCount + ' number' + (recipNumberCount > 1 ? 's' : ''));
        UA_APP_UTILITY.refreshMobileDuplicateCount();
    },
    showDropDownItemsOnDisplaySlipClick: function (ddElemId) {
        $(`#${ddElemId} .dropdownInner`).show();
        $(`#${ddElemId} .inpDropdownFilter`).focus();
        UA_APP_UTILITY.showFocusCancellerOverlay('#' + ddElemId);
    },
    showFocusCancellerOverlay: function (target) {
        $(target).append('<div class="focus-canceller-overlay" onclick="UA_APP_UTILITY.removeFocusCancellerOverlay()"></div>');
    },
    removeFocusCancellerOverlay: function () {
        $('.focusCancellerOverlaySupported').hide();
        $('.focus-canceller-overlay').remove();
    },
    emptyFunction: function () { },
    log: function (txt) {
        console.log(txt);
    },
    makeUAServiceAuthorizedHTTPCall: async function (url, method, data, headers) {
        var returnResponse = null;
        await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({ url: url, method: method, data: data, headers: headers, appCode: extensionName }).then((response) => {
            response = response.data;
            returnResponse = response;
        }).catch(err => {
            console.log(err);
        });
        return returnResponse;
    },
    saveSettingInOrgCommonCredDoc: async function (property, value, processId = 0) {
        try {
            UA_APP_UTILITY.ACTUAL_ORG_COMMON_SETTINGS[property] = value;
            if (typeof value === "object") {
                for (var item in value) {
                    if (value[item] === undefined) {
                        delete value[item];
                    }
                }
            }
            return await UA_APP_UTILITY.saveSettingInCredDoc(property, value, processId = 0, false);
        }
        catch (ex) {
            console.log('unable to add cache data', property, value, ex);
            return false;
        }
    },
    saveSettingInCredDoc: async function (property, value, processId = 0, isPersonal = true) {
        var obj = {};
        obj[property] = value;
        var docName = `uaAppPreferences/${extensionName.split("for")[1] + "_orgs"}/${appsConfig.UA_DESK_ORG_ID}/${extensionName}${isPersonal ? '/personal/' + currentUser.uid : ''}`;
        await db.doc(docName).set(obj,
            { merge: true }).then((res) => {
                processCompleted(processId);
            });
        if (isPersonal) {
            UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS[property] = value;
        }
        else {
            UA_APP_UTILITY.ORG_COMMON_SETTINGS[property] = value;
            UA_APP_UTILITY.ACTUAL_ORG_COMMON_SETTINGS[property] = value;
        }
        return true;
    },
    getSettingFromOrgCommonCredDoc: async function (property) {
        return await UA_APP_UTILITY.getSettingFromCredDoc(property, false);
    },
    deleteSettingFromCredDoc: async function (property) {
        if (UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.hasOwnProperty(property)) {
            await UA_APP_UTILITY.saveSettingInCredDoc(property, firebase.firestore.FieldValue.delete());
            return true;
        }
        else {
            if (UA_APP_UTILITY.ACTUAL_ORG_COMMON_SETTINGS && UA_APP_UTILITY.ACTUAL_ORG_COMMON_SETTINGS.hasOwnProperty(property)) {
                await UA_APP_UTILITY.saveSettingInOrgCommonCredDoc(property, firebase.firestore.FieldValue.delete());
                return true;
            }
        }
        return false;
    },
    getSettingFromCredDoc: async function (property, isPersonal = true) {
        if (!UA_APP_UTILITY.ORG_COMMON_SETTINGS) {
            var orgDoc = `uaAppPreferences/${extensionName.split("for")[1] + "_orgs"}/${appsConfig.UA_DESK_ORG_ID}/${extensionName}`;
            var myInfoCall = await db.doc(orgDoc).get().then(doc => {
                if (doc.data()) {
                    try {
                        if (UA_APP_UTILITY.ORG_COMMON_SETTINGS) {
                            UA_APP_UTILITY.ORG_COMMON_SETTINGS = { ...UA_APP_UTILITY.ORG_COMMON_SETTINGS, ...doc.data() };
                        }
                        else {
                            UA_APP_UTILITY.ORG_COMMON_SETTINGS = doc.data();
                        }
                    }
                    catch (ex) { console.log(ex); };
                    if (!UA_APP_UTILITY.ACTUAL_ORG_COMMON_SETTINGS) {
                        UA_APP_UTILITY.ACTUAL_ORG_COMMON_SETTINGS = doc.data();
                        UA_APP_UTILITY.actualOrgCommonSettingResolved();
                    }
                } else {
                    if (!UA_APP_UTILITY.ORG_COMMON_SETTINGS) {
                        UA_APP_UTILITY.ORG_COMMON_SETTINGS = {};
                    }
                    UA_APP_UTILITY.ACTUAL_ORG_COMMON_SETTINGS = {};
                    UA_APP_UTILITY.actualOrgCommonSettingResolved();
                }
                return true;
            }).catch(err => { console.log(err); return false; });

            personalDoc = `uaAppPreferences/${extensionName.split("for")[1] + "_orgs"}/${appsConfig.UA_DESK_ORG_ID}/${extensionName}/personal/${currentUser.uid}`;
            var myInfoCall = await db.doc(personalDoc).get().then(doc => {
                if (doc.data()) {
                    try {
                        if (UA_APP_UTILITY.ORG_COMMON_SETTINGS) {
                            UA_APP_UTILITY.ORG_COMMON_SETTINGS = { ...UA_APP_UTILITY.ORG_COMMON_SETTINGS, ...doc.data() };
                        }
                        else {
                            UA_APP_UTILITY.ORG_COMMON_SETTINGS = doc.data();
                        }
                    } catch (ex) { console.log(ex); };
                    if (!UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS) {
                        UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS = doc.data();
                        UA_APP_UTILITY.actualPersonalCommonSettingResolved();
                    }
                } else {
                    if (!UA_APP_UTILITY.ORG_COMMON_SETTINGS) {
                        UA_APP_UTILITY.ORG_COMMON_SETTINGS = {};
                    }
                    UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS = {};
                    UA_APP_UTILITY.actualPersonalCommonSettingResolved();
                }
                return true;
            }).catch(err => { console.log(err); return false; });
        }
        try {
            return UA_APP_UTILITY.ORG_COMMON_SETTINGS[property];
        }
        catch (Ex) {
            //            console.log(Ex);
            return null;
        }
    },
    showCreateNewSenderUI: function () {
        $("#addNewSenderWindow").show();
        $('#inp-ssf-new-sender-id').focus();
    },
    showCreateMsgTemplatesUI: function (title = "", content = "", templateName = "") {
        $("#addNewTemplateMessageWindow").show();
        $('#inp-ssf-template-title').val(title).focus();
        $('#inp-ssf-main-template-message').val(content);
        if (valueExists(content)) {
            $('#inp-ssf-main-template-message').focus();
        }
        try {
            if (["twilio", "messagebird"].includes(appCodeTPA)) {
                $("#messageAttachmentInputHolderForNewTemp").show();
                UA_APP_UTILITY.removeCurrentUploadedAttachment();
                if (templateName && UA_APP_UTILITY.fetchedTemplates[templateName] && UA_APP_UTILITY.fetchedTemplates[templateName].attachmentData && UA_APP_UTILITY.fetchedTemplates[templateName].attachmentData.isForNewTemp) {
                    UA_APP_UTILITY.currentAttachedFile = UA_APP_UTILITY.fetchedTemplates[templateName].attachmentData;
                    $('.cht-attachmentNameHolder.ForNewTemp').css({ 'display': 'inline-block' });
                    $('.cht-attachmentNameHolder.ForNewTemp .cht-attch-close').show();
                    $('.cht-attachmentNameHolder.ForNewTemp .cht-attch-prog').hide();
                    $('.cht-attachmentNameHolder.ForNewTemp .cht-attch-status').text(getSafeString(UA_APP_UTILITY.currentAttachedFile.fileMeta.name));
                }
            }
        }
        catch (er) { console.log(er) }
    },
    getUAOrgCommonSettings: function () {

    },
    getUASavedMsgTemplates: async function () {
        var templatesArray = [];
        await UA_APP_UTILITY.getSettingFromOrgCommonCredDoc('');
        for (var item in UA_APP_UTILITY.ORG_COMMON_SETTINGS) {
            if (item.indexOf('ua_msg_template_item_') === 0) {
                UA_APP_UTILITY.ORG_COMMON_SETTINGS[item]['id'] = item;
                templatesArray.push(UA_APP_UTILITY.ORG_COMMON_SETTINGS[item]);
            }
        }
        return templatesArray;
    },
    getUASavedSenders: async function () {
        var sendersArray = [];
        await UA_APP_UTILITY.getSettingFromOrgCommonCredDoc('');
        if (UA_APP_UTILITY.ORG_COMMON_SETTINGS) {
            for (var item in UA_APP_UTILITY.ORG_COMMON_SETTINGS) {
                if (item.indexOf('ua_msg_saved_senders_') === 0) {
                    UA_APP_UTILITY.ORG_COMMON_SETTINGS[item]['id'] = item;
                    sendersArray.push(UA_APP_UTILITY.ORG_COMMON_SETTINGS[item]);
                }
            }
        }
        return sendersArray;
    },
    hideUAMessageSender: async function (val, id) {
        val = val.replace(/[^0-9a-z]/gi, '');
        let isAlreadyHidden = localStorage.getItem('hideUAMessageSender_' + extensionName + '_' + val) === 'true';
        if (isAlreadyHidden) {
            localStorage.setItem('hideUAMessageSender_' + extensionName + '_' + val, 'false');
            $('#' + id).removeClass('isHiddenDDItem');
        }
        else {
            localStorage.setItem('hideUAMessageSender_' + extensionName + '_' + val, 'true');
            $('#' + id).addClass('isHiddenDDItem');
        }
    },
    deleteUASavedPreferenceItem: async function (val) {
        var processId = curId++;
        showTopProgressBar(processId);
        var res = await UA_APP_UTILITY.deleteSettingFromCredDoc(val);
        if (res) {
            if (val.startsWith('ua_msg_template')) {
                try {
                    let dataLabelName = val.replace('ua_msg_template_item_', '');
                    if (UA_APP_UTILITY.fetchedTemplates[val] && UA_APP_UTILITY.fetchedTemplates[val].attachmentData) {
                        let attachmentData = UA_APP_UTILITY.fetchedTemplates[val].attachmentData;
                        let storageRef = firebase.storage().ref('extensionAttachments/' + extensionName + '_attachments/' + currentUser.uid + '/' + attachmentData.fileType + '/' + attachmentData.uniqFileName);
                        await storageRef.delete().then((res) => {
                            console.log(res);
                        }).catch((err) => {
                            console.log(err);
                        });
                    }
                    if ($(".ddi-item[data-ddlabel='" + dataLabelName + "']").length > 0) {
                        $(".ddi-item[data-ddlabel='" + dataLabelName + "']").remove();
                        removeTopProgressBar(processId);
                        return false;
                    }
                }
                catch (e) { console.log(e); }
            }
            window.location.reload();
        }
    },
    choseDefaultUASavedPreferenceItem_Sender: function (val) {
        if (localStorage.getItem('choseDefaultUASavedPreferenceItem_Sender') === val) {
            localStorage.removeItem('choseDefaultUASavedPreferenceItem_Sender');
        }
        else {
            localStorage.setItem('choseDefaultUASavedPreferenceItem_Sender', val);
        }
        window.location.reload();
    },
    renderSavedSendersInDropdowns: async function () {
        try {
            var supportedChannels = [UA_APP_UTILITY.MESSAGING_CHANNELS.SMS];
            if (typeof UA_TPA_FEATURES.getSupportedChannels === "function") {
                supportedChannels = UA_TPA_FEATURES.getSupportedChannels();
            }
            supportedSendersDDOptions = [];
            supportedChannels.forEach(chanItem => {
                supportedSendersDDOptions.push({
                    'label': chanItem.label,
                    'value': chanItem.id
                });
            });
            UA_APP_UTILITY.renderSelectableDropdown('#ssf-fitem-new-channel-dd-holder', 'Select Sender Type', supportedSendersDDOptions, 'UA_APP_UTILITY.log');
        }
        catch (Ex) {
            console.log(Ex);
        }
        try {
            UA_APP_UTILITY.addAddNewActionSender();
            var sendersArray = await UA_APP_UTILITY.getUASavedSenders();
            sendersArray.forEach(templateItem => {
                UA_APP_UTILITY.addMessaegSender(templateItem.id, UA_APP_UTILITY.MESSAGING_CHANNELS[templateItem.channel], templateItem.label, templateItem);
                isSenderInitialized = true;
            });
            UA_APP_UTILITY.UA_SAVED_SENDERS_FETCH_COMPLETED();
            UA_APP_UTILITY.renderSelectableDropdown('#ssf-fitem-new-channel-dd-visibility-holder', 'All Users', [{
                'label': "All Users",
                'value': 'all_users'
            },
            {
                'label': 'Only Me',
                'value': 'only_me'
            }], 'UA_APP_UTILITY.log');

        }
        catch (ex) {
            console.log(ex);
        }
    },
    addAddNewActionSender: function () {
        if (!UA_APP_UTILITY.isSenderInitialized) {
            UA_APP_UTILITY.senderDropdownID = UA_APP_UTILITY.renderSelectableDropdown('#DD_HOLDER_SENDER_LIST', 'Choose Sender', [{
                'label': '+ Add New Sender',
                'value': '---',
                'action': 'invokeFunction',
                'function': 'UA_APP_UTILITY.showCreateNewSenderUI',
                'isActionBtn': true
            }], 'UA_APP_UTILITY.chooseMessageSender', false, false, UA_APP_UTILITY.senderDropdownID);
            UA_APP_UTILITY.isSenderInitialized = true;
        }
    },
    isSenderInitialized: false,
    chooseMessageSender: function (val, refreshConversationList = true) {
        UA_APP_UTILITY.currentSender = UA_APP_UTILITY.fetchedSenders[val];
        $("#primary-send-btn").html(`<span class="material-icons ssf-fitem-ttl-icon">send</span> Send ${getSafeString(UA_APP_UTILITY.currentSender.channel.label)}`);
        $("#messageAttachmentInputHolder").toggle(UA_APP_UTILITY.currentSender.channel.attachmentSupported);
        UA_APP_UTILITY.toggleFieldSenderInvalidErrorMessage(false);
        if (typeof UA_TPA_FEATURES.messageSenderSelected === "function") {
            if (UA_APP_UTILITY.isFormView()) {
                UA_TPA_FEATURES.messageSenderSelected(refreshConversationList);
                return false;
            }
            if (!UA_SAAS_SERVICE_APP.widgetContext.module || !UA_SAAS_SERVICE_APP.widgetContext.entityId || UA_APP_UTILITY.chatCurrentRecipient || !UA_APP_UTILITY.isConvView()) {
                UA_TPA_FEATURES.messageSenderSelected(refreshConversationList);
            }
            else {
                console.log("not rendering all convs for chooseMessageSender");
            }
        }
    },
    toggleFieldSenderInvalidErrorMessage: function (status, message) {
        UA_APP_UTILITY.toggleFieldErrorMessage('sms-form-item-item-sender', status);
    },
    smoothScrollToElem: function (elem) {
        $('html, body').animate({
            scrollTop: $(elem).offset().top - 70
        }, 0);
    },
    toggleFieldRecipientInvalidErrorMessage: function (status, message) {
        UA_APP_UTILITY.toggleFieldErrorMessage('sms-form-item-item-recipient', status);
    },
    toggleFieldMessageInvalidErrorMessage: function (status, message) {
        UA_APP_UTILITY.toggleFieldErrorMessage('sms-form-item-item-message', status);
    },
    toggleFieldErrorMessage: function (itemId, status) {
        if (status) {
            UA_APP_UTILITY.smoothScrollToElem('#' + itemId);
        }
        $("#" + itemId + ' .inp-item-errorText').hide().toggle(status);
    },
    refreshMessageBox: function () {
        $("#chatbox-message-holder").html("");
        if (typeof UA_TPA_FEATURES.fetchMessagesAndAddToChatBox === "function") {
            UA_TPA_FEATURES.fetchMessagesAndAddToChatBox();
        }
    },
    refreshConversationBox: function () {
        $("#chatbox-message-holder").html("");
        $("#uaCHATConvListHolder").html("");
        if (typeof UA_TPA_FEATURES.fetchConversationsAndAddToChatBox === "function") {
            UA_TPA_FEATURES.fetchConversationsAndAddToChatBox();
        }
    },
    addMessaegSender: function (id, channel, label, senderObj) {
        try {
            UA_APP_UTILITY.addAddNewActionSender();
            label = channel ? channel.label + " - " + label : label;
            if (!senderObj) {
                senderObj = {
                    'label': label,
                    'value': id
                };
            }
            if (!senderObj.hasOwnProperty('value')) {
                senderObj.value = id;
            }
            senderObj.channel = channel;
            UA_APP_UTILITY.fetchedSenders[id] = senderObj;
            let isHiddenItem = false;
            try {
                let plainId = id;
                if (typeof plainId === "number") {
                    plainId = plainId + "";
                }
                plainId = plainId.replace(/[^0-9a-z]/gi, '');
                isHiddenItem = localStorage.getItem('hideUAMessageSender_' + extensionName + '_' + plainId) === 'true';
            }
            catch (ex) {
                console.log(ex);
            }
            UA_APP_UTILITY.senderDropdownID = UA_APP_UTILITY.renderSelectableDropdown('#DD_HOLDER_SENDER_LIST', 'Choose Sender', [{
                'label': label,
                'value': id,
                'selected': (id === localStorage.getItem("choseDefaultUASavedPreferenceItem_Sender")),
                'hidden': isHiddenItem
            }], 'UA_APP_UTILITY.chooseMessageSender', false, false, UA_APP_UTILITY.senderDropdownID, [{
                'ddActionMapType': UA_APP_UTILITY.ddActionMap.select_by_default,
                'callback': 'UA_APP_UTILITY.choseDefaultUASavedPreferenceItem_Sender'
            },
            {
                'ddActionMapType': UA_APP_UTILITY.ddActionMap.hide,
                'callback': 'UA_APP_UTILITY.hideUAMessageSender'
            }, {
                'ddActionMapType': UA_APP_UTILITY.ddActionMap.delete,
                'callback': 'UA_APP_UTILITY.deleteUASavedPreferenceItem'
            }]);
        }
        catch (ex) {
            console.log(ex);
        }
    },
    isConvChatView() {
        return UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CONVERSATIONS || UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CHAT;
    },
    isChatView() {
        return UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CHAT;
    },
    isConvView() {
        return UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CONVERSATIONS;
    },
    isFormView() {
        return !UA_APP_UTILITY.currentMessagingView || UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.MESSAGE_FORM;
    },
    renderSavedTemplatesInDropdowns: async function (targets) {
        var templatesArray = await UA_APP_UTILITY.getUASavedMsgTemplates();
        var templateDropdownItems = [{
            'label': '+ Add New Template',
            'value': '---',
            'action': 'invokeFunction',
            'function': 'UA_APP_UTILITY.showCreateMsgTemplatesUI',
            'isActionBtn': true
        }];
        try {
            templatesArray.sort(function (a, b) {
                return a.title.toLowerCase() < (b.title.toLowerCase()) ? -1 : 1;
            });
        }
        catch (ex) {
            console.log(ex);
        }
        // templatesArray.forEach(templateItem=>{
        for (var i = 0; i < templatesArray.length; i++) {
            var templateItem = templatesArray[i];
            UA_APP_UTILITY.fetchedTemplates[templateItem.id] = templateItem;
            var templateItem_id = templateItem.id;
            if (extensionName.indexOf("lookupfor") > -1) {
                templateItem_id = templateItem.id + (targets ? ":::" + targets.split("@")[1] : "");
            }
            if (!templateItem.saasModule || templateItem.saasModule == UA_SAAS_SERVICE_APP.widgetContext.module) {
                templateDropdownItems.push({
                    'label': templateItem.title,
                    'value': templateItem_id
                });
            }
        }
        UA_APP_UTILITY.renderSelectableDropdown('#ssf-fitem-new-template-visibility-holder', 'All Users', [{
            'label': "All Users",
            'value': 'all_users'
        },
        {
            'label': 'Only Me',
            'value': 'only_me'
        }], 'UA_APP_UTILITY.log');

        let placedId = '#ssf-fitem-template-var-holder';
        let dropdownLabel = "Templates";
        if (extensionName.indexOf("lookupfor") > -1) {
            placedId = targets ? targets.split("@")[0] : ".lri-actions";
            dropdownLabel = "Template Messages";
        }
        UA_APP_UTILITY.renderSelectableDropdown(placedId, dropdownLabel, templateDropdownItems, 'UA_TPA_FEATURES.insertTemplateContentInMessageInput', false, false, null, [{
            'ddActionMapType': UA_APP_UTILITY.ddActionMap.edit,
            'callback': 'UA_APP_UTILITY.editUASavedTemplateItem'
        }, {
            'ddActionMapType': UA_APP_UTILITY.ddActionMap.delete,
            'callback': 'UA_APP_UTILITY.deleteUASavedPreferenceItem'
        }]);
    },
    editUASavedTemplateItem: async function (templateName) {
        if (queryParams.get("iframeId")) {
            templateName = templateName.split(":::")[0];
        }
        UA_APP_UTILITY.showCreateMsgTemplatesUI(UA_APP_UTILITY.fetchedTemplates[templateName].title, UA_APP_UTILITY.fetchedTemplates[templateName].message, templateName);
    },
    saveNewMsgTemplate: async function () {
        var templateName = $("#inp-ssf-template-title").val().trim();
        if (!valueExists(templateName)) {
            showErroMessage('Template name is required.');
            return;
        }
        var templateMessage = $("#inp-ssf-main-template-message").val();
        if (!valueExists(templateMessage) || !valueExists(templateMessage.trim())) {
            showErroMessage('Template message is empty.');
            return;
        }
        var templateVisiblityType = $("#ssf-fitem-new-template-visibility-holder .dropdownOuter").attr('data-selectedval');
        if (!templateVisiblityType) {
            templateVisiblityType = 'all_users';
        }
        templateName = templateName.trim().substring(0, 150).replace(/[^A-Za-z0-9]/g, '_');
        var templateObj = {
            title: templateName,
            message: templateMessage,
            visibility: templateVisiblityType,
            createdTime: new Date().getTime(),
            createdBy_UA: currentUser.uid,
            createdBy_SAAS: appsConfig.APP_UNIQUE_ID,
            saasModule: UA_SAAS_SERVICE_APP.widgetContext && UA_SAAS_SERVICE_APP.widgetContext.module ? UA_SAAS_SERVICE_APP.widgetContext.module : null
        };
        try {
            if (UA_APP_UTILITY.currentAttachedFile && UA_APP_UTILITY.currentAttachedFile.isForNewTemp) {
                templateObj.attachmentData = UA_APP_UTILITY.currentAttachedFile;
                let fileMeta = await UA_APP_UTILITY.stringify(templateObj.attachmentData.fileMeta);
                delete templateObj.attachmentData.fileMeta;
                templateObj.attachmentData.fileMeta = JSON.parse(fileMeta);
            }
            else {
                templateObj.attachmentData = null;
            }
        }
        catch (err) {
            console.log(err);
        }
        showTopProgressBar(curId++);
        $("#addNewTemplateMessageWindow .primary-form-btn").text('Please wait while saving template...');
        if (templateVisiblityType === "all_users") {
            await UA_APP_UTILITY.saveSettingInOrgCommonCredDoc('ua_msg_template_item_' + templateName, templateObj);
        }
        if (templateVisiblityType === "only_me") {
            await UA_APP_UTILITY.saveSettingInCredDoc('ua_msg_template_item_' + templateName, templateObj);
        }
        $("#addNewTemplateMessageWindow").fadeOut();
        window.location.reload();
    },
    stringify: function (obj) {
        const replacer = [];
        for (const key in obj) {
            replacer.push(key);
        }
        return JSON.stringify(obj, replacer);
    },
    saveNewSender: async function () {
        var senderChannel = $("#ssf-fitem-new-channel-dd-holder .dropdownOuter").attr('data-selectedval');
        if (!senderChannel) {
            showErroMessage('Select Channel Sender Type');
            return;
        }
        var senderID = $("#inp-ssf-new-sender-id").val().trim();
        if (!valueExists(senderID)) {
            showErroMessage('Sender ID is empty.');
            return;
        }
        var senderLabel = $("#inp-ssf-new-sender-name").val().trim();
        if (!valueExists(senderLabel)) {
            senderLabel = senderID;
        }
        var templateVisiblityType = $("#ssf-fitem-new-channel-dd-visibility-holder .dropdownOuter").attr('data-selectedval');
        if (!templateVisiblityType) {
            templateVisiblityType = 'all_users';
        }
        var senderAddedID = senderLabel.trim().substring(0, 150).replace(/[^A-Za-z0-9]/g, '_');
        var templateObj = {
            id: senderAddedID,
            channel: senderChannel,
            label: senderLabel,
            value: senderID,
            visibility: templateVisiblityType,
            createdTime: new Date().getTime(),
            createdBy_UA: currentUser.uid,
            createdBy_SAAS: appsConfig.APP_UNIQUE_ID
        };
        showTopProgressBar(curId++);
        $("#addNewSenderWindow .primary-form-btn").text('Please wait while saving Sender...');
        if (templateVisiblityType === "all_users") {
            await UA_APP_UTILITY.saveSettingInOrgCommonCredDoc('ua_msg_saved_senders_' + senderAddedID, templateObj);
        }
        if (templateVisiblityType === "only_me") {
            await UA_APP_UTILITY.saveSettingInCredDoc('ua_msg_saved_senders_' + senderAddedID, templateObj);
        }
        $("#addNewTemplateMessageWindow").fadeOut();
        window.location.reload();
    },
    addRecipientPhoneFieldType: function (propertyName, label) {
        if (UA_APP_UTILITY.availableRecipientFieldTypes.hasOwnProperty(propertyName)) {
            return;
        }
        UA_APP_UTILITY.availableRecipientFieldTypes[propertyName] = {
            label: label
        };
        propertyName = getSafeString(propertyName);
        $('#ssf-recip-phonetype-holder').append(`<div id="ssfrpt-item-${propertyName}" class="ssfrpt-item selected" data-recipFieldName="${propertyName}" onclick="UA_APP_UTILITY.filterRecipientPhonesByType('${propertyName}')">
                                ${getSafeString(label)}
                            </div>`);
    },
    filterRecipientPhonesByType: function (propertyName) {
        if (propertyName) {
            $(`#ssfrpt-item-${propertyName}`).toggleClass('selected');
        }
        $(`.ssf-recip-item`).addClass('unselected').removeClass('selected');
        $('.ssfrpt-item.selected').each((i, item) => {
            var propName = $(item).attr('data-recipFieldName');
            UA_APP_UTILITY.selectedRecipFieldTypes.add($(item).attr('data-recipFieldName'));
            $(`.ssf-recip-field-${propName}`).removeClass('unselected').addClass('selected');
        });
        $('.ssf-recip-field-manual').removeClass('unselected').addClass('selected');
        UA_APP_UTILITY.refreshSelectedRecipCountInUI();
    },
    selectPhoneType: function (category) {
        if (!$(`#ssfrpt-item-Phone`).hasClass('selected')) {
            this.filterRecipientPhonesByType(category);
        }
    },
    addInStoredRecipientInventory: function (contact) {
        UA_APP_UTILITY.storedRecipientInventory[contact.id] = contact;
        for (var item in UA_APP_UTILITY.availableRecipientFieldTypes) {
            if (!contact[item] || !valueExists(contact[item])) {
                continue;
            }
            UA_APP_UTILITY.addRecipientItemInUI(contact.name, contact[item], contact.id, item, false);
        }
        UA_APP_UTILITY.filterRecipientPhonesByType();
    },
    removeRecipientItemInUI: function (id) {
        $('#' + id).addClass('unselected').removeClass('selected');
        UA_APP_UTILITY.refreshSelectedRecipCountInUI();
    },
    removeAllRecipeientsinUI: function () {
        $('.ssf-recip-item').remove();
    },
    getFocusedInputMessageElement: function () {
        if ($('#' + UA_APP_UTILITY.lastFocusedInputElemForPlaceholderInsert).attr('readonly')) {
            return;
        }
        return '#' + UA_APP_UTILITY.lastFocusedInputElemForPlaceholderInsert;
        //        return $('#addNewTemplateMessageWindow').css('display') === "block" ? '#inp-ssf-main-template-message' : '#inp-ssf-main-message';
    },
    templateMergeFieldSelected: function (value) {
        UA_APP_UTILITY.insertMergeField(value.split('-')[0], value.split('-')[1]);
    },
    copyFieldsSelected: async function (value) {
        copyToClipBoard(" ");
        let fetchValue = value.split('-')[1] ? UA_CURRENT_ENTITY_OBJ[value.split('-')[0]] : `\${${value.split('-')[0]}}`;
        let fetchedValue = await UA_APP_UTILITY.getTemplateAppliedMessage(fetchValue, UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[Object.keys(UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS)[0]]);
        let isCopied = await copyToClipBoard(fetchedValue);
        if (isCopied) {
            UA_APP_UTILITY.tempAlert("Copied", 1000);
        }
    },
    tempAlert: function (msg, duration) {
        var el = document.createElement("div");
        el.setAttribute("style", "position: absolute;top: 10%;left: 50%;background-color: white;padding: 10px 20px;border: 1px solid #aaa;border-radius: 4px;");
        el.innerHTML = msg;
        setTimeout(function () {
            el.parentNode.removeChild(el);
        }, duration);
        document.body.appendChild(el);
    },
    insertMergeField: function (mergeField, module) {
        let mergeValue = module ? UA_CURRENT_ENTITY_OBJ[mergeField] : `\${${mergeField}}`;
        document.querySelector(UA_APP_UTILITY.getFocusedInputMessageElement()).setRangeText(mergeValue, $(UA_APP_UTILITY.getFocusedInputMessageElement()).prop("selectionStart"), $(UA_APP_UTILITY.getFocusedInputMessageElement()).prop("selectionEnd"), "end")
        $(UA_APP_UTILITY.getFocusedInputMessageElement()).focus();
    },
    resetMessageFormFields: function () {
        if ($(".ssf-temp-placehold-item-input").length > 0) {
            $(".ssf-temp-placehold-item-input").val("");
            $("#ssf-fitem-template-placeholder-holder").hide();
            $("#ssf-recip-chosen-template-holder").hide();
        }
        $("#inp-ssf-main-message").val("");
        $('#inp-ssf-main-message').removeAttr('readonly');
        $(".ssf-recip-item").removeClass('selected').addClass('unselected');
        $('.ssfrpt-item').removeClass('selected');
        $("#recip-count-holder").text('Selected 0 numbers');
        $("#ssf-recip-chosen-template-holder").hide();
        UA_APP_UTILITY.removeCurrentUploadedAttachment();
        UA_APP_UTILITY.chosenMessgeTemplate = null;
        if (UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CHAT || UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CONVERSATIONS) {
            UA_APP_UTILITY.autoGrowTextArea(document.getElementById('inp-ssf-main-message'));
        }
    },
    fetchedAllModuleRecords: function () {
        UA_APP_UTILITY.loadDefaultSingeNumberIfChatView();
        if (UA_APP_UTILITY.isConvView()) {
            if (UA_SAAS_SERVICE_APP.widgetContext.module || $('#DD_HOLDER_RECIPIENT_LIST .ddi-item').length === 0) {
                $('#sms-form-item-item-recipient').append(`<button onclick="UA_APP_UTILITY.loadAllConversationAsInbox()" class="convShowAllBtn">Show all</button>`);
            }
        }
    },
    loadAllConversationAsInbox: function () {
        UA_SAAS_SERVICE_APP.widgetContext.module = null;
        UA_APP_UTILITY.resetConversationParticipants();
        $("#nav_viewTypeSwitch").hide();
        UA_TPA_FEATURES.getConversationsList();
    },
    recipientDDID: null,
    addRecipientItemInUI: function (name, number, id, phoneFieldType, isManualAdd = false) {
        number = number.replace(/[^0-9]/gi, '');
        if (!valueExists(number)) {
            //console.log('Skipping number addition since empty value.');
            return false;
        }
        name = (name && name !== "undefined") ? name : "No Name";
        var fieldTypeClasses = " ssf-recip-field-" + phoneFieldType + " ";
        if (isManualAdd) {
            id = 'newmanual' + id;
            for (var property in UA_APP_UTILITY.availableRecipientFieldTypes) {
                fieldTypeClasses += ` ssf-recip-field-${property} `;
            }
            fieldTypeClasses += ` ssf-recip-field-manual selected `;
        }
        UA_APP_UTILITY.recipientDDID = UA_APP_UTILITY.renderSelectableDropdown('#DD_HOLDER_RECIPIENT_LIST', 'Select Recipient', [{
            'label': `${getSafeString(name)} - ${getSafeString(number)} ${isManualAdd ? '' : '(' + UA_APP_UTILITY.availableRecipientFieldTypes[phoneFieldType].label + ')'}`,
            'value': `${id}-_-${number}`
        }], 'UA_APP_UTILITY.chatRecipientSelected', false, false, UA_APP_UTILITY.recipientDDID);
        let nthDuplicate = 0;
        nthDuplicate = $(`.ssf-recip-item[data-recip-number="${number}"].selected`).length;
        $('.ssf-added-recip-holder').append(`<div class="ssf-recip-item ${fieldTypeClasses}" data-recip-id="${id}" data-recip-number="${number}" id="ssf-recipitem-${id}-${phoneFieldType}">
                                    <div class="ssf-recip-item-label">
                                       ${getSafeString(name)} - ${getSafeString(number)} <i>${isManualAdd ? '' : '(' + UA_APP_UTILITY.availableRecipientFieldTypes[phoneFieldType].label + ')'}</i> ${nthDuplicate > 0 ? '<div class="ssfrdup-text">Duplicate: ' + parseInt(nthDuplicate + 1) + '</div>' : ''}
                                    </div>
                                    <div class="ssf-recip-item-remove" onclick="UA_APP_UTILITY.removeRecipientItemInUI('ssf-recipitem-${id}-${phoneFieldType}')">
                                        x
                                    </div>
                                </div>`).show();
        $('#ssf-new-recip-form').hide();
    },
    refreshMobileDuplicateCount: function () {
        try {
            let numberVsElementCountMap = {};
            let printedNumberVsElementCountMap = {};
            $('.ssf-recip-item.selected').each(function (i, item) {
                let number = $(item).attr('data-recip-number');
                if (!numberVsElementCountMap.hasOwnProperty(number)) {
                    numberVsElementCountMap[number] = 0;
                    printedNumberVsElementCountMap[number] = 0;
                }
                numberVsElementCountMap[number]++;
            });
            $('.ssf-recip-item.selected').each(function (i, item) {
                let number = $(item).attr('data-recip-number');
                let itemId = $(item).attr('id');
                $('#' + itemId + ' .ssfrdup-text').remove();
                printedNumberVsElementCountMap[number]++;
                if (numberVsElementCountMap[number] > 1 && printedNumberVsElementCountMap[number] > 1) {
                    $(item).append(`<div class="ssfrdup-text">Duplicate: ${printedNumberVsElementCountMap[number]}</div>`);
                }
            });
        } catch (ex) {
            console.log(ex);
        }
    },
    chatCurrentRecipient: null,
    chatRecipientSelected: function (value) {
        UA_APP_UTILITY.chatCurrentRecipient = {
            'id': value.split('-_-')[0],
            'address': value.split('-_-')[1]
        };
        if (UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CHAT) {
            UA_APP_UTILITY.refreshMessageBox();
        }
        if (UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CONVERSATIONS) {
            UA_APP_UTILITY.refreshConversationBox();
        }
    },
    getCurrentMessageRecipients: function (provideAllInDropDown = false) {
        var recipNumbers = [];
        if (!UA_APP_UTILITY.currentMessagingView) {
            UA_APP_UTILITY.currentMessagingView = UA_APP_UTILITY.appPrefinedUIViews.MESSAGE_FORM;
        }
        if (provideAllInDropDown === false && (UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CHAT || UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CONVERSATIONS)) {
            recipNumbers.push({
                'id': UA_APP_UTILITY.chatCurrentRecipient.id,
                'number': UA_APP_UTILITY.chatCurrentRecipient.address
            });
        }
        var defCountryCode = $("#ssf-rec-def-countrycode input").val().trim().replace(/[^0-9]+/, '');
        var defCountryCodeCriteria = $("#ssf-rec-def-countrycode-criteria select").val();
        if (provideAllInDropDown || UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.MESSAGE_FORM || UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.SETTINGS) {
            $('.ssf-recip-item.selected').each((i, item) => {
                let phNumber = $(item).attr('data-recip-number').replace(/[^0-9]+/, '');
                if (valueExists(defCountryCode)) {
                    if (defCountryCodeCriteria !== "ignore") {
                        phNumber = phNumber.replace(/^0+/, '');
                        if (defCountryCodeCriteria === "always") {
                            phNumber = defCountryCode + phNumber;
                        }
                        if (defCountryCodeCriteria === "if_number_starts_zero") {
                            phNumber = phNumber.replace(/^0+/, defCountryCode);
                        }
                        if (defCountryCodeCriteria === "if_number_not_starts_country_code") {
                            if (phNumber.indexOf(defCountryCode) !== 0) {
                                phNumber = defCountryCode + phNumber;
                            }
                        }
                    }
                }
                phNumber = "+" + phNumber.replace(/^0+/, '');;
                recipNumbers.push({
                    'id': $(item).attr('data-recip-id'),
                    'number': phNumber
                });
            });
        }
        UA_SAAS_SERVICE_APP.recipNumbers = recipNumbers;
        return recipNumbers;
    },
    showAddNewRecipUI: function (appForm) {
        if ($('.ssf-recip-item').length === 0) {
            $('.ssf-added-recip-holder').hide();
        }
        else {
            $('.ssf-added-recip-holder').show();
        }

        if (appForm === "excel") {
            $('.ssf-new-recip-excel-form').slideToggle(100);
        } else {
            $('.ssf-new-recip-form').slideToggle(100);
            $('#inp-new-recip').focus();
        }

        if (localStorage.getItem('ssf-new-recip-countrycode-label')) {
            $('#ssf-new-recip-countrycode .dropdownDisplayText').text(localStorage.getItem('ssf-new-recip-countrycode-label'));
            $('#ssf-new-recip-countrycode .dropdownOuter').attr('data-selectedval', localStorage.getItem('ssf-new-recip-countrycode-val'));
        }

    },
    showAddNewRecipFromCSVUI: function () {
        this.showAddNewRecipUI('excel');
    },
    addRecipientViaManualInput: function () {
        var countryCode = null;
        var recipientNumber = $('#inp-new-recip').val().replace(/[^0-9]/g, '');
        if (recipientNumber.length < 5) {
            showErroWindow("Enter Valid Recipient Number", 'Please enter valid recipient number to proceed!');
            return;
        }
        if ($('#ssf-new-recip-countrycode .dropdownOuter').attr('data-selectedval')) {
            countryCode = $('#ssf-new-recip-countrycode .dropdownOuter').attr('data-selectedval');
            localStorage.setItem('ssf-new-recip-countrycode-label', $('#ssf-new-recip-countrycode .dropdownDisplayText').text());
            localStorage.setItem('ssf-new-recip-countrycode-val', countryCode);
            countryCode = countryCode.replace(/[^0-9]/g, '');
            if (recipientNumber.indexOf(countryCode) !== 0) {
                recipientNumber = countryCode + '' + recipientNumber;
            }
        }
        UA_APP_UTILITY.addRecipientItemInUI('No name', recipientNumber, new Date().getTime(), "mobile", true);
        $('#inp-new-recip').val('');
        UA_APP_UTILITY.refreshSelectedRecipCountInUI();
    },

    createdAllCredentials: 0,
    saveAPIKeyInCredentials: async function (serviceName, keyName, keyValue, callback) {
        if (keyValue === null || (typeof keyValue !== "object" && !valueExists(keyValue))) {
            showErroMessage("Please provide API Key");
            return;
        }
        UA_APP_UTILITY.createdAllCredentials = 0;
        curId = Math.round(Math.random(1000, 9999) * 10000);
        var credAdProcess = curId++;
        showProcess(`Adding credentials (1)...`, credAdProcess);
        var tokenObj = {};
        if (keyName === "__multiple_keys__") {
            tokenObj = keyValue;
        }
        else {
            tokenObj[keyName] = keyValue;
        }
        var mergeOption = {};
        if (keyName !== "authtoken") {
            mergeOption = { "merge": true };
        }
        if (keyName === "__multiple_keys__" && keyValue.hasOwnProperty('authtoken')) {
            mergeOption = {};
        }
        await db.collection("ulgebraUsers").doc(currentUser.uid).collection('extensions').doc(extensionName).collection("tokens").doc(serviceName).set(tokenObj, mergeOption).then(res => {
            UA_APP_UTILITY.createdAllCredentials++;
            processCompleted(credAdProcess);
            UA_APP_UTILITY.doSuccessAuthCallback(callback);
        });
        var credAdProcess2 = curId++;
        showProcess(`Adding credentials (2)...`, credAdProcess2);
        var updateObj = {};
        updateObj[serviceName === 'saas' ? 'saasAuth' : 'tpaAuth'] = true;
        db.collection("ulgebraUsers").doc(currentUser.uid).collection('extensions').doc(extensionName).set(updateObj, { "merge": true }).
            then((res) => {
                UA_APP_UTILITY.createdAllCredentials++;
                processCompleted(credAdProcess2);
                UA_APP_UTILITY.doSuccessAuthCallback(callback);
            });
        if (serviceName !== "saas") {
            var credAdProcess3 = curId++;
            showProcess(`Adding credentials (3)...`, credAdProcess3);
            tokenObj['uid'] = currentUser.uid;
            tokenObj['t'] = (new Date().getTime());
            db.collection(extensionName.split("for")[1] + "_orgs").doc(appsConfig.UA_DESK_ORG_ID + "").collection(extensionName).doc(appsConfig.APP_UNIQUE_ID + "").set(tokenObj).
                then((res) => {
                    UA_APP_UTILITY.createdAllCredentials++;
                    processCompleted(credAdProcess3);
                    UA_APP_UTILITY.doSuccessAuthCallback(callback);
                });
        } else {
            UA_APP_UTILITY.createdAllCredentials++;
            UA_APP_UTILITY.doSuccessAuthCallback(callback);
        }
    },

    doSuccessAuthCallback: function (callback) {
        if (UA_APP_UTILITY.createdAllCredentials === 3) {
            callback();
        }
    },

    showWorkFlowInstructionDialog: function () {
        hideUserNavSideBar();
        $('#calculated-form-wiparams-holder').hide();
        $("#workflowInstructionDialog").show();
        $(".hideoncalculatedwiparams").show();
    },
    openUploader: function () {
        document.getElementById("inputFileAttach").click();
    },
    fileSizeCheckWc: function (fileSize) {
        if (fileSize < 1000000) {
            fileSize = fileSize / 1024;
        } else {
            fileSize = fileSize / (1024 * 1024);
            if (fileSize > 5) {
                alert(`file size is ${fileSize.toFixed(2)}Mb.
                Please select file less than 5Mb.`);
                return false;
            }
        }
        return true;
    },
    removeCurrentUploadedAttachment: function (attachmentID = null) {
        UA_APP_UTILITY.currentAttachedFile = null;
        $('.cht-attachmentNameHolder').hide();
        UA_APP_UTILITY.reCalcluateChatBoxSize();
        if (attachmentID) {
            $('#cht-attach-' + attachmentID).remove();
            delete UA_APP_UTILITY.currentAttachedExtraFiles[attachmentID];
        }
    },
    attachFileChange: function (e, custom = "") {
        var file = e.files[0];
        if (!UA_APP_UTILITY.fileSizeCheckWc(file.size)) {
            return false;
        }
        var metadata = {
            customMetadata: {
                'file_name': file.name,
                'file_size': file.size
            }
        };
        let fileTyle;
        if (file.type.includes('image/')) {
            fileTyle = 'image';
        }
        else if (e.files[0].type.includes('audio/')) {
            fileTyle = 'audio';
        }
        else if (file.type.includes('video/')) {
            fileTyle = 'video';
        }
        else if (file.type.includes('application/')) {
            if (file.type.includes('pdf')) {
                fileTyle = 'application/pdf';
            } else if (file.type.includes('zip')) {
                fileTyle = 'application/zip';
            } else if (file.type.includes('excel')) {
                fileTyle = 'application/excel';
            } else {
                fileTyle = 'application/msdownload';
            }
        }
        else if (file.type.includes('text/')) {
            fileTyle = 'text';
        }
        UA_APP_UTILITY.fileUploadComplete = false;
        let uniqFileName = file.name.substring(0, file.name.lastIndexOf('.')) + '_' + Date.now() + file.name.substring(file.name.lastIndexOf('.'), file.name.length)
        var task = firebase.storage().ref('extensionAttachments/' + extensionName + '_attachments/' + currentUser.uid + '/' + fileTyle + '/' + uniqFileName).put(file, metadata);
        //        $('#attachedfile').html(`<div class="loadingdiv"> Uploading file ${getSafeString(file.name)}... </div>`);
        $('.cht-attachmentNameHolder' + `${custom ? "." + custom : ""}`).css({ 'display': 'inline-block' });
        $('.cht-attachmentNameHolder' + `${custom ? "." + custom : ""}` + ' .cht-attch-status').text('Uploading file ' + getSafeString(file.name) + '...');
        $('.cht-attachmentNameHolder' + `${custom ? "." + custom : ""}` + ' .cht-attch-close').hide();
        if (UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CHAT || UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CONVERSATIONS) {

            UA_APP_UTILITY.reCalcluateChatBoxSize();
        }
        task.on('state_changed',
            function progress(snapshot) {
                var progress = parseInt((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                //console.log('Upload is ' + progress + '% done');
                $('.cht-attachmentNameHolder' + `${custom ? "." + custom : ""}` + ' .cht-attch-prog').text(progress + '%').show();
                //                    $('#attachedfile').html(`<div class="loadingdiv"> ${progress}% Uploading file: ${getSafeString(file.name)}... </div>`);
                switch (snapshot.state) {
                    case firebase.storage.TaskState.PAUSED: // or 'paused'
                        //console.log('Upload is paused');
                        break;
                    case firebase.storage.TaskState.RUNNING: // or 'running'
                        //console.log('Upload is running');
                        break;
                }
            },
            function error(err) {
                console.log("File upload error. Try again", err);
                UA_APP_UTILITY.fileUploadComplete = true;
                $('.cht-attachmentNameHolder' + `${custom ? "." + custom : ""}` + ' .cht-attch-prog').hide();
                $('.cht-attachmentNameHolder' + `${custom ? "." + custom : ""}` + ' .cht-attch-status').text('Error uploading ' + getSafeString(file.name));
                //                    $('#attachedfile').html(`<div class="loadingdiv"> Error Uploading file: ${getSafeString(file.name)}... </div>`);
            },
            function complete() {
                document.getElementById("inputFileAttach" + `${custom ? custom : ""}`).value = "";
                let storageRef = firebase.storage().ref('extensionAttachments/' + extensionName + '_attachments/' + currentUser.uid + '/' + fileTyle + '/' + uniqFileName);
                storageRef.getDownloadURL().then(imgURL => {
                    UA_APP_UTILITY.currentAttachedFile = {
                        'randomFileRequestID': 'defaultfirstfile',
                        "mediaUrl": imgURL,
                        "fileMeta": file
                    };
                    if (custom && custom == "ForNewTemp") {
                        UA_APP_UTILITY.currentAttachedFile.fileType = fileTyle;
                        UA_APP_UTILITY.currentAttachedFile.uniqFileName = uniqFileName;
                        UA_APP_UTILITY.currentAttachedFile.isForNewTemp = true;
                    }
                    //                        $('#attachedfile').html(`${getSafeString(file.name)}`);
                    UA_APP_UTILITY.fileUploadComplete = true;
                    $('.cht-attachmentNameHolder' + `${custom ? "." + custom : ""}` + ' .cht-attch-close').show();
                    $('.cht-attachmentNameHolder' + `${custom ? "." + custom : ""}` + ' .cht-attch-prog').hide();
                    $('.cht-attachmentNameHolder' + `${custom ? "." + custom : ""}` + ' .cht-attch-status').text(getSafeString(file.name));
                });
            }
        );

    },
    removeAllExtraUploadedAttachment: function () {
        $('.cht-attachmentNameHolder').remove();
        UA_APP_UTILITY.currentAttachedExtraFiles = {};
    },
    currentAttachedExtraFiles: {},
    fetchAndUploadAndGetDownloadURLOfAttachment: async function (url, fileName, fileType, isSaasServiceType, displayFileAsPersistAttach = true, needtoSetInCurrentAttachfile = true, needtoSetInCurrentAttachedExtraFiles = true) {
        let randomFileRequestID = new Date().getTime() + '_' + parseInt(Math.random(100000, 99999) * 1000000);
        try {
            if (isSaasServiceType) {
                UA_APP_UTILITY.fileUploadComplete = false;
                if (displayFileAsPersistAttach) {
                    $(`#messageSSFItemAns`).prepend(`<div id="cht-attach-${randomFileRequestID}" class="cht-attachmentNameHolder" style="display: inline-block;">
                        <span class="cht-attch-status">
                            ...
                        </span>
                        <button class="cht-attch-prog">
                            10%
                        </button>
                        <button onclick="UA_APP_UTILITY.removeCurrentUploadedAttachment('${randomFileRequestID}')" class="cht-attch-close" style="display: block;">x</button>
                    </div>`);
                    $('#cht-attach-' + randomFileRequestID + ' .cht-attch-status').text('Uploading file ' + getSafeString(fileName) + '...');
                    $('#cht-attach-' + randomFileRequestID + ' .cht-attch-close').hide();
                }
                let apiResponse = await UA_OAUTH_PROCESSOR.getAPIResponse(url, 'GET', {}, { afterFetchAction: 'storeAndProvideDownloadURL', _uaFileURLRequestName: fileName });
                if (apiResponse.status === 200) {
                    UA_APP_UTILITY.fileUploadComplete = true;
                    let resolvedAttachObj = {
                        "mediaUrl": apiResponse.data._ua_download_url,
                        "randomFileRequestID": randomFileRequestID,
                        "fileMeta": {
                            'name': fileName,
                            'type': fileType ? fileType : apiResponse.data.fileType
                        }
                    };;
                    if (needtoSetInCurrentAttachfile) {
                        UA_APP_UTILITY.currentAttachedFile = resolvedAttachObj;
                    }
                    else if (needtoSetInCurrentAttachedExtraFiles) {
                        UA_APP_UTILITY.currentAttachedExtraFiles[randomFileRequestID] = resolvedAttachObj;
                    }
                    if (displayFileAsPersistAttach) {
                        $('#cht-attach-' + randomFileRequestID + ' .cht-attch-close').show();
                        $('#cht-attach-' + randomFileRequestID + ' .cht-attch-prog').hide();
                        $('#cht-attach-' + randomFileRequestID + ' .cht-attch-status').text(getSafeString(fileName));
                    }
                }
                else {
                    if (displayFileAsPersistAttach) {
                        $('#cht-attach-' + randomFileRequestID + ' .cht-attch-close').show();
                        $('#cht-attach-' + randomFileRequestID + ' .cht-attch-prog').hide();
                        $('#cht-attach-' + randomFileRequestID + ' .cht-attch-status').text('Error uploading ' + getSafeString(fileName));
                    }
                }
                UA_APP_UTILITY.fileUploadComplete = true;
            }
        }
        catch (error) {
            console.log(error);
            UA_APP_UTILITY.fileUploadComplete = true; return false;
        }
        return randomFileRequestID;
    },
    getPersonalWebhookAuthtoken: async function () {
        await firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({ appCode: extensionName, action: "GET_WEBHOOK_AUTH_TOKEN" }).then((response) => {
            // console.log('getPersonalWebhookAuthtoken response recd', response);
            var authtoken = response.data.authtoken;
            if (authtoken) {
                UA_APP_UTILITY.PERSONAL_WEBHOOK_TOKEN = authtoken;
                UA_TPA_FEATURES.renderWorkflowBodyCode(authtoken);
                $("#UA-WORKFLOW-AUTHTOKEN-DISPLAY").text(authtoken.saas);
                $('#wi-webhook-url textarea').val(`https://api.ulgebra.com/v1/workflows?extensionName=${extensionName}`);
            }
            return true;
        }).catch(err => {
            console.log(err);
            return false;
        });
    },
    reloadWindow: function () {
        window.location.reload();
    },
    getTemplateAppliedMessage: function (messageText, recipObjItem) {
        if (!recipObjItem) {
            //return messageText.trim();
        }
        var templateFieldsList = [];
        //        if(typeof UA_TPA_FEATURES.getMatchingMsgTemplateList === "function"){
        //            templateFieldsList = UA_TPA_FEATURES.getMatchingMsgTemplateList(messageText);
        //        }
        //        else{
        templateFieldsList = messageText.match(/\$\{[A-Za-z0-9._\-]+\}*/g);
        //        }
        if (!templateFieldsList || templateFieldsList.length === 0) {
            return messageText;
        }
        templateFieldsList.forEach(fieldListItem => {
            var fieldListItemLabel = fieldListItem.substr(2, fieldListItem.length - 3);
            let module = fieldListItemLabel.split('.')[0];
            let fieldName = fieldListItemLabel.split('.')[1];
            let fieldValue = fieldListItem;
            if (module === "CURRENT_USER") {
                fieldValue = UA_SAAS_SERVICE_APP.CURRENT_USER_INFO[fieldName];
            }
            else {
                if (!recipObjItem) {
                    try {
                        if (UA_SAAS_SERVICE_APP.widgetContext.entityId && UA_SAAS_SERVICE_APP.widgetContext.entityId.split(',').length === 1) {
                            recipObjItem = UA_SAAS_SERVICE_APP.FETCHED_RECORD_DETAILS[UA_SAAS_SERVICE_APP.widgetContext.entityId];
                        }
                        else {
                            recipObjItem = {};
                        }
                    }
                    catch (ex) {
                        console.log(ex);
                    }
                }
                fieldValue = recipObjItem[fieldName];
            }
            if (!fieldValue) {
                fieldValue = "";
            }
            messageText = messageText.replaceAll(fieldListItem, fieldValue);
        });
        return messageText.trim();
    },
    addWorkflowBodyCode: function (id, title, object) {
        if (extensionName.endsWith("salesporcecrm")) {
            object.extensionName = extensionName;
        }
        if (id === "__manual_calculated_configuration__") {
            $("#calculated-form-wiparams-holder").html('');
            $("#calculated-form-wiparams-holder").append(`<div class="wiparams-body-code-ttl">${title}</div><pre class="wiparams-pre-code" id="wiparams-body-code-${id}">` + syntaxHighlight(JSON.stringify(object, undefined, 4)) + `</pre><div class="wi-calculated-workflow-tip"> You may need to replace <b>module</b>, <b>recordId</b> and other params with <b>${saasServiceName} place holders</b> while configuring workflows</div>`);
        }
        else {
            if ($(`#wiparams-body-code-${id}`).length > 0) {
                return;
            }
            $("#wiparams-holder").append(`<div class="wiparams-body-code-ttl">${title}</div><pre class="wiparams-pre-code" id="wiparams-body-code-${id}">` + syntaxHighlight(JSON.stringify(object, undefined, 4)) + '</pre>');
        }
    },
    showClickWithinTimeAction: function () {
        $('#body').append(`<div id="countdown">
            <div id="countdown-number"></div>
            <svg>
              <circle r="18" cx="20" cy="20"></circle>
            </svg>
          </div>`);
    },
    getWebhookBaseURL: function () {
        return `https://api.ulgebra.com/v1/webhooks?a=${extensionName}&o=${appsConfig.UA_DESK_ORG_ID}`;
    },
    getAuthorizedWebhookURL: function () {
        return UA_APP_UTILITY.getWebhookBaseURL() + `&t=${UA_APP_UTILITY.PERSONAL_WEBHOOK_TOKEN.tpa}`;
    },
    getSAASWebhookBaseURL: function () {
        return `https://api.ulgebra.com/v1/webhooks?a=${extensionName}&o=${appsConfig.UA_DESK_ORG_ID}&ua_call_origin=saas`;
    },
    getAuthorizedSAASWebhookURL: function () {
        return UA_APP_UTILITY.getSAASWebhookBaseURL() + `&t=${UA_APP_UTILITY.PERSONAL_WEBHOOK_TOKEN.saas}`;
    },
    appPrefinedUIViews: {
        MESSAGE_FORM: {
            "className": "view-message-form-view",
            "label": "Form View"
        },
        CHAT: {
            "className": "view-chat-view",
            "label": "Chat View"
        },
        CONVERSATIONS: {
            "className": "view-conversation-view view-chat-view",
            "label": "Conversations View"
        },
        SETTINGS: {
            "className": "settingPageView",
            "label": "App Settings"
        }
    },
    currentMessagingView: null,
    changeCurrentView: function (viewType, sendEvents = true) {
        //        $('.nvts_item').removeClass('selected');
        //        $('.nvts_item.nav-view-'+viewType).addClass('selected');
        if (UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews[viewType]) {
            return;
        }
        for (var item in UA_APP_UTILITY.appPrefinedUIViews) {
            $('body').removeClass(UA_APP_UTILITY.appPrefinedUIViews[item].className);
        }
        UA_APP_UTILITY.currentMessagingView = UA_APP_UTILITY.appPrefinedUIViews[viewType];
        $('#nav_viewTypeSwitch .dropdownDisplayText').text(UA_APP_UTILITY.currentMessagingView.label);
        $('body').addClass(UA_APP_UTILITY.currentMessagingView.className);
        if (typeof UA_SAAS_SERVICE_APP.changedCurrentView === "function") {
            UA_SAAS_SERVICE_APP.changedCurrentView();
        }
        localStorage.setItem('ua_app_ui_last_view_' + extensionName, viewType);
        UA_APP_UTILITY.reCalcluateChatBoxSize();
        if (sendEvents) {
            UA_APP_UTILITY.loadDefaultSingeNumberIfChatView();
            UA_APP_UTILITY.CURRENT_CONVERSATION_ID = null;
            if (UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CONVERSATIONS) {
                if (!UA_SAAS_SERVICE_APP.widgetContext.module || !UA_SAAS_SERVICE_APP.widgetContext.entityId) {
                    UA_APP_UTILITY.initiateRTListeners();
                    if (typeof UA_TPA_FEATURES.getConversationsList === "function") {
                        $("#uaCHATConvListHolder").html('');
                        $("#chatbox-message-holder").html('');
                        UA_TPA_FEATURES.getConversationsList();
                    }
                }
                else {
                    console.log('not rendering all convs');
                }
            }
            if (UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CHAT) {
                UA_APP_UTILITY.refreshMessageBox();
            }
        }
    },
    TPA_SENDERS_FETCH_COMPLETE: false,
    UA_SAVED_SENDERS_FETCH_COMPLETE: false,
    TPA_SENDERS_FETCH_COMPLETED: function () {
        UA_APP_UTILITY.TPA_SENDERS_FETCH_COMPLETE = true;
        UA_APP_UTILITY.selectDefaultSingleSenderNumber();
    },
    UA_SAVED_SENDERS_FETCH_COMPLETED: function () {
        UA_APP_UTILITY.UA_SAVED_SENDERS_FETCH_COMPLETE = true;
        UA_APP_UTILITY.selectDefaultSingleSenderNumber();
    },
    selectDefaultSingleSenderNumber: function () {
        if (UA_APP_UTILITY.TPA_SENDERS_FETCH_COMPLETE && UA_APP_UTILITY.UA_SAVED_SENDERS_FETCH_COMPLETE) {
            if ($('#DD_HOLDER_SENDER_LIST .ddi-item').length === 2 && !UA_APP_UTILITY.currentSender) {
                $('#DD_HOLDER_SENDER_LIST .ddi-item:nth-of-type(2)').click();
            }
        }
    },
    loadDefaultSingeNumberIfChatView: function () {
        if (UA_APP_UTILITY.CURRENT_SUGGESTED_CONVERSATION_ID !== null) {
            return false;
        }
        if (!UA_SAAS_SERVICE_APP.widgetContext.module) {
            return false;
        }
        if (UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CHAT) {
            if ($('#DD_HOLDER_RECIPIENT_LIST .ddi-item').length === 1) {
                $('#DD_HOLDER_RECIPIENT_LIST .ddi-item:first-of-type').click();
            }
        }
        if (UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CONVERSATIONS) {
            if (typeof UA_TPA_FEATURES.fetchConversationsForAllCurrentRecipients === "function") {
                UA_TPA_FEATURES.fetchConversationsForAllCurrentRecipients();
            }
            else {
                if ($('#DD_HOLDER_RECIPIENT_LIST .ddi-item').length === 1) {
                    $('#DD_HOLDER_RECIPIENT_LIST .ddi-item:first-of-type').click();
                }
            }
        }
    },
    reCalcluateChatBoxSize: function () {
        if (UA_APP_UTILITY.currentMessagingView !== UA_APP_UTILITY.appPrefinedUIViews.CHAT && UA_APP_UTILITY.currentMessagingView !== UA_APP_UTILITY.appPrefinedUIViews.CONVERSATIONS) {
            return false;
        }
        let footerSize = $(".messageActionFooter").height() + 10;
        $('#chatbox-message-holder').css({ 'bottom': ($(".messageActionFooter").height() + 10) + 'px', 'max-height': 50 + innerHeight - (footerSize) + 'px' });
    },
    addMessageInBox: function (msgObj, addAtLast) {
        try {
            let convIDClean = UA_APP_UTILITY.getCleanStringForHTMLAttribute(msgObj.conversationID);
            if (UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CONVERSATIONS) {
                if ($('#CONV-ITEM-' + convIDClean).length > 0) {
                    if (parseInt(msgObj.createdTime) > $('#CONV-ITEM-' + convIDClean).attr('data-last-msg-time')) {
                        msgObj.time = msgObj.createdTime;
                        UA_APP_UTILITY.updateConversationItemIfExists(convIDClean, msgObj);
                    }
                }
            }
            if (UA_APP_UTILITY.CURRENT_CONVERSATION_ID !== convIDClean) {
                return;
            }
            UA_APP_UTILITY.FETCHED_MESSAGES[msgObj.id] = msgObj;
            var msgType = msgObj.type;
            var content = getSafeString(msgObj.text ? msgObj.text : "").replace(/(?:\r\n|\r|\n)/g, '<br>');
            var mediaHTML = "";
            var caption = "";
            if (msgObj.content === "'-_-_-_-'") {
                content = `<span class="spinnow material-icons" style="font-size: 20px;display:inline-block;">sync</span> Template Message Loading ...`;
            }
            if (msgObj.location) {
                content = "http://maps.google.com/?ll=" + msgObj.latitude + "," + msgObj.longitude;
            }
            if (msgObj.attachments) {
                msgObj.attachments.forEach(attachment => {
                    if (attachment.type === "image") {
                        mediaHTML += `<img src="${attachment.url}"/>`;
                    }
                    else if (attachment.type === "video") {
                        mediaHTML += `<video controls src="${attachment.url}"/>`;
                    }
                    else if (attachment.type === "audio") {
                        mediaHTML += `<audio controls src="${attachment.url}"/>`;
                    }
                    else if (attachment.type === "file") {
                        mediaHTML += `<div class="msg-attch"><span class="material-icons">attachment</span> <a target="_blank" href="${attachment.url}">Click to download file</a></div>`;
                    }
                    if (attachment.url === '-_-_-_-') {
                        mediaHTML += '<div class="medPlaceHoldr"><span class="spinnow material-icons" style="font-size: 20px;display:inline-block;">sync</span> Loading attachment ...</div>';
                    }
                });
            }
            var inComing = msgObj.isIncoming;
            var personHTML = "";
            if (msgObj.author) {
                let sourceObj = msgObj.author;
                personHTML = `<div class="chat-message-author" title="${sourceObj.email}"><b>${sourceObj.name} </b><img class="cma-ppic" src="${sourceObj.pic}"> </div>`;
            }
            var walimittext = `<a href="https://apps.ulgebra.com/whatsapp-business-api-integration/limitations#h.ytd5bu2d0930" target="_blank">See Why?</a>`;
            var msgInnerHTML = `<div class="chatmessage-inner">
                                                                <div class="chatmessage-content" onclick="showMessageTheater('${msgObj.id}')">
                                                                   ${personHTML} <div class="msgmedia-holdr">${mediaHTML}</div> ${caption} <span class="chmsgcontenttext"> ${content} </span>
                                                                </div>
                                                                <div class="chatmessage-time">
                                                                    <span class="fullTime"> from <b> ${msgObj.from} </b> <b> to ${msgObj.to} </b> via <b class="c-green">${msgObj.channel.label}</b></span>
                                                                    <span class="smallTime" title="${new Date(msgObj.createdTime).toLocaleString()}">${UA_APP_UTILITY.getTimeString(msgObj.createdTime, true)}</span>
                                                                    ${inComing ? '' : '<span class="msgStatus">' + (UA_APP_UTILITY.getMessageStatusHTML(msgObj.status)) + `${msgObj.error ? ' <i class="c-crimson">' + msgObj.error.message + '</i>' : ''} </span>`}
                                                                </div>
                                                            </div>`;
            var msgHTML = (`<div id="msgitem-${msgObj.id}" ${msgObj.isUnRead ? ` onclick="UA_APP_UTILITY.removeMsgUnReadStyle('${msgObj.id}')" ` : ''} class="msgsts-${msgObj.status} chatmessage-item ${inComing ? 'direction-in' : 'direction-out'} ${msgObj.isUnRead ? 'unread' : ''}">
                                                            ${msgInnerHTML}
                                                        </div>`);

            if ($(`#msgitem-${msgObj.id}`).length > 0) {
                $(`#msgitem-${msgObj.id}`).html(msgInnerHTML);
                //console.log('exiting msg '+msgObj.id+ ' is updated.');
            }
            else {
                if (addAtLast) {
                    $('.chatmessages-inner').append(msgHTML);
                }
                else {
                    $('.chatmessages-inner').prepend(msgHTML);
                }
                if (msgObj.isUnRead) {
                    $("#MID-LOADING-" + msgObj.id).remove();
                }
                if (msgObj.isUnRead || addAtLast) {
                    $(".chatmessages-inner").scrollTop($(".chatmessages-inner")[0].scrollHeight);
                }
            }
            setTimeout(msgObj => { UA_APP_UTILITY.checkAndShowIfConvWindowForWhatsApp() }, 1000);
        }
        catch (exc) {
            console.log(exc);
        }
    },
    checkAndShowIfConvWindowForWhatsApp: function () {
        try {
            if (UA_APP_UTILITY.currentSender.channel.label.toLowerCase().startsWith("whatsapp")) {
                let msgId = $('.chatmessage-item.direction-in')[$('.chatmessage-item.direction-in').length - 1].getAttribute('id').replace('msgitem-', '')
                let msgCreatedTime = new Date(UA_APP_UTILITY.FETCHED_MESSAGES[msgId].createdTime).getTime();
                let windowExpiryTime = msgCreatedTime + (1 * 24 * 60 * 60 * 1000);
                let todayTime = new Date().getTime();
                if (todayTime >= windowExpiryTime) {
                    console.log("Only you can send template messages to this contact.");
                    UA_APP_UTILITY.setOnlyCanSendTempMsgsHandler(true);
                }
                else {
                    UA_APP_UTILITY.setOnlyCanSendTempMsgsHandler(false);
                }
            }
        }
        catch (ex) {
            console.log(ex);
        }
    },
    scrollChatBoxToLatest: function () {
        $(".chatmessages-inner").scrollTop($(".chatmessages-inner")[0].scrollHeight);
    },
    closeMessageTheater: function () {
        $(".msgTheater").removeClass('msgTheater');
        $(".chatbox-config").slideDown(100);
        $(".chataction").slideDown(100);
        $('.theaterClose').hide();
    },

    showMessageTheater: function (msgId) {
        $(".theaterClose").show();
        $(".chatbox-config").slideUp(100);
        $(".chataction").slideUp(100);
        $("#msgitem-" + msgId).addClass('msgTheater');
    },

    removeMsgUnReadStyle: function (msgId) {
        $('#msgitem-' + msgId).removeClass('unread');
    },

    getMessageStatusHTML: function (status) {
        switch (status) {
            case "failed":
                return `<span class="material-icons">error_outline</span>`;
                break;
            case "rejected":
                return `<span class="material-icons">block</span>`;
                break;
            case "pending":
                return `<span class="material-icons">flight_takeoff</span>`;
                break;

            case "sent":
                return `<span class="material-icons">done</span>`;
                break;

            case "delivered":
                return `<span class="material-icons">done_all</span>`;
                break;

            case "read":
                return `<span class="material-icons">done_all</span>`;
                break;
            case "received":
                return `<span class="material-icons">call_received</span>`;
                break;

            default:
                return status;
                break;
        }
    },
    getSafeStringForRTDBPath: function (path) {
        return path.replace(/[^a-zA-Z0-9\-_]/g, '-');
    },
    RTCs: {},
    convDataRTDBListener: null,
    msgDataRTDBListener: null,
    initiateRTListeners: function (CID) {
        try {
            if (UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CONVERSATIONS || (UA_APP_UTILITY.currentMessagingView.value == "CHAT" && extensionName == "karixforzohocrm")) {
                if (UA_APP_UTILITY.msgDataRTDBListener) {
                    UA_APP_UTILITY.msgDataRTDBListener.off();
                }
                if (UA_APP_UTILITY.convDataRTDBListener) {
                    return false;
                }
                UA_APP_UTILITY.convDataRTDBListener = firebase.database().ref(`/conversationMessageAlerts/${extensionName}/${appsConfig.UA_DESK_ORG_ID.replace(/[^a-zA-Z0-9\-_]/g, '-')}/`);
                UA_APP_UTILITY.convDataRTDBListener.on('child_changed', function (snapshot) {
                    //console.log('new conv event '+snapshot.key);
                    UA_APP_UTILITY.newRTEvent(snapshot.key, snapshot.val());
                });
                return false;
            }
            CID = UA_APP_UTILITY.getCleanStringForHTMLAttribute(CID);
            if (UA_APP_UTILITY.RTCs.hasOwnProperty(CID)) {
                //console.log("RTC already inited "+UA_APP_UTILITY.RTCs[CID]);
                return;
            }
            UA_APP_UTILITY.RTCs[CID] = 0;
            //console.log('NEW_RT_INiTIATED', CID);
            if (UA_APP_UTILITY.msgDataRTDBListener) {
                UA_APP_UTILITY.msgDataRTDBListener.off();
            }
            if (UA_APP_UTILITY.convDataRTDBListener) {
                UA_APP_UTILITY.convDataRTDBListener.off();
            }
            UA_APP_UTILITY.msgDataRTDBListener = firebase.database().ref(`/conversationMessageAlerts/${extensionName}/${appsConfig.UA_DESK_ORG_ID.replace(/[^a-zA-Z0-9\-_]/g, '-')}/${CID.replace(/[^a-zA-Z0-9\-_]/g, '-')}`);
            UA_APP_UTILITY.msgDataRTDBListener.on('value', function (snapshot) {
                UA_APP_UTILITY.RTCs[CID]++;
                if (UA_APP_UTILITY.RTCs[CID] === 1) {
                    return;
                }
                UA_APP_UTILITY.newRTEvent(CID, snapshot.val());
            });
        }
        catch (exc) {
            console.log(exc);
        }
    },

    newRTEvent: function (CID, MID) {
        //console.log('NEW_RT_RECIEVED', CID, MID);
        if (CID) {
            CID = UA_APP_UTILITY.getCleanStringForHTMLAttribute(CID);
        }
        if (MID.c === "msg-status") {
            let msgItemID = `#msgitem-${MID.a}`;
            MID.b = MID.b.toLowerCase();
            if ($(msgItemID).length > 0) {
                $(msgItemID).attr('class').split(/\s+/).forEach(item => {
                    if (item.indexOf("msgsts") === 0) {
                        $(msgItemID).removeClass(item);
                    }
                });
                $(msgItemID).addClass('msgsts-' + MID.b);
                $(msgItemID + ` .msgStatus`).html(UA_APP_UTILITY.getMessageStatusHTML(MID.b));
                $(msgItemID + ` .msgStatus .material-icons`).addClass('zoomEffect');
                $(msgItemID).css({ 'background-color': 'aliceblue' });
                setTimeout(() => { $(msgItemID + ` .msgStatus .material-icons`).removeClass('zoomEffect'); $(msgItemID).css({ 'background-color': 'unset' }); }, 1000);
            }
            let convItemID = `#CONV-ITEM-` + UA_APP_UTILITY.getCleanStringForHTMLAttribute(CID);
            if ($(convItemID).length > 0) {
                let existingClass = $(`${convItemID} .conv-msg-sts`).attr('class');
                $(`${convItemID} .conv-msg-sts`).removeClass(existingClass.split(' ')[1]).addClass(MID.b);
                $(convItemID + ` .conv-msg-sts`).html(UA_APP_UTILITY.getMessageStatusHTML(MID.b));
                $(convItemID + ` .conv-msg-sts .material-icons`).addClass('zoomEffect');
                setTimeout(() => { $(convItemID + ` .conv-msg-sts .material-icons`).removeClass('zoomEffect'); }, 1000);
            }
        }
        if (UA_APP_UTILITY.currentMessagingView === UA_APP_UTILITY.appPrefinedUIViews.CONVERSATIONS) {
            var doUpdateForthismessage = UA_SAAS_SERVICE_APP.widgetContext.module && UA_APP_UTILITY.LOCAL_FETCHED_CONVERSATION_IDS.includes(CID);
            if (!doUpdateForthismessage) {
                doUpdateForthismessage = !UA_SAAS_SERVICE_APP.widgetContext.module || CID === UA_APP_UTILITY.CURRENT_CONVERSATION_ID || $(`#CONV-ITEM-${UA_APP_UTILITY.getCleanStringForHTMLAttribute(CID)}`).length > 0;
            }
            if (doUpdateForthismessage) {
                if (typeof UA_TPA_FEATURES.addNewConversationByIDInConversationBox === "function") {
                    UA_TPA_FEATURES.addNewConversationByIDInConversationBox(CID, MID.a);
                }
            }
            return true;
        }
        if (typeof UA_TPA_FEATURES.newRTEventReceived === "function") {
            UA_TPA_FEATURES.newRTEventReceived(CID, MID);
        }
    },
    mainMessageKeyInputReceived: function (event) {
        if (event.keyCode === 13 && !event.shiftKey) {
            event.preventDefault();
            if (UA_APP_UTILITY.isConvChatView()) {
                if ($("#inp-ssf-main-message").val().trim().length > 0) {
                    $("#inp-ssf-main-message").blur();
                    $("#primary-send-btn").click();
                }
            }
        }
    },
    lookUpTermKeyInputReceived: function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            UA_TPA_FEATURES.lookUpSearchValueUpdated();
        }
    },
    eventHistorySearchContactInput: function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            UA_TPA_FEATURES.loadSeduledEvents();
        }
    },
    showReAuthorizeERRORWithClose: function () {
        hideUserNavSideBar();
        UA_TPA_FEATURES.showReAuthorizeERRORWithClose();
        UA_LIC_UTILITY.showExistingInvitedAdminDDHTML();
    },
    saveTPAIncomingSAASTargetModule: function () {
        UA_APP_UTILITY.saveSettingInCredDoc('ua_incoming_saas_target_module', $('#in-msg-saas-entity-module-select').val());
    },
    actualOrgCommonSettingResolvedEventFired: false,
    CALL_AFTER_ORG_COMMON_SETTING_RESOLVED: [],
    actualOrgCommonSettingResolved: function () {
        if (!UA_APP_UTILITY.actualOrgCommonSettingResolvedEventFired) {
            UA_APP_UTILITY.actualOrgCommonSettingResolvedEventFired = true;
            if (typeof UA_TPA_FEATURES.actualOrgCommonSettingResolved === "function") {
                UA_TPA_FEATURES.actualOrgCommonSettingResolved();
            }
            UA_LIC_UTILITY.addFetchedUsersToOrgCache();
            UA_APP_UTILITY.CALL_AFTER_ORG_COMMON_SETTING_RESOLVED.forEach(item => {
                item();
            })
        }
    },
    actualPersonalCommonSettingResolvedEventFired: false,
    CALL_AFTER_PERSONAL_COMMON_SETTING_RESOLVED: [],
    actualPersonalCommonSettingResolved: function () {
        if (!UA_APP_UTILITY.actualPersonalCommonSettingResolvedEventFired) {
            UA_APP_UTILITY.actualPersonalCommonSettingResolvedEventFired = true;
            if (typeof UA_TPA_FEATURES.actualPersonalCommonSettingResolved === "function") {
                UA_TPA_FEATURES.actualPersonalCommonSettingResolved();
            }
            UA_APP_UTILITY.CALL_AFTER_PERSONAL_COMMON_SETTING_RESOLVED.forEach(item => {
                item();
            })
        }
    },
    performConversationSearchOnNumber: function () {
        if (typeof UA_TPA_FEATURES.performConversationSearchOnNumber === "function") {
            UA_TPA_FEATURES.performConversationSearchOnNumber($('#ua-msg-conv-search-number').val().trim());
        }
    },
    resetConversationParticipants: function () {
        UA_APP_UTILITY.currentSender = null;
        UA_APP_UTILITY.chatCurrentRecipient = null;
        $('#DD_HOLDER_SENDER_LIST .dropdownDisplayText').text('Select a Sender');
        $('#DD_HOLDER_RECIPIENT_LIST .dropdownDisplayText').text('Select a Recipient');
        $('#ua-msg-conv-search-number').val('');
    },
    TIME_ENTRY_TICKET_START_TIME: new Date().getTime(),
    selectConversationToLoad: function (convID, event = null) {
        if (event && event.target && event.target.classList.toString().includes("ucli-saas-view-all-btn")) return;
        let convIDClean = UA_APP_UTILITY.getCleanStringForHTMLAttribute(convID);
        UA_APP_UTILITY.CURRENT_CONVERSATION_ID = convIDClean;
        $(`#CONV-ITEM-${convIDClean} .ucli-unread`).text('0').fadeOut();
        UA_TPA_FEATURES.loadConversationInChatBox(convID, false);
        // if(UA_APP_UTILITY.SUGGESTED_SAAS_TPA_COV_MAP.hasOwnProperty(convID)){
        //     $("#DD_HOLDER_SENDER_LIST .dropdownDisplayText").text(UA_APP_UTILITY.currentSender.label);
        //     $("#DD_HOLDER_RECIPIENT_LIST .dropdownDisplayText").text(UA_APP_UTILITY.chatCurrentRecipient.label);
        // }
    },
    CURRENT_CONVERSATION_ID: null,
    LOCAL_FETCHED_CONVERSATION_IDS: [],
    getCleanStringForHTMLAttribute: function (str) {
        if (!str) {
            str = '';
        }
        return str.replace(/[^a-zA-Z0-9\-_]/g, '-');
    },
    getCleanStringEncodedHTMLAttribute: function (str) {
        if (!str) {
            str = '';
        }
        return str.replace(/[\u00A0-\u9999<>\&]/g, function (i) {
            return '&#' + i.charCodeAt(0) + ';';
        });
    },
    updateConversationItemIfExists: function (convID, convObj) {
        let convIDClean = UA_APP_UTILITY.getCleanStringForHTMLAttribute(convID);
        if ($(` #CONV-ITEM-${convIDClean}`).length > 0) {
            if (parseInt(convObj.time) > $('#CONV-ITEM-' + convIDClean).attr('data-last-msg-time')) {
                $(`#CONV-ITEM-${convIDClean} .ucli-msg`).html(`${convObj.channel ? convObj.channel.label : "SMS"} : <span class="conv-msg-sts ${convObj.status}">${UA_APP_UTILITY.getMessageStatusHTML(convObj.status)}</span> <span class="conv-msg-text">${getSafeString(convObj.message)}</span>`);
                $(`#CONV-ITEM-${convIDClean} .ucli-time`).html(getTimeString(convObj.time, true));
                $(`#CONV-ITEM-${convIDClean}`).attr('data-last-msg-time', parseInt(convObj.time));
                let existingClass = $(`#CONV-ITEM-${convIDClean} .conv-msg-sts`).attr('class');
                $(`#CONV-ITEM-${convIDClean} .conv-msg-sts`).removeClass(existingClass.split(' ')[1]).addClass(convObj.status);
                $(` #CONV-ITEM-${convIDClean}`).removeClass('last-msg-in');
                if (convObj.status === "received" && UA_APP_UTILITY.CURRENT_CONVERSATION_ID !== convID) {
                    $(`#CONV-ITEM-${convIDClean}`).addClass('last-msg-in');
                    $(`#CONV-ITEM-${convIDClean} .ucli-unread`).fadeOut().text(parseInt($(`#CONV-ITEM-${convIDClean} .ucli-unread`).text()) + 1).fadeIn();
                }
                UA_APP_UTILITY.reorderConvBasedOnTime();
            }
        }
    },
    FETCHED_CONVERSATIONS: {},
    addConversationItemToConversationBox: function (convObj) {
        try {
            UA_APP_UTILITY.FETCHED_CONVERSATIONS[convObj.id] = convObj;
            let convIDClean = UA_APP_UTILITY.getCleanStringForHTMLAttribute(convObj.id);
            if ($(`#CONV-ITEM-${convIDClean}`).length > 0) {
                UA_APP_UTILITY.updateConversationItemIfExists(convIDClean, convObj);
                return false;
            }
            UA_APP_UTILITY.LOCAL_FETCHED_CONVERSATION_IDS.push(convObj.id);
            let refId = new Date().getTime();
            $('#uaCHATConvListHolder').prepend(`
                        <div class="ua-conv-list-item ${convObj.status === "received" ? "last-msg-in" : ''}" data-last-msg-time="${parseInt(convObj.time)}" id="CONV-ITEM-${convIDClean}" onclick="UA_APP_UTILITY.selectConversationToLoad('${convObj.id}', event)">
                            <div class="ucli-pic" style="background-image:url('./images/messenger_icons/${convObj.channel.id.toLowerCase()}-50.png')">  </div>
                            <div class="ucli-det">
                                <div class="ucli-time">
                                    ${getTimeString(convObj.time, true)}
                                </div>
                                <div class="ucli-name">
                                    ${convObj.name}
                                </div>
                                <div class="ucli-msg">
                                    ${convObj.channel ? convObj.channel.label : "SMS"} : <span class="conv-msg-sts ${convObj.status}">${UA_APP_UTILITY.getMessageStatusHTML(convObj.status)}</span> <span class="conv-msg-text">${getSafeString(convObj.message)}</span>
                                </div>
                                <div class="ucli-unread">0</div>
                                <div class="ucli-saas-det ${refId}">
                                    <div class="ucli-saas-init-rcd ${refId}"></div>
                                    <div class="ucli-saas-view-all ${refId}" style="display: none;"><span class="ucli-saas-view-all-btn" onclick="$('.ucli-saas-all-rcd.${refId}').toggle();event.target.textContent = event.target.textContent == 'close'?'view all':'close'">view all<span></div>
                                    <div class="ucli-saas-all-rcd ${refId}" style="display: none;"></div>
                                </div>
                            </div>
                        </div>
                    `);
            if (!UA_SAAS_SERVICE_APP.widgetContext.module || !UA_SAAS_SERVICE_APP.widgetContext.entityId) {
                UA_APP_UTILITY.fetchToRelatedRecordsForChat(convObj, refId);
                UA_APP_UTILITY.reorderConvBasedOnTime();
            }
        }
        catch (ex) {
            console.log(ex);
        }
    },

    relatedRecordsListFromSaas: {},
    fetchToRelatedRecordsForChat: async function (convObj, refId) {
        try {
            if (!UA_TPA_FEATURES.getConvRelatedRecordsFromSaas || typeof UA_TPA_FEATURES.getConvRelatedRecordsFromSaas !== 'function') return;
            var relatedRecords = await UA_TPA_FEATURES.getConvRelatedRecordsFromSaas(convObj);
            let allRelatedFromSaasHtmlText = '';
            let initRelatedFromSaasHtmlText = '';
            let recordsCount = 0;
            UA_APP_UTILITY.relatedRecordsListFromSaas[convObj.id ? convObj.id : refId] = relatedRecords;
            if (relatedRecords && relatedRecords.length > 0) {
                if (relatedRecords[0] && relatedRecords[0].status && relatedRecords[0].status == "retry_request") {
                    return UA_APP_UTILITY.fetchToRelatedRecordsForChat(convObj, refId);
                }
                $(`.ucli-saas-all-rcd.${refId}`).empty();
                $(`.ucli-saas-init-rcd.${refId}`).empty();
                // relatedRecords.forEach((item)=>{
                for (var i = 0; i < relatedRecords.length; i++) {
                    let item = relatedRecords[i];
                    allRelatedFromSaasHtmlText += `<div class="ucli-saas-det-item" onclick="window.open('${item.record.webURL ? item.record.webURL : "#"}')" title="${getSafeString(item.module.replace("_", " "))}">
                                                        <div class="ucli-saas-det-item-mdl-name">
                                                            ${getSafeString(item.record.owner_name)}
                                                        </div>
                                                        <div class="ucli-saas-det-item-rcd-name">
                                                            ${getSafeString(item.record.name)}
                                                        </div>
                                                    </div>`
                    recordsCount++;
                    if (recordsCount == 1) initRelatedFromSaasHtmlText = allRelatedFromSaasHtmlText;
                    if (recordsCount > 1) $(`.ucli-saas-view-all.${refId}`).show();
                }
                $(`.ucli-saas-all-rcd.${refId}`).append(allRelatedFromSaasHtmlText);
                $(`.ucli-saas-init-rcd.${refId}`).append(initRelatedFromSaasHtmlText);
            }
        }
        catch (ex) { console.log(ex); }
    },

    reorderConvBasedOnTime: function () {
        $('#uaCHATConvListHolder .ua-conv-list-item').sort(function (a, b) {
            return $(b).data('last-msg-time') - $(a).data('last-msg-time');
        }).appendTo('#uaCHATConvListHolder');
    },
    updateDefaultCountryCode: function () {
        let countryCode = $("#ssf-rec-def-countrycode input").val().trim();
        let criteria = $("#ssf-rec-def-countrycode-criteria select").val();
        if (!valueExists(countryCode)) {
            criteria = 'ignore';
        }
        localStorage.setItem('ua_ui_config_msg_def_countrycode', countryCode);
        localStorage.setItem('ua_ui_config_msg_def_countrycode_criteria', criteria);
    },
    CURRENT_EVENT_PAGE_VIEW: "new",
    SUPPORTED_EVENT_PAGE_VIEWS: ["new", "history", "links"],
    pageEventNavItemSelected: function (navType) {
        UA_APP_UTILITY.SUPPORTED_EVENT_PAGE_VIEWS.forEach(item => {
            $('body').removeClass('view-event-view-' + item);
        });
        $('body').addClass('view-event-view-' + navType);
        UA_APP_UTILITY.CURRENT_EVENT_PAGE_VIEW = navType;
        $('.peana-nav-item').removeClass('selected');
        $('.peana-tab-container').hide();
        $('#peanea-tab-' + navType).show();
        $('#peanean-' + navType).addClass('selected');
        if (typeof UA_TPA_FEATURES.pageEventNavItemSelected === "function") {
            UA_TPA_FEATURES.pageEventNavItemSelected(navType);
        }
    },
    addToOrgCacheIfNotExists: function (key, value) {
        if (!UA_APP_UTILITY.ACTUAL_ORG_COMMON_SETTINGS) {
            UA_APP_UTILITY.CALL_AFTER_ORG_COMMON_SETTING_RESOLVED.push(function () {
                UA_APP_UTILITY.addToOrgCacheIfNotExists(key, value);
            });
            return;
        }
        let cachename = '_ua_app_cache_' + key;
        let alreadyExists = UA_APP_UTILITY.ACTUAL_ORG_COMMON_SETTINGS && UA_APP_UTILITY.ACTUAL_ORG_COMMON_SETTINGS[cachename];
        if (!alreadyExists) {
            UA_APP_UTILITY.saveSettingInOrgCommonCredDoc(cachename, value);
        }
    },
    SUGGESTED_SAAS_USER_EMAIL: null,
    doNotShowAccountMismatch: function () {
        localStorage.setItem('ua_ignore_account_mismatch_for_' + currentUser.email, this.SUGGESTED_SAAS_USER_EMAIL);
        window.location.reload();
    },
    receivedSuggestedCurrentEnvLoginEmailID: function (email) {
        if (valueExists(email)) {
            this.SUGGESTED_SAAS_USER_EMAIL = email;
            $('.sign-in-suggest-email').html(email);
            if (currentUser && currentUser.email !== email) {
                console.log('MISMATCH ON EMAIL SUGGESTED MATCH');
                try {
                    if (localStorage.getItem('ua_ignore_account_mismatch_for_' + currentUser.email)) {
                        if (localStorage.getItem('ua_ignore_account_mismatch_for_' + currentUser.email) === this.SUGGESTED_SAAS_USER_EMAIL) {
                            return true;
                        }
                    }
                } catch (ex) { console.log(ex); }
                showErroWindow('Ulgebra Account Mismatch!', `Kindly login using email <b class="sign-in-suggest-email" style="font-size: 15px; padding: 2px 10px;">${email}</b>.<br><br><div style="font-size:14px;color:rgb(80,80,80)"> Current Ulgebra account email <i style="color:crimson">${currentUser.email}</i> does not match with ${saasServiceName} email <b>${email}</b>.</div><br> <button style="font-size:15px !important" class="ua_service_login_btn ua_primary_action_btn" onclick="signOut()">Login again</button> <br><br> <div onclick="UA_APP_UTILITY.doNotShowAccountMismatch()" style="text-decoration: underline; color: grey; cursor: pointer; margin-top: 15px;">I understand the risk, continue with current email.</div>`, true, false, 50000);
                UA_SAAS_SERVICE_APP = {};
                UA_TPA_FEATURES = {};
            }
        }
    },
    insertIncomingWD_supportedModules: function () {
        if (!UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS) {
            UA_APP_UTILITY.CALL_AFTER_PERSONAL_COMMON_SETTING_RESOLVED.push(() => { UA_APP_UTILITY.insertIncomingWD_supportedModules(); });
            return;
        }
        $('#suo-item-incoming-nav').show();
        let supportedModuleDD = ``;
        if (UA_SAAS_SERVICE_APP.supportedIncomingModules) {
            //            supportedModuleDD = `<div>When a new event booked in Calendly, create <select onchange="UA_APP_UTILITY.saveTPAIncomingSAASTargetModule()" id="in-msg-saas-entity-module-select" style="font-size: 20px; margin-left: 5px; margin-right: 5px; border-bottom: 1px dotted black;">`;
            UA_SAAS_SERVICE_APP.supportedIncomingModules.forEach(item => {
                let selected = item.selected;
                if (UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS && UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.hasOwnProperty('ua_incoming_saas_target_module')) {
                    selected = UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.ua_incoming_saas_target_module === item.value;
                }
                supportedModuleDD += `<option ${selected ? 'selected' : ''} value="${item.value}">${item.label}</option>`;
            });
            //            supportedModuleDD+=`</select> in ${extensionName.split('for')[1].toUpperCase()}</div>`;
        }
        $('#error-window-tpa_service_senders #incoming-module-config-item #in-msg-saas-entity-module-select').append(supportedModuleDD);
        if (UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS.hasOwnProperty('ua_app_setting_incoming_no_create_new_entity')) {
            document.getElementById('tpa-switch-incoming-new-entity-create-disable').checked = UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS['ua_app_setting_incoming_no_create_new_entity'];
        }
    },
    toggleIncomingCaptureConfig: function () {
        let enableIncoming = document.getElementById('tpa-switch-incoming-enable-capture').checked;
        UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS['ua_incoming_enable_capture_' + currentUser.uid] = enableIncoming;
        UA_APP_UTILITY.saveSettingInCredDoc('ua_incoming_enable_capture_' + currentUser.uid, enableIncoming);
        if (enableIncoming) {
            $('.tpaichan-devider').fadeIn();
            if (!UA_TPA_FEATURES.EXISTING_WEBHOOK_ID) {
                UA_TPA_FEATURES.addNewWebhook();
            }
        }
        else {
            $('.tpaichan-devider:not(#master-tpai-incom-chan-item)').fadeOut();
            if (UA_TPA_FEATURES.EXISTING_WEBHOOK_ID !== null) {
                UA_TPA_FEATURES.deleteExistingWebhook();
            }
        }
    },
    toggleSAASTicketReplyCaptureConfig: function () {
        let enableIncoming = document.getElementById('saas-switch-incoming-comments-enable-capture').checked;
        UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS['ua_incoming_saas_ticket_reply_enable_capture_' + currentUser.uid] = enableIncoming;
        UA_APP_UTILITY.saveSettingInCredDoc('ua_incoming_saas_ticket_reply_enable_capture_' + currentUser.uid, enableIncoming);
        if (enableIncoming) {
            if (UA_SAAS_SERVICE_APP.EXISTING_WEBHOOK_ID === null) {
                UA_SAAS_SERVICE_APP.addNewWebhook();
            }
        }
        else {
            if (UA_SAAS_SERVICE_APP.EXISTING_WEBHOOK_ID !== null) {
                UA_SAAS_SERVICE_APP.deleteExistingWebhook();
            }
        }
    },
    updateEscapedMultiLineInput: function () {
        let originalVal = $("#inp-workflow-multiline-msg").val().trim();
        var myJSONString = JSON.stringify(originalVal).substr(1, originalVal.length);
        var myEscapedJSONString = myJSONString.replace(/\\n/g, "\\n")
            .replace(/\\'/g, "\\'")
            .replace(/\\"/g, '\\"')
            .replace(/\\&/g, "\\&")
            .replace(/\\r/g, "\\r")
            .replace(/\\t/g, "\\t")
            .replace(/\\b/g, "\\b")
            .replace(/\\f/g, "\\f");
        $('#ua-inp-escaped-multiline-msg').val(myEscapedJSONString);
    },
    toggleIncomingNewEntityCreationConfig: function () {
        UA_APP_UTILITY.saveSettingInCredDoc('ua_app_setting_incoming_no_create_new_entity', document.getElementById('tpa-switch-incoming-new-entity-create-disable').checked);
    },
    MESSAGING_WORKFLOW_CODE_SHOW_MODE: false,
    prepareAndSendSMSForWorkflowCode: function () {
        UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE = true;
        UA_TPA_FEATURES.prepareAndSendSMS();
    },
    prepareAndSendSMS: function () {
        UA_APP_UTILITY.MESSAGING_WORKFLOW_CODE_SHOW_MODE = false;
        UA_TPA_FEATURES.prepareAndSendSMS();
    },
    switchToFormViewForManualWorkflowInputConfig: function () {
        $('#workflowInstructionDialog').hide();
        UA_APP_UTILITY.changeCurrentView('MESSAGE_FORM');
        $('.userInfoBtn').click();
        $("#ssf-manual-workflow-btn-holder").css({
            'display': 'inline-block',
            'margin-top': '0px',
            'margin-left': '5px'
        });
    },
    showMessagingWorkflowCode: function (code) {

        $('#msgItemsProgHolder').html('');
        $('#sms-prog-window').hide();
        code.module = UA_SAAS_SERVICE_APP.widgetContext && UA_SAAS_SERVICE_APP.widgetContext.module ? UA_SAAS_SERVICE_APP.widgetContext.module : "FILL_HERE";
        let recordId = null;
        if (code.module) {
            try {
                if (UA_SAAS_SERVICE_APP.widgetContext.entityId) {
                    if (typeof UA_SAAS_SERVICE_APP.widgetContext.entityId === "string") {
                        recordId = UA_SAAS_SERVICE_APP.widgetContext.entityId;
                    }
                    if (Array.isArray(UA_SAAS_SERVICE_APP.widgetContext.entityId)) {
                        recordId = UA_SAAS_SERVICE_APP.widgetContext.entityId[0];
                    }
                }
            }
            catch (ex) {
                console.log(ex);
            }
        }
        code.recordId = recordId ? recordId : "FILL_HERE";
        if (UA_APP_UTILITY.currentSender) {
            code.channel = UA_APP_UTILITY.currentSender.channel.label;
        }
        code.ulgebra_webhook_authtoken = UA_APP_UTILITY.PERSONAL_WEBHOOK_TOKEN.saas;
        UA_APP_UTILITY.addWorkflowBodyCode('__manual_calculated_configuration__', 'Workflow code for given inputs, change paramters below if needed', code);
        $("#calculated-form-wiparams-holder").show();
        $('#workflowInstructionDialog').show();
        $('.hideoncalculatedwiparams').hide();
    },
    updateUserMappingFieldValue: function (tpaUserID, saasUserIDSelectElem) {
        let saasUserID = $(saasUserIDSelectElem + ' .tpaichan-field-mapping-tpa-fields select').val();
        UA_APP_UTILITY.saveSettingInCredDoc('ua_incoming_tpa_saas_user_mapping_' + tpaUserID, saasUserID);
    },
    getUserMappingFieldValue: function (tpaUserID) {
        return UA_APP_UTILITY.ACTUAL_PERSONAL_COMMON_SETTINGS['ua_incoming_tpa_saas_user_mapping_' + tpaUserID];
    },
    NOTIFICATIONS_COUNT_RT_LISTENER: null,
    CURRENT_UNREAD_NOTIF_COUNT: 0,
    startListeningNotificationsCountForUser: function (userId) {
        try {
            if (!extensionName.startsWith('acuity') && !extensionName.startsWith('twilio') && !extensionName.startsWith('messagebird') && !extensionName.startsWith('whatcetra') && !extensionName.startsWith('ringcentral') && !extensionName.startsWith('calendly') && !extensionName.startsWith('pinnacle') && !extensionName.startsWith('karix')) {
                $('#nav_notificationIcon').hide();
                return false;
            }
            if (extensionName.endsWith('zohobooks')) {
                $('#nav_notificationIcon').hide();
                return false;
            }
            if (!userId) {
                return false;
            }
            if (this.NOTIFICATIONS_COUNT_RT_LISTENER) {
                this.NOTIFICATIONS_COUNT_RT_LISTENER.off();
            }
            this.NOTIFICATIONS_COUNT_RT_LISTENER = firebase.database().ref(`user_unread_notifications_count/${userId}/${extensionName}`);
            this.NOTIFICATIONS_COUNT_RT_LISTENER.on('value', function (snapshot) {
                let notifCount = snapshot.val() ? Math.ceil(snapshot.val() * 10) / 10 : 0;
                if (notifCount < 0) {
                    notifCount = 0;
                }
                $('#ua_current_app_alert_count').text(Intl.NumberFormat('en-US', {
                    notation: "compact",
                    maximumFractionDigits: 1
                }).format(notifCount));
                $('#nav_notificationIcon').toggleClass('no-notifications', notifCount === 0);
                UA_APP_UTILITY.CURRENT_UNREAD_NOTIF_COUNT = notifCount;
                if (notifCount > 0) {
                    if ($('.notification-side-panel-outer').css('display') !== "none") {
                        UA_APP_UTILITY.renderAndShowNotifications(true);
                    }
                } else {
                    if (notifCount === 0) {
                        $('.nspc-item').removeClass('unread');
                    }
                }
            });
        } catch (ex) { console.log(ex); }
    },
    markAllMyNotificationsAsTrue: async function () {
        if (document.querySelectorAll(".nspc-item.unread").length > 0) {
            var items = document.querySelectorAll(".nspc-item.unread");
            for (var i = 0; i < items.length; i++) {
                let item = items[i];
                let notifId = item.id.split("uanotifid-")[1];
                await UA_APP_UTILITY.uaUserNotificationStatusUpdate(null, notifId);
            }
        }
        firebase.database().ref(`user_unread_notifications_count/${currentUser.uid}/${extensionName}`).set(0);
    },

    uaUserNotificationStatusUpdate: async function (event, notifId) {
        if (!notifId) return;
        if ($(`#uanotifid-${notifId}`).hasClass("unread")) {
            await db.collection("ulgebraUsers").doc(currentUser.uid).collection('extensions').doc(extensionName).collection("notifications").doc(notifId).set({ readTime: new Date().getTime() }, { merge: true });
            $(`#uanotifid-${notifId}`).removeClass('unread');
            if (UA_APP_UTILITY.CURRENT_UNREAD_NOTIF_COUNT > 0) {
                firebase.database().ref(`user_unread_notifications_count/${currentUser.uid}/${extensionName}`).set(firebase.database.ServerValue.increment(-1));
            }
        }
    },

    renderAndShowNotifications: async function (fetchForLatest = false) {
        try {
            if ($('.notification-side-panel-outer').css('display') !== 'none' && !fetchForLatest) {
                $('.notification-side-panel-outer').hide();
                return false;
            }
            if (!extensionName) {
                return false;
            }
            $('.notification-side-panel-outer').show();
            var resp = await db.collection("ulgebraUsers").doc(currentUser.uid).collection('extensions').doc(extensionName).collection("notifications").orderBy('ct', 'desc').limit(50).get();
            if (resp.docs) {
                let renderedElems = 0;
                resp.docs.forEach(item => {
                    renderedElems++;
                    let itemData = item.data();
                    if ($(`#uanotifid-${item.id}`).length > 0) {
                        return;
                    }
                    let isUnReadNotication = true;
                    if (itemData && itemData.hasOwnProperty("readTime")) {
                        isUnReadNotication = false;
                    }
                    else if (itemData.ct < 1663581977486) {
                        isUnReadNotication = fetchForLatest || renderedElems <= UA_APP_UTILITY.CURRENT_UNREAD_NOTIF_COUNT;
                    }
                    if (!itemData.nGrpID) {
                        itemData.nGrpID = "";
                    }
                    let notifItemHTML = `
                <div class="nspc-item ${isUnReadNotication ? 'unread' : ''}" id="uanotifid-${item.id}" onclick="UA_APP_UTILITY.uaUserNotificationStatusUpdate(event, '${item.id}')">
                    <a target="_blank" href="./app-notification-click?notificationId=${item.id}&appCode=${extensionName}&notificationTitle=${encodeURIComponent(itemData.nTtl)}&notificationContent=${encodeURIComponent(itemData.nMsg)}">
                        <div class="nspc-item-ttl">
                            ${UA_APP_UTILITY.getSafeString(itemData.nTtl)}
                        </div>
                    </a>
                    <div class="nspc-item-content">
                        ${UA_APP_UTILITY.getSafeString(itemData.nMsg)}
                    </div>
                    <div class="nspc-item-time">
                        ${UA_APP_UTILITY.getTimeString(itemData.ct, true)}
                    </div>
                    <div class="thisNotiFootElement" style="display: flex; width: 100%; font-size: 10px; height: 15px; align-items: flex-end;">
                        <div class="thisNotiFootElement_entityOpen" style="display: flex;align-items: center;cursor: pointer; width: 50px;" title="Open Record">
                            <div>
                                <i class="material-icons" style="color: rgb(148, 148, 181); font-size: 18px;" onmouseover="$(this).css({'color': 'rgb(169 123 33)'});" onmouseout="$(this).css({'color': '#9494b5'});" onclick="window.open('./app-notification-click?notificationId=${item.id}&appCode=${extensionName}&notificationTitle=${encodeURIComponent(itemData.nTtl)}&notificationContent=${encodeURIComponent(itemData.nMsg)}')"> person_search </i>
                            </div>
                            <div></div>
                        </div>
                        <div class="thisNotiFootElement_chatOpen" style="display: ${!itemData.nGrpID ? 'none' : 'flex'};align-items: center;cursor: pointer; width: 50px;" title="Open Chat">
                            <div>
                                <i class="material-icons" style="color: rgb(148, 148, 181); font-size: 18px; position: relative; top: 2px;" onmouseover="$(this).css({'color': 'rgb(52 130 207)'});" onmouseout="$(this).css({'color': '#9494b5'});" onclick="UA_APP_UTILITY.openChatIdToChatBox('${itemData.nGrpID}')"> question_answer </i>
                            </div>
                            <div></div>
                        </div>
                        <div class="thisNotiFootElement_chatNewTap" style="display: ${!itemData.nGrpID ? 'none' : 'flex'};align-items: center;cursor: pointer; width: 50px;" title="Open Chat in New Tab">
                            <div>
                                <i class="material-icons" style="color: rgb(148, 148, 181); font-size: 18px; fill: rgb(169, 123, 33);" onmouseover="$(this).css({'color': 'black'});" onmouseout="$(this).css({'color': '#9494b5'});" onclick="window.open('${window.location.href + '&openChatId=' + itemData.nGrpID}')"> open_in_new </i>
                            </div>
                            <div></div>
                        </div>
                    </div>
                    
                </div>
            `;
                    if (fetchForLatest) {
                        $('#notification-side-panel-list-holder').prepend(notifItemHTML);
                    }
                    else {
                        $('#notification-side-panel-list-holder').append(notifItemHTML);
                    }

                });
            }

        } catch (ex) { console.log(ex); }
    },
    openChatIdToChatBox: async function (chatId) {
        try {
            UA_APP_UTILITY.currentMessagingView = UA_APP_UTILITY.appPrefinedUIViews['CHAT'];
            $('#nav_viewTypeSwitch .dropdownDisplayText').text(UA_APP_UTILITY.currentMessagingView.label);
            $('body').addClass(UA_APP_UTILITY.currentMessagingView.className);
            UA_APP_UTILITY.CURRENT_CONVERSATION_ID = null;
            $("#chatbox-message-holder").html("");
            UA_APP_UTILITY.selectConversationToLoad(chatId, event);
        } catch (ex) { console.log(ex); }
    },
    getAppPrettyName: function (extensionName) {
        var appTPACode = "undefined";
        var appPrettyName = "Ulgebra";
        try {
            appTPACode = extensionName ? extensionName.split('for')[0] : "undefined";
            appPrettyName = appPrettyNameMap[appTPACode] ? appPrettyNameMap[appTPACode] : "Ulgebra";
        }
        catch (ex) {
            console.log(ex);
        }
        return appPrettyName;
    },
    CURRENT_SUGGESTED_CONVERSATION_ID: null,
    SUGGESTED_SAAS_TPA_COV_MAP: {},
    suggestConversationSAASTPAMapFound: function (saasTpaMap) {
        let conversationId = saasTpaMap.conversationId;
        if (conversationId) {
            UA_APP_UTILITY.SUGGESTED_SAAS_TPA_COV_MAP[conversationId] = saasTpaMap;
            UA_APP_UTILITY.suggestConversationIDFound(conversationId);
        }
    },
    suggestConversationIDFound: function (CURRENT_SUGGESTED_CONVERSATION_ID) {
        UA_APP_UTILITY.CURRENT_SUGGESTED_CONVERSATION_ID = CURRENT_SUGGESTED_CONVERSATION_ID;
        UA_APP_UTILITY.selectConversationToLoad(CURRENT_SUGGESTED_CONVERSATION_ID, null);
    },
    getServiceSAASTPAConversationMapIfExists: async function (serviceName, module, identifier) {
        if (!identifier || !module || !serviceName) {
            return null;
        }
        var data = null;
        var db = firebase.firestore();
        let docPath = `${extensionName}/${serviceName}/${appsConfig.UA_DESK_ORG_ID}/${module}/${identifier}`;
        console.log('resolved docpath getCreatedEntityInService ' + docPath);
        await db.collection("entityMapsCreatedInServiceForSearch").doc(docPath)
            .get().then(doc => {
                if (doc.exists) {
                    data = doc.data();
                }
                return true;
            });
        return data;
    },
    PRESENCE_HANDLER_GLOBAL_RTDB_LISTERNER: null,
    PRESENCE_HANDLER_MINE_RTDB_LISTERNER: null,
    setMySelfAsOnlinePresent: function (ref) {
        ref.set({
            'status': 'online',
            'name': currentUser.displayName,
            'pic': currentUser.photoURL,
            'saasUserID': UA_SAAS_SERVICE_APP.CURRENT_USER_INFO ? UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.id : null,
            'ct': new Date().getTime()
        });
    },
    appCollisionPresenceHandler: function initiatePresenceHandler(pageID) {
        if (!currentUser || !appsConfig.UA_DESK_ORG_ID) {
            return;
        }
        UA_APP_UTILITY.clearUACLUserList();
        try {
            if (UA_APP_UTILITY.PRESENCE_HANDLER_MINE_RTDB_LISTERNER) {
                UA_APP_UTILITY.PRESENCE_HANDLER_MINE_RTDB_LISTERNER.off();
            }
            if (UA_APP_UTILITY.PRESENCE_HANDLER_GLOBAL_RTDB_LISTERNER) {
                UA_APP_UTILITY.PRESENCE_HANDLER_GLOBAL_RTDB_LISTERNER.off();
            }
            pageID = UA_APP_UTILITY.getSafeStringForRTDBPath(pageID);
            UA_APP_UTILITY.PRESENCE_HANDLER_MINE_RTDB_LISTERNER = firebase.database().ref(`presenceAndCollisions/${extensionName}/${UA_APP_UTILITY.getSafeStringForRTDBPath(appsConfig.UA_DESK_ORG_ID)}/${pageID}/${currentUser.uid}`);
            UA_APP_UTILITY.setMySelfAsOnlinePresent(UA_APP_UTILITY.PRESENCE_HANDLER_MINE_RTDB_LISTERNER);
            UA_APP_UTILITY.PRESENCE_HANDLER_MINE_RTDB_LISTERNER.onDisconnect().remove();
            UA_APP_UTILITY.PRESENCE_HANDLER_MINE_RTDB_LISTERNER.on('value', function (snapshot) {
                if (snapshot.val() && snapshot.val().status !== 'online') {
                    UA_APP_UTILITY.setMySelfAsOnlinePresent(snapshot.ref());
                }
            });
            UA_APP_UTILITY.PRESENCE_HANDLER_GLOBAL_RTDB_LISTERNER = firebase.database().ref(`presenceAndCollisions/${extensionName}/${UA_APP_UTILITY.getSafeStringForRTDBPath(appsConfig.UA_DESK_ORG_ID)}/${pageID}`);
            UA_APP_UTILITY.PRESENCE_HANDLER_GLOBAL_RTDB_LISTERNER.get().then((snapshot) => {
                if (snapshot.exists()) {
                    let onlineUsersObj = snapshot.val();
                    console.log(onlineUsersObj);
                    for (var i in onlineUsersObj) {
                        console.log(onlineUsersObj[i]);
                        UA_APP_UTILITY.showUACLUserAdd(i, onlineUsersObj[i]);
                    }
                }
                else {
                    console.log("No ones data available");
                }
            }).catch((error) => {
                console.error(error);
            });
            UA_APP_UTILITY.PRESENCE_HANDLER_GLOBAL_RTDB_LISTERNER.on('child_added', function (snapshot) {
                console.log('child_added..', snapshot.key, snapshot.val());
                UA_APP_UTILITY.showUACLUserAdd(snapshot.key, snapshot.val());
            });
            UA_APP_UTILITY.PRESENCE_HANDLER_GLOBAL_RTDB_LISTERNER.on('child_changed', function (snapshot) {
                console.log('child_changed..', snapshot.key, snapshot.val());
            });
            UA_APP_UTILITY.PRESENCE_HANDLER_GLOBAL_RTDB_LISTERNER.on('child_removed', function (snapshot) {
                console.log('child_removed..', snapshot.key, snapshot.val());
                UA_APP_UTILITY.removeUACLUser(snapshot.key);
            });
        } catch (ex) { console.log(ex); }
    },
    hideUACLUserAlert: function (id) {
        $(`#uaclu-item-${id}`).hide();
    },
    removeUACLUser: function (id) {
        console.log(`user ${id} is offline`);
        $(`#uaclu-item-${id} .ripple`).remove();
        $(`#uaclu-item-${id}`).addClass('expired-uaclu').attr('id', `expired-uaclu-item-${id}`);
        $(`#expired-uaclu-item-${id} .uaclu-time`).text('has left this page');
        setTimeout(() => {
            $(`#expired-uaclu-item-${id}`).slideUp();
        }, 3000);
    },
    clearUACLUserList: function () {
        $('.uaclu-item').remove();
    },
    showUACLUserAdd: function (id, userObj) {
        console.log(`user ${id} is online`);
        if (id === currentUser.uid) {
            return false;
        }
        if ($(`#uaclu-item-${id}`).length > 0) {
            return false;
        }
        $('#agent-user-collisions-list-holder').append(`
            <div class="uaclu-item" id="uaclu-item-${id}">
                <div class="ripple"></div>
                <div class="uaclu-pic"></div>
                <div class="uaclu-name">
                    ${userObj.name}
                    <div class="uaclu-time">is also in this page since ${UA_APP_UTILITY.getTimeString(userObj.ct).replace('ago', '')}</div>
                </div>
                <div class="uaclu-close" onclick="UA_APP_UTILITY.hideUACLUserAlert('${id}')">
                    x
                </div>
            </div>
        `);
    },
    stripHtml: function (html) {
        let tmp = document.createElement("DIV");
        tmp.innerHTML = html;
        return tmp.textContent || tmp.innerText || "";
    },

    setOnlyCanSendTempMsgsHandler: async function (isWantToSet) {
        try {
            if (isWantToSet) {
                // $('#inp-ssf-main-message').attr('readonly', true);
                $(".ua-send-alert.ua-send-warning").show();
            }
            else {
                // $('#inp-ssf-main-message').removeAttr('readonly');
                $(".ua-send-alert.ua-send-warning").hide();
            }
            UA_APP_UTILITY.reCalcluateChatBoxSize();
        }
        catch (err) { console.log(err); return false; }
    },

    diff_hours: function (dt2, dt1) {
        try {
            var diff = (dt2.getTime() - dt1.getTime()) / 1000;
            diff /= (60 * 60);
            return Math.abs(Math.round(diff));

        }
        catch (err) { console.log(err); return null; }
    },
    setChatFocusModeOnChatBoxHover: function () {
        if (!$('body').hasClass('event-chatbox-on-focus')) {
            $('body').addClass('event-chatbox-on-focus');
            UA_APP_UTILITY.reCalcluateChatBoxSize();
        }
    },
    removeChatFocusModeOnChatBoxHover: function () {
        if ($('body').hasClass('event-chatbox-on-focus')) {
            $('body').removeClass('event-chatbox-on-focus');
            UA_APP_UTILITY.reCalcluateChatBoxSize();
        }
    },

    isProviderInitialized: false,
    addAddNewActionProvider: function () {
        try {
            if (!UA_APP_UTILITY.isProviderInitialized) {
                UA_APP_UTILITY.providerDropdownID = UA_APP_UTILITY.renderSelectableDropdown('#DD_HOLDER_PROVIDER_LIST', 'Choose Provider', [{
                    'label': '+ Add New SMS Provider',
                    'value': '---',
                    'action': 'invokeFunction',
                    'function': `UA_APP_UTILITY.showAllSmsAppListUI`,
                    'isActionBtn': true
                }], 'console.log', false, false, UA_APP_UTILITY.providerDropdownID);
                UA_APP_UTILITY.isProviderInitialized = true;
            }
        }
        catch (ex) {
            console.log(ex);
        }
    },
    addMessaegProvider: function () {
        try {
            UA_APP_UTILITY.addAddNewActionProvider();
            let listDropdownItems = [];
            UA_APP_UTILITY.UA_APPS_LIST_FOR_USER.forEach((appId) => {
                let tpaName = appId.split("for")[0];
                if (appId.indexOf(appCodeSAAS) > -1 && ["calendly", "acuityscheduling", "lookup", "anysms", appCodeSAAS].includes(tpaName) != true) {
                    listDropdownItems.push({
                        'label': appPrettyNameMap[tpaName],
                        'value': appId,
                        'selected': appId == extensionName
                    });
                }
            });

            UA_APP_UTILITY.senderDropdownID = UA_APP_UTILITY.renderSelectableDropdown('#DD_HOLDER_PROVIDER_LIST', 'Choose Provider', listDropdownItems, 'UA_APP_UTILITY.redirectByAppId', false, false, UA_APP_UTILITY.providerDropdownID);
        }
        catch (ex) {
            console.log(ex);
        }
    },

    APP_NOTIFICATIONS_COUNT: {},
    UA_APPS_LIST_FOR_USER: [],

    renderInitialElements_anysms: async (isAnySms = false) => {
        try {
            UA_APP_UTILITY.startListeningNotificationsCountForUser_anysms(console.log);
            UA_APP_UTILITY.UA_APPS_LIST_FOR_USER = await UA_APP_UTILITY.getUserAuthorizedExtentions(currentUser.uid);
            // UA_APP_UTILITY.addMessaegProvider();
            $("#ssf-fitem-ttl-currentTpa-name").text(`${appPrettyNameMap[appCodeTPA]}`);
            if (isAnySms) {
                $(".pageContentHolder").html("")
                $("#suo-item-workflow-nav").hide()
                $("#ac_name_label_tpa").hide();
                $("#ua-all-in-one-sms-container").show();
                let appId = localStorage.getItem("ua-allInOneSms-appCode-" + appCodeSAAS);
                if (appId && appId.indexOf(appCodeSAAS) > -1) UA_APP_UTILITY.redirectByAppId(appId);
            }
            UA_APP_UTILITY.renderUserAppsList(UA_APP_UTILITY.UA_APPS_LIST_FOR_USER, appCodeSAAS);
        }
        catch (ex) {
            console.log(ex);
        }
    },

    showAllSmsAppListUI: function () {
        $("#ua-all-in-one-sms-container").show();
        $(".ua-all-in-one-sms-close").show();
    },

    getUserAuthorizedExtentions: async function (userId) {
        try {
            var userAppsList = [];
            await db.collection("ulgebraUsers").doc(userId).collection("extensions").get().then(async function (docs) {
                await docs.forEach(doc => {
                    userAppsList.push(doc.id);
                });
                return;
            });
            return userAppsList;
        }
        catch (ex) {
            console.log(ex);
        }
    },

    startListeningNotificationsCountForUser_anysms: async function (callback) {
        try {
            if (!firebase.auth().currentUser) return;
            let NOTIFICATIONS_COUNT_RT_LISTENER = firebase.database().ref(`user_unread_notifications_count/${firebase.auth().currentUser.uid}`);
            await NOTIFICATIONS_COUNT_RT_LISTENER.on('value', function (snapshot) {
                UA_APP_UTILITY.APP_NOTIFICATIONS_COUNT = snapshot.val();
                return callback(UA_APP_UTILITY.APP_NOTIFICATIONS_COUNT);
            });
        }
        catch (ex) {
            console.log(ex);
        }
    },

    renderUserAppsList: async function (appList, saasName) {
        try {
            // if(!appList && appList.length < 1){
            UA_APP_UTILITY.renderUltraAppsList(saasName);
            return;
            // }
            appList.push("addNewfor" + saasName);
            appList.forEach((appId) => {
                let tpaName = appId.split("for")[0];
                if (appId.indexOf(saasName) > -1 && ["calendly", "acuityscheduling", "lookup", "anysms", saasName].includes(tpaName) != true) {
                    UA_APP_UTILITY.showAppIconInUI_anysms(appId, UA_APP_UTILITY.APP_NOTIFICATIONS_COUNT[appId], "UA_APP_UTILITY.redirectByAppId");
                }
            });
        }
        catch (ex) {
            console.log(ex);
        }
    },

    renderUltraAppsList: async function (saasName) {
        // if(extensionName.startsWith('pinnacle') || extensionName.startsWith('telebu') || extensionName.startsWith('karix') || extensionName.startsWith('rivet') || extensionName.startsWith('telnyx')){
        //     return false;
        // }
        // else{
        $("#show-use-other-providers").show();
        $("#show-use-other-providers-divider").show();
        // }
        try {
            $(".ua-all-in-one-sms-ttl").text("Choose Providers");
            $("#ua-allInOneSms-apps-holder").empty()
            Object.keys(_requiredTPAScripts).forEach((tpaName) => {
                let appId = tpaName + "for" + saasName;
                if (["calendly", "acuityscheduling", "lookup", "anysms", "whatsappweb"].includes(tpaName) != true) {
                    UA_APP_UTILITY.showAppIconInUI_anysms(appId, UA_APP_UTILITY.APP_NOTIFICATIONS_COUNT[appId], "UA_APP_UTILITY.redirectByAppId");
                }
            });
        }
        catch (ex) {
            console.log(ex);
        }
    },

    showAppIconInUI_anysms: async function (appId, notifCount, onclickFunc) {
        try {
            notifCount = notifCount && notifCount > 0 ? notifCount : 0;
            let iconHtmlText = "";
            let isPlus = false;
            let tpaName = appId.split("for")[0];
            let isInstalled = false;
            if (UA_APP_UTILITY.UA_APPS_LIST_FOR_USER.includes(appId)) isInstalled = true;
            if (tpaName && tpaName != "addNew") {
                iconHtmlText = `<img class="ua-allInOneSms-appItem-icon" src="https://sms.ulgebra.com/js/chrome-extentions/floating-app/apps_logo/${tpaName}.png">`;
            }
            else {
                isPlus = true;
                onclickFunc = "UA_APP_UTILITY.renderUltraAppsList";
                iconHtmlText = `<div class="ua-allInOneSms-appItem-add">+</div>`;
            }
            $("#ua-allInOneSms-apps-holder").append(`<div class="ua-allInOneSms-appItem ${isInstalled ? "ua-iacard" : ""}" onclick="${onclickFunc}('${appId}', event)">
                                                        <div class="ua-allInOneSms-appItem-notif" style="display:${notifCount ? "block" : "none"}">
                                                            <div class="">${notifCount}</div>
                                                        </div>
                                                        <div class="ua-allInOneSms-appItem-icon-holder">
                                                            ${iconHtmlText}
                                                        </div>
                                                        <div class="ua-allInOneSms-appItem-name">${isPlus ? "Add" : appPrettyNameMap[tpaName]}</div>
                                                    </div>`);
        }
        catch (ex) {
            console.log(ex);
        }
    },

    redirectByAppId: async function (appId) {
        try {
            if (appId && appId == extensionName) return;
            localStorage.setItem("ua-allInOneSms-appCode-" + appCodeSAAS, appId);
            let loc = location.href.toString().replace(extensionName, appId);
            if (loc.indexOf("&anysms=true") < 0) loc = loc + "&anysms=true";
            location.href = loc;
        }
        catch (ex) {
            console.log(ex);
        }
    },



    appUsageAnalyticsMain: function () {

        let userStatisticDiv = showErroWindow("App Usage Analytics", `<div id="userStatisticOuterDiv"></div>`, false, true, 1000);

        let statisticDivisions = ['credit-spent', 'message-sent-from-ui', 'incoming-event', 'workflow-start', 'workflow-invalid-hubspot-code', 'workflow-message-sent-success', 'workflow-message-sent-error', 'workflow-history-add-success', 'workflow-history-add-error', 'workflow-end-success', 'workflow-end-error', 'desk-reply-start', 'desk-reply-end-success'];

        statisticDivisions.forEach(function (subStat) {

            if (subStat === 'workflow-invalid-hubspot-code' && extensionName.indexOf('hubspot') <= -1) {
                return;
            }

            $('#' + userStatisticDiv + ' #userStatisticOuterDiv').append(`<div class="userSubStatisticDiv" onclick="UA_APP_UTILITY.userStatisticDetailsView(this, '${subStat}');">${subStat.split('-').join(' ').capitalize()}</div>`);

        });

    },
    userStatisticDetailsView: function (selected, subStat) {

        if ($(selected).next().attr('id') === 'userSubStatisticDetailsDiv') {
            return;
        }
        $('#userSubStatisticDetailsDiv').remove();
        $('.userSubStatisticDivSelected').removeClass('userSubStatisticDivSelected');
        $(selected).addClass('userSubStatisticDivSelected').after(`<div id='userSubStatisticDetailsDiv'><div>`);
        firebase.database().ref(`ualic_user_statistic_calendar_metric/${currentUser.uid}/${extensionName}/${subStat}`).once('value', function (snapshot) {
            if (snapshot.val()) {
                Object.keys(snapshot.val()).forEach(function (statKey) {
                    $('#userSubStatisticDetailsDiv').append(`<div class="userSubStatisticDetailSubDiv"><div class="userSubStatisticDetailSubDivHead">${statKey}</div><div class="userSubStatisticDetailSubDivMid"> : </div><div class="userSubStatisticDetailSubDivBody">${snapshot.val()[statKey]}</div></div>`);
                });
            }
            else {
                $('#userSubStatisticDetailsDiv').append(`<div class="userSubStatisticDetailSubDivEmpty">Empty</div>`);
            }
        });

    },
    doNotShowAppCursorInstall: function () {
        $('.chromeextnpopup').hide();
        localStorage.setItem('do_not_show_install_app_cursor_install_popup', true);
    },
    showAppCursorInstallIfNotInstalled: function () {
        var isChrome = !!window.chrome;
        if (!isChrome) {
            return false;
        }
        if (queryParams.get('anysms') || extensionName.startsWith("lookupfor")) {
            return false;
        }
        UA_APP_UTILITY.isChromeAPPInstalled(UA_APP_UTILITY.CHROME_EXTENSIONS.APPCURSOR, UA_APP_UTILITY.showIfAppCursorNotInstalled);
    },
    showIfAppCursorNotInstalled: function (isAlreadyInstalled) {
        if (isAlreadyInstalled || localStorage.getItem('do_not_show_install_app_cursor_install_popup')) {
            return false;
        }
        $(`body`).append(`<div class="chromeextnpopup">
        <div class="cetp-inner">
            <div class="cetp-ttl" style="
    padding-top: 7px;
">
            <span class="material-icons" style="
    vertical-align: middle;
    margin-right: 3px;
    margin-top: -4px;
    color: orangered;
">auto_awesome</span>    Add AppCursor<div class="cetp-close" onclick="$('.chromeextnpopup').hide()">x</div>
            </div>
            <div class="cetp-content" style="line-height: 1.5;"><ul>${isUASMSApp ? '<li>Send Bulk messages to ' + saasServiceName + ' contacts <li>' + appPrettyName + ' Chat bubble inside ' + saasServiceName + ' </li>' : ''} <li> ${appPrettyName} Instant Notification bubble inside ${saasServiceName}</li><li> Access ${appPrettyName} anywhere inside ${saasServiceName}</li> <br>
                <a href="https://chrome.google.com/webstore/detail/appcursor-all-app-wigets/lgjaomhkknkffgpfbpngnbcinhpddbpp" target="_blank"> 
                    <button class="cetp-install" style="
">
                        <span class="material-icons">add</span> Add Chrome Extension
                    </button>
                </a>
    <b onclick="UA_APP_UTILITY.doNotShowAppCursorInstall()" style="
    font-size: 10px;
    font-weight: normal;
    margin-left: 10px;
    color: grey;
    cursor: pointer;
">Skip, Do not show again</b>
            </div>
        </div>
    </div>`);
    },
    CHROME_EXTENSIONS: {
        APPCURSOR: "lgjaomhkknkffgpfbpngnbcinhpddbpp"
    },
    isChromeAPPInstalled: function (extensionId, callback) {
        try {
            var isChrome = !!window.chrome;
            if (!isChrome) {
                callback(false);
            }
            var img;
            img = new Image();
            img.src = "chrome-extension://" + extensionId + "/images/ulgebra_trans_logo.png";
            img.onload = function () {
                callback(true);
            };
            img.onerror = function () {
                callback(false);
            };
        }
        catch (e) {
            console.log(e);
            callback(false);
        }
    },
    currencySymbolMap: {
        AED: 'د.إ',
        AFN: '؋',
        ALL: 'L',
        AMD: '֏',
        ANG: 'ƒ',
        AOA: 'Kz',
        ARS: '$',
        AUD: '$',
        AWG: 'ƒ',
        AZN: '₼',
        BAM: 'KM',
        BBD: '$',
        BDT: '৳',
        BGN: 'лв',
        BHD: '.د.ب',
        BIF: 'FBu',
        BMD: '$',
        BND: '$',
        BOB: '$b',
        BOV: 'BOV',
        BRL: 'R$',
        BSD: '$',
        BTC: '₿',
        BTN: 'Nu.',
        BWP: 'P',
        BYN: 'Br',
        BYR: 'Br',
        BZD: 'BZ$',
        CAD: '$',
        CDF: 'FC',
        CHE: 'CHE',
        CHF: 'CHF',
        CHW: 'CHW',
        CLF: 'CLF',
        CLP: '$',
        CNY: '¥',
        COP: '$',
        COU: 'COU',
        CRC: '₡',
        CUC: '$',
        CUP: '₱',
        CVE: '$',
        CZK: 'Kč',
        DJF: 'Fdj',
        DKK: 'kr',
        DOP: 'RD$',
        DZD: 'دج',
        EEK: 'kr',
        EGP: '£',
        ERN: 'Nfk',
        ETB: 'Br',
        ETH: 'Ξ',
        EUR: '€',
        FJD: '$',
        FKP: '£',
        GBP: '£',
        GEL: '₾',
        GGP: '£',
        GHC: '₵',
        GHS: 'GH₵',
        GIP: '£',
        GMD: 'D',
        GNF: 'FG',
        GTQ: 'Q',
        GYD: '$',
        HKD: '$',
        HNL: 'L',
        HRK: 'kn',
        HTG: 'G',
        HUF: 'Ft',
        IDR: 'Rp',
        ILS: '₪',
        IMP: '£',
        INR: '₹',
        IQD: 'ع.د',
        IRR: '﷼',
        ISK: 'kr',
        JEP: '£',
        JMD: 'J$',
        JOD: 'JD',
        JPY: '¥',
        KES: 'KSh',
        KGS: 'лв',
        KHR: '៛',
        KMF: 'CF',
        KPW: '₩',
        KRW: '₩',
        KWD: 'KD',
        KYD: '$',
        KZT: '₸',
        LAK: '₭',
        LBP: '£',
        LKR: '₨',
        LRD: '$',
        LSL: 'M',
        LTC: 'Ł',
        LTL: 'Lt',
        LVL: 'Ls',
        LYD: 'LD',
        MAD: 'MAD',
        MDL: 'lei',
        MGA: 'Ar',
        MKD: 'ден',
        MMK: 'K',
        MNT: '₮',
        MOP: 'MOP$',
        MRO: 'UM',
        MRU: 'UM',
        MUR: '₨',
        MVR: 'Rf',
        MWK: 'MK',
        MXN: '$',
        MXV: 'MXV',
        MYR: 'RM',
        MZN: 'MT',
        NAD: '$',
        NGN: '₦',
        NIO: 'C$',
        NOK: 'kr',
        NPR: '₨',
        NZD: '$',
        OMR: '﷼',
        PAB: 'B/.',
        PEN: 'S/.',
        PGK: 'K',
        PHP: '₱',
        PKR: '₨',
        PLN: 'zł',
        PYG: 'Gs',
        QAR: '﷼',
        RMB: '￥',
        RON: 'lei',
        RSD: 'Дин.',
        RUB: '₽',
        RWF: 'R₣',
        SAR: '﷼',
        SBD: '$',
        SCR: '₨',
        SDG: 'ج.س.',
        SEK: 'kr',
        SGD: 'S$',
        SHP: '£',
        SLL: 'Le',
        SOS: 'S',
        SRD: '$',
        SSP: '£',
        STD: 'Db',
        STN: 'Db',
        SVC: '$',
        SYP: '£',
        SZL: 'E',
        THB: '฿',
        TJS: 'SM',
        TMT: 'T',
        TND: 'د.ت',
        TOP: 'T$',
        TRL: '₤',
        TRY: '₺',
        TTD: 'TT$',
        TVD: '$',
        TWD: 'NT$',
        TZS: 'TSh',
        UAH: '₴',
        UGX: 'USh',
        USD: '$',
        UYI: 'UYI',
        UYU: '$U',
        UYW: 'UYW',
        UZS: 'лв',
        VEF: 'Bs',
        VES: 'Bs.S',
        VND: '₫',
        VUV: 'VT',
        WST: 'WS$',
        XAF: 'FCFA',
        XBT: 'Ƀ',
        XCD: '$',
        XOF: 'CFA',
        XPF: '₣',
        XSU: 'Sucre',
        XUA: 'XUA',
        YER: '﷼',
        ZAR: 'R',
        ZMW: 'ZK',
        ZWD: 'Z$',
        ZWL: '$'
    },
    countryCallingCodeArray: [
        {
            "name": "Afghanistan",
            "dial_code": "+93",
            "code": "AF"
        },
        {
            "name": "Aland Islands",
            "dial_code": "+358",
            "code": "AX"
        },
        {
            "name": "Albania",
            "dial_code": "+355",
            "code": "AL"
        },
        {
            "name": "Algeria",
            "dial_code": "+213",
            "code": "DZ"
        },
        {
            "name": "AmericanSamoa",
            "dial_code": "+1684",
            "code": "AS"
        },
        {
            "name": "Andorra",
            "dial_code": "+376",
            "code": "AD"
        },
        {
            "name": "Angola",
            "dial_code": "+244",
            "code": "AO"
        },
        {
            "name": "Anguilla",
            "dial_code": "+1264",
            "code": "AI"
        },
        {
            "name": "Antarctica",
            "dial_code": "+672",
            "code": "AQ"
        },
        {
            "name": "Antigua and Barbuda",
            "dial_code": "+1268",
            "code": "AG"
        },
        {
            "name": "Argentina",
            "dial_code": "+54",
            "code": "AR"
        },
        {
            "name": "Armenia",
            "dial_code": "+374",
            "code": "AM"
        },
        {
            "name": "Aruba",
            "dial_code": "+297",
            "code": "AW"
        },
        {
            "name": "Australia",
            "dial_code": "+61",
            "code": "AU"
        },
        {
            "name": "Austria",
            "dial_code": "+43",
            "code": "AT"
        },
        {
            "name": "Azerbaijan",
            "dial_code": "+994",
            "code": "AZ"
        },
        {
            "name": "Bahamas",
            "dial_code": "+1242",
            "code": "BS"
        },
        {
            "name": "Bahrain",
            "dial_code": "+973",
            "code": "BH"
        },
        {
            "name": "Bangladesh",
            "dial_code": "+880",
            "code": "BD"
        },
        {
            "name": "Barbados",
            "dial_code": "+1246",
            "code": "BB"
        },
        {
            "name": "Belarus",
            "dial_code": "+375",
            "code": "BY"
        },
        {
            "name": "Belgium",
            "dial_code": "+32",
            "code": "BE"
        },
        {
            "name": "Belize",
            "dial_code": "+501",
            "code": "BZ"
        },
        {
            "name": "Benin",
            "dial_code": "+229",
            "code": "BJ"
        },
        {
            "name": "Bermuda",
            "dial_code": "+1441",
            "code": "BM"
        },
        {
            "name": "Bhutan",
            "dial_code": "+975",
            "code": "BT"
        },
        {
            "name": "Bolivia, Plurinational State of",
            "dial_code": "+591",
            "code": "BO"
        },
        {
            "name": "Bosnia and Herzegovina",
            "dial_code": "+387",
            "code": "BA"
        },
        {
            "name": "Botswana",
            "dial_code": "+267",
            "code": "BW"
        },
        {
            "name": "Brazil",
            "dial_code": "+55",
            "code": "BR"
        },
        {
            "name": "British Indian Ocean Territory",
            "dial_code": "+246",
            "code": "IO"
        },
        {
            "name": "Brunei Darussalam",
            "dial_code": "+673",
            "code": "BN"
        },
        {
            "name": "Bulgaria",
            "dial_code": "+359",
            "code": "BG"
        },
        {
            "name": "Burkina Faso",
            "dial_code": "+226",
            "code": "BF"
        },
        {
            "name": "Burundi",
            "dial_code": "+257",
            "code": "BI"
        },
        {
            "name": "Cambodia",
            "dial_code": "+855",
            "code": "KH"
        },
        {
            "name": "Cameroon",
            "dial_code": "+237",
            "code": "CM"
        },
        {
            "name": "Canada",
            "dial_code": "+1",
            "code": "CA"
        },
        {
            "name": "Cape Verde",
            "dial_code": "+238",
            "code": "CV"
        },
        {
            "name": "Cayman Islands",
            "dial_code": "+ 345",
            "code": "KY"
        },
        {
            "name": "Central African Republic",
            "dial_code": "+236",
            "code": "CF"
        },
        {
            "name": "Chad",
            "dial_code": "+235",
            "code": "TD"
        },
        {
            "name": "Chile",
            "dial_code": "+56",
            "code": "CL"
        },
        {
            "name": "China",
            "dial_code": "+86",
            "code": "CN"
        },
        {
            "name": "Christmas Island",
            "dial_code": "+61",
            "code": "CX"
        },
        {
            "name": "Cocos (Keeling) Islands",
            "dial_code": "+61",
            "code": "CC"
        },
        {
            "name": "Colombia",
            "dial_code": "+57",
            "code": "CO"
        },
        {
            "name": "Comoros",
            "dial_code": "+269",
            "code": "KM"
        },
        {
            "name": "Congo",
            "dial_code": "+242",
            "code": "CG"
        },
        {
            "name": "Congo, The Democratic Republic of the Congo",
            "dial_code": "+243",
            "code": "CD"
        },
        {
            "name": "Cook Islands",
            "dial_code": "+682",
            "code": "CK"
        },
        {
            "name": "Costa Rica",
            "dial_code": "+506",
            "code": "CR"
        },
        {
            "name": "Cote d'Ivoire",
            "dial_code": "+225",
            "code": "CI"
        },
        {
            "name": "Croatia",
            "dial_code": "+385",
            "code": "HR"
        },
        {
            "name": "Cuba",
            "dial_code": "+53",
            "code": "CU"
        },
        {
            "name": "Cyprus",
            "dial_code": "+357",
            "code": "CY"
        },
        {
            "name": "Czech Republic",
            "dial_code": "+420",
            "code": "CZ"
        },
        {
            "name": "Denmark",
            "dial_code": "+45",
            "code": "DK"
        },
        {
            "name": "Djibouti",
            "dial_code": "+253",
            "code": "DJ"
        },
        {
            "name": "Dominica",
            "dial_code": "+1767",
            "code": "DM"
        },
        {
            "name": "Dominican Republic",
            "dial_code": "+1849",
            "code": "DO"
        },
        {
            "name": "Ecuador",
            "dial_code": "+593",
            "code": "EC"
        },
        {
            "name": "Egypt",
            "dial_code": "+20",
            "code": "EG"
        },
        {
            "name": "El Salvador",
            "dial_code": "+503",
            "code": "SV"
        },
        {
            "name": "Equatorial Guinea",
            "dial_code": "+240",
            "code": "GQ"
        },
        {
            "name": "Eritrea",
            "dial_code": "+291",
            "code": "ER"
        },
        {
            "name": "Estonia",
            "dial_code": "+372",
            "code": "EE"
        },
        {
            "name": "Ethiopia",
            "dial_code": "+251",
            "code": "ET"
        },
        {
            "name": "Falkland Islands (Malvinas)",
            "dial_code": "+500",
            "code": "FK"
        },
        {
            "name": "Faroe Islands",
            "dial_code": "+298",
            "code": "FO"
        },
        {
            "name": "Fiji",
            "dial_code": "+679",
            "code": "FJ"
        },
        {
            "name": "Finland",
            "dial_code": "+358",
            "code": "FI"
        },
        {
            "name": "France",
            "dial_code": "+33",
            "code": "FR"
        },
        {
            "name": "French Guiana",
            "dial_code": "+594",
            "code": "GF"
        },
        {
            "name": "French Polynesia",
            "dial_code": "+689",
            "code": "PF"
        },
        {
            "name": "Gabon",
            "dial_code": "+241",
            "code": "GA"
        },
        {
            "name": "Gambia",
            "dial_code": "+220",
            "code": "GM"
        },
        {
            "name": "Georgia",
            "dial_code": "+995",
            "code": "GE"
        },
        {
            "name": "Germany",
            "dial_code": "+49",
            "code": "DE"
        },
        {
            "name": "Ghana",
            "dial_code": "+233",
            "code": "GH"
        },
        {
            "name": "Gibraltar",
            "dial_code": "+350",
            "code": "GI"
        },
        {
            "name": "Greece",
            "dial_code": "+30",
            "code": "GR"
        },
        {
            "name": "Greenland",
            "dial_code": "+299",
            "code": "GL"
        },
        {
            "name": "Grenada",
            "dial_code": "+1473",
            "code": "GD"
        },
        {
            "name": "Guadeloupe",
            "dial_code": "+590",
            "code": "GP"
        },
        {
            "name": "Guam",
            "dial_code": "+1671",
            "code": "GU"
        },
        {
            "name": "Guatemala",
            "dial_code": "+502",
            "code": "GT"
        },
        {
            "name": "Guernsey",
            "dial_code": "+44",
            "code": "GG"
        },
        {
            "name": "Guinea",
            "dial_code": "+224",
            "code": "GN"
        },
        {
            "name": "Guinea-Bissau",
            "dial_code": "+245",
            "code": "GW"
        },
        {
            "name": "Guyana",
            "dial_code": "+595",
            "code": "GY"
        },
        {
            "name": "Haiti",
            "dial_code": "+509",
            "code": "HT"
        },
        {
            "name": "Holy See (Vatican City State)",
            "dial_code": "+379",
            "code": "VA"
        },
        {
            "name": "Honduras",
            "dial_code": "+504",
            "code": "HN"
        },
        {
            "name": "Hong Kong",
            "dial_code": "+852",
            "code": "HK"
        },
        {
            "name": "Hungary",
            "dial_code": "+36",
            "code": "HU"
        },
        {
            "name": "Iceland",
            "dial_code": "+354",
            "code": "IS"
        },
        {
            "name": "India",
            "dial_code": "+91",
            "code": "IN"
        },
        {
            "name": "Indonesia",
            "dial_code": "+62",
            "code": "ID"
        },
        {
            "name": "Iran, Islamic Republic of Persian Gulf",
            "dial_code": "+98",
            "code": "IR"
        },
        {
            "name": "Iraq",
            "dial_code": "+964",
            "code": "IQ"
        },
        {
            "name": "Ireland",
            "dial_code": "+353",
            "code": "IE"
        },
        {
            "name": "Isle of Man",
            "dial_code": "+44",
            "code": "IM"
        },
        {
            "name": "Israel",
            "dial_code": "+972",
            "code": "IL"
        },
        {
            "name": "Italy",
            "dial_code": "+39",
            "code": "IT"
        },
        {
            "name": "Jamaica",
            "dial_code": "+1876",
            "code": "JM"
        },
        {
            "name": "Japan",
            "dial_code": "+81",
            "code": "JP"
        },
        {
            "name": "Jersey",
            "dial_code": "+44",
            "code": "JE"
        },
        {
            "name": "Jordan",
            "dial_code": "+962",
            "code": "JO"
        },
        {
            "name": "Kazakhstan",
            "dial_code": "+77",
            "code": "KZ"
        },
        {
            "name": "Kenya",
            "dial_code": "+254",
            "code": "KE"
        },
        {
            "name": "Kiribati",
            "dial_code": "+686",
            "code": "KI"
        },
        {
            "name": "Korea, Democratic People's Republic of Korea",
            "dial_code": "+850",
            "code": "KP"
        },
        {
            "name": "Korea, Republic of South Korea",
            "dial_code": "+82",
            "code": "KR"
        },
        {
            "name": "Kuwait",
            "dial_code": "+965",
            "code": "KW"
        },
        {
            "name": "Kyrgyzstan",
            "dial_code": "+996",
            "code": "KG"
        },
        {
            "name": "Laos",
            "dial_code": "+856",
            "code": "LA"
        },
        {
            "name": "Latvia",
            "dial_code": "+371",
            "code": "LV"
        },
        {
            "name": "Lebanon",
            "dial_code": "+961",
            "code": "LB"
        },
        {
            "name": "Lesotho",
            "dial_code": "+266",
            "code": "LS"
        },
        {
            "name": "Liberia",
            "dial_code": "+231",
            "code": "LR"
        },
        {
            "name": "Libyan Arab Jamahiriya",
            "dial_code": "+218",
            "code": "LY"
        },
        {
            "name": "Liechtenstein",
            "dial_code": "+423",
            "code": "LI"
        },
        {
            "name": "Lithuania",
            "dial_code": "+370",
            "code": "LT"
        },
        {
            "name": "Luxembourg",
            "dial_code": "+352",
            "code": "LU"
        },
        {
            "name": "Macao",
            "dial_code": "+853",
            "code": "MO"
        },
        {
            "name": "Macedonia",
            "dial_code": "+389",
            "code": "MK"
        },
        {
            "name": "Madagascar",
            "dial_code": "+261",
            "code": "MG"
        },
        {
            "name": "Malawi",
            "dial_code": "+265",
            "code": "MW"
        },
        {
            "name": "Malaysia",
            "dial_code": "+60",
            "code": "MY"
        },
        {
            "name": "Maldives",
            "dial_code": "+960",
            "code": "MV"
        },
        {
            "name": "Mali",
            "dial_code": "+223",
            "code": "ML"
        },
        {
            "name": "Malta",
            "dial_code": "+356",
            "code": "MT"
        },
        {
            "name": "Marshall Islands",
            "dial_code": "+692",
            "code": "MH"
        },
        {
            "name": "Martinique",
            "dial_code": "+596",
            "code": "MQ"
        },
        {
            "name": "Mauritania",
            "dial_code": "+222",
            "code": "MR"
        },
        {
            "name": "Mauritius",
            "dial_code": "+230",
            "code": "MU"
        },
        {
            "name": "Mayotte",
            "dial_code": "+262",
            "code": "YT"
        },
        {
            "name": "Mexico",
            "dial_code": "+52",
            "code": "MX"
        },
        {
            "name": "Micronesia, Federated States of Micronesia",
            "dial_code": "+691",
            "code": "FM"
        },
        {
            "name": "Moldova",
            "dial_code": "+373",
            "code": "MD"
        },
        {
            "name": "Monaco",
            "dial_code": "+377",
            "code": "MC"
        },
        {
            "name": "Mongolia",
            "dial_code": "+976",
            "code": "MN"
        },
        {
            "name": "Montenegro",
            "dial_code": "+382",
            "code": "ME"
        },
        {
            "name": "Montserrat",
            "dial_code": "+1664",
            "code": "MS"
        },
        {
            "name": "Morocco",
            "dial_code": "+212",
            "code": "MA"
        },
        {
            "name": "Mozambique",
            "dial_code": "+258",
            "code": "MZ"
        },
        {
            "name": "Myanmar",
            "dial_code": "+95",
            "code": "MM"
        },
        {
            "name": "Namibia",
            "dial_code": "+264",
            "code": "NA"
        },
        {
            "name": "Nauru",
            "dial_code": "+674",
            "code": "NR"
        },
        {
            "name": "Nepal",
            "dial_code": "+977",
            "code": "NP"
        },
        {
            "name": "Netherlands",
            "dial_code": "+31",
            "code": "NL"
        },
        {
            "name": "Netherlands Antilles",
            "dial_code": "+599",
            "code": "AN"
        },
        {
            "name": "New Caledonia",
            "dial_code": "+687",
            "code": "NC"
        },
        {
            "name": "New Zealand",
            "dial_code": "+64",
            "code": "NZ"
        },
        {
            "name": "Nicaragua",
            "dial_code": "+505",
            "code": "NI"
        },
        {
            "name": "Niger",
            "dial_code": "+227",
            "code": "NE"
        },
        {
            "name": "Nigeria",
            "dial_code": "+234",
            "code": "NG"
        },
        {
            "name": "Niue",
            "dial_code": "+683",
            "code": "NU"
        },
        {
            "name": "Norfolk Island",
            "dial_code": "+672",
            "code": "NF"
        },
        {
            "name": "Northern Mariana Islands",
            "dial_code": "+1670",
            "code": "MP"
        },
        {
            "name": "Norway",
            "dial_code": "+47",
            "code": "NO"
        },
        {
            "name": "Oman",
            "dial_code": "+968",
            "code": "OM"
        },
        {
            "name": "Pakistan",
            "dial_code": "+92",
            "code": "PK"
        },
        {
            "name": "Palau",
            "dial_code": "+680",
            "code": "PW"
        },
        {
            "name": "Palestinian Territory, Occupied",
            "dial_code": "+970",
            "code": "PS"
        },
        {
            "name": "Panama",
            "dial_code": "+507",
            "code": "PA"
        },
        {
            "name": "Papua New Guinea",
            "dial_code": "+675",
            "code": "PG"
        },
        {
            "name": "Paraguay",
            "dial_code": "+595",
            "code": "PY"
        },
        {
            "name": "Peru",
            "dial_code": "+51",
            "code": "PE"
        },
        {
            "name": "Philippines",
            "dial_code": "+63",
            "code": "PH"
        },
        {
            "name": "Pitcairn",
            "dial_code": "+872",
            "code": "PN"
        },
        {
            "name": "Poland",
            "dial_code": "+48",
            "code": "PL"
        },
        {
            "name": "Portugal",
            "dial_code": "+351",
            "code": "PT"
        },
        {
            "name": "Puerto Rico",
            "dial_code": "+1939",
            "code": "PR"
        },
        {
            "name": "Qatar",
            "dial_code": "+974",
            "code": "QA"
        },
        {
            "name": "Romania",
            "dial_code": "+40",
            "code": "RO"
        },
        {
            "name": "Russia",
            "dial_code": "+7",
            "code": "RU"
        },
        {
            "name": "Rwanda",
            "dial_code": "+250",
            "code": "RW"
        },
        {
            "name": "Reunion",
            "dial_code": "+262",
            "code": "RE"
        },
        {
            "name": "Saint Barthelemy",
            "dial_code": "+590",
            "code": "BL"
        },
        {
            "name": "Saint Helena, Ascension and Tristan Da Cunha",
            "dial_code": "+290",
            "code": "SH"
        },
        {
            "name": "Saint Kitts and Nevis",
            "dial_code": "+1869",
            "code": "KN"
        },
        {
            "name": "Saint Lucia",
            "dial_code": "+1758",
            "code": "LC"
        },
        {
            "name": "Saint Martin",
            "dial_code": "+590",
            "code": "MF"
        },
        {
            "name": "Saint Pierre and Miquelon",
            "dial_code": "+508",
            "code": "PM"
        },
        {
            "name": "Saint Vincent and the Grenadines",
            "dial_code": "+1784",
            "code": "VC"
        },
        {
            "name": "Samoa",
            "dial_code": "+685",
            "code": "WS"
        },
        {
            "name": "San Marino",
            "dial_code": "+378",
            "code": "SM"
        },
        {
            "name": "Sao Tome and Principe",
            "dial_code": "+239",
            "code": "ST"
        },
        {
            "name": "Saudi Arabia",
            "dial_code": "+966",
            "code": "SA"
        },
        {
            "name": "Senegal",
            "dial_code": "+221",
            "code": "SN"
        },
        {
            "name": "Serbia",
            "dial_code": "+381",
            "code": "RS"
        },
        {
            "name": "Seychelles",
            "dial_code": "+248",
            "code": "SC"
        },
        {
            "name": "Sierra Leone",
            "dial_code": "+232",
            "code": "SL"
        },
        {
            "name": "Singapore",
            "dial_code": "+65",
            "code": "SG"
        },
        {
            "name": "Slovakia",
            "dial_code": "+421",
            "code": "SK"
        },
        {
            "name": "Slovenia",
            "dial_code": "+386",
            "code": "SI"
        },
        {
            "name": "Solomon Islands",
            "dial_code": "+677",
            "code": "SB"
        },
        {
            "name": "Somalia",
            "dial_code": "+252",
            "code": "SO"
        },
        {
            "name": "South Africa",
            "dial_code": "+27",
            "code": "ZA"
        },
        {
            "name": "South Sudan",
            "dial_code": "+211",
            "code": "SS"
        },
        {
            "name": "South Georgia and the South Sandwich Islands",
            "dial_code": "+500",
            "code": "GS"
        },
        {
            "name": "Spain",
            "dial_code": "+34",
            "code": "ES"
        },
        {
            "name": "Sri Lanka",
            "dial_code": "+94",
            "code": "LK"
        },
        {
            "name": "Sudan",
            "dial_code": "+249",
            "code": "SD"
        },
        {
            "name": "Suriname",
            "dial_code": "+597",
            "code": "SR"
        },
        {
            "name": "Svalbard and Jan Mayen",
            "dial_code": "+47",
            "code": "SJ"
        },
        {
            "name": "Swaziland",
            "dial_code": "+268",
            "code": "SZ"
        },
        {
            "name": "Sweden",
            "dial_code": "+46",
            "code": "SE"
        },
        {
            "name": "Switzerland",
            "dial_code": "+41",
            "code": "CH"
        },
        {
            "name": "Syrian Arab Republic",
            "dial_code": "+963",
            "code": "SY"
        },
        {
            "name": "Taiwan",
            "dial_code": "+886",
            "code": "TW"
        },
        {
            "name": "Tajikistan",
            "dial_code": "+992",
            "code": "TJ"
        },
        {
            "name": "Tanzania, United Republic of Tanzania",
            "dial_code": "+255",
            "code": "TZ"
        },
        {
            "name": "Thailand",
            "dial_code": "+66",
            "code": "TH"
        },
        {
            "name": "Timor-Leste",
            "dial_code": "+670",
            "code": "TL"
        },
        {
            "name": "Togo",
            "dial_code": "+228",
            "code": "TG"
        },
        {
            "name": "Tokelau",
            "dial_code": "+690",
            "code": "TK"
        },
        {
            "name": "Tonga",
            "dial_code": "+676",
            "code": "TO"
        },
        {
            "name": "Trinidad and Tobago",
            "dial_code": "+1868",
            "code": "TT"
        },
        {
            "name": "Tunisia",
            "dial_code": "+216",
            "code": "TN"
        },
        {
            "name": "Turkey",
            "dial_code": "+90",
            "code": "TR"
        },
        {
            "name": "Turkmenistan",
            "dial_code": "+993",
            "code": "TM"
        },
        {
            "name": "Turks and Caicos Islands",
            "dial_code": "+1649",
            "code": "TC"
        },
        {
            "name": "Tuvalu",
            "dial_code": "+688",
            "code": "TV"
        },
        {
            "name": "Uganda",
            "dial_code": "+256",
            "code": "UG"
        },
        {
            "name": "Ukraine",
            "dial_code": "+380",
            "code": "UA"
        },
        {
            "name": "United Arab Emirates",
            "dial_code": "+971",
            "code": "AE"
        },
        {
            "name": "United Kingdom",
            "dial_code": "+44",
            "code": "GB"
        },
        {
            "name": "United States",
            "dial_code": "+1",
            "code": "US"
        },
        {
            "name": "Uruguay",
            "dial_code": "+598",
            "code": "UY"
        },
        {
            "name": "Uzbekistan",
            "dial_code": "+998",
            "code": "UZ"
        },
        {
            "name": "Vanuatu",
            "dial_code": "+678",
            "code": "VU"
        },
        {
            "name": "Venezuela, Bolivarian Republic of Venezuela",
            "dial_code": "+58",
            "code": "VE"
        },
        {
            "name": "Vietnam",
            "dial_code": "+84",
            "code": "VN"
        },
        {
            "name": "Virgin Islands, British",
            "dial_code": "+1284",
            "code": "VG"
        },
        {
            "name": "Virgin Islands, U.S.",
            "dial_code": "+1340",
            "code": "VI"
        },
        {
            "name": "Wallis and Futuna",
            "dial_code": "+681",
            "code": "WF"
        },
        {
            "name": "Yemen",
            "dial_code": "+967",
            "code": "YE"
        },
        {
            "name": "Zambia",
            "dial_code": "+260",
            "code": "ZM"
        },
        {
            "name": "Zimbabwe",
            "dial_code": "+263",
            "code": "ZW"
        }
    ]
};
function getTimeString(previous, timeForToday = false) {

    previous = new Date(previous);
    var msPerMinute = 60 * 1000;
    var msPerHour = msPerMinute * 60;
    var msPerDay = msPerHour * 24;
    var msPerMonth = msPerDay * 30;
    var msPerYear = msPerDay * 365;

    var elapsed = new Date() - previous;
    var onlyTime = new Date(previous).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }).toLowerCase();
    if (elapsed < msPerMinute) {
        if (timeForToday) {
            return onlyTime;
        }
        return Math.round(elapsed / 1000) + ' seconds ago';
    }
    else if (elapsed < msPerHour) {
        if (timeForToday) {
            return onlyTime;
        }
        return Math.round(elapsed / msPerMinute) + ' minutes ago';
    }
    else if (elapsed < msPerDay) {
        if (timeForToday && Math.round(elapsed / msPerHour) < 5) {
            return onlyTime;
        }
        return Math.round(elapsed / msPerHour) + ' hours ago';
    }
    else if (elapsed < msPerMonth) {
        return Math.round(elapsed / msPerDay) + ' days ago';
    }
    else if (elapsed < msPerYear) {
        return Math.round(elapsed / msPerMonth) + ' months ago';
    }
    else {
        return Math.round(elapsed / msPerYear) + ' years ago';
    }
}

var UA_LIC_UTILITY = {

    SAAS_FETCHED_USERS_LIST: {},
    SAAS_FETCHED_USERS_IDS_LIST: {},
    FETCHED_THIS_ADMIN_SHARED_USERS_LIST: null,
    CALL_AFTER_SHARED_PEOPLE_FETCHED: [],
    EXISTING_INVITED_ADMIN_LIST: null,
    CURRENT_SELECTED_ORG_ADMIN_USER_ID: null,
    CREDITS_RT_LISTENER: null,
    CURRENT_USER_DOES_HAVE_ANY_VALID_PLAN: false,
    initialize: async function () {
        try {
            this.startListeningCreditsForUser(currentUser.uid);
            if (extensionName.startsWith('pinnacle') || extensionName.startsWith('telebu') || extensionName.startsWith('karix') || extensionName.startsWith('rivet') || extensionName.startsWith('telnyx')) {
                $("#suo-item-ua-app-lic-plan").remove();
            }
            if (extensionName.startsWith('pinnacle') || extensionName.startsWith('telebu') || extensionName.endsWith('zohobooks') || extensionName.startsWith('karix') || extensionName.startsWith('rivet') || extensionName.startsWith('telnyx') || extensionName.startsWith('whatcetra') || extensionName.startsWith('lookupfor')) {
                $("#saasAuthIDCredit").hide();
            }
            if (extensionName.indexOf("zohobooks") > 0) {
                $("#suo-item-lic-users-manage-users").hide();
                return;
            }
            if (localStorage.getItem('UA_LIC_UTILITY___CURRENT_SELECTED_ORG_ADMIN_USER_ID_' + extensionName + '_' + currentUser.uid)) {
                UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID = localStorage.getItem('UA_LIC_UTILITY___CURRENT_SELECTED_ORG_ADMIN_USER_ID_' + extensionName + '_' + currentUser.uid);
                UA_LIC_UTILITY.hideNavigationsForImpersonatingUser();
                UA_LIC_UTILITY.startListeningCreditsForUser(UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID);
            }
            UA_LIC_UTILITY.getInvitedMeAdminList();
            if (UA_LIC_UTILITY.FETCHED_THIS_ADMIN_SHARED_USERS_LIST !== null) {
                return;
            }
            var resp = await db.collection("ulgebraUsers").doc(currentUser.uid).collection('extensions').doc(extensionName).collection("uaOrgUsers").get();
            UA_LIC_UTILITY.FETCHED_THIS_ADMIN_SHARED_USERS_LIST = {};
            if (resp.docs) {
                resp.docs.forEach(item => {
                    let itemData = item.data();
                    if (!UA_LIC_UTILITY.FETCHED_THIS_ADMIN_SHARED_USERS_LIST[itemData.saasOrgID]) {
                        UA_LIC_UTILITY.FETCHED_THIS_ADMIN_SHARED_USERS_LIST[itemData.saasOrgID] = {};
                    }
                    UA_LIC_UTILITY.FETCHED_THIS_ADMIN_SHARED_USERS_LIST[itemData.saasOrgID][item.id + ''] = itemData;
                });
            }
            $('.sslic-bulk-actions').show();
            UA_LIC_UTILITY.CALL_AFTER_SHARED_PEOPLE_FETCHED.forEach(item => {
                item();
            });
        }
        catch (ex) {
            console.log(ex);
            $('.sslic-bulk-actions').show();
            UA_LIC_UTILITY.CALL_AFTER_SHARED_PEOPLE_FETCHED.forEach(item => {
                item();
            });
        }
    },

    startListeningCreditsForUser: function (userId) {
        if (!userId) {
            return false;
        }
        if (this.CREDITS_RT_LISTENER) {
            this.CREDITS_RT_LISTENER.off();
        }
        if (typeof extensionName === "undefined" || !extensionName) {
            extensionName = queryParams.get("appCode");
        }
        this.CREDITS_RT_LISTENER = firebase.database().ref(`ualic_user_credits/${userId}`);
        this.CREDITS_RT_LISTENER.on('value', function (snapshot) {
            $('.ua-lic-credit-amount').text(snapshot.val() ? Intl.NumberFormat('en-US', {
                notation: "compact",
                maximumFractionDigits: 1
            }).format(Math.ceil(snapshot.val() * 10) / 10) : 0);

            let showPlanShowAlert = snapshot.val() < -50;
            if (UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID) {
                if (currentUser.uid !== UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID) {
                    if (snapshot.val() > 30) {
                        if (!UA_LIC_UTILITY.CURRENT_USER_DOES_HAVE_ANY_VALID_PLAN) {
                            showPlanShowAlert = true;
                        }
                    }
                }
            }
            showPlanShowAlert = false;
            if (showPlanShowAlert) {
                if (extensionName.startsWith('pinnacle') || extensionName.startsWith('telebu') || extensionName.startsWith('karix') || extensionName.startsWith('rivet') || extensionName.startsWith('telnyx') || extensionName.startsWith('whatcetra')) {
                    console.log('debug message ok on credit');
                    return false;
                }
                $('#ua-lic-product-plans').css({ 'z-index': 10000000, 'display': 'block' }).show();
                $("#trial-expire-subscription-dynamic-island").css('display', 'inline-block');
                $('.pmt-ttl-close').hide();
                $('.pricingAlertTipOuter').show();
            } else {
                $('#ua-lic-product-plans').css({ 'z-index': 10000000, 'display': 'block' }).hide();
                $("#trial-expire-subscription-dynamic-island").css('display', 'none');
                $('.pmt-ttl-close').show();
                $('.pricingAlertTipOuter').hide();
            }
        });
    },

    addFetchedUserToUIList: function (userID) {
        var userObj = UA_LIC_UTILITY.SAAS_FETCHED_USERS_LIST[userID + ''];
        $('#SAAS_LIC_USERS_CONFIGURE_LIST').append(
            `<div class="ssf-lic-usr-list-tem ${UA_LIC_UTILITY.FETCHED_THIS_ADMIN_SHARED_USERS_LIST[appsConfig.UA_DESK_ORG_ID] && UA_LIC_UTILITY.FETCHED_THIS_ADMIN_SHARED_USERS_LIST[appsConfig.UA_DESK_ORG_ID].hasOwnProperty(userObj.email) ? ' sslic-cred-shared-user ' : ''}" id="ssflic-user-${userObj.id}">
                                    <div class="sslic-index">
                                        ${$('.ssf-lic-usr-list-tem').length + 1}
                                    </div>
                                    <div class="sslic-pic">
                                        <div class="picletter">${userObj.name.substr(0, 1)}</div>
                                    </div>
                                    <div class="sslic-identity">
                                        <div class="sslic-name">
                                            ${getSafeString(userObj.name)}
                                        </div>
                                        <div class="sslic-email">
                                            ${getSafeString(userObj.email)}
                                        </div>
                                    </div>
                                    <div class="sslic-actions">
                                        <button onclick="UA_LIC_UTILITY.addUserToMyOrganization('${userObj.id}')" class="sslic-act-item sslic-act-add">
                                            Add
                                        </button>
                                        <button class="sslic-act-item sslic-act-wait">
                                            Processing...
                                        </button>
                                        <button onclick="UA_LIC_UTILITY.removeUserToMyOrganization('${userObj.id}')" class="sslic-act-item sslic-act-remove">
                                            Remove
                                        </button>
                                    </div>
                                </div>`);
    },
    CALL_AFTER_SAAS_USERS_FETCHED: [],
    saasUserListFetchCompleted: function () {
        UA_LIC_UTILITY.CALL_AFTER_SAAS_USERS_FETCHED.forEach(item => {
            item();
        });
        try {
            UA_ULGEBRA_CRM_UTILITY.submitAppUserCountForUpdates(Object.keys(UA_LIC_UTILITY.SAAS_FETCHED_USERS_LIST).length, UA_LIC_UTILITY.SAAS_FETCHED_USERS_LIST[UA_SAAS_SERVICE_APP.CURRENT_USER_INFO.id].isAdmin);
        } catch (e) {
            console.log(e);
            UA_ULGEBRA_CRM_UTILITY.submitAppUserCountForUpdates(Object.keys(UA_LIC_UTILITY.SAAS_FETCHED_USERS_LIST).length);
        }
    },

    addFetchedSAASUser: function (id, name, email, photo, isAdmin, phone = null, url = null) {

        id = id + '';

        UA_LIC_UTILITY.SAAS_FETCHED_USERS_LIST[id + ''] = {
            id: id,
            name: name,
            email: email,
            photo: photo,
            isAdmin: isAdmin,
            phone: phone,
            url: url,
            saasOrgID: appsConfig.UA_DESK_ORG_ID
        };

        UA_LIC_UTILITY.SAAS_FETCHED_USERS_IDS_LIST[id + ''] = UA_LIC_UTILITY.SAAS_FETCHED_USERS_LIST[id + ''];
        UA_LIC_UTILITY.SAAS_FETCHED_USERS_LIST[email] = UA_LIC_UTILITY.SAAS_FETCHED_USERS_LIST[id + ''];

        if (UA_LIC_UTILITY.FETCHED_THIS_ADMIN_SHARED_USERS_LIST === null) {
            UA_LIC_UTILITY.CALL_AFTER_SHARED_PEOPLE_FETCHED.push(function () {
                UA_LIC_UTILITY.addFetchedUserToUIList(id);
            });
        } else {
            UA_LIC_UTILITY.addFetchedUserToUIList(id);
        }

    },

    addUserToMyOrganization: async function (userId) {
        userId = userId + '';
        $('#ssflic-user-' + userId + ' .sslic-act-item').hide();
        $('#ssflic-user-' + userId + ' .sslic-act-wait').show();
        var objToAdd = UA_LIC_UTILITY.SAAS_FETCHED_USERS_LIST[userId];
        objToAdd.createdTime = new Date().getTime(); //TODO need to add automatic trigger or firestore rule
        objToAdd.invitedAdminUserID = currentUser.uid;
        objToAdd.invitedAdminUserEmail = currentUser.email;
        objToAdd.invitedAdminUserName = currentUser.displayName;
        objToAdd.invitedAdminUserPic = currentUser.photoURL;
        Object.keys(objToAdd).forEach(item => {
            if (!objToAdd[item]) {
                objToAdd[item] = "null";
            }
        });
        let userEmail = UA_LIC_UTILITY.SAAS_FETCHED_USERS_LIST[userId].email;
        await db.collection("ulgebraUsers").doc(currentUser.uid).collection('extensions').doc(extensionName).collection("uaOrgUsers").doc(userEmail).set(objToAdd);
        await db.collection("uaOrgUsersEmailMap").doc(userEmail).collection('extensions').doc(extensionName).collection('uaOrgAdmins').doc(currentUser.uid).set(objToAdd);
        $('#ssflic-user-' + userId + ' .sslic-act-item').hide();
        $('#ssflic-user-' + userId + ' .sslic-act-remove').show();
        $('#ssflic-user-' + userId).addClass('sslic-cred-shared-user');
    },

    removeUserToMyOrganization: async function (userId) {
        $('#ssflic-user-' + userId + ' .sslic-act-item').hide();
        $('#ssflic-user-' + userId + ' .sslic-act-wait').show();
        await db.collection("ulgebraUsers").doc(currentUser.uid).collection('extensions').doc(extensionName).collection("uaOrgUsers").doc(UA_LIC_UTILITY.SAAS_FETCHED_USERS_LIST[userId].email).delete();
        await db.collection("uaOrgUsersEmailMap").doc(UA_LIC_UTILITY.SAAS_FETCHED_USERS_LIST[userId].email).collection('extensions').doc(extensionName).collection('uaOrgAdmins').doc(currentUser.uid).delete();
        $('#ssflic-user-' + userId + ' .sslic-act-item').hide();
        $('#ssflic-user-' + userId + ' .sslic-act-add').show();
        $('#ssflic-user-' + userId).removeClass('sslic-cred-shared-user');
    },

    getInvitedMeAdminList: async function () {
        let colPath = `uaOrgUsersEmailMap/${currentUser.email}/extensions/${extensionName}/uaOrgAdmins`;
        //console.log('getting user doc for '+currentUser.email+'.. '+colPath);
        let existingInvitedAdminsList = await db.collection(colPath).get();
        UA_LIC_UTILITY.EXISTING_INVITED_ADMIN_LIST = {};
        existingInvitedAdminsList.docs.forEach(item => {
            UA_LIC_UTILITY.EXISTING_INVITED_ADMIN_LIST[item.id] = item.data();
            if (item.data()._ua_lic_adminUserID && item.data().currentSelected) {
                UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID = item.id;
                UA_LIC_UTILITY.startListeningCreditsForUser(UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID);
                UA_LIC_UTILITY.hideNavigationsForImpersonatingUser();
                if (UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID !== localStorage.getItem('UA_LIC_UTILITY___CURRENT_SELECTED_ORG_ADMIN_USER_ID_' + extensionName + '_' + currentUser.uid)) {
                    localStorage.setItem('UA_LIC_UTILITY___CURRENT_SELECTED_ORG_ADMIN_USER_ID_' + extensionName + '_' + currentUser.uid, UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID);
                    window.location.reload();
                }
            }
        });
        try {
            if (!UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID || UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID === currentUser.uid) {
                let adminInvitesCount = Object.keys(UA_LIC_UTILITY.EXISTING_INVITED_ADMIN_LIST).length;
                if (adminInvitesCount > 0) {
                    let adminNotifText = `App Admin: Accept ${adminInvitesCount} invites`;
                    if (adminInvitesCount === 1) {
                        adminNotifText = `App admin: Accept ${UA_LIC_UTILITY.EXISTING_INVITED_ADMIN_LIST[Object.keys(UA_LIC_UTILITY.EXISTING_INVITED_ADMIN_LIST)[0]].email}`;
                    }
                    $('#signedInUserAppAdminHolder').css('display', 'inline-block').text(adminNotifText);
                }
            }
        } catch (ex) {
            console.log(ex);
        }
        if (UA_LIC_UTILITY.callAfterGetInvitedMeAdminList) {
            if (UA_LIC_UTILITY.doNotCallAfterGetInvitedMeAdminListIfDefaultSelected && UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID) {
                return;
            }
            UA_LIC_UTILITY.showExistingInvitedAdminDDHTML();
        }
    },
    callAfterGetInvitedMeAdminList: false,
    doNotCallAfterGetInvitedMeAdminListIfDefaultSelected: false,
    showExistingInvitedAdminDDHTML: function (ignoreIfAlreadySelectedDefault = false, showIfNoneSharedAlert = false) {
        if (!UA_LIC_UTILITY.EXISTING_INVITED_ADMIN_LIST) {
            UA_LIC_UTILITY.callAfterGetInvitedMeAdminList = true;
            UA_LIC_UTILITY.doNotCallAfterGetInvitedMeAdminListIfDefaultSelected = ignoreIfAlreadySelectedDefault;
            return;
        }
        if (ignoreIfAlreadySelectedDefault && UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID) {
            return;
        }
        //            console.log(UA_LIC_UTILITY.EXISTING_INVITED_ADMIN_LIST);
        if (Object.keys(UA_LIC_UTILITY.EXISTING_INVITED_ADMIN_LIST).length > 0) {
            let errWindID = showErroWindow('Choose ' + appPrettyName + ' account for the app', `<div class="ualic-existing-admin-chooser"> Below admins have shared their ${appPrettyName} account with you, choose existing account or configure another account.</div>`, false, false, 5000000000);
            Object.keys(UA_LIC_UTILITY.EXISTING_INVITED_ADMIN_LIST).forEach(item => {
                let adminItem = UA_LIC_UTILITY.EXISTING_INVITED_ADMIN_LIST[item];
                $('#' + errWindID + ' .error-window-detail .ualic-existing-admin-chooser').append(`
                        <div class="ualic-exist-admin-account-item ${adminItem.currentSelected ? ' selected ' : ''}" onclick="UA_LIC_UTILITY.setCurrentMyOrgAdminAccount('${item}')">
                            <div class="ualiceaa-name">${adminItem.invitedAdminUserName}</div>
                            <div class="ualiceaa-email">${adminItem.invitedAdminUserEmail}</div>
                        </div>
                    `);
            });
            $('#' + errWindID + ' .error-window-detail .ualic-existing-admin-chooser').append(`
                        <div class="ualic-exist-admin-account-item my-own-ac-lic-btn" onclick="UA_LIC_UTILITY.removeCurrentSelectedOrgAdminAccount('${errWindID}')">
                            <div class="ualiceaa-name">Use another account</div>
                            <div class="ualiceaa-email">Click to configure</div>
                        </div>
                `);
        } else {
            if (showIfNoneSharedAlert) {
                showErroWindow('No shared access found!', `<div style="
                    background-color: white;
                    padding: 10px 15px;
                    border-radius: 5px;
                ">  You have not been given any shared access. Kindly contact you administrator to give you access by going to <br><br> App Settings => Manage App Users => Allow "${currentUser.email}". </div>`, false, false, 5000000000);
            }
        }
    },

    removeCurrentSelectedOrgAdminAccount: async function (existingConfigWindowID) {
        $('#' + existingConfigWindowID).remove();
        localStorage.removeItem('UA_LIC_UTILITY___CURRENT_SELECTED_ORG_ADMIN_USER_ID_' + extensionName + '_' + currentUser.uid);
        if (UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID) {
            await db.collection("uaOrgUsersEmailMap").doc(currentUser.email).collection('extensions').doc(extensionName).collection('uaOrgAdmins').doc(UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID).update({
                currentSelected: false
            });
            UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', '__multiple_keys__', {
                '_ua_lic_adminUserID': null,
                '_ua_lic_adminUserEmail': null
            }, console.log);
        }
    },

    setCurrentMyOrgAdminAccount: async function (adminUserID) {
        let saveObj = {
            'currentSelected': true,
            '_ua_lic_adminUserID': adminUserID,
            '_ua_lic_adminUserEmail': UA_LIC_UTILITY.EXISTING_INVITED_ADMIN_LIST[adminUserID].invitedAdminUserEmail
        };
        if (UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID) {
            await db.collection("uaOrgUsersEmailMap").doc(currentUser.email).collection('extensions').doc(extensionName).collection('uaOrgAdmins').doc(UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID).update({
                currentSelected: false
            });
        }
        await db.collection("uaOrgUsersEmailMap").doc(currentUser.email).collection('extensions').doc(extensionName).collection('uaOrgAdmins').doc(adminUserID).update(saveObj);
        localStorage.setItem('UA_LIC_UTILITY___CURRENT_SELECTED_ORG_ADMIN_USER_ID_' + extensionName + '_' + currentUser.uid, adminUserID);
        delete saveObj.currentSelected;
        UA_APP_UTILITY.saveAPIKeyInCredentials('tpa', '__multiple_keys__', saveObj, UA_APP_UTILITY.reloadWindow);
    },

    showUserPermissionLICDialog: function () {
        hideUserNavSideBar();
        $(`#UA_LIC_USERS_CONFIG_MODAL`).show();
    },

    hideNavigationsForImpersonatingUser: function () {
        if (UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID && UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID !== currentUser.uid) {
            $("#suo-item-lic-users-manage-users").css('opacity', '0.3');
            $("#suo-item-incoming-nav").css('opacity', '0.3');
            $("#suo-item-workflow-nav").css('opacity', '0.3');
            $('#signedInUserAppAdminHolder').css('display', 'inline-block').text(`App Admin: ${UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID}`);
            if (UA_LIC_UTILITY.EXISTING_INVITED_ADMIN_LIST && UA_LIC_UTILITY.EXISTING_INVITED_ADMIN_LIST[UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID]) {
                $('#signedInUserAppAdminHolder').css('display', 'inline-block').text(`App Admin: ${UA_LIC_UTILITY.EXISTING_INVITED_ADMIN_LIST[UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID].email}`);
            }
        }
    },

    addFetchedUsersToOrgCache: function () {
        try {
            if (!UA_APP_UTILITY.ACTUAL_ORG_COMMON_SETTINGS) {
                UA_APP_UTILITY.CALL_AFTER_ORG_COMMON_SETTING_RESOLVED.push(() => { UA_LIC_UTILITY.addFetchedUsersToOrgCache(); });
                return;
            }
            if (Object.keys(UA_LIC_UTILITY.SAAS_FETCHED_USERS_IDS_LIST).length === 0) {
                UA_LIC_UTILITY.CALL_AFTER_SAAS_USERS_FETCHED.push(UA_LIC_UTILITY.addFetchedUsersToOrgCache);
                return;
            }
            for (var userID in UA_LIC_UTILITY.SAAS_FETCHED_USERS_IDS_LIST) {
                let userObj = UA_LIC_UTILITY.SAAS_FETCHED_USERS_IDS_LIST[userID];
                UA_APP_UTILITY.addToOrgCacheIfNotExists('saas_my_org_members_' + userID, userObj);
            }
        }
        catch (ex) {
            //                setTimeout(UA_LIC_UTILITY.addFetchedUsersToOrgCache, 120000);
            console.log(ex);
        }
    },

    getAskSharedAccessAdminTip: function () {
        return `<div id="ask-shared-access-admin-tip" style="background-color: lightyellow;color: #805f0c;padding: 7px 10px;font-size: 12px;font-weight: normal;border-radius: 5px;margin-left: 2px;text-align: center;margin-bottom: 15px;box-shadow: 0px 1px 2px rgba(0,0,0,0.1);">Continue if you're an admin, otherwise contact your admin to provide access in <b>Manage App Users</b></div>`;
    }
};

UA_FIELD_MAPPPING_UTILITY = {
    FETCHED_RECENT_EVENTS_LIST: {},
    FETCHED_SAVED_FIELD_MAPPINGS: {},
    initialize: function () {
        this.renderInitialElements();
        UA_TPA_FEATURES.setCreateModuleRecordFields = emptyFunction;
        UA_TPA_FEATURES.setViewModuleRecordFields = emptyFunction;
        UA_SAAS_SERVICE_APP.retrieveModuleFieldsForLookUp((fields) => {

        });
        UA_FIELD_MAPPPING_UTILITY.fetchAndRenderSavedFieldMappings();
    },
    renderInitialElements: async function () {
        this.fetchAndRenderSaaSSupportedModules();
        this.fetchAndRenderTPARecentEvents();
    },
    fetchAndRenderSaaSSupportedModules: async function () {
        let supportedModulesHTML = '<option value="-">Select module to load fields</option>';
        Object.keys(UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES).forEach(moduleName => {
            let moduleItem = UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[moduleName];
            supportedModulesHTML += `<option value="${moduleItem.apiName}">${(moduleItem.displayLabel ? moduleItem.displayLabel : moduleName).capitalize()}</option>`;
        });
        $("#select-tpaichan-field-mapping-saas-module-new").html(supportedModulesHTML);
    },
    fetchAndRenderTPARecentEvents: async function () {
        var resp = await db.collection("ulgebraUsers").doc(currentUser.uid).collection('extensions').doc(extensionName).collection('activityLog').doc('incomingWebhooksEvents').collection('tpa').orderBy('ct', 'desc').limit(15).get();
        this.FETCHED_RECENT_EVENTS_LIST = {};
        let latestEventRows = `<option value="-">Select recent event to load fields</option>`;
        if (resp.docs) {
            resp.docs.forEach(item => {
                let itemData = item.data();
                this.FETCHED_RECENT_EVENTS_LIST[item.id + ''] = itemData;
                latestEventRows += `
                            <option value="${item.id}">${itemData.title} (${UA_APP_UTILITY.getTimeString(itemData.ct)})</option>;
                        `;
            });
        }
        if (!resp.docs || resp.docs.length === 0) {
            latestEventRows += `<option value="-">This integration did not receive any events recently. Kindly trigger an event and try again</option>`;
        }
        $("#select-tpaichan-field-latest-event-new").html(latestEventRows);
    },
    jsonpath_readObject: function (object, jsonPath) {
        let parentPath = jsonPath;
        Object.keys(object).forEach(key => {
            let value = object[key];
            let jsonPath = parentPath + "." + key;
            if (Array.isArray(value)) {
                UA_FIELD_MAPPPING_UTILITY.jsonpath_readArray(value, jsonPath);
            }
            else if (typeof value === 'object' && value !== null) {
                UA_FIELD_MAPPPING_UTILITY.jsonpath_readObject(value, jsonPath);
            }
            else { // is a value
                this.jsonpath_pathValueListMap[jsonPath] = value;
                UA_FIELD_MAPPPING_UTILITY.jsonpath_pathList.push(jsonPath);
            }
        });
    },
    jsonpath_pathList: [],
    jsonpath_pathValueListMap: {},
    jsonpath_readArray: function (array, jsonPath) {
        let parentPath = jsonPath;
        for (var i = 0; i < array.length; i++) {
            value = array[i];
            let jsonPath = parentPath + "[" + i + "]";
            if (Array.isArray(value)) {
                UA_FIELD_MAPPPING_UTILITY.jsonpath_readArray(value, jsonPath);
            }
            else if (typeof value === 'object' && value !== null) {
                UA_FIELD_MAPPPING_UTILITY.jsonpath_readObject(value, jsonPath);
            }
            else { // is a value
                this.jsonpath_pathValueListMap[jsonPath] = value;
                UA_FIELD_MAPPPING_UTILITY.jsonpath_pathList.push(jsonPath);
            }
        }
    },
    saasModuleFieldUpdated: function (moduleValue) {
        if (moduleValue === '-') {
            return false;
        }
        let moduleFieldsObj = null;
        if (UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[moduleValue] !== undefined) {
            moduleFieldsObj = UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[moduleValue];
        }
        if (UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[moduleValue + 's'] !== undefined) {
            moduleFieldsObj = UA_SAAS_SERVICE_APP.LOOKUP_SUPPORTED_MODULES[moduleValue + 's'];
        }
        if (!moduleFieldsObj) {
            return false;
        }
        let moduleFieldsList = moduleFieldsObj.fields;
        let moduleMapRows = ``;
        let orderedModuleFieldsList = [];
        let requiredModuleFieldsList = [];
        Object.keys(moduleFieldsList).forEach(fieldName => {
            if (moduleFieldsList[fieldName].system_mandatory) {
                requiredModuleFieldsList.push(fieldName);
            }
        });
        Object.keys(moduleFieldsList).forEach(fieldName => {
            if (moduleFieldsList[fieldName].field_read_only) {
                return false;
            }
            if (requiredModuleFieldsList.includes(fieldName)) {
                return false;
            }
            orderedModuleFieldsList.push(fieldName);
        });
        orderedModuleFieldsList = orderedModuleFieldsList.sort();
        orderedModuleFieldsList = requiredModuleFieldsList.concat(orderedModuleFieldsList);
        orderedModuleFieldsList.forEach(fieldName => {
            let fieldItem = moduleFieldsList[fieldName];
            let picklistFieldItemHtml = "";
            try {
                if (fieldItem.data_type == "picklist" && fieldItem.pick_list_values && fieldItem.pick_list_values.length > 0) {
                    picklistFieldItemHtml = `<div style="position: relative;display: inline-block;width: max-content;margin: 2px 2px 2px 10px;vertical-align: middle;box-sizing: border-box;text-overflow: clip;overflow-wrap: anywhere;font-size: 11px;color: rgb(80,80,80);">
                                                <div onclick="$('#tfmsf-item-field-${UA_APP_UTILITY.getCleanStringForHTMLAttribute(fieldName)}-defaultList').toggle()" style="margin: 2px;vertical-align: middle;box-sizing: border-box;text-overflow: clip;overflow-wrap: anywhere;font-size: 11px;color: rgb(80,80,80);cursor: pointer;box-shadow: 0px 0px 0px 1px #aaa;text-align: center;padding: 2px 10px;border-radius: 4px;">
                                                    Show Available Values
                                                </div>
                                                <div id="tfmsf-item-field-${UA_APP_UTILITY.getCleanStringForHTMLAttribute(fieldName)}-defaultList" style="display:none;position: absolute;background: white;width: max-content;max-width: 200px;border: 1px solid #aaa;border-radius: 4px;padding: 7px;z-index: 1;max-height: 250px;overflow: auto;">`;
                    fieldItem.pick_list_values.forEach((item) => {
                        picklistFieldItemHtml += `<div style="position: relative;display: inline-flex;width: 100%;padding: 2px;background: whitesmoke;margin: 2px 0px;">
                                                <div>${item.display_value}</div>
                                                <div style="margin-left: 10px;">:</div>
                                                <div style="position: relative;margin-left: 20px;">${typeof item.actual_value === "object" ? JSON.stringify(item.actual_value) : item.actual_value}</div>
                                            </div>`
                    });
                    picklistFieldItemHtml += `</div></div>`;
                }
            }
            catch (err) {
                console.log(err);
            }
            moduleMapRows += `
            <div class="tfmsf-row-item" id="tfmsf-item-field-${UA_APP_UTILITY.getCleanStringForHTMLAttribute(fieldName)}">
                <div class="tfmsf-col tpaichan-field-mapping-saas-fields" title="${getSafeString(fieldItem.api_name + ' - ' + fieldItem.display_label)}">
                    ${UA_SAAS_SERVICE_APP.RENDER_RAW_FIELD_ID_IN_FIELDMAPPING ? fieldItem.api_name : fieldItem.display_label} <i>(${fieldItem.data_type})</i> ${fieldItem.system_mandatory ? '<b style="color:crimson">(Required)</b>' : ''} ${fieldItem.field_read_only ? '<b style="color:crimson">(Read-only)</b>' : ''}
                    ${picklistFieldItemHtml ? picklistFieldItemHtml : ""}
                </div>
                <div class="tfmsf-col tpaichan-field-mapping-saas-divider">
                    
                </div>
                <div class="tfmsf-col tpaichan-field-mapping-tpa-fields">
                    <input type="text" class="tpaichanfina" placeholder="Choose a field or type custom value" data-targetFieldName="${fieldName}"/>   
                </div>
            </div>`;
        });
        $("#tfmsf-row-items-holder-new").html(moduleMapRows);
        Array.from(document.querySelectorAll('.tpaichanfina')).forEach(function (element) {
            element.addEventListener('focus', function (e) {
                e.preventDefault();
                $(".tpaichan-tpa-event-fields-dd-outer").attr('data-targetfieldname', $(e.target).attr('data-targetfieldname')).show().css('top', (e.target.getBoundingClientRect().top + window.scrollY - 65));
            });
            element.addEventListener('blur', function (e) {
                e.preventDefault();
                // $(".tpaichan-tpa-event-fields-dd-outer").hide();
            });
        });
        $('.tfmsf-desc-tip, .tfmsf-actions-holder').show();
    },
    tpaRecentFieldEventUpdated: function (recentEventId) {
        if (recentEventId === '-') {
            return false;
        }
        let recentEventObj = this.FETCHED_RECENT_EVENTS_LIST[recentEventId].actualSourceObj;
        let tpaEventFieldsHTML = '';
        this.jsonpath_pathList = [];
        this.jsonpath_pathValueListMap = {};
        this.jsonpath_readObject(recentEventObj, '$');
        UA_FIELD_MAPPPING_UTILITY.jsonpath_pathList.sort().forEach(jsonPathName => {
            tpaEventFieldsHTML += `<div class="tpaif-dditem" data-jsonpathName="${jsonPathName}">${jsonPathName} <i data-jsonpathName="${jsonPathName}">${JSON.stringify(UA_FIELD_MAPPPING_UTILITY.jsonpath_pathValueListMap[jsonPathName])}</i></div>`;
        });

        $("#tpaichan-tpa-event-fields-dd-items").html(tpaEventFieldsHTML);
        Array.from(document.querySelectorAll('.tpaif-dditem')).forEach(function (element) {
            element.addEventListener('click', function (e) {
                e.preventDefault();
                $("#tfmsf-item-field-" + UA_APP_UTILITY.getCleanStringForHTMLAttribute($('.tpaichan-tpa-event-fields-dd-outer').attr('data-targetfieldname')) + ' input.tpaichanfina').val('${{{' + $(e.target).attr('data-jsonpathname') + '}}}');
                $(".tpaichan-tpa-event-fields-dd-outer").hide();
            });
        });
    },
    renderIncomingFieldMappingChannelOptions: function (label, value) {
        $('#fscaf-map-on-select').append(`<option value="${value}">${label}</option>`);
    },
    saveFieldMappingItem: async function () {
        let fieldMapName = $('#fsca-inp-name').val();
        let filterName = $('#fscaf-map-on-select').val();
        let saasFieldModuleName = $('#select-tpaichan-field-mapping-saas-module-new').val();
        let eventJSONUsedForMapping = this.FETCHED_RECENT_EVENTS_LIST[$("#select-tpaichan-field-latest-event-new").val()];
        var el = document.getElementById('fscaf-map-on-select');
        var filterValueLabel = el.options[el.selectedIndex].innerHTML;
        if (fieldMapName.trim().length === 0) {
            showErroWindow("Error!", 'Please fill field mapping name');
            return false;
        }

        if (filterName.endsWith("=")) {
            let filterNameCustomValue = $('#fscaf-map-on-select-other').val().trim();
            if (!valueExists(filterNameCustomValue)) {
                showErroWindow("Error!", 'Enter valid value for when to consider this field mapping on field.');
                return false;
            }
            else {
                filterName = filterName + filterNameCustomValue;
                filterValueLabel = filterValueLabel + ' ' + filterNameCustomValue;
            }
        }

        if (filterName === '-') {
            showErroWindow("Error!", 'Please choose when to consider this field mapping ');
            return false;
        }

        if (saasFieldModuleName === '-') {
            showErroWindow("Error!", 'Select module to configure field mapping');
            return false;
        }

        if (!eventJSONUsedForMapping && !this.CURRENT_EDITING_FIELD_MAP_ID) {
            showErroWindow("Error!", 'Please choose latest event to configure field mapping');
            return false;
        }

        let fieldsMap = {};

        Array.from(document.querySelectorAll('#tpaichan-field-mapping-list-holder .tfmsf-row-item')).forEach(function (element) {
            if (!element.id) {
                return;
            }
            let fieldName = element.id.replace('tfmsf-item-field-', '');
            let fieldValue = $('#' + element.id + ' input.tpaichanfina').val();
            if (!fieldValue || fieldValue.trim().length === 0) {
                return;
            }
            fieldsMap[fieldName] = fieldValue;
        });
        let fieldMappingObject = {
            'name': fieldMapName,
            'fields': fieldsMap,
            'filterValue': filterName,
            'filterValueLabel': filterValueLabel,
            'saasFieldModuleName': saasFieldModuleName,
            'fieldMappingType': 'TPA',
            'ct': new Date().getTime()
        };

        let progId = curId++;
        showTopProgressBar(progId);

        if (!this.CURRENT_EDITING_FIELD_MAP_ID) {
            await db.collection("ulgebraUsers").doc(currentUser.uid).collection('extensions').doc(extensionName).collection('fieldMappings').add(fieldMappingObject).then(res => {
                removeTopProgressBar(progId);
                window.location.reload();
            }).catch(ex => {
                console.log(ex);
                removeTopProgressBar(progId);
            });
        } else {
            await db.collection("ulgebraUsers").doc(currentUser.uid).collection('extensions').doc(extensionName).collection('fieldMappings').doc(this.CURRENT_EDITING_FIELD_MAP_ID).set(fieldMappingObject).then(res => {
                removeTopProgressBar(progId);
                window.location.reload();
            }).catch(ex => {
                console.log(ex);
                removeTopProgressBar(progId);
            });
        }

    },

    fetchAndRenderSavedFieldMappings: async function () {
        let fieldMappingListResponse = await db.collection("ulgebraUsers").doc(currentUser.uid).collection('extensions').doc(extensionName).collection('fieldMappings').orderBy('ct', 'desc').get();
        let savedFieldMappingListHolder = ``;
        if (fieldMappingListResponse.docs) {
            fieldMappingListResponse.docs.forEach(item => {
                let itemData = item.data();
                this.FETCHED_SAVED_FIELD_MAPPINGS[item.id + ''] = itemData;
                savedFieldMappingListHolder += `
                    <div class="fmlitem" id="fmlitem-${item.id}">
                        <div class="fmlitemname">
                            ${getSafeString(itemData.name)}
                        </div>
                        <div class="fmlitemfilter">
                            Consider on <b>${itemData.filterValueLabel}</b>
                        </div>
                        <div class="fmlitemmodule">
                            ${saasServiceName} <b>${itemData.saasFieldModuleName}</b> <span class="fmlitemfieldcount">${Object.keys(itemData.fields).length} fields mapped</span>
                        </div>
                        <div class="fmlitemtime">
                          <span class="fmlitemact" onclick="UA_FIELD_MAPPPING_UTILITY.fieldMappingItemAction('edit', '${item.id}')">Edit</span> <span class="fmlitemact" onclick="UA_FIELD_MAPPPING_UTILITY.fieldMappingItemAction('duplicate', '${item.id}')">Duplicate</span> <span  class="fmlitemact" onclick="UA_FIELD_MAPPPING_UTILITY.fieldMappingItemAction('delete', '${item.id}')">Delete</span>  Created ${UA_APP_UTILITY.getTimeString(itemData.ct)} 
                        </div>
                    </div>
                `;
            });
        }
        $('.fsca-field-map-list-holder').html(savedFieldMappingListHolder.length === 0 ? 'You have no saved field mappings. <b>Create one</b>.' : savedFieldMappingListHolder);
    },

    fieldMappingItemAction: async function (actionType, id) {
        if (actionType === "delete") {
            if (confirm(`Are you sure you want to delete "${UA_FIELD_MAPPPING_UTILITY.FETCHED_SAVED_FIELD_MAPPINGS[id].name}" field mapping?`)) {
                await db.collection("ulgebraUsers").doc(currentUser.uid).collection('extensions').doc(extensionName).collection('fieldMappings').doc(id).delete();
                $('#fmlitem-' + id).remove();
            }
            else {
                return false;
            }
        }
        if (actionType === "duplicate") {
            let originalObject = UA_FIELD_MAPPPING_UTILITY.FETCHED_SAVED_FIELD_MAPPINGS[id];
            originalObject.name = "Duplicate of " + originalObject.name;
            originalObject.filterValue = "---unset---";
            originalObject.filterValueLabel = '* TO BE CONFIGURED *';
            originalObject.ct = new Date().getTime();
            let progId = curId++;
            showTopProgressBar(progId);
            await db.collection("ulgebraUsers").doc(currentUser.uid).collection('extensions').doc(extensionName).collection('fieldMappings').add(originalObject).then(res => {
                removeTopProgressBar(progId);
                window.location.reload();
            }).catch(ex => {
                console.log(ex);
                removeTopProgressBar(progId);
            });
        }
        if (actionType === "edit") {
            let obj = UA_FIELD_MAPPPING_UTILITY.FETCHED_SAVED_FIELD_MAPPINGS[id];
            this.showFieldMappingEditForm(id);
            $('#fsca-inp-name').val(obj.name);
            $('#fscaf-map-on-select').val((!obj.filterValue || obj.filterValue === '---unset---') ? '-' : obj.filterValue);
            var filterValueExistsAlready = false;
            $('#fscaf-map-on-select').each(function () {
                if (this.value === obj.filterValue) {
                    filterValueExistsAlready = true;
                    return false;
                }
            });
            if (!filterValueExistsAlready) {
                $('#fscaf-map-on-select').append(`<option value="${getSafeString(obj.filterValue)}" selected>${getSafeString(obj.filterValueLabel)}</option>`);
            }
            $('#select-tpaichan-field-mapping-saas-module-new').val(obj.saasFieldModuleName);
            UA_FIELD_MAPPPING_UTILITY.saasModuleFieldUpdated(obj.saasFieldModuleName);

            Object.keys(obj.fields).forEach(keyName => {
                try {
                    $('#tfmsf-item-field-' + keyName + ' input.tpaichanfina').val(obj.fields[keyName]);
                }
                catch (ex) { console.log(ex); }
            });

        }
    },

    resetNewMappingFormFields: function () {
        $('#fsca-new-field-map-form form').val('');
        $('#fsca-new-field-map-form select').val('-');
    },

    CURRENT_EDITING_FIELD_MAP_ID: null,

    showFieldMappingEditForm: function (savedID) {
        this.resetNewMappingFormFields();
        $('#fsca-new-field-map-form').show();
        $('#fsca-new-field-map-list').hide();

        if (savedID && UA_FIELD_MAPPPING_UTILITY.FETCHED_SAVED_FIELD_MAPPINGS.hasOwnProperty(savedID)) {
            $('#fieldmapform-ttl').text('Update Field Mapping');
            this.CURRENT_EDITING_FIELD_MAP_ID = savedID;
        }
        else {
            $('#fieldmapform-ttl').text('Create New Field Mapping');
            this.CURRENT_EDITING_FIELD_MAP_ID = null;
        }
    },
    closeFieldMappingEditForm: function () {
        this.CURRENT_EDITING_FIELD_MAP_ID = null;
        $('#fsca-new-field-map-form').hide();
        $('#fsca-new-field-map-list').show();
    },

    fieldMappingCriteriaSelected: function () {
        let selectedval = $("#fscaf-map-on-select").val();
        $('#fscaf-map-on-select-other').toggle(selectedval.endsWith("="));
    }

    // fieldMappingCriteriaSelectOtherBlur: function(){
    //     let selectedval = $("#fscaf-map-on-select").val();
    //     let selectedText = $("#fscaf-map-on-select").find('option:selected').text();
    //     let givenOtherInputVal = $('#fscaf-map-on-select-other').val();
    //     let calculatedVal = selectedval + givenOtherInputVal;
    //     $("#fscaf-map-on-select").append(`<option value="${getSafeString(calculatedVal)}" selected>${selectedText} ${givenOtherInputVal}</option>`);
    // }
}

function deepEqualTwoObjectProperties(x, y) {
    const ok = Object.keys, tx = typeof x, ty = typeof y;
    return x && y && tx === 'object' && tx === ty ? (
        ok(x).length === ok(y).length &&
        ok(x).every(key => deepEqual(x[key], y[key]))
    ) : (x === y);
}

UA_INBOX_UTILITY = {
    getMessagesOfContactNumber: async function (contactNumber) {
        contactNumber = contactNumber.replace(/\D/g, '');
        let dbPath = `ulgebraUsers/${UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID ? UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID : currentUser.uid}/extensions/${extensionName}/iinbox/chats/UA_INBOX_CONTACTS/${contactNumber}/messages`;
        let messages = {};
        await db.collection(dbPath).orderBy('_uaCreatedTime', 'desc').get().then(colResp => {
            if (colResp.docs) {
                colResp.docs.forEach(item => {
                    item = item.data();
                    messages[item._id] = item;
                });
            }
        });
        return messages;
    },

    addOutgoingMessageToUAInbox: async function (conversationId, contactId, messageId, messageData, contactIdType = "random") {
        userId = UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID ? UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID : currentUser.uid;
        originService = 'ulgebra_chat_ui';
        let inboxData = {
            userId: userId,
            originService: originService,
            conversationId: conversationId,
            contactId: contactId,
            messageId: messageId,
            messageData: messageData,
            contactIdType: contactIdType
        };
        firebase.functions().httpsCallable('new_callServiceAuthorizedAPI')({ appCode: extensionName, TPAService: extensionName.split("for")[0], action: "UA_ADD_OUTGOING_MESSAGE_INBOX", data: inboxData, _ua_lic_adminUserID: UA_LIC_UTILITY.CURRENT_SELECTED_ORG_ADMIN_USER_ID }).then((response) => { console.log(response); });
    }
};

UA_ULGEBRA_CRM_UTILITY = {

    USER_PROFILE_DATA_CLOUD: null,

    initialize: async function () {
        let existingUserProfileResp = await firebase.firestore().collection("ulgebraUsers").doc(currentUser.uid).get();
        UA_ULGEBRA_CRM_UTILITY.USER_PROFILE_DATA_CLOUD = existingUserProfileResp.data();
        if (!UA_ULGEBRA_CRM_UTILITY.USER_PROFILE_DATA_CLOUD || !Object.keys(UA_ULGEBRA_CRM_UTILITY.USER_PROFILE_DATA_CLOUD).length === 0) {
            setTimeout(UA_ULGEBRA_CRM_UTILITY.initialize, 3000);
            console.log(' unable to resolve USER_PROFILE_DATA_CLOUD, trying after 3secs');
            return false;
        }
        UA_ULGEBRA_CRM_UTILITY.updateAuthorizedAppCode();
        UA_ULGEBRA_CRM_UTILITY.checkForPhoneIfNotRequire();
        UA_ULGEBRA_CRM_UTILITY.submitRegionForUpdates();
    },

    updateAuthorizedAppCode: function () {
        if (!UA_ULGEBRA_CRM_UTILITY.USER_PROFILE_DATA_CLOUD.inAppCodes) {
            UA_ULGEBRA_CRM_UTILITY.USER_PROFILE_DATA_CLOUD.inAppCodes = [];
        }
        if (!UA_ULGEBRA_CRM_UTILITY.USER_PROFILE_DATA_CLOUD.inAppCodes.includes(extensionName)) {
            UA_ULGEBRA_CRM_UTILITY.USER_PROFILE_DATA_CLOUD.inAppCodes.push(extensionName);
            UA_ULGEBRA_CRM_UTILITY.performCRMUpdateSchemaChange({
                'inAppCodes': UA_ULGEBRA_CRM_UTILITY.USER_PROFILE_DATA_CLOUD.inAppCodes
            });
        }
    },

    submitPhoneNumberForUpdates: function () {
        $('#UA_LIC_CRM_PHONE_CONFIG_MODAL').hide();
        let phNumber = $('#ua-crm-da-phnumber').val().replace(/[^0-9]/g, '');
        if (valueExists(phNumber.trim())) {
            UA_ULGEBRA_CRM_UTILITY.performCRMUpdateSchemaChange({
                'ph': phNumber
            });
        }
        else {
            $('#ua-crm-da-phnumber').after('</br><div style="color:red">Enter valid number</div>');
            $('#UA_LIC_CRM_PHONE_CONFIG_MODAL').show();
            $('#ua-crm-da-phnumber').focus();
        }
    },

    submitRegionForUpdates: function () {
        if (UA_ULGEBRA_CRM_UTILITY.USER_PROFILE_DATA_CLOUD.timezoneText !== Intl.DateTimeFormat().resolvedOptions().timeZone) {
            UA_ULGEBRA_CRM_UTILITY.performCRMUpdateSchemaChange({
                'timezoneText': Intl.DateTimeFormat().resolvedOptions().timeZone
            });
        }
    },

    submitAppUserCountForUpdates: function (newCount, isSAASAdmin = false) {
        if (!newCount) {
            return false;
        }
        if (newCount > 0) {
            $("#quan-count").val(newCount);
        }
        if (!UA_ULGEBRA_CRM_UTILITY.USER_PROFILE_DATA_CLOUD || !UA_ULGEBRA_CRM_UTILITY.USER_PROFILE_DATA_CLOUD.ct) {
            setTimeout((newCount, isSAASAdmin) => { UA_ULGEBRA_CRM_UTILITY.submitAppUserCountForUpdates(newCount, isSAASAdmin); }, 5000);
            return false;
        }
        if (newCount) {
            if (UA_ULGEBRA_CRM_UTILITY.USER_PROFILE_DATA_CLOUD.appUsCount <= newCount && UA_ULGEBRA_CRM_UTILITY.USER_PROFILE_DATA_CLOUD.isSAASAdmin === isSAASAdmin) {
                return true;
            }
            UA_ULGEBRA_CRM_UTILITY.performCRMUpdateSchemaChange({
                'appUsCount': newCount,
                'isSAASAdmin': isSAASAdmin ? isSAASAdmin : false
            });
        }
    },

    checkForPhoneIfNotRequire: async function () {
        if (!UA_ULGEBRA_CRM_UTILITY.USER_PROFILE_DATA_CLOUD || !UA_ULGEBRA_CRM_UTILITY.USER_PROFILE_DATA_CLOUD.ct) {
            return false;
        }
        if (!UA_ULGEBRA_CRM_UTILITY.USER_PROFILE_DATA_CLOUD.ph || !valueExists(UA_ULGEBRA_CRM_UTILITY.USER_PROFILE_DATA_CLOUD.ph.replace(/[^0-9]/g, ''))) {
            $('#UA_LIC_CRM_PHONE_CONFIG_MODAL').show();
            $('#ua-crm-da-phnumber').focus();
        }
    },

    updateAppsConfig: function () {

    },

    updateUsersCount: function () {

    },

    performCRMUpdateSchemaChange: async function (updatedData) {
        await firebase.firestore().collection("ulgebraUsers").doc(currentUser.uid).set(updatedData, { merge: true });
        console.log('New User schema has been updated.');
    }
};
