var extensionId= "haphhpfcpfeagcmannpebjjjpdlbhflh";
var inExtensionId= "njeihbodihkkobpnmekmkplikbkmehed";
var inWatiExtensionId = "bbikfkealmdmdfmembclkahlkpgkjbjm";
var waExtensionId= "bjnjgehlejjebdcpfcdickaaiignnijd";
var isExInstalled = false;
var isInExtInstalled = false;
var userEmail="";
var mobileNumberSettings={};
var zsckey = "";
var serviceOrigin="";
document.addEventListener("DOMContentLoaded", function(event) {
	const urlParams = new URLSearchParams(window.location.search);
	const tab = urlParams.get('tab');
	serviceOrigin = urlParams.get('serviceOrigin'); 
	if(tab == "web"){
		defaultModules = ["Leads","Contacts","Accounts"];
	} 
	ZOHO.embeddedApp.init().then(function(){
		ZOHO.CRM.CONFIG.getCurrentUser().then((res)=>{
			currentUser = res['users'][0]; 
			userEmail = res['users'][0].email;
			checkExtension();
			checkInExtension();
			checkInWatiExtension();
			checkWaExtension();
			connectNow();
		});
		ZOHO.CRM.API.getOrgVariable("whatsappforzohocrm__mobileNumberSettings").then(function(resp){
			if(resp && resp.Success && resp.Success.Content && resp.Success.Content != "0"){
				mobileNumberSettings =JSON.parse(resp.Success.Content);
				document.getElementById("incomingUserType").checked= false ;
				if(mobileNumberSettings["isModule"]){
					document.getElementById("incomingUserType").checked= true;
				}
				updateIncomingUserType();
				if(mobileNumberSettings["module"]){
					document.getElementById("module").value=mobileNumberSettings["module"] ;
				}
				if(mobileNumberSettings["lar_id"]){
					document.getElementById("lar_id").value=mobileNumberSettings["lar_id"] ;
				}
				if(mobileNumberSettings["Lead_Source"]){
					document.getElementById("lead_source").value=mobileNumberSettings["Lead_Source"] ;
				}
			}	
			loadUsersMap();

		});
		var getmap = {"nameSpace":"<portal_name.extension_namespace>"};
		ZOHO.CRM.CONNECTOR.invokeAPI("crm.zapikey",getmap).then(function(resp){
			zsckey = JSON.parse(resp).response;
			showWebhookURL();
		});
		ZOHO.CRM.META.getModules().then(function(modulesData){
       		modulesData.modules.forEach(function(module){
       			if(module.api_name == "whatsappforzohocrm__WhatsApp_History"){
       				document.getElementById("historylink").href = serviceOrigin+"/crm/tab/"+module.module_name;
       			}
       			else if(module.api_name == "whatsappforzohocrm__WhatsApp_Templates"){
       				document.getElementById("templatelink").href = serviceOrigin+"/crm/tab/"+module.module_name;
       			}
       		});
       	});		
	});
});
function saveUsersMap(userId){
	var value;
	if(userId == "newchat"){
		value = document.getElementById(userId).value;
	}
	else{
		value = document.getElementById("usersMap"+userId).value;
	}
	if(mobileNumberSettings && !mobileNumberSettings.usersMap){
		mobileNumberSettings.usersMap={};
	}
	mobileNumberSettings.usersMap[userId]=value;
	updateOrgVariables("whatsappforzohocrm__mobileNumberSettings",mobileNumberSettings);
}
function loadUsersMap(){
	ZOHO.CRM.API.getAllUsers({Type:"ActiveUsers"}).then(function(data){
		var dropdown = "";
		var usersMap=mobileNumberSettings.usersMap;
		if(!usersMap){
			usersMap={};
		}
	    $('#usersmap').append('<div style="margin-top:15px;"><div class="ib">Assign New WhatCetra Chats</div><i class="ib arrow material-icons">trending_flat</i><select id="newchat"></select><span class="savebtn" onclick="saveUsersMap('+"'newchat'"+')">Save<span></div>');
	    var usersOption='<option value="">Select user</option>';
	    $('#usersmap').append('<div> <div class="ib"><h4><u>Zoho CRM users</u></h4></div><i  class="ib arrow material-icons">trending_flat</i><div class="ib"><h4><u>WhatCetra Users</u></h4></div></div>');
	    data.users.forEach(function(user){
	    	if(user.status == "active"){
	    		$('#usersmap').append('<div><div class="ib">'+user.email+'</div><i class="ib arrow material-icons">trending_flat</i><input class="ib" type="text" id="usersMap'+user.id+'"></input><span class="savebtn" onclick="saveUsersMap('+"'"+user.id+"'"+')">Save<span></div>');
		    	if(usersMap && usersMap && usersMap[user.id] && usersMap[user.id]){
		    		document.getElementById('usersMap'+user.id).value = usersMap[user.id];
		    	}
		    	else{
		    		document.getElementById('usersMap'+user.id).value = "";
		    	}
		    	usersOption = usersOption+"<option value="+user.id+">"+user.email+"</option>";
		    }	
	    });
	    if(usersOption){
	    	$('#newchat').append(usersOption);
	    }
	    if(usersMap.newchat){
	    	document.getElementById('newchat').value = usersMap.newchat;
	    }
	});	
}
function checkExtension(){
	chrome.runtime.sendMessage(extensionId, { "message": "version" },function (reply){
		console.log(reply);
		if(reply && reply.version){
			$(".Installed").show();
			$(".notInstalled").hide();
			$(".wwinstall").hide();
			
		}
	});
}
function checkInExtension(){
	chrome.runtime.sendMessage(inExtensionId, { "message": "version" },function (reply){
		console.log(reply);
		if(reply && reply.version){
			$(".insideInstalled").show();
			$(".insideNotInstalled").hide();
			$(".insideinstall").hide();
			
		}
	});
}
function checkInWatiExtension(){
	chrome.runtime.sendMessage(inWatiExtensionId, { "message": "version" },function (reply){
		console.log(reply);
		if(reply && reply.version){
			$(".insideInstalled").show();
			$(".insideNotInstalled").hide();
			$(".insideinstall").hide();
			
		}
	});
}
function checkWaExtension(){
	chrome.runtime.sendMessage(waExtensionId, { "message": "version" },function (reply){
		console.log(reply);
		if(reply && reply.version){
			$(".waInstalled").show();
			$(".waNotInstalled").hide();
			$(".wainstall").hide();
			
		}
	});
}
function connectNow(isWati){
	let extId = isWati == "true"?inWatiExtensionId:inExtensionId;
	if(!userEmail){
		alert("Please try again");
		return;
	}
	$('#connect-btn').text('Authorizing...');
	
	$('.zinside-btn').text('Authorized...');
	$('.zinside-btn').text('Connecting Chrome Extension...');
	var userDomainURL = window.location.ancestorOrigins[0];
	userDomainURL = userDomainURL.replace("crmplus", "crm");
	chrome.runtime.sendMessage(extId, {"zsckey" : zsckey, "uemail" : userEmail, "crmdomain":userDomainURL}, function(response) {
		console.log(response);
		if (!response || !response.connected){
		  console.log('Error, Try again ');
		}
		else{
			$(".insideConnected").show();
			$(".insideNotConnected").hide();
			$(".insideConnect").hide();
		}
	});
}
function saveIncomingSettings(value,key){
	mobileNumberSettings[key]=value;
	updateOrgVariables("whatsappforzohocrm__mobileNumberSettings",mobileNumberSettings);
}
function saveLeadSource(value){
	saveIncomingSettings(value,"Lead_Source");
}
function saveAssignmentId(value){
	saveIncomingSettings(value,"lar_id");
}
async function updateOrgVariables(apiname,value){
	$("#mwind").show();
	$("#mwind").text('Configurations are Saving...');
	await ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": apiname,"value": value});
	$("#mwind").text('Configurations are Saved Successfully');
	setTimeout(function(){$("#mwind").hide(); }, 1000);

}
function updateIncomingUserType(){
    if(document.getElementById('incomingUserType').checked){
        $("#newuserselect-opt").css({'display': 'inline-block'});
    }else{
        $("#newuserselect-opt").hide();
    }
}

function showWebhookURL(){
	
	var domain="";
	if(window.location.ancestorOrigins && window.location.ancestorOrigins[0]){
       let baseUrl = window.location.ancestorOrigins[0];
       domain = baseUrl.substring(baseUrl.indexOf(".zoho.")+".zoho.".length);
    }
    if(window.location.ancestorOrigins && window.location.ancestorOrigins[0].indexOf("zohosandbox.com") != -1){
        domain = "https://plugin-whatsappforzohocrm.zohosandbox.com/";
    }
    else{
        domain = "https://platform.zoho."+domain.trim();
    }
	document.getElementById("Webhookurl").value=domain+"/crm/v2/functions/whatsappforzohocrm__whatcetra_notifications/actions/execute?auth_type=apikey&zapikey="+zsckey;
}
