// Replace with your publishable key
// https://dashboard.stripe.com/apikeys
const STRIPE_PUBLISHABLE_KEY = 'pk_live_vZ8o9dQoM2zharvDfdExWgZf00GIVF1VvZ';

// Replace with your tax ids
// https://dashboard.stripe.com/tax-rates
var taxRates = ['txr_1Hv2S0Ho9p6j6FgR0qRizWPw'];
var selectedProductId = null;
var selectedCurrency =  new Date().toString().indexOf("India") >0 ? "inr" : "usd";
var selectedPaymentPlanId = null;
var currencyMap = {
  "inr": "₹"  ,
  "usd": "$",
  "eur": "€",
  "gbp": "£",
  "aud": "A$"
};
var selectedSubType = "recurring";
// Replace with your Firebase project config.
// var firebaseConfig = {
//        apiKey: "AIzaSyDsvl4lleRa8k-3UuDqltueXZy1V27f0Sw",
//        authDomain: "ulgebra-license.firebaseapp.com",
//        databaseURL: "https://ulgebra-license.firebaseio.com",
//        projectId: "ulgebra-license",
//        storageBucket: "ulgebra-license.appspot.com",
//        messagingSenderId: "356364764905",
//        appId: "1:356364764905:web:4bea988d613e73e1805278",
//        measurementId: "G-54FN510HDW"
//      };
//      firebase.initializeApp(firebaseConfig);

      var pricesObj = {};

// Replace with your cloud functions location
const functionLocation = 'us-central1';

// Initialize Firebase
//const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();

/**
 * Firebase Authentication configuration
 */
const firebaseUI = new firebaseui.auth.AuthUI(firebase.auth());
const firebaseUiConfig = {
  callbacks: {
    signInSuccessWithAuthResult: function (authResult, redirectUrl) {
      // User successfully signed in.
      // Return type determines whether we continue the redirect automatically
      // or whether we leave that to developer to handle.
      return true;
    },
    uiShown: () => {
      document.querySelector('#loader').style.display = 'none';
    },
  },
  signInFlow: 'popup',
  signInSuccessUrl: '/store'+window.location.search,
  signInOptions: [
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    firebase.auth.EmailAuthProvider.PROVIDER_ID,
  ],
  credentialHelper: firebaseui.auth.CredentialHelper.NONE,
  // Your terms of service url.
  tosUrl: 'https://apps.ulgebra.com/terms',
  // Your privacy policy url.
  privacyPolicyUrl: 'https://apps.ulgebra.com/privacy-policy',
};
var enableTrialModesForPay = true;
firebase.auth().onAuthStateChanged((firebaseUser) => {
  if (firebaseUser) {
      $('.signedin').show();
      $('.product-container').css({'display': 'inline-flex'});
    document.querySelector('#loader').style.display = 'none';
    //document.querySelector('main').style.display = 'block';
    currentUser = firebaseUser.uid;
    userSignedIn(firebaseUser);
    startDataListeners();
    fetchExistingLicenseOwnersList();
    showUserCredits();
    if(currentUser === "mKHY4LuTiDf53TYWBfKBeJkP91U2" || currentUser === "bFpolQyCMWZn1XcgP5gTLkLJSy93" || currentUser === "hQ7TqwxcxXM9Iey12sxYk43ezCb2" || currentUser === "c25Bpr10MXWSg7pRIgeH7msOoRB3" || currentUser === "bFpolQyCMWZn1XcgP5gTLkLJSy93" || currentUser === "c25Bpr10MXWSg7pRIgeH7msOoRB3" || currentUser === "VnhHx6o4buTAJ8H79RZTRzretVv1" || currentUser === "YHlDQbCKf4MKAW1wM0tcYoKKgBD3"){
      $('#lic-imperson-holder').show();
      isAdminUsers = true;
    }
  } else {
       $('.signedin').hide();
       $('.product-container').hide();
//    document.querySelector('main').style.display = 'none';
    firebaseUI.start('#firebaseui-auth-container', firebaseUiConfig);
  }
});

var isAdminUsers = false;
function userSignedIn(user){
    $("#userimg").attr({'src': user.photoURL});
    $("#username").text(user.displayName);
    $('#currencySelector').val(selectedCurrency);
}
/**
 * Data listeners
 */

 function startDataListeners_old() {
      firebase.database().ref(`ualic_user_credits/${currentUser}`).on('value', function(snapshot){
        $('#ua-lic-credit-amount').html(snapshot.val() ? Math.ceil(snapshot.val() * 10)/10 : 0);
      });
      db.collection('customers').doc(currentUser).collection('subscriptions').get().then(function (querySnapshot) {

          if(!querySnapshot.docs.length) {
              console.log('empty');
          }
          querySnapshot.forEach(async function (doc) {
              //console.log(product);
              let product_Id = doc.data().product.slice(9);
              db.collection('products').doc(product_Id).get().then(function (doc) {
                    $('.subcribedProductListHolder').append(`<div id="subId_${productId}" data-quantity="${doc.data().quantity}" onclick="subcribedProductId(event, '${productId}', '${doc.id}')" class="product-item">
                    <div class="product-name">
                        <img src="${doc.data().images[0]}"/> ${doc.data().name}
                    </div>
                    <div class="plan-name">
                       Click to Add users
                    </div>
                </div>`);
              });

        });
        });

}


function singleDigitChange(num) {
  return (num+'').length == 1 ? '0'+num : num;
}

var transferLoading = `<div class="loaderOuterDiv">

<div class="wcMsgLoadingInner" title="loading…"><svg class="wcMsgLoadingSVG" width="17" height="17" viewBox="0 0 46 46" role="status"><circle class="wcMsgLoadingSvgCircle" cx="23" cy="23" r="20" fill="none" stroke-width="6" style="stroke: rgb(178 53 119);"></circle></svg></div>
                            </div>`;
var lastTransferVisible = '';
var lastTransferVisibleWA = '';
var transferCreditsArr = {};
var waTransEmptyCheck = false;
var TransEmptyCheck = false;

async function showUserCredits() {


  $(".addIntegaration_outerBodyH").find('.outerTransferCreditsShowN').remove();

  if($('.outerTransferCreditsShow').is(':visible'))
  $('.addIntegaration_outerBodyH').append(transferLoading);
  $('.addIntegaration_outerBodyH').scrollTop($('.addIntegaration_outerBodyH')[0].scrollHeight);

  await db.collection('UALicenseOwners').doc(currentUser).collection('credit_adjustments').orderBy('ct', "desc").startAfter(lastTransferVisible).limit(25).get().then(async function (doc) {

    await db.collection('UALICTOWCUserCreditActivity').doc(currentUser).collection('activities').orderBy('ct', "desc").startAfter(lastTransferVisibleWA).limit(25).get().then(async function (ques) {

        if(ques && ques.docs && ques.docs.length) {

          lastTransferVisibleWA = ques.docs[ques.docs.length - 1];

          ques.docs.forEach(function(resp) {

            resp = resp.data();
            transferCreditsArr[resp.ct] = resp;

          });

          if(ques.docs.length == 25)
          TransEmptyCheck = true;

        }

        if(doc && doc.docs && doc.docs.length) {

           lastTransferVisible = doc.docs[doc.docs.length - 1];

           doc.docs.forEach(function(resp) {

              resp = resp.data();
              transferCreditsArr[resp.ct] = resp;

           });

           if(doc.docs.length == 25)
           waTransEmptyCheck = true;

        }

        await showUserCreditsMain(transferCreditsArr)

    });
        
  });

}

async function showUserCreditsMain(tranObj) {

  tranObj = Object.keys(tranObj).sort().reverse().reduce((obj, key) => {
      obj[key] = tranObj[key]
      return obj
  }, {});

  if(tranObj && Object.keys(tranObj).length) {


   $(".addIntegaration_outerBodyH").find('.loaderOuterDiv').remove();

   let checkEndRecord = 0;
   Object.keys(tranObj).forEach(function(tranKey) {

      checkEndRecord++;
      resp = tranObj[tranKey];
      value = new Date(resp.ct);
      toTimeSet = singleDigitChange(value.toLocaleTimeString().split(':')[0])+':'+value.toLocaleTimeString().split(':')[1]+' '+value.toLocaleTimeString().split(':')[2].slice(3);
      getTime = singleDigitChange(value.getDate())+'/'+singleDigitChange(value.getMonth()+1)+'/'+value.getFullYear()+' &nbsp;'+toTimeSet;
      diffCredit = Number(resp.new) - Number(resp.old);
      if(Number(resp.old) != Number(resp.new))
      $('.addIntegaration_outerBodyH').append(`<div class="outerTransferCreditsShow">
                      <div class="outerTransferCreditsShowM1">
                        <div class="outerTransferCreditsShowM1From">${resp.old}</div>
                        <div class="outerTransferCreditsShowM1Svg">
                            ${diffCredit <= 0 ? '<div class="outerTransferCreditsShowM1SvgM outerTransferCreditsShowM1SvgMr">&#8640;</div><div class="outerTransferCreditsShowM1SvgTSub outerTransferCreditsShowM1SvgTSubre">'+diffCredit+'</div>' : '<div class="outerTransferCreditsShowM1SvgTAdd">+'+diffCredit+'</div><div class="outerTransferCreditsShowM1SvgM">&#8641;</div>'} 
                        </div>
                        <div class="outerTransferCreditsShowM1To">${resp.new}</div>
                      </div>
                      <div class="outerTransferCreditsShowM2">
                        ${resp.hint ? `<div class="outerTransferCreditsShowM2hint">${resp.hint}</div>` : `<div class="outerTransferCreditsShowM2Fstatus">
                          <div class="outerTransferCreditsShowM2FstatusM">${resp.oldStatus}</div>
                            ${resp.oldSub.items ? `<div class="outerTransferCreditsShowM2FstatusT">
                              <div class="outerTransferCreditsShowM2FstatusT1"><i>${resp.oldSub.items[0].price.product.name}</i><br><b>${resp.oldSub.items[0].price.nickname}</b> × <b>${resp.oldSub.items[0].quantity}</b></div></div>` : ''}                                    
                          
                          </div>
                        <div class="outerTransferCreditsShowM2Gstatus"></div>
                        <div class="outerTransferCreditsShowM2Tstatus">
                          <div class="outerTransferCreditsShowM2TstatusM">${resp.newStatus}</div>
                            ${resp.newSub.items ? `<div class="outerTransferCreditsShowM2TstatusT">
                              <div class="outerTransferCreditsShowM2TstatusT1"><i>${resp.newSub.items[0].price.product.name}</i><br><b>${resp.newSub.items[0].price.nickname}</b> × <b>${resp.newSub.items[0].quantity}</b></div></div>` : ''}                                    
                          
                        </div>`}
                        
                        </div>
                      <div class="outerTransferCreditsShowM3">
                        <div class="outerTransferCreditsShowM3T">${getTime}</div>
                        <div class="outerTransferCreditsShowM3Svg"><svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 15 15" height="15px" viewBox="0 0 24 24" width="15px" fill="#cdc2d0"><g><path d="M0,0h24v24H0V0z" fill="none"/></g><g><path d="M11.99,2C6.47,2,2,6.48,2,12s4.47,10,9.99,10C17.52,22,22,17.52,22,12S17.52,2,11.99,2z M15.29,16.71L11,12.41V7h2v4.59 l3.71,3.71L15.29,16.71z"/></g></svg></div>
                      
                    </div>
                  </div>`);
      else if(Object.keys(tranObj).length == checkEndRecord) {

        if($('.outerTransferCreditsShow').is(':visible')) {
          $(".addIntegaration_outerBodyH").find('.loaderOuterDiv').remove();
          $(".addIntegaration_outerBodyH").find('.outerTransferCreditsShowN').remove();
        }
        else {
          $('.addIntegaration_outerBodyH').html(`<div class="outerTransferCreditsShowE">Empty</div>`);  
        }
        

      }

   });

   if(!waTransEmptyCheck && !TransEmptyCheck) {}
   else {
      $('.addIntegaration_outerBodyH').append(`<div class="outerTransferCreditsShowN" onclick="showUserCredits()">Next</div>`); 
   }
   

}
else {
  if($('.outerTransferCreditsShow').is(':visible')) {
    $(".addIntegaration_outerBodyH").find('.loaderOuterDiv').remove();
    $(".addIntegaration_outerBodyH").find('.outerTransferCreditsShowN').remove();
  }
  else {
    $('.addIntegaration_outerBodyH').html(`<div class="outerTransferCreditsShowE">Empty</div>`);
  }
  
}


}

function viewMySubcribed(selected) {

  if($(selected).attr('data-checked') == 1) {
        $(selected).attr('data-checked', 0);
        $(selected).removeClass('subcribed-portal-buttonTrue');
        $('#selectProductsContainer').show();
        $('#subcribedProductsContainer').hide();
  }
  else {
      $(selected).attr('data-checked', 1);
      $(selected).addClass('subcribed-portal-buttonTrue');
      $('#selectProductsContainer').hide();
      $('#subcribedProductsContainer').show();
  }

}

function subcribedProductId(quantity, subcribeId) {



  db.collection('customers').doc(currentUser).collection('subscriptions').doc(subcribeId).collection('users').get().then(function (querySnapshot) {

          if(!querySnapshot.docs.length) {
              for(i=0; i<quantity; i++) {
                  $('.userEmailAddOuter').append(`<div class="addUserEmailIdOut">
                      <div class="addUserEmailIdBox" style=""><input type="email" data-added="0" placeholder="Add User Email" class="addUserEmailIdInput"></div>
                      <div class="addedUserEmailIdOut" style="display: none;">
                          <div class="addedUserEmailId" style=""></div>
                          <div class="addUserEmailIdRemove">×</div>
                      </div>
                  </div>`);
              }
              $('.addUserEmailIdInput').first().focus();
              return true;
          }

          let allQuantityCheck = 0;
          querySnapshot.forEach(async function (doc) {

              allQuantityCheck++;
              $('.userEmailAddOuter').append(`<div class="addUserEmailIdOut">
                      <div class="addUserEmailIdBox"  style="display: none;"><input type="email" data-added="1" placeholder="Add User Email" class="addUserEmailIdInput"></div>
                      <div class="addedUserEmailIdOut">
                          <div class="addedUserEmailId" style="">${doc.data().email}</div>
                          <div class="addUserEmailIdRemove">×</div>
                      </div>
                  </div>`);

          });

          if(allQuantityCheck < quantity) {
              quantity = quantity - allQuantityCheck;
              for(i=0; i<quantity; i++) {
                  $('.userEmailAddOuter').append(`<div class="addUserEmailIdOut">
                      <div class="addUserEmailIdBox" style=""><input type="email" data-added="0" onkeypress="$(this).removeClass('addUserEmailIdInputErr')" onfocus="$(this).css({'border-color': '#2172f6'})" onfocusout="$(this).css({'border-color': 'black'})" placeholder="Add User Email" class="addUserEmailIdInput"></div>
                      <div class="addedUserEmailIdOut" style="display: none;">
                          <div class="addedUserEmailId" style=""></div>
                          <div class="addUserEmailIdRemove" onclick="removeEditUserEmail(this)">×</div>
                      </div>
                  </div>`);
              }
          }
          else {
            $('.addUsersEmailIdSaveButt').hide();
          }

  });

  function removeEditUserEmail(selected) {
      $('.addUsersEmailIdSaveButt').show();
      $(this).parent().parent().find('.addedUserEmailIdOut').hide();
      $(this).parent().parent().find('.addUserEmailIdBox').show();
      $(this).parent().parent().find('.addUserEmailIdInput').attr('data-added', '0');
  }

  function addUsersEmailIdSaveButt() {
      $('.addUserEmailIdInput').each(function() {

        if($(this).attr('data-added') == '0') {

          let reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
          if($(this).val().trim() == '' || reg.test(int_mail) == false) {
            $(this).addClass('addUserEmailIdInputErr').focus();
            return false;
          }

          db.collection('customers').doc(currentUser).collection('subscriptions').doc(subcribeId).collection('users').doc($(this).val().trim()).set({
              email: $(this).val().trim(),
              createdTime: Date.now()
          }, {merge: true});

          $(this).parent().parent().find('.addedUserEmailIdOut').show();
          $(this).parent().parent().find('.addedUserEmailId').text($(this).val().trim());
          $(this).val('');
          $(this).parent().parent().find('.addUserEmailIdInput').attr('data-added', '1');
          $(this).parent().parent().find('.addUserEmailIdBox').hide();

        }

      });
  }

}
UA_PRODS_LIST = {};
function startDataListeners() {
  // Get all our products and render them to the page
  const products = document.querySelector('.products');
  const template = document.querySelector('#product');


    db.collection('products').where('active', '==', true).orderBy('stripe_metadata_payment_page_order', 'asc')
    .get()
    .then(function (querySnapshot) {
      querySnapshot.forEach(async function (doc) {
          const product = doc.data();
          const productId = doc.id;
          UA_PRODS_LIST[productId] = product;
          // console.log(product);
          var companyDomain = product.stripe_metadata_company;
          if(!companyDomain || companyDomain.length === 0){
            companyDomain = "ulgebra.com";
          }
          var companyNameSlug = companyDomain.replaceAll('.', '-');
          var companyNameSlugElement = 'prod-app-company-'+companyNameSlug;
          if($('#'+companyNameSlugElement).length === 0){
            $('.productListHolder').append(`
              <div id="${companyNameSlugElement}" class="prod_company_holder">
                <div class="prod_company_ttl">
                  <img src="./store/images/company-slug-logos/${companyNameSlug}.png">
              </div><div class="prod_company_apps_holder"> </div></div>
              `);
          }

          $('#'+companyNameSlugElement+' .prod_company_apps_holder').append(`<div id="pid_${productId}" data-prodname="${product.name}" onclick="selectProductId('${productId}')" class="product-item only-prod-item">
              <div class="product-name">
                  <img src="${product.images[0]}"/> ${product.name}
              </div>
              <div class="plan-name">
                 ${product.stripe_metadata_per_user_price ? ('<b>'+product.stripe_metadata_per_user_price+'</b> per user per month') : 'Click to view prices'}
              </div>
              <div class="plan-appcode">${product.stripe_metadata_appcode}</div>
          </div>`);
        $(".processing").show();
        const priceSnap = await doc.ref
          .collection('prices')
          .orderBy('unit_amount')
          .get();
        $(".processing").hide();
//        if (!'content' in document.createElement('template')) {
//          console.error('Your browser doesn’t support HTML template elements.');
//          return;
//        }
//        container.querySelector('.description').innerText =
//          product.description ? product.description.toUpperCase() || '';
        // Prices dropdown
        priceSnap.docs.forEach((doc) => {
            //console.log(productId, doc.id);
            const priceData = doc.data();
            //console.log(priceData);
            const priceId = doc.id;

            if(!priceData.active || priceData.type === "one_time"){
              //console.log("inactive or one_time");
            }
            else{
                var featureHTML = '';
                  Object.keys(product).forEach(item=>{
                      if(item.startsWith('stripe_metadata_plan_feat')){
                          var stripeFeatPlanName = item.split('stripe_metadata_plan_feat_')[1];
                          if(priceData.description.toLowerCase().replaceAll(/[\W_]+/g, '') === stripeFeatPlanName){
                              product[item].split('\n').forEach(featLineITEM=>{
                                  if(featLineITEM.trim().length <1){
                                      return;
                                  }
                                  featureHTML+= `<div class="pmtppf-item">${featLineITEM}</div>`;
                              });
                          }
                      }
                  });
                  featureHTML+= `<div class="pmtppf-item">+Ulgebra Credits for paid amount</div>`;
                  $('.priceListHolder').append(`<div id="plid_${priceId}" class="product-item price-item ${priceData.currency} ${priceData.type} ${productId} ">
                  <div class="plan-name">
                      <div class="pmtp-planname">
                            ${priceData.description}
                        </div>
                        <div class="pmtp-planprice">
                            ${currencyMap[priceData.currency]}${priceData.unit_amount/100}
                        </div>
                        <div class="pmtp-planperhow">
                            /${product.stripe_metadata_amount_per_display ? product.stripe_metadata_amount_per_display+' /'+priceData.interval : ''}
                        </div>
                      <div class="pmtp-plan-features">
                            ${featureHTML}
                        </div>
                      <div class="pmtp-plan-actions">
                            <button class="pmtppa-btn" onclick="selectPaymentPlan('${priceId}')">${priceData.trial_period_days >0 ? 'Start Free Trial': 'Select Plan'}</button>
                        </div>
                  </div>
              </div>`);
                if(!pricesObj[product.id]){
                    pricesObj[product.id] = {};
                }
              pricesObj[product.id][doc.id] = doc.data();

            }



//          const content = document.createTextNode(
//            `${new Intl.NumberFormat('en-US', {
//              style: 'currency',
//              currency: priceData.currency,
//            }).format((priceData.unit_amount / 100).toFixed(2))} per ${
//              priceData.interval
//            }`
//          );
//          const option = document.createElement('option');
//          option.value = priceId;
//          option.appendChild(content);
//          container.querySelector('#price').appendChild(option);
//        });
//
//        if (product.images.length) {
//          const img = container.querySelector('img');
//          img.src = product.images[0];
//          img.alt = product.name;
//        }
//
//        const form = container.querySelector('form');
//        form.addEventListener('submit', subscribe);
//
//        products.appendChild(container);
      });
        initialNavigateToProdFromQuery(productId);
    });
    });

  // Get all subscriptions for the customer
  /*db.collection('customers')
    .doc(currentUser)
    .collection('subscriptions')
    .where('status', 'in', ['trialing', 'active'])
    .onSnapshot(async (snapshot) => {
      if (snapshot.empty) {
        // Show products
        document.querySelector('#subscribe').style.display = 'block';
        return;
      }
      document.querySelector('#subscribe').style.display = 'none';
      document.querySelector('#my-subscription').style.display = 'block';
      // In this implementation we only expect one Subscription to exist
      const subscription = snapshot.docs[0].data();
      const priceData = (await subscription.price.get()).data();
      document.querySelector(
        '#my-subscription p'
      ).textContent = `You are paying ${new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: priceData.currency,
      }).format((priceData.unit_amount / 100).toFixed(2))} per ${
        priceData.interval
      }, giving you the role: ${await getCustomClaimRole()}. 🥳`;
    });*/
}
var existingLicenseOwnerList = {};
function fetchExistingLicenseOwnersList(){
    firebase.database().ref(`ualic_user_credits/${currentUser}`).on('value', function(snapshot){
        $('#ua-lic-credit-amount').text(snapshot.val() ? Math.ceil(snapshot.val() * 10)/10 : 0);
      });
    renderSubscribeUserList();
  db.collection('UALicenseOwners').doc(currentUser).collection('subscriptions').get().then(snapshot=>{
    if(snapshot.empty){
      return;
    }
    snapshot.docs.forEach(item=>{
      existingLicenseOwnerList[item.id] = item.data();
    });
  });
}

function renderSubscribeUserList(){
  try{
$('.license-configure-panel').show();
$('.lcpi-item-fetch-status').text('Checking for active subscriptions...');
$('.lcp-lic-item').remove();
  db.collection('customers')
    .doc(currentUser)
    .collection('subscriptions')
    .get().then(async (snapshot) => {
      if (snapshot.empty) {
        $('.lcpi-item-fetch-status').text("You don't have any active subscriptions, purchase below then configure users here.");
        return;
      }
      enableTrialModesForPay = false;
      var activeSubsCount = 0;
      for(var i=0;i<snapshot.docs.length;i++){
        try{
      const subscription = snapshot.docs[i].data();
      const productMeta = (await subscription.product.get());
      var productId = productMeta.id;
      var productData = productMeta.data();
      var priceMeta = (await subscription.price.get());
      $('.lcpi-item-fetch-status').hide();
      var priceId = priceMeta.id;
      var priceData = priceMeta.data();
      var subscriptionId = snapshot.docs[i].id;
      var isActive = subscription.status === "active" || subscription.status === "trialing";
      if((!isActive && !includeInActiveSubs)){
        continue;
      }
      if(!priceData){
        //console.log(' priceData not found '+ priceId);
      }
      if(!productData){
        //console.log(' productData not found '+ productId);
      }
      activeSubsCount++;


      let isUserChangeAllowed = false;
      if(isActive && subscription.current_period_start) {
        isUserChangeAllowed = Number(subscription.current_period_start.seconds+'000');
        isUserChangeAllowed = new Date(isUserChangeAllowed).setDate(new Date(isUserChangeAllowed).getDate()+25);
        if(isUserChangeAllowed >= new Date().getTime())
        isUserChangeAllowed = true;
        else
        isUserChangeAllowed = false;
      }

      if(isAdminUsers) {
        isUserChangeAllowed = true;
      }


      $('.lcp-content').append(`
        <div class="lcp-lic-item" id="license-item-${subscriptionId}">
            <div class="lcpi-itemmeta">
            <div class="lcpi-icon">
              <img src="${productData.images ? productData.images[0]: ''}"/>
            </div>
            <div class="lcpi-ttl-meta">
              <div class="lcp-lic-item-ttl">
                ${productData.name}
              </div>
              <div class="lcpi-quantity">
                <div class="lcpi-plantype">${priceData.description}</div> <div class="lcpi-plantype">${subscription.quantity} Users</div>

                <span style="color: grey;margin-left: 15px;"> ${subscription.status.toUpperCase()} ${isActive ? " from <b>"+(subscription.current_period_start ? subscription.current_period_start.toDate().toLocaleDateString(): "")+"</b> to <b>"+(subscription.current_period_end ? subscription.current_period_end.toDate().toLocaleDateString(): "")+"</b>" : "<span class='c-red' style='margin-right:10px'>Subscription is not active</span>"+" Cancelled on <b>"+(subscription.canceled_at ? subscription.canceled_at.toDate().toLocaleDateString() : "")+"</b>"} </span>

              </div>
            </div>
          </div>
            <div class="lcpi-user-tip">
              ${productData.stripe_metadata_userIdText &&  productData.stripe_metadata_userIdText !== "" ? productData.stripe_metadata_userIdText : 'Provide EMail Ids of the CRM users to allow them to use this license.'}
            </div>
            <div class="lcpi-users">

            </div>
            <div class="lcpi-action">
              <button class="lcpi-action-save" ${!isUserChangeAllowed ? "style='display:none'" : ""} onclick="updateLICUsers('${subscriptionId}','${priceId}')">Save</button>
            </div>
          </div>`);

      
      var availLicesesCount = 0;
      for(var j=0;j<subscription.quantity;j++){
        var value = existingLicenseOwnerList[subscriptionId] && existingLicenseOwnerList[subscriptionId].userIds.length > j ? existingLicenseOwnerList[subscriptionId].userIds[j]: "";
        if(isUserChangeAllowed || !value || value.trim().length === 0){
          availLicesesCount++;
        }
        $(`#license-item-${subscriptionId} .lcpi-users`).append(`<div class="lcpi-user-item">
                <div class="lcpi-u-index">${j+1}</div><div class="lcpi-u-inp"><input class="inp_lcpi-user" type="text" ${!isUserChangeAllowed && value && value.trim() ? "disabled" : ""} value="${value}"/></div>
              </div>`);
        // if(!isUserChangeAllowed && value && value.trim())
        // $('.lcpi-action-save').last().remove();
        // else
        // $('.lcpi-action-save').last().show();
      }
      if(availLicesesCount){
        $('.lcpi-action-save').last().show();
      }else{
        $('.lcpi-action-save').last().remove();
      }
      /*document.querySelector(
        '#my-subscription p'
      ).textContent = `You are paying ${new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: priceData.currency,
      }).format((priceData.unit_amount / 100).toFixed(2))} per ${
        priceData.interval
      }, giving you the role: ${await getCustomClaimRole()}. 🥳`;*/
    }catch(ex){
      console.log(ex);
    }
    }
    if(activeSubsCount === 0){
      $('.lcpi-item-fetch-status').text("You don't have any active subscriptions, purchase below then configure users here.").show();
    }
    });
  }catch(exc){
    console.log(exc);
  }
}

var impersonUserId = null;
var includeInActiveSubs = false;
function fetchUserSubs(){
  if($("#user-id-inp-imperson").val().trim()!==""){
    currentUser = $("#user-id-inp-imperson").val().trim();
    includeInActiveSubs = true;
    $("#lic-imperson-email").html("<b style='color:blue'> --- Checking user... ---</b>");
    db.doc(`/ulgebraUsers/${currentUser}/private/profile`).get().then(doc=>{
      if(!doc.exists){
        $("#lic-imperson-email").html("<b style='color:red'> --- Invalid USER ID</b>");
        return;
      }
      $("#lic-imperson-email").text(" --- "+doc.data().em);
      fetchExistingLicenseOwnersList();
      $('.addIntegaration_outerBodyH').html(transferLoading);
      lastTransferVisible = '';
      lastTransferVisibleWA = '';
      transferCreditsArr = {};
      waTransEmptyCheck = false;
      TransEmptyCheck = false;
      showUserCredits();
  });
  }
}

function initialNavigateToProdFromQuery(renderedProdId) {
  try{
      let queryParams =  new URLSearchParams(window.location.search);
      if(queryParams.get("appCode")){
        let resolvedProdId = null
        Object.keys(UA_PRODS_LIST).forEach(prod_id=>{
            let prodItem = UA_PRODS_LIST[prod_id];
            if(prodItem.hasOwnProperty('stripe_metadata_appcode') && prodItem.stripe_metadata_appcode === queryParams.get("appCode")){
              resolvedProdId = prod_id;
            }
        });
        if(resolvedProdId){
          queryParams.set("product_id", resolvedProdId);
        }
      }
      let prod_id = queryParams.get("product_id");
      if(prod_id && prod_id!=="" && (!renderedProdId || renderedProdId === prod_id)){
        $('html, body').animate({scrollTop: $(`#pid_${prod_id}`).offset().top - 50}, 500);
        selectProductId(prod_id);
    }
  }catch(ex){
    console.log(ex);
  }
}
/**
 * Event listeners
 */

// Signout button
document
  .getElementById('signout')
  .addEventListener('click', () => firebase.auth().signOut());

// Checkout handler
async function proceedToPayment() {
  if(selectedCurrency !== "inr"){
    taxRates = ["txr_1IlvlbHo9p6j6FgRG8iwtor9"];
  }
  $(".processing").show();
  const docRef = await db
    .collection('customers')
    .doc(currentUser)
    .collection('checkout_sessions')
    .add({
      price: selectedPaymentPlanId,
      allow_promotion_codes: true,
      tax_rates: taxRates,
      success_url: window.location.origin + "/store",
      cancel_url: window.location.origin + "/store",
      quantity: parseInt($("#quan-count").val()),
      trial_from_plan: enableTrialModesForPay,
      metadata: {
        tax_rate: taxRates.length > 0 ? '18% GST exclusive' : '',
      },
    });
  // Wait for the CheckoutSession to get attached by the extension
  docRef.onSnapshot((snap) => {
    const { error, sessionId } = snap.data();
    if (error) {
      // Show an error to your customer and then inspect your function logs.
      alert(`An error occured: ${error.message}`);
      document.querySelectorAll('button').forEach((b) => (b.disabled = false));
    }
    if (sessionId) {
      // We have a session, let's redirect to Checkout
      // Init Stripe
      //console.log(sessionId);
      const stripe = Stripe(STRIPE_PUBLISHABLE_KEY);
      stripe.redirectToCheckout({ sessionId });
    }
  });
}

// Billing portal handler
document
  .querySelector('#billing-portal-button')
  .addEventListener('click', async (event) => {
      try{
            $(".processing").show();
            //document.querySelectorAll('button').forEach((b) => (b.disabled = true));

            // Call billing portal function
            const functionRef = firebase
              .app()
              .functions(functionLocation)
              .httpsCallable('ext-firestore-stripe-subscriptions-createPortalLink');
            const { data } = await functionRef({ returnUrl: window.location.origin });
            window.location.assign(data.url);
      }
      catch(exc){
          console.log(exc);
          alert("Unable to retrive your subscriptions or you don't have any subscriptions.");
          $(".processing").hide();
      }
  });

// Get custom claim role helper
async function getCustomClaimRole() {
  await firebase.auth().currentUser.getIdToken(true);
  const decodedToken = await firebase.auth().currentUser.getIdTokenResult();
  return decodedToken.claims.stripeRole;
}

function selectProductId(productid){
    $('#ua-ua-credit-consume-note').toggle(productid === "prod_MGbKLzmgcTnb40");
    $('#pmtw-selected-prod-name').text($('#pid_'+productid).attr('data-prodname'));
    selectedProductId = productid;
    $('.product-item').removeClass('chosenProduct');
    $('#pid_'+productid).addClass('chosenProduct');
    // $('.prod'+productid+' .price-item').hide();
    // $('.prod'+productid+' .price-item').css({'display' : 'inline-block'});
    $('#ua-lic-product-plans').show();
    showPrices();
    $(".pmt-userquantityholder").hide();
    history.pushState(null, '', `/store?product_id=${productid}`);
    $('html, body').animate({scrollTop: $(`#pid_${productid}`).offset().top - 50}, 500);

}
function selectPaymentPlan(planId){
    selectedPaymentPlanId = planId;
    $(".pmt-userquantityholder").show();
    $('.price-item').removeClass('chosenProduct');
    $('#plid_'+planId).addClass('chosenProduct');
    document.getElementById("selectProductsContainer").scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest"
    });
}
function showPrices(){
    $(".showAfterSubType").show();
    //selectedSubType = document.getElementById("rad-onetimesub").checked ? "one_time" : "recurring";
    $('.price-list .product-item').hide();
    $('.price-list .product-item.'+selectedProductId+'.'+selectedSubType+'.'+selectedCurrency).css({'display': 'inline-block'});
    // $('html, body').animate({scrollTop: $('#selectProductsContainer').offset().top - 50}, 500);
}

$(document).ready(function(){
   $('input[type="radio"]').on('click change', function(e) {
        showPrices();
    });
});

function adjustQuantity(increase){
    var currentVal = parseInt($('#quan-count').val());
    var nextVal = increase ? currentVal+1 : currentVal-1;
    $('#quan-count').val(nextVal);
    if(nextVal <2){
        $("#quan-decrease").attr({'disabled': true});
    }
    else{
        $("#quan-decrease").removeAttr("disabled");
    }
}

function changeCurrency(){
    selectedCurrency = $("#currencySelector").val();
    // $('.price-list').hide();
    // $('.product-item').removeClass('chosenProduct');
    initialNavigateToProdFromQuery();
}

function updateLICUsers(subscriptionId, priceId){
  var updatableUserIds = [];
  var updateCallFunction = firebase.functions().httpsCallable('saveLICUsersList');
  $(`#license-item-${subscriptionId} .inp_lcpi-user`).each(function(index){
    var idItem = $(this).val().trim();
    if(idItem !==""){
      if(!updatableUserIds.includes(idItem)){
        updatableUserIds.push(idItem);
      }
    }
  });
  $(`#license-item-${subscriptionId} .lcpi-action`).append('<b class="licpi-cs-status" style="color:royalblue"> Saving user details...</b>');
  updateCallFunction({
    "priceId": priceId,
    "subscriptionId": subscriptionId,
    "userIds": updatableUserIds,
    "impersonUserId" : currentUser
  }).then(res=>{
    $(`#license-item-${subscriptionId} .licpi-cs-status`).css({'color':'green'}).text("User details has been saved");
    setTimeout(function(){
      $(`#license-item-${subscriptionId} .licpi-cs-status`).remove();
    }, 2000);
    //console.log(res);
  }).catch(ex=>{
    console.log(ex);
  });
}

function filterProducts(ev){
    var val = ev.value.toLowerCase();
    
    if(val !== ""){
        //$('.tblfldhide').hide();
        document.querySelectorAll('.only-prod-item').forEach(item=>{
            if(item.getAttribute('data-prodname').toLowerCase().indexOf(val) < 0){
                item.style.display = 'none';
            }else{
                item.style.display = 'block';
            }
        });
    }
    else{
        $('.tblfldhide').show();
        $('.only-prod-item').show();
    }
}


function transferCreditsToWhatcetraOpen() {

  $("#addIntegaration").show(100);
  $(".addIntegaration_outer").show();

}

function transferCreditsToWhatcetraOpenH() {

  $("#addIntegarationH").show(100);
  $(".addIntegaration_outerH").show();

}

function close_integarationDiv() {
    $("#addIntegaration").hide();
    $(".addIntegaration_outer").hide();
    $('.statusTransferCreditsDiv').hide();
    $('.statusTransferCreditsDivBody').text('').css({'color': '#000000db'});
    $('.statusTransferCreditsDivFood').hide();
}

function close_integarationDivH() {
    $("#addIntegarationH").hide();
    $(".addIntegaration_outerH").hide();
}

function addIntegaration_outer(e) {
    if($(e.target).hasClass('addIntegaration_outer')) {
        close_integarationDiv();
    }
}

function addIntegaration_outerH(e) {
    if($(e.target).hasClass('addIntegaration_outerH')) {
        close_integarationDivH();
    }
}

function addIntegarate_first(selected, e) {
    $(selected).css({"border-color":"transparent"});
    $('#error_inputEmail').text('').hide();
    if(e.which == 13 && $(selected).val().trim() != ''){      
      if(emailCheckFunction()) {
        $('.integ_input').last().focus();
      }
      else {
        $('#integaration_name').focus();
      }
    }
}

function emailCheckFunction() {

  let mailArr = $("#integaration_name").val().trim();
  
  if(!mailArr) {
      $('#error_inputEmail').text('* please enter email.').show();
      $('#integaration_name').css({"border-color": "#d20000"});
      return false;
  }
  
  mailArr = mailArr.split(/[ ,]+/);
  
  isWrongMail = false;
  
  for(let i=0; i < mailArr.length; i++) {
    if(mailArr[i] && validEmail(mailArr[i]) === false)
    {
      $('#error_inputEmail').text('* please enter valid email.').show();
      $('#integaration_name').css({"border-color": "#d20000"});
      isWrongMail = true;
    }
    else if(mailArr[i]) {
        $('#error_inputEmail').text('').hide();
        $('#integaration_name').css({"border-color": "transparent"});
    }
  }
  
  return !isWrongMail;

}

async function creditsCheckFunction() {

  let changeCredit = $("#integarate_webhook").val().trim();
  changeCredit = Number(changeCredit.replace( /\D+/g, ''));
  let totCredits = 0;

  await firebase.database().ref(`ualic_user_credits/${currentUser}`).on('value', function(snapshot){
        totCredits = snapshot.val();
  });

  totCredits = totCredits ? Number((totCredits+"").replace( /\D+/g, '')) : 0;

  if(!totCredits) {
      $('#error_inputIntegarate').text('* you don`t have credits.').show();
      $('#integarate_webhook').css({"border-color": "#d20000"});
      return false;
  }
  else if(!changeCredit) {
      $('#error_inputIntegarate').text('* please enter the credits.').show();
      $('#integarate_webhook').css({"border-color": "#d20000"});
      return false;
  }
  else if (!totCredits && totCredits < changeCredit)
  {
      $('#error_inputIntegarate').text('* your available credits are low.').show();
      $('#integarate_webhook').css({"border-color": "#d20000"});
      return false;
  }
  else {
    $('#error_inputIntegarate').text('').hide();
    $('#integarate_webhook').css({"border-color": "transparent"});
    return true;
  }

}

function addIntegarate_second(selected, e) {
    $(selected).css({"border-color":"transparent"});
    $('#error_inputIntegarate').text('').hide();
    if(e.which == 13 && $(selected).val().trim() != '') {
        if(creditsCheckFunction()) {
          $('.integaration_footer').find('#waCreditChangeSend').click();
        }
        else {
          $('#integarate_webhook').focus();
        }        
    }
};

function onKeypressNumberField(selected, e) {
    if (isNaN(String.fromCharCode(e.keyCode))) {
        if(!Number(String(e.key)))
        e.preventDefault();
    }
    else if (e.keyCode == 13) {
        e.preventDefault();
        return false;
    }
    else if (e.keyCode == 32) {
        e.preventDefault();
        return false;
    }
    else if (e.keyCode == 45) {
        e.preventDefault();
        return false;
    }
    else if (e.keyCode == 46) {
        e.preventDefault();
        return false;
    }
    else if(true){

        $(selected).css({"border-color":"transparent"});
        return true;

    }

}

async function transferCreditsToWhatcetra() {

  let creditsCheck = await creditsCheckFunction();
  if(!emailCheckFunction()) {
    $('#integaration_name').focus();
    return;
  }
  else if(!creditsCheck) {
    $('#integarate_webhook').focus();
    return;
  }
  else {

        $('.statusTransferCreditsDiv').show();
        $('.statusTransferCreditsDivBody').html('<span class="TransferringCreditsToWhatcetra">Transferring...<br><br></span>').css({'color': '#000000db'});
        $('.statusTransferCreditsDivFood').hide();

        let mail = $("#integaration_name").val().trim();
        let changeCredit = $("#integarate_webhook").val().trim();
        changeCredit = Number((changeCredit+"").replace( /\D+/g, ''));
        
        let mailsArray = mail.split(/[ ,]+/);
        let LastMailCount = 0;
        mailsArray.forEach(mailItem=>{
            
            if(!mailItem) {
                return false;
            }

            var transferCallFunction = firebase.functions().httpsCallable('transferCreditsToWhatCetra');
            transferCallFunction({
              "transferCreditToEmail": mailItem,
              "transferCreditCount": changeCredit
            }).then(res=>{

                //console.log(res);
                res = res.data;
                if(res.status != 'success') {
                  $('.statusTransferCreditsDivBody').append('<span style="color:#8f1717db"><b style="color: #153c69;">'+mailItem+' :</b> '+ res.message+'</span> <br><br>');
                  $('.statusTransferCreditsDivFood').show();
                }
                else {
                  $('.statusTransferCreditsDivBody').append('<span style="color:#0b5701db"><b style="color: #153c69;">'+mailItem+' :</b> '+ res.message+'</span> <br><br>');
                  $('.statusTransferCreditsDivFood').show();
                  $("#integaration_name").val('');
                  $("#integarate_webhook").val('')
                  setTimeout(function(){close_integarationDiv();}, 2000);
                }
                
                if(LastMailCount === mailsArray.length-1) {
                    $('.TransferringCreditsToWhatcetra').remove();
                }
                else {
                    LastMailCount++;
                }

            }).catch(ex=>{
              console.log(ex);
            });
        });

  }
  
}

function validEmail(e) {
    var filter = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
    return String(e).search (filter) != -1;
}

function transferCreditsToUlgebraOpen() {

  $("#addIntegaration_ulg").show(100);
  $(".addIntegaration_outer_ulg").show();

}

function close_integarationDiv_ulg() {
    $("#addIntegaration_ulg").hide();
    $(".addIntegaration_outer_ulg").hide();
    $('.statusTransferCreditsDiv_ulg').hide();
    $('.statusTransferCreditsDivBody_ulg').text('').css({'color': '#000000db'});
    $('.statusTransferCreditsDivFood_ulg').hide();
}

function addIntegaration_outer_ulg(e) {
    if($(e.target).hasClass('addIntegaration_outer_ulg')) {
        close_integarationDiv_ulg();
    }
}

function addIntegarate_first_ulg(selected, e) {
    $(selected).css({"border-color":"transparent"});
    $('#error_inputEmail_ulg').text('').hide();
    if(e.which == 13 && $(selected).val().trim() != ''){      
      if(emailCheckFunction_ulg()) {
        $('.integ_input_ulg').last().focus();
      }
      else {
        $('#integaration_name_ulg').focus();
      }
    }
}

function emailCheckFunction_ulg() {
    
  let mailArr = $("#integaration_name_ulg").val().trim();
  
  if(!mailArr) {
      $('#error_inputEmail_ulg').text('* please enter email.').show();
      $('#integaration_name_ulg').css({"border-color": "#d20000"});
      return false;
  }
  
  mailArr = mailArr.split(/[ ,]+/);
  
  isWrongMail = false;
  
  for(let i=0; i < mailArr.length; i++) {
    if(mailArr[i] && validEmail(mailArr[i]) === false)
    {
      $('#error_inputEmail_ulg').text('* please enter valid email.').show();
      $('#integaration_name_ulg').css({"border-color": "#d20000"});
      isWrongMail = true;
    }
    else if(mailArr[i]) {
        $('#error_inputEmail_ulg').text('').hide();
        $('#integaration_name_ulg').css({"border-color": "transparent"});
    }
  }
  
  return !isWrongMail;

}

async function creditsCheckFunction_ulg() {

  let changeCredit = $("#integarate_webhook_ulg").val().trim();
  changeCredit = Number(changeCredit.replace( /\D+/g, ''));
  let totCredits = 0;

  await firebase.database().ref(`ualic_user_credits/${currentUser}`).on('value', function(snapshot){
        totCredits = snapshot.val();
  });

  totCredits = totCredits ? Number((totCredits+"").replace( /\D+/g, '')) : 0;

  if(!totCredits) {
      $('#error_inputIntegarate_ulg').text('* you don`t have credits.').show();
      $('#integarate_webhook_ulg').css({"border-color": "#d20000"});
      return false;
  }
  else if(!changeCredit) {
      $('#error_inputIntegarate_ulg').text('* please enter the credits.').show();
      $('#integarate_webhook_ulg').css({"border-color": "#d20000"});
      return false;
  }
  else if (!totCredits && totCredits < changeCredit)
  {
      $('#error_inputIntegarate_ulg').text('* your available credits are low.').show();
      $('#integarate_webhook_ulg').css({"border-color": "#d20000"});
      return false;
  }
  else {
    $('#error_inputIntegarate_ulg').text('').hide();
    $('#integarate_webhook_ulg').css({"border-color": "transparent"});
    return true;
  }

}

function addIntegarate_second_ulg(selected, e) {
    $(selected).css({"border-color":"transparent"});
    $('#error_inputIntegarate_ulg').text('').hide();
    if(e.which == 13 && $(selected).val().trim() != '') {
        if(creditsCheckFunction_ulg()) {
          $('.integaration_footer_ulg').find('#waCreditChangeSend_ulg').click();
        }
        else {
          $('#integarate_webhook_ulg').focus();
        }        
    }
};

async function transferCreditsToUlgebra() {

  let creditsCheck = await creditsCheckFunction_ulg();
   if(!emailCheckFunction_ulg()) {
    $('#integaration_name_ulg').focus();
    return;
  }
  if(!creditsCheck) {
    $('#integarate_webhook_ulg').focus();
    return;
  }
  else {

        $('.statusTransferCreditsDiv_ulg').show();
        $('.statusTransferCreditsDivBody_ulg').html('<span class="TransferringCreditsToUlgebra">Transferring...<br><br></span>').css({'color': '#000000db'});
        $('.statusTransferCreditsDivFood_ulg').hide();

        let mail = $("#integaration_name_ulg").val().trim();
        let changeCredit = $("#integarate_webhook_ulg").val().trim();
        changeCredit = Number((changeCredit+"").replace( /\D+/g, ''));
        let mailsArray = mail.split(/[ ,]+/);
        let LastMailCount = 0;
        mailsArray.forEach(mailItem=>{
            
          if(!mailItem) {
              return false;
          }
          let transferCallFunction = firebase.functions().httpsCallable('transferCreditsToAnotherUlgebraAccount');
          transferCallFunction({
            "transferCreditToEmail": mailItem,
            "transferCreditCount": changeCredit
          }).then(res=>{
              
              //console.log(res);
              res = res.data;
              if(res.status != 'success') {
                $('.statusTransferCreditsDivBody_ulg').append('<span style="color:#8f1717db"><b style="color: #153c69;">'+mailItem+' :</b> '+ res.message+'</span> <br><br>');
                $('.statusTransferCreditsDivFood_ulg').show();
              }
              else {
                $('.statusTransferCreditsDivBody_ulg').append('<span style="color:#0b5701db"><b style="color: #153c69;">'+mailItem+' :</b> '+ res.message+'</span> <br><br>');
                $('.statusTransferCreditsDivFood_ulg').show();
                $("#integaration_name_ulg").val('');
                $("#integarate_webhook_ulg").val('')
                setTimeout(function(){close_integarationDiv_ulg();}, 2000);
              }
              
            if(LastMailCount === mailsArray.length-1) {
                $('.TransferringCreditsToUlgebra').remove();
            }
            else {
                LastMailCount++;
            }
            
          }).catch(ex=>{
            console.log(ex);
          });
          
      });

  }
  
}
