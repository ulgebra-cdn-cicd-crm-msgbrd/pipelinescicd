var recordId;
var recordModule;
const urlParams = new URLSearchParams(window.location.search);
var serviceOrigin = urlParams.get('serviceOrigin'); 
var zsckey=null;
var firebase_auth = false;
async function makeCalendlyCall(data,method){
    if(firebase_auth){
        data.method = method;
        data.zapikey = zsckey;
        return fetch("https://us-central1-ulgebra-license.cloudfunctions.net/makeCalendlyHTTPRequestFromZoho", {
            "method" :"POST",
            "body":JSON.stringify(data)
        })
        .then(function (response) {  
            console.log(response);
            return response.json();
        })
        .then(function (responseJSON) {
            console.log(responseJSON);
            if(responseJSON && responseJSON.error && responseJSON.error.message == "user_not_found"){
                 return null;
            }
            return responseJSON;
        })
        .catch(function (error) {
            console.log("Error: " + error);
            return null;
        });
    }
    else{
        return await ZOHO.CRM.CONNECTOR.invokeAPI("calendlyforzohocrm.calendly.calendly"+method.toLowerCase()+"calls",data);
    }

}
async function checkCalendlyApiKey(){
    await getCalendlyDetails();
    if(!calendlyDetails || !calendlyDetails.current_organization){
        return;
    }
    return getWebhooks().then(async function(webhooks){
        var isWebhookFound = false;
        if(webhooks && webhooks.collection && webhooks.collection.length){
            for(let i=0;i<webhooks.collection.length;i++){
                if(webhooks.collection[i].callback_url == crm_url && webhooks.collection[i].state == "active"){
                    if(isWebhookFound == true){
                        await _deleteWebhook(webhooks.collection[i].uri.split("/")[4]);
                    }
                    isWebhookFound  = true;
                }
            }
            if(isWebhookFound){
                document.getElementById("loader").style.display = "none";
       //       document.getElementById("apikey").disabled = true;
                // document.getElementById("remove").style.display="inline-block";
                // document.getElementById("save").style.display="none";
                document.getElementById("ErrorText").innerHTML = "";
                return true;
            }
        }
        await addWebhook();
        document.getElementById("loader").style.display = "none";
        return true;
    }); 
}
var calendlyDetails = {};
async function getCalendlyDetails(){
    try{
        var resp = await makeCalendlyCall({"url":"users/me"},"GET");
    }
    catch(e){
        console.log(e);
        if(e && e.code == "403" && e.message == "Authorization Exception"){
            document.getElementById("loader").style.display = "none";
            document.getElementById("Error").style.display = "block";
            var configureLink = serviceOrigin+"/crm/settings/extensions/all/calendlyforzohocrm?tab=webInteg&subTab=marketPlace&nameSpace=calendlyforzohocrm&portalName=ulgebra";
            var version = await ZOHO.CRM.FUNCTIONS.execute("calendlyforzohocrm__addeventandcontactfromcalendly", {"action":"getVersion"});
            if(version && version.details.output && (parseInt(version.details.output) > 86)){
                document.getElementById("ErrorText").innerHTML=`Please Authorize Calendly in <a style="color:#09e8f7;" target='_blank' href="${configureLink}">Extension configure page</a>`;
                try{
                    var resp2 = await ZOHO.CRM.CONNECTOR.invokeAPI("calendlyforzohocrm.calendly.calendlyhandler",{"url":"ulgebralogs?action=checkCalendlyConnector","body":{"url":"https://api.calendly.com/users/me","method":"GET","email":orgDetails.org[0].primary_email}});
                    console.log(resp2);
                }
                catch(e){
                    console.log(e);
                }
            }
            else{
                document.getElementById("ErrorText").innerHTML=`Please update the Extension to Latest Version <a style="color:#09e8f7;" target='_blank' href="${configureLink}">here</a>`;
            }
        }
        calendlyDetails = null;
        return null;
    }
    if(resp.response){
        resp = JSON.parse(resp.response);
    }
    if(resp && resp.resource){
        calendlyDetails = resp.resource;
        return;
    }
}
async function addWebhook(){
    if(!calendlyDetails || !calendlyDetails.current_organization){
        return false;
    }
    var resp = await ZOHO.CRM.FUNCTIONS.execute("calendlyforzohocrm__addeventandcontactfromcalendly", {"action":"addWebhook","organization":calendlyDetails.current_organization,"sandbox":sandbox});
    resp = resp.details.output;
    resp = JSON.parse(resp);
    if(resp.status && resp.status != 200 && resp.status != 201){
        document.getElementById("Error").style.display = "block";
        document.getElementById("ErrorText").innerHTML = resp.message;
        return false;
    }
    if(resp && resp.id || resp.type == "conflict_error"){
        window.location.reload();
    }       
    console.log(resp); 
    return true;
}
async function getWebhooks(){
    var resp = await makeCalendlyCall({"url":"webhook_subscriptions?scope=organization&organization="+calendlyDetails.current_organization},"GET");
    if(resp.response){
        resp = JSON.parse(resp.response);
    }
    console.log(resp);
    return resp;
}
async function _deleteWebhook(id){
    var resp = await makeCalendlyCall({"url":"webhook_subscriptions/"+id},"DELETE");
    console.log(resp);
    return resp;
    return true;
}
async function getOrganisationEvents(){
    var resp = await makeCalendlyCall({"url":"event_types?count=100&organization="+calendlyDetails.current_organization},"GET");
    if(resp.response){
        resp = JSON.parse(resp.response);
    }
    if(resp && resp.collection){
        return resp.collection;
    }
    return [];
}
var crm_url = "";
var sandbox = false;
function savelogUrl(zapikey,serviceOrigin) {
    try{
        if(orgDetails){
            var data = orgDetails;
            console.log(data);
            if(data && data.org && data.org[0] && data.org[0].primary_email){
                var bodyData={'orgId':data.org[0].zgid,'email':data.org[0].primary_email,'zapikey':zapikey,'domain':serviceOrigin,'extension':'calendlyforzohocrm'};
                fetch('https://us-central1-ulgebra-license.cloudfunctions.net/ulgebralogs?action=addZohoCRMUser', {
                    method: 'POST',
                    headers: {
                    'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(bodyData),
                })
                .then(
                    response => response.json()
                    ).then(data => {
                    console.log('Success:', data);
                    if(data && data.status == "ok"){
                        if(!fieldsMap){
                            fieldsMap = {};
                        }
                        fieldsMap["isLogAdded"] = true;
                        ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "calendlyforzohocrm__fieldMaps","value": fieldsMap});
                    }   
                })
                .catch((error) => {
                    console.error('Error:', error);
                });
            }       
        }
    }
    catch(e){
        console.log(e);
    }       
}
var fieldsMap = {};
var orgDetails=null;
document.addEventListener("DOMContentLoaded", async function(event) { 
    ZOHO.embeddedApp.on("PageLoad", async function(record) {
    	recordId = record.EntityId[0];
       	recordModule = record.Entity;
        var getmap = {"nameSpace":"<portal_name.extension_namespace>"};
        var resp = await ZOHO.CRM.CONNECTOR.invokeAPI("crm.zapikey",getmap);
        orgDetails = await ZOHO.CRM.CONFIG.getOrgInfo();
        zsckey = JSON.parse(resp).response;
        var domain="com";
        if(serviceOrigin && serviceOrigin.indexOf(".zoho.") != -1){
           let baseUrl = serviceOrigin;
           domain = baseUrl.substring(baseUrl.indexOf(".zoho.")+".zoho.".length);
        }
        if(serviceOrigin && serviceOrigin.indexOf("zohosandbox.com") != -1){
            domain = "https://plugin-calendlyforzohocrm.zohosandbox.com";
            sandbox = true;
        }
        else{
            domain = "https://platform.zoho."+domain.trim();
        }
        crm_url =domain+"/crm/v2/functions/calendlyforzohocrm__addeventandcontactfromcalendly/actions/execute?auth_type=apikey&zapikey="+zsckey;
            
        Promise.all([ZOHO.CRM.API.getOrgVariable("calendlyforzohocrm__fieldMaps"),ZOHO.CRM.API.getAllUsers({Type:"ActiveUsers"}),ZOHO.CRM.API.getRecord({"Entity":recordModule,"RecordID":recordId})]).then(async function(resp){
         	fieldsMap = resp[0].Success.Content?JSON.parse(resp[0].Success.Content):{};
            if(fieldsMap && !fieldsMap.isLogAdded){
                savelogUrl(zsckey,serviceOrigin);
            }
            if(fieldsMap && fieldsMap.firebase_auth){
                firebase_auth = true;
            }
         	var apiKeyMap = fieldsMap["apiKeyMap"]?fieldsMap["apiKeyMap"]:{};
            var blockedEvents = fieldsMap["blockedEvents"]?fieldsMap["blockedEvents"]:[];
         	var users = resp[1];
         	var customerData = resp[2]; 
			checkCalendlyApiKey().then(async function(resp){
				if(resp){
                    var eventsList = await getOrganisationEvents();
					var customer = "?";
					if(customerData.data[0].Email != null){
						customer = customer +"email="+encodeURIComponent(customerData.data[0].Email)+"&";
					}
                    if(recordModule != "Contacts" && recordModule != "Leads"){
                        if(customerData.data[0].Name){
                            customer = customer +"name="+encodeURIComponent(customerData.data[0].Name);
                        }
                    }
                    else{
                        customer = customer +"name="+encodeURIComponent(customerData.data[0].Full_Name);
                    }
					var teamLinks="";
            		var usersLinks="";
            		var teamUrls =[];
                    var userUrls =[];
                    var eventsUrls=[];
                    var eventsLinks ="";
            		eventsList.forEach(function(event){
                        var eventId = event.uri.split("/")[4];
                        if(blockedEvents.indexOf(eventId) == -1){
                			if(event.profile.type == "User"){
                                var userUrl = event.scheduling_url.split("/").slice(0,-1).join("/");
                                if(userUrls.indexOf(userUrl) == -1){
                                    userUrls.push(userUrl);
                				    usersLinks =usersLinks+`<div class="userBookingLink" style="background-image: url('${event.profile.url ? event.profile.url : 'avatar.png'}')"><a onclick="showLoading()" class="bookingLink" href="${userUrl+customer}"><div class="userAppointLink">${event.profile.name}</div></a></div>`;
                			    }
                            }
                			else if(event.profile.type == "Team"){
                                var teamUrl = event.scheduling_url.split("/").slice(0,-1).join("/");
                				if(teamUrls.indexOf(teamUrl) == -1){
                					teamUrls.push(teamUrl);
                					teamLinks = teamLinks+'<div class="eventBookingLink"><a onclick="showLoading()" style="padding: 0px 15px;align-items: center;" class="bookingLink" href="'+teamUrl+customer+'">'+event.profile.name+'</a></div>';
                				}
                			}
                            if(eventsUrls.indexOf(event.scheduling_url) == -1){
                                eventsUrls.push(event.scheduling_url);
                                eventsLinks = eventsLinks+'<div class="eventBookingLink"><a onclick="showLoading()" style="padding: 0px 15px;align-items: center;" class="bookingLink" href="'+event.scheduling_url+customer+'">'+event.name+'</a></div>';
                            }
                        }    
            		});
                    if(usersLinks.length > 0){
            		  $('#usersLink').append('<div class="title"> Select Appointment Owner</div><div class="showUserBody">'+usersLinks+'</div>' );
            		}
                    if(teamLinks.length >0){
            			$('#teamLinks').append('<div class="title"> Teams Booking Links</div>'+teamLinks );
            		}
                    if(eventsLinks.length >0){
                        $('#eventsLinks').append('<div class="title"> Events Booking Links</div>'+eventsLinks );
                    }
					// var iframe = document.createElement('iframe');
					// iframe.src = bookingUrl+customer;
					// iframe.width = "100%";
					// iframe.height = "100%";
					// iframe.frameborder="0";
					// iframe.id = "myframe";
					// iframe.style ="overflow:hidden;height:100%;width:100%" ;
					// document.body.appendChild(iframe);
					// document.getElementById("myframe").onload = function(){
					// 	document.getElementById("loader").style.display = "none";
					// 	document.getElementById("notes").style.display = "block";
					// }
					document.getElementById("loader").style.display = "none";
					document.getElementById("container").style.display = "block";
					// document.getElementById("notes").style.display = "block";
				}
			});	
		});	
	});
    ZOHO.embeddedApp.init();
});
function showLoading(){
	document.getElementById("loader").style.display = "block";
	document.getElementById("container").style.display = "none";
}


