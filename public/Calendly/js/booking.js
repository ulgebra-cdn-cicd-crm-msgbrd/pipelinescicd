var recordId;
var recordModule;
function checkCalendlyApiKey(apiKey){
	apiRequest =   {
        url: "https://calendly.com/api/v1/echo",
        headers: {
        "X-TOKEN": apiKey,
        }
    }
    return ZOHO.CRM.HTTP.get(apiRequest).then(function(apiDetails){
    	apiDetails = JSON.parse(apiDetails);
    	if(apiDetails.status == 401 || apiDetails.type == "authentication_error"){
    		document.getElementById("error").innerHTML = apiDetails.message+", Please change your Calendly API key in Extension Settings";
    		document.getElementById("loader").style.display = "none";
    		return false;
    	}
    	else{
    		document.getElementById("error").innerHTML = "";
    		return true;
    	}
    });	
}
document.addEventListener("DOMContentLoaded", function(event) { 
    ZOHO.embeddedApp.on("PageLoad", function(record) {
    	recordId = record.EntityId[0];
       	recordModule = record.Entity;
        Promise.all([ZOHO.CRM.API.getOrgVariable("calendlyforzohocrm__Api_Key"),ZOHO.CRM.API.getOrgVariable("calendlyforzohocrm__fieldMaps"),ZOHO.CRM.API.getAllUsers({Type:"AllUsers"}),ZOHO.CRM.API.getRecord({"Entity":recordModule,"RecordID":recordId})]).then(async function(resp){
         	var apiKeyData = resp[0];
         	var fieldsMap = resp[1].Success.Content?JSON.parse(resp[1].Success.Content):{};
         	var apiKeyMap = fieldsMap["apiKeyMap"]?fieldsMap["apiKeyMap"]:{};
            var blockedEvents = fieldsMap["blockedEvents"]?fieldsMap["blockedEvents"]:[];
         	var users = resp[2];
         	var customerData = resp[3]; 
			if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content){
				var apiKey = apiKeyData.Success.Content;

				checkCalendlyApiKey(apiKey).then(function(resp){
    				if(resp){
    					var apiKeyList = [];
    					apiKeyList.push(apiKey);
    					users.users.forEach(function(user){
    						if(apiKeyMap[user.id] && apiKeyList.indexOf(apiKeyMap[user.id]) == -1){
    							apiKeyList.push(apiKeyMap[user.id]);
    						}
    					})
    					eventsFetchList =[];
    					apiKeyList.forEach(function(apik){
    						var userRequest =   {
			                    url: "https://calendly.com/api/v1/users/me/event_types?include=owner",
			                    headers: {
			                    "X-TOKEN": apik,
			                    }
			                }
			                eventsFetchList.push(ZOHO.CRM.HTTP.get(userRequest));

    					})
		                
		                Promise.all(eventsFetchList).then(function(responses){
		                	var eventsList =[];
		                	responses.forEach(function(eventDetails){
		                		var events = JSON.parse(eventDetails);
                                eventsList = eventsList.concat(events.data);
		                		eventsList = eventsList.concat(events.included);
		                	});
							var customer = "?";
							if(customerData.data[0].Email != null){
								customer = customer +"email="+encodeURIComponent(customerData.data[0].Email)+"&";
							}
                            if(recordModule != "Contacts" && recordModule != "Leads"){
                                if(customerData.data[0].Name){
                                    customer = customer +"name="+encodeURIComponent(customerData.data[0].Name);
                                }
                            }
                            else{
                                customer = customer +"name="+encodeURIComponent(customerData.data[0].Full_Name);
                            }
							var teamLinks="";
                    		var usersLinks="";
                    		var teamUrls =[];
                            var eventsUrls=[];
                            var eventsLinks ="";
                    		eventsList.forEach(function(event){
                                if(blockedEvents.indexOf(event.id) == -1){
                        			if(event.type == "users"){
                        				usersLinks =usersLinks+`<div class="userBookingLink" style="background-image: url('${event.attributes.avatar.url ? event.attributes.avatar.url : 'avatar.png'}')"><a onclick="showLoading()" class="bookingLink" href="${event.attributes.url+customer}"><div class="userAppointLink">${event.attributes.name}</div></a></div>`;
                        			}
                        			else if(event.type == "teams"){
                        				if(teamUrls.indexOf(event.attributes.url) == -1){
                        					teamUrls.push(event.attributes.url);
                        					teamLinks = teamLinks+'<div class="eventBookingLink"><a onclick="showLoading()" style="padding: 0px 15px;align-items: center;" class="bookingLink" href="'+event.attributes.url+customer+'">'+event.attributes.name+'</a></div>';
                        				}
                        			}
                                    else if(event.type == "event_types"){
                                        if(eventsUrls.indexOf(event.attributes.url) == -1){
                                            eventsUrls.push(event.attributes.url);
                                            eventsLinks = eventsLinks+'<div class="eventBookingLink"><a onclick="showLoading()" style="padding: 0px 15px;align-items: center;" class="bookingLink" href="'+event.attributes.url+customer+'">'+event.attributes.name+'</a></div>';
                                        }
                                    }
                                }    
                    		});
                    		$('#usersLink').append('<div class="title"> Select Appointment Owner</div><div class="showUserBody">'+usersLinks+'</div>' );
                    		if(teamLinks.length >0){
                    			$('#teamLinks').append('<div class="title"> Teams Booking Links</div>'+teamLinks );
                    		}
                            if(eventsLinks.length >0){
                                $('#eventsLinks').append('<div class="title"> Events Booking Links</div>'+eventsLinks );
                            }
							// var iframe = document.createElement('iframe');
							// iframe.src = bookingUrl+customer;
							// iframe.width = "100%";
							// iframe.height = "100%";
							// iframe.frameborder="0";
							// iframe.id = "myframe";
							// iframe.style ="overflow:hidden;height:100%;width:100%" ;
							// document.body.appendChild(iframe);
							// document.getElementById("myframe").onload = function(){
							// 	document.getElementById("loader").style.display = "none";
							// 	document.getElementById("notes").style.display = "block";
							// }
							document.getElementById("loader").style.display = "none";
							document.getElementById("container").style.display = "block";
							// document.getElementById("notes").style.display = "block";
						});	
					}
				});	
			}	
			else{
				document.getElementById("error").innerHTML = " Please enter your Calendly API key in Extension Settings page";
    			document.getElementById("loader").style.display = "none";
			}
		});	
	});
    ZOHO.embeddedApp.init();
});
function showLoading(){
	document.getElementById("loader").style.display = "block";
	document.getElementById("container").style.display = "none";
}


