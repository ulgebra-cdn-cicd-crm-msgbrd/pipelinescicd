		var emailContentForContacts = [];
		var emailContentForLeads = [];
		var emailContentText = "";
		var startIndex =0;
		var endIndex = 0;
		var currentEditor="";
		var subject="";
		var sel;
		var range;
		var eventTypes;
		var fieldMappingMap = {};
		var fromFields =[];
		var users=[];
		// var contactsFields = ["Event Description","Assistant", "Asst_Phone", "Department", "Description", "Fax","Home_Phone", "Lead_Source", "Mailing_City", "Mailing_Country", "Mailing_Street", "Mailing_Zip", "Mobile", "Other_City", "Other_Country", "Other_Phone", "Other_State", "Other_Street", "Other_Zip", "Phone", "Salutation", "Secondary_Email", "Skype_ID", "Title", "Twitter","notes"];
		// var leadsFields = ["Event Description","Annual_Revenue", "City", "Company", "Country","Description", "Designation", "Email", "Fax","Industry","Lead_Source", "Lead_Status", "Mobile", "No_of_Employees", "Phone", "Rating", "Salutation", "Secondary_Email", "Skype_ID", "State", "Street","Twitter", "Website", "Zip_Code","notes"];
        var contactsFields =[];
        var leadsFields = [];
        var eventsFields=[];
        var commomFields=[{"api_name":"Event Description","field_label":"Event Description"},{"api_name":"notes","field_label":"Notes"}];
        var crm_url="";
        const urlParams = new URLSearchParams(window.location.search);
		var serviceOrigin = urlParams.get('serviceOrigin'); 
		var selectedModule="";
		var emailField="";
		var sandbox = false;
		var apiKey = null;
		var blockedEvents=[];
		var eventsModuleMap={};
		var moduleFieldsMap={};
		var version=null;
		var orgDetails = null;
		var zsckey=null;
		var firebase_auth = false;

		var crmRecAddModulesList = [];

		async function makeCalendlyCall(data,method){
			if(firebase_auth){
				data.method = method;
				data.zapikey = zsckey;
				return fetch("https://us-central1-ulgebra-license.cloudfunctions.net/makeCalendlyHTTPRequestFromZoho", {
			        "method" :"POST",
			        "body":JSON.stringify(data)
			    })
			    .then(function (response) {  
			        console.log(response);
			        return response.json();
			    })
			    .then(function (responseJSON) {
			        console.log(responseJSON);
			        if(responseJSON && responseJSON.error && responseJSON.error.message == "user_not_found"){
			        	 return null;
			        }
			        return responseJSON;
			    })
			    .catch(function (error) {
			        console.log("Error: " + error);
			        return null;
			    });
			}
			else{
				return await ZOHO.CRM.CONNECTOR.invokeAPI("calendlyforzohocrm.calendly.calendly"+method.toLowerCase()+"calls",data);
			}

		}
        document.addEventListener("DOMContentLoaded", function(event) {
        	document.getElementById("loader").style.display = "none";
			document.getElementById("contentDiv").style.display = "block";
			document.getElementById("google_translate_element").style.display = "block";
        	ZOHO.embeddedApp.init().then(async function(){
        			// var cusotmerData = ["Owner", "Email", "$currency_symbol", "Other_Phone", "Mailing_State", "$upcoming_activity", "Other_State", "Other_Country", "Last_Activity_Time", "Department", "$process_flow", "Assistant", "Mailing_Country", "id", "$approved", "Reporting_To", "$approval", "Other_City", "Created_Time", "$editable", "Home_Phone", "$status", "Created_By", "Secondary_Email", "Description", "Vendor_Name", "Mailing_Zip", "$photo_id", "Twitter", "Other_Zip", "Mailing_Street", "Salutation", "First_Name", "Full_Name", "Asst_Phone", "Record_Image", "Modified_By", "Skype_ID", "Phone", "Account_Name", "Email_Opt_Out", "Modified_Time", "Date_of_Birth", "Mailing_City", "Title", "Other_Street", "Mobile", "Last_Name", "Lead_Source", "Tag", "Fax"];
        		var getmap = {"nameSpace":"<portal_name.extension_namespace>"};
				var resp = await ZOHO.CRM.CONNECTOR.invokeAPI("crm.zapikey",getmap);
				orgDetails = await ZOHO.CRM.CONFIG.getOrgInfo();
				zsckey = JSON.parse(resp).response;
				var domain="com";
				if(serviceOrigin && serviceOrigin.indexOf(".zoho.") != -1){
			       let baseUrl = serviceOrigin;
			       domain = baseUrl.substring(baseUrl.indexOf(".zoho.")+".zoho.".length);
			    }
			    if(serviceOrigin && serviceOrigin.indexOf("zohosandbox.com") != -1){
			        domain = "https://plugin-calendlyforzohocrm.zohosandbox.com";
			        sandbox = true;
			    }
			    else{
			        domain = "https://platform.zoho."+domain.trim();
			    }
				crm_url =domain+ "/crm/v2/functions/calendlyforzohocrm__addeventandcontactfromcalendly/actions/execute?auth_type=apikey&zapikey="+zsckey;
        		apiKey = await ZOHO.CRM.API.getOrgVariable("calendlyforzohocrm__Api_Key").then(function(apiKeyData){
	            	if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content && apiKeyData.Success.Content != "null"){
	            		return apiKeyData.Success.Content;
					}	
					return null;
	            });	

        		try{        			
					ZOHO.CRM.META.getModules().then(function(data){
						console.log(data);	
						if(data && data.modules && data.modules.length > 0)
						{
							let modules = data.modules;
							for(var i=0;i<modules.length;i++)
							{
								if(modules[i].api_name == "Leads" || modules[i].api_name == "Contacts")
								{
									crmRecAddModulesList.push(modules[i].api_name.toString());
								}
							}
						}

						$("#modules").empty();
						$("#modules").append(`<option value="none">No modules</option>`);
						if(crmRecAddModulesList && crmRecAddModulesList.length>0)
						{
							$("#modules").empty();
							for(var i=0;i<crmRecAddModulesList.length;i++)
							{
								$("#modules").append(`<option value="${crmRecAddModulesList[i]}">${crmRecAddModulesList[i]}</option>`);
							}
						}
					});
        		}
    			catch(e){
    				console.log(e);
    			}

	            try{
		            var versionDetails = await ZOHO.CRM.FUNCTIONS.execute("calendlyforzohocrm__addeventandcontactfromcalendly", {"action":"getVersion"});
	    			if(versionDetails && versionDetails.details.output){
	    				version = parseInt(versionDetails.details.output);
	    			}	
    			}
    			catch(e){
    				console.log(e);
    			}
    			var messageData = await ZOHO.CRM.API.getOrgVariable("calendlyforzohocrm__fieldMaps");
    			if(messageData && messageData.Success && messageData.Success.Content){
					fieldMappingMap = JSON.parse(messageData.Success.Content);
				}
				if(fieldMappingMap && fieldMappingMap.firebase_auth){
					firebase_auth = true;
				}
				checkCalendlyApiKey().then(async function(resp){
    				if(resp){
    					// userRequest1 =   {
		          //           url: "https://calendly.com/api/v1/users/me",
		          //           headers: {
		          //           "X-TOKEN": apiKey,
		          //           }
		          //       }
		          //       ZOHO.CRM.HTTP.get(userRequest1).then(function(userDetails){

	                	addListItem("EmailForContacts","Your Booking Link","dropdown-header");
                		addListItem("EmailForContacts",calendlyDetails.name,"dropdown-item",calendlyDetails.scheduling_url+"?%customerInfo%");
                		addListItem("EmailForLeads","Your Booking Link","dropdown-header");
                		addListItem("EmailForLeads",calendlyDetails.name,"dropdown-item",calendlyDetails.scheduling_url+"?%customerInfo%");
                		addListItem("WhatsAppForContacts","Your Booking Link","dropdown-header");
                		addListItem("WhatsAppForContacts",calendlyDetails.name,"dropdown-item",calendlyDetails.scheduling_url+"?%customerInfo%");
                		addListItem("WhatsAppForLeads","Your Booking Link","dropdown-header");
                		addListItem("WhatsAppForLeads",calendlyDetails.name,"dropdown-item",calendlyDetails.scheduling_url+"?%customerInfo%");
		                
						// userRequest2 =   {
		    //                 url: "https://calendly.com/api/v1/users/me/event_types?include=owner",
		    //                 headers: {
		    //                 "X-TOKEN": apiKey,
		    //                 }
		    //             }

		                // ZOHO.CRM.HTTP.get(userRequest2).then(async function(eventDetails){
		                getOrganisationEvents().then(async function(eventDetails){
		                	eventTypes = eventDetails;
		                    var userEvents = await getUserEvents();	
		                    eventTypes = eventTypes.concat(userEvents);
		                    // userEvents.forEach(function(userevent){
		                    // 	if(userevent.active == true && eventIds.indexOf(userevent.uri) == -1){
		                    // 		eventTypes.push(userevent);
		                    // 	}
		                    // })
                    		addListItem("EmailForContacts","Events Booking Link","dropdown-header");
                    		addListItem("EmailForLeads","Events Booking Link","dropdown-header");
                    		addListItem("WhatsAppForContacts","Events Booking Link","dropdown-header");
                    		addListItem("WhatsAppForLeads","Events Booking Link","dropdown-header");
                    		var eventsLinks = "";
                    		var eventsLinksForWhatsapp = "";
                    		eventDetails.forEach(function(event){
                    			if(event.active == true){
	                    			addListItem("EmailForContacts",event.name,"dropdown-item",event.scheduling_url+"?%customerInfo%");
	                    			addListItem("EmailForLeads",event.name,"dropdown-item",event.scheduling_url+"?%customerInfo%");
	                    			addListItem("WhatsAppForContacts",event.name,"dropdown-item",event.scheduling_url+"?%customerInfo%");
	                    			addListItem("WhatsAppForLeads",event.name,"dropdown-item",event.scheduling_url+"?%customerInfo%");
	                    			eventsLinksForWhatsapp = eventsLinksForWhatsapp+event.scheduling_url+"?%customerInfo%<br>";
	                    			eventsLinks = eventsLinks+"<a href='"+event.scheduling_url+"?%customerInfo%"+"'>"+event.name+"</a><br>";
	                    		}	
                    		});
			    			var cusotmerData = ["Full_Name","First_Name","Last_Name"];
			    			 ZOHO.CRM.API.getOrgVariable("calendlyforzohocrm__email_content_for_contacts").then(function(messageData){
			        			var cusotmerData = ["Full_Name","First_Name","Last_Name"];
				    			addListItem("EmailForContacts","Contacts Fields","dropdown-header");
				    			cusotmerData.forEach(function(field){
				    				addListItem("EmailForContacts",field,"dropdown-item","variable");
				    			});
				    			emailContentForContacts = JSON.parse(messageData.Success.Content);
				    			for(let i=0;i <emailContentForContacts.length;i++){
									if(i==0){
						    			document.getElementById("emailContentEmailForContacts").innerHTML =emailContentForContacts[0].emailContent.replace(/<br>/g,"\n").replace("%eventsLinks%",eventsLinks);
					        			document.getElementById("subjectEmailForContacts").innerHTML =emailContentForContacts[0].subject;
									}
									else{
										cloneEditor(i,emailContentForContacts[i],"EmailForContacts");
									}
								}	
							});
							ZOHO.CRM.API.getOrgVariable("calendlyforzohocrm__email_content_for_leads").then(function(messageData){
								var cusotmerData = ["Full_Name","First_Name","Last_Name"];
				    			addListItem("EmailForLeads","Leads Fields","dropdown-header");
				    			cusotmerData.forEach(function(field){
				    				addListItem("EmailForLeads",field,"dropdown-item","variable");
				    			});
				    			emailContentForLeads = JSON.parse(messageData.Success.Content);
								for(let i=0;i <emailContentForLeads.length;i++){
									if(i==0){
										document.getElementById("emailContentEmailForLeads").innerHTML =emailContentForLeads[0].emailContent.replace(/<br>/g,"\n").replace("%eventsLinks%",eventsLinks);
										document.getElementById("subjectEmailForLeads").innerHTML =emailContentForLeads[0].subject;
									}
									else{
										cloneEditor(i,emailContentForLeads[i],"EmailForLeads");
									}
								}
								document.getElementById("loader").style.display = "none";
								document.getElementById("contentDiv").style.display = "block";
								document.getElementById("google_translate_element").style.display = "block";
							});
							ZOHO.CRM.API.getOrgVariable("calendlyforzohocrm__whatsapp_message_for_contact").then(function(messageData){
								document.getElementById("emailContentWhatsAppForContacts").innerHTML =messageData.Success.Content.replace(/<br>/g,"\n").replace("%eventsLinks%",eventsLinksForWhatsapp);
								var cusotmerData = ["Full_Name","First_Name","Last_Name"];
				    			addListItem("WhatsAppForContacts","Contacts Fields","dropdown-header");
				    			cusotmerData.forEach(function(field){
				    				addListItem("WhatsAppForContacts",field,"dropdown-item","variable");
				    			});
							});
							ZOHO.CRM.API.getOrgVariable("calendlyforzohocrm__whatsapp_message_for_leads").then(function(messageData){
								document.getElementById("emailContentWhatsAppForLeads").innerHTML =messageData.Success.Content.replace(/<br>/g,"\n").replace("%eventsLinks%",eventsLinksForWhatsapp);
								var cusotmerData = ["Full_Name","First_Name","Last_Name"];
				    			addListItem("WhatsAppForLeads","Leads Fields","dropdown-header");
				    			cusotmerData.forEach(function(field){
				    				addListItem("WhatsAppForLeads",field,"dropdown-item","variable");
				    			});
							});
							
							if(fieldMappingMap && !fieldMappingMap.isLogAdded){
								savelogUrl(zsckey,serviceOrigin);
							}
							if(fieldMappingMap && fieldMappingMap.lar_enabled){
								document.getElementById("assignment_rule").style.display="inline-block";
								if(fieldMappingMap.lar_id){
									document.getElementById("lar_id").value = fieldMappingMap.lar_id;
								}
							}
							// if(fieldMappingMap && fieldMappingMap.clayoutIdShow){
							// 	document.getElementById("commonLayout").style.display="block";
							// }
							// if(fieldMappingMap && fieldMappingMap.clayoutId){
							// 	 document.getElementById("clayoutId").value = fieldMappingMap.clayoutId;
							// }
							try{
								if(crmRecAddModulesList.includes("Leads") == true)
								{
									try
									{
										var leadsFieldsResp = await ZOHO.CRM.META.getFields({"Entity":"Leads"});
										leadsFields = leadsFieldsResp.fields;
										moduleFieldsMap["Leads"]=leadsFieldsResp.fields;
									}
									catch(e)
									{
										console.log(e);
									}			
										
								}
								if(crmRecAddModulesList.includes("Contacts") == true)
								{
									try
									{	
										var contactsFieldsResp = await ZOHO.CRM.META.getFields({"Entity":"Contacts"});
										contactsFields = contactsFieldsResp.fields;
										moduleFieldsMap["Contacts"]= contactsFieldsResp.fields;
									}
									catch(e)
									{
										console.log(e);
									}	
								}
							    var eventsFieldsResp = await ZOHO.CRM.META.getFields({"Entity":"Events"});
								eventsFields = eventsFieldsResp.fields;
								moduleFieldsMap["Events"]= eventsFieldsResp.fields;
								if(fieldMappingMap.customModule){
									emailField = fieldMappingMap.emailField;
									emailField = "Email";
									$("#modules").append(`<option value="${fieldMappingMap.customModule}">${fieldMappingMap.customModule}</option>`)
									var customModuleFieldsResp = await ZOHO.CRM.META.getFields({"Entity":fieldMappingMap.customModule});
									customModuleFields = customModuleFieldsResp.fields;
									moduleFieldsMap[fieldMappingMap.customModule]= customModuleFields.fields;
								}
							}
							catch(e){
								console.log(e);
							}	
							ZOHO.CRM.API.getOrgVariable("calendlyforzohocrm__Module").then(async function(moduleData){
				            	if(moduleData && moduleData.Success && moduleData.Success.Content){
									document.getElementById("modules").value = moduleData.Success.Content;
									selectedModule = moduleData.Success.Content;
									if(selectedModule && moduleFieldsMap[selectedModule]){
										fromFields = moduleFieldsMap[selectedModule];
									}
									else{
										selectedModule = "Contacts";
										fromFields = contactsFields;
									}
									blockedEvents=fieldMappingMap.blockedEvents?fieldMappingMap.blockedEvents:[];
									eventsModuleMap=fieldMappingMap.eventsModuleMap?fieldMappingMap.eventsModuleMap:{};
									var fieldMapUi ="";
									var eventFieldMap="";
									//var userEvents = await getUserEvents();
									//eventTypes = eventTypes.concat(userEvents);
									var eventIdsList=[];
									eventTypes.forEach(function(event){
										let eventId = event.uri.split("/")[4];
										if(eventIdsList.indexOf(eventId) == -1){
											eventIdsList.push(eventId);
											if(!fieldMappingMap[eventId]){
												fieldMappingMap[eventId]={"Event Description":"all_answers"};
											}
											let moduleSelectbox="";
											if(version > 94){
												moduleSelectbox=`<select id="${eventId}_module" onchange="selectModuleForEvents('${eventId}')"><option value="Contacts">Contacts</option><option value="Leads">Leads</option></select>`;
											}
											eventFieldMap = eventFieldMap + '<div class="eventFieldMap"><div class="eventname collapsible" id="'+eventId+'_name" onclick="toggleFieldMap('+"'"+eventId+"'"+')" style="display:flex;flex-direction:row;"><span style="flex-grow:10;" >'+event.name+' ('+event.scheduling_url.split("/")[3]+') Fields Mapping</span> <span style="flex-grow:1;align-slef:flex-end;"><label onclick="stopClick()" class="switch2"><input id="'+eventId+'_enable" type="checkbox" onclick="enableDisableEvent('+"'"+eventId+"'"+')"><span class="slider2 round"></span></label></span></div><div class="fieldMapperContainer" id="'+eventId+'">'+moduleSelectbox+getfieldMapping(eventId)+'</div></div>';
									 	}
									});
									$('#fieldsMap').html(eventFieldMap); 
									eventTypes.forEach(function(event){
										let eventId = event.uri.split("/")[4];
										if(version > 94){
											document.getElementById(eventId+"_module").value = eventsModuleMap[eventId]?eventsModuleMap[eventId]:selectedModule;
										}
										document.getElementById(eventId+"_enable").checked = (blockedEvents.indexOf(eventId) == -1)? true  : false;
										document.getElementById(eventId+"_name").style.backgroundColor= (blockedEvents.indexOf(eventId) == -1)? "grey"  : "#7777774d";
									});
									ZOHO.CRM.API.getAllUsers({Type:"ActiveUsers"}).then(function(data){
										    console.log(data);
										    users = data.users;
										    var usersLayoutUi ='<div class="eventFieldMap"><div class="eventname collapsible" id="layout_name" onclick="toggleFieldMap('+"'layout'"+')">Layout Settings</div><div class="fieldMapperContainer" id="layout">'+getLayoutMapping()+'</div></div>';
										$('#layoutMap').html(usersLayoutUi); 
										if(document.getElementById("apiKeyMap")){
											var apiLayoutUi ='<div class="eventFieldMap"><div class="eventname collapsible" id="api_name" onclick="toggleFieldMap('+"'api'"+')">Calendly API Key Map</div><div class="fieldMapperContainer" id="api">'+getApiMapping()+'</div></div>';
											$('#apiKeyMap').html(apiLayoutUi); 
										}	
										if(document.getElementById("userMap")){
											var userLayoutUi ='<div class="eventFieldMap"><div class="eventname collapsible" id="user_name" onclick="toggleFieldMap('+"'user'"+')">Calendly - ZOHO CRM Users Map</div><div class="fieldMapperContainer" id="user">'+getUserMapping()+'</div></div>';
											$('#userMap').html(userLayoutUi); 
										}	
									});
										
								}	
							});	

                    	});
                    }
                    else{
                    	document.getElementById("loader").style.display = "none";
						document.getElementById("contentDiv").style.display = "block";
						document.getElementById("google_translate_element").style.display = "block";
                    }
                });
		        // var orgData = ["max_per_page", "country", "photo_id", "city", "description", "mc_status", "gapps_enabled", "street", "alias", "currency", "id", "state", "fax", "employee_count", "zip", "website", "ezgid", "currency_symbol", "oauth_presence", "mobile", "currency_locale", "primary_zuid", "zia_portal_id", "time_zone", "zgid", "country_code", "license_details", "phone", "company_name", "privacy_settings", "primary_email", "iso_code"];
		       
	    //         ZOHO.CRM.API.getOrgVariable("calendlyforzohocrm__Api_Key").then(function(apiKeyData){
	    //         	if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content){
					// 	document.getElementById("apikey").value = apiKeyData.Success.Content;
	    //         		checkCalendlyApiKey(apiKeyData.Success.Content);
					// }	
	    //         });	
	            ZOHO.CRM.API.getOrgVariable("calendlyforzohocrm__SalesSignal").then(function(salesSignalData){
	            	if(salesSignalData && salesSignalData.Success && salesSignalData.Success.Content){
						document.getElementById("salesSignal").checked = (salesSignalData.Success.Content == "true");
					}	
	            });	
	            ZOHO.CRM.API.getOrgVariable("calendlyforzohocrm__Activity").then(function(activityData){
	            	if(activityData && activityData.Success && activityData.Success.Content){
						document.getElementById("activities").value = activityData.Success.Content;
						if(activityData.Success.Content == "Tasks"){
							document.getElementById("note").innerHTML = "Appointment Start date will be mapped as a Due Date for the Task record in zoho CRM";
						}
						else{
							document.getElementById("note").innerHTML ="";
						}
					}	
	            });	
	           
	        });  
			
        });

		function savelogUrl(zapikey,serviceOrigin) {
			try{
				if(orgDetails){
					var data = orgDetails;
					console.log(data);
					if(data && data.org && data.org[0] && data.org[0].primary_email){
						var bodyData={'orgId':data.org[0].zgid,'email':data.org[0].primary_email,'zapikey':zapikey,'domain':serviceOrigin,'extension':'calendlyforzohocrm'};
						fetch('https://us-central1-ulgebra-license.cloudfunctions.net/ulgebralogs?action=addZohoCRMUser', {
							method: 'POST',
							headers: {
							'Content-Type': 'application/json',
							},
							body: JSON.stringify(bodyData),
						})
						.then(
							response => response.json()
							).then(data => {
							console.log('Success:', data);
							if(data && data.status == "ok"){
								if(!fieldMappingMap){
									fieldMappingMap = {};
								}
								fieldMappingMap["isLogAdded"] = true;
			        			ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "calendlyforzohocrm__fieldMaps","value": fieldMappingMap});
			        		}	
						})
						.catch((error) => {
							console.error('Error:', error);
						});
				    }		
				}
		    }
		    catch(e){
		    	console.log(e);
		    }		
		}
        function selectModuleForEvents(eventId) {
        	var sModule = document.getElementById(eventId+"_module").value; 
        	eventsModuleMap[eventId]=sModule;
        	fieldMappingMap["eventsModuleMap"] = eventsModuleMap;
        	ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname":"calendlyforzohocrm__fieldMaps","value":fieldMappingMap});
        	ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname":"calendlyforzohocrm__fieldMaps","value":fieldMappingMap});
        	var moduleSelectbox=`<select id="${eventId}_module" onchange="selectModuleForEvents('${eventId}')"><option value="Contacts">Contacts</option><option value="Leads">Leads</option></select>`;
        	document.getElementById(eventId).innerHTML=moduleSelectbox+getfieldMapping(eventId);
        	document.getElementById(eventId+"_module").value = sModule;
        }
        async function saveRule(){
        	var lar_id = $("#lar_id").val().trim();
    		document.getElementById("Error").style.display = "block";
        	document.getElementById("ErrorText").innerText="Saving Configuration ..."
    		fieldMappingMap["lar_id"]=lar_id;
    		await ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "calendlyforzohocrm__fieldMaps","value": fieldMappingMap});
    		document.getElementById("Error").style.display = "none";
        }
      //   async function savecLayoutId(){
      //   	var clayoutId = $("#clayoutId").val().trim();
    		// document.getElementById("Error").style.display = "block";
      //   	document.getElementById("ErrorText").innerText="Saving Configuration ..."
    		// fieldMappingMap["clayoutId"]=clayoutId;
    		// await ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "calendlyforzohocrm__fieldMaps","value": fieldMappingMap});
    		// document.getElementById("Error").style.display = "none";
      //   }
        
        function stopClick(){
        	event.stopPropagation();
        }
        function enableDisableEvent(eventId){
        	event.stopPropagation();
			if(fieldMappingMap["blockedEvents"]){
				blockedEvents = fieldMappingMap["blockedEvents"];
			}
			var value = document.getElementById(eventId+"_enable").checked;
        	var eventIndex = blockedEvents.indexOf(eventId);
        	if(value && eventIndex != -1){
        		blockedEvents.splice(eventIndex, 1);
        		document.getElementById(eventId+"_name").style.backgroundColor="grey";

        	}
        	else if(eventIndex === -1){
        		  blockedEvents.push(eventId);
        		  document.getElementById(eventId+"_name").style.backgroundColor="#7777774d";
        	}
        	fieldMappingMap["blockedEvents"]=blockedEvents;
	        ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname":"calendlyforzohocrm__fieldMaps","value":fieldMappingMap});
        }
   //      async function getUserEvents(){
   //      	var apiKeyMap = {};
			// if(fieldMappingMap["apiKeyMap"]){
			// 	apiKeyMap = fieldMappingMap["apiKeyMap"];
			// }
			// var apiKeyList=[];
			// var adminApiKey = document.getElementById("apikey").value;
			// Object.keys(apiKeyMap).forEach(function(userId){
			// 	if(apiKeyMap[userId] && (adminApiKey != apiKeyMap[userId])){
			// 		apiKeyList.push(apiKeyMap[userId]);
			// 	}
			// })
			// var eventsApiList=[];
			// apiKeyList.forEach(function(apiKey){
			// 	var eventsApiMap =  {
   //                  url: "https://calendly.com/api/v1/users/me/event_types?include=owner",
   //                  headers: {
   //                  "X-TOKEN": apiKey,
   //                  }
   //              }
			// 	eventsApiList.push(ZOHO.CRM.HTTP.get(eventsApiMap));
			// })
			// var events=[];
			// await Promise.all(eventsApiList).then(function(eventsResponses){
			// 	eventsResponses.forEach(function(eventresponse){
			// 		if(eventresponse){
			// 			events = events.concat(JSON.parse(eventresponse).data)
			// 		}
			// 	})
			// });
			// return events;
   //      }
        function getUserMapping(){
        	var userMap = {};
			if(fieldMappingMap["userMap"]){
				userMap = fieldMappingMap["userMap"];
			}
			var calendlyusers = Object.keys(userMap);
			var result ='';
			var addButton =true ;
			if(!calendlyusers.length){
				result = result+getUserMapField("",userMap,"user",addButton);
			}
			calendlyusers.forEach(function(user){
				result = result+getUserMapField(user,userMap,"user",addButton);
				addButton=false
			});	
			return result;
        }
        function getUserMapField(user,layoutMap,type,isAdd){
			var userId ='';
			if(user && layoutMap[user]){
				userId = layoutMap[user];
			}
			var fromField ='<input  class="layout" type="text" value="'+user+'" onchange="saveUserMap('+"'cal_user'"+',this)"></input>'; 	
			var usersUIList="";
			var selectedUser="<option value=''>Select User</option>";
			users.forEach(function(user){
				if(user.id == userId){
					selectedUser = '<option value="'+user.id+'">'+user.full_name+'</option>';
				}
				else{
					usersUIList = usersUIList+'<option value="'+user.id+'">'+user.full_name+'</option>';
				}
			});
			usersUIList = selectedUser+usersUIList; 
			var toField='<div class="userDrop"><div class="styled-select slate fieldMap"><select name="field4" onchange="saveUserMap('+"'crm_user'"+',this)"> '+usersUIList+'</select></div></div>';  
			var arrow =	'<div class="arrowIcon" style="padding-left:5px;padding-right:5px;width:9%;"> <i class="material-icons ">arrow_forward</i></div>';
			var deleteField = '<div class="arrowIcon" style="padding-left:5px;padding-right:5px;width:7%;float:right;cursor:pointer;"> <i class="material-icons" onclick="deleteUserMap(this)">delete</i> </div>';
			if(isAdd){
				deleteField = '<div class="arrowIcon" style="padding-left:5px;padding-right:5px;width:7%;float:right;cursor:pointer;"> <i class="material-icons" onclick="addUserMap(this)">add</i> </div>';
			}
			var fieldMap ='<div  class="fieldMapp">'+fromField+arrow+toField+deleteField+'</div>';
			return fieldMap;
		}
		function addUserMap(element){
			var userId ='';
			var type ="user"
			var fromField ='<input  class="layout" type="text" value="" onchange="saveUserMap('+"'cal_user'"+',this)"></input>'; 	
			var usersUIList="";
			var selectedUser="<option value=''>Select User</option>";
			users.forEach(function(user){
				usersUIList = usersUIList+'<option value="'+user.id+'">'+user.full_name+'</option>';
			});
			usersUIList = selectedUser+usersUIList; 
			var toField='<div class="userDrop"><div class="styled-select slate fieldMap"><select name="field4" onchange="saveUserMap('+"'crm_user'"+',this)"> '+usersUIList+'</select></div></div>';  
			var arrow =	'<div class="arrowIcon" style="padding-left:5px;padding-right:5px;width:9%;"> <i class="material-icons ">arrow_forward</i></div>';
			var deleteField = '<div class="arrowIcon" style="padding-left:5px;padding-right:5px;width:7%;float:right;cursor:pointer;"> <i class="material-icons" onclick="deleteUserMap(this)">delete</i> </div>';
			var fieldMap ='<div  class="fieldMapp">'+fromField+arrow+toField+deleteField+'</div>';
			$("#user").append(fieldMap);
		}
		async function saveUserMap(type,element){
			document.getElementById("Error").style.display = "block";
        	document.getElementById("ErrorText").innerText="Saving Configuration ..."
			var saveElement ;
			if(type == "cal_user"){
				saveElement = element.parentElement
			}
			else if(type == "crm_user"){
				saveElement = element.parentElement.parentElement.parentElement;
			}
			var fieldsMap = {};
			var key =saveElement.children[0].value;
    		var value = saveElement.children[2].children[0].children[0].value;
    		if(key != "" && value != ""){ 
    			var mapElements = saveElement.parentElement.children;
    			for(let i=0;i<mapElements.length;i++){
    				var mapEle = mapElements[i];
    				let mkey =mapEle.children[0].value;
    				var mvalue = mapEle.children[2].children[0].children[0].value;
    				if(mkey != "" && mvalue != ""){ 
    					fieldsMap[mkey]=mvalue;
    			    }	
    			}
        		fieldMappingMap["userMap"]=fieldsMap;
        		await ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "calendlyforzohocrm__fieldMaps","value": fieldMappingMap});
        	}
        	document.getElementById("Error").style.display = "none";	
		}
		async function deleteUserMap(deleteIcon){
			document.getElementById("Error").style.display = "block";
        	document.getElementById("ErrorText").innerText="Saving Configuration ..."
			var deleteElement = deleteIcon.parentElement.parentElement
    		var fieldsMap = fieldMappingMap["userMap"];
    		var key =deleteElement.children[0].value;
    		var value = deleteElement.children[2].children[0].children[0].value;
    		if(key != "" && value != ""){ 
        		delete fieldsMap[key];
        		fieldMappingMap["userMap"]=fieldsMap;
        		await ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "calendlyforzohocrm__fieldMaps","value": fieldMappingMap});
        	}	
			deleteElement.parentElement.removeChild(deleteElement);
			document.getElementById("Error").style.display = "none";
		}
        function getApiMapping(){
        	var apiKeyMap = {};
			if(fieldMappingMap["apiKeyMap"]){
				apiKeyMap = fieldMappingMap["apiKeyMap"];
			}
			var result ='';
			users.forEach(function(user){
				result = result+getUserLayoutField(user,apiKeyMap,"apiKey");
			});	
			return result;
        }
		function getLayoutMapping(){
			var layoutMap = {};
			if(fieldMappingMap["layoutMap"]){
				layoutMap = fieldMappingMap["layoutMap"];
			}
			var result ='';
			users.forEach(function(user){
				result = result+getUserLayoutField(user,layoutMap,"layout");
			});	
			return result;
		}
		function getUserLayoutField(user,layoutMap,type,isAdd){
			var apiname = "calendlyforzohocrm__fieldMaps";
			var layoutId ='';
			if(layoutMap[user.id]){
				layoutId = layoutMap[user.id];
			}
			var fromField='<div class="userDrop"><div class="styled-select slate fieldMap"><select name="field4" disabled><option value="'+user.id+'">'+user.full_name+'</option></select></div></div>';  
			var toFiled ='<input id="'+user.id+'" class="layout" type="text" value="'+layoutId+'" onchange="updateOrgVariables('+"'"+type+"'"+',this)"></input>'; 	
			var arrow =	'<div class="arrowIcon"> <i class="material-icons ">arrow_back</i></div>';
			var fieldMap ='<div  class="fieldMapp">'+fromField+arrow+toFiled+'</div>';
			return fieldMap;
		}
		function getfieldMapping(eventId){
			var fieldsMap = fieldMappingMap[eventId];
			var result="";
			var keysList = Object.keys(fieldsMap);
			if(selectedModule != "Contacts" && selectedModule != "Leads"){
				result=getSinglefieldMap(emailField,"email","",true,eventId);
				keysList.push(emailField);
			}
			else{
				result=getSinglefieldMap("Email","email","",true,eventId);
				keysList.push("Email");
			}
			Object.keys(fieldsMap).forEach(function(key){
				if(!emailField || key != emailField){
					result = result+getSinglefieldMap(key,fieldsMap[key],keysList,false,eventId);
				}
			});
			return result;
		}
		function toggleFieldMap(id){
			$('#'+id+'_name').toggleClass('active');
			$('#'+id).toggle(500);
		}
		function getSinglefieldMap(key, value,Keys,noOption,eventId){
			var fieldsList ="";
			var calendlyFields="";
			var apiname = "calendlyforzohocrm__fieldMaps";
			var localfromFields = fromFields;
			if(eventId && eventsModuleMap[eventId] && moduleFieldsMap[eventsModuleMap[eventId]]){
				localfromFields = moduleFieldsMap[eventsModuleMap[eventId]];
			}
			var custom_input = "";
			if(!noOption){
				
				var selectedOption="";
				commomFields.forEach(function(field){
					if(key == field.api_name){
						selectedOption = '<option value="'+field.api_name+'">'+field.field_label+'</option>';
					}
					else if(Keys.indexOf(field.api_name) == -1){
						fieldsList = fieldsList+'<option value="'+field.api_name+'">'+field.field_label+'</option>';
					}
				});
				
				localfromFields.forEach(function(field){
					if(key == field.api_name){
						selectedOption = '<option value="'+field.api_name+'">'+field.field_label+'</option>';
					}
					else if(Keys.indexOf(field.api_name) == -1){
						fieldsList = fieldsList+'<option value="'+field.api_name+'">'+field.field_label+'</option>';
					}
				});
				eventsFields.forEach(function(field){
					if(key == "events."+field.api_name){
						selectedOption = '<option value="events.'+field.api_name+'">Events - '+field.field_label+'</option>';
					}
					else if(field.custom_field && Keys.indexOf("events."+field.api_name) == -1){
						fieldsList = fieldsList+'<option value="events.'+field.api_name+'">Events - '+field.field_label+'</option>';
					}
				});
				if(!selectedOption){
					if(key.indexOf("events.") == 0 ){
						selectedOption = '<option value="'+key+'">Events - '+key.substring(7)+'</option>';
					}
					else{
						selectedOption =   '<option value="'+key+'">'+key+'</option>';
					}
				}
				fieldsList =selectedOption+""+fieldsList;
				var toFields=["Name","Email","Text reminder number","Location","All Answers"];
				var selectField ="";
				toFields.forEach(function(field){
					if(field.replace(/ /gi, "_").toLowerCase() != value){
						calendlyFields = calendlyFields+'<option value="'+field.replace(/ /gi, "_").toLowerCase()+'">'+field+'</option>';
					}
					else{
						selectField = '<option value="'+field.replace(/ /gi, "_").toLowerCase()+'">'+field+'</option>';
					}
				});
				for(let i=1;i<=10;i++){
					if(value != 'answer_'+i){
						calendlyFields = calendlyFields+'<option value="answer_'+i+'">Answer '+i+'</option>';
					}
					else{
						selectField = '<option value="answer_'+i+'">Answer '+i+'</option>';
					}
				}
				var trackingFields =["utm_campaign","utm_source","utm_medium","utm_content","utm_term","salesforce_uuid"];
				trackingFields.forEach(function(field){
					if(field != value){
						calendlyFields = calendlyFields+'<option value="'+field+'">'+field+'</option>';
					}
					else{
						selectField = '<option value="'+field+'">'+field+'</option>';
					}
				});
				var savebutton = "";
				if(version > 95){
                    savebutton = '<span style="display:none;"><br><i class="material-icons" onclick="deleteFieldMap(this)" style="margin-top:5px;">save</i></span>';
					custom_input = '<input type="text" onchange="saveCustomValue(this)" style="display:none;margin-top:3px;padding:8px;height:30px;width:200px;" maxlength=50>';
					if(!selectField && value){
						savebutton = '<span ><br><i class="material-icons" onclick="deleteFieldMap(this)" style="margin-top:5px;">save</i></span>';
						custom_input = '<input type="text" onchange="saveCustomValue(this)" style="margin-top:3px;padding:8px;height:30px;width:200px;" value="'+value+'" maxlength=50>';
					}
				}	
				calendlyFields = selectField+'<option value="custom_value">Custom value</option>'+calendlyFields;
				var delet =	'<div class="arrowIcon" style="float:right;cursor:pointer;"> <i class="material-icons" onclick="deleteFieldMap(this)">delete</i>'+savebutton+'</div>';
			}
			else{
				if(selectedModule != "Contacts" && selectedModule != "Leads"){
					localfromFields.forEach(function(field){
						if(key == field.api_name){
							fieldsList = '<option value="'+field.api_name+'">'+field.field_label+'</option>';
						}
					});
			    }
			    else{
			    	fieldsList='<option value="Email">Email</option>';
			    }		
				calendlyFields = calendlyFields+'<option value="email">Email</option>';
				var delet =	'<div class="arrowIcon" style="float:right;cursor:pointer;"> <i class="material-icons" onclick="addFieldMap(this)">add</i> </div>';
			}	
			var fromField='<div class="fromField"><div class="styled-select slate fieldMap"><select name="field4" onchange="updateOrgVariables('+"'"+apiname+"'"+',this)">'+fieldsList+'</select></div></div>';  
			var toFiled ='<div class="toField"><div class="styled-select slate fieldMap"><select name="field4" onchange="updateOrgVariables('+"'"+apiname+"'"+',this)">'+calendlyFields+'</select></div>'+custom_input+'</div>'; 	
			var arrow =	'<div class="arrowIcon"> <i class="material-icons ">arrow_back</i></div>';
			var fieldMap ='<div  class="fieldMapp">'+fromField+arrow+delet+toFiled+'</div>';
			return fieldMap;
		}
		function addFieldMap(addElement){
			var apiname = "calendlyforzohocrm__fieldMaps";
			var eventId = addElement.parentNode.parentNode.parentNode.id;
			var localfromFields = fromFields;
			if(eventId && eventsModuleMap[eventId] && moduleFieldsMap[eventsModuleMap[eventId]]){
				localfromFields = moduleFieldsMap[eventsModuleMap[eventId]];
			}
			var fieldsList ="";
			fieldsList = fieldsList+'<option value="Select">Select</option>';
			var keysList = Object.keys(fieldMappingMap[eventId]);
			if(selectedModule != "Contacts" && selectedModule != "Leads"){
				keysList.push(emailField);
			}
			else{
				keysList.push("Email");
			}
			commomFields.forEach(function(field){
				if(keysList.indexOf(field.api_name) == -1){
					fieldsList = fieldsList+'<option value="'+field.api_name+'">'+field.field_label+'</option>';
				}
			});
			localfromFields.forEach(function(field){
				if(keysList.indexOf(field.api_name) == -1){
					fieldsList = fieldsList+'<option value="'+field.api_name+'">'+field.field_label+'</option>';
				}
			});
			eventsFields.forEach(function(field){
				if(field.custom_field && Object.keys(fieldMappingMap[eventId]).indexOf("events."+field.api_name) == -1){
					fieldsList = fieldsList+'<option value="events.'+field.api_name+'">Events - '+field.field_label+'</option>';
				}
			});
			calendlyFields='<option value="Select">Select</option>';
			var toFields=["Name","Email","Text reminder number","Location","All Answers"];
			var custom_input = '';
			if(version > 95){
				toFields = ["Custom value"].concat(toFields);
				custom_input = '<input type="text" onchange="saveCustomValue(this)" style="display:none;margin-top:3px;padding:2px;height:30px;width:200px;" maxlength=50>';
			}
			toFields.forEach(function(field){
				calendlyFields = calendlyFields+'<option value="'+field.replace(/ /gi, "_").toLowerCase()+'">'+field+'</option>';
			});
			for(let i=1;i<=10;i++){
				calendlyFields = calendlyFields+'<option value="answer_'+i+'">Answer '+i+'</option>';
			}
			var trackingFields =["utm_campaign","utm_source","utm_medium","utm_content","utm_term","salesforce_uuid"];
			trackingFields.forEach(function(field){
				calendlyFields = calendlyFields+'<option value="'+field+'">'+field+'</option>';
			});
			var value = "";
			var fromField='<div class="fromField"><div class="styled-select slate fieldMap"><select  name="field4" onchange="updateOrgVariables('+"'"+apiname+"'"+',this)">'+fieldsList+'</select></div></div>';  
			var toFiled ='<div class="toField"><div class="styled-select slate fieldMap"><select name="field4" onchange="updateOrgVariables('+"'"+apiname+"'"+',this)">'+calendlyFields+'</select></div>'+custom_input+'</div>'; 	
			var arrow =	'<div class="arrowIcon"> <i class="material-icons ">arrow_back</i></div>';
			var delet =	'<div class="arrowIcon" style="float:right;cursor:pointer;"> <i class="material-icons" onclick="deleteFieldMap(this)">delete</i><span style="display:none;"><br><i class="material-icons" onclick="deleteFieldMap(this)" style="margin-top:5px;">save</i></span> </div>';
			var fieldMap ='<div id="'+value+'" class="fieldMapp">'+fromField+arrow+delet+toFiled+'</div>';
			$('#'+eventId).append(fieldMap);
		}
		function deleteFieldMap(deleteIcon){
			var deleteElement = deleteIcon.parentElement.parentElement
    		var eventId = deleteElement.parentElement.id;
    		var fieldsMap = fieldMappingMap[eventId];
    		var key =deleteElement.children[0].children[0].children[0].value;
    		var value = deleteElement.children[3].children[0].children[0].value;
    		if(key != "Select" && value != "Select"){ 
        		delete fieldsMap[key];
        		fieldMappingMap[eventId]=fieldsMap;
        		ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "calendlyforzohocrm__fieldMaps","value": fieldMappingMap});
        	}	
			deleteElement.parentElement.removeChild(deleteElement);
		}
		function cloneEditor(i,email,editor){
			var element = document.getElementById("emailEditor"+editor);
			var deleteButton = document.createElement("i");
			deleteButton.classList.add("edit");
			deleteButton.classList.add("material-icons");
			deleteButton.classList.add("notranslate");
			deleteButton.id = "deleteIcon"+editor+"_"+i;
			deleteButton.innerText = "delete";
			deleteButton.addEventListener("click", function(){ deleteEditor(this); });
			var clone = element.cloneNode(true);
			clone.id =element.id+"_"+i;
			clone.children[1].id = element.children[1].id+"_"+i;
			clone.children[3].id =element.children[3].id+"_"+i;
			clone.children[4].children[0].id = element.children[4].children[0].id+"_"+i;
			clone.children[4].children[0].children[0].id=element.children[4].children[0].children[0].id+"_"+i;
			clone.children[4].children[1].id=element.children[4].children[1].id+"_"+i;
			clone.children[4].children[1].children[0].id=element.children[4].children[1].children[0].id+"_"+i;
			clone.children[4].children[2].id=element.children[4].children[2].id+"_"+i;
			clone.children[4].children[3].children[0].id=element.children[4].children[3].children[0].id+"_"+i;
			clone.children[4].children[3].children[1].id=element.children[4].children[3].children[1].id+"_"+i;
			clone.children[4].children[3].children[2].id=element.children[4].children[3].children[2].id+"_"+i;
			var addButton = clone.children[4].children[3].children[1];
			addButton.parentElement.removeChild(addButton);
			clone.children[4].children[3].insertBefore(deleteButton, clone.children[4].children[3].children[1]); 
			if(email){
				clone.children[1].innerHTML = email.subject;
				clone.children[4].children[2].innerHTML=email.emailContent;
			}
			var parent = document.getElementById("emailEditors"+editor);
			parent.appendChild(clone);
		}
		
		function googleTranslateElementInit() {
		  new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
		}
        function addListItem(parent,text,className,value){
        	
			// var node = document.createElement("LI");
			// node.classList.add(className);
			if(className == "dropdown-item"){
				// var buttonNode = document.createElement("BUTTON");
				// buttonNode.classList.add(className);
				// var textnode = document.createTextNode(text);
				// buttonNode.appendChild(textnode);
				// buttonNode.addEventListener("click", insert);
				// if(value){
				// 	var x = document.createElement("INPUT");
				// 	x.setAttribute("type", "hidden");
				// 	x.setAttribute("value",value);
	   //      		buttonNode.appendChild(x);
	   //      	}
	   //      	node.appendChild(buttonNode);	
				var linode = '<li class="'+className+'"><button class="'+className+'" onclick="insert(this)">'+text+'<input type="hidden" value="'+value+'"></button></li>';
			}
			else{
				// var textnode = document.createTextNode(text);
				// node.appendChild(textnode);
				var linode = '<li class="'+className+'">'+text+'</li>';
			}
			// document.getElementById("dropdown-menu-email"+parent).appendChild(node);
			 $('#dropdown-menu-email'+parent).append(linode);

        }
        async function getOrganisationEvents(){
        	var resp = await makeCalendlyCall({"url":"event_types?count=100&organization="+calendlyDetails.current_organization},"GET");
        	if(resp.response){
				resp = JSON.parse(resp.response);
			}
        	if(resp && resp.collection){
        		return resp.collection;
        	}
        	return [];
        }
        async function getUserEvents(){
        	var resp = await makeCalendlyCall({"url":"event_types?count=100&user="+calendlyDetails.uri},"GET");
        	if(resp.response){
				resp = JSON.parse(resp.response);
			}
        	if(resp && resp.collection){
        		return resp.collection;
        	}
        	return [];
        }
        async function checkCalendlyApiKey(){
        	document.getElementById("Error").style.display = "block";
        	document.getElementById("ErrorText").innerText="Checking Calendly connection ..."
        	await getCalendlyDetails();
        	if(!calendlyDetails || !calendlyDetails.current_organization){
        		return;
        	}
        	return getWebhooks().then(async function(webhooks){
            	document.getElementById("Error").style.display = "none";
            	var isWebhookFound = false;
                if(webhooks && webhooks.collection && webhooks.collection.length){
                	for(let i=0;i<webhooks.collection.length;i++){
                		if(webhooks.collection[i].callback_url == crm_url && webhooks.collection[i].state == "active"){
                			if(isWebhookFound == true){
                				await _deleteWebhook(webhooks.collection[i].uri.split("/")[4]);
                			}
                			isWebhookFound  = true;
                		}
                		else if(webhooks.collection[i].callback_url == crm_url && webhooks.collection[i].state == "disabled"){
                			await _deleteWebhook(webhooks.collection[i].uri.split("/")[4]);
                		}
                	}
                	if(isWebhookFound){
                		if(apiKey){
                			deleteOldWebhooks();
                		}
               //  		document.getElementById("apikey").disabled = true;
            			// document.getElementById("remove").style.display="inline-block";
	            		// document.getElementById("save").style.display="none";
	            		// document.getElementById("error").innerHTML = "";
            			return true;
                	}
                }
                await addWebhook();
                return true;
            });	
        }
        var calendlyDetails = {};
        async function getCalendlyDetails(){
        	try{
        		var resp = await makeCalendlyCall({"url":"users/me"},"GET");
        	}
        	catch(e){
        		console.log(e);
        		if(e && e.code == "403" && e.message == "Authorization Exception"){
	        		var configureLink = serviceOrigin+"/crm/settings/extensions/all/calendlyforzohocrm?tab=webInteg&subTab=marketPlace&nameSpace=calendlyforzohocrm&portalName=ulgebra";
        			if(version > 86){
	        			document.getElementById("ErrorText").innerHTML=`Please Authorize Calendly in <a style="color:#09e8f7;" target='_blank' href="${configureLink}">Extension configure page</a>`;
	        			try{
        					var resp2 = await ZOHO.CRM.CONNECTOR.invokeAPI("calendlyforzohocrm.calendly.calendlyhandler",{"url":"ulgebralogs?action=checkCalendlyConnector","body":{"url":"https://api.calendly.com/users/me","method":"GET","email":orgDetails.org[0].primary_email}});
        					console.log(resp2);
        				}
        				catch(e){
        					console.log(e);
        				}
        			}
        			else{
        				document.getElementById("ErrorText").innerHTML=`Please update the Extension to Latest Version <a style="color:#09e8f7;" target='_blank' href="${configureLink}">here</a>`;
        			}
        		}
        		calendlyDetails = null;
        		return null;
        	}
        	if(resp.response){
				resp = JSON.parse(resp.response);
			}
        	if(resp && resp.resource){
        		calendlyDetails = resp.resource;
        		return;
        	}
        }
        async function addWebhook(){
        	if(!calendlyDetails || !calendlyDetails.current_organization){
        		return false;
        	}
        	var resp = await ZOHO.CRM.FUNCTIONS.execute("calendlyforzohocrm__addeventandcontactfromcalendly", {"action":"addWebhook","organization":calendlyDetails.current_organization,"sandbox":sandbox});
        	resp = resp.details.output;
            resp = JSON.parse(resp);
            if(resp.status && resp.status != 200 && resp.status != 201){
            	document.getElementById("Error").style.display = "block";
				document.getElementById("ErrorText").innerText = resp.message;
        		return false;
        	}
            if(resp && resp.id || resp.type == "conflict_error"){
            	window.location.reload();
	        }		
            console.log(resp); 
            return true;
        }
        async function getWebhooks(){
			var resp = await makeCalendlyCall({"url":"webhook_subscriptions?scope=organization&organization="+calendlyDetails.current_organization},"GET");
			if(resp.response){
				resp = JSON.parse(resp.response);
			}
			console.log(resp);
			return resp;
		}

		async function deleteWebhook(){
			document.getElementById("Error").style.display = "block";
        	document.getElementById("ErrorText").innerText="Deleting Webhooks ..."
			// var apiKey = document.getElementById("apikey").value;
			// var apiRequest =   {
   //              url: "https://calendly.com/api/v1/hooks",
   //              headers: {
   //              "X-TOKEN": apiKey,
   //              }
   //          }
   //          var webhooks = await ZOHO.CRM.HTTP.get(apiRequest);
            var webhooks = await getWebhooks();
            for(let i=0;i<webhooks.collection.length;i++){
        		if(webhooks.collection[i].callback_url == crm_url && webhooks.collection[i].state == "active"){
        			await _deleteWebhook(webhooks.collection[i].uri.split("/")[4]);
        		}
        	}
    		window.location.reload();
        	return true;
		}
		async function _deleteWebhook(id){
			// var apiRequest =   {
   //              url: "https://calendly.com/api/v1/hooks/"+id,
   //              headers: {
   //              "X-TOKEN": apiKey,
   //              }
   //          }
   //          await ZOHO.CRM.HTTP.delete(apiRequest);
   			var resp = await makeCalendlyCall({"url":"webhook_subscriptions/"+id},"DELETE");
			console.log(resp);
			return resp;
            return true;
		}

		async function deleteOldWebhooks(){
			document.getElementById("Error").style.display = "block";
        	document.getElementById("ErrorText").innerText="Deleting Old Webhooks ..."
			var apiRequest =   {
                url: "https://calendly.com/api/v1/hooks",
                headers: {
                "X-TOKEN": apiKey,
                }
            }
            var webhooks = await ZOHO.CRM.HTTP.get(apiRequest);
            webhooks = JSON.parse(webhooks); 
            for(let i=0;i<webhooks.data.length;i++){
        		if(webhooks.data[i].attributes.url == crm_url && webhooks.data[i].attributes.state == "active"){
        			await _deleteOldWebhook(webhooks.data[i].id);
        		}
        	}
            await ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname":"calendlyforzohocrm__Api_Key","value":"null"});
        	document.getElementById("Error").style.display = "none";
        	document.getElementById("ErrorText").innerText="";
        	return true;
		}
		async function _deleteOldWebhook(id){
			var apiRequest =   {
                url: "https://calendly.com/api/v1/hooks/"+id,
                headers: {
                "X-TOKEN": apiKey,
                }
            }
            await ZOHO.CRM.HTTP.delete(apiRequest);
            return true;
		}
        async function saveCustomValue(element) {
        	document.getElementById("Error").style.display = "block";
        	document.getElementById("ErrorText").innerText="Saving Configuration ..."
			console.log(element.value);
			var filedMapElement = element.parentElement.parentElement;
    		var eventId = filedMapElement.parentElement.id;
    		var fieldsMap = fieldMappingMap[eventId];
    		var key =filedMapElement.children[0].children[0].children[0].value;
    		var value = filedMapElement.children[3].children[0].children[0].value;
    		var input = filedMapElement.children[3].children[1];
    		if(value == "custom_value"){
				value = input.value;
    		}
    		else{
    			input.style.display = "none";
    		}
    		if(key != "Select" && value != "Select"){ 
        		fieldsMap[key]=value;
        		fieldMappingMap[eventId]=fieldsMap;
        		await ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "calendlyforzohocrm__fieldMaps","value": fieldMappingMap});
        	}	
        	document.getElementById("Error").style.display = "none";
		}
        async function updateOrgVariables(apiname,value){
        	document.getElementById("Error").style.display = "block";
        	document.getElementById("ErrorText").innerText="Saving Configuration ..."
        	if(apiname == "layout"){
        		var layoutMap = {};
				if(fieldMappingMap["layoutMap"]){
					layoutMap = fieldMappingMap["layoutMap"];
				}
        		layoutMap[value.id]=value.value;
        		fieldMappingMap["layoutMap"] = layoutMap;
        		await ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "calendlyforzohocrm__fieldMaps","value": fieldMappingMap});
         	    document.getElementById("Error").style.display = "none";
         	}
        	else if(apiname == "apiKey"){
        		var layoutMap = {};
				if(fieldMappingMap["apiKeyMap"]){
					layoutMap = fieldMappingMap["apiKeyMap"];
				}
        		layoutMap[value.id]=value.value;
        		fieldMappingMap["apiKeyMap"] = layoutMap;
        		await ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "calendlyforzohocrm__fieldMaps","value": fieldMappingMap});
        		document.getElementById("Error").style.display = "none";
        		window.location.reload();
        	}
        	else if(apiname == "calendlyforzohocrm__Api_Key"){
        		checkCalendlyApiKey().then(async function(resp){
        			if(resp){
        				await ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": apiname,"value": value});
        			}
        			document.getElementById("Error").style.display = "none";
        		});
        	}
        	else if(apiname == "calendlyforzohocrm__fieldMaps"){
        		var filedMapElement = value.parentElement.parentElement.parentElement;
        		var eventId = filedMapElement.parentElement.id;
        		var fieldsMap = fieldMappingMap[eventId];
        		var key =filedMapElement.children[0].children[0].children[0].value;
        		var value = filedMapElement.children[3].children[0].children[0].value;
        		var input = filedMapElement.children[3].children[1];
        		if(value == "custom_value"){
        			if(input.style.display != "block"){
        				input.style.display = "block";
        				filedMapElement.children[2].children[1].style.display='inline';
        				if(input.value){
        					value = input.value;
        				}
        				else{
	        				document.getElementById("Error").style.display = "none";
	        				return;
	        			}	
        			}
 					value = input.value;
        		}
        		else{
        			filedMapElement.children[2].children[1].style.display='none';
        			input.style.display = "none";
        		}
        		if(key != "Select" && value != "Select"){ 
	        		fieldsMap[key]=value;
	        		fieldMappingMap[eventId]=fieldsMap;
	        		await ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": apiname,"value": fieldMappingMap});
	        	}	
	        	document.getElementById("Error").style.display = "none";
        	}
        	else{
        		if(apiname == "calendlyforzohocrm__Module"){
        			var eventFieldMap ="";
        			selectedModule = value;
        			fromFields = moduleFieldsMap[selectedModule];
    	// 			if(value == "Contacts"){
					// 	fromFields = contactsFields
					// }
					// else if(value == "Leads"){
					// 	fromFields = leadsFields;
					// }
					// else if(value && customModuleFields){
					// 	fromFields = customModuleFields
					// }
					var eventIdsList=[];
					eventTypes.forEach(function(event){
						let eventId = event.uri.split("/")[4];
						if(eventIdsList.indexOf(eventId) == -1){
							eventIdsList.push(eventId);
							fieldMappingMap[eventId]={"Event Description":"all_answers"};
							let moduleSelectbox="";
							if(version > 94){
								moduleSelectbox=`<select id="${eventId}_module" onchange="selectModuleForEvents('${eventId}')"><option value="Contacts">Contacts</option><option value="Leads">Leads</option></select>`;
							}
							eventFieldMap = eventFieldMap + '<div class="eventFieldMap"><div class="eventname collapsible" id="'+eventId+'_name" onclick="toggleFieldMap('+"'"+eventId+"'"+')" style="display:flex;flex-direction:row;"><span style="flex-grow:10;" >'+event.name+' ('+event.scheduling_url.split("/")[3]+') Fields Mapping</span> <span style="flex-grow:1;align-slef:flex-end;"><label onclick="stopClick()" class="switch2"><input id="'+eventId+'_enable" type="checkbox" onclick="enableDisableEvent('+"'"+eventId+"'"+')"><span class="slider2 round"></span></label></span></div><div class="fieldMapperContainer" id="'+eventId+'">'+moduleSelectbox+getfieldMapping(eventId)+'</div></div>';
					 	}
					});
					$('#fieldsMap').html(eventFieldMap); 
					eventTypes.forEach(function(event){
						let eventId = event.uri.split("/")[4];
						if(version > 94){
							document.getElementById(eventId+"_module").value = eventsModuleMap[eventId]?eventsModuleMap[eventId]:selectedModule;
						}
						document.getElementById(eventId+"_enable").checked = (blockedEvents.indexOf(eventId) == -1)? true  : false;
						document.getElementById(eventId+"_name").style.backgroundColor= (blockedEvents.indexOf(eventId) == -1)? "grey"  : "#7777774d";
					});
					eventIdsList=[];
					eventTypes.forEach(function(event){
						let eventId = event.uri.split("/")[4];
						if(eventIdsList.indexOf(eventId) == -1 && blockedEvents.indexOf(eventId) == -1){
							eventIdsList.push(eventId);
							toggleFieldMap(eventId);
					    }		
					});
        			await ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": "calendlyforzohocrm__fieldMaps","value": fieldMappingMap});
        		}
        		await ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": apiname,"value": value});
        		document.getElementById("Error").style.display = "none";
        		
        		if(apiname == "calendlyforzohocrm__Activity"){
        			if(value == "Tasks"){
        				document.getElementById("note").innerHTML = "Appointment Start date will be mapped as a Due Date for the Task record in zoho CRM";
        			}
        			else{
        				document.getElementById("note").innerHTML = "";
        			}
        		}	
        	}
        }
		function styling(tag)
		{
			document.execCommand(tag);
		}
		function link(){
			$("#linkForm"+currentEditor).slideToggle("slow");
		}
		function image(){
			$("#imageForm"+currentEditor).slideToggle("slow");
		}
		function addLink(){
			var href = document.getElementById("linkUrl"+currentEditor).value;
		    if (range) {
				if(range.startOffset == range.endOffset){
					if(range.commonAncestorContainer.parentNode.href){
						range.commonAncestorContainer.parentNode.href=href;
					}
					else{
						var span = document.createElement('a');
						span.setAttribute('href',href);
						span.innerText = href;
						range.insertNode(span);
			        	range.setStartAfter(span);
			        }	
				}
				else{
					var data = range.commonAncestorContainer.data;
					var start = range.startOffset;
					var end = range.endOffset;
					range.commonAncestorContainer.data="";
					var span = document.createElement('span');
					span.appendChild( document.createTextNode(data.substring(0,start)) );
					var atag = document.createElement('a');
					atag.setAttribute('href',href);
					atag.innerText = data.substring(start,end);
					span.appendChild(atag);
					span.appendChild( document.createTextNode(data.substring(end)) );
					range.insertNode(span);
		        	range.setStartAfter(span);
				}
		        range.collapse(true);
		    }
			$("#linkForm"+currentEditor).slideToggle("slow");
		}
		function addImage(){
			var href = document.getElementById("imageUrl"+currentEditor).value;
			var span = document.createElement('img');
			span.setAttribute('src',href);
			span.innerText = href;
			range.insertNode(span);
        	range.setStartAfter(span);
			$("#imageForm"+currentEditor).slideToggle("slow");
		}
		function openlink(){
			sel = window.getSelection();
		    if (sel && sel.rangeCount) {
		        range = sel.getRangeAt(0);
		      }  
			if(range && range.commonAncestorContainer.wholeText){
				if(range.commonAncestorContainer.parentNode.href){
					document.getElementById("linkUrl"+currentEditor).value = range.commonAncestorContainer.parentNode.href;
					$("#linkForm"+currentEditor).slideToggle("slow");
				}
			}	
		}
		function insert(bookingLink){
    		// var bookingLink = this;
			var range;
			if (sel && sel.rangeCount) {
		        range = sel.getRangeAt(0);
		        range.collapse(true);
	    		if(bookingLink.children[0].value == "variable"){
	    		    var span = document.createElement("span");
	    		    span.appendChild( document.createTextNode('%'+bookingLink.innerText+'%') );
	        		range.insertNode(span);
	    		}
	    		else{
	    			if(currentEditor.indexOf("WhatsApp") != -1){
	    				var span = document.createElement("span");
		    		    span.appendChild( document.createTextNode(bookingLink.children[0].value));
		        		range.insertNode(span);
	    			}
	    			else{
	    				var span = document.createElement('a');
						span.setAttribute('href',bookingLink.children[0].value);
						span.innerText = bookingLink.innerText;
						range.insertNode(span);
	    			}	
	    		}
	    		range.setStartAfter(span);
		        range.collapse(true);
		        sel.removeAllRanges();
		        sel.addRange(range);
		    }    
		}
		function showEditor(editor){
			currentEditor = editor.id.substring(8);
			emailContentText = document.getElementById("emailContent"+currentEditor).innerHTML;
			var coll = document.getElementsByClassName("edit")
			for(var i=0, len=coll.length; i<len; i++)
		    {
		        coll[i].style["display"] = "none";
		    }
			// document.getElementById("editIcon"+editor).style.display= "none";
			document.getElementById("editorbuttons"+currentEditor).style.display= "block";
			document.getElementById("editButtons"+currentEditor).style.display= "block";
			document.getElementById("emailContent"+currentEditor).contentEditable = "true";
			if(currentEditor.indexOf("Email") != -1){
				document.getElementById("subject"+currentEditor).contentEditable = "true";
				subject = document.getElementById("subject"+currentEditor).innerHTML;
			}	
			$('#emailContent'+currentEditor).focus();
		}
		function addEditor(editor){
			// var emailContent = document.getElementById("emailContent"+editor).innerHTML;
			// var subject = document.getElementById("subject"+editor).innerHTML;
			// var element = document.getElementById("emailEditor"+editor.id.substring(7));
			// var email ={"emailContent":emailContent,"subject":subject}
			if(editor.id.substring(7).indexOf("Contacts") != -1){
				var emailId = parseInt(emailContentForContacts[emailContentForContacts.length-1].emailId)+1;
				var customVariable ="calendlyforzohocrm__email_content_for_contacts";
			}
			else{
				var emailId = parseInt(emailContentForLeads[emailContentForLeads.length-1].emailId)+1;
				var customVariable ="calendlyforzohocrm__email_content_for_leads";
			}
			cloneEditor(emailId,"",editor.id.substring(7));
			currentEditor =editor.id.substring(7)+"_"+emailId;
			saveEmailContent(customVariable);
		}
		function saveEmailContent(customVariable){
			emailContentText =JSON.parse(JSON.stringify(document.getElementById("emailContent"+currentEditor).innerHTML).replace(/\\n/gi, "<br>"));
			var emailContent = emailContentText;
			if(customVariable.indexOf("email") != -1){
				subject = document.getElementById("subject"+currentEditor).innerHTML;
				var emailId = (currentEditor.indexOf("_") == -1)?0:currentEditor.substring(currentEditor.indexOf("_")+1);
				emailContent = {"subject":subject,"emailContent":emailContentText,"emailId":emailId};
				if(currentEditor.indexOf("Contacts") != -1){
					for(var i=0; i<emailContentForContacts.length;i++){
						if(emailContentForContacts[i].emailId == emailId){
							emailContentForContacts[i]=emailContent;
							break;
						}
					}
					if(i == emailContentForContacts.length){
						emailContentForContacts.push(emailContent);
					}
					updateOrgVariables(customVariable,emailContentForContacts);
				}
				else{
					for(var i=0; i<emailContentForLeads.length;i++){
						if(emailContentForLeads[i].emailId == emailId){
							emailContentForLeads[i]=emailContent;
							break;
						}
					}
					if(i == emailContentForLeads.length){
						emailContentForLeads.push(emailContent);
					}
					updateOrgVariables(customVariable,emailContentForLeads);
				}	
			}
			else{
				updateOrgVariables(customVariable,emailContent);
			}
			cancelEditor(currentEditor);
		}
		function deleteEditor(editor){
			var emailId = editor.id.substring(editor.id.indexOf("_")+1);
			if(editor.id.indexOf("Contacts") != -1){
				for(var i=0; i<emailContentForContacts.length;i++){
					if(emailContentForContacts[i].emailId == emailId){
						emailContentForContacts.splice(i,1);
						break;
					}
				}
				updateOrgVariables("calendlyforzohocrm__email_content_for_contacts",emailContentForContacts);
			}
			else{
				for(var i=0; i<emailContentForLeads.length;i++){
					if(emailContentForLeads[i].emailId == emailId){
						emailContentForLeads.splice(i,1);
						break;
					}
				}
				updateOrgVariables("calendlyforzohocrm__email_content_for_leads",emailContentForLeads);
			}	
			var editorElement = document.getElementById("emailEditor"+editor.id.substring(10));
			editorElement.parentElement.removeChild(editorElement);
		}
		function cancelEditor(editor){
			// currentEditor = editor.parentElement.id.substring(13);
			document.getElementById("emailContent"+currentEditor).innerHTML = emailContentText;
			var coll = document.getElementsByClassName("edit")
			for(var i=0, len=coll.length; i<len; i++)
		    {
		        coll[i].style["display"] = "block";
		    }
			// document.getElementById("editIcon"+editor).style.display= "block";
			document.getElementById("editorbuttons"+currentEditor).style.display= "none";
			document.getElementById("editButtons"+currentEditor).style.display= "none";
			document.getElementById("emailContent"+currentEditor).contentEditable = "false";
			$("#linkForm"+currentEditor).hide("slow");
			$("#imageForm"+currentEditor).hide("slow");
			if(currentEditor.indexOf("Email") != -1){
				document.getElementById("subject"+currentEditor).contentEditable = "false";
				document.getElementById("subject"+currentEditor).innerHTML = subject;
			}
			currentEditor ="";	
		}
