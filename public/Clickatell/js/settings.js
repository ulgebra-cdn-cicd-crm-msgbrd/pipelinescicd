// var emailContentText = "Hi ,\n\nHope you're well. I'm writing to know how our business can help you.\n\nPlease choose a event to schedule an appointment:\n\n${eventsUrls} See you soon!\n\n %ownerName% \n\n %companyName%";
		var emailContentForContacts = [];
		var emailContentForLeads = [];
		var emailContentText = "";
		var customVariable = "";
		var startIndex =0;
		var endIndex = 0;
		var currentEditor="";
		var subject="";
		var name="";
		var sel;
		var range;
		var smsTemplates = [];
		var templateSettings;
		var phoneFieldsSetting;
        document.addEventListener("DOMContentLoaded", function(event) {
        	ZOHO.embeddedApp.init().then(function(){
        		getAPIURL();
        		ZOHO.CRM.UI.Resize({height:"1000",width:"1000"}).then(function(data){
					console.log(data);
				});
        		ZOHO.CRM.API.getOrgVariable("clickatellsmscrm__clickatellApiKey").then(function(apiKeyData){
        			
	        		if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content && apiKeyData.Success.Content != "0"){
	        			document.getElementById("apikey").value=apiKeyData.Success.Content;
	        		}	
	        	});	
	        	ZOHO.CRM.API.getOrgVariable("clickatellsmscrm__Clickatell_Phone_Number").then(function(apiKeyData){
        			
	        		if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content && apiKeyData.Success.Content != "0"){
	        			document.getElementById("clickatellPhone").value=apiKeyData.Success.Content;
	        		}	
	        	});	
	        	var oldmodules =["Leads","Contacts","Deals","Accounts"];
	        	var DealsPhoneList ='';
	        	var modules=[];
	        	ZOHO.CRM.META.getModules().then(function(userModules){
	        		var fetchList =[];
	        		userModules.modules.forEach(function(module){
	        			if(oldmodules.indexOf(module.api_name)!= -1 && module.api_supported && module.visible){
	        				fetchList.push(ZOHO.CRM.META.getFields({"Entity":module.api_name}));
	        				modules.push(module.api_name);
	        			}
	        		});
		        	Promise.all(fetchList).then(function(data){ 
						for(let i=0;i<data.length;i++){
							var phoneList='';
							if(modules[i] == "Contacts" || modules[i] == "Accounts"){
								data[i].fields.forEach(function(field){
									if(field.data_type == "phone"){
										DealsPhoneList =DealsPhoneList+ `<li class="templateItem" onclick="savePhoneFields('Deals','${modules[i]}.${field.api_name}')">${modules[i]} ${field.field_label}</li>`;
									}	
								});	
							}
							data[i].fields.forEach(function(field){
								if(field.data_type == "phone"){
									phoneList =phoneList+ `<li class="templateItem" onclick="savePhoneFields('${modules[i]}','${field.api_name}')">${field.field_label}</li>`;
								}	
							});	
							if($(`#${modules[i]}PhoneList`)){
								$(`#${modules[i]}PhoneList`).append(phoneList);
							}
						}
						$(`#DealsPhoneList`).append(DealsPhoneList);
						ZOHO.CRM.API.getOrgVariable("clickatellsmscrm__phonefields").then(function(phoneFieldsData){
							if(phoneFieldsData && phoneFieldsData.Success && phoneFieldsData.Success.Content && phoneFieldsData.Success.Content != "0"){
		        				phoneFieldsSetting =JSON.parse(phoneFieldsData.Success.Content);
		        				Object.keys(phoneFieldsSetting).forEach(function(key){
		        					document.getElementById("selected"+key+"Phone").innerText = phoneFieldsSetting[key].replace(/\./g," ").replace(/\_/g," ");;
		        				});
		        			}
						});
					});
				}); 	
	        	ZOHO.CRM.API.getOrgVariable("clickatellsmscrm__EventReminderSettings").then(function(salesSignalData){
	        		document.getElementById("loader").style.display= "none";
        			document.getElementById("contentDiv").style.display= "block";
	        		if(salesSignalData && salesSignalData.Success && salesSignalData.Success.Content && salesSignalData.Success.Content != "0"){
	        			templateSettings =JSON.parse(salesSignalData.Success.Content);
	        			document.getElementById("salesSignal").checked = (templateSettings.enable == true);
	        		}	
		        	ZOHO.CRM.API.searchRecord({Entity:"clickatellsmscrm__SMS_Templates",Type:"criteria",Query:"(clickatellsmscrm__Module_Name:equals:Events)"})
					.then(function(data){
						smsTemplates = data.data;
						if(data.data){
							var templateList ="";
							for(let i=0;i <data.data.length;i++){ 
								if(templateSettings.templateId == data.data[i].id){
									document.getElementById("selectedTemplate").innerText = data.data[i].Name;
									document.getElementById("tooltiptext").innerText = data.data[i].Name;
								}
								templateList =templateList+ '<li class="templateItem" id="'+data.data[i].id+'" onclick="saveTemplateVariable(this)"></li>';
								// templateList =templateList+ '<option  value="'+data.data[i].id+'"">'+data.data[i].Name+'</option>';
							}
							$('#templateList').append(templateList);
							if(templateList == ""){
								$('#templateList').append('<li style="text-align:center;">No Templates</li>');
							}
							else{
								for(let i=0;i <data.data.length;i++){ 
									document.getElementById(data.data[i].id).innerText = data.data[i].Name;
								}
							}	
						}
						else{
							$('#templateList').append('<li style="text-align:center;">No Templates</li>');
						}
					});	
	        	});	
	        });    	
        });
        function getAPIURL(){
   			var func_name = "clickatellsmscrm__bulksms";
    		var req_data ={
				"arguments": JSON.stringify({
				"action": "getApiURL",	
				})
			};
			ZOHO.CRM.FUNCTIONS.execute(func_name, req_data)
			.then(function(data){
				document.getElementById("webHookurl").innerText = data.details.output;
				if(document.getElementById("incomingurl")){
					document.getElementById("incomingurl").innerText = data.details.output+"&incoming=true";
				}
				if(document.getElementById("statusurl")){
					document.getElementById("statusurl").innerText = data.details.output+"&statuscall=true";
				}
			});
        }
        function savePhoneFields(model,phoneField){
        	phoneFieldsSetting[model]=phoneField;
        	document.getElementById("selected"+model+"Phone").innerText = phoneField.replace(/\./g," ").replace(/\_/g," ");;;
        	updateOrgVariables("clickatellsmscrm__phonefields",phoneFieldsSetting);
        }
        function saveTemplateVariable(editor){
        	for(var i=0; i<smsTemplates.length;i++){
				if(smsTemplates[i].id == editor.id){
					templateId = smsTemplates[i].id;
					document.getElementById("selectedTemplate").innerText = smsTemplates[i].Name;
					document.getElementById("tooltiptext").innerText = smsTemplates[i].Name;
					updateOrgVariables("clickatellsmscrm__EventReminderSettings",editor.id,"templateId");
					break;
				}
			}
        }
        function updateOrgVariables(apiname,value,key){
    		if(apiname == "clickatellsmscrm__clickatellApiKey"){
    			value = document.getElementById("apikey").value;
    		}
    		else if(apiname == "clickatellsmscrm__Clickatell_Phone_Number"){
    			value = document.getElementById("clickatellPhone").value;
    		}
    		else if(apiname == "clickatellsmscrm__EventReminderSettings"){
				templateSettings[key]=value;
				value =templateSettings;
    		}
			document.getElementById("ErrorText").innerText = "Saving...";
			document.getElementById("Error").style.display= "block";
    		ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": apiname,"value": value}).then(function(res){
    			document.getElementById("ErrorText").innerText = "Saved";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 500);
    		});
        }
