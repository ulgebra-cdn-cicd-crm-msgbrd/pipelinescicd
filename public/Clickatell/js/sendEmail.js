		var emailContent = [];
		var emailContentText = "";
		var startIndex =0;
		var endIndex = 0;
		var currentEditor="";
		var subject="";
		var sel;
		var range;
		var calendarsMap;
		var recordIds;
		var recordModule;
		var ButtonPosition;
		var smsTemplates;
		var templateId;
		var scheduledTime;
		var func_name;
		var countryCode;
		var moduleFields;
		var userFields;
		var historyFields;
		var apiKey;
		var authId;
		var src;
		var currentRecords =[];
		var users = [];
		var dealContactId;
		var dealAccountId;
		var recipientName;
		var phoneField="Mobile";
		var unsubscribedField;
        document.addEventListener("DOMContentLoaded", function(event) {
        	var timeList=["12:00 AM","12:15 AM","12:30 AM","12:45 AM","01:00 AM","01:15 AM","01:30 AM","01:45 AM","02:00 AM","02:15 AM","02:30 AM","02:45 AM","03:00 AM","03:15 AM","03:30 AM","03:45 AM","04:00 AM","04:15 AM","04:30 AM","04:45 AM","05:00 AM","05:15 AM","05:30 AM","05:45 AM","06:00 AM","06:15 AM","06:30 AM","06:45 AM","07:00 AM","07:15 AM","07:30 AM","07:45 AM","08:00 AM","08:15 AM","08:30 AM","08:45 AM","09:00 AM","09:15 AM","09:30 AM","09:45 AM","10:00 AM","10:15 AM","10:30 AM","10:45 AM","11:00 AM","11:15 AM","11:30 AM","11:45 AM","12:00 PM","12:15 PM","12:30 PM","12:45 PM","01:00 PM","01:15 PM","01:30 PM","01:45 PM","02:00 PM","02:15 PM","02:30 PM","02:45 PM","03:00 PM","03:15 PM","03:30 PM","03:45 PM","04:00 PM","04:15 PM","04:30 PM","04:45 PM","05:00 PM","05:15 PM","05:30 PM","05:45 PM","06:00 PM","06:15 PM","06:30 PM","06:45 PM","07:00 PM","07:15 PM","07:30 PM","07:45 PM","08:00 PM","08:15 PM","08:30 PM","08:45 PM","09:00 PM","09:15 PM","09:30 PM","09:45 PM","10:00 PM","10:15 PM","10:30 PM","10:45 PM","11:00 PM","11:15 PM","11:30 PM","11:45 PM"];
    		var timeOptions ="";
    		timeList.forEach(function(time){
    			 timeOptions = timeOptions +"<option  value='"+time+"'>"+time+"</option>"
    		});
        	ZOHO.embeddedApp.on("PageLoad", function(record) {
               	recordIds = record.EntityId;
               	recordModule = record.Entity;
               	ButtonPosition = record.ButtonPosition;
				ZOHO.CRM.API.getOrgVariable("clickatellsmscrm__phonefields").then(function(phoneFieldsData){
					if(phoneFieldsData && phoneFieldsData.Success && phoneFieldsData.Success.Content && phoneFieldsData.Success.Content != "0"){
        				var phoneFieldsSetting =JSON.parse(phoneFieldsData.Success.Content);
        				phoneField = phoneFieldsSetting[recordModule];
        			}
				});
				try{
					ZOHO.CRM.API.getOrgVariable("clickatellsmscrm__EventReminderSettings").then(function(settings){
						if(settings && settings.Success && settings.Success.Content && settings.Success.Content != "0"){
	        				var settingsData =JSON.parse(settings.Success.Content);
	        				if(settingsData && settingsData["unsubscribedFieldMap"]){
	        					let unsubscribedFieldMap = settingsData["unsubscribedFieldMap"];
	        					unsubscribedField = unsubscribedFieldMap[recordModule];
	        				}
	        			}
					});
				}
				catch(e){
					console.log(e);
				}
				
				ZOHO.CRM.META.getFields({"Entity":"Users"}).then(function(data){
					userFields = data.fields;
					data.fields.forEach(function(field){
						addListItem("dropdown-menu-user",field.field_label,"dropdown-item","Users."+field.field_label);
					});
				});
				ZOHO.CRM.META.getFields({"Entity":"clickatellsmscrm__SMS_History"}).then(function(data){
					historyFields = data.fields;
				});		
				ZOHO.CRM.API.getAllUsers({Type:"AllUsers"}).then(function(data){
					users = data.users;
				});		
				ZOHO.CRM.API.getOrgVariable("clickatellsmscrm__countyCode").then(function(apiKeyData){
					if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content && apiKeyData.Success.Content != "0"){
						countryCode = apiKeyData.Success.Content;
					}
	               	ZOHO.CRM.API.searchRecord({Entity:"clickatellsmscrm__SMS_Templates",Type:"criteria",Query:"(clickatellsmscrm__Module_Name:equals:"+recordModule+")"})
					.then(function(data){
						smsTemplates = data.data;
						var templateList="";
						if(data.data){
							for(let i=0;i <data.data.length;i++){
								templateList =templateList+ '<li class="templateItem" id="'+data.data[i].id+'" onclick="showsms(this)"></li>';
							}
							$('#templateList').append(templateList);
							if(templateList == ""){
								$('#templateList').append('<li style="text-align:center;">No Templates</li>');
							}
							else{
								for(let i=0;i <data.data.length;i++){
									document.getElementById(data.data[i].id).innerText = data.data[i].Name;
								}
							}
						}
						else{
							$('#templateList').append('<li style="text-align:center;">No Templates</li>');
						}	
						
		                	
			            ZOHO.CRM.API.getRecord({Entity:recordModule,RecordID:recordIds})
						.then(function(data2){
							currentRecords = data2.data;
							selectModule(recordModule,data2.data[0]);
							if(record.ButtonPosition == "DetailView"){
								func_name = "clickatellsmscrm__smshandler";
								if(recordModule != "Deals"){
									if(recordModule != "Accounts"){
										recipientName = data2.data[0].Full_Name;
									}
									else{
										recipientName = data2.data[0].Account_Name;
									}
									document.getElementById("loader").style.display= "none";
									if(document.getElementById("container")){
										document.getElementById("container").style.display= "block";
									}
								}
								else{
									if(data2.data[0].Account_Name && data2.data[0].Account_Name.id){
										dealAccountId = data2.data[0].Account_Name.id;
										ZOHO.CRM.API.getRecord({Entity:"Accounts",RecordID:data2.data[0].Account_Name.id})
										.then(function(data3){
											var NumberList = "";
											var selectedNumber = "";
											if(data3.data[0].Phone != null){
												NumberList =NumberList+ '<li class="templateItem" onclick="setNumber(this)">Accounts Phone('+data3.data[0].Phone+') </li>';
												selectedNumber='Accounts Phone('+data3.data[0].Phone+')';
											}
											if(data2.data[0].Contact_Name && data2.data[0].Contact_Name.id){
												dealContactId = data2.data[0].Contact_Name.id
												ZOHO.CRM.API.getRecord({Entity:"Contacts",RecordID:data2.data[0].Contact_Name.id})
												.then(function(data4){
													var record = data4.data[0];
													recipientName = record.Full_Name;
													var phoneFields = ["Mobile","Phone","Asst Phone","Home Phone","Other Phone"];
													phoneFields.forEach(function(field){
														if(record[field.replace(" ", "_")] != null){
															if(selectedNumber == ""){
																selectedNumber = 'Contacts '+field+'('+record[field.replace(" ", "_")]+')';
															}
															NumberList =NumberList+ '<li class="templateItem" onclick="setNumber(this)">Contacts '+field+'('+record[field.replace(" ", "_")]+') </li>';
														}
													});	
													if(NumberList == ""){
														document.getElementById("ErrorText").innerText = "Mobile field is empty.";
														document.getElementById("Error").style.display= "block";
													}
													else{
														if(document.getElementById("selectedNumber")){
															document.getElementById("selectedNumber").innerText = selectedNumber;
															document.getElementById("numbertooltiptext").innerText = selectedNumber;
														}	
														selectedNumber = selectedNumber.substring(selectedNumber.indexOf("(")+1,selectedNumber.indexOf(")"));
														if(selectedNumber){
										        			selectedNumber = selectedNumber.replace(/\D/g,'');

															if(countryCode && selectedNumber.length > countryCode.length && selectedNumber.substring(0,countryCode.length) != countryCode){
																selectedNumber = countryCode+""+selectedNumber;
															}
														}	
														document.getElementById("editNumber").value =  selectedNumber;
													}
													$('#NumberList').append(NumberList);
													document.getElementById("loader").style.display= "none";
													if(document.getElementById("container")){
														document.getElementById("container").style.display= "block";
													}
												});
											}	
											else{
												if(NumberList == ""){
													document.getElementById("ErrorText").innerText = "Mobile field is empty.";
													document.getElementById("Error").style.display= "block";
												}
												else{
													if(document.getElementById("selectedNumber")){
														document.getElementById("selectedNumber").innerText = selectedNumber;
														document.getElementById("numbertooltiptext").innerText = selectedNumber;
													}	
													selectedNumber = selectedNumber.substring(selectedNumber.indexOf("(")+1,selectedNumber.indexOf(")"));
													if(selectedNumber){
									        			selectedNumber = selectedNumber.replace(/\D/g,'');

														if(countryCode && selectedNumber.length > countryCode.length && selectedNumber.substring(0,countryCode.length) != countryCode){
															selectedNumber = countryCode+""+selectedNumber;
														}
													}	
													document.getElementById("editNumber").value =  selectedNumber;
												}
												$('#NumberList').append(NumberList);
												document.getElementById("loader").style.display= "none";
												if(document.getElementById("container")){
													document.getElementById("container").style.display= "block";
												}
											}
										});
									}
									else if(data2.data[0].Contact_Name && data2.data[0].Contact_Name.id){
										dealContactId = data2.data[0].Contact_Name.id;
										ZOHO.CRM.API.getRecord({Entity:"Contacts",RecordID:data2.data[0].Contact_Name.id})
										.then(function(data4){
											var NumberList = "";
											var selectedNumber = "";
											var record = data4.data[0];
											recipientName = record.Full_Name;
											var phoneFields = ["Mobile","Phone","Asst Phone","Home Phone","Other Phone"];
											phoneFields.forEach(function(field){
												if(record[field.replace(" ", "_")] != null){
													if(selectedNumber == ""){
														selectedNumber = 'Contacts '+field+'('+record[field.replace(" ", "_")]+')';
													}
													NumberList =NumberList+ '<li class="templateItem" onclick="setNumber(this)">Contacts '+field+'('+record[field.replace(" ", "_")]+') </li>';
												}
											});	
											if(NumberList == ""){
												document.getElementById("ErrorText").innerText = "Mobile field is empty.";
												document.getElementById("Error").style.display= "block";
											}
											else{
												if(document.getElementById("selectedNumber")){
													document.getElementById("selectedNumber").innerText = selectedNumber;
												}
												if(document.getElementById("numbertooltiptext")){
													document.getElementById("numbertooltiptext").innerText = selectedNumber;
												}
												selectedNumber = selectedNumber.substring(selectedNumber.indexOf("(")+1,selectedNumber.indexOf(")"));
												if(selectedNumber){
								        			selectedNumber = selectedNumber.replace(/\D/g,'');

													if(countryCode && selectedNumber.length > countryCode.length && selectedNumber.substring(0,countryCode.length) != countryCode){
														selectedNumber = countryCode+""+selectedNumber;
													}
												}	
												document.getElementById("editNumber").value =  selectedNumber;
											}
											$('#NumberList').append(NumberList);
											document.getElementById("loader").style.display= "none";
											if(document.getElementById("container")){
												document.getElementById("container").style.display= "block";
											}
										});
									}
									else{
										document.getElementById("loader").style.display= "none";
										if(document.getElementById("container")){
											document.getElementById("container").style.display= "block";
										}
									}
								}	
							}	
							else{
								document.getElementById("editNumberui").style.display= "none";
								document.getElementById("loader").style.display= "none";
								if(document.getElementById("container")){
									document.getElementById("container").style.display= "block";
								}
								func_name = "clickatellsmscrm__bulksms";
							}	
						})
						
					}) 
				});	
	        });
        	ZOHO.embeddedApp.init().then(function(){
				$('#timeList').append(timeOptions);
	   //  		var aestTime = new Date().toLocaleString("en-US", {timeZone: "Australia/Brisbane"});
				// aestTime = new Date(aestTime);
	    		$('#datepicker').datepicker().datepicker('setDate',new Date());
	    		var date = document.getElementById("datepicker").value;
	    		var time = document.getElementById("timeList").value;
	    		document.getElementById("scheduledDateTime").innerText=new Date(date).toDateString()+" at "+time +" ("+Intl.DateTimeFormat().resolvedOptions().timeZone+")";

	        	ZOHO.CRM.API.getOrgVariable("clickatellsmscrm__clickatellApiKey").then(function(apiKeyData){
					if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content == ""){
						document.getElementById("ErrorText").innerText = "Please enter your Clickatell api key in extension configuration page.";
			        	document.getElementById("Error").style.display= "block";
					}
					else{
						apiKey = apiKeyData.Success.Content;
					}
				});	
				ZOHO.CRM.API.getOrgVariable("clickatellsmscrm__Clickatell_Phone_Number").then(function(apiKeyData){
	        		if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content && valueExists(apiKeyData.Success.Content)){
	        			 src = apiKeyData.Success.Content;
	        		}	
	        	});	
        	});
        	const el = document.getElementById('emailContentEmail');

			el.addEventListener('paste', (e) => {
			  // Get user's pasted data
			  let data = e.clipboardData.getData('text/html') ||
			      e.clipboardData.getData('text/plain');
			  
			  // Filter out everything except simple text and allowable HTML elements
			  let regex = /<(?!(\/\s*)?()[>,\s])([^>])*>/g;
			  data = data.replace(regex, '');
			  
			  // Insert the filtered content
			  document.execCommand('insertHTML', false, data);

			  // Prevent the standard paste behavior
			  e.preventDefault();
			});
			var content_id = 'emailContentEmail';  
			max = 1600;
			//binding keyup/down events on the contenteditable div
			$('#'+content_id).keyup(function(e){ check_charcount(content_id, max, e); });
			$('#'+content_id).keydown(function(e){ check_charcount(content_id, max, e); });

			function check_charcount(content_id, max, e)
			{   
			    if(e.which != 8 && $('#'+content_id).text().length > max)
			    {
			    	document.getElementById("ErrorText").innerText = "Message should be within 2000 characters.";
	        		document.getElementById("Error").style.display= "block";
	        		// document.getElementById("ErrorText").style.color="red";
					setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
			       // $('#'+content_id).text($('#'+content_id).text().substring(0, max));
			       e.preventDefault();
			    }
			}
        });

		// function selectModule(module){
		// 	document.getElementById("moduleFields").innerText = "Insert "+module+" Fields";
		// 	document.getElementById("dropdown-menu-email").innerHTML="";
		// 	ZOHO.CRM.META.getFields({"Entity":module}).then(function(data){
		// 		moduleFields = data.fields;
		// 		data.fields.forEach(function(field){
		// 			addListItem("dropdown-menu-email",field.field_label,"dropdown-item",module+"."+field.field_label);
		// 		});
		// 	});	
		// }
		function selectModule(module,record){
			document.getElementById("moduleFields").innerText = "Insert "+module+" Fields";
			var customerData = [];
			var phoneFields = [];	
			var phoneFieldsApiNames=[];	
			ZOHO.CRM.META.getFields({"Entity":module}).then(function(data){
				moduleFields = data.fields;
				data.fields.forEach(function(field){
					customerData.push(field.field_label);
					
				})
				document.getElementById("dropdown-menu-email").innerHTML="";
				customerData.forEach(function(field){
					addListItem("dropdown-menu-email",field,"dropdown-item",module+"."+field);
				});	
				if(module != "Deals" && func_name == "clickatellsmscrm__smshandler"){
					var NumberList = "";
					var selectedNumber = "";
					data.fields.forEach(function(field){
						if(field.data_type == "phone"){
							if(record[field.api_name] != null){
								if(selectedNumber == ""){
									selectedNumber = field.field_label+'('+record[field.api_name]+')';
								}
								NumberList =NumberList+ '<li class="templateItem" onclick="setNumber(this)">'+field.field_label+'('+record[field.api_name]+') </li>';
							}
						}	
					});	
					if(NumberList == ""){
						document.getElementById("ErrorText").innerText = "Mobile field is empty.";
						document.getElementById("Error").style.display= "block";
					}
					else{
						if(document.getElementById("selectedNumber")){
	        				document.getElementById("selectedNumber").innerText = selectedNumber;
						}
						if(document.getElementById("numbertooltiptext")){
							document.getElementById("numbertooltiptext").innerText = selectedNumber;
						}
						selectedNumber = selectedNumber.substring(selectedNumber.indexOf("(")+1,selectedNumber.indexOf(")"));
						if(selectedNumber){
		        			selectedNumber = selectedNumber.replace(/\D/g,'');

							if(countryCode && selectedNumber.length > countryCode.length && selectedNumber.substring(0,countryCode.length) != countryCode){
								selectedNumber = countryCode+""+selectedNumber;
							}
						}	
						document.getElementById("editNumber").value =  selectedNumber;
					}
					$('#NumberList').append(NumberList);
				}	
			});	
		}
		function setNumber(editor){
			var selectedNumber =editor.innerText.substring(editor.innerText.indexOf("(")+1,editor.innerText.indexOf(")"));
			if(selectedNumber){
    			selectedNumber = selectedNumber.replace(/\D/g,'');

				if(countryCode && selectedNumber.length > countryCode.length && selectedNumber.substring(0,countryCode.length) != countryCode){
					selectedNumber = countryCode+""+selectedNumber;
				}
			}	
			if(document.getElementById("selectedNumber")){
				document.getElementById("selectedNumber").innerText =  editor.innerText;
			}
			document.getElementById("editNumber").value =  selectedNumber;
			if(document.getElementById("numbertooltiptext")){
				document.getElementById("numbertooltiptext").innerText = editor.innerText;
			}
		}
        function showsms(editor){
			for(var i=0; i<smsTemplates.length;i++){
				if(smsTemplates[i].id == editor.id){
					templateId = smsTemplates[i].id;
					document.getElementById("selectedTemplate").innerText = smsTemplates[i].Name;
					document.getElementById("tooltiptext").innerText = smsTemplates[i].Name;
					document.getElementById("emailContentEmail").innerText = smsTemplates[i].clickatellsmscrm__Message;
					break;
				}
			}
		}
		function checkMobileNumber(Mobile,currentRecord){
			if(func_name == "clickatellsmscrm__bulksms"){
				return Promise.all([getMobileNumber(Mobile,currentRecord)]).then(function(resp){
					if(resp[0].Mobile){
						Mobile = resp[0].Mobile.replace(/\D/g,'');
						var request ={
					        url : "https://rest.messagebird.com/lookup/" + Mobile,
					        headers:{
					              Authorization:"AccessKey 7NMPor0R8DofSHH61SpViNNqQ",
					        }
					    }
					    return ZOHO.CRM.HTTP.get(request).then(function(phoneData){
					    	if(JSON.parse(phoneData).countryPrefix == null){
					    		if(countryCode && Mobile.length > countryCode.length){
									Mobile = countryCode+""+Mobile;
								}
					    	}
					    	return {"Mobile":Mobile,"toModule":resp[0].toModule,"toId":resp[0].toId,"recipientName":resp[0].recipientName};
					    });	
					}
					else{
						return {};
					}    
				});
			}
			else{
				return {"Mobile":Mobile};
			}	
		}
		function getMobileNumber(Mobile,currentRecord){
			if(recordModule == "Deals" && phoneField.indexOf(".") != -1){
				var toModule = phoneField.substring(0,phoneField.indexOf(".")-1);
				var dealphoneField = phoneField.substring(phoneField.indexOf(".")+1);
				if(currentRecord[toModule+"_Name"] && currentRecord[toModule+"_Name"].id ){
					var toId = currentRecord[toModule+"_Name"].id ;
					return ZOHO.CRM.API.getRecord({Entity:toModule+"s",RecordID:toId}).then(function(contactData){
						if(contactData.data[0][dealphoneField] != null && contactData.data[0][dealphoneField] != ""){
							if(toModule == "Contact"){
								var recipientName = contactData.data[0].Full_Name;
							}
							else if(toModule == "Account"){
								var recipientName = contactData.data[0].Account_Name;
							}	
							return {"Mobile":contactData.data[0][dealphoneField],"toModule":toModule,"toId":toId,"recipientName":recipientName};
						}
					});	
				}
				else{
					return {"Mobile":null};
				}
			}	
			else{
				return {"Mobile":currentRecord[phoneField]};
			}
		}
        function sendSMS(){
        	var MobileNumber = document.getElementById("editNumber").value;
        	if(document.getElementById("selectedNumber")){
	        	var toModule = recordModule?document.getElementById("selectedNumber").innerText.substring(0,7):"";
	        	if(toModule == "Contact"){
	        		var toId = dealContactId;
	        	}
	        	else if(toModule == "Account"){
	        		var toId = dealAccountId;
	        	}
	        }	
        	var date = document.getElementById("datepicker").value;
    		var time = document.getElementById("timeList").value;
    		if(document.getElementById("emailContentEmail").innerText.length >1600){
    			document.getElementById("ErrorText").innerText = "Message should be within 2000 characters.";
        		document.getElementById("Error").style.display= "block";
        		document.getElementById("Error").style.color="red";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
    		}
    		else if(scheduledTime && new Date(date+" "+time).getTime() < new Date().getTime()){
    			document.getElementById("ErrorText").innerText = "Schedule time should be in future.";
    			document.getElementById("Error").style.display= "block";
    		}
        	else if(document.getElementById("emailContentEmail").innerText.replace(/\n/g,"").replace(/\t/g,"").replace(/ /g,"") == ""){
        		document.getElementById("ErrorText").innerText = "Message cannot be empty.";
        		document.getElementById("Error").style.display= "block";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
        	}
        	else if(func_name == "clickatellsmscrm__smshandler" && (MobileNumber == null || MobileNumber == "")){
        		document.getElementById("ErrorText").innerText = "Mobile field is empty.";
		        document.getElementById("Error").style.display= "block";
		        setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
        	}
        	else{
        		var notes = "";
        		if(func_name == "clickatellsmscrm__bulksms"){
        			MobileNumber =null;
        			notes = "\n("+recordModule +" without mobile number will be ignored.)";
        		}
	        	document.getElementById("ErrorText").innerText = "Sending... "+notes;
				document.getElementById("Error").style.display= "block";
	        	var message = document.getElementById("emailContentEmail").innerText;
				var successRecords=0;
	        	var recordsLength = currentRecords.length;
	        	var recordIndex = 0;
				currentRecords.forEach(function(currentRecord){
					if(!unsubscribedField || !currentRecord[unsubscribedField]){
	        		if(func_name == "clickatellsmscrm__bulksms"){
	        			if(recordModule != "Deals"){
	        				MobileNumber = currentRecord[phoneField];
	        			}
	        			else{

	        			}

	        			if(MobileNumber){
		        			MobileNumber = MobileNumber.replace(/\D/g,'');

							if(countryCode && MobileNumber.length > countryCode.length && MobileNumber.substring(0,countryCode.length) != countryCode){
								MobileNumber = countryCode+""+MobileNumber;
							}
						}	
	        		}
	        		Promise.all([checkMobileNumber(MobileNumber,currentRecord)]).then(function(numberresp){
	        			MobileNumber = numberresp[0].Mobile;
	        			if(func_name == "clickatellsmscrm__bulksms"){
	        				toModule = numberresp[0].toModule;
	        				toId = numberresp[0].toId;
	        				recipientName = numberresp[0].recipientName;
	        			}	
		        		var recordId = currentRecord.id;
		        		var argumentsData = {
							"message" : message,
							"recordModule" : recordModule,
							"scheduledTime":scheduledTime,
							"templateId":templateId,
							"recordId" : recordId,
							"to":MobileNumber
						};
						var filledMessage = JSON.parse(JSON.stringify(getMessageWithFields(argumentsData,currentRecord)));
						if(!recipientName && currentRecord.Full_Name){
							recipientName = currentRecord.Full_Name;
						}
						var req_data={"Name":"SMS to " + recipientName,"clickatellsmscrm__Message":filledMessage,"clickatellsmscrm__Recipient_Number":MobileNumber};
						historyFields.forEach(function(field){
							if(field.data_type == "lookup" && field.lookup.module.api_name == recordModule){
								req_data[field.api_name]=currentRecord.id;
							}
						});
						if(recordModule == "Deals"){
							req_data["clickatellsmscrm__"+toModule]=toId;
						}
						filledMessage = filledMessage.trim();
						if(filledMessage.length > 2000)
						{
							document.getElementById("ErrorText").innerText = "Message is Too Large.";
			        		document.getElementById("Error").style.display= "block";
							setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
							return ;
						}
						else if(filledMessage.length < 1)
						{
							document.getElementById("ErrorText").innerText = "Merge Fields value is empty.";
			        		document.getElementById("Error").style.display= "block";
							setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
							return;
						}
						if(argumentsData.scheduledTime != null)
						{
							time = argumentsData.scheduledTime.substring(0,19) + "+00:00";
							req_data["clickatellsmscrm__Scheduled_Time"]=time.toString();
							req_data["clickatellsmscrm__Status"]="Scheduled";
						}
						if(!argumentsData.scheduledTime)
						{
							to = argumentsData.to?argumentsData.to.replace(/\D/g,''):"";
							console.log("mm"+filledMessage);
							url = "https://platform.clickatell.com/messages/http/send?apiKey=" + apiKey + "&to=" + to + "&content=" + encodeURIComponent(filledMessage);
							if(src != "0" && src != null && src != "null")
							{
								src = src.replace(/\D/g,'');
								url = url + "&from=" + src;
							}
							var request = {
				                url: url,
				                headers: {
				                	"Content-Type": "application/json"
				                }
				            };
							ZOHO.CRM.HTTP.get(request).then(function(resp){
								console.log("resp"+resp);
								recordIndex = recordIndex+1;
								resp = JSON.parse(resp);
								if(resp.messages && resp.messages.length >0)
								{
									resp = resp.messages[0];
								}
								if(!resp.error)
								{
									successRecords=successRecords+1;
									req_data["clickatellsmscrm__Status"]="Sent";
									req_data["clickatellsmscrm__Message_Id"]=resp.apiMessageId;
									
									ZOHO.CRM.API.insertRecord({Entity:"clickatellsmscrm__SMS_History",APIData:req_data,Trigger:["workflow"]}).then(function(response){
										var responseInfo	= response.data[0];
										var resCode			= responseInfo.code;
										if(resCode == 'SUCCESS'){
											if(func_name != "clickatellsmscrm__bulksms"){
												document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your SMS has been sent successfully.</div>';
												setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload(); }, 1500);
											}	
										}
										else{
											if(func_name != "clickatellsmscrm__bulksms"){
												document.getElementById("ErrorText").innerText = "Opps! Something went wrong from server side. Please try after sometimes!!!";
												setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
											}	
										}
									});
									if(recordIndex == recordsLength && func_name == "clickatellsmscrm__bulksms"){
										document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your SMS has been sent successfully.</div>';
										setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload(); }, 1500);
									}
									
								}
								else{

									if(recordIndex == recordsLength && func_name == "clickatellsmscrm__bulksms"){
										if(successRecords == 0){
											document.getElementById("ErrorText").innerHTML = "Mobile field is empty or invalid for all choosen " + argumentsData.recordModule+ ".";
										}
										else{
											document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your SMS has been sent successfully.</div>';
											setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload(); }, 1500);
										}
									}
									if(resp== "" || resp.toString().indexOf("Request-URI Too Large") != -1)
									{
										document.getElementById("ErrorText").innerHTML = "Message is Too Large";
										setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
									}
									else if(resp.error)
									{
										if(func_name != "clickatellsmscrm__bulksms"){
											document.getElementById("ErrorText").innerHTML = resp.error.replace(/phone/g,"mobile");
											setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
										}	
									}
									else{
										if(func_name != "clickatellsmscrm__bulksms"){
											document.getElementById("ErrorText").innerText = "Opps! Something went wrong from server side. Please try after sometimes!!!";
											setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
										}	
									}
								}	
							});	
						}
						else
						{
							ZOHO.CRM.API.insertRecord({Entity:"clickatellsmscrm__SMS_History",APIData:req_data,Trigger:["workflow"]}).then(function(response){
								recordIndex = recordIndex+1;
								var responseInfo	= response.data[0];
								var resCode			= responseInfo.code;
								if(resCode == 'SUCCESS'){
									successRecords=successRecords+1;
									if(func_name != "clickatellsmscrm__bulksms"){
										document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your SMS has been scheduled successfully.</div>';
										setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload();}, 1500);
									}	
									
								}
								else{
									if(func_name != "clickatellsmscrm__bulksms"){
										document.getElementById("ErrorText").innerText = "Opps! Something went wrong from server side. Please try after sometimes!!!";
						        		document.getElementById("Error").style.display= "block";
										setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
									}	
								}
								if(recordIndex == recordsLength && func_name == "clickatellsmscrm__bulksms"){
									document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your SMS has been scheduled successfully.</div>';
									setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload();}, 1500);
								}
							});
						}
					});	
					}
					else{
						recordIndex = recordIndex+1;
						if(recordIndex == recordsLength && func_name == "clickatellsmscrm__bulksms"){
							document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your SMS has been scheduled successfully.</div>';
							setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload();}, 1500);
						}
					}	
				});
			}		
        }
        function getMessageWithFields(messageDetails,currentRecord){
        	var message = JSON.parse(JSON.stringify(messageDetails.message));
			var customerData=[];
			var module = messageDetails.recordModule;
			if(messageDetails.recordModule == "Leads")
			{
				customerData = ["Lead Id","Annual Revenue","City","Company","Country","Created By","Created Time","Description","Designation","Email","Email Opt Out","Fax","First Name","Full Name","Industry","Last Activity Time","Last Name","Lead Source","Lead Status","Mobile","Modified By","Modified Time","No of Employees","Owner","Phone","Rating","Record Image","Salutation","Secondary Email","Skype ID","State","Street","Tag","Twitter","Website","Zip Code"];
			}
			else if(messageDetails.recordModule == "Contacts")
			{
				customerData = ["Contact Id","Account Name","Assistant","Asst Phone","Owner","Created By","Created Time","Date of Birth","Department","Description","Email","Email Opt Out","Fax","First Name","Full Name","Home Phone","Last Activity Time","Last Name","Lead Source","Mailing City","Mailing Country","Mailing State","Mailing Street","Mailing Zip","Mobile","Modified By","Modified Time","Other City","Other Country","Other Phone","Other State","Other Street","Other Zip","Phone","Record Image","Reporting To","Salutation","Secondary Email","Skype ID","Title","Twitter","Vendor Name"];
			}
			moduleFields.forEach(function(field){
				try{
				var replace = "\\${"+module+"."+field.field_label+"}";
				var re = new RegExp(replace,"g");
				if(currentRecord[field.api_name] != null)
				{
					var value = currentRecord[field.api_name];
					if(value.name)
					{
						value = value.name;
					}
					
					message = message.replace(re,value);
				}
				else
				{
					message = message.toString().replace(re," ");
				}
				}
				catch(e){
					console.log(e);
				}	
			});	
			customerData.forEach(function(field){
				if(field == "Contact Id" || field == "Lead Id" || field == "Account Id" || field == "Deal Id")
				{
					var rfield = "id";
				}
				else
				{
					var rfield = field;
				}
				try{
				var replace = "\\${"+module+"."+field+"}";
				var re = new RegExp(replace,"g");
				if(currentRecord[rfield.replace(/ /g,"_")] != null)
				{
					var value = currentRecord[rfield.replace(/ /g,"_") + ""];
					if(value.name)
					{
						value = value.name;
					}
					
					message = message.replace(re,value);
				}
				else
				{
					message = message.toString().replace(re," ");
				}
				}
				catch(e){
					console.log(e);
				}
			});
			if(currentRecord.Owner != null)
			{
				var ownerId = currentRecord.Owner.id;
			}
			if(ownerId != null)
			{
				var currentUser;
				users.forEach(function(user){
					if(user.id == ownerId){
						currentUser = user;
					}
				})
				if(currentUser){
					userFields.forEach(function(field){
						try{
						var replace = "\\${Users."+field.field_label+"}";
						var re = new RegExp(replace,"g");
						if(currentUser[field.api_name] != null)
						{
							var value = currentUser[field.api_name];
							if(value.name)
							{
								value = value.name;
							}
							
							message = message.replace(re,value);
						}
						else
						{
							message = message.toString().replace(re," ");
						}
						}
						catch(e){
							console.log(e);
						}
					});
					return message;
				}
			}
			else{
				return message;
			}
			
        }
        function valueExists(val) {
		    return val !== null && val !== undefined && val.length > 1 && val!=="null";
		}
		function googleTranslateElementInit() {
		  new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
		}
        function addListItem(id,text,className,value){
			if(className == "dropdown-item"){
				var linode = '<li class="'+className+'"><button class="'+className+'" onclick="insert(this)">'+text+'<input type="hidden" value="'+value+'"></button></li>';
			}
			else{
				var linode = '<li class="'+className+'">'+text+'</li>';
			}
			$('#'+id).append(linode);

        }
		function styling(tag)
		{
			document.execCommand(tag);
		}
		function link(){
			$("#linkForm").slideToggle("slow");
		}
		function image(){
			$("#imageForm").slideToggle("slow");
		}
		function addLink(){
			var href = document.getElementById("linkUrl").value;
		    if (range) {
				if(range.startOffset == range.endOffset){
					if(range.commonAncestorContainer.parentNode.href){
						range.commonAncestorContainer.parentNode.href=href;
					}
					else{
						var span = document.createElement('a');
						span.setAttribute('href',href);
						span.innerText = href;
						range.insertNode(span);
			        	range.setStartAfter(span);
			        }	
				}
				else{
					var data = range.commonAncestorContainer.data;
					var start = range.startOffset;
					var end = range.endOffset;
					range.commonAncestorContainer.data="";
					var span = document.createElement('span');
					span.appendChild( document.createTextNode(data.substring(0,start)) );
					var atag = document.createElement('a');
					atag.setAttribute('href',href);
					atag.innerText = data.substring(start,end);
					span.appendChild(atag);
					span.appendChild( document.createTextNode(data.substring(end)) );
					range.insertNode(span);
		        	range.setStartAfter(span);
				}
		        range.collapse(true);
		    }
			$("#linkForm").slideToggle("slow");
		}
		function addImage(){
			var href = document.getElementById("imageUrl").value;
			var span = document.createElement('img');
			span.setAttribute('src',href);
			span.innerText = href;
			range.insertNode(span);
        	range.setStartAfter(span);
			$("#imageForm").slideToggle("slow");
		}
		function openlink(){
			sel = window.getSelection();
		    if (sel && sel.rangeCount) {
		        range = sel.getRangeAt(0);
		      }  
			if(range && range.commonAncestorContainer.wholeText){
				if(range.commonAncestorContainer.parentNode.href){
					document.getElementById("linkUrl").value = range.commonAncestorContainer.parentNode.href;
					$("#linkForm").slideToggle("slow");
				}
			}	
		}
		function insert(bookingLink){
    		// var bookingLink = this;
			var range;

			if (sel && sel.rangeCount && isDescendant(sel.focusNode)){
		        range = sel.getRangeAt(0);
		        range.collapse(true);
    		    var span = document.createElement("span");
    		    span.appendChild( document.createTextNode('${'+bookingLink.children[0].value+'}') );
        		range.insertNode(span);
	    		range.setStartAfter(span);
		        range.collapse(true);
		        sel.removeAllRanges();
		        sel.addRange(range);
		    }    
		}
		function isDescendant(child) {
			var parent = document.getElementById("emailContentEmail");
		     var node = child.parentNode;
		     while (node != null) {
		         if (node == parent || child == parent) {
		             return true;
		         }
		         node = node.parentNode;
		     }
		     return false;
		}
		function enableSchedule(element){
			if(element.checked == true){
				document.getElementById("send").innerText="Schedule";
				var date = document.getElementById("datepicker").value;
    			var time = document.getElementById("timeList").value;
    			scheduledTime = new Date(date+" "+time).toISOString();
			}
			else{
				document.getElementById("send").innerText="Send";
				scheduledTime = undefined;
			}
		}
		function openDatePicker(){
    		document.getElementById("dateTime").style.display= "block";
    		if(ButtonPosition == "DetailView"){
    			document.getElementById("dateTime").style.top= "84%";
    		}
    		else{
    			document.getElementById("dateTime").style.top= "60%";
    		}
    		document.getElementById("Error").style.display= "block";
		}	
		function scheduleClose(){
			var date = document.getElementById("datepicker").value;
    		var time = document.getElementById("timeList").value;
    		if(new Date(date+" "+time).getTime() < new Date().getTime()){
    			document.getElementById("ErrorText").innerText = "Schedule time should be in future.";
    		}
    		else{
    			document.getElementById("ErrorText").innerText = "";
	    		document.getElementById("dateTime").style.display= "none";
	    		document.getElementById("Error").style.display= "none";
	    		document.getElementById("scheduleCheck").checked =true;
	    		document.getElementById("send").innerText="Schedule";
	    		document.getElementById("scheduledDateTime").innerText=new Date(date).toDateString()+" at "+time +" ("+Intl.DateTimeFormat().resolvedOptions().timeZone+")";
	    		scheduledTime = new Date(date+" "+time).toISOString();
	    	}	
		}
		function cancel(){
			document.getElementById("Error").style.display= "none";
		}

