var extensionName = 'RingCentral';
var extensionAPI = 'ringcentralsmsforzohocrm1__';

var extensionConnectorName = 'ringcentral';
var extensionConnector = extensionAPI.split('__')[0]+"."+extensionConnectorName;

var extensionFunction = 'sendsmsapi';
var extensionInvokeAPI = extensionAPI + extensionFunction;
var extensionCredential = extensionAPI + 'inSettings';

var inSettings = {};


async function invokeConnector(ApiName, body) {

    return await ZOHO.CRM.CONNECTOR.invokeAPI(extensionConnector+"."+ApiName, body).then(function(data) {

        if(data && data.response)
        return JSON.parse(data.response);
        else
        return false;

    });

}

async function isAuthorizedToConnector() {
    
    return await ZOHO.CRM.CONNECTOR.isConnectorAuthorized(extensionConnector).then(function(data) {
        
        if(data && data != "true")
        return false;
        else
        return true;

    });

}

async function authorizedToConnector() {

    return await ZOHO.CRM.CONNECTOR.authorize(extensionConnector).then(function(data) {
        
        if(data && data != "true")
        return false;
        else
        return true;
        
    });

}

var checkModules = [];
var checkModulesNames = [];
var createModule = "";

async function newAuthorizedToConnector() {

    await authorizedToConnector().then(async function(Authorized) {

        document.getElementById("loader").style.display= "flex";
        isAuthorized = Authorized;
        if(!Authorized)
        window.location.reload();
        else {
            await reAuthorizedToConnector();
        }

    });

}

document.addEventListener("DOMContentLoaded", async function(event) {

    await ZOHO.embeddedApp.init().then(async function() {

        await getAPIURL();
        

        await reAuthorizedToConnector();

    });

});


async function reAuthorizedToConnector() {

    await isAuthorizedToConnector().then(async function(isAuthorized) {

            if(!isAuthorized) {
                $('.authorizationFaild').show();
                $('.extraSettingPage').hide();
                document.getElementById("loader").style.display= "none";
            }
            else {

                $('.authorizationFaild').hide();
                $('.extraSettingPage').show();

                await modulesListLoad();
                await modulesListLoadCreate();

                await ZOHO.CRM.API.getOrgVariable(extensionCredential).then(function(apiKeyData){   
            
                    if(apiKeyData && apiKeyData.Success && apiKeyData.Success.Content && apiKeyData.Success.Content != "0"){
                        credentials = JSON.parse(apiKeyData.Success.Content);
                        if(credentials.modules && credentials.createModule) {

                            checkModules = credentials.modules;
                            createModule = credentials.createModule;
                            
                            for(let i=0; i<checkModules.length;i++) {
                                let checkModule = checkModules[i];
                                $('#modulesListIncomingSup_'+checkModule).find('.modulesListIncomingSup').click();
                            }

                            $('#modulesListIncomingCreateSup_'+createModule).find('.modulesListIncomingCreateSup').click();
                        }
                        else {
                            // $('.cancelSettingsButt').hide();
                            // CredentialsUpdateShow();
                        }
                    }   

                }); 

                $('.outerOfSettingDetails').show();

                document.getElementById("loader").style.display= "none";
            }

        }); 

}

function textCopyInCommand(selected) {
    $('.webHookurlCopiedDiv').hide();
    let copyFrom = $('<textarea/>');
    copyFrom.text($(selected).attr('data-copyText'));
    $('body').append(copyFrom);
    copyFrom.select();
    document.execCommand('copy');
    copyFrom.remove();

    $('.copyTextDiv').fadeIn(500);
    $('.copyTextDiv').fadeOut(1500);
}
        

async function getAPIURL() {

    let urlParams = new URLSearchParams(window.location.search);
    let serviceOrigin = urlParams.get('serviceOrigin');

    let getmap = {"nameSpace":"<portal_name.extension_namespace>"};
    let resp = await ZOHO.CRM.CONNECTOR.invokeAPI("crm.zapikey",getmap);
    let zapikey = JSON.parse(resp).response;
    let domain = "com";
    if(serviceOrigin.indexOf(".zoho.") != -1){
        domain = serviceOrigin.substring(serviceOrigin.indexOf(".zoho.")+6);
    }
    let webHookurl = `https://platform.zoho.${domain}/crm/v2/functions/${extensionInvokeAPI}/actions/execute?auth_type=apikey&zapikey=${zapikey}`;
    document.getElementById("webHookurl").innerText = webHookurl;

    $('.webHookurlCopyLogo').attr('data-copyText', webHookurl);

}
       
function updateOrgVariables(apiname, value){
    ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": apiname,"value": value}).then(function(res) {
        //wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingMsgSuccess">${verifiedTickSVG}</div><div class="sendingMsgSuccessHint">${'Saved'}</div></div>`,'','Okay',true,true);
        //CredentialsUpdateHide();
        //setTimeout(function(){ wcConfirmHide(); }, 1500);
    });
}

function saveCreds(){

    let api_key = document.getElementById('apikey').value;
    let sender_id = document.getElementById('senderId').value;
    //let extraId = document.getElementById('extraId').value;

    if(api_key == '' || api_key == null) {
        wcConfirm('Please enter API Key..',"",'Okay',true,false);
        return;
    }
    else if(sender_id == '' || sender_id == null) {
        wcConfirm('Please enter Sender Id..',"",'Okay',true,false);
        return;
    }
    // else if(extraId == '' || extraId == null) {
    //     wcConfirm('Please enter Sender Id..',"",'Okay',true,false);
    //     return;
    // }
    else if(api_key == credentials['apikey'] && sender_id == credentials['senderId']) {
        wcConfirm('No Changes..',"$('#Error').hide()",'Okay',true,false);
        return;
    }
    else {

        wcConfirm(`<div class="sendingLoaderOuter"><div class="sendingLoaderInner">${loader}</div><div class="sendingLoaderHint">Saving...</div></div>`,'','Okay',true,true);

        credentials['apikey'] = api_key;
        credentials['senderId'] = sender_id;                
        //credentials['extraId'] = extraId;
        updateOrgVariables(extensionCredential, credentials);

    }
    
}

var loader = `<div class="wcMsgLoadingInner" title="loading…"><svg class="wcMsgLoadingSVG" width="17" height="17" viewBox="0 0 46 46" role="status"><circle class="wcMsgLoadingSvgCircle" cx="23" cy="23" r="20" fill="none" stroke-width="6" style="stroke: rgb(57 82 234);"></circle></svg></div>`;

var verifiedTickSVG = `<svg class="sendingMsgSuccessSpan" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000"><g><rect fill="none" height="24" width="24"/></g><g><path d="M23,12l-2.44-2.79l0.34-3.69l-3.61-0.82L15.4,1.5L12,2.96L8.6,1.5L6.71,4.69L3.1,5.5L3.44,9.2L1,12l2.44,2.79l-0.34,3.7 l3.61,0.82L8.6,22.5l3.4-1.47l3.4,1.46l1.89-3.19l3.61-0.82l-0.34-3.69L23,12z M10.09,16.72l-3.8-3.81l1.48-1.48l2.32,2.33 l5.85-5.87l1.48,1.48L10.09,16.72z"/></g></svg>`;

        
function wcConfirm(htmlText, runFunc, confirmTitle, alart, buttonHide) {
    $('.wcConfirmOuter').remove();
    $('body').append(`<div class="wcConfirmOuter">
        <div class="wcConfirmInner divPopUpShow">
            <div class="wcConfirmBody">
                ${htmlText}
            </div>
            <div class="wcConfirmButt" style="display:${buttonHide?'none':'flex'};">
                <div class="wcConfirmCancel" style="display:${alart?'none':'block'};" onclick="wcConfirmHide()">Cancel</div>
                <div class="wcConfirmYes" onclick="wcConfirmHide();${runFunc}">${confirmTitle}</div>
            </div>
        </div>
    </div>`);
}

function wcConfirmHide() {

        $('.wcConfirmInner').removeClass('divPopUpHide').removeClass('divPopUpShow').addClass('divPopUpHide').hide();
        $('.wcConfirmOuter').fadeOut(0);
        setTimeout(function() { $('.wcConfirmInner').removeClass('divPopUpHide').removeClass('divPopUpShow').html('').hide().fadeOut(); $('.wcConfirmOuter').remove(); }, 300);

}

$(document).keypress(function(e) {

    if($('.wcConfirmYes').is(':visible'))
    if(e.which == 13) {
        $('.wcConfirmYes').click();
    }

});

function CredentialsUpdateShow() {
    $('.extensionSettingInput').show();
    $('.hideCredentialsDetails').hide();
    $('.editCredentialsDetails').hide();
    $('.saveSettingsButtOut').show();
}

function CredentialsUpdateHide() {

    if(credentials.apikey && credentials.senderId) {
        $("#apikeyHide").html(credentials.apikey.substring(0,5)+"&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;");
        $("#senderIdHide").html("&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;");
        //$("#extraIdHide").html("&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;&#9733;");

        $('.extensionSettingInput').hide();
        $('.hideCredentialsDetails').show();
        $('.editCredentialsDetails').show();        
        $('.cancelSettingsButt').show();
        $('.saveSettingsButtOut').hide();
    }
    
}

async function modulesListLoad() {
    await ZOHO.CRM.META.getModules().then(function(data){
        data.modules.forEach(function(module){
            addListItem("modulesListIncoming",module.module_name,"dropdown-item",module.api_name);
        });
    });
}

function addListItem(id,text,className,value){

    let linode = `<li class="${className}" id="modulesListIncomingSup_${value}"><button class="dropdown_Butt modulesListIncomingSup" onmouseover="checkModuleList_onmouseover(this)" onmouseout="checkModuleList_onmouseonmouseout(this)" onShow="false" onclick="insert(this)" value="${value}" text="${text}">${text}<span class="addModuleToCheckListSVG" style="display: none;"><svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="18px" fill="#45596b" style="fill: #45596b;"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M18 13h-5v5c0 .55-.45 1-1 1s-1-.45-1-1v-5H6c-.55 0-1-.45-1-1s.45-1 1-1h5V6c0-.55.45-1 1-1s1 .45 1 1v5h5c.55 0 1 .45 1 1s-.45 1-1 1z"></path></svg></span></button></li>`;
    $('#'+id).append(linode);

}

function checkModuleList_onmouseover(selected) {

    if($(selected).attr("onShow") == "false") {
        $(selected).find('.addModuleToCheckListSVG').show();
    }
    else {
        //$(selected).find('.addModuleToCheckListSVG').hide();
    }

}

function checkModuleList_onmouseonmouseout(selected) {

    if($(selected).attr("onShow") == "false") {
        $(selected).find('.addModuleToCheckListSVG').hide();
    }
    else {
        //$(selected).find('.addModuleToCheckListSVG').hide();
    }

}

function insert(selected) {
    if($(selected).attr("onShow") == "false") {
        //$('.checkModuleHoverDiffClass').click().removeClass('checkModuleHoverDiffClass');
        $(selected).attr("onShow", "true");
        $(selected).parent().addClass('checkModuleHoverDiffClass');
        $(selected).find('.addModuleToCheckListSVG').html(`<svg class="addModuleToCheckListSVGSvg" xmlns="http://www.w3.org/2000/svg" height="17px" viewBox="0 0 24 24" width="24px" fill="#9f1212"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M7 19h10c.55 0 1 .45 1 1s-.45 1-1 1H7c-.55 0-1-.45-1-1s.45-1 1-1z"/></svg>`).css({'right': '3px', 'top': '-1px'}).show();
        if(!checkModules.length || checkModules.indexOf($(selected).attr("value")) == -1)
        checkModules.push($(selected).attr("value"));
        if(!checkModulesNames.length || checkModulesNames.indexOf($(selected).attr("text")) == -1)
        checkModulesNames.push($(selected).attr("text"));
        let setVal = {"modules":checkModules,"createModule": createModule};
        updateOrgVariables(extensionCredential, setVal);
        if(checkModulesNames.length)
        $('.outerOfSettingDetailsBSup1BSelected').html(checkModulesNames.toString().replaceAll(',', ', '));
        else
        $('.outerOfSettingDetailsBSup1BSelected').html(`<span class="emptyouterOfSettingDetailsBSelected">No Modules</span>`);  
    }
    else {
        $(selected).attr("onShow", "false");
        $(selected).parent().removeClass('checkModuleHoverDiffClass');
        $(selected).find('.addModuleToCheckListSVG').html(`<svg class="addModuleToCheckListSVGSvg" xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="18px" fill="#45596b" style="fill: #45596b;"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M18 13h-5v5c0 .55-.45 1-1 1s-1-.45-1-1v-5H6c-.55 0-1-.45-1-1s.45-1 1-1h5V6c0-.55.45-1 1-1s1 .45 1 1v5h5c.55 0 1 .45 1 1s-.45 1-1 1z"></path></svg>`).css({'right': '8px', 'top': '5px'});
        checkModules = checkModules.filter(item => item !== $(selected).attr("value"));
        checkModulesNames = checkModulesNames.filter(item => item !== $(selected).attr("text"));
        let setVal = {"modules":checkModules,"createModule": createModule};
        updateOrgVariables(extensionCredential, setVal);
        if(checkModulesNames.length)
        $('.outerOfSettingDetailsBSup1BSelected').html(checkModulesNames.toString().replaceAll(',', ', '));
        else
        $('.outerOfSettingDetailsBSup1BSelected').html(`<span class="emptyouterOfSettingDetailsBSelected">No Modules</span>`); 
    }
}

async function modulesListLoadCreate() {
    await ZOHO.CRM.META.getModules().then(function(data){
        data.modules.forEach(function(module){
            addListItemCreate("modulesListIncomingCreate",module.module_name,"dropdown-item",module.api_name);
        });
    });
}

function addListItemCreate(id,text,className,value){

    let linode = `<li class="${className}" id="modulesListIncomingCreateSup_${value}"><button class="dropdown_Butt modulesListIncomingCreateSup" onmouseover="checkModuleListCreate_onmouseover(this)" onmouseout="checkModuleListCreate_onmouseonmouseout(this)" onShow="false" onclick="insertCreate(this)" value="${value}" text="${text}">${text}<span class="addModuleToCheckListSVGCreate" style="display: none;"><svg xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="18px" fill="#45596b" style="fill: #45596b;"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M18 13h-5v5c0 .55-.45 1-1 1s-1-.45-1-1v-5H6c-.55 0-1-.45-1-1s.45-1 1-1h5V6c0-.55.45-1 1-1s1 .45 1 1v5h5c.55 0 1 .45 1 1s-.45 1-1 1z"></path></svg></span></button></li>`;
    $('#'+id).append(linode);

}

function checkModuleListCreate_onmouseover(selected) {

    if($(selected).attr("onShow") == "false") {
        $(selected).find('.addModuleToCheckListSVGCreate').show();
    }
    else {
        //$(selected).find('.addModuleToCheckListSVG').hide();
    }

}

function checkModuleListCreate_onmouseonmouseout(selected) {

    if($(selected).attr("onShow") == "false") {
        $(selected).find('.addModuleToCheckListSVGCreate').hide();
    }
    else {
        //$(selected).find('.addModuleToCheckListSVG').hide();
    }

}

function insertCreate(selected) {
    if($(selected).attr("onShow") == "false") {
        $('.checkModuleCreateHoverDiffClass').removeClass('checkModuleCreateHoverDiffClass');
        $('.modulesListIncomingCreateSup').attr("onShow", "false");
        $('.modulesListIncomingCreateSup').find('.addModuleToCheckListSVGCreate').html(`<svg class="addModuleToCheckListSVGCreateSvg" xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="18px" fill="#45596b" style="fill: #45596b;"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M18 13h-5v5c0 .55-.45 1-1 1s-1-.45-1-1v-5H6c-.55 0-1-.45-1-1s.45-1 1-1h5V6c0-.55.45-1 1-1s1 .45 1 1v5h5c.55 0 1 .45 1 1s-.45 1-1 1z"></path></svg>`).css({'right': '8px', 'top': '5px'}).hide();
        
        $(selected).attr("onShow", "true");
        $(selected).parent().addClass('checkModuleCreateHoverDiffClass');
        $(selected).find('.addModuleToCheckListSVGCreate').html(`<svg class="addModuleToCheckListSVGCreateSvg" xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="18px" fill="#1c6b1c"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M17.3 6.3c-.39-.39-1.02-.39-1.41 0l-5.64 5.64 1.41 1.41L17.3 7.7c.38-.38.38-1.02 0-1.4zm4.24-.01l-9.88 9.88-3.48-3.47c-.39-.39-1.02-.39-1.41 0-.39.39-.39 1.02 0 1.41l4.18 4.18c.39.39 1.02.39 1.41 0L22.95 7.71c.39-.39.39-1.02 0-1.41h-.01c-.38-.4-1.01-.4-1.4-.01zM1.12 14.12L5.3 18.3c.39.39 1.02.39 1.41 0l.7-.7-4.88-4.9c-.39-.39-1.02-.39-1.41 0-.39.39-.39 1.03 0 1.42z"/></svg>`).css({'right': '8px', 'top': '5px'}).show();
        createModule = $(selected).attr("value");
        let setVal = {"modules":checkModules,"createModule": createModule};
        updateOrgVariables(extensionCredential, setVal);
        if(createModule)
        $('.outerOfSettingDetailsBSup2BSelected').html($(selected).attr("text"));
        else
        $('.outerOfSettingDetailsBSup2BSelected').html(`<span class="emptyouterOfSettingDetailsBSelected">No Module</span>`); 
    }
    else {
        $(selected).attr("onShow", "false");
        $(selected).parent().removeClass('checkModuleCreateHoverDiffClass');
        $(selected).find('.addModuleToCheckListSVGCreate').html(`<svg class="addModuleToCheckListSVGCreateSvg" xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 24 24" width="18px" fill="#45596b" style="fill: #45596b;"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M18 13h-5v5c0 .55-.45 1-1 1s-1-.45-1-1v-5H6c-.55 0-1-.45-1-1s.45-1 1-1h5V6c0-.55.45-1 1-1s1 .45 1 1v5h5c.55 0 1 .45 1 1s-.45 1-1 1z"></path></svg>`).css({'right': '8px', 'top': '5px'});
        createModule = "";
        let setVal = {"modules":checkModules,"createModule": createModule};
        updateOrgVariables(extensionCredential, setVal);
        if(createModule)
        $('.outerOfSettingDetailsBSup2BSelected').html($(selected).attr("text"));
        else
        $('.outerOfSettingDetailsBSup2BSelected').html(`<span class="emptyouterOfSettingDetailsBSelected">No Module</span>`); 
    }
}


function mainBodyClickFunction(e) {

    if($(e.target).hasClass('addModuleToCheckListSVG') || $(e.target).hasClass('addModuleToCheckListSVGSvg') || $(e.target).hasClass('dropdown_Butt') || $(e.target).hasClass('dropdown-item') || $(e.target).hasClass('dropdown-menu') || $(e.target).hasClass('dropListButt') || $(e.target).hasClass('choose') || $(e.target).hasClass('arrowIcon')){

    }
    else{
        $('.chooseModule').removeAttr("open");
    }

    if($(e.target).hasClass('addModuleToCheckListSVGCreate') || $(e.target).hasClass('addModuleToCheckListSVGCreateSvg') || $(e.target).hasClass('dropdown_Butt') || $(e.target).hasClass('dropdown-item') || $(e.target).hasClass('dropdown-menu') || $(e.target).hasClass('dropListButtCreate') || $(e.target).hasClass('createChoose') || $(e.target).hasClass('dp_arrowIcon')){

    }
    else{
        $('.createModuleDrop').removeAttr("open");
    }

}