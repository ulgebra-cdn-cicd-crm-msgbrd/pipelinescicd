const functions = require('firebase-functions');
const admin = require('firebase-admin');
const request = require('request-promise');
const crypto = require("crypto");
const qs = require('querystring');
var http = require('http');
admin.initializeApp();

let runTimeOpts = {vpcConnector:'karixconnector',vpcConnectorEgressSettings :'ALL_TRAFFIC'};
exports.oauthCallbackHubspot = functions.https.onRequest(async(req, res) => {
	var db = admin.firestore();
	console.log("query");
	console.log(req.query);
	if(req.method === 'GET' && req.query.code && req.query.state){
		console.log("in");
		const state = req.query.state;
		const userId = state.substring(state.indexOf('-')+1);
		const extension =state.substring(0,state.indexOf('-'));
		var extensionName = extension.substring(0,extension.indexOf("forhubspotcrm"))
		var authCodeProof = {"client_id":"5fbdbb64-ccf2-49c7-905a-d0eb7b26cbcd","client_secret":"6897a16f-f3fd-41ad-9563-2c2b8cca290e"}
        authCodeProof["grant_type"]='authorization_code';
        authCodeProof["redirect_uri"]='https://us-central1-hubspot-integration-7aacd.cloudfunctions.net/oauthCallbackHubspot';
        authCodeProof["code"]=req.query.code;
        console.log(authCodeProof);
	    var tokens ='';
	    try {
			const responseBody = await request.post('https://api.hubapi.com/oauth/v1/token', {
			  form: authCodeProof
			});

			tokens = JSON.parse(responseBody);

			console.log('       > Received an access token and refresh token');
			console.log(responseBody);
			console.log("testign");
		}
		catch (e) {
			console.error(`       > Error exchanging ${authCodeProof.grant_type} for access token`);
			console.log(e);
			return res.redirect('https://hubspot-karix.web.app/hubspot/'+extensionName+'/settings.html');
		}

		console.log("testign2");
		console.log(tokens.access_token);
		if(tokens.access_token){
			console.log("accessToken");
            var portalId="";
			try{
				var optionsMe = {
					headers: {  Authorization: `Bearer ${tokens.access_token}`,'Content-Type': 'application/json' }
				};
				var accountDetails = await request.get('https://api.hubapi.com/integrations/v1/me', optionsMe);
				accountDetails = JSON.parse(accountDetails);
				portalId = accountDetails.portalId+"";
				await db.collection("Users").doc(userId).set({hptId:accountDetails.portalId+"",timezone:accountDetails.timeZone},{ merge: true });
			}
			catch(e){
				console.log('error on getting accountDetails');
				console.log(e);
				return res.redirect('https://hubspot-karix.web.app/hubspot/'+extensionName+'/settings.html?error='+e);
			}

			var expireTime = new Date().getTime()+(tokens.expires_in*1000)-1000;
            await db.collection("hubspotOrgs").doc(portalId).collection('extensions').doc(extension).collection("tokens").doc("hubspot")
            .set({access: tokens.access_token,refresh:tokens.refresh_token ,expt:expireTime},{ merge: true });
            await db.collection("hubspotOrgs").doc(portalId).collection('extensions').doc(extension).set({authorized_userId:userId,isHubAuth:true,ct:new Date().getTime()},{ merge: true });

            return res.redirect('https://hubspot-karix.web.app/hubspot/'+extensionName+'/settings.html');
        }
		else{
			console.log("accessToken not found");
			console.log(responseBody);
			return res.redirect('https://hubspot-karix.web.app/hubspot/'+extensionName+'/settings.html');
		}
	}
	else{
		return res.status(403).send('Forbidden!');
	}
});

async function getLatestHubspotAccessToken(userId,extension,portalId) {
	var db = admin.firestore();
	console.log(userId);
	console.log(extension);
	var extensionName = extension.substring(0,extension.indexOf("forhubspotcrm"));
	if(userId){
		console.log("getPortalId");
	    portalId =  await db.collection("Users").doc(userId).get()
	    .then(function(docRef){
	        if(docRef.exists && docRef.data() && docRef.data().hptId){
	             return docRef.data().hptId;
	        }
	        return null;
	    });
	}
	console.log("portalId");
    console.log(portalId);
    if(!portalId){
        console.log("portalId not found");
        return null;
    }
    console.log("test="+extension);
    var extensionSettings = await db.collection("hubspotOrgs").doc(portalId+"").collection("extensions").doc(extension).collection("tokens").get()
    .then(querySnapshot => {
        var data ={};
        querySnapshot.forEach(doc => {
            // console.log(doc.id, " => ", doc.data());
            data[doc.id]=doc.data();
        });
        return data;
    });
    console.log("extensionSettings11");
	console.log(JSON.stringify(extensionSettings));

	if(extensionSettings && extensionSettings.hubspot && extensionSettings.hubspot.access && extensionSettings.hubspot.refresh){
		if(extensionSettings.hubspot.expt && new Date().getTime() < new Date(extensionSettings.hubspot.expt).getTime()){
			return extensionSettings.hubspot.access;
		}
		else{
            var refreshTokenProof = {"client_id":"5fbdbb64-ccf2-49c7-905a-d0eb7b26cbcd","client_secret":"6897a16f-f3fd-41ad-9563-2c2b8cca290e"}
	        refreshTokenProof["grant_type"]='refresh_token';
	        refreshTokenProof["redirect_uri"]='https://us-central1-hubspot-integration-7aacd.cloudfunctions.net/oauthCallbackHubspot';
	        refreshTokenProof["refresh_token"]=extensionSettings.hubspot.refresh;
		    var tokens ='';

		    console.log("extensionSettings.hubspot.refresh");
		    console.log(extensionSettings.hubspot.refresh)
		    try {
				const responseBody = await request.post('https://api.hubapi.com/oauth/v1/token', {
				  form: refreshTokenProof
				});
				tokens = JSON.parse(responseBody);
				var expireTime = new Date().getTime()+(tokens.expires_in*1000)-1000;
				await db.collection("hubspotOrgs").doc(portalId+"").collection('extensions').doc(extension).collection("tokens").doc("hubspot")
				.set({access:tokens.access_token,refresh:tokens.refresh_token ,expt:expireTime},{ merge: true });
				return tokens.access_token;
			}
			catch(e){
				console.error(`       > Error exchanging refresh_token for access token`);
				console.log(e);
				return null;
			}
		}
	}
	else{
		console.log("hubspot not authorized");
		console.log(JSON.stringify(extensionSettings));
		return null;
	}
}
exports.makeHubspotHTTPCall = functions.https.onCall(async(data, context) => {
    try {
        console.log(JSON.stringify(data));
        data = JSON.parse(data);
        console.log(context.auth.uid);
        const uid = context.auth.uid;
        var extension = data.extension;
        console.log(extension);
        var accessToken = await getLatestHubspotAccessToken(uid,extension);
        console.log(accessToken);
        if(accessToken){
            var options = {
                headers: {Authorization: `Bearer ${accessToken}`, 'Content-Type': 'application/json'},
                body: data.data,
                method: data.method,
                url: data.url,
                json: true
            };
            console.log("options");
            console.log(JSON.stringify(options));
            const responseBody = await request(options);
            return responseBody;
        }
        else{
            return "accesstoken_not_found";
        }
    }
    catch (exe) {
        console.log("error");
        console.log(exe);
        console.log(JSON.stringify(exe));
        return "error";
    }
});

exports.unInstallApp = functions.https.onCall(async(data, context) => {
	console.log("data");
	console.log(data);
    var extension = data.extensionName;
    const userId = context.auth.uid;
    let db = admin.firestore();
    var portalId = await db.collection("Users").doc(userId).get().then(function(doc){
        if(doc.exists && doc.data() && doc.data().hptId){
            return doc.data().hptId;
        }
        return null;
    });
    if(!portalId){
        console.log("portalId_not_found");
        return "portalId_not_found";
    }
    var accountIds = [];
    var extensionSettings = await db.collection("hubspotOrgs").doc(portalId).collection("extensions").doc(extension).collection("tokens").get().then(querySnapshot => {
        var data ={};
        querySnapshot.forEach(doc => {
            accountIds.push(doc.id);
            console.log(doc.id, " => ", doc.data());
            data[doc.id]=doc.data();
        });
        return data;
    });
    console.log(extensionSettings);
	var appStatus={};
	if(data && data.service && data.authKeyword && extensionSettings && extensionSettings[data.service]){
		await db.collection("hubspotOrgs").doc(portalId).collection('extensions').doc(extension).collection("tokens").doc(data.service).delete();
		extensionSettings[data.service]=null;
		appStatus[data.authKeyword]= false;
	}
    try{
        if(accountIds && accountIds.length){
            accountIds.forEach(async function(accountId){
                if(accountId !== "hubspot" && accountId !== data.service){
                    await db.collection("hubspotOrgs").doc(portalId).collection('extensions').doc(extension).collection("tokens").doc(accountId).delete();
                }
            })
            appStatus[data.authKeyword]= false;
        }
    }
    catch(e){
        console.log(e);
        console.log("delete_error");
    }
    if(extensionSettings && extensionSettings.hubspot && extensionSettings.hubspot.refresh){
        console.log("hubspot in");
        var accessTokenHubspot = extensionSettings.hubspot.refresh;
        var optionsHubspot = {
            headers: {  Authorization: `Bearer ${accessTokenHubspot}`,'Content-Type': 'application/json' },
        };
        try{
            await request.delete('https://api.hubapi.com/oauth/v1/refresh-tokens/'+extensionSettings.hubspot.refresh, optionsHubspot);
        }
        catch(e){
            console.log("error in deleting hubspot webhook")
            console.log(e);
            return "error";
        }
        await db.collection("hubspotOrgs").doc(portalId).collection('extensions').doc(extension).collection("tokens").doc("hubspot").delete();
		appStatus.isHubAuth= false;
    }
    await db.collection("hubspotOrgs").doc(portalId).collection('extensions').doc(extension).set(appStatus,{ merge: true });
    await db.collection("Users").doc(userId).set({"hptId":""},{merge:true});
    return "success";
});

async function makeHubspotcall(data){
	try {
    	console.log(data);
    	data = JSON.parse(data);
    	console.log(data.userId);
        const uid = data.userId;
        var extension = data.extension;
        console.log(extension);
        var accessToken = data.accessToken;
        if(!accessToken){
        	accessToken = await getLatestHubspotAccessToken(uid,extension,data.portalId);
        }
        console.log(accessToken);
        if(!accessToken){
        	console.log("accessToken not found");
        	return null;
        }
        var options = {
            headers: {Authorization: `Bearer ${accessToken}`, 'Content-Type': 'application/json'},
            body: data.data,
            method: data.method,
            url: data.url,
            json: true
        };
        console.log("options");
        console.log(JSON.stringify(options));
        const responseBody = await request(options);
        return responseBody;
    }
    catch (exe) {
    	console.log("error in makeHubspotcall");
        console.log(JSON.stringify(exe));
        return "error";
    }
}
function addContact(data,userId,extension,accessToken){
	try{
	    var body={
	        "properties":{
	            "company":data.Company,
	            "email": data.Email,
	            "firstname": data.First_Name,
	            "lastname": data.Last_Name,
	            "phone": data.Phone
	        }
	    }
	    var apiData =JSON.stringify({
	        "url":"https://api.hubapi.com/crm/v3/objects/contacts",
	        "method": "POST",
	        "data":body,
	        "extension":extension,
	        "userId":userId,
	        "accessToken":accessToken
	    }) ;
	    return makeHubspotcall(apiData).then(function(resp){
	        console.log(JSON.stringify(resp));
	        return resp;
	    })
	}
	catch(e){
		console.log("error in addContact");
		console.log(e);
	}
}

exports.hubspotcard = functions.https.onRequest(async(req, res) => {
	console.log(JSON.stringify(req.query));
	console.log(req.url);
    var appName = "twilio";
    if(req.query && req.query.app){
    	appName = req.query.app;
    }
    var extensionName = appName+"forhubspotcrm";
	var url = "https://us-central1-hubspot-integration-7aacd.cloudfunctions.net/hubspotcard"+req.url.substring(1);
	var app
	if(!validateHubspot(req.headers["x-hubspot-signature-version"],req.headers["x-hubspot-signature"],req.method,url,"",extensionName)){
		console.log("invalid_hash");
		return res.status(404).send({
			"status": "success"
		});
	}
	if(req.method === 'GET'){
        let db = admin.firestore();
        var authKeyword = "is"+appName.substring(0,1).toUpperCase()+appName.substring(1)+"Auth";
        console.log(req.query.portalId);
        console.log(extensionName);
        var portalId = req.query.portalId+"";
    	// var extensionDetails = await db.collection("hubspotOrgs").doc(portalId).collection("extensions").doc(extensionName).get().then(doc =>{
     //        //console.log(' got extension details...'+doc.data());
     //        return doc.data();
     //    });
        var cardDetail = {
           "results":[],
           "settingsAction": {
              "type": "IFRAME",
              "width": 890,
              "height": 800,
              "uri": "https://hubspot-karix.web.app/hubspot/"+appName+"/settings.html?contact="+encodeURIComponent(JSON.stringify(req.query)),
              "label": "Settings"
            },
            "primaryAction": {
              "type": "IFRAME",
              "width": 890,
              "height":800,
              "uri": "https://hubspot-karix.web.app/hubspot/"+appName+"/chat.html?contact="+encodeURIComponent(JSON.stringify(req.query)),
              "label": "Send "+appName.substring(0,1).toUpperCase()+appName.substring(1)+" Message"
            }
        }
        console.log(encodeURIComponent(req.query));
        return res.status(200).send(cardDetail);
	}
	else{
        return res.status(403).send('Forbidden!');
	}
});

function uawc_getCountryCode(number) {
    var country_code = [1, 1242, 1246, 1264, 1268, 1284, 1340, 1345, 1441, 1473, 1649, 1664, 1758, 1787, 1809, 1868, 1869, 1876, 2, 20, 212, 213, 216, 218, 220, 221, 222, 223, 224, 226, 227, 228, 229, 231, 232, 233, 234, 236, 237, 238, 239, 240, 241, 242, 244, 245, 248, 249, 250, 251, 252, 253, 254, 256, 257, 258, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 269, 27, 290, 291, 297, 298, 299, 30, 31, 32, 33, 34, 350, 351, 352, 353, 354, 356, 357, 358, 359, 36, 370, 371, 372, 373, 374, 375, 376, 377, 378, 379, 380, 381, 385, 386, 387, 389, 39, 40, 41, 417, 42, 421, 43, 44, 45, 46, 47, 48, 49, 500, 501, 502, 503, 504, 505, 506, 507, 509, 51, 52, 53, 54, 55, 56, 57, 58, 590, 591, 592, 593, 594, 595, 596, 597, 598, 60, 61, 62, 63, 64, 65, 66, 670, 671, 672, 673, 674, 675, 676, 677, 678, 679, 680, 681, 682, 683, 686, 687, 688, 689, 691, 692, 7, 7, 7, 7, 7, 7880, 81, 82, 84, 850, 852, 853, 855, 856, 86, 880, 886, 90, 90392, 91, 94, 95, 960, 961, 962, 963, 964, 965, 966, 967, 968, 969, 971, 972, 973, 974, 975, 976, 977, 98, 993, 994, 996];
    if (number.toString().length > 10)
    {
        num = number.toString();
        var num_len = num.split(num.slice(-10))[0];
        for (let c of country_code)
        {
            if (c + "" === num_len + "")
            {
                return c;
            }
        }
    }
    return null;
}
exports.handleActions = functions.https.onCall(async(data, context) => {
	try{
		const userId = context.auth.uid;
	    let db = admin.firestore();
	    var action = data.action;
	    var portalId = null;
    	portalId = await db.collection("Users").doc(userId).get().then(function(docRef){
            if(docRef.exists && docRef.data().hptId){
                return docRef.data().hptId;
            }
            return null;
        });
    	console.log(portalId);
        if(!portalId){
        	return "portalId_not_found";
        }
        if(action === "connectApp"){
	    	var credentials = data.credentials?data.credentials:{};
	    	credentials.t = new Date().getTime();
            if(data.newData){
    	        try{
                    return db.collection("hubspotOrgs").doc(portalId).collection('extensions').doc(data.extensionName).collection("tokens").add(credentials).then(async function(docRef){
                        var settings = {};
                        settings[docRef.id+""]= data.settings;
                        settings.isKarixAuth = data.isKarixAuth;
                        await db.collection("hubspotOrgs").doc(portalId).collection('extensions').doc(data.extensionName).set(settings, {"merge":true});
                        return "success";
                    });
                }
                catch(e){
                    console.log(e);
                    return "error";
                }
            }
            else{
                await db.collection("hubspotOrgs").doc(portalId).collection('extensions').doc(data.extensionName).collection("tokens").doc(data.appName).set(credentials);
                await db.collection("hubspotOrgs").doc(portalId).collection('extensions').doc(data.extensionName).set(data.settings, {"merge":true});
                return "success";
            }    
	    }
	    else if(action === "saveSettings"){
	    	await db.collection("hubspotOrgs").doc(portalId).collection('extensions').doc(data.extensionName).set(data.settings, {"merge":true});
	   		return "success";
	    }
        else if(action === "removeAccount"){
            var updateMap = {[data.accountId]:admin.firestore.FieldValue.delete()};
            if(data.accountId === "karix"){
                updateMap['domain'] = admin.firestore.FieldValue.delete();
                updateMap['senderid'] = admin.firestore.FieldValue.delete();
            }
            if(data.accountNo === 1){
                updateMap["isKarixAuth"] = false;
            }
            await db.collection("hubspotOrgs").doc(portalId).collection('extensions').doc(data.extensionName).update(updateMap);
            await db.collection("hubspotOrgs").doc(portalId).collection('extensions').doc(data.extensionName).collection("tokens").doc(data.accountId).delete();
            return "success";
        }
	}
	catch(e){
		console.log(e);
		return "error";
	}
});
exports.makeAppCall = functions.runWith(runTimeOpts).https.onCall(async(data, context) => {
	try {
        data = JSON.parse(data);
        try{
            if(data.action === "testip"){
                var ipCall = {
                    "url":`https://plugin-logs.zohosandbox.com/crm/v2/functions/logs__pluginhandler/actions/execute?auth_type=apikey&zapikey=1001.7a90efe1ea0445bc84ca7d4a9df54700.78606eb968a8fbaf2410f4bd262a09cc&from=karixappcall`,
                    "method": 'GET'
                };
                console.log("ipCall");
                console.log(JSON.stringify(ipCall));
                var resp12 = await request(ipCall);
                console.log(JSON.stringify(resp12));
            }
        }
        catch(e){
            console.log(e);
        }
        // console.log(data);
        const userId =context.auth.uid;
        var extension = data.extension;
        // console.log(extension);
        var db = admin.firestore();
        var portalId = data.portalId;
        if(userId){
        	console.log(userId);
	        portalId = await db.collection("Users").doc(userId+"").get().then(function(doc){
		        if(doc.exists && doc.data() && doc.data().hptId){
		            return doc.data().hptId;
		        }
		        return null;
		    });
		}
	    if(!portalId){
	        console.log("portalId_not_found");
	        return {"error":"portalId_not_found","status":500};
	    }
        var credentials={};
    	credentials = await db.collection("hubspotOrgs").doc(portalId+"").collection("extensions").doc(extension).collection("tokens").doc(data.appName).get().then(function(doc){
    		if(doc.exists && doc.data()){
    			return doc.data();
    		}
    		return null;
    	});
        console.log(credentials);
        if(!credentials){
        	console.log("credentials_not_found");
        	return {"error":"credentials_not_found","status":500};
        }
        let headers = data.headers ? data.headers: '{}';
        let url = data.url;
        Object.keys(credentials).forEach(function(key){
        	console.log(key);
        	var replace = "\\${"+key+"}";
            var re = new RegExp(replace,"g");
        	headers = headers.replace(re,credentials[key]);
        	url = url.replace(re,credentials[key]);
        });
        console.log(headers);
        console.log(url);
        if(headers){
        	try{
        		headers = JSON.parse(headers);
       		}
        	catch(e){
        		console.log(e);
        	}
        	console.log(headers)
        }
        if(data.authType === "basic_auth" && headers.Authorization){
        	headers.Authorization ="Basic "+Buffer.from(headers.Authorization).toString('base64');
        }
        try{
        	var options = {
                'method': data.method, //you can set what request you want to be
                'url': url,
                'body': data.data,
                'headers': headers,
                'json': true
            };
            console.log("options");
            console.log(JSON.stringify(options));
            const responseBody = await request(options);
            try{
	            if(responseBody){
	            	 console.log(JSON.stringify(responseBody));
	            }
        	}
        	catch(e){
        		console.log(e);
        	}
            return {"data" : responseBody,"status" : 200};
        }
        catch(error){
            console.log(error);
            return {"error":"server internal error","status":500};
        }
    }
    catch (exe) {
        console.log("error");
        console.log(exe);
        console.log(JSON.stringify(exe));
        return {"error":"server internal error","code":500};
    }
});
exports.hubspotWebhookAction = functions.runWith(runTimeOpts).https.onRequest(async(req, res) => {
	try{
        try{
            if(req.query.action === "testip"){
                var ipCall = {
                    "url":`https://plugin-logs.zohosandbox.com/crm/v2/functions/logs__pluginhandler/actions/execute?auth_type=apikey&zapikey=1001.7a90efe1ea0445bc84ca7d4a9df54700.78606eb968a8fbaf2410f4bd262a09cc&from=karix`,
                    "method": 'GET'
                };
                console.log("ipCall");
                console.log(JSON.stringify(ipCall));
                var resp12 = await request(ipCall);
                console.log(JSON.stringify(resp12));
            }
        }
        catch(e){
            console.log(e);
        }
		console.log(JSON.stringify(req.body));
		console.log(JSON.stringify(req.query));
		var url = "https://us-central1-hubspot-integration-7aacd.cloudfunctions.net/hubspotWebhookAction?app="+req.query.app;
		if(!validateHubspot(req.headers["x-hubspot-signature-version"],req.headers["x-hubspot-signature"],req.method,url,JSON.stringify(req.body),req.query.app)){
			console.log("invalid_hash");
			return res.status(404).send({
				"status": "success"
			});
		}
		var extensionName = req.query.app;
		var contactId = req.body.object.objectId;
		var portalId = req.body.origin.portalId;
	    if(req.body.origin && req.body.inputFields){
	        req.body = req.body.inputFields;
	        if(req.body.contactNumber && req.body.contactNumber.indexOf(",") !== -1){
	            var phones = req.body.contactNumber.split(",");
	            for(let i=0;i<phones.length;i++){
	                if(phones[i]){
	                    req.body.contactNumber = phones[i];
	                    break;
	                }
	            }
	        }
	    }
        if(!req.body.apikey || !req.body.senderid || !req.body.domain){
            return res.status(200).send({
                "error": "credentials_not_found"
            });
        }
	    let db = admin.firestore();
	    var senderId = req.body.senderid;
	    var toPhone = req.body.contactNumber;
	    if(toPhone){
		    toPhone = toPhone.replace(/\D/g, '');
	        var countryCode = uawc_getCountryCode(toPhone);
	        if (!countryCode && req.body.countryCode) {
	            toPhone = req.body.countryCode+""+toPhone;
	        }
	    }
	    var striptags = require('striptags');
	    var htmlentity = require('html-entities');
	    var msgContent = htmlentity.decode(striptags(req.body.text));
	    var eventTemplateId="1065431";
	    var history = null;
	    var status ="";
	    var messageId = "";
        var karixApiData = {
            "url":`https://${req.body.domain}//httpapi/QueryStringReceiver?ver=1.0&key=${req.body.apikey}&dest=${toPhone}&send=${req.body.senderid}&text=${encodeURIComponent(msgContent)}&type=PM`,
            "method": 'GET'
        };
    	console.log("karixApiData");
        console.log(JSON.stringify(karixApiData));
    	var resp = await request(karixApiData);
        console.log(JSON.stringify(resp));
        status = "failed";
        if(resp && resp.includes('Statuscode=200') && resp.indexOf('Request ID=N/A') === -1) {
            messageId= resp.substring(resp.indexOf("Request ID=")).split(" &")[0].split("=")[1];
            status = "sent";
        }
        history = {"platform":"SMS","messageId":messageId,"direction":"sent","sender_phone":req.body.senderid,"receiver_phone":"+"+toPhone.replace(/\D/g, ''),"status":status,"message":msgContent};
	    if(toPhone && toPhone.toString().length >15){
	    	history.status = "failed";
	    }
	    if(eventTemplateId && history){
	    	var body={
		        "eventTemplateId":eventTemplateId,
		        "objectId":contactId,
		        "tokens": history
		    }
		    var apiData2 =JSON.stringify({
		        "url":"https://api.hubapi.com/crm/v3/timeline/events",
		        "method": "POST",
		        "data":body,
		        "extension":extensionName,
		        "portalId":portalId
		    });
		    var hresp = await makeHubspotcall(apiData2).then(function(hresp){
		        console.log(JSON.stringify(hresp));
		        return hresp;
		    })
		}
		return res.status(200).send({
			"status": "success"
		});
	}
	catch(exc){
		console.log("error in hubspotWebhookAction");
		console.log(exc);
        console.log(JSON.stringify(exc));
		return res.status(200).send({
			"status": "error"
		});
	}
});
function validateHubspot(version,hash,method,url,body,extensionName){
	console.log(hash);
    var client_secretMap ={
	"karixforhubspotcrm":"6897a16f-f3fd-41ad-9563-2c2b8cca290e"};
    if(client_secretMap[extensionName]){
    	var source_string = client_secretMap[extensionName];
    	if(version === "v1"){
	      	source_string = source_string+body;
	    }
	    else if(version === "v2"){
	    	source_string = source_string+method+url+body;
	    }
	    console.log(source_string);
	    var source_hash = crypto.createHash('sha256').update(source_string).digest('hex');
      	console.log("source_hash");
      	console.log(source_hash);
      	if(source_hash === hash){
      		return true;
      	}
    }
    return false;
}
exports.karixCallback = functions.https.onRequest(async (req, res) => {
    try{
    	//{"api_version":"2.0","type":"message","data":{"delivered_time":"2021-06-07T16:56:26.575043Z","created_time":"2021-06-07T16:56:24.481516Z","status":"delivered","api_version":"2.0","request_uid":"e63cc379-5c83-4e1c-a47d-e170eb2eb431","content_type":"text","refund":null,"content":{"text":"test"},"total_cost":"0.004","channel_details":{"whatsapp":{"whatsapp_fee":"0","source_profile":{"name":""},"type":"conversation","platform_fee":"0.004"}},"uid":"0d307f62-016c-4051-8031-382ad24093ec","redact":false,"sent_time":"2021-06-07T16:56:25.009985Z","updated_time":"2021-06-07T16:56:26.575043Z","direction":"outbound","country":"IN","error":null,"source":"+13253077759","channel":"whatsapp","destination":"+918012178547","account_uid":"214f7103-1b54-4a57-9af8-0b5c58efeb52"},"uid":"05d78b6c-cdf9-4bfa-ac69-5c0bb58ee35a"}
    	console.log(JSON.stringify(req.body));
    	console.log(JSON.stringify(req.headers));
    	console.log(JSON.stringify(req.query));
    	if(req.body.type !== "message"){
    		return res.status(200).send('ok');
    	}
    	var extension = "karixforhubspotcrm";
    	var userId = req.query.userId;
    	var db = admin.firestore();
	    var contact = req.body.contact;
	    var message = req.body.data;
	    var extId = "";
    	if(message.direction === "outbound"){
    		extId = message.source+"::"+message.destination;
    		admin.database().ref("/kas/"+extId).update({
                ts: message.uid,
                st : message.status,
                et: "status"
            });
        	console.log('status update');
    	}
    	else if(message.direction === "inbound"){
        	console.log('new incoming message');
    		extId = message.destination+"::"+message.source;
    		admin.database().ref("/kar/"+extId).update({
                ts: message.uid,
                st : message.status,
                et: "new"
            });
        	var type = message.content_type;
			var content = message.content.text;
			var attachmentURLs = [];
			if(type !== "text") {
				if(type === "location") {
					let locationJSON = message.content.location;
					content += "http://maps.google.com/?ll="+locationJSON.latitude+","+locationJSON.longitude;
				}
				else {
					content=message.content[message.content_type];
				    if(content.url){
				        content = (content.caption?content.caption+", ":"")+"Url : "+content.url ;
				    }

				}
			}

    		var history ={"platform":message.channel,"messageId":message.uid,"direction":message.direction,"sender_phone":message.source,"receiver_phone":message.destination,"status":message.status,"message":content};
        	var accessToken = await getLatestHubspotAccessToken(userId,extension);
        	if(!accessToken){
        		console.log("accessToken_not_found");
        		return res.status(200).send('ok');
        	}
        	var contactId=null;
    		var from =message.source;
			var apiData =JSON.stringify({
		        "url":"https://api.hubapi.com/contacts/v1/search/query?q="+from,
		        "method": "GET",
		        "extension":extension,
		        "userId":userId,
		        "accessToken":accessToken
		    }) ;
		    var searchresp = await makeHubspotcall(apiData).then(function(searchresp){
		    	console.log("search resp");
		        console.log(JSON.stringify(searchresp));
		        return searchresp;
		    })
		    if(searchresp && searchresp.contacts.length){
		        contactId = searchresp.contacts[0].vid;
		        console.log("contactIdsearch"+contactId);
		    }
    		console.log("contactId");
    		console.log(contactId);
		    if(!contactId){
		    	var inputMap = {"Phone":from};
		    	if(message.channel_details && message.channel_details[message.channel] && message.channel_details[message.channel]["source_profile"] && message.channel_details[message.channel]["source_profile"].name){
			    	var fullname = message.channel_details[message.channel]["source_profile"].name;
		        	inputMap["Last_Name"]=fullname;
				}
				else{
					inputMap["Last_Name"]=from;
				}

		        if(fullname && fullname.split(" ")[1]){
		            inputMap["First_Name"]=fullname.split(" ")[0];
		            inputMap["Last_Name"]=fullname.substring(fullname.indexOf(" "));
		        }
		        if(contact.firstName){
		        	 inputMap["First_Name"] = contact.firstName;
		        }
		        if(contact.lastName){
		        	 inputMap["Last_Name"] = contact.lastName;
		        }
		        var resp = await addContact(inputMap,userId,extension,accessToken);
		        contactId = resp.id;
		        console.log("contactIdadd"+contactId);
		    }
		    var body={
		        "eventTemplateId":"1056546",
		        "objectId":contactId,
		        "tokens": history
		    }
		    var apiData2 =JSON.stringify({
		        "url":"https://api.hubapi.com/crm/v3/timeline/events",
		        "method": "POST",
		        "data":body,
		        "extension":extension,
		        "userId":userId,
		        "accessToken":accessToken
		    }) ;
		    var historyResp = await makeHubspotcall(apiData2).then(function(resp){
		        return resp;
		    })
	        console.log(" karix message has been added in hubspot CRM ");
    	}
        return res.status(200).send('ok');
    }
    catch (e) {
    	console.log("error");
        console.log(e);
        return res.status(200).send('ok');
    }
});