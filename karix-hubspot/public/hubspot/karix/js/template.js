var extensionName= "karixforhubspotcrm";
var firebaseApp;
var db;
var contact;
var fields;
var countryCode; 
var sel;
var currentRecord;
var fromPhoneNumber = "";
var templates;
var currentTemplateId =null;

var contactId = null;
var domain = null;

var recordModule = "Contacts";
var userId;
var portalId;
async function handleauthoriztion(){
    $("#title").text("Karix Template Manager");
    if(firebase.auth().currentUser){
        userId = firebase.auth().currentUser.uid;
        portalId = await db.collection("Users").doc(userId).get().then(function(doc) {
            if(doc.exists && doc.data().hptId){
                return doc.data().hptId;
            }
            return null;
        });
        if(!portalId){
            showErroMessage("Please Authorize the HubSpot and Karix in <a  href='https://app.azurna.com/hubspot/karix/settings' target='_blank'><button class='ext'>Extension Settings</button></a>");
            return;
        }
        db.collection("hubspotOrgs").doc(portalId).collection("extensions").doc(extensionName).get().then(async function(doc){
          if(doc.exists && doc.data().isHubAuth && doc.data().isKarixAuth){
            senderid = doc.data().senderid;
            initialize();
          }
          else{
            showErroMessage("Please Authorize the HubSpot and Karix in <a  href='https://app.azurna.com/hubspot/karix/settings' target='_blank'><button class='ext'>Extension Settings</button></a>");
          }
        })
    }    
}  


async function initialize(){
    fields = await getContactFields();
    setTemplates();
    setFields(fields);
}

async function setTemplates(){
     $('#template').html("");
    if(firebase.auth().currentUser){
        let userId =firebase.auth().currentUser.uid;
        templates = await db.collection("hubspotSettings").doc(userId).collection('karixTemplates').get().then(querySnapshot => {
            var data ={};
            querySnapshot.forEach(doc => {
                console.log(doc.id, " => ", doc.data());
                data[doc.id]=doc.data();
            });
            return data;
        });
    }    
    var templateList="";

    if(templates){
        templateList =templateList+ '<option  value="">New Template</option>';
        var templateIds = Object.keys(templates);
        templateIds.forEach(function(templateId){
            templateList =templateList+ '<option  id="'+templateId+'" value="'+templateId+'" ></option>';
        })
        $('#template').append(templateList);
        templateIds.forEach(function(templateId){
            let template = templates[templateId];
            document.getElementById(templateId).innerText = template.n; 
        })
    }
    else{
        $('#templateList').append('<option style="text-align:center;value="">No Templates</option>');
    }
    document.getElementById("Error").style.display= "none";
    document.getElementById("mloader").style.display= "none";
    document.getElementById("container").style.display="block";
}
async function selectTemplate(editor){
    if( editor.value && templates[ editor.value]){
        currentTemplateId = editor.value;
        $("#templateSection").show();
        $("#templateId").val(currentTemplateId);
        
        document.getElementById("templateName").value = templates[editor.value].n;
        document.getElementById("emailContentEmail").innerText = templates[editor.value].m;
    }       
    else{
        currentTemplateId = null;
        $("#templateId").val("");
        $("#templateSection").hide();
        document.getElementById("templateName").value = "";
        document.getElementById("emailContentEmail").innerText = "";
    }
}  
   
function setFields(fields){
    if(fields && fields.data){
        fields.data.forEach(function(field){
            if(field.groupName == "contactinformation" && field.formField == true){
                addListItem("dropdown-menu-email",field.label,"dropdown-item","Contacts."+field.label);
            }
        }); 
    }    
}
function addListItem(id,text,className,value){
    if(className == "dropdown-item"){
        var linode = '<li class="'+className+'"><button class="'+className+'" onclick="insert(this)">'+text+'<input type="hidden" value="'+value+'"></button></li>';
    }
    else{
        var linode = '<li class="'+className+'">'+text+'</li>';
    }
    $('#'+id).append(linode);
}
function insert(bookingLink){
    // var bookingLink = this;
    var range;

    if (sel && sel.rangeCount && isDescendant(sel.focusNode)){
        range = sel.getRangeAt(0);
        range.collapse(true);
        var span = document.createElement("span");
        span.appendChild( document.createTextNode('${'+bookingLink.children[0].value+'}') );
        range.insertNode(span);
        range.setStartAfter(span);
        range.collapse(true);
        sel.removeAllRanges();
        sel.addRange(range);
    }    
}
function isDescendant(child) {
    var parent = document.getElementById("emailContentEmail");
     var node = child.parentNode;
     while (node != null) {
         if (node == parent || child == parent) {
             return true;
         }
         node = node.parentNode;
     }
     return false;
}
function openlink(){
    sel = window.getSelection();
    if (sel && sel.rangeCount) {
        range = sel.getRangeAt(0);
    }  
}     

function getContactFields(){
    var apiData =JSON.stringify({
        "url":"https://api.hubapi.com/properties/v1/contacts/properties",
        "method": "GET",
        "extension":extensionName
    }) ;
    return makeHubspotcall(apiData).then(function(resp){
        console.log(resp);
        return resp;
    })
}

function makeHubspotcall(data){
    var makeHubspotHTTPCall = firebase.functions().httpsCallable('makeHubspotHTTPCall');
    return makeHubspotHTTPCall(data).then(function(result) {
      // Read result of the Cloud Function.
    if(result && result.data == "accesstoken_not_found"){
        document.getElementById("ErrorText").innerHTML = "Please Authorize the HubSpot in <a  href='https://app.azurna.com/HubSpot/karix/settings' target='_blank'><button class='ext'>Extension Settings</button></a>";
        document.getElementById("Error").style.display= "block";
        return null;
      }
      return result;
    }).catch(function(error) {
      // Getting the Error details.
      var code = error.code;
      var message = error.message;
      var details = error.details;
      console.log(error)
      document.getElementById("ErrorText").innerHTML = "Please Authorize the HubSpot in <a  href='https://app.azurna.com/HubSpot/karix/settings' target='_blank'><button class='ext'>Extension Settings</button></a>";
      document.getElementById("Error").style.display= "block";
      return null;
      // ...
    });

}

function showTemplateOption(checker){
    if(checker.checked){
        document.getElementById("templateOption").style.display="inline-block";
    }
    else{
        document.getElementById("templateOption").style.display="none";
    }
}
var currentTemplateId;
async function saveTemplate(){
    var message = document.getElementById("emailContentEmail").innerText;
    var templateName = document.getElementById("templateName").value;
    if(!templateName || templateName.trim() == "" ){
        document.getElementById("ErrorText").innerText = "Template Name cannot be empty.";
        document.getElementById("Error").style.display= "block";
        document.getElementById("Error").style.color="red";
        setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
    }
    else if(message.length >2000){
        document.getElementById("ErrorText").innerText = "Message should be within 2000 characters.";
        document.getElementById("Error").style.display= "block";
        document.getElementById("Error").style.color="red";
        setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
    }
    else if(message.replace(/\n/g,"").replace(/\t/g,"").replace(/ /g,"") == ""){
        document.getElementById("ErrorText").innerText = "Message cannot be empty.";
        document.getElementById("Error").style.display= "block";
        setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
    }
    else{
        if(firebase.auth().currentUser){
            document.getElementById("ErrorText").innerText = "Saving... ";
            document.getElementById("Error").style.display= "block";
            let userId =firebase.auth().currentUser.uid;
            if(currentTemplateId){
                await db.collection("hubspotSettings").doc(userId).collection('karixTemplates').doc(currentTemplateId).set({m:message,n:templateName});
            }
            else{
                await db.collection("hubspotSettings").doc(userId).collection('karixTemplates').add({m:message,n:templateName});
            }
            document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Template has been saved successfully.</div>';
            setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
            window.location.reload();
        }    
    }
}

async function deleteTemplate(){
    if(firebase.auth().currentUser){
        document.getElementById("ErrorText").innerText = "deleteting... ";
        document.getElementById("Error").style.display= "block";
        let userId =firebase.auth().currentUser.uid;
        if(currentTemplateId){
            await db.collection("hubspotSettings").doc(userId).collection('karixTemplates').doc(currentTemplateId).delete();
            currentTemplateId = null;
            window.location.reload();
        }
        document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Template has been deleted successfully.</div>';
        setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
    }    
}  


var attPur = true;
function openUploader(){
    if(attPur){
        document.getElementById("inputFileAttach").click();
    }
    else{
        if(!document.getElementById("wa-attach-tip")){
            $("#attachDiv").append(`
            <span id="wa-attach-tip" style="display: block;margin: 5px;border: 1px dotted crimson;padding: 7px 15px;background-color: rgba(200,0,0,0.1);word-break: unset;border-radius: 26px;font-size: 14px;color: crimson;">You need to purchase license to access this feature. <a href="https://apps.ulgebra.com/hubspot/whatsapp-web/whatsapp-web-for-hubspot" target="_blank">More info</a><div class="" style="
                    margin-top: 5px;
                "><a href="https://forms.gle/3k6bCPSRWBpEfifL6" target="_blank" style="
                    background-color: green;
                    color: white;
                    padding: 4px 15px 5px;
                    border-radius: 25px;
                    box-shadow: 0 2px 2px rgba(0,0,0,0.2);
                ">Request Access </a>
                </div> </span>
            `);
        }    
    }
}
function fileSizeCheckWc(fileSize) {
      if(fileSize < 1000000){
         fileSize = fileSize/1024 ;
      }else{
         fileSize = fileSize/(1024*1024) ;
         if(fileSize > 5) {
            alert(`file size is ${fileSize.toFixed(2)}Mb.
            Please select file less than 5Mb.`);
            return false;
        }
      }
      return true;
}

var attachedfile={}
function attachFileChange(e) {

        var FileButton = document.getElementById("inputFileAttach");

        var file = e.files[0];
        if(!fileSizeCheckWc(file.size)) {
            return false;
        }
        var metadata = {
                          customMetadata: {
                            'file_name': file.name,
                            'file_size': file.size
                          }
                        }

        let fileTyle;
        if(file.type.includes('image/')) {
            fileTyle = 'image';
        }
        else if(e.files[0].type.includes('audio/')) {
            fileTyle = 'audio';
        }
        else if(file.type.includes('video/')) {
            fileTyle = 'video';
        }
        else if(file.type.includes('application/')) {
            if(file.type.includes('pdf')) {
                fileTyle = 'application/pdf';
            }
            else if(file.type.includes('zip')) {
                fileTyle = 'application/zip';
            }
            else if(file.type.includes('excel')) {
                fileTyle = 'application/excel';
            }
            else {
                fileTyle = 'application/msdownload';
            }
        }
        else if(file.type.includes('text/')) {
            fileTyle = 'text';
        }

        let uniqFileName = file.name+',wcUniqNameId='+Date.now();
        var task = firebase.storage().ref('whatcetra/'+currentUser.uid+'/'+fileTyle+'/'+uniqFileName).put(file, metadata);
        $('#attachedfile').append(`<div class="loadingdiv"> file is uploading...</div>`);
        task.on('state_changed',
            function progress(snapshot){
                switch (snapshot.state) {
                  case firebase.storage.TaskState.PAUSED: // or 'paused'
                    //console.log('Upload is paused');
                    break;
                  case firebase.storage.TaskState.RUNNING: // or 'running'
                    //console.log('Upload is running');
                    break;
                }
            },
            function error(err){
                console.log("file not uploaded.")
            },
            function complete(){
                document.getElementById("inputFileAttach").value = "";
                $('.loadingdiv').remove();
                let storageRef =  firebase.storage().ref('whatcetra/'+currentUser.uid+'/'+fileTyle+'/'+uniqFileName);
                storageRef.getDownloadURL().then(imgURL =>{
                    attachedfile = {"mediaUrl":imgURL,"fileMeta":file};
                    $('#attachedfile').append(`<div > ${file.name}</div>`);
                      // sendMessage('message', true, {
                      //   "url": imgURL,
                      //   "name": file.name,
                      //   "type": file.type,
                      //   "size": file.size
                      // });
                });

            }
        );

}
 
// function addContact(){
//     var data = {"fields":{"NAME":"test","PHONE":[{"VALUE":"3333333","VALUE_TYPE":"WORK"}]}};
//     var apiData ={
//         "url":"https://"+domain+"/rest/crm.lead.add",
//         "method": "POST",
//         "data":data,
//         "extension":extensionName
//     } ;
//     makeBitrix24call(apiData).then(function(resp){
//         console.log(resp);
//         return resp;
//     })
// }