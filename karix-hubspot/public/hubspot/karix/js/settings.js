 // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional

var portalId=null;
let allAuthorized = false; 

let createdAllCredentials = 0;
let deletedAllCredentials = 0;
// var UALIC_APP_PRODUCT_ID = 'prod_IsDzgkJSMnJ7H3';
var extensionName = 'karixforhubspotcrm';

function addAccount(){
    $("#twiliocreds").show();
    $('#acuitybutton').html(`Authorize Now`).attr({'disabled': false}).css({'background-color': 'royalblue'});
}

function addSenderid(){
    $("#senderids").append(`<input style="padding: 5px 10px;margin-top:5px;"  type="text" required class="senderid" placeholder="Enter Karix Sender ID"/>`)
}

async function uninstall() {
        var user = firebase.auth().currentUser;
        if (user) {
            var yesAgree = confirm("\n\n Are you sure you want to disable integration?");
            if(yesAgree){karix_accounts
                showProcess(`Removing authorizations ...`, 1);
                var user = firebase.auth().currentUser;
                if(user){
                    // await deleteKarixWebhook();
                    var uninstallApp = firebase.functions().httpsCallable('unInstallApp');
                    uninstallApp({"extensionName":extensionName,"service":"karix","authKeyword":"isKarixAuth","isNew":true}).then(function(result) {
                        processCompleted(1);
                        window.location.reload();
                    });
                }
            }
        }
}
  

  function openHubspot(){
    var user = firebase.auth().currentUser;
    if(user){
        var hubspoturl = "https://app.hubspot.com/oauth/authorize?client_id=5fbdbb64-ccf2-49c7-905a-d0eb7b26cbcd&redirect_uri=https://us-central1-hubspot-integration-7aacd.cloudfunctions.net/oauthCallbackHubspot&scope=contacts%20automation%20timeline&state="+extensionName+"-"+user.uid;
         window.open(hubspoturl,"_self");
    }
    else{
      alert("Please sign-in");
    }    
  } 
  


async function saveAPIKey() {
    if(!isHubAuth){
        alert("Please Authorize HubSpot first.");
        return;
    }
    var user = firebase.auth().currentUser;
    let apikey = $("#apikey").val();
    let senderid = $("#senderid").val();
    let domain = $("#domain").val();

    
    if(!valueExists(apikey) || !valueExists(senderid)){
        showErroMessage("Please fill all the credentials");
        return;
    }
    var senderIds =[];
    var senderidsUi = document.getElementsByClassName("senderid");
    if(senderidsUi && senderidsUi.length){
        for(let i=0;i<senderidsUi.length;i++){
            if(senderidsUi[i] && senderidsUi[i].value){
                senderIds.push(senderidsUi[i].value);
            }
        }
    }
    if (user) {
        createdAllCredentials = 0;
        var credAdProcess1 = curId++;
        showProcess(`Adding credentials (1)...`, credAdProcess1);
        var handleActions = firebase.functions().httpsCallable('handleActions');
        handleActions({"newData":true,"isKarixAuth":true,"action":"connectApp","appName":"karix","credentials":{"apikey": apikey.trim(),"domain":domain.trim(),"senderIds":senderIds},"settings":{"domain":domain.trim(),"senderIds":senderIds},"extensionName":extensionName}).then(async function(result) {
            console.log(result);
            // await addKarixWebhook();
            processCompleted(credAdProcess1);
            handleauthoriztion();
        });
    }
    else {
        alert("Please sign-in");
    }
}
function makeAppcall(data){
    data.headers = '{"Authorization": "${username}:${password}"}';
    data.appName = "karix";
    data.extension= extensionName;
    data.authType = "basic_auth";
    data = JSON.stringify(data);
    var makeAppCall = firebase.functions().httpsCallable('makeAppCall');
    return makeAppCall(data).then(function(result) {
      // Read result of the Cloud Function.
      return result.data;
    }).catch(function(error) {
      // Getting the Error details.
      var code = error.code;
      var message = error.message;
      var details = error.details;
      console.log(error)
      return null;
      // ...
    });

}

async function addKarixWebhook(){
    try{
        var apiData ={
            "url":"https://api.karix.io/webhook/",
            "method": "POST",
            "data":{"name":"HubSpot - Karix Integratinon","events_url":"https://us-central1-ulgebra-license.cloudfunctions.net/karixCallback?userId="+firebase.auth().currentUser.uid}
        };
        var resp = await makeAppcall(apiData).then(function(resp){
            console.log(resp);
            return resp;
        });
    }
    catch(e){
        console.log(e);
        return;
    }    
}
async function deleteKarixWebhook(){
    var webhooks = await makeAppcall({"url":"https://api.karix.io/webhook/","method":"GET"});
    webhooks=webhooks?webhooks.data:null;
    var webhookId;

    if(webhooks && webhooks.objects && webhooks.objects.length){
        webhooks.objects.forEach(function(webhook){
            if(webhook.name == "HubSpot - Karix Integratinon" && webhook.events_url == "https://us-central1-ulgebra-license.cloudfunctions.net/karixCallback?userId="+firebase.auth().currentUser.uid){
                webhookId = webhook.uid;
            }
        })
    }
    if(webhookId){
        var apiData ={
            "url":"https://api.karix.io/webhook/"+webhookId+"/",
            "method": "DELETE",
            "data":{"uid":webhookId}
        };
        var resp = await makeAppcall(apiData).then(function(resp){
            console.log(resp);
            return resp;
        });
    }    
}
function reloadIfAllCredsCompleted(){
    if(createdAllCredentials === 3){
        handleauthoriztion();
    }
}

function reloadIfAllCredsDeleted(){
    if(deletedAllCredentials === 3){
        window.location.reload();
    }
}
function removeAccount(accountId){
    var credAdProcess1 = curId++;
    showProcess(`Removing credentials...`, credAdProcess1);
    var handleActions = firebase.functions().httpsCallable('handleActions');
    handleActions({"action":"removeAccount","accountId":accountId,"extensionName":extensionName,"accountNo":karix_accounts}).then(async function(result) {
        console.log(result);
        karix_accounts--;
        processCompleted(credAdProcess1);
        handleauthoriztion();
    });
}
var isHubAuth = false;
var karix_accounts = 0;
async function handleauthoriztion() {
    $("#WTR-HUBS-AUTH").addClass('waitToResolve');
    $("#WTR-INTEG-STATUS").addClass('waitToResolve');
    var user = firebase.auth().currentUser;
    if (user) {
        // $("#reply_url").val("https://us-central1-ulgebra-license.cloudfunctions.net/clickatellCallback?action=reply&userId="+user.uid+"&apiKey=YOUR_CLICKATELL_API_KEY");
        // $("#status_url").val("https://us-central1-ulgebra-license.cloudfunctions.net/clickatellCallback?action=status&userId="+user.uid+"&apiKey=YOUR_CLICKATELL_API_KEY");
        $("#WTR-HUBS-AUTH").addClass('waitToResolve');
        if(!portalId){
            portalId = await db.collection("Users").doc(user.uid).get().then(function(doc) {
                if(doc.exists && doc.data().hptId){
                    return doc.data().hptId;
                }
                return null;
            });
        }
        if(portalId){
            var docRef = db.collection("hubspotOrgs").doc(portalId).collection('extensions').doc(extensionName);
            docRef.get().then(function (doc) {
              
                $("#WTR-ACUITY-AUTH").removeClass('waitToResolve');
                $("#WTR-INTEG-STATUS").removeClass('waitToResolve');
                $("#WTR-HUBS-AUTH").removeClass('waitToResolve');
                if (doc.exists) {
                    console.log("Document data:", doc.data());
                    var data = doc.data();
                    let zohoISConnected = data && data.isHubAuth;
                    let gotoISConnected = data && data.isKarixAuth;

                    if (zohoISConnected) {
                        isHubAuth = true;
                        $("#hubspotbutton").html(`<span class="material-icons">check_circle</span> Authorized`).attr({'disabled': 'true'}).css({'background-color': 'green'});
                    }

                    if (gotoISConnected) {
                        $("#karix_accounts").html('<h4>Connected Karix Accounts</h4>');
                        karix_accounts =0;
                        if(data.domain && data.senderid){
                            karix_accounts++;
                            $("#karix_accounts").append(`<div id="karix">${data.domain} <button style="float:right;" onclick="removeAccount('karix')">Remove</button></div><br>`)
                        }
                        var keys = Object.keys(data);
                        keys.forEach(function(key){
                            if(typeof data[key] == "object" && data[key] && data[key].domain){
                                karix_accounts++;
                                $("#karix_accounts").append(`<div id="${key}">${data[key].domain} <button style="float:right;" onclick="removeAccount('${key}')">Remove</button></div><br>`)
                            }
                        })
                        if(karix_accounts){
                            $("#karix_accounts").show();
                        }
                        else{
                            $("#karix_accounts").hide();
                        }
                        $('#acuitybutton').html(`<span class="material-icons">check_circle</span> Authorized`).attr({'disabled': 'true'}).css({'background-color': 'green'});
                    }
                    else {
                            $("#twiliocreds").show();
                            $('#acuitybutton').html(`Authorize Now`).attr({'disabled': false}).css({'background-color': 'royalblue'});
                    }

                    if (zohoISConnected && gotoISConnected) {
                        $("#twiliocreds").hide();
                        $("#INTEG-STATUS").text("Enabled").css({'background-color': '#06d506'});
                        $("#UNINSTALL-APP-BTN").show();
                        allAuthorized = true;
                    } else {
                        $("#WTR-PH-NUMS").removeClass('waitToResolve');
                        $("#INTEG-STATUS").text("Not Active").css({'background-color': 'grey'});
                        $("#UNINSTALL-APP-BTN").hide();
                    }
                    if(zohoISConnected || gotoISConnected){
                        $("#UNINSTALL-APP-BTN").show();
                    }
                }
                else {
                    $("#WTR-PH-NUMS").removeClass('waitToResolve');
                    $("#INTEG-STATUS").text("Not Active").css({'background-color': 'grey'});
                    $("#UNINSTALL-APP-BTN").hide();
                    console.log("No such document!");
                }

            }).catch(function (error) {
                console.log("Error getting document:", error);
            });
        }
        else{
            $("#WTR-ACUITY-AUTH").removeClass('waitToResolve');
            $("#WTR-INTEG-STATUS").removeClass('waitToResolve');
            $("#WTR-HUBS-AUTH").removeClass('waitToResolve');
            $("#WTR-PH-NUMS").removeClass('waitToResolve');
            $("#INTEG-STATUS").text("Not Active").css({'background-color': 'grey'});
            $("#UNINSTALL-APP-BTN").hide();
        }    
    }
}


function showReAUTHORIZEError() {
    showErroWindow('Kindly authorize the app again', '<div style="margin-top:-20px;">Go to <b style="color:royalblue">GENERAL SETTINGS</b> tab above. Then  click <b style="color:royalblue">Revoke</b> button and authroize again. Then visit this tab. Refer the image below. <br><br> <a target="_blank" title="Click to View Image" href="https://puplicfiles.s3.us-east-2.amazonaws.com/uhoioe8y28g79e/inconsistent_app_init.png"><img id="inconst_error_img" src="https://puplicfiles.s3.us-east-2.amazonaws.com/uhoioe8y28g79e/inconsistent_app_init.png" style="max-width:80%;max-height:320px"/></a>');
    $('.error-window-title').css({'margin-top': '-30px'});
}

function showAddPhoneDialog(){
    showErroWindow();
}


function removeElem(elem){
    $(elem).remove();
}

//   function addwebhook(){
//     var body={
//   "actionUrl": "https://us-central1-hubspot-integration-7aacd.cloudfunctions.net/hubspotWebhookAction?app=karixforhubspotcrm",
//   "published": true,
//   "inputFields": [
//     {
//       "typeDefinition": {
//         "name": "text",
//         "type": "string",
//         "fieldType": "textarea"
//       },
//       "supportedValueTypes": [
//         "STATIC_VALUE"
//       ],
//       "isRequired": true
//     },
//     {
//       "typeDefinition": {
//         "name": "contactNumber",
//         "type": "string",
//         "fieldType": "text"
//       },
//       "supportedValueTypes": [
//         "STATIC_VALUE"
//       ],
//       "isRequired": true
//     },
//     {
//       "typeDefinition": {
//         "name": "countryCode",
//         "type": "string",
//         "fieldType": "text"
//       },
//       "supportedValueTypes": [
//         "STATIC_VALUE"
//       ],
//       "isRequired": false
//     },
//     {
//       "typeDefinition": {
//         "name": "senderid",
//         "type": "string",
//         "fieldType": "text"
//       },
//       "supportedValueTypes": [
//         "STATIC_VALUE"
//       ],
//       "isRequired": true
//     },
//     {
//       "typeDefinition": {
//         "name": "apikey",
//         "type": "string",
//         "fieldType": "text"
//       },
//       "supportedValueTypes": [
//         "STATIC_VALUE"
//       ],
//       "isRequired": true
//     },
//     {
//       "typeDefinition": {
//         "name": "domain",
//         "type": "string",
//         "fieldType": "text"
//       },
//       "supportedValueTypes": [
//         "STATIC_VALUE"
//       ],
//       "isRequired": true
//     }
//   ],
//   "labels": {
//     "en": {
//       "inputFieldLabels": {
//         "text": "Message",
//         "contactNumber":"Contact Number",
//         "countryCode":"CountryCode",
//         "senderid":"Karix Sender Id",
//         "apikey":"Karix Api Key",
//         "domain":"Karix Domain"
//       },
//       "inputFieldDescriptions": {
//         "text":"Enter Message",
//         "contactNumber":"Enter Contact Number",
//         "countryCode":"Enter CountryCode",
//         "senderid":"Enter Karix Sender Id",
//         "apikey":"Enter Karix Api Key",
//         "domain":"Enter Karix Domain"
//       },
//       "actionName": "Send Karix SMS",
//       "actionDescription": "Send SMS via Karix",
//       "appDisplayName": "Karix",
//       "actionCardContent": "Send Karix SMS"
//     },
//   },
//   "objectTypes": [
//     "CONTACT","COMPANY","DEAL"
//   ]
// };
    
//     fetch("https://us-central1-ulgebra-license.cloudfunctions.net/msbrequest", {
//         "method" :"POST",
//         headers: {
//             'Content-Type': 'application/json;charset=utf-8'
//         },
//         body: JSON.stringify({
//             "url":"",
//             "method": "POST",
//             "data":body,
//             "headers":{
//                 "content-type": "application/json"
//             }
//         })
//     })
//     .then(function (response) {  
//         console.log(response);
//         return response.json();
//     })
//     .then(function (myJson) {
//         console.log(myJson);
//     })
//     .catch(function (error) {
//         console.log("Error: " + error);
//     });
//   }

