
const urlParams = new URLSearchParams(window.location.search);
const data = JSON.parse(urlParams.get('contact'));
const contactId = data.associatedObjectId;
const portalId =data.portalId;

// const contactId = urlParams.get('contactId');
// const portalId =urlParams.get('portalId');
var extensionName= "karixforhubspotcrm";
var contact;
var fields;
var countryCode; 
var currentRecord;
var recordModule="Contacts";
var templates;
var userId;
var userEmail;
var customerNo;
var appsConfig = {
    "AUTH_ID": undefined,
    "AUTH_TOKEN": undefined,
    "PHONE_NUMBER": undefined,
    "APPLICATION_ID": undefined,
    "APP_UNIQUE_ID": undefined,
    "UA_DESK_ORG_ID": undefined,
    "TWILIO_SERVICE_ID": undefined,
    "CURRENT_CHAT_CHANNEL": "sms",
    "MESSAGES_LIST": {},
    "CURRENT_CONV_ID" : undefined
};
var RTCs = {

    };
var initialAppsConfig = {
    "AUTH_ID": undefined,
    "AUTH_TOKEN": undefined,
    "PHONE_NUMBER": undefined,
    "APPLICATION_ID": undefined,
    "APP_UNIQUE_ID": undefined
};
appsConfig.UA_DESK_ORG_ID = portalId;

let allAuthorized = false;
let phoneFetched = false;
let haveWhatsAppNumber = false;
let showWhatsAppSandboxNumber = true;
var database ;
var siteAPIURL = "https://us-central1-ulgebra-license.cloudfunctions.net";

function makeAppcall(data){
    data.headers = '{"Authorization": "${username}:${password}"}';
    data.appName = "karix";
    data.extension= extensionName;
    data.authType = "basic_auth";
    data = JSON.stringify(data);
    var makeAppCall = firebase.functions().httpsCallable('makeAppCall');
    return makeAppCall(data).then(function(result) {
      // Read result of the Cloud Function.
      return result.data;
    }).catch(function(error) {
      // Getting the Error details.
      var code = error.code;
      var message = error.message;
      var details = error.details;
      console.log(error)
      return null;
      // ...
    });

}
async function handleauthoriztion(){
    if(firebase.auth().currentUser){
        userId = firebase.auth().currentUser.uid;
        db.collection("hubspotOrgs").doc(portalId).collection("extensions").doc(extensionName).get().then(async function(doc){
          if(doc.exists && doc.data().isHubAuth && doc.data().isKarixAuth){
            initializeFromConfigParams();
          }
          else{
            showErroMessage("Please Authorize the hubspot in <a  href='https://app.azurna.com/hubspot/karix/settings' target='_blank'><button class='ext'>Extension Settings</button></a>");
          }
        })
    }    
}  
var initLoading = 0;
function initializeFromConfigParams() {
    getRecordDetails();
    loadKarixumbers();
}
async function getRecordDetails(){
    let userId =firebase.auth().currentUser.uid;
    contact = await getContact(contactId);
    if(contact){
        currentRecord = contact.data.properties;
    }
    fields = await getContactFields();
    settings = await db.collection("hubspotSettings").doc(userId).get().then(doc => {
        if(doc.exists){
            return doc.data();
        }
        else{
            return null;
        }    
    });
    var phoneField = "";
    if(settings){
        countryCode = settings.countryCode;
        var phoneField = settings.phoneField?settings.phoneField.Contacts:"";
    }    
    var phoneList='';
    fields.data.forEach(function(field){
        if(field.textDisplayHint == "phone_number" && field.groupName == "contactinformation" && currentRecord[field.name]){
            $("#contact_numbers").append(`<div class="sgd-item" onclick="selectPhone('${parseInt(currentRecord[field.name].value)}')">
                ${currentRecord[field.name].value} <i> ( ${field.label} ) <i>
            </div>`);
            if(phoneField == field.name || !$("#chat_from_phone").val()){
                $("#chat_from_phone").val(currentRecord[field.name].value.replace(/\D/g,''))
            }
        }
    });
    $("#chat_from_phone").val()?checkPhoneNumber($("#chat_from_phone").val()):"";
    // $('#phoneNumberConfigWindow').show();
    // getTemplateDetails();
    $("#WTR-CHAT-CONFIG").removeClass('waitToResolve');
    initLoading++;
    if(initLoading == 2){
        $("#WTR-PH-NUMS").removeClass('waitToResolve');
    }
}
async function loadKarixumbers(){
    var apiData ={
        "url":"https://api.karix.io/number/",
        "method": "GET"
    };
    var karixNumbers = await makeAppcall(apiData).then(function(resp){
        console.log(resp);
        return resp.data?resp.data:null;
    });
    initLoading++;
    if(initLoading == 2){
        $("#WTR-PH-NUMS").removeClass('waitToResolve');
    }
   
    $(".phoneItems").html("");
    if(showWhatsAppSandboxNumber){
        renderPhoneNumber({
            "number": "+13253077759",
            "service": {
                "Whatsapp Sandbox":true
            },
            "region":{"country":"US"},
        });
        renderPhoneNumber({
            "number": "KARIXM",
            "service": {
                "SMS":true
            },
            "region":{"country":"US"},
        });
    }
    if(karixNumbers && karixNumbers.objects && karixNumbers.objects.length){
        karixNumbers.objects.forEach(function(number){
            if(number && number.number_details){
                renderPhoneNumber(number.number_details);
            }
        })
    }
    $("#phoneNumberConfigWindow").show();
}
function renderPhoneNumber(obj){
    let classes = "";
    var capabilities=[];
    Object.keys(obj.service).forEach(item=>{
        if(obj.service[item]){
            capabilities.push(item);
            classes += "phch-"+item+" ";
        }
    });
    classes = classes.toLowerCase();
    $(".phoneItems").append(`<div class="phoneItem ${classes}" id="elem-${obj.number}">
        <div class="phoneNum">
        ${obj.number} ( ${obj.region.country} )
        <div class="sub-sts c-green">
            Capable of ${capabilities.join(", ")}
        </div>
        </div>
        <div class="phoneAct">
            <button class="subscribe" onclick="chooseChannel('${obj.number}', '${capabilities.join(", ").toLowerCase()}')">
                Choose
            </button>
        </div>
    </div>`);
}
        
function chooseChannel(phNumber, capacities){
    $("#chat_to_phone").attr({'ph': phNumber});
    var channelOptions = '';
    if(capacities.indexOf('whatsapp sandbox')>-1){
         $("#chat_to_phone").attr({'ch': "whatsapp"}).html(phNumber+` ( WhatsApp Sandbox ) <span class="material-icons">arrow_drop_down</span>`);
    }
    else if(capacities.indexOf('whatsapp')>-1){
         $("#chat_to_phone").attr({'ch': "whatsapp"}).html(phNumber+` ( WhatsApp ) <span class="material-icons">arrow_drop_down</span>`);
    }
    else if(capacities.indexOf('sms')>-1){
         $("#chat_to_phone").attr({'ch': "sms"}).html(phNumber+` ( SMS ) <span class="material-icons">arrow_drop_down</span>`);
    }
    if(capacities.indexOf('whatsapp')>-1 || capacities.indexOf('whatsapp sandbox')>-1){
        channelOptions = channelOptions+ '<option value="whatsapp">WhatsApp</option>';
    }
    if(capacities.indexOf('sms')>-1){
        channelOptions =channelOptions+ '<option value="sms">SMS</option>';
    }
    $("#channel").append(channelOptions);
    $("#phoneNumberConfigWindow").hide();
    refreshConversationBox();
}
async function refreshConversationBox(){
    appsConfig.MESSAGES_LIST = [];
    appsConfig.CURRENT_CHAT_CHANNEL =  $("#chat_to_phone").attr('ch');
    $('.chatbox-outer').append(`<div class="loadingdiv"> <span class="spinnow material-icons" style="font-size: 20px;display:inline-block;">toys</span> Fetching ${appsConfig.CURRENT_CHAT_CHANNEL === "sms" ? "SMS": "WhatsApp"} messages ...</div>`);
    
    var apiDataOutgoing ={
        "url":"https://api.karix.io/message/?source="+encodeURIComponent("+"+$("#chat_to_phone").attr('ph').replace(/\D/g,''))+"&destination="+encodeURIComponent("+"+$("#chat_from_phone").val().replace(/\D/g,'')),
        "method": "GET"
    };
    var apiDataIncoming ={
        "url":"https://api.karix.io/message/?destination="+encodeURIComponent("+"+$("#chat_to_phone").attr('ph').replace(/\D/g,''))+"&source="+encodeURIComponent("+"+$("#chat_from_phone").val().replace(/\D/g,'')),
        "method": "GET"
    };
    var messagesOutgoing = await makeAppcall(apiDataOutgoing).then(function(resp){
        console.log(resp);
        return resp?resp.data:null;
    });

    var messagesIncoming = await makeAppcall(apiDataIncoming).then(function(resp){
        console.log(resp);
        return resp?resp.data:null;
    });
    var messages =[];

    if(messagesOutgoing && messagesOutgoing.objects && messagesOutgoing.objects.length){
        messages = messages.concat(messagesOutgoing.objects);
    }
    if(messagesIncoming && messagesIncoming.objects && messagesIncoming.objects.length){
        messages = messages.concat(messagesIncoming.objects);
    }  
    if(messages.length){
        messages.forEach(function(item){
             appsConfig.MESSAGES_LIST[new Date(item.created_time).getTime()] = item;
        })
    }
    let extId = "+"+$("#chat_to_phone").attr('ph').replace(/\D/g,'')+"::+"+$("#chat_from_phone").val().replace(/\D/g,'');
    if(appsConfig.MESSAGES_LIST && Object.keys(appsConfig.MESSAGES_LIST).length){
        $("#chatbox-message-holder").html("");
        refreshMessagesFromStoredObject(extId);
    }    
    $(".loadingdiv").remove();
}

function refreshMessagesFromStoredObject(convId){
    appsConfig.CURRENT_CONV_ID = convId;
    initiateRTListeners(convId);
    $("#existingConvWindow").removeClass('ch-channel-whatsapp').removeClass('ch-channel-sms').addClass('ch-channel-'+appsConfig.CURRENT_CHAT_CHANNEL);
    Object.keys(appsConfig.MESSAGES_LIST).sort().reverse().forEach(item=>{
        addMessageInBox(appsConfig.MESSAGES_LIST[item], false);
    });
}

function addMessageInBox(msgObj, addAtLast,file) {
    var content = escapeHTMLS(msgObj.content ? msgObj.content[msgObj.content_type] : "");
    if(msgObj.content_type == "text"){
        content = content.replace(/\n/g,"<br>");
    }
    else if(msgObj.content[msgObj.content_type].caption){
        content = msgObj.content[msgObj.content_type].caption;
    }
    else{
        content = "";
    }
    var mediaHTML = "";
    var inComing = msgObj.direction === "inbound";
    var dataTime = new Date(msgObj.created_time);
    var msgHTML = (`<div  id="cm-${msgObj.uid}" class="chatmessage-item ${inComing ? 'direction-in' :'direction-out' } msgsts-${msgObj.status} ${msgObj.unread ? 'unread': ''}">
        <div class="chatmessage-inner">
            <div class="chatmessage-content" onclick="showMessageTheater('${msgObj.uid}')" id="media-${msgObj.uid}">
                ${content} 
            </div>
            <div class="chatmessage-time" title="${new Date(dataTime).toLocaleString()}">
                <span class="fullTime">${new Date(dataTime).toLocaleString()}</span>
                <span class="smallTime">${getTimeString(dataTime)}</span>
                ${inComing ? '' : '<span class="msgStatus" id="st-'+msgObj.uid+'">'+(getStatusHTML(msgObj.status?msgObj.status:msgObj.direction))+'</span>'}
            </div>
        </div>
    </div>`);
    $(".chatmessages-inner").scrollTop($(".chatmessages-inner")[0].scrollHeight);
    if (addAtLast) {
        $("#MID-LOADING-" + msgObj.uid).remove();
        $('.chatmessages-inner').append(msgHTML);
    } else {
        $('.chatmessages-inner').prepend(msgHTML);
    }
    if(msgObj.content[msgObj.content_type].url){

        var mediaType = msgObj.content[msgObj.content_type].type;
        if(!mediaType && file && file.type){
            mediaType = file.type;
        }
        $(`#media-${msgObj.uid}`).prepend('<div class="medPlaceHoldr"><span class="spinnow material-icons" style="font-size: 20px;display:inline-block;">toys</span> Loading attachment ...</div>');
        $(`#media-${msgObj.uid} .medPlaceHoldr`).remove();
        $(`#media-${msgObj.uid}`).prepend(getMediaHtml(msgObj.content[msgObj.content_type].url,mediaType)+" <br>");
    }        
}
async function selectPhone(phone){
    $("#chat_from_phone").val(phone.replace(/\D/g,''));

    await checkPhoneNumber(phone);
    refreshConversationBox();
}
function checkPhoneNumber(no){
    $('.chatbox-outer').append(`<div class="loadingdiv"> <span class="spinnow material-icons" style="font-size: 20px;display:inline-block;">toys</span> Validating the choosen phone number...</div>`);
    return fetch("https://us-central1-ulgebra-license.cloudfunctions.net/msbrequest", {
        "method" :"POST",
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({
            "url":"https://rest.messagebird.com/lookup/" + no,
            "method": "GET",
            "headers":{
                "Authorization":"AccessKey 7NMPor0R8DofSHH61SpViNNqQ"
            }
        })
    })
    .then(function (response) {  
        console.log(response);
        return response.json();
    })
    .then(function (myJson) {
        console.log(myJson);
        if(myJson && myJson.code == 400){
            if(countryCode && countryCode != "0" && no.length > countryCode.length){
                $("#chat_from_phone").val(countryCode+""+no);
            }
            else{
                // showErroMessage(`<div >Invalid Phone number.Please Make sure <b>Country Code</b> is added in <span onclick="redirectToSettings()" style="color:blue;cursor:pointer;">extension settings.</span></div>`)
            }
        }
        $(".loadingdiv").remove();
        return true;
    })
    .catch(function (error) {
        $(".loadingdiv").remove();
        console.log("Error: " + error);
    });
} 

async function getTemplateDetails(){
    if(firebase.auth().currentUser){
        let userId =firebase.auth().currentUser.uid;
        templates = await db.collection("hubspotSettings").doc(userId).collection('whatsappTemplates').get().then(querySnapshot => {
            var data ={};
            querySnapshot.forEach(doc => {
                console.log(doc.id, " => ", doc.data());
                data[doc.id]=doc.data();
            });
            return data;
        });
    }    
    $("#WTR-TEMP").removeClass('waitToResolve');
    var templateList="";
    if(templates){
        templateList =templateList+ '<li class="templateItem" onclick="showsms(this)">Choose Template</li>';
        var templateIds = Object.keys(templates);
        templateIds.forEach(function(templateId){
            templateList = `${templateList}<option value="${templateId}" id="${templateId}"></option>`;
        });
        $('#templateList').append(templateList);
        templateIds.forEach(function(templateId){
            let template = templates[templateId];
            document.getElementById(templateId).innerText = template.n; 
        })
    }
    else{
        $('#templateList').append(`<option value="" >No Templates</option>`);
    }
}
function selectTemplate(templateId, textAreaId) {
    if(!templateId){
        $("#" + textAreaId).val("");
        return;
    }
    for (var i = 0; i < smsTemplates.length; i++) {
        if (smsTemplates[i].id == templateId) {
            $("#" + textAreaId).val(getMessageWithFields(smsTemplates[i].whatsappforzohocrm0__Message));
            break;
        }
    }
}

function getContact(contactId){
    var apiData =JSON.stringify({
        "url":"https://api.hubapi.com/contacts/v1/contact/vid/"+contactId+"/profile",
        "method": "GET",
        "extension":extensionName
    });
    return makeHubspotcall(apiData).then(function(resp){
        console.log(resp);
        return resp;
    })
}
function getContactFields(){
    var apiData =JSON.stringify({
        "url":"https://api.hubapi.com/properties/v1/contacts/properties",
        "method": "GET", 
        "extension":extensionName
    });
    return makeHubspotcall(apiData).then(function(resp){
        console.log(resp);
        return resp;
    })
}

// function getMessage(url){
//     return new Promise(resolve => {
//         fetch(siteAPIURL+"/makeTwilioHTTPCall", {
//             "method" :"POST",
//             headers: {
//                 'Content-Type': 'application/json;charset=utf-8'
//             },
//             body: JSON.stringify({
//                 "url": url,
//                 "method": "GET",
//                 "orgId": appsConfig.UA_DESK_ORG_ID,
//                 "secContext": appsConfig.APP_UNIQUE_ID,
//                 "appCode": extensionName,
//             })
//         })
//         .then(function (response) {
//             return response.json();
//         })
//         .then(function (myJson) {
//             console.log(myJson);
//             if(myJson.error){
//                 showErroMessage(myJson.error.message);
//                 return true;
//             }
//             resolve(myJson);
//         })
//         .catch(function (error) {
//           console.log("Error: " + error);
//           resolve(error);
//         });
//     });    
// }


async function addHistory(data,mediaUrl){
    console.log(data);
    var content=data.content[data.content_type];
    if(content.url){
        content = (content.caption?content.caption+", ":"")+"Url : "+content.url ;
    }
    var history ={"platform":data.channel,"messageId":data.uid,"direction":data.direction,"sender_phone":data.source,"receiver_phone":data.destination,"status":data.status,"message":content};
    var body={ 
        "eventTemplateId":"1056546",
        "objectId":contactId,
        "tokens": history
    }
    var apiData =JSON.stringify({
        "url":"https://api.hubapi.com/crm/v3/timeline/events",
        "method": "POST",
        "data":body,
        "extension":extensionName
    }) ;
    var resp = await makeHubspotcall(apiData).then(function(resp){
        console.log(resp);
        return resp;
    })
}

function listernForStatusChange(chatId,mId){
    var starCountRef = firebase.database().ref('kas/'+chatId);
    starCountRef.on('value', function(snapshot) {
        RTCs[chatId]++;
        if(RTCs[chatId] === 1){
            return;
        }
        console.log(snapshot.val());
        if(snapshot.val() && snapshot.val().ts == mId && $("#st-"+mId).length){
            $("#st-"+mId).html(getStatusHTML(snapshot.val().st));
        }
    });
}

function makeHubspotcall(data){
    var makeHubspotHTTPCall = firebase.functions().httpsCallable('makeHubspotHTTPCall');
    return makeHubspotHTTPCall(data).then(function(result) {
      // Read result of the Cloud Function.
      if(result && result.data == "accesstoken_not_found"){
        showErroMessage("Please Authorize the hubspot in <a  href='https://app.azurna.com/hubspot/karix/settings' target='_blank'><button class='ext'>Extension Settings</button></a>");
        return null;
      }
      return result;
    }).catch(function(error) {
      // Getting the Error details.
      var code = error.code;
      var message = error.message;
      var details = error.details;
      console.log(error)
      showErroMessage("Please Authorize the hubspot in <a  href='https://app.azurna.com/hubspot/karix/settings' target='_blank'><button class='ext'>Extension Settings</button></a>");
      return null;
      // ...
    });
}




function getMessageWithFields(messageDetails){
    var message = messageDetails.message;
    var customerData=[];
    
    fields.data.forEach(function(field){
        try{
            var replace = "\\${Contacts."+field.label+"}";
            var re = new RegExp(replace,"g");
            if(currentRecord[field.name] != null)
            {
                var value = currentRecord[field.name].value;
                if(value.name)
                {
                    value = value.name;
                }
                message = message.replace(re,value);
            }
            else
            {
                message = message.toString().replace(re," ");
            }
        }catch(err){
            loadDebugInfo(":unable-to-replace-plc:"+err);
        }
    }); 
    return message;
}
















async function renderChatMessages(fromPhone, toPhone){
     return new Promise(resolve => {
       fromPhone = fromPhone.replace( /^\D+/g, '');
       toPhone = toPhone.replace( /^\D+/g, '');
       let params = "";
       if(fromPhone !== ""){
           params += "From="+ encodeURIComponent(appsConfig.CURRENT_CHAT_CHANNEL === "whatsapp" ? "whatsapp:+" : "+") +fromPhone;
       }
       if(toPhone !== ""){
            if(params!==""){
                params+="&";
            }
           params += "To="+ encodeURIComponent(appsConfig.CURRENT_CHAT_CHANNEL === "whatsapp" ? "whatsapp:+" : "+") +toPhone;
       }
       fetch(siteAPIURL+"/makeTwilioHTTPCall", {
        "method" :"POST",
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({
            "url": `https://api.twilio.com/2010-04-01/Accounts/{{SID}}/Messages.json?${params}&PageSize=99`,
            "method": "GET",
            "orgId": appsConfig.UA_DESK_ORG_ID,
            "secContext": appsConfig.APP_UNIQUE_ID,
            "appCode": extensionName
        })
    })
    .then(function (response) {
        $("#WTR-PH-NUMS").removeClass('waitToResolve');
        return response.json();
    })
    .then(function (myJson) {
        console.log(myJson);
        if(myJson.error){
            showErroMessage(myJson.error.message);
            return true;
       }
       myJson.data.messages.forEach((item)=>{
           appsConfig.MESSAGES_LIST[new Date(item.date_created).getTime()] = item;
       });
       resolve('ok');
    })
    .catch(function (error) {
      console.log("Error: " + error);
      resolve('noo');
    });
     });
}

window.addEventListener('load', (function(){
    $("#main-message-text").on('keypress', function (e) {
        if (e.keyCode === 13 && !e.shiftKey) {
            if ($("#enterToSendCheck").is(":checked")) {
                event.preventDefault();
                sendMessage();
                return false;
            }
        }
    });
 
    // $("#chat_from_phone").on('keyup', function (e) {
    //     if (e.keyCode === 13) {
    //         refreshConversationBox();
    //     }
    // });
 

    $(".chatmessages-inner").on('click', (function() {
        $('.chatmessage-item.unread').removeClass('unread');
    }));

    $("input[type='number']").bind('keydown', function () {
         $(this).attr("size", Math.max(10,$(this).val().length) );
    });

    if(document.getElementById("inputFile")){
        document.getElementById("inputFile").addEventListener("change", (function (event) {
            if(appsConfig.CURRENT_CHAT_CHANNEL == "whatsapp"){
                handleFile(event);
                // var file = document.getElementById("inputFile").files[0];
                // if (((file.size / 1024) / 1024) > 10) {
                //   $(".loadingdiv").remove();
                //   $('.chatbox-outer').append(`<div style="background-color:red;" class="loadingdiv"> file size should be within 10 Mb.</div>`);
                //   setTimeout((function(){ $(".loadingdiv").remove(); }), 2000);
                //   return;
                // }
                // if(file.type.indexOf("image") == -1 && file.type.indexOf("audio") == -1 && file.type.indexOf("video") == -1 && file.type.indexOf("pdf") == -1){
                //   $(".loadingdiv").remove();
                //   $('.chatbox-outer').append(`<div style="background-color:red;" class="loadingdiv"> This file type not supported.</div>`);
                //   setTimeout((function(){ $(".loadingdiv").remove(); }), 2000);
                //   return;
                // }
                // sendfile(file);
            }
            else{
                alert("attachment only supported for WhatsApp channel");
            }    
        }));
    }
}), false); // NB **not** 'onload'

    function handleFile(event){
        $('.chatbox-outer').append(`<div class="loadingdiv"> <span class="spinnow material-icons" style="font-size: 20px;display:inline-block;">toys</span> file is uploading...</div>`);
        if(((document.getElementById("inputFile").files[0].size / 1024) / 1024) > 10) {
            $(".loadingdiv").remove();
            $('.chatbox-outer').append(`<div style="background-color:red;" class="loadingdiv"> file size should be within 10 Mb.</div>`);
            setTimeout((function(){ $(".loadingdiv").remove(); }), 2000);
            return;
        }
        var file = document.getElementById("inputFile").files[0];
        var docRef = db.collection("ulgebraUsers").doc(firebase.auth().currentUser.uid);
        docRef.get().then(function(doc) {
            if (doc.exists && doc.data().trackSize) {
                let userUsageData = doc.data();
                if(((userUsageData.trackSize+parseInt(file.size)) <= (10*1024*1024)) && (userUsageData.trackCount < userUsageData.trackLimit) && (userUsageData.lastTrackCT+30000 < new Date().getTime())){
                    sendfile(document.getElementById("inputFile").files[0]);
                }
                else{
                    $(".loadingdiv").remove();
                    $('.chatbox-outer').append(`<div style="background-color:red;" class="loadingdiv"> your files upload limit exceeded,Please upgrade it.</div>`);
                    setTimeout((function(){ $(".loadingdiv").remove(); }), 2000);
                }  
                console.log("Document data:", doc.data());
            }
            else {
                sendfile(document.getElementById("inputFile").files[0]);
                console.log("No such document!");
            }
        }).catch(function(error) {
            $(".loadingdiv").remove();
            $('.chatbox-outer').append(`<div style="background-color:red;" class="loadingdiv">${error}</div>`);
            setTimeout((function(){ $(".loadingdiv").remove(); }), 2000);
            console.log("Error getting  document:", error);
        });
    }
   var imagebase64 = "";
   async function sendMessage(mediaUrl,file) {
        try{
        var text = $("#main-message-text").val().trim();
        text = decodeURIComponent(encodeURIComponent(text).replace(/%C2%A0/g, "%20"));
        if((text && text.length) || file)
        {
            $('.chatbox-outer').append(`<div class="loadingdiv"> <span class="spinnow material-icons" style="font-size: 20px;display:inline-block;">toys</span> Sending message...</div>`);

            let toPhone = $("#chat_from_phone").val().replace( /^\D+/g, '');
            let fromPhone = $("#chat_to_phone").attr('ph').replace( /^\D+/g, '');
                
            var messageObj= {
                "channel":appsConfig.CURRENT_CHAT_CHANNEL,
                "destination":["+"+toPhone],
                "source":"KARIXM",
                "events_url":"https://us-central1-ulgebra-license.cloudfunctions.net/karixCallback?action=status&userId="+firebase.auth().currentUser.uid
            };
            if(file && mediaUrl){
                messageObj["content"]= {
                    "media":{
                       "url":mediaUrl,
                       "caption":text?text:""
                    }   
                }
            }
            else if(text){
                 messageObj["content"]= {"text":text};
            }
            $("#main-message-text").val("");
                var apiData ={
                "url":"https://api.karix.io/message/",
                "method": "POST",
                "data":messageObj
            };
            var sendresp = await makeAppcall(apiData).then(function(resp){
                console.log(resp);
                return resp?resp.data:null;
            });
            
            if(sendresp && sendresp.objects){
                var data = sendresp.objects[0];
                addMessageInBox(data, true,file);
                var chatId = "+"+fromPhone+"::+"+toPhone;
                await firebase.database().ref("/kas/"+chatId).update({ts:data.uid,st:data.status});
                listernForStatusChange(chatId,data.uid);
                addHistory(data,mediaUrl);
                $(".loadingdiv").remove();
                $(".chatmessages-inner").scrollTop($(".chatmessages-inner")[0].scrollHeight);
            }   
            else{
                $(".loadingdiv").remove();
                showErroMessage('Unable to send your message. Try again.');
            } 
          
        }
        else{
             $('.chatbox-outer').append(`<div class="loadingdiv"> Message Cannot be empty</div>`);
            setTimeout((() => {
                $(".loadingdiv").remove();
            }), 1000); 
        }    
        }catch(exc){
            console.log(exc);
            $(".loadingdiv").remove();
            showErroMessage('Unable to send your message. Try again.');
        }
    }
    async function addTimeEntryForOutgoingMessages(){
        try{
        if(!ZOHODESK){
            return;
        }
        var ticketId = await ZOHODESK.get("ticket.id");
        var agentId = await ZOHODESK.get("user.id")
        ticketId = ticketId["ticket.id"];
        agentId = agentId["user.id"];
        if(!ticketId){
            return;
        }
        var reqObj = {
                url: `https://desk.zoho.com/api/v1/tickets/${ticketId}/timeEntry`,
                type: "POST",
                connectionLinkName : "readreceiptconnection",
                type : "POST", 
                headers:{
                    "orgId" : appsConfig.UA_DESK_ORG_ID
                },
                postBody: {
                    "executedTime" : new Date().toISOString(),
                    "description": "Twilio SMS Sent",
                    "ownerId": agentId
                }
            };
            ZOHODESK.request(reqObj).then(function (response) {
console.log(response);
            });
        }catch(exc){
            console.log("exception for adding timentry", exc);
        }
    }

function showReAUTHORIZEError() {
    showErroWindow('Kindly authorize the app again', '<div style="margin-top:-20px;">Go to <b style="color:royalblue">GENERAL SETTINGS</b> tab above. Then  click <b style="color:royalblue">Revoke</b> button and authroize again. Then visit this tab. Refer the image below. <br><br> <a target="_blank" title="Click to View Image" href="https://puplicfiles.s3.us-east-2.amazonaws.com/uhoioe8y28g79e/inconsistent_app_init.png"><img id="inconst_error_img" src="https://puplicfiles.s3.us-east-2.amazonaws.com/uhoioe8y28g79e/inconsistent_app_init.png" style="max-width:80%;max-height:320px"/></a>');
    $('.error-window-title').css({'margin-top': '-30px'});
}



function removeElem(elem){
    $(elem).remove();
}

function redirectToServiceNumbersPage(){
    if(!appsConfig.TWILIO_SERVICE_ID){
        showErroMessage("Authorize Twilio first, otherwise disable app and authorize again.");
        return;
    }else{
        window.open("https://www.twilio.com/console/sms/services/"+appsConfig.TWILIO_SERVICE_ID+"/senders", "_blank");
    }
}


    
    function closeTheater(){
        $(".msgTheater").removeClass('msgTheater');
        $(".chatbox-config").slideDown(100);
            $(".chataction").slideDown(100);
            $('.theaterClose').hide();
    }
    
    function showMessageTheater(msgId){
            $(".theaterClose").show();
            $(".chatbox-config").slideUp(100);
            $(".chataction").slideUp(100);
            $("#cm-"+msgId).addClass('msgTheater');     
    }
    
    function getStatusHTML(status){
        var walimittext = `<a href="https://apps.ulgebra.com/whatsapp-business-api-integration/limitations#h.ytd5bu2d0930" target="_blank">See Why?</a>`;
        switch (status) {
            case "failed":
                return  `FAILED to send.`;
                break;
            case "queued":
                return  `<span class="material-icons">flight_takeoff</span>`;
                break;
                
            case "sent":
                return  `<span class="material-icons">done</span>`;
                break;
                
            case "delivered":
                return  `<span class="material-icons">done_all</span>`;
                break;
                
            case "read":
                return  `<span class="material-icons">done_all</span>`;
                break;
                
            default:
                return status;
                break;
        }
    }
    
    function getMediaHtml(url,content_type){
        var mediaHTML = "";
        if(!content_type){
            return `<div class="msg-attch"><span class="material-icons">attachment</span> <a target="_blank" href="${url}">Click to download file</a></div>`;
        }
        if(content_type.indexOf("image") != -1){
            mediaHTML = `<img src="${url}"/>`;
        }
        else if(content_type.indexOf("video") != -1){
            mediaHTML = `<video controls src="${url}"/>`;
        }
        else if(content_type.indexOf("audio") != -1){
            mediaHTML = `<audio controls src="${url}"/>`;
        }
        else{
            mediaHTML = `<div class="msg-attch"><span class="material-icons">attachment</span> <a target="_blank" href="${url}">Click to download file</a></div>`;
        }
        return mediaHTML;
    }
    
    function escapeHTMLS(rawStr) {
        return $('<textarea/>').text(rawStr).html();
    }
    
    function initiateRTListeners(CID){
        try{
            if(RTCs.hasOwnProperty(CID)){
              console.log("RTC already inited "+RTCs[CID]);
              return;
            }
            RTCs[CID] = 0;

            var starCountRef = firebase.database().ref('kar/'+CID+'/ts');
            starCountRef.on('value', function(snapshot) {
                RTCs[CID]++;
                if(RTCs[CID] === 1){
                    return;
                }
                newRTEvent(CID, snapshot.val());
            });
        }
        catch(exc){
            console.log(exc);
        }
    }
    
    function newRTEvent(CID, MID) {
        addNewMessageToWindow(CID, MID);
    }
    
    async function addNewMessageToWindow(CID, MID) {
        if (appsConfig.CURRENT_CONV_ID !== CID) {
            console.log('Target conv window ' + CID + ' not open...ignoring');
            return;
        }
        if($("#cm-"+MID).length){
            //$("#cm-"+MID+" .msgstatus").html();
            return;
        }
        var mObj = await makeAppcall({"url":"https://api.karix.io/message/"+encodeURIComponent(MID)+"/","method":"GET"}).then(function(data){
            return data?data.data.data:null;
        })
        if(mObj){
            mObj.unread = true;
            addMessageInBox(mObj, true);
            $(".chatmessages-inner").scrollTop($(".chatmessages-inner")[0].scrollHeight);
        }    
           
    }
    
     function getTimeString(previous) {
            previous = new Date(previous);
            var msPerMinute = 60 * 1000;
            var msPerHour = msPerMinute * 60;
            var msPerDay = msPerHour * 24;
            var msPerMonth = msPerDay * 30;
            var msPerYear = msPerDay * 365;

            var elapsed = new Date() - previous;

            if (elapsed < msPerMinute) {
                if(Math.round(elapsed/1000) === 0){
                    return "Just Now";
                }
                 return Math.round(elapsed/1000) + ' seconds ago';
            }

            else if (elapsed < msPerHour) {
                 return Math.round(elapsed/msPerMinute) + ' minutes ago';
            }

            else if (elapsed < msPerDay ) {
                 return Math.round(elapsed/msPerHour ) + ' hours ago';
            }

            else if (elapsed < msPerMonth) {
                return ' ' + Math.round(elapsed/msPerDay) + ' days ago';
            }

            else if (elapsed < msPerYear) {
                return ' ' + Math.round(elapsed/msPerMonth) + ' months ago';
            }

            else {
                return ' ' + Math.round(elapsed/msPerYear ) + ' years ago';
            }
        }
        
        
        
      
     
        
    function openUploader(){
        if(appsConfig.CURRENT_CHAT_CHANNEL == "whatsapp"){
            document.getElementById("inputFile").click();
        }
        else{
            alert("attachment only supported for WhatsApp channel");
        } 
    }
    
    function sendfile(file){
        // File or Blob named mountains.jpg
        // Create the file metadata
        var metadata = {
          contentType: file.type,
        };

        // Upload file and metadata to the object 'images/mountains.jpg'
        var uploadTask = storageRef.child(firebase.auth().currentUser.uid+"/"+(new Date().getTime())+"_"+file.name).put(file, metadata);
        
         var attProcess = curId++;
         showProcess(`Uploading attachment <span id='uplp-${attProcess}'>0</span> % ...`, attProcess);
        
        // Listen for state changes, errors, and completion of the upload.
        uploadTask.on('state_changed', function(snapshot){
          // Observe state change events such as progress, pause, and resume
          // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
          var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          console.log('Upload is ' + progress + '% done');
          $("#uplp-"+attProcess).text(parseInt(progress));
         
          // switch (snapshot.state) {
          //   case firebase.storage.TaskState.PAUSED: // or 'paused'
          //     console.log('Upload is paused');
          //     break;
          //   case firebase.storage.TaskState.RUNNING: // or 'running'
          //     console.log('Upload is running');
          //     break;
          // }
        }, function(error) {
            showErroMessage(error);
          // Handle unsuccessful uploads
        }, function() {
          // Handle successful uploads on complete
          // For instance, get the download URL: https://firebasestorage.googleapis.com/...
          uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
            console.log('File available at', downloadURL);
            processCompleted(attProcess);
            sendMessage(downloadURL,file);
          });
        });
    }
