
const urlParams = new URLSearchParams(window.location.search);
const data = JSON.parse(urlParams.get('contact'));
const contactId = data.associatedObjectId;
const portalId =data.portalId;
var countryCode;
var recordModule = "Contacts";

// const contactId = urlParams.get('contactId');
// const portalId =urlParams.get('portalId');
var extensionName= "karixforhubspotcrm";
var settings={};
async function handleauthoriztion(){
    if(firebase.auth().currentUser){
        database = firebase.database();
        userId = firebase.auth().currentUser.uid;
        db.collection("hubspotOrgs").doc(portalId).collection("extensions").doc(extensionName).get().then(async function(doc){
          if(doc.exists && doc.data().isHubAuth && doc.data().isKarixAuth){
            var data = doc.data();
            settings = data;
            if(data.domain){
                $("#account").append(`<option value="karix" selected>${data.domain} </option>`);
                if(data.senderid){
                    $("#senderid").append(`<option value="${data.senderid}" selected>${data.senderid} </option>`);
                }
            }
            var keys = Object.keys(data);
            keys.forEach(function(key){
                if(typeof data[key] == "object" && data[key] && data[key].domain){
                    $("#account").append(`<option value="${key}">${data[key].domain} </option>`)
                }
            });
            initialize();
          }
          else{
            showErroMessage("Please Authorize the HubSpot and Karix in <a  href='settings.html' target='_blank'><button class='ext'>Extension Settings</button></a>");
          }
        })
    }    
}  
function selectAccount(){
     $("#senderid").html('');
    var accountId = $("#account").val();
    if(accountId == "karix" && settings.senderid){
        $("#senderid").append(`<option value="${settings.senderid}" selected>${settings.senderid} </option>`);
    }
    else if(accountId && settings[accountId] && settings[accountId].senderIds){
        settings[accountId].senderIds.forEach(function(senderid){
            $("#senderid").append(`<option value="${senderid}">${senderid} </option>`)
        })
    }
}
async function initialize(){
    contact = await getContact(contactId);
    currentRecord = contact.data.properties;
    fields = await getContactFields();
    setTemplates();
    setFields(fields);
    setPhoneFields();
    document.getElementById("container").style.display="block";
    document.getElementById("mloader").style.display= "none";
}


async function setTemplates(){
    if(firebase.auth().currentUser){
        let userId =firebase.auth().currentUser.uid;
        templates = await db.collection("hubspotSettings").doc(userId).collection('karixTemplates').get().then(querySnapshot => {
            var data ={};
            querySnapshot.forEach(doc => {
                console.log(doc.id, " => ", doc.data());
                data[doc.id]=doc.data();
            });
            return data;
        });
    }    
    var templateList="";
    $('#templateList').html("");
    if(templates){
        templateList =templateList+ '<li class="templateItem" onclick="showsms(this)">Choose Template</li>';
        var templateIds = Object.keys(templates);
        templateIds.forEach(function(templateId){
            templateList =templateList+ '<li class="templateItem" id="'+templateId+'" onclick="showsms(this)"></li>';
        })
        $('#templateList').append(templateList);
        templateIds.forEach(function(templateId){
            let template = templates[templateId];
            document.getElementById(templateId).innerText = template.n; 
        })
    }
    else{
        $('#templateList').append('<li style="text-align:center;">No Templates</li>');
    }
}
function showsms(editor){
    if(editor && editor.id && templates[editor.id]){
        currentTemplateId = editor.id;
        // document.getElementById("editTemplate").innerText = "Edit Template";
        // document.getElementById("deleteTemplate").style.display = "block";
        document.getElementById("selectedTemplate").innerText = templates[editor.id].n;
        document.getElementById("tooltiptext").innerText = templates[editor.id].n;
        // document.getElementById("templateName").value = templates[editor.id].n;
        document.getElementById("emailContentEmail").innerText = templates[editor.id].m;
    }       
    else{
        currentTemplateId = null;
        // document.getElementById("editTemplate").innerText = "Save as Template";
        // document.getElementById("deleteTemplate").style.display = "none";
        document.getElementById("selectedTemplate").innerText = "Choose Template";
        document.getElementById("tooltiptext").innerText = "Choose Template";
        // document.getElementById("templateName").value = "";
        document.getElementById("emailContentEmail").innerText = "";
    }
}  
function setPhoneFields(){
    if(currentRecord){
        var NumberList= '';
        var selectedNumber='';
        fields.data.forEach(function(field){
            if(field.textDisplayHint == "phone_number" && field.groupName == "contactinformation" && currentRecord[field.name] && currentRecord[field.name].value){
                let no = currentRecord[field.name].value.replace(/\D/g,'');
                if(selectedNumber == ""){
                    selectedNumber = field.label+'('+no+')';
                }
                NumberList =NumberList+ '<li class="templateItem" onclick="setNumber(this)">'+field.label+'('+no+') </li>';
            }
        });
        if(selectedNumber){
            setNumber({"innerText":selectedNumber});
            $('#NumberList').append(NumberList); 
        }    
    }    
}
function setNumber(editor){
    var selectedNumber =editor.innerText.substring(editor.innerText.indexOf("(")+1,editor.innerText.indexOf(")"));
    if(selectedNumber){
        selectedNumber = selectedNumber.replace(/\D/g,'');
        checkPhoneNumber(selectedNumber);
    }   
    document.getElementById("selectedNumber").innerText =  editor.innerText;
    document.getElementById("numbertooltiptext").innerText = editor.innerText;
}
function checkPhoneNumber(no,isFromSend){
    return fetch("https://us-central1-ulgebra-license.cloudfunctions.net/msbrequest", {
        "method" :"POST",
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({
            "url":"https://rest.messagebird.com/lookup/" + no,
            "method": "GET",
            "headers":{
                "Authorization":"AccessKey 7NMPor0R8DofSHH61SpViNNqQ"
            }
        })
    })
    .then(function (response) {  
        console.log(response);
        return response.json();
    })
    .then(function (data) {
        console.log(data);
        if(data && data.data && data.data.countryPrefix){
            if(isFromSend)
            {
                return true;
            }
            else{
                $("#countryCode").val(data.data.countryPrefix);
                $("#editNumber").val(data.data.phoneNumber.toString().substring(data.data.countryPrefix.toString().length));
            }    
        }
        else{
            if(isFromSend)
            {
                return false;
            }
            else{
                $("#editNumber").val(no);
            }
        }
    })
    .catch(function (error) {
        console.log("Error: " + error);
        return false;
    });
} 
function setFields(fields){
    if(fields && fields.data){
        fields.data.forEach(function(field){
            if(field.groupName == "contactinformation" && field.formField == true){
                addListItem("dropdown-menu-email",field.label,"dropdown-item","Contacts."+field.label);
            }
        }); 
    }    
}
function addListItem(id,text,className,value){
    if(className == "dropdown-item"){
        var linode = '<li class="'+className+'"><button class="'+className+'" onclick="insert(this)">'+text+'<input type="hidden" value="'+value+'"></button></li>';
    }
    else{
        var linode = '<li class="'+className+'">'+text+'</li>';
    }
    $('#'+id).append(linode);
}
function insert(bookingLink){
    // var bookingLink = this;
    var range;

    if (sel && sel.rangeCount && isDescendant(sel.focusNode)){
        range = sel.getRangeAt(0);
        range.collapse(true);
        var span = document.createElement("span");
        span.appendChild( document.createTextNode('${'+bookingLink.children[0].value+'}') );
        range.insertNode(span);
        range.setStartAfter(span);
        range.collapse(true);
        sel.removeAllRanges();
        sel.addRange(range);
    }    
}
function isDescendant(child) {
    var parent = document.getElementById("emailContentEmail");
     var node = child.parentNode;
     while (node != null) {
         if (node == parent || child == parent) {
             return true;
         }
         node = node.parentNode;
     }
     return false;
}
function openlink(){
    sel = window.getSelection();
    if (sel && sel.rangeCount) {
        range = sel.getRangeAt(0);
    }  
}     
function getContact(contactId){
    var apiData =JSON.stringify({
        "url":"https://api.hubapi.com/contacts/v1/contact/vid/"+contactId+"/profile",
        "method": "GET",
        "extension":extensionName
    }) ;
    return makeHubspotcall(apiData).then(function(resp){
        console.log(resp);
        return resp;
    })
}
function getContactFields(){
    var apiData =JSON.stringify({
        "url":"https://api.hubapi.com/properties/v1/contacts/properties",
        "method": "GET",
        "extension":extensionName
    }) ;
    return makeHubspotcall(apiData).then(function(resp){
        console.log(resp);
        return resp;
    })
}
function addHistory(data){
    var body={ 
        "eventTemplateId":"1065431",
        "objectId":contactId,
        "tokens": data
    }
    var apiData =JSON.stringify({
        "url":"https://api.hubapi.com/crm/v3/timeline/events",
        "method": "POST",
        "data":body,
        "extension":extensionName
    }) ;
    return makeHubspotcall(apiData).then(function(resp){
        console.log(resp);
        return resp;
    })
}
// function addTimeLineEvents(){
    
// }
function makeHubspotcall(data){
    var makeHubspotHTTPCall = firebase.functions().httpsCallable('makeHubspotHTTPCall');
    return makeHubspotHTTPCall(data).then(function(result) {
      // Read result of the Cloud Function.
      return result;
    }).catch(function(error) {
      // Getting the Error details.
      var code = error.code;
      var message = error.message;
      var details = error.details;
      console.log(error)
      return null;
      // ...
    });

}


var msgType = 'text';
async function sendSMS(){
    var Mobile = document.getElementById("editNumber").value;
    Mobile = Mobile.replace(/\D/g,'');
    var countryCode = $("#countryCode").val();
    var senderid =$("#senderid").val();
    var accountId = $("#account").val();
    if(countryCode){
        Mobile=countryCode+""+Mobile;
        document.getElementById("ErrorText").innerText = "checking phoneNumber... ";
        document.getElementById("Error").style.display= "block";
        var isValidMobile = await checkPhoneNumber(Mobile,true);
        if(!isValidMobile){
            document.getElementById("ErrorText").innerText = "Invalid PhoneNumber.";
            setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
            return ;
        }
    }
    else{
        document.getElementById("ErrorText").innerText = "Please Select countryCode.";
        document.getElementById("Error").style.display= "block";
        setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
        return ;
    }
    if(document.getElementById("emailContentEmail").innerText.length >153){
        document.getElementById("ErrorText").innerText = "Message should be within 153 characters.";
        document.getElementById("Error").style.display= "block";
        document.getElementById("Error").style.color="red";
        setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
    }
    else if(document.getElementById("emailContentEmail").innerText.replace(/\n/g,"").replace(/\t/g,"").replace(/ /g,"") == ""){
        document.getElementById("ErrorText").innerText = "Message cannot be empty.";
        document.getElementById("Error").style.display= "block";
        setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
    }
    else if((Mobile == null || Mobile == "")){
        document.getElementById("ErrorText").innerText = "Mobile field is empty.";
        document.getElementById("Error").style.display= "block";
        setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
    }
    else if(!senderid){
        document.getElementById("ErrorText").innerText = "Please Select SenderId.";
        document.getElementById("Error").style.display= "block";
        setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
        return ;
    }
    else if(!accountId){
        document.getElementById("ErrorText").innerText = "Please Select Account.";
        document.getElementById("Error").style.display= "block";
        setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
        return ;
    }
    else{
        var domain = document.getElementById('account')[document.getElementById('account').selectedIndex].innerHTML;
        domain = domain.trim();
        document.getElementById("ErrorText").innerText = "Sending... ";
        document.getElementById("Error").style.display= "block";
        var message = document.getElementById("emailContentEmail").innerText;
        message = decodeURIComponent(encodeURIComponent(message).replaceAll("%C2%A0","%20"));
        // message =message.replace(/<b>/g, "*").replace(/<\/b>/g, "*").replace(/<strike>/g, "~").replace(/<\/strike>/g, "~");
        var arguments = {
            "action": "sendSMS",    
            "message" : message,
            "recordModule" : recordModule,
            "recordId" : contactId,
            "to":Mobile,
           
        };

        message = getMessageWithFields(arguments);
        var req_data={"direction":"sent","sender_phone":senderid,"receiver_phone":Mobile,"status":"sent","message":message};
        
        // let apiForm = new FormData();
        // apiForm.append('authkey', credential.apikey);
        // apiForm.append('mobiles', to);
        // apiForm.append('message', filledMessage);
        // apiForm.append('sender', credential.brandName);
        // apiForm.append('route', '4');
        var request = {
            "url":'https://'+domain+'//httpapi/QueryStringReceiver?ver=1.0&key=${apikey}&dest='+Mobile+'&send='+senderid+'&text='+encodeURIComponent(message)+'&type=PM',
            "method": 'GET',
            "extension":extensionName,
            "appName":accountId
        };
    


        var apiData =JSON.stringify(request) ;
        var resp = await makeAppcall(apiData).then(function(resp){
            console.log(resp);
            return resp.data?resp.data.toString():null;
        });
        // Request accepted for Request ID=2086717926489511600 & Statuscode=200 & Info=Platform Accepted  & Time=2021-07-08 11:26:48
        // Empty mobile number for Request ID=N/A & Statuscode=-113 & Info=REJECTED  & Time=2021-07-08 11:26:11
        // Invalid Credentials for Request ID=N/A & Statuscode=-108 & Info=REJECTED  & Time=2021-07-08 11:23:05
        var messageId = "";
        if(resp && resp.includes('Statuscode=200') && resp.indexOf('Request ID=N/A') == -1) {
            messageId= resp.substring(resp.indexOf("Request ID=")).split(" &")[0].split("=")[1];
            var history = {"platform":"SMS","messageId":messageId,"direction":"sent","sender_phone":senderid,"receiver_phone":Mobile,"status":"sent","message":message};
            addHistory(history).then(function(response){
                var responseInfo  = response.data;
                if(responseInfo && responseInfo.id){
                    document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your Message has been sent successfully.</div>';
                    document.getElementById("Error").style.display= "block";
                }
                else{
                    document.getElementById("ErrorText").innerText = "Opps! Something went wrong from server side. Please try after sometimes!!!";
                    document.getElementById("Error").style.display= "block";
                    setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 3000);
                }
            });
        }
        else if(resp){
            document.getElementById("ErrorText").innerText = resp.substring(0, resp.indexOf(" ID="));
            document.getElementById("Error").style.display= "block";
            setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 3000);
            return;
        }
        else{
            document.getElementById("ErrorText").innerText ="Something went wrong, please try after sometime";
            document.getElementById("Error").style.display= "block";
            setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 3000);
            return;
        }
    }       
}

function makeAppcall(data){
    var makeAppCall = firebase.functions().httpsCallable('makeAppCall');
    return makeAppCall(data).then(function(result) {
      // Read result of the Cloud Function.
      return result.data;
    }).catch(function(error) {
      // Getting the Error details.
      var code = error.code;
      var message = error.message;
      var details = error.details;
      console.log(error)
      return null;
      // ...
    });

}

function getSanitizedContent(msg){
    return encodeURIComponent(msg);
    // if(extensionVersion){
    //     $("body").append(`<img style="display:none" src="https://us-central1-slackappp-39d3d.cloudfunctions.net/zdeskww?debug=wawebext-single::cx-mail:${userEmail}:::-cexv:${extensionVersion.version}::" />`);
    // }
    // if(extensionVersion && parseFloat(extensionVersion.version) > 8){
    //     console.log('extension version is > 8');
    //     return encodeURIComponent(msg);
    // }
    // else{
    //     console.log('extension version is not > 8');
    //     return msg;
    // }
}
function getMessageWithFields(messageDetails){
    var message = messageDetails.message;
    var customerData=[];
    
    fields.data.forEach(function(field){
        try{
            var replace = "\\${Contacts."+field.label+"}";
            var re = new RegExp(replace,"g");
            if(currentRecord[field.name] != null)
            {
                var value = currentRecord[field.name].value;
                if(value.name)
                {
                    value = value.name;
                }
                message = message.replace(re,value);
            }
            else
            {
                message = message.toString().replace(re," ");
            }
        }catch(err){
            loadDebugInfo(":unable-to-replace-plc:"+err);
        }
    }); 
    return message;
}
function showTemplateOption(checker){
    if(checker.checked){
        document.getElementById("templateOption").style.display="inline-block";
    }
    else{
        document.getElementById("templateOption").style.display="none";
    }
}
async function deleteTemplate(){
    if(firebase.auth().currentUser){
        document.getElementById("ErrorText").innerText = "deleteting... ";
        document.getElementById("Error").style.display= "block";
        let userId =firebase.auth().currentUser.uid;
        if(currentTemplateId){
            await db.collection("hubspotSettings").doc(userId).collection('karixTemplates').doc(currentTemplateId).delete();
            currentTemplateId = null;
            showsms();
            $('#templateList').html('');
            setTemplates();
        }
        document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Template has been deleted successfully.</div>';
        setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
    }    
}  

var currentTemplateId;
async function saveTemplate(){
    var message = document.getElementById("emailContentEmail").innerText;
    var templateName = document.getElementById("templateName").value;
    if(!templateName || templateName.trim() == "" ){
        document.getElementById("ErrorText").innerText = "Template Name cannot be empty.";
        document.getElementById("Error").style.display= "block";
        document.getElementById("Error").style.color="red";
        setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
    }
    else if(message.length >2000){
        document.getElementById("ErrorText").innerText = "Message should be within 2000 characters.";
        document.getElementById("Error").style.display= "block";
        document.getElementById("Error").style.color="red";
        setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
    }
    else if(message.replace(/\n/g,"").replace(/\t/g,"").replace(/ /g,"") == ""){
        document.getElementById("ErrorText").innerText = "Message cannot be empty.";
        document.getElementById("Error").style.display= "block";
        setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
    }
    else{
        if(firebase.auth().currentUser){
            document.getElementById("ErrorText").innerText = "Saving... ";
            document.getElementById("Error").style.display= "block";
            let userId =firebase.auth().currentUser.uid;
            if(currentTemplateId){
                await db.collection("hubspotSettings").doc(userId).collection('karixTemplates').doc(currentTemplateId).set({m:message,n:templateName});
            }
            else{
                await db.collection("hubspotSettings").doc(userId).collection('karixTemplates').add({m:message,n:templateName});
            }
            document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Template has been saved successfully.</div>';
            setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
        }    
    }
}

function showForcedAlert(){
            $('body').append(`<div id="announcetip" class="bottomTip" ><div class="bottomTipActions"><a href="https://apps.ulgebra.com/hubspot/whatsapp-web/whatsapp-web-for-hubspot" target="_blank"> Upgrade Plan </a>  <a style="margin:0px 5px" href="https://wa.me/917397415648?text=Hello%20WhatCetra,%20I%20have%20a%20query" target="_blank">Contact Us</a> </div>
                    <span onclick="$('#announcetip').hide()" class="tt-close">X</span>
                </div>`);
}           
function loadDebugInfo(txt){
    $("body").append(`<img style="display:none" src="https://us-central1-slackappp-39d3d.cloudfunctions.net/zdeskww?debug=wawebext-bulk::cx-mail:${userEmail}:::-cexv:${encodeURIComponent(txt)}::" />`)
}