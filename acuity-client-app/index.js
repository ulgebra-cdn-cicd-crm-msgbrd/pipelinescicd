var currentUser = null;
var calendarId = null;
window.onload = (function () {
    const urlParams = new URLSearchParams(window.location.search);
    calendarId = urlParams.get('calendarId');
    if(!calendarId){
        alert("Please calendarId in url eg. (https://<url>?calendarId=<your_acuity_calendar_id>)");
    }
    const firebaseConfig = {
        apiKey: "AIzaSyDDs3mryd1hXgNS3Uson9SxL2EuzlCMw5A",
        authDomain: "ivonnetr1b1display.firebaseapp.com",
        databaseURL: "https://ivonnetr1b1display-default-rtdb.firebaseio.com",
        projectId: "ivonnetr1b1display",
        storageBucket: "ivonnetr1b1display.appspot.com",
        messagingSenderId: "14784830802",
        appId: "1:14784830802:web:fe88a5c795d76fbf566e8b",
        measurementId: "G-PZRXZMKJ1H"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    db = firebase.firestore();
    var uiConfig = {
        callbacks: {
            // Called when the user has been successfully signed in.
            'signInSuccessWithAuthResult': function (authResult, redirectUrl) {
                if (currentUser == null && authResult.user) {
                     handleauthoriztion();
                }
                return false;
            },
            uiShown: function () {
                $(".loader").hide();
            }
        },
        'credentialHelper': firebaseui.auth.CredentialHelper.NONE,
        signInFlow: 'popup',
        signInOptions: [
            {
                "provider": firebase.auth.EmailAuthProvider.PROVIDER_ID
            },
            {
                provider: firebase.auth.GoogleAuthProvider.PROVIDER_ID,
                clientId: '14784830802-4v3u9afal7meso8u22160f1hi9mad1li.apps.googleusercontent.com'
            }
        ],
        // credentialHelper: firebaseui.auth.CredentialHelper.GOOGLE_YOLO,
        tosUrl: 'https://apps.ulgebra.com/terms',
        privacyPolicyUrl: 'https://apps.ulgebra.com/privacy-policy'

    };

    var fbaseauth = firebase.auth();
    fbaseauth.onAuthStateChanged(function (user) {
         handleauthoriztion();
    });
    fbaseauth.setPersistence(firebase.auth.Auth.Persistence.SESSION);
    uiConfig.credentialHelper = firebaseui.auth.CredentialHelper.GOOGLE_YOLO;
    var ui = new firebaseui.auth.AuthUI(fbaseauth);
    ui.start('#firebaseui-auth-container', uiConfig);
});
var currentAppointmentId = null;
function handleauthoriztion(){
    if(firebase.auth().currentUser){
        $("#firebaseui-auth-container").hide();
        console.log("user signed in");
        $("#dashboard").show();
        if(calendarId){
            var starCountRef = firebase.database().ref(`/acuity/${calendarId}/currentAppointment`);
            starCountRef.on('value', (snapshot) => {
              const data = snapshot.val();
                if(data.action == "startMeeting"){
                    currentAppointmentId = data.appointmentId;
                    showAppointment(data.appointmentId);
                }
                else if(data.action == "endMeeting" && currentAppointmentId == data.appointmentId){
                    currentAppointmentId = null;
                    $("#ivonneMainLogoLarge").show();
                    $("#apmt").hide();
                }
            });
        }
        else{
            alert("Please calendarId in url eg. (https://<url>?calendarId=3333333)");
        }    
    }
    else{
        console.log("user not signed in");
        $("#dashboard").hide();
    }
}
async function showAppointment(appointmentId){
    var getAcuityAppointment = firebase.functions().httpsCallable('getAcuityAppointment');
    var apmt = await getAcuityAppointment(JSON.stringify({"appointmentId":appointmentId})).then(function(result) {
        if(result && result.data){
            if(result.data == "user_not_allowed"){
                alert("You don't have permission to view appointments.");
                return null;
            }   
            return result.data;
        }
        return null;
    }).catch(function(error) {
      // Getting the Error details.
      var code = error.code;
      var message = error.message;
      var details = error.details;
      console.log(error)
      return null;
    });
    if(apmt && apmt.calendar){
        $("#ivonneMainLogoLarge").hide();
        $("#apmt").show();
        $("#name").text(apmt.firstName+" "+apmt.lastName);
        $("#email").text(apmt.email);
        $("#phone").text(apmt.phone);
        $("#calendar").text(apmt.calendar);
        $("#type").text(apmt.type);
        $("#todayDate").text(new Date().toLocaleDateString());
        var fields = [];
        apmt.forms.forEach(function(form){
            if(form && form.values){
                fields = fields.concat(form.values);
            }
        });
        console.log(fields);
        fields.forEach(function(field){
            if($("#field_"+field.fieldID).length && field.value){
                if(field.fieldID == 10383954 || field.fieldID == 10383955){
                    $("#field_"+field.fieldID).attr("src",field.value);
                    $("#field_"+field.fieldID).show();
                }
                else if(field.fieldID == 9018339 || field.fieldID == 9018362){
                    $("#field_"+field.fieldID).text(field.value);

                    if(field.value.toLowerCase() == "no"){
                        $("#field_"+field.fieldID).css("color", 'rgb(255,0,0)');
                    }
                    else{
                        $("#field_"+field.fieldID).css("color", 'rgb(60,179,113)');
                    }
                }
                else if(field.fieldID == 9018883){
                    if(field.value.indexOf("Trying to get pregnant") != -1){
                        $("#field_"+field.fieldID+"_trying").css("color", 'rgb(255,0,0)');
                    }
                    if(field.value.indexOf("Currently pregnant") != -1){
                        $("#field_"+field.fieldID+"_pregnant").css("color", 'rgb(255,0,0)');
                    }
                    if(field.value.indexOf("Nursing") != -1){
                        $("#field_"+field.fieldID+"_nursing").css("color", 'rgb(255,0,0)');
                    }
                }
                else{
                    $("#field_"+field.fieldID).text(field.value);
                }
            }
        })
    }
}