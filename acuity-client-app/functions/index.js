const functions = require("firebase-functions");
const admin = require('firebase-admin');
const axios = require('axios');
const crypto = require("crypto");
const { CloudTasksClient } = require('@google-cloud/tasks')
admin.initializeApp()

exports.acuityWebhook = functions.https.onRequest(async(req, res) => {
	try{
		let db = admin.firestore();
		console.log(JSON.stringify(req.body));
		var isCalendarAllowed = await db.collection('AllowedCalendars').doc(req.body.calendarID+"").get().then(function(docRef){
			if(docRef.exists && docRef.data().allowed === true){
				return true;
			}
			return false;
		});
		console.log("isCalendarAllowed");
		console.log(isCalendarAllowed);
		if(req.body && isCalendarAllowed){
			console.log(JSON.stringify(req.headers));
			if(req.headers["x-acuity-signature"]){
				console.log("verification");
				var hash = crypto.createHmac('sha256',"9fa69e381563aa6903df2f32099f66ca").update(req.rawBody.toString()).digest("base64");
				console.log(hash);
				if (hash !== req.headers['x-acuity-signature']) {
					console.log("authentication_error");
					return res.status(200).send("authentication_error");
				}
			}
			if(req.method === 'POST'){
				const project = 'ivonnetr1b1display'; 
				const location = 'northamerica-northeast1';
				const queue = 'acuity-display';
				const tasksClient = new CloudTasksClient();
				const parent = tasksClient.queuePath(project, location, queue);
				var appointmentId = req.body.id;
				var action = req.body.action;
            	var base64Token = Buffer.from("21015249:9fa69e381563aa6903df2f32099f66ca").toString('base64');
				const headers = {
				      Authorization: "Basic "+base64Token,
				      'Content-Type': 'application/json'
				};
		        const result = await axios.get('https://acuityscheduling.com/api/v1/appointments/'+appointmentId, {
			    	headers: headers
			    });
			    if(result && result.data && result.data.calendarID){
			    	console.log("appointment retrieved successfully.");
			    }
			    else{
			    	console.log(JSON.stringify(result));
			    }
				var appointment = result.data;
				var startTime = new Date(appointment.datetime).getTime();
				var endTime = new Date(appointment.datetime).getTime()+appointment.duration*60000;
				var inSeconds = new Date(appointment.datetime).getTime()/1000 - Date.now()/1000;
				var endTaskSeconds = inSeconds + (appointment.duration*60);
				const payload = appointmentId;
				if(action === "appointment.rescheduled" || action === "appointment.canceled"){
					var endTaskName = null;
			    	var taskName = await db.collection("AcuityAppointments").doc(appointmentId+"").get().then(function(docRef){
			    		if(docRef.exists && docRef.data() && docRef.data().taskName){
			    			endTaskName = docRef.data().endTaskName;
			    			return docRef.data().taskName;
			    		}
			    		return null;
			    	})
			    	if(taskName){
			    		try{
				    		const deleteRequest = {
								name: taskName
							};
				    		const [deleteResponse] = await tasksClient.deleteTask(deleteRequest);
				    	    console.log("deleteResponse");
				    		console.log(deleteResponse);
				        }
				        catch(e){
				        	console.log("error in deleting startTask")
				        	console.log(e);
				        }		
			        }
			        if(endTaskName){ 	
			            try{
				    		const deleteRequest2 = {
								name: endTaskName
							};
				    		const [deleteResponse2] = await tasksClient.deleteTask(deleteRequest2);
				    	    console.log("deleteResponse2");
				    		console.log(deleteResponse2);
				    	}
				    	catch(e){
				    		console.log("error in deleting endtask");
				    		console.log(e);
				    	}	
			    	}
			    	try{
			    		console.log(appointmentId +"is deleted");
			    		await db.collection("AcuityAppointments").doc(appointmentId+"").delete();
			    	}
			    	catch(e){
			    		console.log("delete error");
			    		console.log(e);
			    	}	
			    }
				if(action === "appointment.scheduled" || action === "appointment.rescheduled"){
					if(endTaskSeconds >= 2591700){
						console.log("time > 30days");
						return res.status(200).send('ok');
					}	
					const task = {
					    httpRequest: {
						  httpMethod: 'POST',
						  url: 'https://us-central1-ivonnetr1b1display.cloudfunctions.net/acuityCloudTaskHandler?action=startMeeting&appointmentId='+appointmentId+'&calendarID='+appointment.calendarID+'&accessToken='+base64Token+'&time='+startTime
						},
						scheduleTime: {
						  seconds: inSeconds + Date.now() / 1000
						}
					};
					const request = {
						parent: parent,
						task: task
					};
					console.log('Sending startTask:');
					console.log(JSON.stringify(task));
					const [response] = await tasksClient.createTask(request);
					const name = response.name;
					console.log(`Created task ${name}`);

					const endtask = {
					    httpRequest: {
						  httpMethod: 'POST',
						  url: 'https://us-central1-ivonnetr1b1display.cloudfunctions.net/acuityCloudTaskHandler?action=endMeeting&appointmentId='+appointmentId+'&calendarID='+appointment.calendarID+'&accessToken='+base64Token+'&time='+endTime
						},
						scheduleTime: {
						  seconds: endTaskSeconds + Date.now() / 1000
						}
					};
					const request2 = {
						parent: parent,
						task: endtask
					};
					console.log('Sending endTask:');
					console.log(JSON.stringify(endtask));
					const [response2] = await tasksClient.createTask(request2);
					const name2 = response2.name;
					console.log(`Created task ${name2}`);
					await db.collection("AcuityAppointments").doc(appointmentId+"").set({"status":action.substring(12),"calendarID":appointment.calendarID,"taskName":name,"endTaskName":name2,"ct": Date.now()},{merge : true});
			    }
				return res.status(200).send('ok');
		    }		
			else{
				return res.status(403).send('Forbidden!');
			}
	    }	
	    return res.status(200).send('ok');	
	}	
    catch(e){
    	console.log("acuityWebhook_error")
    	console.log(e);
    	return res.status(200).send('ok');
    }		
});

exports.acuityCloudTaskHandler = functions.https.onRequest(async(req, res) => {
	try{
		console.log(JSON.stringify(req.query));
	    let base64Token = Buffer.from("21015249:9fa69e381563aa6903df2f32099f66ca").toString('base64');
		if(req.query && req.query.accessToken && req.query.accessToken === base64Token){
			var dbPath = `/acuity/${req.query.calendarID}/currentAppointment`;
			if(req.query.action === "endMeeting"){
				var currentMeetingId = await admin.database().ref(dbPath).get().then((snapshot) => {
					if(snapshot.exists()){
						return snapshot.val().appointmentId;
					} 
					return null;
				});
				console.log(currentMeetingId);
				if(currentMeetingId && (currentMeetingId !== req.query.appointmentId)){
					return res.status(200).send('ok');
				}
			}
			await admin.database().ref(dbPath).set({"appointmentId":req.query.appointmentId,"action":req.query.action,"time":req.query.time});
		}
		else{
			console.log("accessToken_not_found");
		}	
	   	return res.status(200).send('ok');
	}
	catch(e){
		console.log("acuityCloudTaskHandler_error");
		console.log(e);
    	return res.status(200).send('ok');
	}
});	
exports.dailySchedular = functions.runWith({timeoutSeconds:540,memory:"1GB"}).https.onRequest(async(req, res) => {
	try{
		let db = admin.firestore();
		console.log(JSON.stringify(req.query));
	    var base64Token = Buffer.from("21015249:9fa69e381563aa6903df2f32099f66ca").toString('base64');
		if(req.query && req.query.accessToken && req.query.accessToken === base64Token){
			const project = 'ivonnetr1b1display'; 
			const location = 'northamerica-northeast1';
			const queue = 'acuity-display';
			const tasksClient = new CloudTasksClient();
			const parent = tasksClient.queuePath(project, location, queue);
			if(req.query && req.query.action === "dailySchedular"){
				try{
					var nextDaySeconds = 86400;
					
					const task = {
					    httpRequest: {
						  httpMethod: 'POST',
						  url: 'https://us-central1-ivonnetr1b1display.cloudfunctions.net/dailySchedular?action=dailySchedular&calendarID='+req.query.calendarID+'&accessToken='+base64Token
						},
						scheduleTime: {
						  seconds: nextDaySeconds + Date.now() / 1000
						}
					};
					const request = {
						parent: parent,
						task: task
					};
					console.log('Sending dailyTask:');
					console.log(JSON.stringify(task));
					const [response] = await tasksClient.createTask(request);
					const name = response.name;
					console.log(`Created task ${name}`);
				}
				catch(e){
					console.log("error in dailyTask add");
					console.log(e);
				}
				var today = new Date();
				var tommorrow = new Date(today.setDate(today.getDate()+1));
				var minDate = tommorrow.toISOString().substring(0,10);
				console.log(minDate);
				const headers = {
				      Authorization: "Basic "+base64Token,
				      'Content-Type': 'application/json'
				};
		        const result = await axios.get('https://acuityscheduling.com/api/v1/appointments?max=50&calendarID='+req.query.calendarID+'&maxDate='+encodeURIComponent(minDate)+'&minDate='+encodeURIComponent(minDate)+'&excludeForms=true', {
			    	headers: headers
			    });
			    // console.log("appmt");
			    // console.log(result);
			    // console.log(result.data);
			    var appointments = [];
			    if(result && result.data && result.data){
			    	appointments = result.data;
			    	console.log("appointments retrieved successfully.");
			    	console.log("total appointments "+appointments.length);
			    }
			    /* eslint-disable no-await-in-loop */
			    for(let i=0; i<appointments.length;i++){
			    	try{
						var isTaskAdded = await db.collection("AcuityAppointments").doc(appointments[i].id+"").get().then(function(doc){
							if(doc.exists){
								return true;
							}
							return false;
						})
						console.log(isTaskAdded +" : "+appointments[i].id);
						if(!isTaskAdded){
					    	var appointment = appointments[i];
				    		var startTime = new Date(appointment.datetime).getTime();
							var endTime = new Date(appointment.datetime).getTime()+appointment.duration*60000;
							var inSeconds = new Date(appointment.datetime).getTime()/1000 - Date.now()/1000;
							var endTaskSeconds = inSeconds + (appointment.duration*60);
				    		var startTask = {
							    httpRequest: {
								  httpMethod: 'POST',
								  url: 'https://us-central1-ivonnetr1b1display.cloudfunctions.net/acuityCloudTaskHandler?action=startMeeting&appointmentId='+appointment.id+'&calendarID='+appointment.calendarID+'&accessToken='+base64Token+'&time='+startTime
								},
								scheduleTime: {
								  seconds: inSeconds + Date.now() / 1000
								}
							};
							var request = {
								parent: parent,
								task: startTask
							};
							console.log('Sending startTask:');
							console.log(JSON.stringify(startTask));
							var name;
							try{
								const [response] = await tasksClient.createTask(request);
								name = response.name;
								console.log(`Created startTask ${name}`);
							}
							catch(e){
								console.log(e);
								console.log("error in startTask");
							}	

							var endtask = {
							    httpRequest: {
								  httpMethod: 'POST',
								  url: 'https://us-central1-ivonnetr1b1display.cloudfunctions.net/acuityCloudTaskHandler?action=endMeeting&appointmentId='+appointment.id+'&calendarID='+appointment.calendarID+'&accessToken='+base64Token+'&time='+endTime
								},
								scheduleTime: {
								  seconds: endTaskSeconds + Date.now() / 1000
								}
							};
							var request2 = {
								parent: parent,
								task: endtask
							};
							console.log('Sending endTask:');
							console.log(JSON.stringify(endtask));
							var name2;
							try{
								const [response2] = await tasksClient.createTask(request2);
								name2 = response2.name;
								console.log(`Created task ${name2}`);
							}
							catch(e){
								console.log(e);
								console.log("error in adding endTask");
							}	
							await db.collection("AcuityAppointments").doc(appointments[i].id+"").set({"status":"scheduled","calendarID":appointment.calendarID,"taskName":name,"endTaskName":name2,"ct": Date.now(),"createdBy":"dailySchedular"},{merge : true});
						}	
					}
					catch(e){
						console.log(e);
						console.log("error for appointment : "+appointments[i].id);
					}	
			    }	
			    /* eslint-enable no-await-in-loop */
			}
		}
		else if(req.query && req.query.action === "addInitTask" && req.query.access === "saen"){
			const project = 'ivonnetr1b1display'; 
			const location = 'northamerica-northeast1';
			const queue = 'acuity-display';
			const tasksClient = new CloudTasksClient();
			const parent = tasksClient.queuePath(project, location, queue);
			let base64Token = Buffer.from("21015249:9fa69e381563aa6903df2f32099f66ca").toString('base64');
			const newtask = {
			    httpRequest: {
				  httpMethod: 'POST',
				  url: 'https://us-central1-ivonnetr1b1display.cloudfunctions.net/dailySchedular?action=dailySchedular&calendarID='+req.query.calendarID+'&accessToken='+base64Token
				},
				scheduleTime: {
				  seconds: 60 + Date.now() / 1000
				}
			};
			const request = {
				parent: parent,
				task: newtask
			};
			console.log('Sending dailyTask:');
			console.log(JSON.stringify(newtask));
			const [response] = await tasksClient.createTask(request);
			const name = response.name;
			console.log(`Created task ${name}`);
		}
		else{
			console.log("accessToken_not_found");
		}	
	   	return res.status(200).send('ok');
	}
	catch(e){
		console.log("acuityCloudTaskHandler_error");
		console.log(e);
    	return res.status(200).send('ok');
	}
});	

exports.getAcuityAppointment = functions.https.onCall(async(data, context) => {
	try {
        console.log(data);
        let db = admin.firestore();
        var isUserAllowed = await db.collection("users").doc(context.auth.uid).get().then(function(docRef){
        	if(docRef.exists && docRef.data().allowed === true){
        		return true;
        	}
        	return false;
        })
        if(!isUserAllowed){
        	console.log("user_not_allowed");
        	console.log(context.auth.uid);
        	return "user_not_allowed";
        }
        data = JSON.parse(data);
        var base64Token = Buffer.from("21015249:9fa69e381563aa6903df2f32099f66ca").toString('base64');
		const headers = {
		      Authorization: "Basic "+base64Token,
		      'Content-Type': 'application/json'
		};
        const result = await axios.get('https://acuityscheduling.com/api/v1/appointments/'+data.appointmentId, {
	      headers: headers
	    });
	    if(result && result.data && result.data.calendarID){
	    	console.log("appointment notified for userId : "+context.auth.uid+" from calendarID : "+result.data.calendarID);
	    }
	    else{
	    	console.log("userId : "+context.auth.uid);
	    	console.log(JSON.stringify(result));
	    }
		return result.data;
    }
    catch (exe) {
        console.log("error");
        console.log(JSON.stringify(exe));
        return "error";
    }
});
