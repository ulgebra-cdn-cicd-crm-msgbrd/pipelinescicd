 // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional

var portalId=null;
let allAuthorized = false; 

let createdAllCredentials = 0;
let deletedAllCredentials = 0;
// var UALIC_APP_PRODUCT_ID = 'prod_IsDzgkJSMnJ7H3';
var extensionName = 'pingbixforhubspotcrm';


async function uninstall() {
        var user = firebase.auth().currentUser;
        if (user) {
            var yesAgree = confirm("\n\n Are you sure you want to disable integration?");
            if(yesAgree){
                showProcess(`Removing authorizations ...`, 1);
                var user = firebase.auth().currentUser;
                if(user){
                    await deletePingbixWebhook();
                    var uninstallApp = firebase.functions().httpsCallable('unInstallApp');
                    uninstallApp({"extensionName":extensionName,"service":"pingbix","authKeyword":"isPingbixAuth","isNew":true}).then(function(result) {
                        processCompleted(1);
                        window.location.reload();
                    });
                }
            }
        }
}
  

  function openHubspot(){
    var user = firebase.auth().currentUser;
    if(user){
        var hubspoturl = "https://app-eu1.hubspot.com/oauth/authorize?client_id=38c3392f-57ec-4d92-b947-2ff658cc61f4&redirect_uri=https://us-central1-pingbix-sms-plugin-hubspot.cloudfunctions.net/oauthCallbackHubspot&scope=automation%20timeline%20oauth%20crm.lists.read%20crm.objects.contacts.read%20crm.objects.contacts.write%20crm.objects.companies.write%20crm.lists.write%20crm.objects.companies.read%20crm.objects.deals.read%20crm.objects.deals.write%20crm.objects.owners.read&state="+extensionName+"-"+user.uid;
         window.open(hubspoturl,"_self");
    }
    else{
      alert("Please sign-in");
    }    
  } 
  


async function saveAPIKey() {
    if(!isHubAuth){
        alert("Please Authorize HubSpot first.");
        return;
    }
    var user = firebase.auth().currentUser;
    let username = $("#username").val();
    let password = $("#password").val();
    let senderid = $("#senderid").val();

    
    if(!valueExists(username) || !valueExists(password)){
        showErroMessage("Please fill all the credentials");
        return;
    }
    if (user) {
        createdAllCredentials = 0;
        var credAdProcess1 = curId++;
        showProcess(`Adding credentials (1)...`, credAdProcess1);
        var handleActions = firebase.functions().httpsCallable('handleActions');
        handleActions({"action":"connectApp","appName":"pingbix","credentials":{"username": username.trim(),"senderid": senderid.trim(),"password":password.trim()},"settings":{"isPingbixAuth":true,"senderid":senderid.trim()},"extensionName":extensionName}).then(async function(result) {
            console.log(result);
            await addPingbixWebhook();
            processCompleted(credAdProcess1);
            handleauthoriztion();
        });
    }
    else {
        alert("Please sign-in");
    }
}
function makeAppcall(data){
    data.appName = "pingbix";
    data.extension= extensionName;
    data = JSON.stringify(data);
    var makeAppCall = firebase.functions().httpsCallable('makeAppCall');
    return makeAppCall(data).then(function(result) {
      // Read result of the Cloud Function.
      return result.data;
    }).catch(function(error) {
      // Getting the Error details.
      var code = error.code;
      var message = error.message;
      var details = error.details;
      console.log(error)
      return null;
      // ...
    });

}
//{"response":{"api":"webhook","action":"delete","status":"error","msg":"Webhook does not exists.","code":"238"}}
//{"response":{"api":"webhook","action":"create","status":"error","msg":"Webook is already added in your account.","code":"236"}}
async function addPingbixWebhook(){
    try{
        var webhookUrl = "https://us-central1-pingbix-sms-plugin-hubspot.cloudfunctions.net/pingbixCallback?userId="+firebase.auth().currentUser.uid+"&portalId="+portalId+"&tId=$transactionId&msgId=$messageId&errorCode=$errorCode&doneTime=$doneDate&receivedTime=$receivedTime&mobile=$mobile";
        var apiData ={
            "url":"https://app.pingbix.com/SMSApi/webhook/create?userid=${username}&password=${password}&smswebhook="+encodeURIComponent(webhookUrl)+"&smswebhookrate=10&smswebhookmethod=GET&output=json",
            "method": "GET"
        };
        var resp = await makeAppcall(apiData).then(function(resp){
            console.log(resp);
            return resp;
        });
    }
    catch(e){
        console.log(e);
        return;
    }    
}
//{"response":{"api":"webhook","action":"delete","status":"success","msg":"Webhook deleted successfully.","code":"200"}}
//{"response":{"api":"webhook","action":"delete","status":"error","msg":"Webhook does not exists.","code":"238"}}
async function deletePingbixWebhook(){
    await makeAppcall({"url":"https://app.pingbix.com/SMSApi/webhook/delete?userid=${username}&password=${password}&output=json","method":"GET"});
}
function reloadIfAllCredsCompleted(){
    if(createdAllCredentials === 3){
        handleauthoriztion();
    }
}

function reloadIfAllCredsDeleted(){
    if(deletedAllCredentials === 3){
        window.location.reload();
    }
}
var isHubAuth = false;
async function handleauthoriztion() {
    $("#WTR-HUBS-AUTH").addClass('waitToResolve');
    $("#WTR-INTEG-STATUS").addClass('waitToResolve');
    var user = firebase.auth().currentUser;
    if (user) {
        // $("#reply_url").val("https://us-central1-ulgebra-license.cloudfunctions.net/clickatellCallback?action=reply&userId="+user.uid+"&apiKey=YOUR_CLICKATELL_API_KEY");
        // $("#status_url").val("https://us-central1-ulgebra-license.cloudfunctions.net/clickatellCallback?action=status&userId="+user.uid+"&apiKey=YOUR_CLICKATELL_API_KEY");
        $("#WTR-HUBS-AUTH").addClass('waitToResolve');
        if(!portalId){
            portalId = await db.collection("Users").doc(user.uid).get().then(function(doc) {
                if(doc.exists && doc.data().hptId){
                    return doc.data().hptId;
                }
                return null;
            });
        }
        if(portalId){
            var docRef = db.collection("hubspotOrgs").doc(portalId).collection('extensions').doc(extensionName);
            docRef.get().then(function (doc) {
              
                $("#WTR-ACUITY-AUTH").removeClass('waitToResolve');
                $("#WTR-INTEG-STATUS").removeClass('waitToResolve');
                $("#WTR-HUBS-AUTH").removeClass('waitToResolve');
                if (doc.exists) {
                    console.log("Document data:", doc.data());
                    var data = doc.data();
                    let zohoISConnected = data && data.isHubAuth;
                    let gotoISConnected = data && data.isPingbixAuth;

                    if (zohoISConnected) {
                        isHubAuth = true;
                        $("#hubspotbutton").html(`<span class="material-icons">check_circle</span> Authorized`).attr({'disabled': 'true'}).css({'background-color': 'green'});
                    }

                    if (gotoISConnected) {
                        $('#acuitybutton').html(`<span class="material-icons">check_circle</span> Authorized`).attr({'disabled': 'true'}).css({'background-color': 'green'});
                    }
                    else {
                        $('#acuitybutton').text("Authorise Now");
                    }

                    if (zohoISConnected && gotoISConnected) {
                        $("#twiliocreds").hide();
                        $("#INTEG-STATUS").text("Enabled").css({'background-color': '#06d506'});
                        $("#UNINSTALL-APP-BTN").show();
                        allAuthorized = true;
                    } else {
                        $("#WTR-PH-NUMS").removeClass('waitToResolve');
                        $("#INTEG-STATUS").text("Not Active").css({'background-color': 'grey'});
                        $("#UNINSTALL-APP-BTN").hide();
                    }
                    if(zohoISConnected || gotoISConnected){
                        $("#UNINSTALL-APP-BTN").show();
                    }
                }
                else {
                    $("#WTR-PH-NUMS").removeClass('waitToResolve');
                    $("#INTEG-STATUS").text("Not Active").css({'background-color': 'grey'});
                    $("#UNINSTALL-APP-BTN").hide();
                    console.log("No such document!");
                }

            }).catch(function (error) {
                console.log("Error getting document:", error);
            });
        }
        else{
            $("#WTR-ACUITY-AUTH").removeClass('waitToResolve');
            $("#WTR-INTEG-STATUS").removeClass('waitToResolve');
            $("#WTR-HUBS-AUTH").removeClass('waitToResolve');
            $("#WTR-PH-NUMS").removeClass('waitToResolve');
            $("#INTEG-STATUS").text("Not Active").css({'background-color': 'grey'});
            $("#UNINSTALL-APP-BTN").hide();
        }    
    }
}


function showReAUTHORIZEError() {
    showErroWindow('Kindly authorize the app again', '<div style="margin-top:-20px;">Go to <b style="color:royalblue">GENERAL SETTINGS</b> tab above. Then  click <b style="color:royalblue">Revoke</b> button and authroize again. Then visit this tab. Refer the image below. <br><br> <a target="_blank" title="Click to View Image" href="https://puplicfiles.s3.us-east-2.amazonaws.com/uhoioe8y28g79e/inconsistent_app_init.png"><img id="inconst_error_img" src="https://puplicfiles.s3.us-east-2.amazonaws.com/uhoioe8y28g79e/inconsistent_app_init.png" style="max-width:80%;max-height:320px"/></a>');
    $('.error-window-title').css({'margin-top': '-30px'});
}

function showAddPhoneDialog(){
    showErroWindow();
}


function removeElem(elem){
    $(elem).remove();
}

//   function addwebhook(){
//     var body={
//   "actionUrl": "https://us-central1-pingbix-sms-plugin-hubspot.cloudfunctions.net/hubspotWebhookAction?app=pingbixforhubspotcrm",
//   "published": true,
//   "inputFields": [
//     {
//       "typeDefinition": {
//         "name": "text",
//         "type": "string",
//         "fieldType": "textarea"
//       },
//       "supportedValueTypes": [
//         "STATIC_VALUE"
//       ],
//       "isRequired": true
//     },
//     {
//       "typeDefinition": {
//         "name": "contactNumber",
//         "type": "string",
//         "fieldType": "text"
//       },
//       "supportedValueTypes": [
//         "STATIC_VALUE"
//       ],
//       "isRequired": true
//     },
//     {
//       "typeDefinition": {
//         "name": "countryCode",
//         "type": "string",
//         "fieldType": "text"
//       },
//       "supportedValueTypes": [
//         "STATIC_VALUE"
//       ],
//       "isRequired": false
//     },
//     {
//       "typeDefinition": {
//         "name": "senderid",
//         "type": "string",
//         "fieldType": "text"
//       },
//       "supportedValueTypes": [
//         "STATIC_VALUE"
//       ],
//       "isRequired": false
//     }
//   ],
//   "labels": {
//     "en": {
//       "inputFieldLabels": {
//         "text": "Message",
//         "contactNumber":"Contact Number",
//         "countryCode":"CountryCode",
//         "senderid":"Pingbix Sender Id"
//       },
//       "inputFieldDescriptions": {
//         "text":"Enter Message",
//         "contactNumber":"Enter Contact Number",
//         "countryCode":"Enter CountryCode",
//         "senderid":"Enter Pingbix Sender Id"
//       },
//       "actionName": "Send Pingbix SMS",
//       "actionDescription": "Send SMS via Pingbix",
//       "appDisplayName": "Pingbix",
//       "actionCardContent": "Send Pingbix SMS"
//     },
//   },
//   "objectTypes": [
//     "CONTACT","COMPANY","DEAL"
//   ]
// };
    
//     fetch("https://us-central1-ulgebra-license.cloudfunctions.net/msbrequest", {
//         "method" :"POST",
//         headers: {
//             'Content-Type': 'application/json;charset=utf-8'
//         },
//         body: JSON.stringify({
//             "method": "POST",
//             "data":body,
//             "headers":{
//                 "content-type": "application/json"
//             }
//         })
//     })
//     .then(function (response) {  
//         console.log(response);
//         return response.json();
//     })
//     .then(function (myJson) {
//         console.log(myJson);
//     })
//     .catch(function (error) {
//         console.log("Error: " + error);
//     });
//   }

