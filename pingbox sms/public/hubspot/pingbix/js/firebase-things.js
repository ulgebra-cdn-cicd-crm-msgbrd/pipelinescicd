var hubIndex = window.location.href.indexOf("#https://app.hubspot.com");
if(hubIndex != -1 && window.location.href.indexOf("/settings") != -1){
    $("body").append(`<div style="position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0,0,0,0.5);
  z-index: 2;
  cursor: pointer;"><a  style="position: absolute;
  top: 50%;
  left: 50%;
  font-size: 25px;
  color: white;
  transform: translate(-50%,-50%);
  -ms-transform: translate(-50%,-50%);" href="${window.location.href.substring(0,hubIndex)}" target="_blank">Go To Settings</a>
  </div>`)
}
var curId = 1000;
var errorId = 1000;
var initTries = 0;
var initProcessId = 1;
var storageRef = null;
var APP = {
    initialize: function () {
        APP.renderEngine.header();
    },
    renderEngine: {
        header: function () {
            $("body").prepend(`
            <header>
                <div class="mainSearchBarHolder" style="background-color:#c7c9cc;">
                    <div class="siteName">
                        <img src="https://app.pingbix.com/assets/adminCustom/assets/images/sgc_5fa83d66b55e7.svg" style="height:1%;width:150px" alt="Pingbix">
                    </div>

            <div class="contactSite">
                <a title="Contact Developer" href="https://apps.ulgebra.com/contact" target="_blank" title="Contact Developers"> <span class="material-icons">contact_support</span> Help</a>
            </div>
                    <div class="signUserInfoHolder signedIn">
                        <div class="userInfoBtn">
                            <img id="signInUserImg"/>
                        </div>
                        <div class="ualic_sub_status_tip">
                ...
            </div>
                        <div class="signedUserOptions">
                            <div class="signedUserOptionsInr">
                                <a id="signedProfileUserLink" class="listVidLink" href="./user.html">
                                    <div class="suo-item">
                                        <span class="material-icons">account_circle</span> <span id="signInUserName"> My Account </span>
                                    </div>
                                </a>
                                <div class="suo-item" id="signoutBtn">
                                        <span class="material-icons">login</span> <span id="signInUserName"> Sign out </span>
                                    </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="pageTitle" style="display:none" >
                    ${document.title}
                </div>
                <div style="display:none" id="lic-sub-allowed-user-holder" class="pmtw-outer">
                    <div class="pmtw-holder">
                        <div class="pmt-ttl">
                            Configure license users <button class="pmtw-close-btn" onclick="$('#lic-sub-allowed-user-holder').hide()">X</button> <button class="lcpi-buymore-lic-btn" onclick="$('#lic-sub-allowed-user-holder').hide();showProductPlanDetails()">Buy More License</button>
                        </div>
                        <div class="pmt-content">
                        <div class="lcpi-item-fetch-status">
                        Loading...</div>
                        </div>
                    </div>
                </div>

                <div class="signedOut signOutbackWindow"></div>
                <div id="notSignedInError">
                    <div class="signTtl">Sign in with HubSpot Email to Continue...<div>
                    <div id="loader"><div class="fetchingAudioBtn"><span class="spinnow material-icons">toys</span></div></div> <br>
                    <div  style="display: none" id="firebaseui-auth-container"></div>
                        </div>
                    </div>
                </div>
                <div class="ua-email-ver-holder" style="
                        display: none;
                        position: fixed;
                        top: 0px;
                        bottom: 0px;
                        left: 0px;
                        right: 0px;
                        background-color: rgba(0,0,0,0.4);
                        z-index: 10;
                        text-align: center;
                    ">
                            <div class="ua-email-ver-inr" style="
                        background-color: white;
                        display: inline-block;
                        margin-top: 5%;
                        padding: 10px 20px;
                        border-radius: 3px;
                        text-align: left;
                        min-width: 30%;
                        box-shadow: 0px 10px 25px rgba(0,0,0,0.5);
                    ">
                                <div class="ua-email-ver-ttl" style="
                        font-size: 25px;
                        margin: 10px 0px 15px;
                        color: crimson;
                    ">
                                    Verify your email!
                                </div>
                                <div class="ua-email-ver-tip">
                                    Check your inbox <b id="ua-email-id"></b> and verify your email. <div style="margin: 15px 0px 15px;"> <button onclick="sendReVerifyMail()" style="
                        padding: 5px 15px;
                        box-shadow: 0px 5px 5px rgba(0,0,0,0.5);
                        border: none;
                        background-color: royalblue;
                        color: white;
                        border-radius: 3px;
                        font-size: 14px;
                        cursor: pointer;
                    ">Resend Mail</button>
                    <button onclick="whatcetraApp.auth().signOut();window.location.reload()" style="
                        padding: 5px 15px;
                        box-shadow: 0px 5px 5px rgba(0,0,0,0.5);
                        border: none;
                        background-color: royalblue;
                        color: white;
                        border-radius: 3px;
                        font-size: 14px;
                        cursor: pointer;
                    ">Refresh</button> 

                    <i id="sentmailstatustext" style="
                        margin: 10px;
                        color: green;
                        display: none;
                    ">Email is sent!</i>

                    </div>
                                </div>
                            </div>
                        </div>
            </header>
            `);
        }
    }
};

function showProductPlanDetails() {
    if($('#ua-lic-product-plans').length > 0){
        $('#ua-lic-product-plans').show();
        $('.error-window-outer').hide();
        return;
    }
    $('.error-window-outer').hide();
    document.body.insertAdjacentHTML('beforeend', `<div style="display:none" id="ua-lic-product-plans" class="pmtw-outer">
        <div class="pmtw-holder">
            <div class="pmt-ttl">
                Choose your subscription plan
                <div class="pmt-chc-holder">
                    <select id="pmtp-currencySelector" onchange="ualic_changeCurrency()">
                        <option value="usd">Select Currency</option>
                        <option value="usd">USD ($)</option>
                        <option value="eur">EURO (€)</option>
                        <option value="inr">INR (₹)</option>
                        <option value="gbp">GBP (£)</option>
                        <option value="aud">AUD (A$)</option>
                    </select>
                </div>
            </div>
            <div class="pmt-content">
                <div class="pmt-plansholder">
                </div>
                <div class="pmt-userquantityholder" style="display: none">
                    <div class="pmtqn-inner">
                        <div class="pmtqn-back" onclick="$('.pmt-userquantityholder').hide()">
                        < Back
                    </div>
                    <div class="pmtqn-ttl">
                        Users / Quantity
                    </div>
                    <div class="pmtqn-subttl">
                        Number of users or agents using this application
                    </div>
                    <div class="pmtqn-content">
                        <div class="pmtqn-inc-btn" onclick="ualic_adjustQuantity(false)">
                            -
                        </div>
                        <div class="pmtqn-inc-inp">
                            <input value="1" id="quan-count" min="1" type="number" step="1"/>
                        </div>
                        <div class="pmtqn-inc-btn" onclick="ualic_adjustQuantity(true)">
                            +
                        </div>
                    </div>
                        <div class="pmtp-plan-actions">
                            <button class="pmtppa-btn" onclick="ualic_proceedToPayment()">Proceed</button>
                        </div>
                </div>
                </div>
            </div>
            <div class="ua-lic-process-loading"><span class="material-icons spinnow processing">
hourglass_top
</span> Processing, Please Wait...

    <div style="display:none" class="ualic_manual_pay_redir_window">
    <a id="ualic_manual_pay_redir_window_url" style="margin-top: 30px;font-size: 16px;display: block;color: blue;text-decoration: underline;cursor: pointer;" target="_blank" href="https://app.ulgebra.com">Click here if you're not redirected to payment</a>
     <div style="margin-top: 30px;font-size: 16px;display: block;color: white;cursor: pointer;background-color: royalblue;display: inline-block;padding: 5px 15px;border-radius: 5px;" onclick="window.location.reload()">I have made the payment, refresh now</div></div>
</div>
        </div>
    </div>`);
}

function addProductPlan(priceID, priceData) {
    var featureHTML = '';
    if(!priceData.active || priceData.type === "one_time"){
              console.log("inactive or one_time");
              return;
            }
    Object.keys(UALIC_APP_PROD_DATA).forEach(item=>{
        if(item.startsWith('stripe_metadata_plan_feat')){
            var stripeFeatPlanName = item.split('stripe_metadata_plan_feat_')[1];
            if(priceData.description.toLowerCase().replaceAll(/[\W_]+/g, '') === stripeFeatPlanName){
                UALIC_APP_PROD_DATA[item].split('\n').forEach(featLineITEM=>{
                    if(featLineITEM.trim().length <1){
                        return;
                    }
                    featureHTML+= `<div class="pmtppf-item">${featLineITEM}</div>`;
                });
            }
        }
    });
    $('.pmt-plansholder').append(`<div ${selectedCurrency !== priceData.currency ? 'style="display:none"': ''} class="pmtp-item price-item ${priceData.currency}">
                        <div class="pmtp-planname">
                            ${priceData.description}
                        </div>
                        <div class="pmtp-planprice">
                            ${currencyMap[priceData.currency]}${priceData.unit_amount/100}
                        </div>
                        <div class="pmtp-planperhow">
                            /${UALIC_APP_PROD_DATA.stripe_metadata_amount_per_display ? UALIC_APP_PROD_DATA.stripe_metadata_amount_per_display+' /'+priceData.interval : ''}
                        </div>
                        <div class="pmtp-plan-features">
                            ${featureHTML}
                        </div>
                        <div class="pmtp-plan-actions">
                            <button class="pmtppa-btn" onclick="ualic_selectPaymentPlan('${priceID}')">${priceData.trial_period_days >0 ? 'Start Free Trial': 'Select Plan'}</button>
                        </div>
                    </div>`);
}

var GoogleAuth;
var currentUser = null;
var db = null;
let isMobileView = mobileCheck();
var initTrackingId = null;
var enableTrialModesForPay = true;
window.onload = (function () {
    /// $('body').append("<audio autoplay src='ZHpyi6oDmZM/vWvTRTsVMiZbhRUPeHLmqbC25mI3/1593485308379.mpeg'></audio>");
    var firebaseConfig = {
        apiKey: "AIzaSyBPQusiIhVP8NiIOvFCQf2hq44CMFXXHck",
        authDomain: "pingbix-sms-plugin-hubspot.firebaseapp.com",
        projectId: "pingbix-sms-plugin-hubspot",
        storageBucket: "pingbix-sms-plugin-hubspot.appspot.com",
        messagingSenderId: "575918364220",
        appId: "1:575918364220:web:60f64af7ca7804fc031539",
        measurementId: "G-JVF4YFQTE4"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    storageRef = firebase.storage().ref();
    db = firebase.firestore();

    // handleClientLoad();
    var uiConfig = {
        callbacks: {
            // Called when the user has been successfully signed in.
            'signInSuccessWithAuthResult': function (authResult, redirectUrl) {
                if (currentUser == null && authResult.user) {
                    handleSignedInUser(authResult.user);
                     handleauthoriztion();
                }
                if (authResult.additionalUserInfo) {
                    document.getElementById('is-new-user').textContent =
                            authResult.additionalUserInfo.isNewUser ?
                            'New User' : 'Existing User';
                }
                // Do not redirect.
                return false;
            },
            uiShown: function () {
                $("#credential_picker_container").hide();
                $("#notSignedInError").hide();
                $(".loader").hide();
                $(".showAfterSignIn").show();
            }
        },
        'credentialHelper': firebaseui.auth.CredentialHelper.NONE,
        signInFlow: 'popup',
        signInOptions: [
            {
                "provider": firebase.auth.EmailAuthProvider.PROVIDER_ID
            }
        ],
        // credentialHelper: firebaseui.auth.CredentialHelper.GOOGLE_YOLO,
        tosUrl: 'https://apps.ulgebra.com/terms',
        privacyPolicyUrl: 'https://apps.ulgebra.com/privacy-policy'

    };


    var fbaseauth = firebase.auth();
    fbaseauth.onAuthStateChanged(function (user) {
        if(user && !user.emailVerified){
            $('.ua-email-ver-holder').show();
            console.log("User email is not verified");
            user.sendEmailVerification();
        }
        $('.waitTillAuthCheck').removeClass('waitToResolve');
        user ? handleSignedInUser(user) : handleSignedOutUser();
         handleauthoriztion();
    });
    try{
        fbaseauth.setPersistence(firebase.auth.Auth.Persistence.LOCAL);
    }
    catch(ex){
        console.log(ex);
        try{
            fbaseauth.setPersistence(firebase.auth.Auth.Persistence.SESSION);
        }catch(ex){
            console.log(ex);
            fbaseauth.setPersistence(firebase.auth.Auth.Persistence.NONE);
        }
    }


    var ui = new firebaseui.auth.AuthUI(fbaseauth);
    ui.start('#firebaseui-auth-container', uiConfig);

    $("#signoutBtn").click((function () {
        firebase.auth().signOut();
    }));

});

var handleSignedOutUser = function () {
    $("#notSignedInError").show();
    $(".signedIn").hide();
    $('#loader').hide();
    $(".signedOut").show();
    $("#firebaseui-auth-container").show();
    $(".showAfterSignIn").hide();
    $("#credential_picker_container").show();
};

function getSafeString(rawStr) {
    if (!rawStr || rawStr.trim() === "") {
        return "";
    }
    return $('<textarea/>').text(rawStr).html();
}

function doPageSignAction(){

}

var handleSignedInUser = function (user) {
    $("#credential_picker_container").hide();
    currentUser = firebase.auth().currentUser;
    $("#notSignedInError").hide();
    $(".showAfterSignIn").show();
    $(".signedIn").show();
    $(".signedOut").hide();
    var photoURL = currentUser.photoURL ? currentUser.photoURL : "https://app.azurna.com/images/profile_icon.png";
    $("#signInUserImg").attr({
        "src": photoURL
    });
    $("#signInUserName").text(currentUser.displayName);
    $("#signedProfileUserLink").attr({
        "href": "/user?id=" + currentUser.uid
    });
    currentUser = firebase.auth().currentUser;
    ualic_getAPPProdDetails();
    doPageSignAction(true);

};

$(function () {

    APP.initialize();

    var isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;

    if(isFirefox){
        $('body').append(`
            <style type="text/css">
                .process-window-outer, .error-window-outer {
                    background-color : rgba(256, 256, 256, 0.85);
                }
            </style>`);
    }

        $('.userInfoBtn').click(function () {
        var hidden = $(".signedUserOptions");
        if (hidden.hasClass('visible')) {
            hidden.animate({"right": "-750px"}).removeClass('visible');
        } else {
            hidden.animate({"right": "0px"}).addClass('visible');
        }
    });

});






function showElem(elem) {
    if (isMobileView) {
        elem.slideDown();
    } else {
        elem.show();
    }
}

function hideElem(elem) {
    if (isMobileView) {
        elem.slideUp();
    } else {
        elem.hide();
    }
}

function removeElem(elem){
    elem.remove();
}

function removeIDElem(elemID){
    $(elemID).remove();
}

function hideElemNoAnimation(elem) {
    elem.hide();
}

function mobileCheck() {
    let check = false;
    (function (a) {
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4)))
            check = true;
    })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
}

function getQueryParamValue(q) {
    let query = new URLSearchParams(window.location.search);
    return query.get(q);
}

function showProcess(text, id){
    $(".process-window-outer").show();
    $("#process-window-items").append(`<div id="process-item-${id}" class="process-window-item"> <i class="material-icons spinnow">hourglass_top</i> ${text}</div>`);
}

function processCompleted(id){
    $(`#process-item-${id}`).remove();
    if(($("#process-window-items").children().length) === 0){
        $(".process-window-outer").hide();
    }
}

function showTryAgainError(){
    showErroMessage('Try again later');
}

function showInvalidCredsError(){
    // showErroMessage('Given Plivo Auth ID or Authtoken is Invalid <br><br> Try again with proper Plivo credentials from <a href="https://console.plivo.com/dashboard/" title="Click to go to plivo dashboard" target="_blank" noopener nofollow>Plivo dashboard</a>.');
}

function showErroMessage(html){
    showErroWindow('Unable to process your request', html);
}

function showErroWindow(title, html){
    var id = errorId++;
   $('body').append(`<div class="error-window-outer" id="error-window-${id}">
            <div class="error-window-inner">
                <div class="error-window-title">
                    ${title}
                </div>
                <div class="error-window-detail">
                    ${html}
                </div>
                <div class="error-window-close" onclick="removeIDElem('#error-window-${id}')">
                   <i class="material-icons">close</i> Close
                </div>
            </div>
        </div>`);
}

function valueExists(val) {
    return val !== null && val !== undefined && val.length > 0 && val!=="null";
}

function closeAllErrorWindows(){
    $('.error-window-outer').remove();
}

var licDetJSON = {};
var licPlanArray = [];
var licCurCredit = 0;
/*var licSupportedPlans = {
"price_1IdrL2Ho9p6j6FgR5qyjJOo0":{
 "type": "premium"
},"price_1IP0aDHo9p6j6FgRibv4isf0":{
"type": "premium"
},"price_1I6CrpHo9p6j6FgRISpRviN4":{
"type": "premium"
},"price_1HyZblHo9p6j6FgRQzDdcl81":{
"type": "premium"
},"price_1HyZbTHo9p6j6FgRGfOwxuwQ":{
"type": "premium"
},"price_1HyZbFHo9p6j6FgRVHU989Un":{
"type": "premium"
},"price_1IP0ZkHo9p6j6FgRqHyLCI4z":{
"type": "business"
},"price_1I6CrFHo9p6j6FgRhnRWESX1":{
"type": "business"
},"price_1HyZaxHo9p6j6FgRnWjTs3we":{
"type": "business"
},"price_1HyZadHo9p6j6FgRe0Ai3LrT":{
"type": "business"
},"price_1HyZEtHo9p6j6FgRAvpfOvqM":{
"type": "business"
},"price_1HyZAgHo9p6j6FgReqwmnUp7":{
"type": "classic"
},"price_1HyZAgHo9p6j6FgRqQGZavms":{
"type": "classic"
},"price_1IP0YzHo9p6j6FgRg15L9O7H":{
"type": "classic"
},"price_1I6CmKHo9p6j6FgRQv6Cpet2":{
"type": "classic"
},"price_1HyeEsHo9p6j6FgRoSkOo1kP":{
"type": "classic"
},"price_1HyeESHo9p6j6FgRy1Pwz7FH":{
"type": "classic"
}};*/
var licChosenSubId = null;
var licChosenPlanId = null;

const STRIPE_PUBLISHABLE_KEY = 'pk_live_vZ8o9dQoM2zharvDfdExWgZf00GIVF1VvZ';

var taxRates = ['txr_1Hv2S0Ho9p6j6FgR0qRizWPw'];
var selectedProductId = null;
var selectedCurrency =  new Date().toString().indexOf("India") >0 ? "inr" : "usd";
var selectedPaymentPlanId = null;
var currencyMap = {
  "inr": "₹"  ,
  "usd": "$",
  "eur": "€",
  "gbp": "£",
  "aud": "A$"
};
var selectedSubType = "recurring";

var pricesObj = {};

const functionLocation = 'us-central1';

function viewMySubcribed(selected) {

  if($(selected).attr('data-checked') == 1) {
        $(selected).attr('data-checked', 0);
        $(selected).removeClass('subcribed-portal-buttonTrue');
        $('#selectProductsContainer').show();
        $('#subcribedProductsContainer').hide();
  }
  else {
      $(selected).attr('data-checked', 1);
      $(selected).addClass('subcribed-portal-buttonTrue');
      $('#selectProductsContainer').hide();
      $('#subcribedProductsContainer').show();
  }

}

function subcribedProductId(quantity, subcribeId) {

  db.collection('customers').doc(currentUser).collection('subscriptions').doc(subcribeId).collection('users').get().then(function (querySnapshot) {

          if(!querySnapshot.docs.length) {
              for(i=0; i<quantity; i++) {
                  $('.userEmailAddOuter').append(`<div class="addUserEmailIdOut">
                      <div class="addUserEmailIdBox" style=""><input type="email" data-added="0" placeholder="Add User Email" class="addUserEmailIdInput"></div>
                      <div class="addedUserEmailIdOut" style="display: none;">
                          <div class="addedUserEmailId" style=""></div>
                          <div class="addUserEmailIdRemove">×</div>
                      </div>
                  </div>`);
              }
              $('.addUserEmailIdInput').first().focus();
              return true;
          }

          let allQuantityCheck = 0;
          querySnapshot.forEach(async function (doc) {

              allQuantityCheck++;
              $('.userEmailAddOuter').append(`<div class="addUserEmailIdOut">
                      <div class="addUserEmailIdBox"  style="display: none;"><input type="email" data-added="1" placeholder="Add User Email" class="addUserEmailIdInput"></div>
                      <div class="addedUserEmailIdOut">
                          <div class="addedUserEmailId" style="">${doc.data().email}</div>
                          <div class="addUserEmailIdRemove">×</div>
                      </div>
                  </div>`);

          });

          if(allQuantityCheck < quantity) {
              quantity = quantity - allQuantityCheck;
              for(i=0; i<quantity; i++) {
                  $('.userEmailAddOuter').append(`<div class="addUserEmailIdOut">
                      <div class="addUserEmailIdBox" style=""><input type="email" data-added="0" onkeypress="$(this).removeClass('addUserEmailIdInputErr')" onfocus="$(this).css({'border-color': '#2172f6'})" onfocusout="$(this).css({'border-color': 'black'})" placeholder="Add User Email" class="addUserEmailIdInput"></div>
                      <div class="addedUserEmailIdOut" style="display: none;">
                          <div class="addedUserEmailId" style=""></div>
                          <div class="addUserEmailIdRemove" onclick="ualic_removeEditUserEmail(this)">×</div>
                      </div>
                  </div>`);
              }
          }
          else {
            $('.addUsersEmailIdSaveButt').hide();
          }

  });

  function removeEditUserEmail(selected) {
      $('.addUsersEmailIdSaveButt').show();
      $(this).parent().parent().find('.addedUserEmailIdOut').hide();
      $(this).parent().parent().find('.addUserEmailIdBox').show();
      $(this).parent().parent().find('.addUserEmailIdInput').attr('data-added', '0');
  }

  function addUsersEmailIdSaveButt() {
      $('.addUserEmailIdInput').each(function() {

        if($(this).attr('data-added') == '0') {

          let reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
          if($(this).val().trim() == '' || reg.test(int_mail) == false) {
            $(this).addClass('addUserEmailIdInputErr').focus();
            return false;
          }

          db.collection('customers').doc(currentUser).collection('subscriptions').doc(subcribeId).collection('users').doc($(this).val().trim()).set({
              email: $(this).val().trim(),
              createdTime: Date.now()
          }, {merge: true});

          $(this).parent().parent().find('.addedUserEmailIdOut').show();
          $(this).parent().parent().find('.addedUserEmailId').text($(this).val().trim());
          $(this).val('');
          $(this).parent().parent().find('.addUserEmailIdInput').attr('data-added', '1');
          $(this).parent().parent().find('.addUserEmailIdBox').hide();

        }

      });
  }

}

var existingLicenseOwnerList = {};
async function ualic_fetchExistingLicenseOwnersList(){
   /* firebase.database().ref(`ualic_user_credits/${currentUser}`).on('value', function(snapshot){
        $('#ua-lic-credit-amount').text(snapshot.val() ? snapshot.val() : 0);
      });*/

  await db.collection('UALicenseOwners').doc(currentUser.uid).collection('subscriptions').get().then(snapshot=>{
    if(snapshot.empty){
      return false;
    }
    snapshot.docs.forEach(item=>{
      existingLicenseOwnerList[item.id] = item.data();
    });
    return true;
  });
  //ualic_renderSubscribeUserList();
}
//var UALIC_APP_PRODUCT_ID = "prod_IZibGrC2KlZfyE";
var UALIC_APP_PROD_DATA = {};
var UALIC_APP_PRICE_DATA = {};

function ualic_getAPPProdDetails(){
    if(typeof UALIC_APP_PRODUCT_ID === "undefined" || !UALIC_APP_PRODUCT_ID){
        $('.ualic_sub_status_tip').text('');
        return;
    }
    $('.ualic_sub_status_tip').text('Fetching product details...');
    ualic_fetchExistingLicenseOwnersList();
    Promise.all([
        (
            Promise.resolve((db.doc(`products/${UALIC_APP_PRODUCT_ID}`).get().then(doc=>{
                UALIC_APP_PROD_DATA = doc.data();
                return true;
            })))
        ),
        (
           Promise.resolve((db.collection(`products/${UALIC_APP_PRODUCT_ID}/prices`).get().then(coll=>{
                coll.docs.forEach(doc=>{
                    UALIC_APP_PRICE_DATA[doc.id] = doc.data();
                });
                return true;
            })))
        )
    ])
    .then(function(){
        console.log("All Done!");
        ualic_renderSubscribeUserList();
        showProductPlanDetails();
        $('.pmt-plansholder').html('');
        for(var item in UALIC_APP_PRICE_DATA){
            addProductPlan(item, UALIC_APP_PRICE_DATA[item]);
        }
    });
}
var existingLicenseOwnerCountForCurrentSubs = 0;
async function ualic_renderSubscribeUserList(){
  try{
    $('.ualic_sub_status_tip').text('Fetching subscription details...');
    $('.license-configure-panel').show();
    $('.lcpi-item-fetch-status').text('Checking for active subscriptions...');
    $('.lcp-lic-item').remove();
    var planPurchaseUsersListFilled = await db.collection('customers').doc(currentUser.uid).collection('subscriptions').get().then(async (snapshot) => {
      if (snapshot.empty) {
        $('.lcpi-item-fetch-status').text("You don't have any active subscriptions, purchase below then configure users here.");
        return false;
      }
      enableTrialModesForPay = false;
      var activeSubsCount = 0;

      for(var i=0;i<snapshot.docs.length;i++){
        try{
      const subscription = snapshot.docs[i].data();
      if(subscription.product.id !== UALIC_APP_PRODUCT_ID){
        continue;
      }
      var productId = UALIC_APP_PRODUCT_ID;
      var productData = UALIC_APP_PROD_DATA;
      $('.lcpi-item-fetch-status').hide();
      var priceData = UALIC_APP_PRICE_DATA[subscription.price.id];
      var priceId = subscription.price.id;
      var subscriptionId = snapshot.docs[i].id;
      var isActive = subscription.status === "active" || subscription.status === "trialing";
      if((!isActive && !includeInActiveSubs)){
        continue;
      }
      planPurchaseUsersListFilled = true;
      if(!priceData){
        console.log(' priceData not found '+ priceId);
        return;
      }
      if(!productData){
        console.log(' productData not found '+ productId);
        return;
      }
      activeSubsCount++;
      $('#lic-sub-allowed-user-holder .pmt-content').append(`
        <div class="lcp-lic-item" id="license-item-${subscriptionId}">
            <div class="lcpi-itemmeta">
            <div class="lcpi-icon">
              <img src="${productData.images ? productData.images[0]: ''}"/>
            </div>
            <div class="lcpi-ttl-meta">
              <div class="lcp-lic-item-ttl">
                ${productData.name}
              </div>
              <div class="lcpi-quantity">
                <div class="lcpi-plantype">${priceData.description}</div> <div class="lcpi-plantype">${subscription.quantity} Users</div>

                <span style="color: grey;margin-left: 15px;"> ${subscription.status.toUpperCase()} ${isActive ? " from <b>"+(subscription.current_period_start ? subscription.current_period_start.toDate().toLocaleDateString(): "")+"</b> to <b>"+(subscription.current_period_end ? subscription.current_period_end.toDate().toLocaleDateString(): "")+"</b>" : "<span class='c-red' style='margin-right:10px'>Subscription is not active</span>"+" Cancelled on <b>"+(subscription.canceled_at ? subscription.canceled_at.toDate().toLocaleDateString() : "")+"</b>"} </span>

              </div>
            </div>
          </div>
            <div class="lcpi-user-tip">
              ${productData.stripe_metadata_userIdText &&  productData.stripe_metadata_userIdText !== "" ? productData.stripe_metadata_userIdText : 'Provide EMail Ids of the CRM users to allow them to use this license.'}
            </div>
            <div class="lcpi-users">

            </div>
            <div class="lcpi-action">
              <button class="lcpi-action-save" ${!isActive ? "style='display:none'" : ""} onclick="ualic_updateLICUsers('${subscriptionId}','${priceId}')">Save</button>
              <button class="lcpi-action-save" ${!isActive ? "style='display:none'" : "style='float: right;background-color: green;'"} onclick="$('#lic-sub-allowed-user-holder').hide();showProductPlanDetails()">Add more users</button>
            </div>
          </div>`);

      for(var j=0;j<subscription.quantity;j++){
        var value = existingLicenseOwnerList[subscriptionId] && existingLicenseOwnerList[subscriptionId].userIds.length > j ? existingLicenseOwnerList[subscriptionId].userIds[j]: "";
        if(value){
            existingLicenseOwnerCountForCurrentSubs++;
        }
        $(`#license-item-${subscriptionId} .lcpi-users`).append(`<div class="lcpi-user-item">
                <div class="lcpi-u-index">${j+1}</div><div class="lcpi-u-inp"><input class="inp_lcpi-user" type="text" ${!isActive ? "disabled" : ""} value="${value}" ${value && value.trim()!=="" ? "disabled readonly" : ""}/></div>
              </div>`);
      }
    }catch(ex){
      console.log(ex);
    }
    }
        if(activeSubsCount === 0){
          showProductPlanDetails();
         // $('.lcpi-item-fetch-status').text("You don't have any active subscriptions, purchase below then configure users here.").show();
        }
        else{
            $('#ua-lic-product-plans').hide();
            if(existingLicenseOwnerCountForCurrentSubs === 0){
                $('#lic-sub-allowed-user-holder').show();
            }
            if(activeSubsCount === 1){
               $('.ualic_sub_status_tip').html(`<button onclick="$('#lic-sub-allowed-user-holder').show()">Configure users</button>`);
            }
            else{
                $('.ualic_sub_status_tip').html(`Active licenses : ${activeSubsCount} <button onclick="$('#lic-sub-allowed-user-holder').show()">Configure users</button>`);
            }
        }
    });
  }catch(exc){
    console.log(exc);
    return false;
  }
  if(!planPurchaseUsersListFilled){

  }
}

var impersonUserId = null;
var includeInActiveSubs = false;

// Checkout handler
async function ualic_proceedToPayment() {
  if(selectedCurrency !== "inr"){
    taxRates = ["txr_1IlvlbHo9p6j6FgRG8iwtor9"];
  }
  $(".ua-lic-process-loading").show();
  const docRef = await db
    .collection('customers')
    .doc(currentUser.uid)
    .collection('checkout_sessions')
    .add({
      price: selectedPaymentPlanId,
      allow_promotion_codes: true,
      tax_rates: taxRates,
      success_url: window.self !== window.top ? 'https://app.ulgebra.com' : window.location.href,
      cancel_url: window.self !== window.top ? 'https://app.ulgebra.com' : window.location.href,
      quantity: parseInt($("#quan-count").val()),
      trial_from_plan: enableTrialModesForPay,
      metadata: {
        tax_rate: taxRates.length > 0 ? '18% GST exclusive' : '',
        payment_made_from: window.location.href
      }
    });
  // Wait for the CheckoutSession to get attached by the extension
  docRef.onSnapshot((snap) => {
    const { error, sessionId } = snap.data();
    if (error) {
      // Show an error to your customer and then inspect your function logs.
      alert(`An error occured: ${error.message}`);
      document.querySelectorAll('button').forEach((b) => (b.disabled = false));
    }
    if (sessionId) {
      // We have a session, let's redirect to Checkout
      // Init Stripe
      if(window.self !== window.top){
        var paymentURL = 'https://app.azurna.com/redirect-to-checkout?stripe_session_id='+sessionId;
        $('.ualic_manual_pay_redir_window').show();
        $('#ualic_manual_pay_redir_window_url').attr({'href': paymentURL});
        window.open(paymentURL, '_blank').focus();
      }
      else{
        const stripe = Stripe(STRIPE_PUBLISHABLE_KEY);
        stripe.redirectToCheckout({ sessionId });
      }
    }
  });
}

// Billing portal handler
async function ualic_redirectToBillingPortal(){
      try{
            $(".processing").show();
            //document.querySelectorAll('button').forEach((b) => (b.disabled = true));

            // Call billing portal function
            const functionRef = firebase
              .app()
              .functions(functionLocation)
              .httpsCallable('ext-firestore-stripe-subscriptions-createPortalLink');
            const { data } = await functionRef({ returnUrl: window.location.origin });
            window.location.assign(data.url);
      }
      catch(exc){
          console.log(exc);
          alert("Unable to retrive your subscriptions or you don't have any subscriptions.");
          $(".processing").hide();
      }
  }

function ualic_selectPaymentPlan(planId){
    selectedPaymentPlanId = planId;
    $('.pmt-userquantityholder').show()
}

function ualic_adjustQuantity(increase){
    var currentVal = parseInt($('#quan-count').val());
    var nextVal = increase ? currentVal+1 : currentVal-1;
    if(nextVal<1){
        return;
    }
    $('#quan-count').val(nextVal);
    if(nextVal <2){
        $("#quan-decrease").attr({'disabled': true});
    }
    else{
        $("#quan-decrease").removeAttr("disabled");
    }
}

function ualic_changeCurrency(){
    selectedCurrency = $("#pmtp-currencySelector").val();
    $('.price-item').hide();
    $('.price-item.'+selectedCurrency).show();
}

function ualic_updateLICUsers(subscriptionId, priceId){
  var updatableUserIds = [];
  var updateCallFunction = firebase.functions().httpsCallable('saveLICUsersList');
  $(`#license-item-${subscriptionId} .inp_lcpi-user`).each(function(index){
    var idItem = $(this).val().trim();
    if(idItem !==""){
      if(!updatableUserIds.includes(idItem)){
        updatableUserIds.push(idItem);
      }
    }
  });
  $(`#license-item-${subscriptionId} .lcpi-action`).append('<b class="licpi-cs-status" style="color:royalblue"> Saving user details...</b>');
  updateCallFunction({
    "priceId": priceId,
    "subscriptionId": subscriptionId,
    "userIds": updatableUserIds
  }).then(res=>{
    $(`#license-item-${subscriptionId} .licpi-cs-status`).css({'color':'green'}).text("User details has been saved");
    setTimeout(function(){
      $(`#license-item-${subscriptionId} .licpi-cs-status`).remove();
    }, 2000);
    console.log(res);
  }).catch(ex=>{
    console.log(ex);
  });
}
